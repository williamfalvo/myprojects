import cImage

def tonoVerde(immagine_originale):
    larghezza = immagine_originale.getWidth()
    altezza = immagine_originale.getHeight()
    nuova_immagine = cImage.EmptyImage(larghezza,altezza)
    
    #Modifico l'immagine
    for i in range(altezza):
        for j in range(larghezza):
            p= immagine_originale.getPixel(j,i)

            green= p.getGreen()
            
            newPixel= cImage.Pixel(0,green,0)
            nuova_immagine.setPixel(j,i,newPixel)  
            
    return nuova_immagine

def tonoRosso(immagine_originale):
    larghezza = immagine_originale.getWidth()
    altezza = immagine_originale.getHeight()
    nuova_immagine = cImage.EmptyImage(larghezza,altezza)
    
    #Modifico l'immagine
    for i in range(altezza):
        for j in range(larghezza):
            p= immagine_originale.getPixel(j,i)

            red= p.getRed()
            
            newPixel= cImage.Pixel(red,0,0)
            nuova_immagine.setPixel(j,i,newPixel)  
            
    return nuova_immagine

def tonoBlu(immagine_originale):
    larghezza = immagine_originale.getWidth()
    altezza = immagine_originale.getHeight()
    nuova_immagine = cImage.EmptyImage(larghezza,altezza)
    
    #Modifico l'immagine
    for i in range(altezza):
        for j in range(larghezza):
            p= immagine_originale.getPixel(j,i)

            blue= p.getBlue()
            
            newPixel= cImage.Pixel(0,0,blue)
            nuova_immagine.setPixel(j,i,newPixel)  
            
    return nuova_immagine

def scombinaImmagine(immagine_originale):
    larghezza = immagine_originale.getWidth()
    altezza = immagine_originale.getHeight()
    nuova_immagine = cImage.EmptyImage(larghezza,altezza)
    
    #Prendo il primo cubo dell'immagine e lo metto in basso a destra
    for i in range(0, altezza//2):
        for j in range(0,larghezza//2):
            pixel= immagine_originale.getPixel(j,i)
            nuova_immagine.setPixel(j+larghezza//2, i+altezza//2, pixel)
    #Prendo il secondo cubo in alto e lo metto e sinistra
    for i in range(0, altezza//2):
        for j in range(larghezza//2, larghezza):
            pixel= immagine_originale.getPixel(j,i)
            nuova_immagine.setPixel(j-larghezza//2, i, pixel)
    #Prendo il primo cubo in basso e lo sposto in alto a destra
    for i in range(altezza//2, altezza):
        for j in range(0, larghezza//2):
            pixel= immagine_originale.getPixel(j,i)
            nuova_immagine.setPixel(j+larghezza//2, i-altezza//2, pixel)
    #Prendo l'ultimo cubo e lo metto al posto del precedente
    for i in range(altezza//2, altezza):
        for j in range(larghezza//2, larghezza):
            pixel= immagine_originale.getPixel(j,i)
            nuova_immagine.setPixel(j-larghezza//2, i, pixel)
            
    return nuova_immagine

def riflettiVerticale(immagine_originale):
    larghezza = immagine_originale.getWidth()
    altezza = immagine_originale.getHeight()
    nuova_immagine = cImage.EmptyImage(larghezza,altezza)
    
    #Modifico l'immagine
    for i in range(altezza-1, -1, -1):
        k= 0
        for j in range(larghezza-1,-1,-1):
            p= immagine_originale.getPixel(j,i)

            red= p.getRed()
            green= p.getGreen()
            blue= p.getBlue()
            
            newPixel= cImage.Pixel(red,green,blue)
            nuova_immagine.setPixel(k,i,newPixel)

            k += 1
            
    return nuova_immagine

def riflettiOrizzontale(immagine_originale):
    larghezza = immagine_originale.getWidth()
    altezza = immagine_originale.getHeight()
    nuova_immagine = cImage.EmptyImage(larghezza,altezza)
    
    #Modifico l'immagine
    for j in range(larghezza-1, -1, -1):
        k= 0
        for i in range(altezza-1,-1,-1):
            p= immagine_originale.getPixel(j,i)

            red= p.getRed()
            green= p.getGreen()
            blue= p.getBlue()
            
            newPixel= cImage.Pixel(red,green,blue)
            nuova_immagine.setPixel(j,k,newPixel)

            k += 1
            
    return nuova_immagine

def convertiGrigio_click(immagine_originale,finestra):
    larghezza = immagine_originale.getWidth()
    altezza = immagine_originale.getHeight()
    nuova_immagine = cImage.EmptyImage(larghezza,altezza)

    #L'utente seleziona una porzione dell'immagine da modificare
    print("Selezionare l'area da modificare cliccando in alto a sinistra e poi in basso a destra")
    clicks= finestra.captureClicks(2)
    (x0,y0)= clicks[0]
    (x1,y1)= clicks[1]
    
    #Creo una copia dell'immagine originale
    for i in range(altezza):
        for j in range(larghezza):
            pixel_originale = immagine_originale.getPixel(j,i)
            nuova_immagine.setPixel(j,i,pixel_originale)
            
    #Modifico in toni di grigio la porzione selezionata precedentemente 
    for row in range(clicks[0][1], clicks[1][1]):
        for col in range(clicks[0][0], clicks[1][0]):
            p= immagine_originale.getPixel(col,row)

            red= p.getRed()
            green= p.getGreen()
            blue= p.getBlue()
            
            grey= (red+green+blue) // 3

            newPixel= cImage.Pixel(grey, grey, grey)
            nuova_immagine.setPixel(col,row,newPixel)  
            
    return nuova_immagine

def aumento_verde(immagine_originale):
    larghezza = immagine_originale.getWidth()
    altezza = immagine_originale.getHeight()
    nuova_immagine = cImage.EmptyImage(larghezza,altezza)
    
    #L'utente indica la percentuale di aumento della componente verde
    percentuale= int(input("Indicare la percentuale di aumento della componente verde: "))

    #Modifico l'immagine
    for i in range(altezza):
        for j in range(larghezza):
            p= immagine_originale.getPixel(j,i)

            #Calcolo il valore della componente verde aumentata
            green_aumento= (p.getGreen()* percentuale)//100
            green= p.getGreen() + green_aumento
            
            #Controllo che la componente rossa non sfori da 255
            if green > 255 :
                green = 255

            #Continuo con la modifica dell'immagine
            red= p.getRed()
            blue= p.getBlue()

            newPixel= cImage.Pixel(red,green,blue)
            nuova_immagine.setPixel(j,i,newPixel)  
            
    return nuova_immagine

def aumento_rosso(immagine_originale):
    larghezza = immagine_originale.getWidth()
    altezza = immagine_originale.getHeight()
    nuova_immagine = cImage.EmptyImage(larghezza,altezza)
    
    #L'utente indica la percentuale di aumento della componente rossa
    percentuale= int(input("Indicare la percentuale di aumento della componente rossa: "))

    #Modifico l'immagine
    for i in range(altezza):
        for j in range(larghezza):
            p= immagine_originale.getPixel(j,i)

            #Calcolo il valore della componente rossa aumentata
            red_aumento= (p.getRed()* percentuale)//100
            red= p.getRed() + red_aumento
            
            #Controllo che la componente rossa non sfori da 255
            if red > 255 :
                red = 255

            #Continuo con la modifica dell'immagine
            green= p.getGreen()
            blue= p.getBlue()

            newPixel= cImage.Pixel(red,green,blue)
            nuova_immagine.setPixel(j,i,newPixel)  
            
    return nuova_immagine

def aumento_blu(immagine_originale):
    larghezza = immagine_originale.getWidth()
    altezza = immagine_originale.getHeight()
    nuova_immagine = cImage.EmptyImage(larghezza,altezza)
    
    #L'utente indica la percentuale di aumento della componente blu
    percentuale= int(input("Indicare la percentuale di aumento della componente blu: "))

    #Modifico l'immagine
    for i in range(altezza):
        for j in range(larghezza):
            p= immagine_originale.getPixel(j,i)

            #Calcolo il valore della componente blu aumentata
            blue_aumento= (p.getBlue()* percentuale)//100
            blue= p.getBlue() + blue_aumento
            
            #Controllo che la componente rossa non sfori da 255
            if blue > 255 :
                blue = 255

            #Continuo con la modifica dell'immagine
            green= p.getGreen()
            red= p.getRed()

            newPixel= cImage.Pixel(red,green,blue)
            nuova_immagine.setPixel(j,i,newPixel)  
            
    return nuova_immagine

def trasformazione(file_immagine):
    # carichiamo e mostriamo l'immagine originale
    immagine_originale = cImage.Image(file_immagine)            
    larghezza = immagine_originale.getWidth()
    altezza = immagine_originale.getHeight()
    finestra = cImage.ImageWin("Immagine originale",larghezza,altezza)
    immagine_originale.draw(finestra)

    # facciamo vedere all'utente le trasformazioni disponibili
    trasformazioni=['aumentoBlu','aumentoRosso','aumentoVerde','convertoGrigio',
                    'riflettiOrizzontale','riflettiVerticale','scombinaImmagine',
                    'tonoBlu','tonoRosso','tonoVerde']
    print("IL PROGRAMMA DISPONE DI QUESTE TRASFORMAZIONI:")
    for i in trasformazioni:
        print('-',i)
    print()

    # facciamo scegliere all'utente quale trasformazione utilizzare
    scelta= input("Digita una delle trasfomazioni disponbili da effetture: ")
    
    # invochiamo la funzione che implementa l'operazione da effettuare
    if scelta == 'aumentoBlu':
        nuova_immagine= aumento_blu(immagine_originale)
    if scelta == 'aumentoRosso':
        nuova_immagine= aumento_rosso(immagine_originale)
    if scelta == 'aumentoVerde':
        nuova_immagine= aumento_verde(immagine_originale)
    if scelta == 'convertoGrigio':
        nuova_immagine= convertiGrigio_click(immagine_originale,finestra)
    if scelta == 'riflettiOrizzontale':
        nuova_immagine= riflettiOrizzontale(immagine_originale)
    if scelta == 'riflettiVerticale':
        nuova_immagine= riflettiVerticale(immagine_originale)
    if scelta == 'scombinaImmagine':
        nuova_immagine= scombinaImmagine(immagine_originale)
    if scelta == 'tonoBlu':
        nuova_immagine= tonoBlu(immagine_originale)
    if scelta == 'tonoRosso':
        nuova_immagine= tonoRosso(immagine_originale)
    if scelta == 'tonoVerde':
        nuova_immagine= tonoVerde(immagine_originale)
        
    # mostiamo la nuova immagine
    larghezza = nuova_immagine.getWidth()
    altezza = nuova_immagine.getHeight()
    finestra_2 = cImage.ImageWin("Nuova immagine",larghezza,altezza)
    nuova_immagine.draw(finestra_2)
    finestra_2.exitOnClick()
    finestra.exitOnClick()
    
def main():
    print("Benvenuto, questo è un programma di tipo testuale che ti permette");
    print("di aprire un immagine gif, presente nella stessa cartella del programma,");
    print("ed effettuare varie trasformazioni (una sola per avvio).");
    print();
    img= input("Digitare l'immagine da aprire (incluso '.gif') : ")
    trasformazione(img)

main()
