﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinViewerInMenu : MonoBehaviour {

    List<string> skinnames;
    List<Sprite> skins;
    int id;
    Text text;
    CharacterEditorMenu cem;
    FastPlayMenu fpm;
    GameObject obj_cem;
    GameObject obj_fpm;

	// Use this for initialization
	void Start () {
        skinnames = GameManager.instance.skins;
        if (skinnames == null) Debug.Log("Colpa delle skin!");
        obj_cem = GameObject.Find("popup_character_edit");
        obj_fpm = GameObject.Find("popup_fastplay");
        cem = gameObject.GetComponentInParent<CharacterEditorMenu>();
        fpm = gameObject.GetComponentInParent<FastPlayMenu>();
        if (obj_cem != null) {
            if (obj_cem.activeSelf) id = cem.Id;
        }
        else if (obj_fpm != null) {
            if (obj_fpm.activeSelf) id = fpm.Skin_id;
        }
        else id = 0;
        text = gameObject.GetComponent<Text>();
        if (text == null) Debug.Log("Colpa del testo!");
    }
	
	// Update is called once per frame
	void Update () {
        if (obj_cem != null) {
            if (obj_cem.activeSelf) id = cem.Id;
        }
        else if (obj_fpm != null) {
            if (obj_fpm.activeSelf) id = fpm.Skin_id;
        }
        text.text = skinnames[id];
	}
}
