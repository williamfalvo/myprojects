﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollow : MonoBehaviour
{	
	//Move the object at the same speed of player
	void Update ()
    {
		transform.position += new Vector3(0, 0, GameManager.instance.vSpeed * Time.deltaTime);
    }
}
