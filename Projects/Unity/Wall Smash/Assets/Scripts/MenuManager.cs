﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class MenuManager
{
    //List<Image> imgs;

    public MenuManager(List<Image> imgs) {
        //this.imgs = imgs;
        foreach (Image i in imgs) {
            i.enabled = true;
        }
    }
    public MenuManager() { }

    public static void GoToRoom(string destinationName) {
        SceneManager.LoadScene(destinationName);
    }


    public static void SpawnPrefab(GameObject popup, List<Image> imgs) {
        GameObject.Find("Canvas").BroadcastMessage("UpdateSkin");
        GameObject.Find("Canvas").BroadcastMessage("UpdateDiff");
        GameObject.Find("Canvas").BroadcastMessage("UpdateMap");
        if (GameManager.instance.Menu_spawner != null) {
            if (GameManager.instance.Menu_spawner != GameObject.Find("fullsize_store")) {
                DestroyPrefab(GameManager.instance.Menu_spawner);
            }
            
        }
        popup.SetActive(true);
        if (imgs != null) {
            foreach (Image i in imgs) {
                i.color = new Color(i.color.r, i.color.g, i.color.b, 0);
                var b = i.GetComponent<Button>();
                b.enabled = false;
                //b.onClick.RemoveAllListeners();
                b.onClick.SetPersistentListenerState(0, UnityEventCallState.Off);
            }
        }
        GameManager.instance.Menu_spawner = popup;
    }

    public static void SpawnPrefab(GameObject popup) {
        SpawnPrefab(popup, null);
    }

    public static void DestroyPrefab(GameObject popup, List<Image> imgs) {
        popup.SetActive(false);
        if (imgs != null) {
            foreach (Image i in imgs) {
                i.color = new Color(i.color.r, i.color.g, i.color.b, 1);
                var b = i.GetComponent<Button>();
                b.enabled = true;
                b.onClick.SetPersistentListenerState(0, UnityEventCallState.RuntimeOnly);
            }
        }

    }

    public static void DestroyPrefab(GameObject popup) {
        DestroyPrefab(popup, null);
    }



}