﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreMainMenu : MonoBehaviour {

    public GameObject currencyTab;
    public GameObject chestsTab;
    public GameObject otherTab;

    public void GoBack() {
        AudioManager.instance.Play("Back");
        MenuManager.DestroyPrefab(GetComponentInParent<MainMenu>().store, GetComponentInParent<MainMenu>().imgs);
    }

    public void SpawnCurrency() {
        MenuManager.DestroyPrefab(chestsTab);
        MenuManager.DestroyPrefab(otherTab);
        MenuManager.SpawnPrefab(currencyTab);
    }

    public void SpawnChests() {
        MenuManager.DestroyPrefab(currencyTab);
        MenuManager.DestroyPrefab(otherTab);
        MenuManager.SpawnPrefab(chestsTab);
    }

    public void SpawnOther() {
        MenuManager.DestroyPrefab(chestsTab);
        MenuManager.DestroyPrefab(currencyTab);
        MenuManager.SpawnPrefab(otherTab);
    }
}
