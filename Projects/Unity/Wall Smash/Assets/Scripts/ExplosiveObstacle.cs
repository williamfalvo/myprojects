﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveObstacle : DestructibleObstacle
{
    // explosive obstacle variables
    public float radius;
    Animator animator;

    // Use this for initialization
    // when the game starts the explosive obstacle is active
    new void Start ()
    {
        base.Start();
        isActive = true;
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	new void FixedUpdate ()
    {
        base.FixedUpdate();

        // when the obstacle is destroyed, it creates an explosion and increases the player's score
        if (hp <= 0 && isActive)
        {
            animator.SetTrigger("Explode");
            AudioManager.instance.Play("WallDestruction");
            Explosion();

            GameManager.instance.MassoEsplosivo += score;
            GameManager.instance.explosiveBoulderCounter++;
            GameManager.instance.objectsHit++;
            GameManager.instance.SetScore();
            isActive = false;
        }        
	}

    // the obstacle loses lifepoints if the ball collides with
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball" && other.gameObject.GetComponent<Rigidbody>().velocity != Vector3.zero)
        {
            Ball dmg = other.gameObject.GetComponent<Ball>();
            hp -= dmg.damage;
        }
    }

    // when the obstacle is destroyed the explosion checks the colliders within the area and destroys them 
    void Explosion()
    {
        Collider[] collidersToDestroy = Physics.OverlapSphere(transform.position, radius);
        
        foreach (Collider nearbyObjects in collidersToDestroy)
        {
            DestructibleObstacle dmg = nearbyObjects.GetComponent<DestructibleObstacle>();

            if (dmg != null)
            {
                dmg.hp = 0;
            }
        }        
    }
}
