﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FailsafeMenu : MonoBehaviour
{

    //Reset the main menu
    public void RestartRoom()
    {
        MenuManager.GoToRoom("MainMenu");
    }

    //Quit game function
    public void QuitGame()
    {
        Application.Quit();
    }
}
