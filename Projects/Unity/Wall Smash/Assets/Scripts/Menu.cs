﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    //Go to the main menu
    public void GoToMenu()
    {
        GameManager.instance.isPlayer1 = false;
        SceneManager.LoadScene("MainMenu");
    }
}
