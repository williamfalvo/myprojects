﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{

    public StartRoundPanel startPanel;

	//Stop the time
	void Awake ()
    {
        Time.timeScale = 0;
	}
	
	void Start () {        
        GameManager.instance.ResetCountdown();

        //Show which of the two players is playing, and what round they are
        if (GameManager.instance.isPlayer1)
        {
            startPanel.player.text = "Player 1";
        }
        else
        {
            startPanel.player.text = "Player 2";
        }

        switch(GameManager.instance.Level)
        {
            case (1):
                startPanel.round.text = "Round 1";
                break;
            case (2):
                startPanel.round.text = "Round 2";
                break;
            case (3):
                startPanel.round.text = "Round 3";
                break;
            default:
                startPanel.round.text = "Round 1";
                break;
        }

    }

    public void PlayGame()
    {
        //Play the time and start round
        Time.timeScale = 1;
        AudioManager.instance.Play("Confirm");
        startPanel.gameObject.SetActive(false);
    }
}
