﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundObstacle : Obstacle
{
    // ground obstacle types variables
    public Type type;
    
    // loop sounds variables
    bool isPlaying = false;
    bool isPaused;

    // Use this for initialization
    new void Start ()
    {
        base.Start();        
    }
        
    new void Update()
    {
        base.Update();

        // when the game stops the specified sounds are paused
        if (Time.timeScale == 0 && isPlaying)
        {
            switch (type)
            {
                case Type.FiammaMobile:
                    AudioManager.instance.Pause("Fire");
                    break;

                case Type.Sega:
                    AudioManager.instance.Pause("Saw");
                    break;

                case Type.Geyser:
                    AudioManager.instance.Pause("Geyser");
                    break;
            }

            isPaused = true;
        }

        // when the agme resumes the specified sounds play again
        if (Time.timeScale == 1 && isPaused)
        {            
            
            switch (type)
            {
                case Type.FiammaMobile:
                    AudioManager.instance.Play("Fire");
                    break;

                case Type.Sega:
                    AudioManager.instance.Play("Saw");
                    break;

                case Type.Geyser:
                    AudioManager.instance.Play("Geyser");
                    break;
            }            

            isPaused = false;
        }
        
    }

    
    new void FixedUpdate ()
    {
        base.FixedUpdate();

        // the sound starts if the player is at a given distance from the obstacle
        if (!isPlaying && gameObject != null && gameObject.transform.position.z <= 30f + camera.position.z)
        {
            switch (type)
            {
                case Type.FiammaMobile:
                    AudioManager.instance.Play("Fire");
                    break;

                case Type.Sega:
                    AudioManager.instance.Play("Saw");
                    break;

                case Type.Geyser:
                    AudioManager.instance.Play("Geyser");
                    break;
            }

            isPlaying = true;
        }       
    }

    new private void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);        

        // if the ball collides with an obstacle, plays a different sound based on the particular obstacle hit
        if (collision.gameObject.tag == "Ball")
        {
            switch (type)
            {

                case Type.Geyser:
                    AudioManager.instance.Play("HardWall");
                    break;

                case Type.Sega:
                    AudioManager.instance.Play("SpikeWall");
                    break;

                case Type.MuroDiPunte:
                    AudioManager.instance.Play("SpikeWall");
                    break;               
            }
        }
    }

    // when the obstacle is destroyed the loop sounds stop playing 
    private void OnDestroy()
    {
        if (isPlaying)
        {
            switch (type)
            {
                case Type.FiammaMobile:
                    AudioManager.instance.Stop("Fire");
                    break;

                case Type.Sega:
                    AudioManager.instance.Stop("Saw");
                    break;

                case Type.Geyser:
                    AudioManager.instance.Stop("Geyser");
                    break;
            }
        }
    }

    public enum Type { FiammaMobile, Sega, MuroDiPunte, Geyser, TappetoDiPunte, MinaClaymore, Lava }
}
