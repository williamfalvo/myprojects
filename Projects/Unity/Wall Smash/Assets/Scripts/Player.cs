﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    Animator animator;
    public bool smash;
    Transform player;
    SpriteRenderer sprite;    

    public GameObject ballPrefab;
    public Transform ball;
    public GameObject obstacle;
    public int hp = 3;
    public float speed = 5;
    public Image[] hpUI = new Image[3];
    public List<Sprite> pg_images;
    Dictionary<string, Sprite> pg_skins;
    Rigidbody rb;
    bool Control;
    public Hit2 angle;

    MeshRenderer mesh;
    bool isInvulnerable = false;
    float timerFx;

	// Use this for initialization
	void Start ()
    {
        // Initializes countdown variables
        GameManager.instance.timer = GameManager.instance.mainTimer;
        GameManager.instance.vspeed = GameManager.instance.vSpeed;

        // Player's components variables
        rb = GetComponent<Rigidbody>();
        animator = GetComponentInChildren<Animator>();
        mesh = GetComponentInChildren<MeshRenderer>();
        player = GetComponent<Transform>();       
        sprite = GetComponent<SpriteRenderer>();

        // Player's skin initialization  
        pg_skins = new Dictionary<string, Sprite>();
        PopulateSkins(GameManager.instance.skins);
        var skinID = GameManager.instance.Skin_index;
        var skinIndex = GameManager.instance.GetSkinName(skinID);

        if (pg_skins.ContainsKey(skinIndex))
        {
            sprite.sprite = pg_skins[skinIndex];
        }        
    }

    void PopulateSkins(List<string> names)
    {
        foreach (Sprite s in pg_images)
        {
            pg_skins.Add(names[pg_images.IndexOf(s)], s);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        // Checks if the Player is invulnerable and if it is, sets the flickering fx and changes the ball layer
        if (isInvulnerable)
        {
            InvulnerabilityFx();

            if (ball != null)
            {
                if (ball.GetComponent<Rigidbody>().velocity == Vector3.zero)
                {
                    //Layer Invulnerable = 12
                    ball.gameObject.layer = 12;
                }
                else
                {
                    //Layer Ball = 11
                    ball.gameObject.layer = 11;
                }
            }
            
        }
        else
        {
            if (ball != null)
            {
                // Layer Ball = 11
                ball.gameObject.layer = 11;
            }            
        }

        // Sets the control
        if(Control == true)
        {
            PlayerColliderActivator();            
        }

        // Countdown activator
        GameManager.instance.Countdown();

        // During the countdown the player is in Idle
        if (GameManager.instance.canCount == false)
        {
            animator.SetBool("Run", true);           
        }
        else
        {
            animator.SetBool("Run", false);        
        }

        // The player follow the Ball along the X axis
        if (ball != null)
        {
            player.position = new Vector3(ball.position.x, player.position.y, player.position.z);

            // if the ball goes past the player, he loses a life and the ball itself
            if (ball.position.z <= transform.position.z - 10)
            {
                AudioManager.instance.Play("BallOut");
                Destroy(ball.gameObject);                
                Damage();
                Destroy(hpUI[hp].gameObject);
                Debug.Log("Ahia!");    
                ball = Instantiate(ballPrefab,player.position + new Vector3(0,0,2), player.rotation).GetComponent<Transform>();
            }
        }
	}

    // the player moves along the z axis with a given speed
    private void FixedUpdate()
    {
        rb.transform.position += new Vector3(0, 0, GameManager.instance.vSpeed * Time.deltaTime);
    }

    // if the player collides
    private void OnCollisionEnter(Collision collision)
    {
        // with any obstacle except for side Walls and Collectibles 
        if(collision.gameObject.tag != "Wall" && collision.gameObject.tag != "CollectibleObstacle")
        {
            Smash();
        }
        
        // with the destructible obstacles the player takes damage, loses the ball and the obstacle is destroyed
        // with the ground obstacles the player takes damage, loses the ball and is placed on the right or on the left of the obstacle depending on where the player collides with
        // with the Tesla coils the player take damage, loses the ball and the entire object(coils and electric field) are placed behind the player
        if (collision.gameObject.tag == "Obstacle")
        {                      
            Damage();
            Destroy(hpUI[hp]);
            Destroy(collision.gameObject, 3);
            Destroy(ball.gameObject);
            ball = Instantiate(ballPrefab, player.position + new Vector3(0, 0, 2), player.rotation).GetComponent<Transform>();
        }
        else if (collision.gameObject.tag == "GroundObstacle")
        {          
            Damage();
            Destroy(hpUI[hp]);
            Destroy(ball.gameObject);
            collision.gameObject.GetComponent<GroundObstacle>().moveHorizontal = false;

            if (collision.transform.position.x <= 0)
            {
                ball = Instantiate(ballPrefab, player.position + new Vector3(2f, 0, 2), player.rotation).GetComponent<Transform>();
            }
            else if (collision.transform.position.x > 0)
            {
                ball = Instantiate(ballPrefab, player.position + new Vector3(-2f, 0, 2), player.rotation).GetComponent<Transform>();
            }
        }
        else if (collision.gameObject.tag == "BobinaDiTesla")
        {      
            Damage();
            Destroy(hpUI[hp]);
            Destroy(ball.gameObject);
            ball = Instantiate(ballPrefab, player.position + new Vector3(0, 0, 2), player.rotation).GetComponent<Transform>();
            collision.gameObject.GetComponentInParent<CampoElettricoBehaviour>().padre.transform.position += new Vector3(0, 0, -2);            
        }     
        
    }

    // if the player collides with a trigger
    private void OnTriggerEnter(Collider other)
    {
        // if it's the end round trigger than starts the End Round Cutscene
        // if it's an explosive obstacle, the obstacle is destroyed, the player takes damage and loses the ball
        if (other.gameObject.tag == "RoundEnd")
        {
            StartCoroutine(EndScene());
        }
        else if (other.gameObject.tag == "GroundObstacle")
        {
            ExplosiveObstacle explosive = other.GetComponent<ExplosiveObstacle>();
            if(explosive != null)
            {
                explosive.hp--;
            }
            Smash();
            Damage();
            Destroy(hpUI[hp]);
            Destroy(ball.gameObject);
            ball = Instantiate(ballPrefab, player.position + new Vector3(0, 0, 2), player.rotation).GetComponent<Transform>();
        }
    }

    // the coroutine plays the End Round Cutscene if the player reaches the end of the level without losing all his lives
    public IEnumerator EndScene()
    {
        if (ball != null)
        {
            Destroy(ball.gameObject);
            player.position += new Vector3(0, 0, speed * Time.deltaTime);
        }

        yield return new WaitForSeconds(2);

        GameManager.instance.EndRound();
    }
    
    // sets the damage for the player
    // the countdown starts, the player loses a life and becomes invulnerable
    // if the player's lives go to zero the round ends
    public void Damage()
    {
        if(hp > 1)
        {          
            GameManager.instance.ResetCountdown();
            hp--;
            ActivateInvulnerability();
        }
        else
        {
            hp--;            
            GameManager.instance.EndRound();
        }
    }

    // activates player's invulnerability
    public void ActivateInvulnerability()
    {
        StartCoroutine(Invulnerability());
    }

    // if the player is invulnerable he does not collides with anything for a litte amount of time (6 seconds)
    public IEnumerator Invulnerability()
    {
        gameObject.GetComponent<BoxCollider>().enabled = false;
        isInvulnerable = true;

        yield return new WaitForSeconds(6);

        Control = true;
    }

    // as soon as the invulnerability ends, the function checks if there's any obstacle around the player and if it is, it extends the invulnerability until the area around the player is free
    // after that it reactivates the player's collision system
    private void PlayerColliderActivator()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 2);

        if (colliders != null)
        {
            bool isFree = true;

            foreach (Collider obstacle in colliders)
            {
                if (obstacle.gameObject.tag != "Plane" && obstacle.gameObject.tag != "Wall" && obstacle.gameObject.tag != "Ball" && obstacle.isTrigger == false)
                {
                    isFree = false;
                }
            }

            if (isFree)
            {
                gameObject.GetComponent<BoxCollider>().enabled = true;
                GameObject.FindGameObjectWithTag("Ball").GetComponent<SphereCollider>().enabled = true;
                Control = false;
                isInvulnerable = false;
                mesh.enabled = true;
            }
        }
        else
        {
            gameObject.GetComponent<BoxCollider>().enabled = true;
            GameObject.FindGameObjectWithTag("Ball").GetComponent<SphereCollider>().enabled = true;
            Control = false;
            isInvulnerable = false;
            mesh.enabled = true;
        }
    }

    // sets an animation when the player collides 
    public void Smash()
    {
        animator.SetTrigger("Smash");
        AudioManager.instance.Play("FaceSmash");
    }

    // sets the animation of forehand
    public void Forehand()
    {
        animator.SetTrigger("Dritto");
        AudioManager.instance.Play("Hit");
    }

    // when the player is invulnerable, he starts flickering until the invulnerability ends
    public void InvulnerabilityFx()
    {
        timerFx += Time.deltaTime;        

        if(timerFx >= 0.25)
        {
            mesh.enabled = !mesh.enabled;
            timerFx = 0;
        }
    }
}
