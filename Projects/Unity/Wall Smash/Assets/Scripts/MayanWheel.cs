﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MayanWheel : MonoBehaviour
{
    public float rotation;
	
	// if the mayan wheel is moving on th right its rotation is negative otherwise is positive
	void Update ()
    {
        if (gameObject.GetComponentInParent<DestructibleObstacle>().hSpeed > 0)
        {
            transform.Rotate(0f, 0f, -rotation);
        }
        else
        {
            transform.Rotate(0f, 0f, rotation);
        }        
    }
}
