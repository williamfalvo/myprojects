﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skybox : MonoBehaviour
{
    //When it touches an object it activates its behavior
    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case ("Obstacle"):
                DestructibleObstacle dest_script = other.GetComponent<DestructibleObstacle>();
                if (dest_script != null)
                {
                    dest_script.enabled = true;
                }
                break;
            case ("GroundObstacle"):
                GroundObstacle ground_script = other.GetComponent<GroundObstacle>();
                if(ground_script != null)
                {
                    ground_script.enabled = true;
                }                
                break;
            case ("CollectibleObstacle"):
                CollectibleObstacle collect_script = other.GetComponent<CollectibleObstacle>();
                if (collect_script != null)
                {
                    collect_script.enabled = true;
                }
                break;
        }
    }
}
