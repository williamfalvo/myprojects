﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundManager : MonoBehaviour
{
    public GameObject[] collectedObstacles;

    void Awake()
    {
        //Start the setup of special obstacles
        GameManager.instance.SetupCollectiblesForActivePlayer(collectedObstacles);
    }
}
