﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hit2 : MonoBehaviour
{
    //
    public float rayLength;
    public LayerMask layermask;
    public Camera cam;

    // glow fx variables
    public SpriteRenderer glow2;
    public GameObject hitFx;
    public GameObject vector;

    // input variables
    GameObject firstTouch;
    Rigidbody ball;
    Vector3 startPosition;
    Vector3 endPosition;
    public Player player;
    public float angleShoot;

    // instantiates an object that stores the infos about the first touch
    void Start ()
    {
        firstTouch = Instantiate(vector, transform.position, Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update ()
    {
        // during the countdown player's inputs are disabled
        if (GameManager.instance.canCount == false)
        {
            // when the player touches the screen, I change the coordinates of that point from screen to world 
            if (Input.GetMouseButtonDown(0) && ball != null)
            {
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, layermask))
                {
                    startPosition = hit.point;
                    firstTouch.transform.position = startPosition;
                }
            }

            // when the player releases his finger from the screen, I get the coordinates and change them froom screen to world
            // the start and the end position are used to draw a vector and give the ball a velocity through a direction multiplied by a constant (speed)
            // the ball moves only if the direction is greater than zero
            if (Input.GetMouseButtonUp(0) && ball != null)
            {
                player.Forehand();
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, layermask))
                {
                    endPosition = hit.point;
                    Vector3 direction = (endPosition - firstTouch.transform.position).normalized;                    
                    Debug.Log(direction);

                    if (direction != Vector3.zero && direction.z > 0)
                    {
                        Instantiate(hitFx, ball.transform.position, Quaternion.identity);
                        direction *= ball.GetComponent<Ball>().speed;
                        ball.velocity = direction;
                        StartCoroutine(ball.GetComponent<Ball>().Trail());
                        GameManager.instance.objectsHit = 0;
                        GameManager.instance.ComboPar = 0;
                    }
                }

            }
        }
        else
        {
            glow2 = GameObject.FindGameObjectWithTag("Glow").GetComponent<SpriteRenderer>();
        }
	}

    // when the ball enters in the hit field, starts glowing to tell the player he can shoot
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            ball = other.GetComponent<Rigidbody>();

            if (glow2 != null)
            {
                glow2.material.color = new Color(1f, 1f, 1f, 1f);
            }

            Debug.Log("Can Shot");
        }
    }

    // when the ball leaves the hit field the ball stops glowing and the player can't shoot anymore
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            ball = null;

            if (glow2 != null)
            {
                glow2.material.color = new Color(1f, 1f, 1f, 0f);
            }            
            
            Debug.Log("Can't Shot");
        }
    }
}
