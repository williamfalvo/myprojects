﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    //MenuManager manager;
    public GameObject quickstart;
    public GameObject pgedit;
    public GameObject store;
    public GameObject options;
    public GameObject panel;

    public List<UnityEngine.UI.Image> imgs;
   
    public void PlayGame()
    {
        MenuManager.GoToRoom("SampleScene");
        //SceneManager.LoadScene("SampleScene");
    }

    public void GoBack() {
        MenuManager.GoToRoom("MainMenu");
    }

    public void ToStore() {
        //MenuManager.SpawnPrefab(store);
        ScrollUntil(3);
    }

    public void ToOptions() {
        //MenuManager.SpawnPrefab(options);
        ScrollUntil(2);
    }

    public void QuickStart() {
        panel.SetActive(true);
        AudioManager.instance.Play("Confirm");
        MenuManager.SpawnPrefab(quickstart, imgs);
        GameManager.instance.OnPopup = true;
    }

    public void EditCharacter() {
        //MenuManager.SpawnPrefab(pgedit);
        ScrollUntil(4);
    }

    public void PrepareForBattle() {
        ScrollUntil(0);
    }

    public void ToFriends() {
        ScrollUntil(1);
    }

    public void BuyGems() {
        GameObject.Find("Canvas").GetComponent<StoreMainMenu>().SpawnCurrency();
        ScrollUntil(3);
    }

    void ScrollUntil(int id) {
        GameObject.Find("MainPanel").GetComponent<ScrollManager>().StartDrag();
        if (!GameManager.instance.OnPopup)
        {
            LerpToBttn(-GameObject.Find("MainPanel").GetComponent<ScrollManager>().bttn[id].GetComponent<RectTransform>().anchoredPosition.x);
        }
        GameObject.Find("MainPanel").GetComponent<ScrollManager>().EndDrag();
    }

    void LerpToBttn(float position) {
        var panel = GameObject.Find("MainPanel").GetComponent<ScrollManager>().panel;
        float newX = Mathf.Lerp(panel.anchoredPosition.x, position, Time.unscaledDeltaTime * 100f);
        Vector2 newPosition = new Vector2(newX, panel.anchoredPosition.y);

        panel.anchoredPosition = newPosition;
    }
}
