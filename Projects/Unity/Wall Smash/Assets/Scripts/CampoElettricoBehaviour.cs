﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampoElettricoBehaviour : MonoBehaviour
{
    // Tesla coil variables and components
    public GameObject campoElettrico;
    public GameObject bobinaDiTesla1;
    public GameObject bobinaDiTesla2;
    public float cooldown;
    public GameObject boom;    
    public GameObject padre;    
    float timer;
    bool isActive = true;
	
	// Update is called once per frame 
	void Update ()
    {
        timer += 0.1f;

        // the electric field starts flickering any second
        if (campoElettrico != null)
        {
            if (timer >= cooldown)
            {
                if (isActive)
                {
                    campoElettrico.GetComponent<BoxCollider>().enabled = false;
                    campoElettrico.GetComponent<MeshRenderer>().enabled = false;
                    isActive = false;
                }
                else
                {
                    campoElettrico.GetComponent<BoxCollider>().enabled = true;
                    campoElettrico.GetComponent<MeshRenderer>().enabled = true;
                    isActive = true;
                }

                timer = 0;
            }

            // if one of the Tesla coils is destroyed the electric field is disabled
            if(bobinaDiTesla1 == null || bobinaDiTesla2 == null)
            {
                Destroy(campoElettrico);
            }            
        }

        // if one of the Tesla coils is destroyed, an explosion is generated 
        if (bobinaDiTesla1 != null && bobinaDiTesla1.GetComponent<DestructibleObstacle>().hp <= 0)
        {
            if (boom != null)
            {
                Instantiate(boom, bobinaDiTesla1.transform.position, Quaternion.identity);
            }

            Destroy(bobinaDiTesla1);
        }

        if (bobinaDiTesla2 != null && bobinaDiTesla2.GetComponent<DestructibleObstacle>().hp <= 0)
        {
            if (boom != null)
            {
                Instantiate(boom, bobinaDiTesla2.transform.position, Quaternion.identity);
            }

            Destroy(bobinaDiTesla2);
        }
    }
}
