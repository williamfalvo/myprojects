﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollManager : MonoBehaviour {

    public RectTransform panel;
    public RectTransform center;
    public GameObject[] bttn;
    public int distOffset;
    public bool toScale;

    public float[] distance;
    public float[] distReposition;
    private bool dragging = false;
    private float bttnDistance;
    private int minButtonNum;
    private int bttnLength;
    private float realOffset;


    // Use this for initialization
    void Awake() {
        bttnLength = bttn.Length;
        distance = new float[bttnLength];
        distReposition = new float[bttnLength];

        float portion = Mathf.Abs(bttn[1].GetComponent<RectTransform>().anchoredPosition.x - bttn[0].GetComponent<RectTransform>().anchoredPosition.x) / Screen.width;
        float scale = (float)Screen.width / 360;
        float defDistance = (float)Mathf.Abs(bttn[1].GetComponent<RectTransform>().anchoredPosition.x - bttn[0].GetComponent<RectTransform>().anchoredPosition.x);
        for (int u = 0; u < bttn.Length; u++) {
            bttn[u].SetActive(false);
        }
        bttnDistance = defDistance * scale;
        for (int i = 0; i < bttn.Length; i++) {
            
            Vector2 defaultAnchoredPosition = new Vector2(bttnDistance * i, bttn[i].GetComponent<RectTransform>().anchoredPosition.y);
            bttn[i].GetComponent<RectTransform>().anchoredPosition = defaultAnchoredPosition;
            //bttn[i].SetActive(true);
        }
        realOffset = (float)distOffset * scale;
    }

    private void OnEnable() {
        dragging = false;    
    }

    // Update is called once per frame
    void Update () {
        if (center != null && panel != null) {
            
            for (int i = 0; i < bttn.Length; i++) {

                //Vector2 defaultAnchoredPosition = new Vector2(bttnDistance * i, bttn[i].GetComponent<RectTransform>().anchoredPosition.y);
                //bttn[i].GetComponent<RectTransform>().anchoredPosition = defaultAnchoredPosition;

                distReposition[i] = center.GetComponent<RectTransform>().position.x - bttn[i].GetComponent<RectTransform>().position.x;
                distance[i] = Mathf.Abs(distReposition[i]);

                if (distReposition[i] > realOffset) {
                    float curX = bttn[i].GetComponent<RectTransform>().anchoredPosition.x;
                    float curY = bttn[i].GetComponent<RectTransform>().anchoredPosition.y;

                    Vector2 newAnchoredPosition = new Vector2(curX + (bttnLength * bttnDistance), curY);
                    bttn[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPosition;
                }

                if (distReposition[i] < -realOffset) {
                    float curX = bttn[i].GetComponent<RectTransform>().anchoredPosition.x;
                    float curY = bttn[i].GetComponent<RectTransform>().anchoredPosition.y;

                    Vector2 newAnchoredPosition = new Vector2(curX - (bttnLength * bttnDistance), curY);
                    bttn[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPosition;
                }
                
                if (toScale) ScaleByDistance(bttn[i].GetComponent<Button>(), distance[i]);
                bttn[i].SetActive(true);
                if (distance[i] <= (realOffset + 10)) bttn[i].SetActive(true);
                else bttn[i].SetActive(false);

            }

            float minDistance = Mathf.Min(distance);
            
            if (!dragging) {
                for (int a = 0; a < bttn.Length; a++) {
                    if (minDistance == distance[a]) minButtonNum = a;
                }
                LerpToBttn(-bttn[minButtonNum].GetComponent<RectTransform>().anchoredPosition.x); //LerpToBttn(minButtonNum * -bttnDistance);
            } 

            
            
        }
    }

    void LerpToBttn(float position) {
        float newX = Mathf.SmoothStep(panel.anchoredPosition.x, position, Time.unscaledDeltaTime * 10f);
        float oldX = panel.anchoredPosition.x;
        Vector2 newPosition = new Vector2(newX, panel.anchoredPosition.y);
        panel.anchoredPosition = newPosition;
    }
    
    public void StartDrag() {
        dragging = true;
    }

    public void EndDrag() {
        dragging = false;
    }

    void ScaleByDistance(Button button, float d) {
        var offset = 1 - (d / (float)(1.5 * realOffset));
        Color scaled = button.image.color;
        scaled.a = (float)(0.25 + (offset * 0.75));
        button.GetComponent<RectTransform>().localScale = new Vector3(offset, offset, offset);
        button.image.color = scaled;       
    }
}
