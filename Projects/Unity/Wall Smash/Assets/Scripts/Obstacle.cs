﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    // obstacles variables
    public bool isAnimation;
    Rigidbody rb;
    public float horizontalSpeed;
    public bool moveHorizontal;
    public float hSpeed;
    protected Transform camera;
    protected Transform skybox;

	// Use this for initialization
	protected void Start ()
    {
        skybox = GameObject.FindGameObjectWithTag("Skybox").GetComponent<Transform>();

        // the obstacles go past the skybox than their scripts are enabled
        if(transform.position.z + 10f > skybox.position.z)
        {
            switch(gameObject.tag)
            {
                case ("Obstacle"):
                    DestructibleObstacle dest_script = GetComponent<DestructibleObstacle>();
                    if (dest_script != null)
                    {
                        dest_script.enabled = true;
                    }
                    break;

                case ("GroundObstacle"):
                    GroundObstacle ground_script = GetComponent<GroundObstacle>();
                    if (ground_script != null)
                    {
                        ground_script.enabled = true;
                    }
                    break;

                case ("CollectibleObstacle"):
                    CollectibleObstacle collect_script = GetComponent<CollectibleObstacle>();
                    if (collect_script != null)
                    {
                        collect_script.enabled = true;
                    }
                    break;
            }
        }

        hSpeed = horizontalSpeed;
        rb = GetComponent<Rigidbody>();
        camera = GameObject.FindGameObjectWithTag("Camera").GetComponent<Transform>();
    }
	
	// Update is called once per frame
    // the obstacle moves horizzontally
	protected void FixedUpdate ()
    {
        if (moveHorizontal)
        {
            rb.transform.position += new Vector3(hSpeed * Time.fixedDeltaTime, 0, 0);
        }       
    }

    protected void Update()
    {
        // during the countdown the horizontal movement is stopped
        if (GameManager.instance.canCount)
        {
            moveHorizontal = false;
        }
        else
        {
            moveHorizontal = true;
        }

        // when the obstacle go past he camera view, it is destroyed and its collider disabled
        if(gameObject.tag != "Wall" && transform.position.z < camera.position.z)
        {
            if(rb.GetComponent<BoxCollider>() != null)
            {
                rb.GetComponent<BoxCollider>().enabled = false;
            }            
        }

        if (transform.position.z < camera.position.z - 15f)
        {
            Destroy(gameObject);
        }
    }

    // if the obstacle moves horizontally and collides with a side wall its movement is reversed
    protected void OnCollisionEnter(Collision collision)
    {
        if (moveHorizontal && collision.gameObject.tag == "Wall")
        {
            hSpeed *= -1;
        }
    }     
}
