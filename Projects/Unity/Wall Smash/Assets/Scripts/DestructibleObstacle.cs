﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DestructibleObstacle : Obstacle
{
    public Sprite[] sprite;
    public int hp;

    public ObstacleType obstacleType;
    public GameObject boom;
    public GameObject dust;

    public int obstacleDamage;
    public int score;    

    Rigidbody ball;
    protected bool isActive;

    // Loop sounds variables
    bool isPlaying = false;
    bool isPaused;
	
    // Use this for initialization
    // when the round starts all the obstacles are active 
	new void Start ()
    {
        base.Start();
        isActive = true;
    }
	
	// Update is called once per frame
    new void Update()
    {
        base.Update();

        // if the obstacle has not a skeleton animation, then creates an array of sprites which represent the different states of the obstacle itself 
        if(!isAnimation)
        {
            SpriteRenderer spr = gameObject.GetComponent<SpriteRenderer>();
            spr.sprite = sprite[hp];
        }

        // when the game stops the specified sounds are paused
        if (Time.timeScale == 0 && isPlaying)
        {
            switch (obstacleType)
            {
                case ObstacleType.BobinaDiTesla:
                    AudioManager.instance.Pause("Tesla");
                    break;

                case ObstacleType.RuotaMaya:
                    AudioManager.instance.Pause("Maya");
                    break;
            }

            isPaused = true;
        }

        // when the agme resumes the specified sounds play again
        if (Time.timeScale == 1 && isPaused)
        {
            switch (obstacleType)
            {
                case ObstacleType.BobinaDiTesla:
                    AudioManager.instance.Play("Tesla");
                    break;

                case ObstacleType.RuotaMaya:
                    AudioManager.instance.Play("Maya");
                    break;
            }

            isPaused = false;
        }
    }

	new void FixedUpdate ()
    {
        base.FixedUpdate();

        // when the obstacles have no life points they are destroyed
        // the destruction causes an explosion
        // for any type of obstacle destroyed the player's score increases by a different amount of points
        // if the player destroys more than 3 obstacles, the combo starts and he gains 100 points more
        if (hp <= 0 && isActive)
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            moveHorizontal = false;

            if (boom != null)
            {
                Instantiate(boom, gameObject.transform.position, Quaternion.identity);
            }

            switch (obstacleType)
            {
                case ObstacleType.MuroSemplice :
                    GameManager.instance.MuroSemplice += score;                    
                    GameManager.instance.simpleWallCounter++;
                    GameManager.instance.objectsHit++;
                    AudioManager.instance.Play("WallDestruction");
                    break;

                case ObstacleType.MuroResistente :
                    GameManager.instance.MuroResistente += score;
                    GameManager.instance.resistentWallCounter++;
                    GameManager.instance.objectsHit++;
                    AudioManager.instance.Play("WallDestruction");
                    break;

                case ObstacleType.Masso :
                    GameManager.instance.Masso += score;
                    GameManager.instance.boulderCounter++;
                    GameManager.instance.objectsHit++;
                    AudioManager.instance.Play("ObstacleDestruction");
                    break;

                case ObstacleType.PilaDiMassi :
                    GameManager.instance.PilaDiMassi += score;
                    GameManager.instance.stackOfBouldersCounter++;
                    GameManager.instance.objectsHit++;
                    AudioManager.instance.Play("ObstacleDestruction");
                    break;

                case ObstacleType.RuotaMaya :
                    GameManager.instance.RuotaDiMaya += score;
                    GameManager.instance.mayanWheelCounter++;
                    AudioManager.instance.Play("ObstacleDestruction");
                    break;                    
            }

            GameManager.instance.objectsHit++;

            if (GameManager.instance.objectsHit > 3)
            {
                AudioManager.instance.Play("Combo");
                GameManager.instance.ComboScore += 100;
                GameManager.instance.ComboPar += 100;
                Debug.Log("COMBO");
            }

            GameManager.instance.SetScore();

            isActive = false;
        }

        if (!isPlaying && gameObject != null && hp > 0 && gameObject.transform.position.z <= 30f + camera.position.z)
        {
            switch (obstacleType)
            {
                case ObstacleType.BobinaDiTesla:
                    AudioManager.instance.Play("Tesla");
                    break;

                case ObstacleType.RuotaMaya:
                    AudioManager.instance.Play("Maya");
                    break;
            }

            isPlaying = true;
        }
    }
    
    new private void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);
        
        //if the ball collides the obstacle loses lifepoints, changes sprite and creates a little cloud of dust
        if (collision.gameObject.tag == "Ball" && collision.gameObject.GetComponent<Rigidbody>().velocity != Vector3.zero)
        {
            Ball dmg = collision.gameObject.GetComponent<Ball>();
            hp -= dmg.damage;            

            if (hp > 0)
            {
                switch (obstacleType)
                {
                    case ObstacleType.MuroResistente:
                        AudioManager.instance.Play("HardWall");
                        break;

                    case ObstacleType.PilaDiMassi :
                        AudioManager.instance.Play("HardWall");
                        break;
                }

                if (dust != null)
                {
                    Instantiate(dust, gameObject.transform.position, Quaternion.identity);
                }
            }
        }              
    }

    // when the obstacle is destroyed the loop sounds stop playing 
    private void OnDestroy()
    {
        if (isPlaying)
        {
            switch (obstacleType)
            {
                case ObstacleType.BobinaDiTesla:
                    AudioManager.instance.Stop("Tesla");
                    break;

                case ObstacleType.RuotaMaya:
                    AudioManager.instance.Stop("Maya");
                    break;
            }
        }
    }

    public enum ObstacleType { MuroSemplice, MuroResistente, BobinaDiTesla, Masso, MassoEsplosivo, PilaDiMassi, RuotaMaya, CollectibleObstacle }
}
