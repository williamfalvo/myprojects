﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/*
 *                                  ATTENZIONE
 *  
    Alcuni script NON verranno effettivamente implementati nella Vertical Slice.
    Servono in caso ci siano più ambientazioni di modo da andare sempre nella scena giusta quindi, per ora, sono disabilitati.

    Metodi e righe in questione sono segnati con la tag #multisetting.
    I metodi e le righe utili solamente in caso di setting unico sono invece segnati con #monosetting.
    I monosettings dovranno essere disabilitati in caso di attivazione dei multisetting, pena conflitti.

    In caso vengano attivati è necessario seguire una determinata convenzione nel naming delle scene.
    Ogni nome deve rispettare la seguente sintassi:

                    AMBIENTAZIONE_ROUND_DIFFICOLTA'
    
    Ad esempio il secondo round di Vulcano a "Medio" sarà "Vulcano_Round2_MEDIUM" (difficoltà in CAPS LOCK)

    UPDATE: Scherzavo. Potendo scegliere la difficoltà dobbiamo usare i multisetting per forza.
*/




public class FastPlayMenu : MonoBehaviour {

    public GameObject panel;
    private int skin_id;

    public int Skin_id {
        get {
            return skin_id;
        }

        set {
            skin_id = value;
        }
    }

    private int diff_id;

    public int Diff_id {
        get {
            return diff_id;
        }

        set {
            diff_id = value;
        }
    }

    private int map_id;

    public int Map_id {
        get {
            return map_id;
        }

        set {
            map_id = value;
        }
    }

    private void Start() {
        skin_id = GameManager.instance.Skin_index;
    }

    
    private void UpdateSkin() {
        skin_id = GameManager.instance.Skin_index;
    }

    private void UpdateDiff() {
        diff_id = GameManager.instance.Diff_index;
    }

    private void UpdateMap() {
        map_id = GameManager.instance.Map_index;
    }


    public void PlayGame()
    {
        AudioManager.instance.Play("Confirm");

        GameManager.instance.Skin_index = skin_id;
        GameManager.instance.Diff_index = diff_id;
        GameManager.instance.Map_index = map_id;        

        var coins = GameManager.instance.GetFreeCoin();
        if (coins >= GameManager.gameFees[diff_id]) {
            GameManager.instance.SetFreeCoin(coins - GameManager.gameFees[diff_id]);
            MenuManager.GoToRoom(ParseDestination()); // #multisetting
            GameManager.instance.OnPopup = false;
        }
        
        //SceneManager.LoadScene("SampleScene");
    }

    public void GoBack() {
        panel.SetActive(false);
        AudioManager.instance.Play("Back");
        GameManager.instance.OnPopup = false;
        MenuManager.DestroyPrefab(GetComponentInParent<MainMenu>().quickstart, GetComponentInParent<MainMenu>().imgs);        
    }

    public void PrevMap() {
        if (map_id > 0) map_id--;
    }

    public void NextMap() {
        var l = GameManager.instance.GetComponent<GameManager>().maps.Capacity - 1;
        if (map_id < l) map_id++;
    }

    public void PrevDiff() {
        if (diff_id > 0) diff_id--;
    }

    public void NextDiff() {
        var l = GameManager.instance.GetComponent<GameManager>().difficulty.Capacity -1;
        if (diff_id < l) diff_id++;
    }



    // Produces the name of the scene based on setting and difficulty

    // #multisetting
    public string ParseDestination() {
        var s = GameManager.instance.maps[Map_id];
        var d = GameManager.instance.difficulty[Diff_id];
        return string.Concat(s, "_Round1_", d);
    }
}
