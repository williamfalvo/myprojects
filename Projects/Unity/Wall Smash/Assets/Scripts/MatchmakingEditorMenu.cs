﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchmakingEditorMenu : MonoBehaviour {

    private int diff_id;
    private int map_id;

    public int Diff_id {
        get {
            return diff_id;
        }

        set {
            diff_id = value;
        }
    }

    public int Map_id {
        get {
            return map_id;
        }

        set {
            map_id = value;
        }
    }

    private void Start() {
        diff_id = FindObjectOfType<GameManager>().GetComponent<GameManager>().Diff_index;
        map_id = FindObjectOfType<GameManager>().GetComponent<GameManager>().Map_index;
    }

    public void SaveConfig() {
        //AudioManager.instance.Play("Confirm");
        GameManager.instance.Diff_index = diff_id;
        GameManager.instance.Map_index = map_id;
        Debug.Log("Configurazione Salvata!");
        GameManager.instance.OnPopup = false;
        MenuManager.DestroyPrefab(GetComponentInParent<OptionsMainMenu>().mmedit, GetComponentInParent<OptionsMainMenu>().imgs);
        //MenuManager.DestroyPrefab(GetComponentInParent<MainMenu>().options, GetComponentInParent<MainMenu>().imgs);
    }

    public void GoBack() {
        MenuManager.DestroyPrefab(GetComponentInParent<OptionsMainMenu>().mmedit, GetComponentInParent<OptionsMainMenu>().imgs);
    }

    public void PrevDiff() {
        if (diff_id > 0) diff_id--;
    }

    public void NextDiff() {
        var l = GameManager.instance.GetComponent<GameManager>().difficulty.Capacity - 1;
        if (diff_id < l) diff_id++;
    }

    public void PrevMap() {
        if (map_id > 0) map_id--;
    }

    public void NextMap() {
        var l = GameManager.instance.GetComponent<GameManager>().maps.Capacity - 1;
        if (map_id < l) map_id++;
    }

    private void UpdateDiff() {
        diff_id = GameManager.instance.Diff_index;
    }

    private void UpdateMap() {
        map_id = GameManager.instance.Map_index;
    }

}
