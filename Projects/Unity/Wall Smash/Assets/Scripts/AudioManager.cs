﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    // Singleton pattern
    public static AudioManager instance = null;

    // Creates an array of sounds whose size is defined in the Inspector
    public Sounds[] sounds;

    Scene scene;
    bool playMenu = false;
    bool playGame = true;

    void Awake()
    {
        // I creates the singleton if there's no one, otherwise I replace it with the current one 
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        // Does not destroy the AudioManager changing Scene
        DontDestroyOnLoad(gameObject);
        
        foreach (Sounds s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;

            s.source.spatialBlend = s.spatialBlend;
            s.source.outputAudioMixerGroup = s.audioMixer;

            s.source.loop = s.loop;
        }
    }
    
    private void Update()
    {
        // Detects the Scene which is actually active
        scene = SceneManager.GetActiveScene();

        // If the MainMenu scene is active plays MenuTheme, otherwise plays MainTheme
        // for any occurrencies stops the previous one 
        if (!playMenu && playGame && scene.name == "MainMenu")
        {
            playGame = false;
            Stop("MainTheme");
            Play("MenuTheme");
            playMenu = true;
        }
        else if (playMenu && !playGame && scene.name != "MainMenu")
        {
            playMenu = false;
            Stop("MenuTheme");
            Play("MainTheme");
            playGame = true;
        }
    }

    // Plays the audioclip with the given name
    public void Play(string name)
    {
        // Searches for the sound's name in the array and if founded, plays that sound 
        Sounds s = Array.Find(sounds, sound => sound.name == name);

        if(s != null)
        {
            s.source.Play();
        }
    }

    // Pauses the audioclip with the given name
    public void Pause(string name)
    {
        // Searches for the sound's name in the array and if founded, pauses that sound
        Sounds s = Array.Find(sounds, sound => sound.name == name);

        if (s != null)
        {
            s.source.Pause();
        }
    }

    // Stops the audioclip with the given name
    public void Stop(string name)
    {
        // Searches for the sound's name in the array and if founded, pauses that sound
        Sounds s = Array.Find(sounds, sound => sound.name == name);

        if (s != null)
        {
            s.source.Stop();
        }
    }
}
