﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class OptionsMainMenu : MonoBehaviour {

    public GameObject mmedit;

    public List<UnityEngine.UI.Image> imgs;

    public AudioMixer audioMixer;
    public AudioMixer sfxMixer;

    public Slider backGroundSlider;
    public Slider sfxSlider;

    public void GoBack() {
        AudioManager.instance.Play("Back");
        MenuManager.DestroyPrefab(GetComponentInParent<MainMenu>().options, GetComponentInParent<MainMenu>().imgs);
    }

    public void EditMatchmaking() {
        MenuManager.SpawnPrefab(mmedit, imgs);
        GameManager.instance.OnPopup = true;
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", volume);
    }

    public void SfxVolume(float volume)
    {
        sfxMixer.SetFloat("volume", volume);
    }

    public void SetBackgroundMusic(bool active)
    {
        if (active)
        {
            audioMixer.SetFloat("volume", backGroundSlider.maxValue);
            backGroundSlider.value = backGroundSlider.maxValue;
        }
        else
        {
            audioMixer.SetFloat("volume", backGroundSlider.minValue);
            backGroundSlider.value = backGroundSlider.minValue;
        }
    }

    public void SetSoundEffects(bool active)
    {
        if (active)
        {
            sfxMixer.SetFloat("volume", sfxSlider.maxValue);
            sfxSlider.value = sfxSlider.maxValue;
        }
        else
        {
            sfxMixer.SetFloat("volume", sfxSlider.minValue);
            sfxSlider.value = sfxSlider.minValue;
        }
    }

}
