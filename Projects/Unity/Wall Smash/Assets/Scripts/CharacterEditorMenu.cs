﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterEditorMenu : MonoBehaviour {

    private int id;

    public int Id {
        get {
            return id;
        }

        set {
            id = value;
        }
    }

    private void Start() {
        id = FindObjectOfType<GameManager>().GetComponent<GameManager>().Skin_index;
    }

    private void UpdateSkin() {
        id = GameManager.instance.Skin_index;
    }

    public void SaveConfig() {
        AudioManager.instance.Play("Confirm");
        GameManager.instance.Skin_index = id;
        if (GameManager.instance.Skin_index == id) Debug.Log("Configurazione Salvata!");
        //GoBack();
    }

    public void GoBack() {
        //MenuManager.GoToRoom("MainMenu");
        MenuManager.DestroyPrefab(GetComponentInParent<MainMenu>().pgedit, GetComponentInParent<MainMenu>().imgs);
    }

    public void PrevSkin() {
        if (id > 0) id--;
    }

    public void NextSkin() {
        var l = GameManager.instance.GetComponent<GameManager>().skins.Capacity - 1;
        if (id < l) id++;
    }
}
