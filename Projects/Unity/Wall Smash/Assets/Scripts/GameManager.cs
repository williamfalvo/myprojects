﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



/*
 *                                  ATTENZIONE
 *  
    Alcuni script NON verranno effettivamente implementati nella Vertical Slice.
    Servono in caso ci siano più ambientazioni di modo da andare sempre nella scena giusta quindi, per ora, sono disabilitati.

    Metodi e righe in questione sono segnati con la tag #multisetting.
    I metodi e le righe utili solamente in caso di setting unico sono invece segnati con #monosetting.
    I monosettings dovranno essere disabilitati in caso di attivazione dei multisetting, pena conflitti.

    In caso vengano attivati è necessario seguire una determinata convenzione nel naming delle scene.
    Ogni nome deve rispettare la seguente sintassi:

                    AMBIENTAZIONE_ROUND_DIFFICOLTA'
    
    Ad esempio il secondo round di Vulcano a "Medio" sarà "Vulcano_Round2_MEDIUM" (difficoltà in CAPS LOCK)

    UPDATE: Scherzavo. Potendo scegliere la difficoltà dobbiamo usare i multisetting per forza.
*/





public struct Wallet
{
    private int free;
    private int premium;   

    public Wallet(int f, int p) {
        free = f;
        premium = p;
    }

    public int Free {
        get { return free; }
        set { free = value; }
    }

    public int Premium {
        get { return premium; }
        set { premium = value; }
    }
}

public class GameManager : MonoBehaviour
{
    //Singleton instance
    public static GameManager instance;

    //Check who is playing
    public bool isPlayer1;

    //CountDown Data
    public float mainTimer = 4f;
    public float timer;
    public bool canCount = true;
    GameObject countDown;
    public Text countDownText;

    //Movement Data
    public float vSpeed;
    public float vspeed;

    Rigidbody ball;
    Vector3 oldVelocity;

    GroundObstacle sfx;

    //Activators Data
    private bool[] activatorPlayer1;
    private bool[] activatorPlayer2;
    private bool[] round1Activator1;
    private bool[] round1Activator2;
    private bool[] round2Activator1;
    private bool[] round2Activator2;

    //Current level
    private int level = 1;

    //Scenes Data
    public List<string> skins;
    private int skin_index;
    public List<string> difficulty;
    private int diff_index;
    public List<string> maps;
    private int map_index;
    public string[] scenes;
    private GameObject menu_spawner;
    private bool onPopup = false;

    //Score Data
    public int finalScorePlayer1;
    public int finalScorePlayer2;
    int roundScore; 
    int finalLife;
    int muroSempliceDistrutto;
    int muroResistenteDistrutto;
    int bobinaDiTeslaDistrutta;
    int massoDistrutto;
    int massoEsplosivoDistrutto;
    int pilaDiMassiDistrutta;
    int ruotaDiMayaDistrutta;
    int obstacleCollected;
    int faceSmashed;
    int comboScore;
    int comboPar;
    int activatorHit;

    //Coins & Diamonds Data
    private Wallet wallet;
    public int free;
    public int premium;
    public static readonly int[] gameFees = { 25, 50, 100 };

    //Combo counter
    public int objectsHit = 0;

    //Round Score Panel Data
    public Text scoreText;
    public RoundScorePanel roundScorePanel;
    GameObject scorePanel;

    //Game Score Panel Data
    GameObject gameScorePanel;
    GameScorePanel gameScore;
    public Sprite[] chest; 

    //Score Round Counter
    [Header("Parametri per lo score finale")]
    public int lifeScorePar;
    public int simpleWallCounter;
    public int resistentWallCounter;
    public int teslaCoilCounter;
    public int boulderCounter;
    public int explosiveBoulderCounter;
    public int stackOfBouldersCounter;
    public int mayanWheelCounter;
    public int obstacleCollectedCounter;
    public int faceSmashedCounter;

    [SerializeField]
    private DestructibleObstacle simpleWall;
    [SerializeField]
    private DestructibleObstacle resistentWall;
    [SerializeField]
    private DestructibleObstacle teslaCoil;
    [SerializeField]
    private DestructibleObstacle boulder;
    [SerializeField]
    private DestructibleObstacle explosiveBoulder;
    [SerializeField]
    private DestructibleObstacle stackOfBoulders;
    [SerializeField]
    private DestructibleObstacle mayanWheel;


    //ScrollMenu Data
    [Header("Parametri per ScrollMenu")]
    public bool IsScrollActive;
    public RectTransform panel;
    public RectTransform center;
    public Button[] bttn;

    private float[] distance;
    public float[] distReposition;
    private bool dragging = false;
    private int bttnDistance;
    private int minButtonNum;
    private int bttnLength;
    private readonly int distOffset = 800;



    private void Awake() {
        //Singleton Check
        if (instance == null) {
            instance = this;
            instance.skin_index = 0;
            DontDestroyOnLoad(this);
            instance.wallet = InitWallet(free, premium);
        }
        else if (instance != null) {
            instance.gameScorePanel = GameObject.FindGameObjectWithTag("GameScore");
            instance.countDown = GameObject.FindGameObjectWithTag("CountDown");
            instance.scorePanel = GameObject.FindGameObjectWithTag("RoundScore");
            GameObject score = GameObject.FindGameObjectWithTag("Score");

            if (score != null) {
                instance.scoreText = score.GetComponent<Text>();
                Debug.Log(instance.scoreText);
            }

            if (instance.scorePanel != null) {
                instance.roundScorePanel = instance.scorePanel.GetComponent<RoundScorePanel>();
                instance.scorePanel.SetActive(false);
            }
            if (instance.countDown != null) {
                instance.countDownText = instance.countDown.GetComponent<Text>();
                instance.countDown.SetActive(false);
            }
            if (instance.gameScorePanel != null) {
                instance.gameScore = instance.gameScorePanel.GetComponent<GameScorePanel>();
                instance.gameScorePanel.SetActive(false);
            }

            Destroy(gameObject);
        }

        if (IsScrollActive) InitScroll();
        
    }


    //Inizializzazione ScrollMenu
    public void InitScroll() {

        bttnLength = bttn.Length;
        distance = new float[bttnLength];
        distReposition = new float[bttnLength];

        bttnDistance = (int)Mathf.Abs(bttn[1].GetComponent<RectTransform>().anchoredPosition.x - bttn[0].GetComponent<RectTransform>().anchoredPosition.x);
    }

    public static Wallet InitWallet(int f, int p) {
        return new Wallet(f, p);
    }

    public int GetFreeCoin() {
        return wallet.Free;
    }

    public int GetPremiumCoin() {
        return wallet.Premium;
    }

    public void SetFreeCoin(int f) {
        wallet.Free = f;
    }

    public void SetPremiumCoin(int p) {
        wallet.Premium = p;
    }

    //Modify in real time the score on UI
    public void SetScore() {
        int scoreSemplice = muroSempliceDistrutto;
        int scoreResistente = muroResistenteDistrutto;
        int scoreTesla = bobinaDiTeslaDistrutta;
        int scoreMasso = massoDistrutto;
        int scoreEsplosivo = massoEsplosivoDistrutto;
        int scorePila = pilaDiMassiDistrutta;
        int scoreMaya = ruotaDiMayaDistrutta;
        int scoreCollect = obstacleCollected;
        int scoreSmash = faceSmashed;
        int scoreCombo = comboScore;
        roundScore = scoreSemplice + scoreResistente + scoreTesla + scoreMasso + scoreEsplosivo + scorePila + scoreMaya + scoreCollect + scoreSmash + scoreCombo;
        instance.scoreText.text = roundScore.ToString();
    }

    //////////////////////////Getter & Setter//////////////////////////
    public string GetSkinName(int id) {
        return skins[id];
    }

    public int GetSkinID(string name) {
        return skins.IndexOf(name);
    }

    public int ActivatorHit
    {
        get { return activatorHit; }
        set { activatorHit = value; }
    }

    public int ComboScore
    {
        get { return comboScore; }
        set { comboScore = value; }
    }

    public int ComboPar
    {
        get { return comboPar; }
        set { comboPar = value; }
    }

    public int MuroSemplice {
        get { return muroSempliceDistrutto; }
        set { muroSempliceDistrutto = value; }
    }

    public int MuroResistente {
        get { return muroResistenteDistrutto; }
        set { muroResistenteDistrutto = value; }
    }

    public int BobinaDiTesla {
        get { return bobinaDiTeslaDistrutta; }
        set { bobinaDiTeslaDistrutta = value; }
    }

    public int Masso {
        get { return massoDistrutto; }
        set { massoDistrutto = value; }
    }

    public int MassoEsplosivo {
        get { return massoEsplosivoDistrutto; }
        set { massoEsplosivoDistrutto = value; }
    }

    public int PilaDiMassi {
        get { return pilaDiMassiDistrutta; }
        set { pilaDiMassiDistrutta = value; }
    }

    public int RuotaDiMaya {
        get { return ruotaDiMayaDistrutta; }
        set { ruotaDiMayaDistrutta = value; }
    }

    public int CollectObstacle {
        get { return obstacleCollected; }
        set { obstacleCollected = value; }
    }

    public int FaceSmash {
        get { return faceSmashed; }
        set { faceSmashed = value; }
    }

    public int Skin_index {
        get {
            return skin_index;
        }

        set {
            skin_index = value;
        }
    }

    public int Diff_index {
        get {
            return diff_index;
        }

        set {
            diff_index = value;
        }
    }

    public int Map_index {
        get {
            return map_index;
        }

        set {
            map_index = value;
        }
    }

    public int Level {
        get {
            return level;
        }

        set {
            level = value;
        }
    }

    public GameObject Menu_spawner {
        get {
            return menu_spawner;
        }

        set {
            menu_spawner = value;
        }
    }

    public bool OnPopup {
        get {
            return onPopup;
        }

        set {
            onPopup = value;
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Register whick activator is activated
    public void ActivateObstacle(int x) {
        if (isPlayer1) {
            activatorPlayer1[x] = true;
        }
        else {
            activatorPlayer2[x] = true;
        }
    }

    //During the start round, if level is 2 or 3, set what special obstacles is active, and if level is 1 or 2 create a new boolean array to register data about this round  
    public void SetupCollectiblesForActivePlayer(GameObject[] collectedObstacles) {
        if (level > 1) {
            bool[] activator;

            //Se è in gioco il player 1 vedo quali oggetti ha preso il player 2 nel precedente round
            if (isPlayer1) {
                Debug.Log("PLAYER1");
                if (level == 2) {
                    activator = round1Activator2;
                }
                else {
                    activator = round2Activator2;
                }
            }
            else {
                Debug.Log("PLAYER2");
                if (level == 2) {
                    activator = round1Activator1;
                }
                else {
                    activator = round2Activator1;
                }
            }
            //Take all of special obstacles and set them active or not in base of the GameManager's data
            for (int i = 0; i < collectedObstacles.Length; ++i) {
                if(!activator[i])
                {
                    if(!collectedObstacles[i].GetComponent<Obstacle>().isAnimation)
                    {
                        collectedObstacles[i].GetComponent<SpriteRenderer>().material.color = new Color(1.0f, 1.0f, 1.0f, 0.3f);
                    }
                    else
                    {
                        collectedObstacles[i].GetComponent<Obstacle>().moveHorizontal = false;
                    }
                    
                    collectedObstacles[i].GetComponent<BoxCollider>().enabled = false;
                }
            }
        }

        if (level < 3) {
            if (isPlayer1) {
                Debug.Log("PLAYER1");
                activatorPlayer1 = new bool[collectedObstacles.Length];
            }
            else {
                Debug.Log("PLAYER2");
                activatorPlayer2 = new bool[collectedObstacles.Length];
            }
        }

    }

    //Set all data of Round Score Panel and add the round score to final score
    public void EndRound()
    {
        Time.timeScale = 0;        

        finalLife = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().hp;

        roundScorePanel.roundNumber.text = level.ToString();

        if (finalLife <= 0) {
            roundScorePanel.finalRoundCondition.text = "No More Lives";
            if(finalLife < 0)
            {
                finalLife = 0;
            }
        }
        else {
            roundScorePanel.finalRoundCondition.text = "Well Done!";
        }

        roundScore += (finalLife * lifeScorePar);
        roundScorePanel.life.text = finalLife.ToString();
        roundScorePanel.lifeScorePar.text = lifeScorePar.ToString();
        roundScorePanel.lifeScoreTot.GetComponent<AnimationScore>().finalScore = finalLife * lifeScorePar;
        roundScorePanel.lifeParent.SetActive(false);

        int obstacle = (simpleWallCounter + resistentWallCounter + teslaCoilCounter + boulderCounter + explosiveBoulderCounter + stackOfBouldersCounter + mayanWheelCounter);
        int simpleWallScore = simpleWall.score * simpleWallCounter;
        int resistentWallScore = resistentWall.score * resistentWallCounter;
        int teslaCoilScore = teslaCoil.score * teslaCoilCounter;
        int boulderScore = boulder.score * boulderCounter;
        int explosiveBoulderScore = explosiveBoulder.score * explosiveBoulderCounter;
        int stackOfBouldersScore = stackOfBoulders.score * stackOfBouldersCounter;
        int mayanWheelScore = mayanWheel.score * mayanWheelCounter;

        roundScorePanel.obstacle.text = obstacle.ToString();
        roundScorePanel.obstacleTot.GetComponent<AnimationScore>().finalScore = (simpleWallScore + resistentWallScore + teslaCoilScore + boulderScore + explosiveBoulderScore + stackOfBouldersScore + mayanWheelScore);
        roundScorePanel.obstacleParent.SetActive(false);
        
        roundScorePanel.activatorHit.GetComponent<AnimationScore>().finalScore = activatorHit;
        roundScorePanel.activatorRound.text = activatorPlayer1.Length.ToString();
        roundScorePanel.activatorHitParent.SetActive(false);

        roundScorePanel.activatorCollected.text = obstacleCollectedCounter.ToString();        
        roundScorePanel.activatorCollectedTot.GetComponent<AnimationScore>().finalScore = (obstacleCollected * obstacleCollectedCounter);
        roundScorePanel.activatorCollectedParent.SetActive(false);
        
        roundScorePanel.maxComboTot.GetComponent<AnimationScore>().finalScore = comboScore;
        roundScorePanel.comboParent.SetActive(false);
        
        roundScorePanel.finalScore.GetComponent<AnimationScore>().finalScore = roundScore;
        roundScorePanel.finalScoreParent.SetActive(false);

        scorePanel.SetActive(true);

        AudioManager.instance.Play("EndRound");

        StartCoroutine(ScoreActivator());

        if (isPlayer1) {
            finalScorePlayer1 += roundScore;
            ResetScore();
        }
        else {
            finalScorePlayer2 += roundScore;
            ResetScore();
        }
    }

    //If level isn't 3, change round and, if player 2 is playing, increase the level, else set enable the final score panel
    public void ChangeRound() {

        AudioManager.instance.Play("Confirm");

        if (GameManager.instance.level < 3) {
            if (!GameManager.instance.isPlayer1) {
                if (GameManager.instance.level == 1) {
                    GameManager.instance.round1Activator2 = GameManager.instance.activatorPlayer2;
                }
                else {
                    GameManager.instance.round2Activator2 = GameManager.instance.activatorPlayer2;
                }
                ++GameManager.instance.level;
            }
            else {
                if (GameManager.instance.level == 1) {
                    GameManager.instance.round1Activator1 = GameManager.instance.activatorPlayer1;
                }
                else {
                    GameManager.instance.round2Activator1 = GameManager.instance.activatorPlayer1;
                }
            }

            GameManager.instance.isPlayer1 = !GameManager.instance.isPlayer1;

            MenuManager.GoToRoom(ParseDestination()); // #multisetting
        }
        else {
            if (GameManager.instance.isPlayer1)
            {
               GameManager.instance.isPlayer1 = !GameManager.instance.isPlayer1;

               MenuManager.GoToRoom(ParseDestination()); // #multisetting
            }
            else {
                GameManager.instance.scorePanel.SetActive(false);

                if (GameManager.instance.finalScorePlayer1 > GameManager.instance.finalScorePlayer2)
                {
                    GameManager.instance.gameScore.player2Glow.SetActive(false);
                    GameManager.instance.gameScore.winnerText.text = "PLAYER 1 WINS!";
                }
                else if (GameManager.instance.finalScorePlayer1 < GameManager.instance.finalScorePlayer2)
                {
                    GameManager.instance.gameScore.player1Glow.SetActive(false);
                    GameManager.instance.gameScore.winnerText.text = "PLAYER 2 WINS!";
                }
                else
                {
                    GameManager.instance.gameScore.player1Glow.SetActive(false);
                    GameManager.instance.gameScore.player2Glow.SetActive(false);
                    GameManager.instance.gameScore.winnerText.text = "DRAW!";
                }

                switch (GameManager.instance.difficulty[GameManager.instance.Diff_index])
                {
                    case ("BEGINNER"):
                        GameManager.instance.gameScore.chest.sprite = chest[0];
                        break;

                    case ("REGULAR"):
                        GameManager.instance.gameScore.chest.sprite = chest[1];
                        break;

                    case ("VETERAN"):
                        GameManager.instance.gameScore.chest.sprite = chest[2];
                        break;
                }
                        GameManager.instance.gameScore.score1.text = GameManager.instance.finalScorePlayer1.ToString();
                GameManager.instance.gameScore.score2.text = GameManager.instance.finalScorePlayer2.ToString();

                
                GameManager.instance.gameScorePanel.SetActive(true);

                AudioManager.instance.Play("GameOver");
            }

        }
    }

    //Return to MainMenu and destroy the actual singleton
    public void LeaveGame()
    {    
        AudioManager.instance.Play("Back");
        MenuManager.GoToRoom("MainMenu");
        GameManager.instance.onPopup = false;
        Destroy(GameManager.instance.gameObject);
    }

    private void Update()
    {       
        // Update dello ScrollMenu
        if (center != null && panel != null) {
            for (int i = 0; i < bttn.Length; i++) {
                distReposition[i] = center.GetComponent<RectTransform>().position.x - bttn[i].GetComponent<RectTransform>().position.x;
                distance[i] = Mathf.Abs(distReposition[i]);

                if (distReposition[i] > distOffset) {
                    float curX = bttn[i].GetComponent<RectTransform>().anchoredPosition.x;
                    float curY = bttn[i].GetComponent<RectTransform>().anchoredPosition.y;

                    Vector2 newAnchoredPosition = new Vector2(curX + (bttnLength * bttnDistance), curY);
                    bttn[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPosition;
                }

                if (distReposition[i] < -distOffset) {
                    float curX = bttn[i].GetComponent<RectTransform>().anchoredPosition.x;
                    float curY = bttn[i].GetComponent<RectTransform>().anchoredPosition.y;

                    Vector2 newAnchoredPosition = new Vector2(curX - (bttnLength * bttnDistance), curY);
                    bttn[i].GetComponent<RectTransform>().anchoredPosition = newAnchoredPosition;
                }
            }

            float minDistance = Mathf.Min(distance);
            for (int a = 0; a < bttn.Length; a++) {
                if (minDistance == distance[a]) minButtonNum = a;
            }

            if (!dragging) LerpToBttn(-bttn[minButtonNum].GetComponent<RectTransform>().anchoredPosition.x);
        }        
    }

    void LerpToBttn(float position) {
        float newX = Mathf.Lerp(panel.anchoredPosition.x, position, Time.deltaTime * 6f);
        Vector2 newPosition = new Vector2(newX, panel.anchoredPosition.y);

        panel.anchoredPosition = newPosition;
    }

    public void StartDrag() {
        dragging = true;
    }

    public void EndDrag() {
        dragging = false;
    }

    public IEnumerator CountDown() {
        float _vSpeed = vSpeed;
        instance.vSpeed = 0;
        instance.countDown.SetActive(true);
        countDownText.text = "3";
        yield return new WaitForSeconds(1);
        instance.countDownText.text = "2";
        yield return new WaitForSeconds(1);
        instance.countDownText.text = "1";
        yield return new WaitForSeconds(1);
        instance.countDownText.text = "SMASH!";
        yield return new WaitForSeconds(1);
        instance.countDown.SetActive(false);
        instance.vSpeed = _vSpeed;
    }

    //CountDown Manager
    public void Countdown()
    {
        if (timer > 0f && canCount)
        {
            instance.countDown.SetActive(true);            
            instance.vSpeed = 0;
            timer -= Time.deltaTime;

            if (timer >= 3f) {
                instance.countDownText.text = "3";
            }
            else if (timer >= 2) {
                instance.countDownText.text = "2";
            }
            else if (timer >= 1) {
                instance.countDownText.text = "1";
            }
            else if (timer > 0) {
                instance.countDownText.text = "SMASH!";
            }
        }
        else if (timer <= 0f && canCount)
        {
            instance.countDown.SetActive(false);
            instance.vSpeed = vspeed;
            if (ball != null && ball.IsSleeping())
            {
                instance.BallWakeUp();
            }
            canCount = false;
            timer = 0f;
        }
    }

    //Enable the Countdown to do their function
    public void ResetCountdown() {
        timer = mainTimer;
        canCount = true;
    }

    //In the end of round reset the partial score & counter for the next round
    public void ResetScore() {
        GameManager.instance.simpleWallCounter = 0;
        GameManager.instance.resistentWallCounter = 0;
        GameManager.instance.teslaCoilCounter = 0;
        GameManager.instance.boulderCounter = 0;
        GameManager.instance.explosiveBoulderCounter = 0;
        GameManager.instance.stackOfBouldersCounter = 0;
        GameManager.instance.mayanWheelCounter = 0;
        GameManager.instance.obstacleCollectedCounter = 0;
        GameManager.instance.faceSmashedCounter = 0;
        GameManager.instance.muroSempliceDistrutto = 0;
        GameManager.instance.muroResistenteDistrutto = 0;
        GameManager.instance.bobinaDiTeslaDistrutta = 0;
        GameManager.instance.massoDistrutto = 0;
        GameManager.instance.massoEsplosivoDistrutto = 0;
        GameManager.instance.pilaDiMassiDistrutta = 0;
        GameManager.instance.ruotaDiMayaDistrutta = 0;
        GameManager.instance.obstacleCollected = 0;
        GameManager.instance.faceSmashed = 0;
        GameManager.instance.activatorHit = 0;
        GameManager.instance.obstacleCollectedCounter = 0;
        GameManager.instance.comboScore = 0;
        GameManager.instance.roundScore = 0;
        GameManager.instance.comboPar = 0;
    }

    //Block the Physics of the ball
    public void BallSleep()
    {
        ball = GameObject.FindGameObjectWithTag("Ball").GetComponent<Rigidbody>();

        if (ball != null)
        {
            if (ball.velocity != Vector3.zero)
            {
                oldVelocity = ball.velocity;
            }

            ball.Sleep();
        }
    }

    //Enable the Physics of the ball
    public void BallWakeUp()
    {
        if (ball != null)
        {
            if (oldVelocity != Vector3.zero)
            {
                ball.velocity = oldVelocity;
            }
            ball.WakeUp();            
        }
    }
    
    //Animation to set score visible in base of the time
    public IEnumerator ScoreActivator()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        roundScorePanel.lifeParent.SetActive(true);

        yield return new WaitForSecondsRealtime(0.5f);
        roundScorePanel.obstacleParent.SetActive(true);

        if(GameManager.instance.level < 3)
        {
            yield return new WaitForSecondsRealtime(0.5f);
            roundScorePanel.activatorHitParent.SetActive(true);

            yield return new WaitForSecondsRealtime(0.5f);
            roundScorePanel.activatorCollectedParent.SetActive(true);
        }

        yield return new WaitForSecondsRealtime(0.5f);
        roundScorePanel.comboParent.SetActive(true);

        yield return new WaitForSecondsRealtime(0.5f);
        roundScorePanel.finalScoreParent.SetActive(true);
    }

    
    // #multisetting
    //Use the string of the stage setting to go into next scene
    public string ParseDestination() {
        var s = GameManager.instance.maps[GameManager.instance.Map_index];
        var r = GameManager.instance.scenes[GameManager.instance.level - 1];
        var d = GameManager.instance.difficulty[GameManager.instance.Diff_index];
        return string.Concat(s, "_", r, "_",  d);
    }
}
