﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    public Image tutorialScreen;
    public Sprite[] img;
    int index;

    public Text chapter;


    // The tutorial starts only if we are on the first level and is playing player one 
    void Start ()
    {
        if(!GameManager.instance.isPlayer1)
        {
            gameObject.SetActive(false);
        }

        chapter.text = "Tutorial: Swipe";
        index = 0;
        tutorialScreen.sprite = img[index];        
	}

    //Based on the tutorial page, let's change the chapter
    void Update ()
    {
		if(index < 2)
        {
            chapter.text = "Tutorial: Swipe";
        }
        else if(index >= 2 && index < 4)
        {
            chapter.text = "Tutorial: Combo";
        }
        else if(index >= 4 && index < img.Length)
        {
            chapter.text = "Tutorial: Activators";
        }
        else
        {
            gameObject.SetActive(false);
        }
	}

    //If we are not at the end of the tutorial, change page
    public void Change()
    {
        AudioManager.instance.Play("Back");

        index++;

        if(index < img.Length)
        {
            tutorialScreen.sprite = img[index];
        }
        
    }

    //Skip the tutorial
    public void Skip()
    {
        AudioManager.instance.Play("Confirm");
        gameObject.SetActive(false);
    }
}
