﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationBotton : MonoBehaviour
{
    bool zoomIn;
    float cooldown;
    float timer;
    public float zoom;

	
	void Start ()
    {
        zoomIn = true;
        cooldown = 0.25f;
	}
	
	//Zoom in and out animation
	void Update ()
    {

        //Every quarter of a second changes from zoom in to zoom out
        timer += zoom;

        if(timer >= cooldown)
        {
            zoomIn = !zoomIn;
            timer = 0;
        }

        if(zoomIn)
        {
            transform.localScale = new Vector3(Mathf.Clamp(transform.localScale.x + zoom, 0, 100)  , Mathf.Clamp(transform.localScale.y + zoom, 0, 100), transform.localScale.z);
        }
        else
        {
            transform.localScale = new Vector3(transform.localScale.x - zoom, transform.localScale.y - zoom, transform.localScale.z);
        }
	}
}
