﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScorePanel : MonoBehaviour
{
    //Final Score Panel reference
    public Text winnerText;
    public Text score1;
    public Text score2;
    public Image chest;
    public GameObject player1Glow;
    public GameObject player2Glow;
}
