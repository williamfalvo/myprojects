﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoundScorePanel : MonoBehaviour
{
    //Round Score Panel reference
    public Text roundNumber;
    public Text finalRoundCondition;
    public GameObject lifeParent;
    public Text life;
    public Text lifeScorePar;
    public Text lifeScoreTot;
    public GameObject obstacleParent;
    public Text obstacle;
    public Text obstacleTot;
    public GameObject activatorHitParent;
    public Text activatorHit;
    public Text activatorRound;
    public GameObject activatorCollectedParent;
    public Text activatorCollected;
    public Text activatorCollectedTot;
    public Text maxCombo;
    public Text maxComboPar;
    public GameObject comboParent;
    public Text maxComboTot;
    public GameObject finalScoreParent;
    public Text finalScore;
}
