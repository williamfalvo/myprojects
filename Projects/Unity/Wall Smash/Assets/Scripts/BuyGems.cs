﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyGems : MonoBehaviour {

    public int gain;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Add the variable "gain" to the number of diamonds owned
    public void Buy() {
        GameManager.instance.SetPremiumCoin(GameManager.instance.GetPremiumCoin() + gain);
    }
}
