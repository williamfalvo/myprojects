﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExternalMovement : MonoBehaviour
{
    Transform tr;
        
    void Start()
    {
        tr = GetComponent<Transform>();
    }

    //Move the external object at the same speed of the player
    void FixedUpdate()
    {
        tr.position -= new Vector3(0, 0, GameManager.instance.vSpeed * Time.fixedDeltaTime);
    }
}
