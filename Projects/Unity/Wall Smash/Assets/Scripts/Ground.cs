﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    // Grounf variables
    Transform tr;

	// Use this for initialization
	void Start ()
    {
        tr = GetComponent<Transform>();        
	}
	
	// Update is called once per frame
    // if the ground go past the camera, it is destroyed
	void Update ()
    {
        if (tr.position.z < GameObject.FindGameObjectWithTag("Camera").GetComponent<Transform>().position.z - 30f)
        {
            Destroy(gameObject);
        }
    }
}
