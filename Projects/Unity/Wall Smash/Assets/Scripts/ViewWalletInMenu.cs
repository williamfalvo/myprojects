﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewWalletInMenu : MonoBehaviour {

    private int free;
    private int premium;
    private Text freetext;
    private Text premiumtext;

	// Use this for initialization
	void Start () {
        free = GameManager.instance.GetFreeCoin();
        premium = GameManager.instance.GetPremiumCoin();
        freetext = GameObject.Find("FreeCoins").GetComponent<Text>();
        premiumtext = GameObject.Find("PremiumCoins").GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        free = GameManager.instance.GetFreeCoin();
        premium = GameManager.instance.GetPremiumCoin();
        freetext.text = " " + free;
        premiumtext.text = " " + premium;
	}
}
