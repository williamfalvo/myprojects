﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

//I create a custom class to store sounds informations and parameters

[System.Serializable]
public class Sounds
{
    //Sound's name
    public string name;

    //Sound's audioclip
    public AudioClip clip;

    //Volume
    [Range(0f, 1f)]
    public float volume;

    //Pitch
    [Range(0.1f, 3f)]
    public float pitch;

    //Loop
    public bool loop;

    //AudioMixer
    public AudioMixerGroup audioMixer;

    //Sets 2D or 3D audio
    [Range(0f, 1f)]
    public float spatialBlend;
    
    [HideInInspector]
    public AudioSource source;
}
