﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Combo : MonoBehaviour
{
    Text comboText;
    public GameObject comboSprite;

    //During start round set the text to empty string
    void Awake()
    {
        comboText = gameObject.GetComponent<Text>();
        comboText.text = "";    
    }

    void Update ()
    {
        //If there is not a current combo set the text with an empty string, otherwise update the text with the combo in progress
        if (GameManager.instance.ComboPar == 0)
        {
            comboText.text = "";
            comboSprite.SetActive(false);
        }
        else
        {
            comboText.text = string.Concat("+", GameManager.instance.ComboPar.ToString());
            comboSprite.SetActive(true);
        }
	}
}
