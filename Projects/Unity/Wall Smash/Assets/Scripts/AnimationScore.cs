﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationScore : MonoBehaviour
{
    int score;
    public int finalScore;
    public bool addScore;
    Text scoreText;

	//Set score to 0 to start the animation of adding score
	void Start ()
    {
        score = 0;
        addScore = true;
        scoreText = gameObject.GetComponent<Text>();
	}
	
	//Adding score animation
	void Update ()
    {
        //As long as score is less than final score, increase score by 400 and update the text
        if (addScore)
        {
            if(score < finalScore)
            {
                score += 400;
            }
            else
            {
                score = finalScore;
                addScore = false;
            }
            scoreText.text = score.ToString();
        }
	}
}
