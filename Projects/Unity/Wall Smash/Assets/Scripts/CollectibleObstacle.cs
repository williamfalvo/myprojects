﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleObstacle : DestructibleObstacle
{
    [Header("Il contatore parte da 0")]
    public int indexOfCollectibles;

    //Fade animation data
    public Material dissolveMaterial;
    Renderer rend; 
    public float speed;
    public float min;
    float currentY;

    bool collected = false;
    bool activated = false; 

    new void Start()
    {
        base.Start();

        rend = GetComponent<Renderer>();
        dissolveMaterial = rend.material;
    }

    new private void FixedUpdate()
    {
        base.FixedUpdate();

        //If enabled, register the boolean to true in the GameManager and start the fade animation
        if (hp <= 0)
        {
            if(!activated)
            {
                GameManager.instance.ActivateObstacle(indexOfCollectibles);
                activated = true;
                GameManager.instance.ActivatorHit++;
                Debug.Log("Inviato a Babbo Natale");
            }
            Dissolve();            
        }

        //If collected, start the fade animation
        if (collected)
        {
            Dissolve();
        }
    }

    new private void OnCollisionEnter(Collision collision)
    {
        //If it collides with the player it sets up as collected and adds the score
        if (collision.gameObject.tag == "Player")
        {            
            AudioManager.instance.Play("Collectible");            
            GameManager.instance.CollectObstacle += score;
            GameManager.instance.obstacleCollectedCounter++;
            GameManager.instance.SetScore();
            collected = true;
        }//If it collides with the moving ball, it starts the sound and decrements its hp by 1
        else if (collision.gameObject.tag == "Ball" && collision.gameObject.GetComponent<Rigidbody>().velocity != Vector3.zero)
        {
            Ball dmg = collision.gameObject.GetComponent<Ball>();
            collected = false;
            hp -= dmg.damage;
            AudioManager.instance.Play("Activator");
        }
    }

    //Fade animation
    public void Dissolve()
    {
        //If the current y is greater than min the animation continues, otherwise it destroys itself
        if (currentY > min)
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            MaterialPropertyBlock mpb = new MaterialPropertyBlock();
            mpb.SetFloat("_DissolveY", currentY);
            currentY -= Time.deltaTime * speed;
            rend.SetPropertyBlock(mpb);
        }
        else
        {
            Destroy(gameObject);
        }        
    }
}
