﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public Rigidbody rb;
    public float speed;
    public float time;
    public float minAngle;
    Vector3 oldVelocity;

    public int damage;    

    // Use this for initialization
    void Start()
    {
        //If difficulty is Beginner, decrease the ball's speed by 2
        if(GameManager.instance.difficulty[GameManager.instance.Diff_index].Equals("BEGINNER"))
        {
            speed -= 2;
        }

        //During the start round set the velocity to 0
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0, 0, 0) * speed;      
    }

    //Make the trail of the ball
    public IEnumerator Trail()
    {
        rb.GetComponent<TrailRenderer>().emitting = true;
        yield return new WaitForSeconds(time);
        if (rb != null)
        {
            rb.GetComponent<TrailRenderer>().emitting = false;
        }
    }

    private void FixedUpdate()
    {
        //It forces the ball not to have an angle greater than minangle
        if (rb.velocity != Vector3.zero)
        {
            Vector3 v = rb.velocity;

            float minAngleRad = minAngle * Mathf.Deg2Rad;

            float angleRad = Mathf.Atan2(Mathf.Abs(v.z), Mathf.Abs(v.x));

            if (angleRad < minAngleRad)
            {
                Debug.Log("Odio la trigonometria!");

                float s = v.magnitude;

                rb.velocity = s * new Vector3(Mathf.Sign(v.x) * Mathf.Cos(minAngleRad), 0, Mathf.Sign(v.z) * Mathf.Sin(minAngleRad));
            }
        }
        else
        {
            rb.transform.position += new Vector3(0, 0, GameManager.instance.vSpeed * Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //If it collides with the wall, it starts the audio
        if (collision.gameObject.tag == "Wall")
        {
            AudioManager.instance.Play("SideWall");
        }
    }
}
