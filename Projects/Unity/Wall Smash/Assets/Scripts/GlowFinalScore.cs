﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlowFinalScore : MonoBehaviour
{
    Text scoreText;
    public bool isPlayer1Score;

	//Set the final score glow of the winner
	void Start ()
    {
        scoreText = GetComponent<Text>();
        if(isPlayer1Score)
        {
            scoreText.text = GameManager.instance.finalScorePlayer1.ToString();
        }
        else
        {
            scoreText.text = GameManager.instance.finalScorePlayer2.ToString();
        }        
	}
	
}
