﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewMapInMenu : MonoBehaviour {

    List<string> maps;
    private GameObject obj_fpm;
    private GameObject obj_mem;
    private FastPlayMenu fpm;
    private MatchmakingEditorMenu mem;
    int map_id;
    Text text;


    // Use this for initialization
    void Start() {
        maps = GameManager.instance.maps;
        obj_fpm = GameObject.Find("popup_fastplay");
        obj_mem = GameObject.Find("popup_matchmaking_edit");
        fpm = gameObject.GetComponentInParent<FastPlayMenu>();
        mem = gameObject.GetComponentInParent<MatchmakingEditorMenu>();
        if (obj_mem != null) {
            if (obj_mem.activeSelf) map_id = mem.Map_id;
        }
        else if (obj_fpm != null) {
            if (obj_fpm.activeSelf) map_id = fpm.Map_id;
        }
        else map_id = 0;
        text = gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        if (obj_mem != null) {
            if (obj_mem.activeSelf) map_id = mem.Map_id;
        }
        else if (obj_fpm != null) {
            if (obj_fpm.activeSelf) map_id = fpm.Map_id;
        }
        text.text = maps[map_id];
    }
}
