﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewDifficultyInMenu : MonoBehaviour {

    List<string> difficulty;
    private GameObject obj_fpm;
    private GameObject obj_mem;
    private FastPlayMenu fpm;
    private MatchmakingEditorMenu mem;
    int diff_id;
    Text text;


    // Use this for initialization
    void Start () {
        difficulty = GameManager.instance.difficulty;
        obj_fpm = GameObject.Find("popup_fastplay");
        obj_mem = GameObject.Find("popup_matchmaking_edit");
        fpm = gameObject.GetComponentInParent<FastPlayMenu>();
        mem = gameObject.GetComponentInParent<MatchmakingEditorMenu>();
        if (obj_mem != null) {
            if (obj_mem.activeSelf) diff_id = mem.Diff_id;
        }
        else if (obj_fpm != null) {
            if (obj_fpm.activeSelf) diff_id = fpm.Diff_id;
        }
        else diff_id = 0;
        text = gameObject.GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        if (obj_mem != null) {
            if (obj_mem.activeSelf) diff_id = mem.Diff_id;
        }
        else if (obj_fpm != null) {
            if (obj_fpm.activeSelf) diff_id = fpm.Diff_id;
        }
        text.text = difficulty[diff_id];
    }
}
