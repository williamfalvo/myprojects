﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinaClaymore : MonoBehaviour
{
    // explosion fx variable
    public GameObject boomfx;    

    // claymore mine component 
    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        // if the player collides with the claymore mine, he takes damage 
        // the claymore mine is destroyed and creates an explosion
        if (collision.gameObject.tag == "Player")
        {
            AudioManager.instance.Play("Claymore");            

            Instantiate(boomfx, rb.transform.position, Quaternion.identity);

            Player dmg = collision.gameObject.GetComponent<Player>();
            dmg.hp--;

            Destroy(gameObject);
        }        
    }
}
