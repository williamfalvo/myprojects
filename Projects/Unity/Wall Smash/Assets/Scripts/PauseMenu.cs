﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenuUI;
    public GameObject leaveMatchUI;
    Rigidbody Velocity;

    //Pause the game and block the ball
    public void Pause()
    {
        AudioManager.instance.Play("PopUp");
        pauseMenuUI.SetActive(true);
        GameManager.instance.BallSleep();
        Time.timeScale = 0f;
    }

    //Resume the game and reset countdown
    public void Resume()
    {
        AudioManager.instance.Play("Confirm");
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;        
        GameManager.instance.ResetCountdown();
    }

    //Open the leave game pop-up
    public void Leave()
    {
        AudioManager.instance.Play("PopUp");
        leaveMatchUI.SetActive(true);
    }

    //Leave the match and return to main menu
    public void Yes()
    {
        AudioManager.instance.Play("Back");
        GameManager.instance.isPlayer1 = false;
        SceneManager.LoadScene("MainMenu");
    }

    //Return to Pause panel
    public void No()
    {
        AudioManager.instance.Play("Confirm");
        leaveMatchUI.SetActive(false);
    }
}
