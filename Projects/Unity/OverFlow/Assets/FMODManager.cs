﻿using UnityEngine;
using FMODUnity;
using FMOD.Studio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FMODManager : MonoBehaviour
{
    EventInstance playerJump;
    EventInstance playerDoubleJump;
    EventInstance playerDamage;
    EventInstance playerDamageDebuff;
    EventInstance playerDash;
    EventInstance playerDeathAll;
    EventInstance playerDeathHumanoid;
    EventInstance playerMissionAborted;
    EventInstance playerFootsteps;
    EventInstance playerHubRespawn;
    EventInstance playerLanding;
    EventInstance playerRangedSkill;
    EventInstance playerShockwaveSkill;
    EventInstance playerLowHp;
    EventInstance interactionChestsCards;

    EventInstance humanEnemiesTriggered;
    EventInstance enemyKilled;
    EventInstance enemiesMelee;
    EventInstance enemiesRanged;
    EventInstance jailerAttack;
    EventInstance minesExplosion;
    EventInstance miragerCopyCreation;
    EventInstance dasherTeleport;
    EventInstance flyingDrone;
    EventInstance hVoltage;
    EventInstance malware;
    EventInstance mobileLaser;

    EventInstance hammerFirstAttack;
    EventInstance hammerSecondAttack;
    EventInstance hammerThirdAttack;
    EventInstance hammerHit;
    EventInstance katanaFirstAttack;
    EventInstance katanaSecondAttack;
    EventInstance katanaThirdAttack;
    EventInstance katanaHit;
    EventInstance spearFirstAttack;
    EventInstance spearSecondAttack;
    EventInstance spearThirdAttack;
    EventInstance spearHit;

    EventInstance terminalLogIn;
    EventInstance terminalLogOut;
    EventInstance terminalRageQuit;
    EventInstance terminalNewChunk;
    EventInstance terminalLevelUp;
    EventInstance terminalChunkCompleted;
    EventInstance hubNpcDialogue;
    
    EventInstance chunkMusic;
    EventInstance mainMenuMusic;
    EventInstance rainChunkMusic;
    EventInstance teleportUsed;
    EventInstance itemUsed;
    EventInstance emptyItem;
    EventInstance menuBack;
    EventInstance menuSelection;
    EventInstance menuNavigation;

    [HideInInspector]
    public float slidersVolumeMusic = 1f;
    [HideInInspector]
    public float slidersVolumeSFX = 1f;

    public static FMODManager instance = null;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        playerJump = RuntimeManager.CreateInstance("event:/SFX/Jump");
        playerDoubleJump = RuntimeManager.CreateInstance("event:/SFX/Second_Jump");
        playerDamage = RuntimeManager.CreateInstance("event:/SFX/Damage");
        playerDamageDebuff = RuntimeManager.CreateInstance("event:/SFX/Debuff");
        playerDash = RuntimeManager.CreateInstance("event:/SFX/Dash");
        playerDeathAll = RuntimeManager.CreateInstance("event:/SFX/Death");
        playerDeathHumanoid = RuntimeManager.CreateInstance("event:/SFX/Player_Killed");
        playerMissionAborted = RuntimeManager.CreateInstance("event:/SFX/Terminal_Gameplay_RageQuit");
        playerFootsteps = RuntimeManager.CreateInstance("event:/SFX/Footsteps");
        playerHubRespawn = RuntimeManager.CreateInstance("event:/SFX/Hub_Respawn");
        playerLanding = RuntimeManager.CreateInstance("event:/SFX/Landing");
        playerRangedSkill = RuntimeManager.CreateInstance("event:/SFX/Ranged_Attack");
        playerShockwaveSkill = RuntimeManager.CreateInstance("event:/SFX/Shockwave");
        playerLowHp = RuntimeManager.CreateInstance("event:/SFX/Low_Hp");
        interactionChestsCards = RuntimeManager.CreateInstance("event:/SFX/Interaction_Chests_Cards");

        humanEnemiesTriggered = RuntimeManager.CreateInstance("event:/SFX/Human_enemies_Triggered");
        enemyKilled = RuntimeManager.CreateInstance("event:/SFX/Enemy_Killed");
        enemiesMelee = RuntimeManager.CreateInstance("event:/SFX/All_Enemies_Melee_Attack");
        jailerAttack = RuntimeManager.CreateInstance("event:/SFX/Jailer_Attack");
        dasherTeleport = RuntimeManager.CreateInstance("event:/SFX/Dasher_Teleport");
        hVoltage = RuntimeManager.CreateInstance("event:/SFX/H!_Voltage_Loop");
        mobileLaser = RuntimeManager.CreateInstance("event:/SFX/Mobile_Laser_NoiseLoop");

        hammerFirstAttack = RuntimeManager.CreateInstance("event:/SFX/Hammer_First_Attack");
        hammerSecondAttack = RuntimeManager.CreateInstance("event:/SFX/Hammer_Second_Attack");
        hammerThirdAttack = RuntimeManager.CreateInstance("event:/SFX/Hammer_Third_Attack");
        hammerHit = RuntimeManager.CreateInstance("event:/SFX/Feedback_Hammer_Hit");
        katanaFirstAttack = RuntimeManager.CreateInstance("event:/SFX/Katana_First_Attack");
        katanaSecondAttack = RuntimeManager.CreateInstance("event:/SFX/Katana_Second_Attack");
        katanaThirdAttack = RuntimeManager.CreateInstance("event:/SFX/Katana_Third_Attack");
        katanaHit = RuntimeManager.CreateInstance("event:/SFX/Feedback_Katana_Hit");
        spearFirstAttack = RuntimeManager.CreateInstance("event:/SFX/Spear_First_Attack");
        spearSecondAttack = RuntimeManager.CreateInstance("event:/SFX/Spear_Second_Attack");
        spearThirdAttack = RuntimeManager.CreateInstance("event:/SFX/Spear_Third_Attack");
        spearHit = RuntimeManager.CreateInstance("event:/SFX/Feedback_Spear_Hit");

        terminalLogIn = RuntimeManager.CreateInstance("event:/SFX/Terminal_Login");
        terminalLogOut = RuntimeManager.CreateInstance("event:/SFX/Terminal_Logout");
        terminalRageQuit = RuntimeManager.CreateInstance("event:/SFX/Terminal_Gameplay_RageQuit");
        terminalNewChunk = RuntimeManager.CreateInstance("event:/SFX/Terminal_Gameplay_NewChunk");
        terminalLevelUp = RuntimeManager.CreateInstance("event:/SFX/Terminal_Gameplay_LevelUp");
        terminalChunkCompleted = RuntimeManager.CreateInstance("event:/SFX/Terminal_Chunk_Completed");
        hubNpcDialogue = RuntimeManager.CreateInstance("event:/SFX/Hub_NPC_Dialogue");

        chunkMusic = RuntimeManager.CreateInstance("event:/Music/Chunk_Music");
        mainMenuMusic = RuntimeManager.CreateInstance("event:/Music/Main_Menu");
        rainChunkMusic = RuntimeManager.CreateInstance("event:/Music/Rain_Background_Chunk");
        teleportUsed = RuntimeManager.CreateInstance("event:/SFX/Teleport_Used");
        itemUsed = RuntimeManager.CreateInstance("event:/SFX/Item_Used");
        emptyItem = RuntimeManager.CreateInstance("event:/SFX/Empty_Item");
        menuBack = RuntimeManager.CreateInstance("event:/SFX/Menu_Back");
        menuSelection = RuntimeManager.CreateInstance("event:/SFX/Menu_Selection");
        menuNavigation = RuntimeManager.CreateInstance("event:/SFX/Menu_Navigation");
    }

    public void PlayMenuMusic()
    {
        mainMenuMusic.start();
    }

    public void DecreaseMenuVolume()
    {
        mainMenuMusic.setVolume(0.4f);
    }

    public void ResetMenuVolume()
    {
        mainMenuMusic.setVolume(1.0f);
    }

    public void StopMenuMusic()
    {
        mainMenuMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void PlayChunkMusic()
    {
        chunkMusic.setParameterValue("Play", Random.Range(.0f, 3.0f));
        chunkMusic.start();
    }
    
    public void DecreaseChunkVolume()
    {
        chunkMusic.setVolume(0.4f);
    }

    public void ResetChunkVolume()
    {
        chunkMusic.setVolume(1.0f);
    }

    public void StopChunkMusic()
    {
        chunkMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void PlayRainMusic()
    {
        rainChunkMusic.start();
    }

    public void DecreaseRainVolume()
    {
        rainChunkMusic.setVolume(0.4f);
    }

    public void ResetRainVolume()
    {
        rainChunkMusic.setVolume(1.0f);
    }

    public void StopRainMusic()
    {
        rainChunkMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void TeleportUsed()
    {
        teleportUsed.start();
    }

    public void PlayTerminalLogIn()
    {
        terminalLogIn.start();
    }

    public void StopTerminalLogIn()
    {
        terminalLogIn.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void PlayTerminalLogOut()
    {
        terminalLogOut.start();
    }

    public void StopTerminalLogOut()
    {
        terminalLogOut.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void PlayerJumpAudio()
    {
        playerJump.start();
    }

    public void PlayerDoubleJumpAudio()
    {
        playerDoubleJump.start();
    }

    public void PlayPlayerDamage()
    {
        playerDamage.start();
    }

    public void PlayPlayerDash()
    {
        playerDash.start();
    }

    public void PlayPlayerDeath()
    {
        playerDeathAll.start();
    }

    public void PlayPlayerFootsteps()
    {
        playerFootsteps.start();
    }

    public void StopPlayerFootsteps()
    {
        playerFootsteps.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void PlayHubSpawn()
    {
        playerHubRespawn.start();
    }

    public void PlayPlayerLanding()
    {
        playerLanding.start();
    }

    public void PlayEnemyDeath()
    {
        enemyKilled.start();
    }

    public void PlayMissionAborted()
    {
        playerMissionAborted.start();
    }

    public void HammerFirstAttack()
    {
        hammerFirstAttack.start();
    }

    public void HammerSecondAttack()
    {
        hammerSecondAttack.start();
    }

    public void HammerThirdAttack()
    {
        hammerThirdAttack.start();
    }

    public void HammerHit()
    {
        hammerHit.start();
    }

    public void KatanaFirstAttack()
    {
        katanaFirstAttack.start();
    }

    public void KatanaSecondAttack()
    {
        katanaSecondAttack.start();
    }

    public void KatanaThirdAttack()
    {
        katanaThirdAttack.start();
    }

    public void KatanaHit()
    {
        katanaHit.start();
    }

    public void SpearFirstAttack()
    {
        spearFirstAttack.start();
    }

    public void SpearSecondAttack()
    {
        spearSecondAttack.start();
    }

    public void SpearThirdAttack()
    {
        spearThirdAttack.start();
    }

    public void SpearHit()
    {
        spearHit.start();
    }

    public void ItemUsed()
    {
        itemUsed.start();
    }

    public void EmptyItem()
    {
        emptyItem.start();
    }

    public void TerminalRageQuit()
    {
        terminalRageQuit.start();
    }

    public void TerminalNewChunk()
    {
        terminalNewChunk.start();
    }

    public void TerminalChunkC()
    {
        terminalChunkCompleted.start();
    }

    public void HubNpcDialogue()
    {
        hubNpcDialogue.start();
    }

    public void PlayMenuBack()
    {
        menuBack.start();
    }

    public void PlayeMenuSelection()
    {
        menuSelection.start();
    }

    public void PlayMenuNavigation()
    {
        menuNavigation.start();
    }

    public void PlayEnemyKilled()
    {
        enemyKilled.start();
    }

    public void PlayDasherTeleport()
    {
        dasherTeleport.start();
    }

    public void PlayEnemiesMelee()
    {
        enemiesMelee.start();
    }

    public void StopEnemiesMelee()
    {
        enemiesMelee.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void PlayPDeathHumanoid()
    {
        playerDeathHumanoid.start();
    }

    public void HumanEnemiesTriggered()
    {
        humanEnemiesTriggered.start();
    }

    public void PlayerRanged()
    {
        playerRangedSkill.start();
    }

    public void PlayerShockwave()
    {
        playerShockwaveSkill.start();
    }

    public void PlayPlayerDamageDebuff()
    {
        playerDamageDebuff.start();
    }

    public void StopPlayerDamageDebuff()
    {
        playerDamageDebuff.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void PlayJailerAttack()
    {
        jailerAttack.start();
    }

    public void PlayPlayerLowHp()
    {
        playerLowHp.start();
    }

    public void StopPlayerLowHp()
    {
        playerLowHp.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void PlayHVoltage()
    {
        hVoltage.start();
    }

    public void StopHVoltage()
    {
        hVoltage.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void PlayMobileLaser()
    {
        mobileLaser.start();
    }

    public void StopMobileLaser()
    {
        mobileLaser.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void PlayLevelUp()
    {
        terminalLevelUp.start();
    }

    public void PlayIntDropCards()
    {
        interactionChestsCards.start();
    }
}
