﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionTrail : MonoBehaviour
{   
    public GameObject TargetSkinMesh;

    [HideInInspector]
    public PlayerController player;

    [Range(0, 1)]
    public float ExportSpeedDelay = 0.1f;
    
    public bool UseLifeTime = false; 
    public float EffectLifeTime = 3;

    [Header("------------------------------------------------------------------------------------------------------------------------------------------------------")]
    
    public string ValueName;
    
    [Range(0, 1)]
    public float ValueTimeDelay = 0.1f;
    
    [Range(0, 1)]
    public float ValueDetail = 0.1f;

    [Range(0, 1)]
    public float alphaValue = .5f;

    private bool NeedObject;

    private void Start()
    {
        
    }

    private void Update()
    {
        if (TargetSkinMesh != null && ValueName != "")
        {
            StopAllCoroutines();
            StartCoroutine("GhostStart");

            if (UseLifeTime == true)
            {
                StartCoroutine("TimerStart");
            }
        }
    }

    IEnumerator GhostStart()
    {
        for (int e = 0; e < e + 1; e++)
        {
            NeedObject = false;
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform tr = transform.GetChild(i);

                if (tr.gameObject.activeSelf == false)
                {
                    tr.position = TargetSkinMesh.transform.position;
                    tr.rotation = TargetSkinMesh.transform.rotation;

                    if(player.indexWeapon == 1)
                    {
                        tr.localScale = new Vector3(player.transform.localScale.x, 1f, 1f);
                    }
                    else
                    {
                        tr.localScale = TargetSkinMesh.transform.lossyScale;
                    }
                    
                    tr.GetComponent<MotionTrailRenderer>().SkinMesh = TargetSkinMesh.GetComponent<SkinnedMeshRenderer>();

                    tr.GetComponent<MotionTrailRenderer>().ValueName = ValueName;
                    tr.GetComponent<MotionTrailRenderer>().ValueTimeDelay = ValueTimeDelay;
                    tr.GetComponent<MotionTrailRenderer>().ValueDetail = ValueDetail;
                    tr.GetComponent<MotionTrailRenderer>().alpha = alphaValue;
                    tr.GetComponent<MotionTrailRenderer>().movement = player.canMove;
                    tr.gameObject.SetActive(true);
                    NeedObject = true;
                    break;
                }
            }           

            yield return new WaitForSeconds(ExportSpeedDelay);
        }

        yield return null;
    }

    IEnumerator TimerStart()
    {
        yield return new WaitForSeconds(EffectLifeTime);

        StopAllCoroutines();

        yield return null;
    }
}
