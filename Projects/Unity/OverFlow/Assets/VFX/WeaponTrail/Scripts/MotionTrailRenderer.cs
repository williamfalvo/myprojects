﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionTrailRenderer : MonoBehaviour
{
    [HideInInspector]public SkinnedMeshRenderer SkinMesh; 
    [HideInInspector]public string ValueName;
    [HideInInspector]public float ValueDetail;
    [HideInInspector]public float ValueTimeDelay;
    [HideInInspector]public float alpha;
    [HideInInspector]public bool movement = false;    

    PlayerController player;
    Mesh BakedMeshResult;

    private void Awake()
    {
        player = GameManager.instance.playerController;
    }

    private void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine("MaterialColorAnimation");
    }    

    IEnumerator MaterialColorAnimation()
    {
        if (BakedMeshResult == null)
        {
            BakedMeshResult = new Mesh();
        }

        SkinMesh.BakeMesh(BakedMeshResult);
        this.GetComponent<MeshFilter>().mesh = BakedMeshResult;

        Renderer rend = GetComponent<Renderer>();      

        Color col1 = player.weapons.Find(GameObject => GameObject.w == player.currentWeapon).c1;
        Color col2 = player.weapons.Find(GameObject => GameObject.w == player.currentWeapon).c2;

        if (movement)
        {
            col1.a = alpha;
            col2.a = alpha;

            rend.material.SetColor("_Color", col1);
            rend.material.SetColor("_Color2", col2);          
        }
        else
        {
            col1.a = 1f;
            col2.a = 1f;

            rend.material.SetColor("_Color", col1);
            rend.material.SetColor("_Color2", col2);
        }

        for (float e = 0; e < 1.1; e += ValueDetail)
        {
            this.GetComponent<MeshRenderer>().material.SetFloat(ValueName, e);
            yield return new WaitForSeconds(ValueTimeDelay);
        }

        this.gameObject.SetActive(false);
    }
}
