﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockJam : MonoBehaviour
{
    [HideInInspector]
    public SkinnedMeshRenderer skinmesh;

    float side;
    Mesh bakedMesh;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Test();        
    }

    public void Test()
    {
        if (bakedMesh == null)
        {
            bakedMesh = new Mesh();
        }

        skinmesh.BakeMesh(bakedMesh);

        GetComponent<ParticleSystemRenderer>().mesh = bakedMesh;

        ParticleSystem subEmit = transform.GetChild(0).GetComponent<ParticleSystem>();
        var sh = subEmit.shape;
        sh.mesh = bakedMesh;

        transform.localScale = new Vector3(GameManager.instance.playerController.transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
}
