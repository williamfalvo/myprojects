﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEffect : MonoBehaviour
{
    public float spawnEffectTime = 2;
    public float pause = 1;
    public AnimationCurve fadeIn;

    public ParticleSystem ps;
    float timer = 0;
    public Renderer _renderer;

    int shaderProperty;

    [HideInInspector]
    public bool done = false;

	void Start ()
    {
        //shaderProperty = Shader.PropertyToID("_cutoff");
        ////_renderer = GetComponent<Renderer>();
        ////ps = GetComponentInChildren <ParticleSystem>();

        //var main = ps.main;
        ////main.duration = spawnEffectTime;       

        //ps.Play();
    }

    private void OnEnable()
    {
        shaderProperty = Shader.PropertyToID("_cutoff");

        _renderer.material.SetFloat(shaderProperty, 0f);

        var main = ps.main;

        ps.Play();
    }

    void Update ()
    {
        if (timer < spawnEffectTime)
        {
            timer += Time.deltaTime;
        }
        //else
        //{
        //    ps.Play();
        //    timer = 0;
        //}

        _renderer.material.SetFloat(shaderProperty, fadeIn.Evaluate(Mathf.InverseLerp(0, spawnEffectTime, timer)));
    }

    public void OnDisable()
    {
        timer = 0f;
    }
}
