// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True,fsmp:False;n:type:ShaderForge.SFN_Final,id:4795,x:33678,y:32848,varname:node_4795,prsc:2|emission-6378-OUT;n:type:ShaderForge.SFN_Tex2d,id:6074,x:32784,y:32786,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:8af9794096e484047ac945601c8358fe,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:2393,x:33043,y:32947,varname:node_2393,prsc:2|A-6074-RGB,B-6940-OUT,C-797-RGB,D-9248-OUT;n:type:ShaderForge.SFN_VertexColor,id:2053,x:32784,y:33342,varname:node_2053,prsc:2;n:type:ShaderForge.SFN_Color,id:797,x:32784,y:33115,ptovrint:True,ptlb:Color,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Vector1,id:9248,x:32784,y:33266,varname:node_9248,prsc:2,v1:2;n:type:ShaderForge.SFN_Time,id:976,x:29851,y:32432,varname:node_976,prsc:2;n:type:ShaderForge.SFN_Multiply,id:960,x:31334,y:32955,varname:node_960,prsc:2|A-976-T,B-4589-OUT;n:type:ShaderForge.SFN_Slider,id:4589,x:30982,y:33128,ptovrint:False,ptlb:waveSpeed,ptin:_waveSpeed,varname:node_4589,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-3,cur:0.5,max:3;n:type:ShaderForge.SFN_Add,id:2770,x:31508,y:32955,varname:node_2770,prsc:2|A-1713-V,B-960-OUT;n:type:ShaderForge.SFN_TexCoord,id:1713,x:31334,y:32804,varname:node_1713,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:9622,x:31705,y:32955,varname:node_9622,prsc:2|A-2770-OUT,B-3763-OUT;n:type:ShaderForge.SFN_Slider,id:3763,x:31338,y:33158,ptovrint:False,ptlb:node_3763,ptin:_node_3763,varname:node_3763,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-10,cur:2,max:10;n:type:ShaderForge.SFN_Frac,id:4481,x:31885,y:32957,varname:node_4481,prsc:2|IN-9622-OUT;n:type:ShaderForge.SFN_Power,id:3780,x:32067,y:33020,varname:node_3780,prsc:2|VAL-4481-OUT,EXP-1988-OUT;n:type:ShaderForge.SFN_Slider,id:1988,x:31685,y:33156,ptovrint:False,ptlb:wavePower,ptin:_wavePower,varname:node_1988,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:3,max:3;n:type:ShaderForge.SFN_Clamp01,id:6940,x:32784,y:32962,varname:node_6940,prsc:2|IN-8874-OUT;n:type:ShaderForge.SFN_Multiply,id:6378,x:33302,y:32946,varname:node_6378,prsc:2|A-2393-OUT,B-3240-OUT;n:type:ShaderForge.SFN_Multiply,id:3240,x:33043,y:33113,varname:node_3240,prsc:2|A-6074-A,B-2053-A;n:type:ShaderForge.SFN_Multiply,id:7973,x:30120,y:32329,varname:node_7973,prsc:2|A-486-OUT,B-976-TSL;n:type:ShaderForge.SFN_ValueProperty,id:486,x:29645,y:32208,ptovrint:False,ptlb:noiseTime,ptin:_noiseTime,varname:node_486,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3;n:type:ShaderForge.SFN_Frac,id:6856,x:30292,y:32329,varname:node_6856,prsc:2|IN-7973-OUT;n:type:ShaderForge.SFN_Multiply,id:444,x:30472,y:32329,varname:node_444,prsc:2|A-6856-OUT,B-7933-OUT,C-486-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7933,x:30259,y:32518,ptovrint:False,ptlb:noisePosterization,ptin:_noisePosterization,varname:node_7933,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:70;n:type:ShaderForge.SFN_Round,id:7110,x:30657,y:32329,varname:node_7110,prsc:2|IN-444-OUT;n:type:ShaderForge.SFN_Add,id:5748,x:30846,y:32329,varname:node_5748,prsc:2|A-7110-OUT,B-1713-UVOUT;n:type:ShaderForge.SFN_Posterize,id:7689,x:31049,y:32329,varname:node_7689,prsc:2|IN-5748-OUT,STPS-7933-OUT;n:type:ShaderForge.SFN_Noise,id:2268,x:31260,y:32329,varname:node_2268,prsc:2|XY-7689-OUT;n:type:ShaderForge.SFN_Slider,id:188,x:31483,y:31959,ptovrint:False,ptlb:noisePower,ptin:_noisePower,varname:node_188,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Round,id:5072,x:30657,y:32148,varname:node_5072,prsc:2|IN-7064-OUT;n:type:ShaderForge.SFN_Multiply,id:7064,x:30472,y:32148,varname:node_7064,prsc:2|A-486-OUT,B-1424-OUT,C-6856-OUT;n:type:ShaderForge.SFN_Add,id:6322,x:30846,y:32148,varname:node_6322,prsc:2|A-5072-OUT,B-1713-UVOUT;n:type:ShaderForge.SFN_Posterize,id:887,x:31049,y:32148,varname:node_887,prsc:2|IN-6322-OUT,STPS-1424-OUT;n:type:ShaderForge.SFN_Noise,id:9186,x:31260,y:32148,varname:node_9186,prsc:2|XY-887-OUT;n:type:ShaderForge.SFN_Round,id:5368,x:30657,y:31971,varname:node_5368,prsc:2|IN-6607-OUT;n:type:ShaderForge.SFN_Multiply,id:6607,x:30472,y:31971,varname:node_6607,prsc:2|A-1912-OUT,B-1424-OUT,C-486-OUT,D-6856-OUT;n:type:ShaderForge.SFN_Add,id:8381,x:30846,y:31971,varname:node_8381,prsc:2|A-5368-OUT,B-1713-UVOUT;n:type:ShaderForge.SFN_Posterize,id:5079,x:31049,y:31971,varname:node_5079,prsc:2|IN-8381-OUT,STPS-1424-OUT;n:type:ShaderForge.SFN_Noise,id:1671,x:31260,y:31971,varname:node_1671,prsc:2|XY-5079-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1424,x:29753,y:31851,ptovrint:False,ptlb:posterizationNoise,ptin:_posterizationNoise,varname:node_1424,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_Vector1,id:1912,x:29585,y:31970,varname:node_1912,prsc:2,v1:2;n:type:ShaderForge.SFN_Add,id:5452,x:31456,y:32070,varname:node_5452,prsc:2|A-1671-OUT,B-9186-OUT;n:type:ShaderForge.SFN_Clamp01,id:421,x:31676,y:32070,varname:node_421,prsc:2|IN-5452-OUT;n:type:ShaderForge.SFN_Multiply,id:6941,x:31915,y:32553,varname:node_6941,prsc:2|A-188-OUT,B-421-OUT,C-2268-OUT;n:type:ShaderForge.SFN_Clamp01,id:6697,x:32158,y:32576,varname:node_6697,prsc:2|IN-6941-OUT;n:type:ShaderForge.SFN_Add,id:8874,x:32543,y:32949,varname:node_8874,prsc:2|A-6697-OUT,B-3780-OUT;proporder:6074-797-4589-3763-1988-486-7933-188-1424;pass:END;sub:END;*/

Shader "Shader Forge/Holo" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _TintColor ("Color", Color) = (1,1,1,1)
        _waveSpeed ("waveSpeed", Range(-3, 3)) = 0.5
        _node_3763 ("node_3763", Range(-10, 10)) = 2
        _wavePower ("wavePower", Range(0, 3)) = 3
        _noiseTime ("noiseTime", Float ) = 3
        _noisePosterization ("noisePosterization", Float ) = 70
        _noisePower ("noisePower", Range(0, 1)) = 0.5
        _posterizationNoise ("posterizationNoise", Float ) = 5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _TintColor;
            uniform float _waveSpeed;
            uniform float _node_3763;
            uniform float _wavePower;
            uniform float _noiseTime;
            uniform float _noisePosterization;
            uniform float _noisePower;
            uniform float _posterizationNoise;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 node_976 = _Time;
                float node_6856 = frac((_noiseTime*node_976.r));
                float2 node_5079 = floor((round((2.0*_posterizationNoise*_noiseTime*node_6856))+i.uv0) * _posterizationNoise) / (_posterizationNoise - 1);
                float2 node_1671_skew = node_5079 + 0.2127+node_5079.x*0.3713*node_5079.y;
                float2 node_1671_rnd = 4.789*sin(489.123*(node_1671_skew));
                float node_1671 = frac(node_1671_rnd.x*node_1671_rnd.y*(1+node_1671_skew.x));
                float2 node_887 = floor((round((_noiseTime*_posterizationNoise*node_6856))+i.uv0) * _posterizationNoise) / (_posterizationNoise - 1);
                float2 node_9186_skew = node_887 + 0.2127+node_887.x*0.3713*node_887.y;
                float2 node_9186_rnd = 4.789*sin(489.123*(node_9186_skew));
                float node_9186 = frac(node_9186_rnd.x*node_9186_rnd.y*(1+node_9186_skew.x));
                float2 node_7689 = floor((round((node_6856*_noisePosterization*_noiseTime))+i.uv0) * _noisePosterization) / (_noisePosterization - 1);
                float2 node_2268_skew = node_7689 + 0.2127+node_7689.x*0.3713*node_7689.y;
                float2 node_2268_rnd = 4.789*sin(489.123*(node_2268_skew));
                float node_2268 = frac(node_2268_rnd.x*node_2268_rnd.y*(1+node_2268_skew.x));
                float3 emissive = ((_MainTex_var.rgb*saturate((saturate((_noisePower*saturate((node_1671+node_9186))*node_2268))+pow(frac(((i.uv0.g+(node_976.g*_waveSpeed))*_node_3763)),_wavePower)))*_TintColor.rgb*2.0)*(_MainTex_var.a*i.vertexColor.a));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
