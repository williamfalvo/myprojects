﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InspectManagerUI : MonoBehaviour
{

    public Image myImage;    
    public GameObject endDrawPhaseButton;
    Sprite startSprite;

    private void Awake()
    {
        startSprite = myImage.sprite;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(EventSystem.current.currentSelectedGameObject == endDrawPhaseButton)
        {
            myImage.sprite = startSprite;
        }
    }

    public void UpdateImage(Card card)
    {
        myImage.sprite = card.artwork;
    }

}
