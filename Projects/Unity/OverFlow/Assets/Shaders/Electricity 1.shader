// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:Dissolve,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True,fsmp:False;n:type:ShaderForge.SFN_Final,id:4795,x:34738,y:32531,varname:node_4795,prsc:2|emission-7137-OUT;n:type:ShaderForge.SFN_VertexColor,id:2053,x:34019,y:32652,varname:node_2053,prsc:2;n:type:ShaderForge.SFN_Color,id:797,x:34019,y:32812,ptovrint:True,ptlb:Color,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.2797289,c3:1,c4:1;n:type:ShaderForge.SFN_Append,id:1915,x:31572,y:32605,varname:node_1915,prsc:2|A-4541-OUT,B-9128-OUT;n:type:ShaderForge.SFN_Time,id:4691,x:31572,y:32760,varname:node_4691,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:4541,x:31399,y:32624,ptovrint:False,ptlb:node_4541,ptin:_node_4541,varname:node_4541,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_ValueProperty,id:9128,x:31399,y:32701,ptovrint:False,ptlb:node_9128,ptin:_node_9128,varname:node_9128,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_Multiply,id:6023,x:31748,y:32605,varname:node_6023,prsc:2|A-1915-OUT,B-4691-T;n:type:ShaderForge.SFN_TexCoord,id:8898,x:31748,y:32760,varname:node_8898,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Add,id:7050,x:31922,y:32605,varname:node_7050,prsc:2|A-6023-OUT,B-8898-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:1566,x:32103,y:32605,varname:node_1566,prsc:2,tex:edb3589461decc243a352e358d6f7a61,ntxv:0,isnm:False|UVIN-7050-OUT,TEX-7579-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:7579,x:31922,y:32782,ptovrint:False,ptlb:node_7579,ptin:_node_7579,varname:node_7579,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:edb3589461decc243a352e358d6f7a61,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Append,id:7718,x:31586,y:32985,varname:node_7718,prsc:2|A-9482-OUT,B-164-OUT;n:type:ShaderForge.SFN_Multiply,id:903,x:31758,y:32985,varname:node_903,prsc:2|A-7718-OUT,B-5276-T;n:type:ShaderForge.SFN_Time,id:5276,x:31602,y:33147,varname:node_5276,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:6498,x:31758,y:33147,varname:node_6498,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ValueProperty,id:9482,x:31378,y:32985,ptovrint:False,ptlb:node_9482,ptin:_node_9482,varname:node_9482,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-0.2;n:type:ShaderForge.SFN_ValueProperty,id:164,x:31378,y:33064,ptovrint:False,ptlb:node_164,ptin:_node_164,varname:node_164,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.05;n:type:ShaderForge.SFN_Add,id:3730,x:31942,y:32985,varname:node_3730,prsc:2|A-903-OUT,B-6498-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:1226,x:32124,y:32985,varname:node_1226,prsc:2,tex:edb3589461decc243a352e358d6f7a61,ntxv:0,isnm:False|UVIN-3730-OUT,TEX-7579-TEX;n:type:ShaderForge.SFN_Slider,id:1850,x:31591,y:32503,ptovrint:False,ptlb:node_1850,ptin:_node_1850,varname:node_1850,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_OneMinus,id:9401,x:31922,y:32454,varname:node_9401,prsc:2|IN-1850-OUT;n:type:ShaderForge.SFN_RemapRange,id:1999,x:32103,y:32446,varname:node_1999,prsc:2,frmn:0,frmx:1,tomn:-0.65,tomx:0.65|IN-9401-OUT;n:type:ShaderForge.SFN_Multiply,id:6891,x:34345,y:32557,varname:node_6891,prsc:2|A-695-OUT,B-2053-RGB,C-797-RGB,D-7852-OUT;n:type:ShaderForge.SFN_Add,id:4125,x:32283,y:32605,varname:node_4125,prsc:2|A-1999-OUT,B-1226-R;n:type:ShaderForge.SFN_Multiply,id:1512,x:32447,y:32446,varname:node_1512,prsc:2|A-8625-OUT,B-4125-OUT;n:type:ShaderForge.SFN_Add,id:8625,x:32283,y:32446,varname:node_8625,prsc:2|A-1999-OUT,B-1566-R;n:type:ShaderForge.SFN_RemapRange,id:6910,x:32608,y:32446,varname:node_6910,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:3|IN-1512-OUT;n:type:ShaderForge.SFN_Clamp01,id:695,x:32768,y:32446,varname:node_695,prsc:2|IN-6910-OUT;n:type:ShaderForge.SFN_OneMinus,id:4764,x:32937,y:32446,varname:node_4764,prsc:2|IN-695-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7852,x:34019,y:32981,ptovrint:False,ptlb:node_7852,ptin:_node_7852,varname:node_7852,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Fresnel,id:6051,x:33752,y:32200,varname:node_6051,prsc:2|EXP-6183-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6183,x:33562,y:32256,ptovrint:False,ptlb:node_6183,ptin:_node_6183,varname:node_6183,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:7137,x:34495,y:32336,varname:node_7137,prsc:2|A-6051-OUT,B-6891-OUT;proporder:797-4541-9128-7579-1850-9482-164-7852-6183;pass:END;sub:END;*/

Shader "Shader Forge/Electricity1" {
    Properties {
        _TintColor ("Color", Color) = (0,0.2797289,1,1)
        _node_4541 ("node_4541", Float ) = 0.2
        _node_9128 ("node_9128", Float ) = 0.2
        _node_7579 ("node_7579", 2D) = "white" {}
        _node_1850 ("node_1850", Range(0, 1)) = 0.5
        _node_9482 ("node_9482", Float ) = -0.2
        _node_164 ("node_164", Float ) = 0.05
        _node_7852 ("node_7852", Float ) = 2
        _node_6183 ("node_6183", Float ) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x 
            #pragma target 3.0
            uniform float4 _TintColor;
            uniform float _node_4541;
            uniform float _node_9128;
            uniform sampler2D _node_7579; uniform float4 _node_7579_ST;
            uniform float _node_9482;
            uniform float _node_164;
            uniform float _node_1850;
            uniform float _node_7852;
            uniform float _node_6183;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float node_6051 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_node_6183);
                float node_1999 = ((1.0 - _node_1850)*1.3+-0.65);
                float4 node_4691 = _Time;
                float2 node_7050 = ((float2(_node_4541,_node_9128)*node_4691.g)+i.uv0);
                float4 node_1566 = tex2D(_node_7579,TRANSFORM_TEX(node_7050, _node_7579));
                float4 node_5276 = _Time;
                float2 node_3730 = ((float2(_node_9482,_node_164)*node_5276.g)+i.uv0);
                float4 node_1226 = tex2D(_node_7579,TRANSFORM_TEX(node_3730, _node_7579));
                float node_695 = saturate((((node_1999+node_1566.r)*(node_1999+node_1226.r))*4.0+-1.0));
                float3 node_6891 = (node_695*i.vertexColor.rgb*_TintColor.rgb*_node_7852);
                float3 emissive = (node_6051*node_6891);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG_COLOR(i.fogCoord, finalRGBA, fixed4(0,0,0,1));
                return finalRGBA;
            }
            ENDCG
        }
    }
    //CustomEditor "ShaderForgeMaterialInspector"
}
