﻿Shader "Unlit/Glow2"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color("Color", Color) = ( 1, 1, 1, 1 )
		_BumpMap("Normal Map", 2D) = "bump" {}
		_RimColor("Rim Color", Color) = ( 1, 1, 1, 1 )
		_RimPower("Rim Power", Range(0, 6.0)) = 3.0
	}

	SubShader
	{
		Tags { "IgnoreProjector" = "True" "Queue" = "Transparent" "RenderType" = "Transparent" }

		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;
		float4 _Color;
		sampler2D _BumpMap;
		float4 _RimColor;
		float _RimPower;
		
		struct Input
		{
			float2 uv_MainTex;
			float4 color : Color;
			float2 uv_BumpMap;
			float3 viewDir;
		};

		void surf(Input IN, inout SurfaceOutput o)
		{
			IN.color = _Color;
			
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb * IN.color;
			o.Alpha = _Color.a;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));

			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			o.Emission = _RimColor.rgb * pow(rim, _RimPower) * 2.0;
		}

        ENDCG        
    }

	/*FallBack "Diffuse"*/
}
