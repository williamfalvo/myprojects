﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD;
using FMODUnity;
using UnityEngine.UI;

public class VolumeRegulationUI : MonoBehaviour
{
    #region Fields

    float musicVolume;
    FMOD.Studio.Bus musicBus;
    public Slider musicSlider;

    float sfxVolume;
    FMOD.Studio.Bus sfxBus;
    public Slider sfxSlider;

    #endregion

    #region Methods

    // Start is called before the first frame update
    void Start()
    {

        musicVolume = FMODManager.instance.slidersVolumeMusic;
        sfxVolume = FMODManager.instance.slidersVolumeSFX;

        musicSlider.value = musicVolume;
        sfxSlider.value = sfxVolume;

        //Music
        musicBus = RuntimeManager.GetBus("bus:/Music");
        //musicBus.getVolume(out musicVolume, out musicVolume);

        //SFX
        sfxBus = RuntimeManager.GetBus("bus:/SFX");
        //sfxBus.getVolume(out sfxVolume, out sfxVolume);
    }

    //Music
    public void OnMusicValueChanged()
    {

        FMODManager.instance.slidersVolumeMusic = musicSlider.value;
       
        musicVolume = musicSlider.value;
        musicBus.setVolume(musicVolume);
    }

    //SFX
    public void OnSFXValueChanged()
    {

        FMODManager.instance.slidersVolumeSFX = sfxSlider.value;
        
        sfxVolume = sfxSlider.value;
        sfxBus.setVolume(sfxVolume);
    }

    #endregion
}
