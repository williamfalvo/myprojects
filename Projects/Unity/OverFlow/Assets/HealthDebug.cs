﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthDebug : MonoBehaviour
{
    public TextMesh textMesh;
    public CharacterStatistics enemy;

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(enemy.transform.localScale.x, 1, 1);

        textMesh.text = enemy.currentHealth.ToString();
    }
}
