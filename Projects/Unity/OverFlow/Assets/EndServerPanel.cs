﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Coffee.UIExtensions;
using UnityEngine.UI;

public class EndServerPanel : MonoBehaviour
{
    List<Card> cards = new List<Card>();

    public GameObject card;
    public Image panelImage;
    public DungeonCreation dg;
    public GameObject tutorialText;
    public Scrollbar scrollbar;

    private void OnEnable()
    {
        InitializePanel();
        StartCoroutine(SetFirstButton());

        if (dg.currentDungeonRooms.Count == 3)
        {
            tutorialText.SetActive(true);
        }
    }

    void InitializePanel()
    {
        cards = GameManager.instance.objectsInventory.cards;

        if(cards.Count < 9)
        {
            scrollbar.enabled = false;
        }

        foreach (Card c in cards)
        {
            bool newCard = true;

            foreach (Card colCard in GameManager.instance.collection.cards)
            {
                if (c.name == colCard.name)
                {
                    newCard = false;
                    break;
                }
            }

            GameObject go = Instantiate(card, transform);

            go.transform.GetChild(1).GetComponent<Image>().sprite = c.artwork;
            go.GetComponentInChildren<CollectionCard>().panelImage = panelImage;

            if (newCard)
            {
                UIShiny shine = go.GetComponentInChildren<UIShiny>();
                shine.enabled = true;

                UIParticle ps = go.GetComponentInChildren<UIParticle>();
                ps.enabled = true;
            }
        }
    }

    IEnumerator SetFirstButton()
    {
        yield return new WaitForEndOfFrame();
        if (cards.Count > 0)
        {
            transform.GetChild(0).GetComponentInChildren<MyButton>().Select();
        }
    }

    private void Update()
    {
        if (Input.GetButtonDown("Back"))
        {
            GameManager.instance.ExitDungeonCall();
        }
    }

}
