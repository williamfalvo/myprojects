﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AddComponentsTroughEditorOnce : MonoBehaviour
{
    [ContextMenu("FindButtons")]
    void AddAudioToAllButtons()
    {
        Button[] buttons;
        buttons = FindObjectsOfType<Button>();

        //Iterate root objects of type button and add the component audio script
        for (int i = 0; i < buttons.Length; ++i)
        {
            buttons[i].gameObject.AddComponent<NavigationAndSelectionAudio>();
        }
    }
}
