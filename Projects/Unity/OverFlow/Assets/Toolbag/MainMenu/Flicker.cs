using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flicker : MonoBehaviour
{
    Light light;

    public float flickerStart = 1;
    public float flickerMinTime = 1;
    public float flickerMaxTime = 3;


    public void Awake()
    {
        light = GetComponent<Light>();
        light.enabled = false;
        StartCoroutine(Loop());
    }

    IEnumerator Loop()
    {
        yield return new WaitForSeconds(1);
        for (int i = 0; i < 3; ++i)
        {
            light.enabled = false;
            yield return null;
            light.enabled = true;
            yield return null;
        }

        for (; ; )
        {
            yield return new WaitForSeconds(Random.Range(flickerMinTime, flickerMaxTime));
            int flickers = Random.Range(2, 3);
            for (int i = 0; i < flickers; ++i)
            {
                light.enabled = false;
                yield return null;
                light.enabled = true;
                yield return null;
            }
        }
    }
}
