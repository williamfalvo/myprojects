﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScaleButton : MonoBehaviour
{
    [HideInInspector]
    protected MyButton button;

    public float scale;
    public float time;

    [HideInInspector]
    protected bool isHighlighted;

    [Header("TUTORIAL INFO") , Space(10)]
    public GameObject tutorialPanel;
    public TextMeshProUGUI tutorialText;
    public string textToUpdate;

    private void Awake()
    {
        button = GetComponent<MyButton>();
    }

    private void Update()
    {
        if (tutorialPanel != null)
        {
            if (button.Highlight())
            {
                tutorialText.text = textToUpdate;
                if (GameManager.instance.tutorial.tutorialOn)
                {
                    if (!tutorialPanel.activeSelf)
                    {
                        tutorialPanel.SetActive(true);
                    }
                }
            }

            if (!GameManager.instance.tutorial.tutorialOn)
            {
                if (tutorialPanel.activeSelf)
                {
                    tutorialPanel.SetActive(false);
                }
            }
        }
        //scales up
        if (button.Highlight() && !isHighlighted)
        {
            StopAllCoroutines();
            StartCoroutine(AnimationScale(scale));
            isHighlighted = true;
        }

        //scales down
        else if(!button.Highlight() && isHighlighted)
        {
            StopAllCoroutines();
            StartCoroutine(AnimationScale(1f));
            isHighlighted = false;
        }
    }

    //scale by a value
    protected IEnumerator AnimationScale(float finalScale)
    {
        WaitForEndOfFrame eof = new WaitForEndOfFrame();
        float startValue = transform.localScale.x;
        float animationAmount = 0.0f;
        float speed = 1.0f / time;

        while (animationAmount < 1.0f)
        {
            animationAmount = Mathf.Min(1.0f, animationAmount + Time.deltaTime * speed);

            float newScale = Mathf.Lerp(startValue, finalScale, animationAmount);
            transform.localScale = new Vector3(newScale, newScale, 1);
            

            yield return eof;
        }
    }
}
