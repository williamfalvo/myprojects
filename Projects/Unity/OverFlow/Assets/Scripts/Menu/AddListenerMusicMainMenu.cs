﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddListenerMusicMainMenu : MonoBehaviour
{
    //Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(MenuMusic);
    }

    void MenuMusic()
    {
        FMODManager.instance.PlayMenuMusic();
    }
}
