﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ServerNavigation : MonoBehaviour
{
    public RectTransform hubCanvas;
    public RectTransform viewport;
    public HorizontalLayoutGroup serversFitter;
    public RectTransform serversContent;
    public Scrollbar scrollbar;
    public float leftPadding;
    public RectTransform[] servers;

    GameObject currentButton;
    GameObject lastButton;
    public float threshold;
    public float size;

    Plot[] missions;

    public GameObject dataServerPanel;
    public TextMeshProUGUI dataServerTxt;

    [HideInInspector]
    public GameObject serverButton;

    void Awake()
    {
        size = serversContent.rect.height-20;

        //set the size of the servers
        foreach(RectTransform server in servers)
        {
            server.sizeDelta = new Vector2(size*0.5f, size*0.5f);
        }
        //first server is bigger
        servers[0].sizeDelta = new Vector2(size, size);


        threshold = leftPadding;

        missions = GameManager.instance.missions;

        //set the mission for every server
        for(int i = 0; i < missions.Length; ++i)
        {
            if(i > servers.Length-1 || servers[i] == null)
            {
                break;
            }
            else
            {
                servers[i].GetComponent<ServerButton>().missionInfo = missions[i].Mission;
            }
        }
    }

    void Update()
    {
        currentButton = EventSystem.current.currentSelectedGameObject; // get the card hovered

        if (currentButton != serverButton) return;

        if(lastButton == null || lastButton != currentButton)
        {
            lastButton = currentButton;

            for(int i = 0; i<servers.Length; ++i)
            {
                if(servers[i].gameObject == serverButton)
                {
                    //set the bar value
                    scrollbar.value = ((i * size * 0.5f) / ((servers.Length + 1) * size * 0.5f + threshold)) * (serversFitter.minWidth / hubCanvas.rect.width);
                }
            }
        }

    }

}
