﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private GameObject firstButton;
    public GameObject controlsButton;
    public GameObject soundButton;
    public GameObject creditsButton;

    public GameObject newGamePanel;
    public GameObject newGameConfirmPanel;
    public GameObject loadGamePanel;
    public GameObject optionsPanel;
    public GameObject controlsPanel;
    public GameObject soundPanel;
    public GameObject creditsPanel;

    public Animation mainCamera;

    private bool onNewGame;
    private bool onNewGameConfirm;
    private bool onLoadGame;
    private bool onOptions;
    private bool onControls;
    private bool onSound;
    private bool onCredits;
    private GameObject lastSelected;
    private GameObject lastSlot;

    public bool OnNewGame { get => onNewGame; set => onNewGame = value; }
    public bool OnLoadGame { get => onLoadGame; set => onLoadGame = value; }
    public bool OnOptions { get => onOptions; set => onOptions = value; }
    public bool OnControls { get => onControls; set => onControls = value; }
    public bool OnSound { get => onSound; set => onSound = value; }
    public bool OnCredits { get => onCredits; set => onCredits = value; }
    public GameObject FirstButton { get => firstButton; set => firstButton = value; }
    public bool OnNewGameConfirm { get => onNewGameConfirm; set => onNewGameConfirm = value; }
    public GameObject LastSlot { get => lastSlot; set => lastSlot = value; }

    private void OnEnable()
    {
        StartCoroutine(EnableMenu());
    }

    IEnumerator EnableMenu()
    {
        yield return new WaitForEndOfFrame();

        EventSystem.current.SetSelectedGameObject(FirstButton);
    }

    public void SelectButton(GameObject button)
    {
        StartCoroutine(SelectButtonCo(button));
    }

    IEnumerator SelectButtonCo(GameObject button)
    {
        yield return new WaitForEndOfFrame();

        EventSystem.current.SetSelectedGameObject(button);
    }

    private void Awake()
    {
        //disable the mouse
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Start()
    {
        //FMODManager.instance.PlayMenuMusic();
    }

    private void OnDestroy()
    {
        FMODManager.instance.StopMenuMusic();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            EventSystem.current.SetSelectedGameObject(lastSelected);
        }

        if (EventSystem.current.currentSelectedGameObject != null)
        {
            lastSelected = EventSystem.current.currentSelectedGameObject;
        }

        if (!mainCamera.isPlaying)
        {
            if (Input.GetButtonDown("Cancel"))
            {
                FMODManager.instance.PlayMenuBack();
                if (OnNewGame)
                {
                    if (!onNewGameConfirm)
                    {
                        newGamePanel.SetActive(false);

                        StartCoroutine(CameraBack("NewGameAnim"));
                        OnNewGame = false;
                    }
                    else
                    {
                        newGameConfirmPanel.SetActive(false);
                        StartCoroutine(SelectButtonCo(lastSlot));
                        onNewGameConfirm = false;
                    }
                    
                }
                else if (onLoadGame)
                {
                    loadGamePanel.SetActive(false);

                    StartCoroutine(CameraBack("LoadGameAnim"));
                    onLoadGame = false;
                }
                else if (onOptions)
                {
                    optionsPanel.SetActive(false);

                    StartCoroutine(CameraBack("OptionsAnim"));
                    onOptions = false;
                }
                else if (OnControls)
                {
                    controlsPanel.SetActive(false);

                    StartCoroutine(SelectButtonCo(controlsButton));
                    OnControls = false;
                    onOptions = true;
                }
                else if (onSound)
                {
                    soundPanel.SetActive(false);

                    StartCoroutine(SelectButtonCo(controlsButton));
                    OnSound = false;
                    onOptions = true;
                }
                else if (OnCredits)
                {
                    creditsPanel.SetActive(false);

                    StartCoroutine(SelectButtonCo(controlsButton));
                    OnCredits = false;
                    onOptions = true;
                }
            }
        }        
    }

    public void ApplicationQuit()
    {
        Application.Quit();
    }

    public void LoadScene(int sceneIndex)
    {
        SceneManager.LoadScene("MenuToHub");
    }

    public void ChangePanel(GameObject nextButton)
    {
        EventSystem.current.SetSelectedGameObject(null);
        if (onNewGame)
        {
            mainCamera.Play("NewGameAnim");
        }
        else if (onLoadGame)
        {
            mainCamera.Play("LoadGameAnim");
        }
        else if (OnOptions)
        {
            mainCamera.Play("OptionsAnim");
        }
        
        StartCoroutine(CameraMove(nextButton));
    }

    IEnumerator CameraMove(GameObject button)
    {
        yield return new WaitUntil(() => !mainCamera.isPlaying);
        EventSystem.current.SetSelectedGameObject(button);
    }

    IEnumerator CameraBack(string animation)
    {
        FirstButton.SetActive(true);

        mainCamera[animation].speed = -1;
        mainCamera[animation].time = mainCamera[animation].length;
        mainCamera.Play(animation);

        yield return new WaitUntil(() => !mainCamera.isPlaying);

        EventSystem.current.SetSelectedGameObject(FirstButton);

        mainCamera[animation].speed = 1;
        mainCamera[animation].time = 0;
    }
}
