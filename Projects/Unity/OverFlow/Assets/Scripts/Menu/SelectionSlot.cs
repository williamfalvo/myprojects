﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using UnityEngine.EventSystems;

public class SelectionSlot : MonoBehaviour, ISelectHandler
{
    public GameObject[] slots;

    public void OnSelect(BaseEventData eventData)
    {
        //DataManager.instance.currentSlot += 1;
        if (eventData.selectedObject == slots[0])
        {
            DataManager.instance.currentSlot = 0;
        }

        if (eventData.selectedObject == slots[1])
        {
            DataManager.instance.currentSlot = 1;
        }

        if (eventData.selectedObject == slots[2])
        {
            DataManager.instance.currentSlot = 2;
        }
    }
}
