﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

//dungeon selection console behaviour 
public class TerminalManager : MonoBehaviour
{
    public GameObject triggerPanel;
    public GameObject mainPanel;
    public GameObject[] mainPanelButtons;
    public GameObject serverPanel;
    public GameObject serverConfirmPanel;
    public GameObject serverConfirmButton;
    public GameObject modsPanel;
    //public GameObject[] modsButton;
    public GameObject cardsPanel;
    public GameObject inventoryContent;
    public GameObject deckContent;
    public GameObject statsPanel;
    public GameObject talentTreePanel;
    public GameObject skillsPanel;
    public GameObject warningMods;
    public GameObject warningSkillPoint;
    public GameObject warningStatsPoint;
    public GameObject warningNewCards;
    public GameObject attributesPanel;
    public GameObject collectionPanel;
    public GameObject warningCollection;
    public GameObject deckFiltersPanel;
    public GameObject blackScreenFilters;
    public CollectionPanel collection;


    
    public GameObject firstButton; // first button that needs do be selected when terminal is open

    bool subPanelActive;
    bool onServer;
    bool onServerConfirm;
    bool onMods;
    bool onCollection;
    bool onTalentTree;
    bool onCards;
    bool onStats;
    
    GameObject currentPanel;
    GameObject lastButton;

    public bool SubPanelActive { get => subPanelActive; set => subPanelActive = value; }    
    public GameObject CurrentPanel { get => currentPanel; set => currentPanel = value; }
    public GameObject LastButton { get => lastButton; set => lastButton = value; }
    public bool OnServer { get => onServer; set => onServer = value; }
    public bool OnServerConfirm { get => onServerConfirm; set => onServerConfirm = value; }
    public bool OnMods { get => onMods; set => onMods = value; }
    public bool OnCollection { get => onCollection; set => onCollection = value; }
    public bool OnTalentTree { get => onTalentTree; set => onTalentTree = value; }
    public bool OnCards { get => onCards; set => onCards = value; }
    public bool OnStats { get => onStats; set => onStats = value; }

    bool left;
    bool right;
    float trigger;
    float lastTrigger;

    bool isInside = false;
    public GameObject inputImage;
    public GameObject[] screens;

    public void LoadDungeon(int scene)
    {
        SceneManager.LoadScene("HubToDungeon");
    }

    private void Start()
    {
        warningCollection.SetActive(collection.checkCollection());

        CheckModsWarning();

        foreach (TierController controller in talentTreePanel.GetComponents<TierController>())
        {
            controller.UpdateTier();
        }

        inventoryContent.GetComponent<HubInventoryController1>().gm = GameManager.instance;
        inventoryContent.GetComponent<HubInventoryController1>().Initialize();
        inventoryContent.GetComponent<HubInventoryController1>().SaveDictionary();
    }

    private void Update()
    {
        trigger = Input.GetAxis("Triggers");

        left = false;
        right = false;

        if (lastTrigger != trigger)
        {
            if (trigger == -1)
            {
                left = true;
            }
            else if (trigger == 1)
            {
                right = true;
            }
        }

        lastTrigger = trigger;

        inputImage.SetActive(isInside);

        //AFTER BETA TEST
        //manages the opening of the terminal panel
        if (isInside)
        {
            if (Input.GetButtonDown("Interact") && !triggerPanel.activeSelf && Mathf.Abs(GameManager.instance.inputManager.horizontalMove) < 0.2f )
            {
                if (subPanelActive) return;

                FMODManager.instance.PlayTerminalLogIn();
                FMODManager.instance.StopTerminalLogOut();
                //DataManager.instance.SaveData();
                //StartCoroutine(DataManager.instance.SavingDelayed());
                DataManager.instance.SaveLastSlot();
                triggerPanel.SetActive(true);
                EventSystem.current.SetSelectedGameObject(firstButton);
                GameManager.instance.inputManager.enabled = false; // deactivates the player's input
                GameManager.instance.SetPause();
            }

            if ((Input.GetButtonDown("Cancel") || Input.GetButtonDown("Dash")) && triggerPanel.activeSelf)
            {
                deckFiltersPanel.SetActive(false);
                blackScreenFilters.SetActive(false);

                if (subPanelActive)
                {
                    if (onServer)
                    {
                        if (onServerConfirm)
                        {
                            onServerConfirm = false;
                            serverConfirmPanel.SetActive(false);
                            StartCoroutine(SelectButtonCo(lastButton));
                        }
                        else
                        {
                            onServer = false;
                            subPanelActive = false;
                            serverPanel.SetActive(false);
                            mainPanel.SetActive(true);
                            FMODManager.instance.PlayMenuBack();
                            StartCoroutine(SelectButtonCo(mainPanelButtons[0]));
                        }
                        return;
                    }
                    else if (onMods)
                    {
                        if (onTalentTree)
                        {
                            onTalentTree = false;
                            talentTreePanel.SetActive(false);
                            modsPanel.SetActive(true);
                            FMODManager.instance.PlayMenuBack();
                            StartCoroutine(SelectButtonCo(lastButton));
                            DataManager.instance.SaveData();
                            StartCoroutine(DataManager.instance.SavingFeedback());
                            CheckModsWarning();
                        }
                        else if (onCards)
                        {
                            GameManager.instance.newCards = false;
                            onCards = false;
                            cardsPanel.SetActive(false);
                            modsPanel.SetActive(true);
                            FMODManager.instance.PlayMenuBack();
                            StartCoroutine(SelectButtonCo(lastButton));
                            DataManager.instance.SaveData();
                            StartCoroutine(DataManager.instance.SavingFeedback());
                            CheckModsWarning();
                        }
                        else if (onStats)
                        {
                            onStats = false;
                            statsPanel.SetActive(false);
                            modsPanel.SetActive(true);
                            FMODManager.instance.PlayMenuBack();
                            StartCoroutine(SelectButtonCo(lastButton));
                            DataManager.instance.SaveData();
                            StartCoroutine(DataManager.instance.SavingFeedback());
                        }
                        else
                        {
                            onMods = false;
                            subPanelActive = false;
                            modsPanel.SetActive(false);
                            mainPanel.SetActive(true);
                            FMODManager.instance.PlayMenuBack();
                            StartCoroutine(SelectButtonCo(mainPanelButtons[1]));
                            DataManager.instance.SaveData();
                            StartCoroutine(DataManager.instance.SavingFeedback());
                        }
                        return;
                    }
                    else if (onCollection)
                    {
                        onCollection = false;
                        subPanelActive = false;
                        collectionPanel.SetActive(false);
                        mainPanel.SetActive(true);
                        FMODManager.instance.PlayMenuBack();
                        StartCoroutine(SelectButtonCo(mainPanelButtons[2]));
                        //DataManager.instance.SaveData();
                        //StartCoroutine(DataManager.instance.SavingFeedback());
                        return;
                    }

                }
                else
                {
                    FMODManager.instance.PlayTerminalLogOut();
                    FMODManager.instance.StopTerminalLogIn();
                    triggerPanel.SetActive(false);
                    GameManager.instance.inputManager.enabled = true;
                    EventSystem.current.SetSelectedGameObject(null);
                    GameManager.instance.SetPause();
                }
            }

            if (onTalentTree)
            {
                if (left || Input.GetKeyDown(KeyCode.K))
                {
                    skillsPanel.SetActive(true);
                    attributesPanel.SetActive(false);
                }
                else if(right || Input.GetKeyDown(KeyCode.L))
                {
                    skillsPanel.SetActive(false);
                    attributesPanel.SetActive(true);
                }
            }

            if (OnCards)
            {
                HubInventoryController1 hub = inventoryContent.GetComponent<HubInventoryController1>();

                
                if ((left || Input.GetKeyDown(KeyCode.K)) && inventoryContent.transform.childCount > 0 && !blackScreenFilters.activeSelf && !hub.OnInventory)
                {
                    hub.lastDeck = EventSystem.current.currentSelectedGameObject;
                    if(hub.lastCard == null)
                    {
                        inventoryContent.transform.GetChild(0).GetComponentInChildren<MyButton>().Select();
                    }
                    else
                    {
                        EventSystem.current.SetSelectedGameObject(hub.lastCard);
                    }
                    
                    hub.OnInventory = true;                    
                }
                else if ((right || Input.GetKeyDown(KeyCode.L)) && deckContent.transform.childCount > 0 && !blackScreenFilters.activeSelf && hub.OnInventory)
                {
                    deckFiltersPanel.SetActive(false);
                    blackScreenFilters.SetActive(false);

                    hub.lastCard = EventSystem.current.currentSelectedGameObject;
                    if (hub.lastDeck == null)
                    {
                        deckContent.transform.GetChild(0).GetComponentInChildren<MyButton>().Select();
                    }
                    else
                    {
                        EventSystem.current.SetSelectedGameObject(hub.lastDeck);
                    }
                    
                    hub.OnInventory = false;
                    
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInside = true;
            foreach(var go in screens)
            {
                go.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInside = false;
            foreach (var go in screens)
            {
                go.SetActive(false);
            }
        }
    }
    

    IEnumerator SelectButtonCo(GameObject button)
    {
        yield return new WaitForEndOfFrame();

        EventSystem.current.SetSelectedGameObject(button);
    }

    public void OpenCardsPanel()
    {
        if (inventoryContent.transform.childCount > 0)
        {
            HubInventoryController1 hub = inventoryContent.GetComponent<HubInventoryController1>();
            hub.OnInventory = true;

            MyButton b = inventoryContent.transform.GetChild(0).GetComponentInChildren<MyButton>();
            StartCoroutine(SelectButtonCo(b.gameObject));
        }
    }

    public void CheckModsWarning()
    {
        bool haveNewCards = GameManager.instance.newCards;
        warningNewCards.SetActive(haveNewCards);

        bool haveSkillPoints = GameManager.instance.player.skillPoints > 0;
        warningSkillPoint.SetActive(haveSkillPoints);

        bool haveStatsPoints = GameManager.instance.player.attributePoints > 0;
        warningStatsPoint.SetActive(haveStatsPoints);

        warningMods.SetActive(haveSkillPoints || haveNewCards || haveStatsPoints);
    }

    public void CheckServerStatus(ServerButton button)
    {
        if (button.isInteractable)
        {
            serverConfirmPanel.SetActive(true);
            EventSystem.current.SetSelectedGameObject(serverConfirmButton);
            onServerConfirm = true;
            lastButton = button.gameObject;
        }
        

    }
}
