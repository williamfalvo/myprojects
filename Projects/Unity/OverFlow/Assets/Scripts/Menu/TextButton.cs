﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


//our button version , but it returns if the button is highlighted
public class TextButton : MonoBehaviour
{
    MyButton button;
    public string pathText;
    public Text textbutton;
    public TextMeshProUGUI path;

    void Start()
    {
        button = GetComponent<MyButton>();
    }

    void Update()
    {
        if (button.Highlight())
        {
            path.text = pathText;
            if(textbutton != null)
            textbutton.enabled = true;
        }
        else
        {
            if (textbutton != null)
            textbutton.enabled = false;
        }
    }
}
