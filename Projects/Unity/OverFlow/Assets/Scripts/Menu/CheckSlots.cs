﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System;
using UnityEngine.EventSystems;

public class CheckSlots : MonoBehaviour
{
    //Save and load screen UI slot1
    public GameObject[] playerLevelText1;
    public GameObject[] gameTimeText1;
    public GameObject[] completionText1;
    public GameObject[] emptySlotText1;

    //Save and load screen UI slot2
    public GameObject[] playerLevelText2;
    public GameObject[] gameTimeText2;
    public GameObject[] completionText2;
    public GameObject[] emptySlotText2;

    //Save and load screen UI slot3
    public GameObject[] playerLevelText3;
    public GameObject[] gameTimeText3;
    public GameObject[] completionText3;
    public GameObject[] emptySlotText3;

    public GameObject confirmOverwrite;
    public GameObject noButton;
    
    private void Start()
    {
        //DataManager.instance.LoadLastSlot();
        for (int i = 0; i < DataManager.instance.dataSaved.Length; i++)
        {
            DataManager.instance.currentSlot = i;
            DataManager.instance.LoadData();

            if (DataManager.instance.currentSlot == 0 && DataManager.instance.dataSaved[0].gTF >= 1f)
            {
                foreach (GameObject pl in playerLevelText1)
                {
                    pl.SetActive(true);
                    pl.GetComponent<TextMeshProUGUI>().text = "[PG Level: " + DataManager.instance.dataSaved[0].level.ToString() + "]";
                }
                //playerLevelText[0].SetActive(true);
                //playerLevelText[0].GetComponent<TextMeshProUGUI>().text = "[PG Level: " + GameManager.instance.player.level.ToString() + "]";
                foreach (GameObject gt in gameTimeText1)
                {
                    gt.SetActive(true);
                    gt.GetComponent<TextMeshProUGUI>().text = "[" + DataManager.instance.dataSaved[0].gTS + " H]";
                }
                //gameTimeText[0].SetActive(true);
                //gameTimeText.GetComponent<TextMeshProUGUI>().text = "[" /*+ dataSaved.gameTimeToSave*/ + " H]";
                foreach (GameObject c in completionText1)
                {
                    c.SetActive(true);
                    c.GetComponent<TextMeshProUGUI>().text = "[Completion:" + DataManager.instance.dataSaved[0].gCompletion.ToString() + "%]";
                }
                //completionText.SetActive(true);
                foreach (GameObject es in emptySlotText1)
                {
                    es.SetActive(false);
                }
                //emptySlotText.SetActive(false);
            }
            //else
            //{
            //    foreach (GameObject es in emptySlotText1)
            //    {
            //        es.SetActive(true);
            //    }
            //    //emptySlotText.SetActive(true);
            //    //return;
            //}

            if (DataManager.instance.currentSlot == 1 && DataManager.instance.dataSaved[1].gTF >= 1f)
            {
                foreach (GameObject pl in playerLevelText2)
                {
                    pl.SetActive(true);
                    pl.GetComponent<TextMeshProUGUI>().text = "[PG Level: " + DataManager.instance.dataSaved[1].level.ToString() + "]";
                }
                //playerLevelText[0].SetActive(true);
                //playerLevelText[0].GetComponent<TextMeshProUGUI>().text = "[PG Level: " + GameManager.instance.player.level.ToString() + "]";
                foreach (GameObject gt in gameTimeText2)
                {
                    gt.SetActive(true);
                    gt.GetComponent<TextMeshProUGUI>().text = "[" + DataManager.instance.dataSaved[1].gTS + " H]";
                }
                //gameTimeText[0].SetActive(true);
                //gameTimeText.GetComponent<TextMeshProUGUI>().text = "[" /*+ dataSaved.gameTimeToSave*/ + " H]";
                foreach (GameObject c in completionText2)
                {
                    c.SetActive(true);
                    c.GetComponent<TextMeshProUGUI>().text = "[Completion:" + DataManager.instance.dataSaved[1].gCompletion.ToString() + "%]";
                }
                //completionText.SetActive(true);
                foreach (GameObject es in emptySlotText2)
                {
                    es.SetActive(false);
                }
                //emptySlotText.SetActive(false);
            }
            //else
            //{
            //    foreach (GameObject es in emptySlotText2)
            //    {
            //        es.SetActive(true);
            //    }
            //    //emptySlotText.SetActive(true);
            //    //return;
            //}

            if (DataManager.instance.currentSlot == 2 && DataManager.instance.dataSaved[2].gTF >= 1f)
            {
                foreach (GameObject pl in playerLevelText3)
                {
                    pl.SetActive(true);
                    pl.GetComponent<TextMeshProUGUI>().text = "[PG Level: " + DataManager.instance.dataSaved[2].level.ToString() + "]";
                }
                //playerLevelText[0].SetActive(true);
                //playerLevelText[0].GetComponent<TextMeshProUGUI>().text = "[PG Level: " + GameManager.instance.player.level.ToString() + "]";
                foreach (GameObject gt in gameTimeText3)
                {
                    gt.SetActive(true);
                    gt.GetComponent<TextMeshProUGUI>().text = "[" + DataManager.instance.dataSaved[2].gTS + " H]";
                }
                //gameTimeText[0].SetActive(true);
                //gameTimeText.GetComponent<TextMeshProUGUI>().text = "[" /*+ dataSaved.gameTimeToSave*/ + " H]";
                foreach (GameObject c in completionText3)
                {
                    c.SetActive(true);
                    c.GetComponent<TextMeshProUGUI>().text = "[Completion:" + DataManager.instance.dataSaved[2].gCompletion.ToString() + "%]";
                }
                //completionText.SetActive(true);
                foreach (GameObject es in emptySlotText3)
                {
                    es.SetActive(false);
                }
                //emptySlotText.SetActive(false);
            }
            //else
            //{
            //    foreach (GameObject es in emptySlotText3)
            //    {
            //        es.SetActive(true);
            //    }
            //    //emptySlotText.SetActive(true);
            //    //return;
            //}
        }
        DataManager.instance.LoadLastSlot();
        DataManager.instance.currentSlot = DataManager.instance.lastSlotSelected.lastSS;

    }
    
    public void OnNewGame(int slotNumber)
    {
        DataManager.instance.currentSlot = slotNumber;
        //DataManager.instance.whatSlot = slotNumber;

        //First Slot
        if (DataManager.instance.currentSlot == 0 && DataManager.instance.dataSaved[0].gTF < 1f)
        {
            foreach (GameObject es in emptySlotText1)
            {
                es.SetActive(true);
            }
            //emptySlotText.SetActive(true);
            SceneManager.LoadScene("MenuToHub");
        }

        if(DataManager.instance.currentSlot == 0 && DataManager.instance.dataSaved[0].gTF >= 1f)
        {
            //EnableDataSlotSaveLoad1();
            confirmOverwrite.SetActive(true);
            EventSystem.current.SetSelectedGameObject(noButton);
        }

        //Second Slot
        if (DataManager.instance.currentSlot == 1 && DataManager.instance.dataSaved[1].gTF < 1f)
        {
            foreach (GameObject es in emptySlotText2)
            {
                es.SetActive(true);
            }
            //emptySlotText.SetActive(true);
            SceneManager.LoadScene("MenuToHub");
        }

        if (DataManager.instance.currentSlot == 1 && DataManager.instance.dataSaved[1].gTF >= 1f)
        {
            //EnableDataSlotSaveLoad1();
            confirmOverwrite.SetActive(true);
            EventSystem.current.SetSelectedGameObject(noButton);
        }

        //Third slot
        if (DataManager.instance.currentSlot == 2 && DataManager.instance.dataSaved[2].gTF < 1f)
        {
            foreach (GameObject es in emptySlotText3)
            {
                es.SetActive(true);
            }
            //emptySlotText.SetActive(true);
            SceneManager.LoadScene("MenuToHub");
        }

        if (DataManager.instance.currentSlot == 2 && DataManager.instance.dataSaved[2].gTF >= 1f)
        {
            //EnableDataSlotSaveLoad1();
            confirmOverwrite.SetActive(true);
            EventSystem.current.SetSelectedGameObject(noButton);
        }
    }

    public void OnLoadGame(int slotNumber)
    {
        DataManager.instance.currentSlot = slotNumber;
        //First Slot
        if (DataManager.instance.currentSlot == 0 && DataManager.instance.dataSaved[0].gTF >= 1f)
        {
            //DataManager.instance.LoadData();
            SceneManager.LoadScene("MenuToHub");
        }

        //Second slot
        else if (DataManager.instance.currentSlot == 1 && DataManager.instance.dataSaved[1].gTF >= 1f)
        {
            //DataManager.instance.LoadData();
            SceneManager.LoadScene("MenuToHub");
        }
        
        //Third slot
        else if (DataManager.instance.currentSlot == 2 && DataManager.instance.dataSaved[2].gTF >= 1f)
        {
            //DataManager.instance.LoadData();
            SceneManager.LoadScene("MenuToHub");
        }
        else
        {
            return;
        }
        //DataManager.instance.whatSlot = DataManager.instance.currentSlot;
    }

    public void OnContinueGame()
    {
        if(DataManager.instance.dataSaved[0].gTF >= 1f)
        {
            SceneManager.LoadScene("MenuToHub");
        }

        else if(DataManager.instance.dataSaved[1].gTF >= 1f)
        {
            SceneManager.LoadScene("MenuToHub");
        }

        else if(DataManager.instance.dataSaved[2].gTF >= 1f)
        {
            SceneManager.LoadScene("MenuToHub");
        }

        else
        {
            return;
        }
    }
    
    public void ResetData()
    {
        DataManager.instance.dataSaved[DataManager.instance.currentSlot] = new DataToSave();
        //if (DataManager.instance.currentSlot == 0)
        //{
        //    DataManager.instance.dataSaved[0] = new DataToSave();
        //}

        //if (DataManager.instance.currentSlot == 1)
        //{
        //    DataManager.instance.dataSaved[1] = new DataToSave();
        //}

        //if (DataManager.instance.currentSlot == 2)
        //{
        //    DataManager.instance.dataSaved[2] = new DataToSave();
        //}
    }
}
