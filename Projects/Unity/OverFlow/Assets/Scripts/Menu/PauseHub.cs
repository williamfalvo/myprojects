﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using FMODUnity;

public class PauseHub : MonoBehaviour
{
    public GameObject pausePanel;
    public GameObject pauseButton;
    public GameObject optionsPanel;
    public GameObject optionsButton;
    public GameObject chipSet;
    public GameObject firstPanel;

    public ScenesManager sceneManager;

    GameManager gameManager;
    private GameObject currentPanel;
    private GameObject lastButton;
    private GameObject lastPanel;
    private bool subPanelActive = false;
    private bool onControls;
    private bool onSound;
    private bool onConfirmExit;

    FMOD.Studio.Bus sfxBusToReset;

    PlayerController player;

    public bool SubPanelActive { get => subPanelActive; set => subPanelActive = value; }
    public bool OnControls { get => onControls; set => onControls = value; }
    public bool OnSound { get => onSound; set => onSound = value; }
    public bool OnConfirmExit { get => onConfirmExit; set => onConfirmExit = value; }
    public GameObject CurrentPanel { get => currentPanel; set => currentPanel = value; }
    public GameObject LastButton { get => lastButton; set => lastButton = value; }
    public GameObject LastPanel { get => lastPanel; set => lastPanel = value; }

    private void Start()
    {
        FMODManager.instance.StopPlayerLowHp();
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Hub"))
        {
            sfxBusToReset = RuntimeManager.GetBus("bus:/SFX");
            sfxBusToReset.setVolume(FMODManager.instance.slidersVolumeSFX);
        }

        gameManager = GameManager.instance;
    }

    // Update is called once per frame
    void Update()
    {

        if (!subPanelActive && gameManager.pausable && Input.GetButtonDown("Pause") )
        {
            if (chipSet != null)
            {
                if (!chipSet.activeSelf)
                {
                    PauseManager();
                }
            }
            else
            {
                PauseManager();
            }
        }

        if(subPanelActive && Input.GetButtonDown("Back"))
        {
            FMODManager.instance.PlayMenuBack();
            if (!onControls && !onSound && !onConfirmExit)
            {
                firstPanel.SetActive(true);
                optionsPanel.SetActive(false);
                EventSystem.current.SetSelectedGameObject(optionsButton);
                subPanelActive = false;
            }
            else
            {
                lastPanel.SetActive(true);
                EventSystem.current.SetSelectedGameObject(LastButton);
                currentPanel.SetActive(false);
            }

            if (onControls) onControls = false;
            if (onSound) onSound = false;
            if (onConfirmExit)
            {
                onConfirmExit = false;
                subPanelActive = false;
            }

        }
    }

    public void ReturnToHub()
    {
        //Reset time scale
        Time.timeScale = 1;
        
        //When player want to return to hub scene, we have to call the dead function
        CharacterStatistics playerStat = gameManager.player;
        float damage = playerStat.currentHealth;
        playerStat.GetComponent<PlayerController>().TakeDamage(damage,false);
    }

    public void OpenPanel()
    {
        gameManager.pausable = false;
    }

    public void ClosePanel()
    {
        gameManager.pausable = true;
        gameManager.inputManager.enabled = true;
    }

    public void PauseManager()
    {
        //if the pause panel is already open or not
        if (pausePanel.activeSelf)
        {
            Time.timeScale = 1;
            gameManager.inputManager.enabled = true;
            gameManager.pausable = true;
            sceneManager.PanelManager(pausePanel);
            EventSystem.current.SetSelectedGameObject(null);
        }
        else
        {
            sceneManager.PanelManager(pausePanel);
            gameManager.inputManager.enabled = false;
            gameManager.pausable = false;
            Time.timeScale = 0;
            EventSystem.current.SetSelectedGameObject(pauseButton);
        }
    }

    public void SetButton(GameObject button)
    {
        StartCoroutine(SetButtonCo(button));
    }

    IEnumerator SetButtonCo(GameObject button)
    {
        yield return new WaitForEndOfFrame();

        EventSystem.current.SetSelectedGameObject(button);
    }
}
