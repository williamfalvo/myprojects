using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerManager : MonoBehaviour
{
    public RoomEnemiesCount roomEnemiesCounter;

    List<Card> cardToRemove = new List<Card>();

    public delegate void CardUsed(Card c);
    public event CardUsed OnCardUsed;

    public List<Card> CardToRemove
    {
        get { return cardToRemove; }
        set { cardToRemove = value; }
    }

    bool resourceFound = false;

    // activate the effects based on trigger (enemy is passed for special effects that needs a target)
    public void ActivateEffect(Trigger.Triggers tr, Enemy en)
    {
        resourceFound = false;

        if (tr == Trigger.Triggers.ChunkEnter)
        {
            roomEnemiesCounter.UpdateEnemiesGUI();
        }
        int cardsToRemove = 0;
        foreach (Card c in GameManager.instance.inGameInventory.cards)
        {
            //check the trigger(0) (the trigger that the effect will use)
            Trigger trg = c.triggers[0];

            if (trg.trigger == tr && !trg.effectUsed)
            {
                if (c.cardLife <= 0)
                {
                    cardToRemove.Add(c);
                    cardsToRemove++;
                }
                else
                {
                    //if its not a resource we use the effect
                    if (c.cardArchetypes[0] != Card.Archetypes.Consumable)
                    {
                        foreach (Effect effect in trg.effects)
                        {
                            effect.enemy = en;
                            effect.Use();
                        }

                        if (trg.trigger == Trigger.Triggers.Equipped)
                        {
                            trg.effectUsed = true; //to check the effect is used only once for equipped
                        }
                    }
                    else
                    {
                        resourceFound = true;

                        foreach (Effect effect in trg.effects)
                        {
                            effect.enemy = en;
                            effect.Use();
                        }

                        FMODManager.instance.ItemUsed();                        

                        //life of the card
                        c.cardLife--;

                        CardConsumed(c);

                        //OnCardUsed?.Invoke(ScriptableObject.CreateInstance<Card>());

                        if (c.cardLife <= 0)
                        {
                            cardToRemove.Add(c);
                            cardsToRemove++;
                        }
                    }
                }
            }
        }

        if ((tr == Trigger.Triggers.Consumable1 || tr == Trigger.Triggers.Consumable2) && resourceFound == false)
        {
            FMODManager.instance.EmptyItem();
        }

        //we do this check so it doesnt loop when card is removed by an effect
        if (cardsToRemove > 0)
        {
            RemoveCards();
        }        

        cardsToRemove = 0;

        // checks if the trigger procs any skill
        foreach (Skill s in GameManager.instance.EquippedSkills)
        {
            Trigger trg = s.trigger;

            if (trg.trigger == tr && !trg.effectUsed)
            {
                foreach (Effect effect in trg.effects)
                {
                    effect.Use();
                }

                if (trg.trigger == Trigger.Triggers.Equipped)
                {
                    trg.effectUsed = true;
                }
            }
        }
    }

    public void RemoveCards()
    {
        // remove all the cards that expired their life
        foreach (Card c in cardToRemove)
        {
            GameManager.instance.graveyard.AddCard(c);
            GameManager.instance.inGameInventory.RemoveCard(c);

            c.triggers[0].effectUsed = false;

            foreach (Effect effect in c.triggers[0].effects)
            {
                if (effect.hasBeenUsed)
                {
                    effect.Discard();
                }
            }

            GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.CardSentToGraveyard, null);
        }

        cardToRemove.Clear();
    }

    public void CardConsumed(Card c)
    {
        OnCardUsed?.Invoke(c);
    }
}
