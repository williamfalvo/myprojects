﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//class that holds the targets that needs to be known by the effects
public class TargetManager : MonoBehaviour
{
    public DeathController deathController;
    public DungeonCreation dungeon;
    public FinishBehaviour finishBehaviour;
    public StatisticsManagerUI statisticsManager;
    public GameObject secondLifeBar;
    public GameObject weaponTrail;

    MotionTrail trail;

    void Awake()
    {
        CharacterStatistics = GameManager.instance.player;

        PlayerController = GameManager.instance.player.GetComponent<PlayerController>();

        Player = GameManager.instance.p;

        GameObject g = Instantiate(weaponTrail, Player.transform.position, Quaternion.identity);
        trail = g.GetComponent<MotionTrail>();
        trail.player = PlayerController;
    }

    private void Update()
    {
        trail.TargetSkinMesh = PlayerController.currentWeapon;
    }

    void Start()
    {
        Deck = GameManager.instance.deck;

        Hand = GameManager.instance.hand;

        Graveyard = GameManager.instance.graveyard;        
    }

    #region Properties

    public GameObject Player { get; set; }

    public CharacterStatistics CharacterStatistics { get; private set; }

    public PlayerController PlayerController { get; private set; }

    public CardInventory Deck { get; private set; }

    public CardInventory Hand { get; private set; }

    public CardInventory Graveyard { get; private set; }

    #endregion

    //returns all the enemies in the chunk
    public List<Enemy> GetAllEnemies()
    {
        foreach (RoomBehaviour room in dungeon.currentDungeonRooms)
        {
            if (room.chunkStarted)
            {
                return room.enemies;
            }
        }

        return null;
    }
}
