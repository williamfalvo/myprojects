﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exercise : MonoBehaviour
{
    List<List<int>> schermo = new List<List<int>>();

    public int height; // altezza schermo
    public int width; // larghezza schermo

    List<List<int>> sprite = new List<List<int>>();

    public int ns; // altezza sprite
    public int ms; // larghezza sprite

    public int xCoord;
    public int yCoord;

    public List<List<int>> CreateScreen(int h, int w)
    {
        for (int i = 0; i < h; i++)
        {
            List<int> row = new List<int>();

            for (int j = 0; j < w; j++)
            {
                row.Add(1);
            }

            schermo.Add(row);
        }

        return schermo;
    }

    public List<List<int>> CreateSprite(int n, int m)
    {
        if (n <= height && m <= width)
        {
            for (int i = 0; i < n; i++)
            {
                List<int> row = new List<int>();

                for (int j = 0; j < m; j++)
                {
                    row.Add(0);
                }

                sprite.Add(row);
            }
        }

        return sprite;
    }    

    public List<List<int>> AddSprite(int x, int y)
    {
        if (x < width && y < height)
        {
            if (x + (ms - 1) < width && y + (ns - 1) <= height)
            {
                for (int i = 0; i < ns; i++)
                {
                    List<int> screenRow = schermo[y];
                    List<int> spriteRow = sprite[i];

                    for (int j = 0; j < ms; j++)
                    {
                        screenRow[x] = spriteRow[j];
                        x++;
                    }

                    schermo[y] = screenRow;

                    y++;
                }

                return schermo;
            }

            return null;
        }

        return null;
    }

    private void Start()
    {
        CreateScreen(height, width);

        CreateSprite(ns, ms);

        AddSprite(xCoord, yCoord);
    }
}
