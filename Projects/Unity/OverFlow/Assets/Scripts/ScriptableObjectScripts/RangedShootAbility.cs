﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Active Skills Effects/Ranged Shoot")]
public class RangedShootAbility : ActiveSkillEffect
{
    public override void Discard()
    {
        target = FindObjectOfType<TargetManager>();

        RangedShoot shoot = target.Player.GetComponentInChildren<RangedShoot>();

        if (shoot == null) return;

        shoot.canDo = false;
    }

    //the skill is already setupped so the spawn of the bullet is managed by RangedShoot
    // script , it will listen the input during the update
}
