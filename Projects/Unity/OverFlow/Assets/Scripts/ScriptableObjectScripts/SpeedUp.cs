﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Carta o Abilità

[CreateAssetMenu(menuName = "Card Effects/SpeedUp")]
public class SpeedUp : Effect
{
    // player's speed modifier
    public float speedModifier;    

    public int counter = 0;

    float unclampedSpeed;

    public override void Use()
    {
        // the effect takes its target from Target Manager
        target = FindObjectOfType<TargetManager>();

        hasBeenUsed = true;        

        // if the effect has no duration, it will last untill its discard
        if (duration == 0)
        {
            Permanent(target.PlayerController);            
        }
        // if is temporary a coroutine starts and when it ends, the efect will be removed
        else
        {
            GameManager.instance.StartCoroutine(Temporary(target.PlayerController));
        }
    }

    public override void Discard()
    {
        if (hasBeenUsed)
        {
            // the effect takes its target from Target Manager and if it's null get out of the method
            PlayerController player = target.PlayerController;
            if (player == null) return;

            player.modifiedSpeed -= delta;

            player.speed -= delta;

            delta = 0;
            hasBeenUsed = false;
        }        
    }

    float delta = 0f;

    // increase player's movement speed
    public void Permanent(PlayerController player)
    {
        unclampedSpeed = player.modifiedSpeed;
        player.modifiedSpeed = Mathf.Clamp(player.modifiedSpeed + speedModifier, player.minSpeed, player.maxSpeed);
        delta += (player.modifiedSpeed - unclampedSpeed);

        player.speed = player.modifiedSpeed;
    }

    IEnumerator Temporary(PlayerController p)
    {
        Permanent(p);

        yield return new WaitForSeconds(duration);

        Discard();
    }
}
