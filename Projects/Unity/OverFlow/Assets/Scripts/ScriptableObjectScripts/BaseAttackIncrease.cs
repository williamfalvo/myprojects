﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Base Attack Temporary Increase")]
public class BaseAttackIncrease : Effect
{
    public float t;
    public float perc;

    public override void Discard()
    {
        if(target == null)
        {
            target = FindObjectOfType<TargetManager>();
        }

        PlayerController player = target.PlayerController;

        if (player == null) return;

        //WeaponClass wp = player.currentWeapon.transform.GetChild(0).GetComponent<WeaponClass>();
        WeaponClass wp = player.weapons[player.indexWeapon].collider.GetComponent<WeaponClass>();

        wp.Atk = wp.baseAtk;
    }

    //increase the dmg of the weapon by a percentage
    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        GameManager.instance.StartCoroutine(AttackIncrease(target.PlayerController));
    }

    public IEnumerator AttackIncrease(PlayerController p)
    {
        WeaponClass wp = p.weapons[p.indexWeapon].collider.GetComponent<WeaponClass>();

        float atk = wp.Atk;

        wp.Atk += (wp.Atk / 100) * perc;

        yield return new WaitForSeconds(t);

        wp.Atk = atk;        
    }
}
