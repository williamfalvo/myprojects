﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Active Skills Effects/Shockwave")]
public class ShockwaveAbility : ActiveSkillEffect
{
    public override void Discard()
    {
        target = FindObjectOfType<TargetManager>();

        //activates the child of the player - shockwave
        Shockwave shockwave = target.Player.GetComponentInChildren<Shockwave>();

        if (shockwave == null) return;

        shockwave.canDo = false;
    }

    public override void Use()
    {
        
    }
}
