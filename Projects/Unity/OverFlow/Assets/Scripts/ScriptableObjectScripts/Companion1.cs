﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Companion1 : MonoBehaviour
{
    public GameObject player;
    public float targetDistance;
    public float allowedDistance = 5f;
    public GameObject npc;
    public float speed;
    public RaycastHit raycast;

    public float magnitude;
    public float frequency;

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(player.transform);

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out raycast))
        {
            targetDistance = raycast.distance;

            if (targetDistance >= allowedDistance)
            {
                speed = 4f;

                //transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed);

                //transform.position = Vector3.LerpUnclamped(transform.position, player.transform.position, speed);

                transform.Translate((Vector3.forward * Time.deltaTime * speed));
            }
            else
            {
                speed = 0f;

                transform.Translate(transform.up * Mathf.Sin(Time.time * frequency) * magnitude);
            }
        }
    }
}
