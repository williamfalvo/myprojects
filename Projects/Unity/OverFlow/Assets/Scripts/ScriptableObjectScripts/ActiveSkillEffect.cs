﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSkillEffect : Effect
{
    public float cooldown;

    public int maxShots;

    public float totalDuration;

    public float resetDuration;

    public float radius;

    public float power;

    public float baseAtk;

    public override void Discard()
    {
        
    }

    public override void Use()
    {
        
    }
}
