﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Abilità

public class MoreDeckCards : Effect
{
    // increase value
    public int deckCardsIncrease;

    public override void Use()
    {
        // increase the maximum number of cards the deck can have
        GameManager.instance.deck.maxCards += deckCardsIncrease;
    }

    public override void Discard()
    {
        GameManager.instance.deck.maxCards -= deckCardsIncrease;
    }  
}
