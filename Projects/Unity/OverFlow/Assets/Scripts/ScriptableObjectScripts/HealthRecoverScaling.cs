﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Health Scaling On Tech")]
public class HealthRecoverScaling : Effect
{
    public float baseHp;

    public override void Discard()
    {
        
    }

    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        HealthRecover(target.CharacterStatistics);
    }

    public void HealthRecover(CharacterStatistics pl)
    {
        float hpAmount = baseHp * pl.techBabValues[(int)(pl.baseIntellect + pl.bonusIntellect)].scaling;

        pl.currentHealth += hpAmount;
    }
}
