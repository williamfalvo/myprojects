﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/On Hit Health Up Permanent")]
public class OnHitHealthUpPermanent : Effect
{
    public float perc;

    public override void Discard()
    {
        
    }

    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        HealthIncrease(target.CharacterStatistics);
    }    

    public void HealthIncrease(CharacterStatistics player)
    {
        if (enemy != null)
        {
            //review why we get the enemy ?
            //shouldnt register in the awake to the hit ?

            EnemyController en = enemy.gameObject.GetComponent<EnemyController>();

            //heals the player once
            player.currentHealth += (en.Dmg / 100f) * perc;
        }
    }
}
