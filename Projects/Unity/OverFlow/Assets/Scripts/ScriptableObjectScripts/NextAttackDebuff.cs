﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Next Attack Debuff")]
public class NextAttackDebuff : Effect
{
    public BuffDebuff debuff;

    public bool onlyOnce;

    bool active = false;   

    public override void Discard()
    {
        active = false;

        GameManager.instance.OnHit -= ApplyDebuff;
    }

    public override void Use()
    {
        GameManager.instance.OnHit += ApplyDebuff;

        active = true;
    }

    //apply the desired debuff on next attack
    public void ApplyDebuff(GameObject en, CharacterStatistics st)
    {
        if (active && st.currentHealth > 0)
        {
            st.DebuffAdd(new BuffDebuff(debuff));

            if (onlyOnce)
            {
                active = false;
            }            
        }                       
    }    
}
