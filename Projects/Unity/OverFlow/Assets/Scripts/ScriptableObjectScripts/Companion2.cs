﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Companion2 : MonoBehaviour
{
    public enum State { Idle, Follow, Attack }

    State currentState;
    float targetDistance;
    Quaternion startAngle;
    Quaternion stepAngle;
    Enemy target;
    float currentCooldown;
    Rigidbody rb;
    Vector3 spawn;

    public GameObject player;
    public GameObject bullet;
    public float frequency;
    public float magnitude;
    public float speed;
    public float allowedDistance;
    public float startAngleValue = 60f;
    public float stepAngleValue = 5f;
    public float attackRange;
    public float bulletSpeed;
    public float cooldown;
    public float offset;
    public float val;

    private void Start()
    {
        startAngle = Quaternion.AngleAxis(-startAngleValue, Vector3.up);
        stepAngle = Quaternion.AngleAxis(stepAngleValue, Vector3.up);
        rb = GetComponent<Rigidbody>();
        spawn = transform.position;
        currentCooldown = cooldown;
    }

    void Update()
    {
        switch (currentState)
        {
            //flies arround the player
            case State.Idle:

                transform.LookAt(player.transform.position + new Vector3(0f, offset, 0f));
                transform.Translate(transform.up * Mathf.Sin(Time.time * frequency) * magnitude);

                CheckPlayerDistance();

                var targetToAggro = CheckForAggro();

                if (targetToAggro != null)
                {
                    target = targetToAggro.GetComponent<Enemy>();

                    if (target != null)
                    {
                        if (Vector3.Distance(transform.position, target.transform.position + new Vector3(0f, 1f, 0f)) < attackRange)
                        {
                            currentState = State.Attack;
                        }
                    }                    
                }

                break;

                //moves toward the player
            case State.Follow:

                CheckHeight();

                transform.LookAt(player.transform.position + new Vector3(0f, offset, 0f));
                rb.MovePosition(transform.position + transform.forward * Time.deltaTime * speed);                

                CheckPlayerDistance();                                

                var aggro = CheckForAggro();

                if (aggro != null)
                {
                    target = aggro.GetComponent<Enemy>();

                    if (target != null)
                    {
                        if (Vector3.Distance(transform.position, target.transform.position + new Vector3(0f, 0.5f, 0f)) < attackRange)
                        {
                            currentState = State.Attack;
                        }
                    }
                }

                break;
                
                //attack the enemy
            case State.Attack:

                if (target != null)
                {

                    if (currentCooldown >= cooldown)
                    {
                        Shoot(target.gameObject.transform);

                        currentCooldown = 0f;
                    }

                    currentCooldown += Time.deltaTime;
                }
                else
                {
                    currentState = State.Idle;

                    currentCooldown = 0f;
                }

                break;
        }
    }

    public void Shoot(Transform target)
    {
        GameObject b = Instantiate(bullet, transform.position, Quaternion.identity);

        CapsuleCollider capsule = target.GetComponent<CapsuleCollider>();               

        Vector3 dir = (new Vector3(target.position.x, target.position.y + capsule.height * 0.6f, target.position.z) - transform.position).normalized;

        Rigidbody rb = b.GetComponent<Rigidbody>();

        rb.velocity = dir * bulletSpeed;

        b.transform.LookAt(target);
    }

    //state follow (moves toward player) if the player is too far away , otherwise idle
    public void CheckPlayerDistance()
    {
        targetDistance = (player.transform.position - transform.position).sqrMagnitude;

        if (targetDistance >= allowedDistance * allowedDistance)
        {
            speed = 8f;

            currentState = State.Follow;
        }
        else
        {
            speed = 0f;

            currentState = State.Idle;
        }
    }

    Quaternion quat = Quaternion.AngleAxis(6f, Vector3.right);

    public Transform CheckForAggro()
    {        
        float aggroRadius = 5f;
        int layermask = 1 << 10; //Enemy layer
        var angle = transform.rotation * startAngle * quat;
        var direction = angle * Vector3.forward;
        var pos = transform.position;

        for (var i = 0; i < 24; i++)
        {
            if (Physics.Raycast(pos, direction, out RaycastHit hit, Mathf.Infinity, layermask))
            {
                var en = hit.collider.GetComponent<Enemy>();

                if (en != null)
                {
                    Debug.DrawRay(pos, direction * hit.distance, Color.red);
                    return en.transform;
                }
                else
                {
                    Debug.DrawRay(pos, direction * aggroRadius, Color.green);
                }
            }
            else
            {
                Debug.DrawRay(pos, direction * aggroRadius, Color.yellow);
            }

            direction = stepAngle * direction;
        }

        return null;
    }

    public void CheckHeight()
    {
        float height = spawn.y - transform.position.y;

        if (Mathf.Abs(height) > 0.7f)
        {
            rb.MovePosition(transform.position + Vector3.up * Mathf.Sign(height) * Time.deltaTime * speed);
        }        
        else
        {
            currentState = State.Idle;
        }     
    }
}
