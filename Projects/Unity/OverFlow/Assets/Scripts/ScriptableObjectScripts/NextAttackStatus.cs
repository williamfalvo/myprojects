﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Next Attack Status")]
public class NextAttackStatus : Effect
{
    public Enemy.EnemyState status;

    public float effectDur;

    bool active = false;

    public bool onlyOnce;

    public override void Discard()
    {
        active = false;
    }

    public override void Use()
    {
        GameManager.instance.OnHit += ApplyStatus;

        active = true;
    }

    //apply the desired status
    public void ApplyStatus(GameObject en, CharacterStatistics character)
    {
        if(active)
        {
            GameManager.instance.StartCoroutine(Status(en.GetComponent<Enemy>()));

            if (onlyOnce)
            {
                active = false;
            }            
        }        
    }

    public IEnumerator Status(Enemy e)
    {
        float health = e.GetComponent<EnemyController>().EnemyStats.currentHealth;
        if (health <= 0) yield break;
        e.state = status;        

        yield return new WaitForSeconds(effectDur);

        if (health <= 0)
        {
            e.state = Enemy.EnemyState.dead;
        }
        else
        {
            e.state = Enemy.EnemyState.search;
        }
                
    }
}
