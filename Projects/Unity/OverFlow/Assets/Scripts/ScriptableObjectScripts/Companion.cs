﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Companion : MonoBehaviour
{
    public enum DroneState { Chase, Attack, Adjust }

    float attackRange = 5f;
    float rayDistance = 5f;
    float stoppingDistance = 5f;

    Vector3 destination;
    Vector3 direction;
    Quaternion desiredRotation;
    Enemy target;
    DroneState currentState;    

    public float horizontalDist;
    public float verticalDist;
    public float speed;
    public float rotSpeed;
    public Transform player;
    public LayerMask layerMask;
    
    void Update()
    {
        switch (currentState)
        {
            case (DroneState.Adjust):

                if (NeedsDestination())
                {
                    GetDestination();
                }

                transform.rotation = desiredRotation;

                transform.Translate(Vector3.forward * Time.deltaTime * speed);

                while (IsPathBlocked())
                {
                    GetDestination();
                }

                var targetToAggro = CheckForAggro();

                if (targetToAggro != null)
                {
                    target = targetToAggro.GetComponent<Enemy>();
                    currentState = DroneState.Chase;
                }

                break;

            case (DroneState.Chase):

                if (target == null)
                {
                    currentState = DroneState.Adjust;
                    return;
                }

                transform.LookAt(target.transform);
                transform.Translate(Vector3.forward * Time.deltaTime * speed);

                if (Vector3.Distance(transform.position, target.transform.position) < attackRange)
                {
                    currentState = DroneState.Attack;
                }

                break;

            case (DroneState.Attack):

                if (target != null)
                {
                    Destroy(target.gameObject);
                }

                currentState = DroneState.Adjust;

                break;
        }
    }

    Quaternion startAngle = Quaternion.AngleAxis(-60, Vector3.up);
    Quaternion stepAngle = Quaternion.AngleAxis(5, Vector3.up);

    public Transform CheckForAggro()
    {
        float aggroRadius = 5f;

        var angle = transform.rotation * startAngle;
        //var direction = angle * Vector3.right;
        var direction = angle * Vector3.forward;
        var pos = transform.position;

        for (var i = 0; i < 24; i++)
        {
            if (Physics.Raycast(pos, direction, out RaycastHit hit))
            {
                var en = hit.collider.GetComponent<Enemy>();

                if (en != null)
                {
                    Debug.DrawRay(pos, direction * hit.distance, Color.red);
                    return en.transform;
                }
                else
                {
                    Debug.DrawRay(pos, direction * aggroRadius, Color.yellow);
                }                
            }
            else
            {
                Debug.DrawRay(pos, direction * aggroRadius, Color.yellow);
            }

            direction = stepAngle * direction;
        }

        return null;
    }

    private bool NeedsDestination()
    {
        if (destination == Vector3.zero)
        {
            return true;
        }

        var distance = Vector3.Distance(transform.position, destination);

        if (distance <= stoppingDistance)
        {
            return true;
        }

        return false;
    }

    private void GetDestination()
    {
        Vector3 testPosition = (transform.position + (transform.forward * 4f)) + new Vector3(Random.Range(-4.5f, 4.5f), 0f, Random.Range(-4.5f, 4.5f));

        destination = new Vector3(testPosition.x, 1f, testPosition.z);

        direction = Vector3.Normalize(destination - transform.position);
        direction = new Vector3(direction.x, 0f, direction.z);
        desiredRotation = Quaternion.LookRotation(direction);
    }

    private bool IsPathBlocked()
    {
        Ray ray = new Ray(transform.position, direction);

        var hit = Physics.RaycastAll(ray, rayDistance, layerMask);
        return hit.Any();
    }
}
