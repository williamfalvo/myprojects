﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Explosion Effect")]
public class ExplosionEffect : Effect
{
    public enum Stat { Strength, Agility, Level, Tech }

    public Stat stat;

    public float baseAtk;
    public float rad;
    public float pow;

    public GameObject explosion;

    float atk;

    float pStat;

    CharacterStatistics.BabScaling[] statValues;

    CharacterStatistics.LevelScaling[] levelValues;

    CharacterStatistics player;

    private void Awake()
    {
        atk = baseAtk;
    }

    public override void Discard()
    {
        
    }

    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        player = target.CharacterStatistics;

        //check which stat affects the damage 
        switch (stat)
        {
            case Stat.Strength:
                pStat = (int)Mathf.Clamp(player.baseStrength + player.bonusStrength, 0, 100);
                statValues = player.agilityStrenghtValues;
                break;

            case Stat.Agility:
                pStat = (int)Mathf.Clamp(player.baseAgility + player.bonusAgility, 0, 100);
                statValues = player.agilityStrenghtValues;
                break;

            case Stat.Tech:
                pStat = (int)Mathf.Clamp(player.baseIntellect + player.bonusIntellect, 0, 100);
                statValues = player.techBabValues;
                break;

            case Stat.Level:
                pStat = player.level;
                levelValues = player.levelValues;
                break;
        }

        Explode();
    }

    public void Explode()
    {
        Vector3 explosioPos = player.gameObject.transform.position;
        int layermask = 1 << 10; //enemy layer

        Collider[] colliders = Physics.OverlapSphere(explosioPos, rad, layermask);

        GameObject fx = Instantiate(explosion, player.transform);
        fx.transform.position += new Vector3(0f, 1f, 0f);
        Destroy(fx, 2.1f);

        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>(); // gets the rigidbody of every enemy

            if (rb != null)
            {
                rb.AddExplosionForce(pow, explosioPos, rad, 0.3f); // push the enemies away from explosion

                //calculates resistence and attack
                EnemyController enemy = hit.GetComponent<EnemyController>();
                float res = (int)Mathf.Clamp(enemy.EnemyStats.baseNeuralResistence + enemy.EnemyStats.bonusNeuralResistence, 0, 100);

                float dmg = 0f;

                if (stat == Stat.Level)
                {
                    dmg = GameManager.instance.damager.CalculateDamage(atk, pStat, levelValues, enemy.EnemyStats.baseDefense, res, enemy.EnemyStats.defenseValues, false, 0f);
                }
                else
                {
                    dmg = GameManager.instance.damager.CalculateDamage(atk, pStat, statValues, enemy.EnemyStats.baseDefense, res, enemy.EnemyStats.defenseValues, false, 0f);
                }               

                DamageText damageT = Instantiate(enemy.damageText, hit.ClosestPoint(enemy.transform.position), Quaternion.identity).GetComponent<DamageText>();
                damageT.textMesh.text = dmg.ToString();

                enemy.TakeDamage(dmg,true);
            }
        }
    }

}
