using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Abilità attiva

public class Shield : ActiveAbility
{
    public GameObject shield;

    Material shieldMaterial;
    Renderer rend;
    Color color;

    private bool shieldOn = false;
    private float timer;
    private float reset;
    private float maxDuration;
    private float resetTime;
    private float force;

    private void Start()
    {
        //setup the ability on the player
        GameManager.instance.OnDungeonStart += SkillCheck;

        player = GetComponentInParent<PlayerController>();
        playerStats = GetComponentInParent<CharacterStatistics>();

        rend = shield.GetComponent<Renderer>();
        shieldMaterial = rend.material;
        color = rend.material.color;
    }

    bool firstTime = false;

    private void Update()
    {
        if (player.Pressed && canDo && !player.StartCooldown)
        {
            if (!firstTime)
            {
                TechCheck();

                firstTime = true;
            }            

            reset = resetTime;
            Countdown();
        }
        else
        {

            shield.SetActive(false);
            shieldOn = false;
        }

        if (timer < maxDuration && timer > 0f && canDo && !shieldOn)
        {
            //starts recharging the shield if it hasnt been completely used
            ResetTime();
        }

        player.SecLifeBar = shield.activeSelf;

        player.anim.SetBool("ShieldOn", shield.activeSelf);
    }

    public void Countdown()
    {
        if (timer > 0f)
        {
            //checks the player is in a good state to activate the shield
            if (player.playerState == PlayerController.PlayerState.idle)
            {
                //activates the shield and loses duration
                player.anim.SetBool("ShieldOn", true);
                shield.SetActive(true);
                shieldOn = true;
                timer -= Time.deltaTime;
                FadeFx();
            }
            else
            {
                shield.SetActive(false);
                shieldOn = false;
                timer -= 0f;
            }
        }
        //if no more shield duration
        else if (timer <= 0f)
        {
            shield.SetActive(false);
            shieldOn = false;
            timer = maxDuration;
            player.StartCooldown = true;
            color.a = 1f;
            firstTime = false;
        }
    }

    //slowly recharge the shield if not fully charged and not pressed
    public void ResetTime()
    {
        if (reset > 0f)
        {
            if (player.Pressed)
            {
                return;
            }

            reset -= Time.deltaTime;
        }
        else if (reset <= 0f)
        {
            timer = maxDuration;
            canDo = true;
            reset = resetTime;
            return;
        }
    }

    public void TechCheck()
    {
        maxDuration += playerStats.intellectValues[(int)(playerStats.baseIntellect + playerStats.bonusIntellect)].shieldDuration;

        timer = maxDuration;
    }

    public void FadeFx()
    {
        MaterialPropertyBlock mpb = new MaterialPropertyBlock();
        mpb.SetColor("_Color", color);
        color.a -= Time.deltaTime / maxDuration;

        rend.SetPropertyBlock(mpb);
    }

    // initialize the skill we equipped in the player script
    public override void SkillCheck()
    {
        foreach (Skill s in GameManager.instance.EquippedSkills)
        {
            if (activeSkill.name == s.name)
            {
                canDo = true;

                ActiveSkillEffect f = s.trigger.effects[0] as ActiveSkillEffect;

                maxDuration = f.totalDuration;
                resetTime = f.resetDuration;

                timer = maxDuration;
                reset = resetTime;

                force = f.power;

                player.SkillCooldown = f.cooldown;
                player.CurrentCooldown = player.SkillCooldown;

                player.MaxCariche = f.maxShots;
            }
        }
    }

}
