﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet1 : MonoBehaviour
{
    public enum Stat { Strength, Agility, Tech, Level }

    public Stat stat;

    public float baseAtk;

    float atk;

    float pStat;

    CharacterStatistics.BabScaling[] statValues;

    CharacterStatistics.LevelScaling[] levelValues;

    CharacterStatistics player;

    public BuffDebuff debuff;

    public bool doDebuff = false;

    public bool techScaling = false;

    public GameObject explosionFx;

    public float radius;

    void Start()
    {
        atk = baseAtk;

        player = GameManager.instance.player;

        if (techScaling)
        {
            atk += (atk * player.intellectValues[(int)(player.baseIntellect + player.bonusIntellect)].companionDamage / 100f);
        }        

        // which stat affects the damage
        switch (stat)
        {
            case Stat.Strength:
                pStat = (int)Mathf.Clamp(player.baseStrength + player.bonusStrength, 0, 100);
                statValues = player.agilityStrenghtValues;
                break;

            case Stat.Agility:
                pStat = (int)Mathf.Clamp(player.baseAgility + player.bonusAgility, 0, 100);
                statValues = player.agilityStrenghtValues;
                break;

            case Stat.Tech:
                pStat = (int)Mathf.Clamp(player.baseIntellect + player.bonusIntellect, 0, 100);
                statValues = player.techBabValues;
                break;

            case Stat.Level:
                pStat = player.level;
                levelValues = player.levelValues;
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9 || other.gameObject.layer == 10 || other.gameObject.layer == 12)
        {
            int layermask = 1 << 10;

            Collider[] colliders = Physics.OverlapSphere(transform.position, radius, layermask);

            foreach (Collider hit in colliders)
            {
                EnemyController enemy = hit.GetComponent<EnemyController>();

                float res = (int)Mathf.Clamp(enemy.EnemyStats.baseNeuralResistence + enemy.EnemyStats.bonusNeuralResistence, 0, 100);

                float dmg = 0f;

                if (stat == Stat.Level)
                {
                    dmg = GameManager.instance.damager.CalculateDamage(atk, pStat, levelValues, enemy.EnemyStats.baseDefense, res, enemy.EnemyStats.defenseValues, false, 0f);
                }
                else
                {
                    dmg = GameManager.instance.damager.CalculateDamage(atk, pStat, statValues, enemy.EnemyStats.baseDefense, res, enemy.EnemyStats.defenseValues, false, 0f);
                }                

                DamageText damageT = Instantiate(enemy.damageText, other.ClosestPoint(enemy.transform.position), Quaternion.identity).GetComponent<DamageText>();
                damageT.textMesh.text = dmg.ToString();

                enemy.TakeDamage(dmg, true);

                if (doDebuff)
                {
                    enemy.EnemyStats.DebuffAdd(debuff);
                }
            }

            GameObject g = Instantiate(explosionFx, transform.position, Quaternion.identity);

            Destroy(g, 2f);

            Destroy(gameObject);
        }        

        //if (other.gameObject.layer != 11 && other.gameObject.layer != 0 && other.gameObject.layer != 15)
        //{
        //    //if enemy layer
        //    if (other.gameObject.layer == 10)
        //    {
        //        EnemyController enemy = other.GetComponent<EnemyController>();

        //        //if enemy not null calculates the damage based on their stats
        //        if (enemy != null)
        //        {
        //            float res = (int)Mathf.Clamp(enemy.EnemyStats.baseNeuralResistence + enemy.EnemyStats.bonusNeuralResistence, 0, 100);

        //            float dmg = GameManager.instance.damager.CalculateDamage(atk, pStat, statValues, enemy.EnemyStats.baseDefense, res, enemy.EnemyStats.defenseValues, false);

        //            enemy.TakeDamage(dmg, true);

        //            if (doDebuff)
        //            {
        //                enemy.EnemyStats.DebuffAdd(debuff);
        //            }                    
        //        }
        //    }

        //    Destroy(gameObject);
        //}        
    }
}
