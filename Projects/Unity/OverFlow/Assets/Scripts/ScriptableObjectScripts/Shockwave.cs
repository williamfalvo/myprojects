﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shockwave : ActiveAbility
{
    float atk;
    float rad;
    float pow;

    public GameObject shockwaveFx;

    void Start()
    {
        //update the player with this skill stat
        GameManager.instance.OnDungeonStart += SkillCheck;

        player = GetComponentInParent<PlayerController>();

        playerStats = GetComponentInParent<CharacterStatistics>();
    }

    void Update()
    {
        //check if the player can activate the ability
        if (player.Pressed && canDo && (player.playerState == PlayerController.PlayerState.idle || player.playerState == PlayerController.PlayerState.walk) && player.StartCooldown == false)
        {
            TechCheck();

            Explosion();
            player.StartCooldown = true; //starts the cooldown of the ability

            player.playerState = PlayerController.PlayerState.ability;
        }
    }

    public void TechCheck()
    {
        ActiveSkillEffect f = activeSkill.trigger.effects[0] as ActiveSkillEffect;

        rad = f.radius;
        pow = f.power;

        rad += (rad * playerStats.intellectValues[(int)(playerStats.baseIntellect + playerStats.bonusIntellect)].shockwaveRange / 100f);
        atk += (rad * playerStats.intellectValues[(int)(playerStats.baseIntellect + playerStats.bonusIntellect)].shockwaveDamage / 100f);
    }

    //setup the skill in the playercontroller
    public override void SkillCheck()
    {
        foreach (Skill s in GameManager.instance.EquippedSkills)
        {
            if (activeSkill.name == s.name)
            {
                canDo = true;

                ActiveSkillEffect f = s.trigger.effects[0] as ActiveSkillEffect;

                rad = f.radius;
                pow = f.power;
                atk = f.baseAtk;

                player.MaxCariche = f.maxShots;
                player.SkillCooldown = f.cooldown;
                player.CurrentCooldown = player.SkillCooldown;
            }
        }
    }

    //real ability
    public void Explosion()
    {
        Vector3 explosioPos = player.gameObject.transform.position;
        int layermask = 1 << 10; // sets Enemy as the only hitted layer

        Collider[] colliders = Physics.OverlapSphere(explosioPos, rad, layermask); // creates the explosion sphere

        GameObject shock = Instantiate(shockwaveFx, transform);
        //FMODManager.instance.PlayerShockwave();
        shock.transform.position += new Vector3(.5f * transform.localScale.x, 1f, 0f);
        Destroy(shock, 3f);

        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();//gets the rigidbody of every enemy hitted

            if (rb != null)
            {
                if (rb.useGravity)
                {
                    rb.AddExplosionForce(pow, explosioPos, rad, 0.3f);//push them away from explosion
                }                

                EnemyController enemy = hit.GetComponent<EnemyController>();
                //calculate the enemy resistence
                float res = (int)Mathf.Clamp(enemy.EnemyStats.baseNeuralResistence + enemy.EnemyStats.bonusNeuralResistence, 0, 100);

                CharacterStatistics p = player.playerStats;
                //calculate the player atk
                float pStat = (int)Mathf.Clamp(p.baseStrength + p.bonusStrength, 0, 100);

                //calculates the damage based on atk-def
                float dmg = GameManager.instance.damager.CalculateDamage(atk, pStat, p.agilityStrenghtValues, enemy.EnemyStats.baseDefense, res, enemy.EnemyStats.defenseValues, false, 0f);

                DamageText damageT = Instantiate(enemy.damageText, hit.ClosestPoint(enemy.transform.position), Quaternion.identity).GetComponent<DamageText>();
                damageT.textMesh.text = dmg.ToString();

                enemy.TakeDamage(dmg,true);
            }
        }
    }
}
