﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Carta o Abilità

[CreateAssetMenu(menuName = "Card Effects/AttackSpeedUp")]
public class AttackSpeedUp : Effect
{
    // attack speed modifier
    public float attackSpeedModifier;

    public override void Use()
    {
        // the effect takes its target from Target Manager
        target = FindObjectOfType<TargetManager>();

        // if the effect has no duration, it will last untill its discard
        if (duration == 0)
        {
            Permanent(target.PlayerController);
        }
        // if is temporary a coroutine starts and when it ends, the efect will be removed
        else
        {
            GameManager.instance.StartCoroutine(Temporary(target.PlayerController));
        }
    }

    public override void Discard()
    {
        // the effect takes its target from Target Manager and if it's null exit from the method
        PlayerController player = target.PlayerController;
        if (player == null) return;

        // set player's animation speed to its default value
        player.anim.speed = 1f;
    }

    public void Permanent(PlayerController player)
    {
        // player's attack animation speed gets faster
        if (player.playerState == PlayerController.PlayerState.attack)
        {
            player.anim.speed += attackSpeedModifier;
        }        
    }
        
    IEnumerator Temporary(PlayerController p)
    {
        Permanent(p);

        yield return new WaitForSeconds(duration);

        Discard();
    }
}
