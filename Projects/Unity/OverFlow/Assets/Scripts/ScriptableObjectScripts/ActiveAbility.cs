﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActiveAbility : MonoBehaviour
{
    public GameObject obj;

    [HideInInspector]
    public bool canDo = false;

    [HideInInspector]
    public PlayerController player;

    [HideInInspector]
    public CharacterStatistics playerStats;

    public Skill activeSkill;

    public abstract void SkillCheck();
}
