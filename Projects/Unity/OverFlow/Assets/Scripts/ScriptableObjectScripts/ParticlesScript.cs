﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesScript : MonoBehaviour
{
    //particle ability effect
    public enum Stat { Strength, Agility, Level, Tech }

    public Stat stat;

    public ParticleSystem part;
    public List<ParticleCollisionEvent> collisionEvents;

    public float baseAtk;
    public float speedMultiplier;
    public float particleDuration;

    Rigidbody rb;

    CharacterStatistics p;
    float pStat;
    CharacterStatistics.BabScaling[] statValues;

    CharacterStatistics.LevelScaling[] levelValues;

    void Start()
    {
        //ParticlePhysicsExtensions.GetCollisionEvents();

        collisionEvents = new List<ParticleCollisionEvent>();

        p = GameManager.instance.player;

        //check wich stat affects the damage
        switch (stat)
        {
            case Stat.Strength:
                pStat = (int)Mathf.Clamp(p.baseStrength + p.bonusStrength, 0, 100);
                statValues = p.agilityStrenghtValues;
                break;

            case Stat.Agility:
                pStat = (int)Mathf.Clamp(p.baseAgility + p.bonusAgility, 0, 100);
                statValues = p.agilityStrenghtValues;
                break;

            case Stat.Tech:
                pStat = (int)Mathf.Clamp(p.baseIntellect + p.bonusIntellect, 0, 100);
                statValues = p.techBabValues;
                break;

            case Stat.Level:
                pStat = p.level;
                levelValues = p.levelValues;
                break;
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);

        rb = other.GetComponent<Rigidbody>();

        int i = 0;

        while (i < numCollisionEvents)
        {
            if (rb != null && rb.gameObject.layer == 10)
            {
                Vector3 pos = collisionEvents[i].intersection; //gets the hit point
                Vector3 force = collisionEvents[i].velocity * speedMultiplier; //gets the velocity and multiplies it

                rb.AddForceAtPosition(force, pos); //add a pushback ?

                //InvokeRepeating("DealDamage", 1f, 1f); //keeps inflicting damage to the enemy and destroys the particle
                DealDamage(other.transform);

                Destroy(gameObject, particleDuration);
            }

            i++;
        }
    }

    //calculates the statistics and applies the right damage
    public void DealDamage(Transform other)
    {
        EnemyController enemy = rb.GetComponent<EnemyController>();

        float res = (int)Mathf.Clamp(enemy.EnemyStats.baseNeuralResistence + enemy.EnemyStats.bonusNeuralResistence, 0, 100);

        float dmg = 0f;

        if (stat == Stat.Level)
        {
            dmg = GameManager.instance.damager.CalculateDamage(baseAtk, pStat, levelValues, enemy.EnemyStats.baseDefense, res, enemy.EnemyStats.defenseValues, false, 0f);
        }
        else
        {
            dmg = GameManager.instance.damager.CalculateDamage(baseAtk, pStat, statValues, enemy.EnemyStats.baseDefense, res, enemy.EnemyStats.defenseValues, false, 0f);
        }     

        DamageText damageT = Instantiate(enemy.damageText, other.position, Quaternion.identity).GetComponent<DamageText>();
        damageT.textMesh.text = dmg.ToString();

        enemy.TakeDamage(dmg,true);
    }
}
