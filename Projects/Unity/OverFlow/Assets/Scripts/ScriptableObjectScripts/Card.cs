﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// general card scriptable object
[CreateAssetMenu(fileName = "General Card")]
public class Card : ScriptableObject
{
// each card has:
    // a name
    public new string name;

    // enumeration of all archetypes a card can have
    public enum Archetypes { Consumable, Armor, WeaponMod, Trinket }

    // a list of the card archetypes
    public List<Archetypes> cardArchetypes;

    // rarity of cards (inspired by YU-GI-OH)
    public enum Rarity { Common, Rare, Uncommon} 

    // a rarity
    public Rarity cardRarity;    

    // an artwork
    public Sprite artwork;    

    public List<Trigger> triggers;

    // the card's life based on its use of current archetype
    public int cardLife;

    [TextArea]
    public string cardDescription;

    public bool Equals(Card card)
    {
        return name == card.name;
    }
}
