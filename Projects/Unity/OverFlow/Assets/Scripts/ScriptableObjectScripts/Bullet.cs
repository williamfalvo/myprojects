﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public enum Stat { Strength, Agility }

    public Stat stat;

    public float baseAtk;

    float atk;

    float pStat;

    CharacterStatistics.BabScaling[] statValues;

    CharacterStatistics player;

    public float speed;

    private Rigidbody rb;

    public GameObject explosionFx;

    private void Awake()
    {
        player = GameManager.instance.player;

        atk = baseAtk + (baseAtk * player.intellectValues[(int)(player.baseIntellect + player.bonusIntellect)].projectileDamage / 100f);

        //depends wich stacts is going to affect the damage
        switch (stat)
        {
            case Stat.Strength:
                pStat = (int)Mathf.Clamp(player.baseStrength + player.bonusStrength, 0, 100);
                statValues = player.agilityStrenghtValues;
                break;

            case Stat.Agility:
                pStat = (int)Mathf.Clamp(player.baseAgility + player.bonusAgility, 0, 100);
                statValues = player.agilityStrenghtValues;
                break;
        }
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.right * speed * Mathf.Sign(GameManager.instance.p.transform.localScale.x);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != 11 && other.gameObject.layer != 0 && other.gameObject.layer != 15)
        {
            //if collides with enemy applicates the damage
            if (other.gameObject.layer == 10)
            {
                EnemyController enemy = other.GetComponent<EnemyController>();

                if (enemy != null)
                {
                    float res = (int)Mathf.Clamp(enemy.EnemyStats.baseNeuralResistence + enemy.EnemyStats.bonusNeuralResistence, 0, 100);

                    float dmg = GameManager.instance.damager.CalculateDamage(atk, pStat, statValues, enemy.EnemyStats.baseDefense, res, enemy.EnemyStats.defenseValues, false, 0f);

                    DamageText damageT = Instantiate(enemy.damageText, other.ClosestPoint(enemy.transform.position), Quaternion.identity).GetComponent<DamageText>();
                    damageT.textMesh.text = dmg.ToString();

                    enemy.TakeDamage(dmg, true);
                }
            }

            //if the layer that collides with is not the player or default id destroys
            Destroy(gameObject);
        }        
    }

    private void OnDestroy()
    {
        GameObject g = Instantiate(explosionFx, transform.position, Quaternion.identity);

        Destroy(g, 1f);
    }
}
