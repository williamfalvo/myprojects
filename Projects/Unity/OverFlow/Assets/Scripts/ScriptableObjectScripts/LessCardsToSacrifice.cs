﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Abilità

[CreateAssetMenu(menuName = "Card Effects/LessCardsToSacrifice")]
public class LessCardsToSacrifice : Effect
{
    // decrease value
    public float minus;

    public override void Use()
    {
        // the effect takes its target from Target Manager
        target = FindObjectOfType<TargetManager>();

        ReduceCardsToSacrifice(target.deathController);
    }

    public override void Discard()
    {
        // the effect takes its target from Target Manager and if it's null get out of the method
        DeathController deathController = target.deathController;
        if (deathController == null) return;
                
        deathController.perc += minus;
    }

    // reduces the number of cards to be sacrified in case of player's death
    public void ReduceCardsToSacrifice(DeathController dc)
    {        
        dc.perc -= minus;
    }
}
