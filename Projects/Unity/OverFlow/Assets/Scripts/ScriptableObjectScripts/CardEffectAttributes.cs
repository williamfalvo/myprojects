﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Carta o Abilità

[CreateAssetMenu(menuName = "Card Effects/AttributesModifier")]
public class CardEffectAttributes : Effect
{
    // player stats modifiers
    public float strenghtModifier = 0;
    public float intellectModifier = 0;
    public float agilityModifier = 0;
    public float neuralResistenceModifier = 0;
    public float luckModifier = 0;
    public float neuralHealthModifier = 0;
    public float animSpeedModifier = 0;
    public float currentHealthModifier = 0;

    public override void Use()
    {
        // the effect takes its target from Target Manager
        target = FindObjectOfType<TargetManager>();

        hasBeenUsed = true;

        // if the effect has no duration, it will last untill its discard
        if (duration == 0)
        {
            Permanent(target.CharacterStatistics);
        }
        // if is temporary a coroutine starts and when it ends, the efect will be removed
        else
        {
            GameManager.instance.StartCoroutine(Temporary(target.CharacterStatistics));
        }
    }

    public override void Discard()
    {
        if (hasBeenUsed)
        {
            // the effect takes its target from Target Manager and if it's null get out of the method
            CharacterStatistics player = target.CharacterStatistics;
            if (player == null) return;

            // remove the values from player's statistics
            player.bonusStrength -= Delta(player.baseStrength + player.bonusStrength, strenghtModifier);
            player.bonusIntellect -= Delta(player.baseIntellect + player.bonusIntellect, intellectModifier);
            player.bonusAgility -= Delta(player.baseAgility + player.bonusAgility, agilityModifier);
            player.bonusNeuralResistence -= Delta(player.baseNeuralResistence + player.bonusNeuralResistence, neuralResistenceModifier);
            player.bonusLuck -= Delta(player.baseLuck + player.bonusLuck, luckModifier);
            player.bonusNeuralHealth -= Delta(player.baseNeuralHealth + player.bonusNeuralHealth, neuralHealthModifier);

            target.PlayerController.anim.speed -= animSpeedModifier;

            // update player's statistics UI
            target.statisticsManager.UpdateStatistics();

            hasBeenUsed = false;
        }        
    }

    public float Delta(float bb, float mod)
    {
        float unclampedStat = bb;
        bb = Mathf.Clamp(bb + mod, 1f, 99f);
        return (bb - unclampedStat);
    }

    public void Permanent(CharacterStatistics player)
    {
        // adds the values to player's statistics        
        player.bonusStrength += Delta(player.baseStrength + player.bonusStrength, strenghtModifier);
        player.bonusIntellect += Delta(player.baseIntellect + player.bonusIntellect, intellectModifier);
        player.bonusAgility += Delta(player.baseAgility + player.bonusAgility, agilityModifier);                
        player.bonusNeuralResistence += Delta(player.baseNeuralResistence + player.bonusNeuralResistence, neuralResistenceModifier);                
        player.bonusLuck += Delta(player.baseLuck + player.bonusLuck, luckModifier);       
        player.bonusNeuralHealth += Delta(player.baseNeuralHealth + player.bonusNeuralHealth, neuralHealthModifier);
        
        player.currentHealth += currentHealthModifier;

        if(player.currentHealth > 160f)
        {
            FMODManager.instance.StopPlayerLowHp();
        }

        if(player.currentHealth <= 0)
        {
            player.currentHealth = 1;
        }

        target.PlayerController.anim.speed += animSpeedModifier;

        // update player's statistics UI
        target.statisticsManager.UpdateStatistics();
    }

    IEnumerator Temporary(CharacterStatistics player)
    {
        Permanent(player);

        yield return new WaitForSeconds(duration);

        Discard();
    }
}
