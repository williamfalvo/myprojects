﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/AttributeEffect")]
public class AttributeEffect : Effect
{
    // player stats modifiers
    public float strenghtModifier;
    public float intellectModifier;
    public float agilityModifier;
    public float neuralResistenceModifier;
    public float luckModifier;
    public float neuralHealthModifier;
    public float animSpeedModifier;
    public float currentHealthModifier;

    CharacterStatistics player;

    public override void Discard()
    {
        player = GameManager.instance.player;

        if (player == null) return;

        // remove the values from player's statistics
        player.bonusStrength -= strenghtModifier;
        player.bonusIntellect -= intellectModifier;
        player.bonusAgility -= agilityModifier;
        player.bonusNeuralResistence -= neuralResistenceModifier;
        player.bonusLuck -= luckModifier;
        player.bonusNeuralHealth -= neuralHealthModifier;
        player.currentHealth -= currentHealthModifier;
    }

    public override void Use()
    {
        player = GameManager.instance.player;

        if (player == null) return;

        player.bonusStrength += strenghtModifier;
        player.bonusIntellect += intellectModifier;
        player.bonusAgility += agilityModifier;
        player.bonusNeuralResistence += neuralResistenceModifier;
        player.bonusLuck += luckModifier;
        player.bonusNeuralHealth += neuralHealthModifier;
        player.currentHealth += currentHealthModifier;

        player.NeuralHealthModified();
    }
}
