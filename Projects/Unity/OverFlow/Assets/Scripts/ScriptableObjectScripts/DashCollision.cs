﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/DashCollsion")]
public class DashCollision : Effect
{
    public enum Stat { Strength, Agility, Tech, Level }

    public Stat stat;    

    public float baseAtk;

    float atk;

    float pStat;

    CharacterStatistics.BabScaling[] statValues;

    CharacterStatistics.LevelScaling[] levelValues;

    CharacterStatistics enemyStats;

    float enemyRes;

    private void Awake()
    {
        atk = baseAtk;
    }

    public override void Discard()
    {
        
    }

    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        CharacterStatistics player = target.CharacterStatistics;        

        //check which stat will affect the damage
        switch (stat)
        {
            case Stat.Strength:
                pStat = (int)Mathf.Clamp(player.baseStrength + player.bonusStrength, 0, 100);
                statValues = player.agilityStrenghtValues;
                break;

            case Stat.Agility:
                pStat = (int)Mathf.Clamp(player.baseAgility + player.bonusAgility, 0, 100);
                statValues = player.agilityStrenghtValues;
                break;

            case Stat.Tech:
                pStat = (int)Mathf.Clamp(player.baseIntellect + player.bonusIntellect, 0, 100);
                statValues = player.techBabValues;
                break;

            case Stat.Level:
                pStat = player.level;
                levelValues = player.levelValues;
                break;
        }        

        if (enemy != null)
        {
            enemyStats = enemy.GetComponent<CharacterStatistics>();

            enemyRes = (int)Mathf.Clamp(enemyStats.baseNeuralResistence + enemyStats.bonusNeuralResistence, 0, 100);
        }

        Permanent();                
    }

    //inflicts the damage to the enemy
    public void Permanent()
    {
        float dmg = 0f;

        if (stat == Stat.Level)
        {
            dmg = GameManager.instance.damager.CalculateDamage(atk, pStat, levelValues, enemyStats.baseDefense, enemyRes, enemyStats.defenseValues, false, 0f);
        }
        else
        {
            dmg = GameManager.instance.damager.CalculateDamage(atk, pStat, statValues, enemyStats.baseDefense, enemyRes, enemyStats.defenseValues, false, 0f);
        }       

        enemy.GetComponent<EnemyController>().TakeDamage(dmg,true);
    }
}
