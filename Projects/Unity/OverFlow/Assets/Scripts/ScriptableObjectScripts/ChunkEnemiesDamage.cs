﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/ChunkEnemiesDamage")]
public class ChunkEnemiesDamage : Effect
{
    public float baseAtk;

    float atk;
    List<Enemy> enemies = new List<Enemy>();

    private void Awake()
    {
        atk = baseAtk;
    }

    public override void Discard()
    {
        
    }

    //find every enemy in the chunk and damages them
    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        enemies = target.GetAllEnemies();

        foreach (Enemy en in enemies)
        {
            EnemyController e = en.GetComponent<EnemyController>();

            float res = (int)Mathf.Clamp(e.EnemyStats.baseNeuralResistence + e.EnemyStats.bonusNeuralResistence, 0, 100);

            CharacterStatistics.BabScaling[] babs = new CharacterStatistics.BabScaling[0];

            float dmg = GameManager.instance.damager.CalculateDamage(atk, 0f, babs, e.EnemyStats.baseDefense, res, e.EnemyStats.defenseValues, false, 0f);

            e.TakeDamage(dmg,true);
        }
    }
}
