﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Weapon Scaling Change")]
public class WeaponScalingChange : Effect
{
    List<WeaponClass.Type> currentTypes = new List<WeaponClass.Type>();

    public override void Discard()
    {
        if (hasBeenUsed)
        {
            UpdateScaling(target.PlayerController);

            hasBeenUsed = false;
        }
    }

    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        hasBeenUsed = true;

        ChangeScaling(target.PlayerController);
    }   
    
    //cycle all the weapons the player has and changes they type to TECH so now they have a stronger scaling
    public void ChangeScaling(PlayerController p)
    {
        foreach (var weapon in p.weapons)
        {
            WeaponClass wp = weapon.collider.GetComponent<WeaponClass>();

            currentTypes.Add(wp.wpType);

            wp.wpType = WeaponClass.Type.Tech;
        }
    }

    //changes the type of every weapon to his old type and scale
    public void UpdateScaling(PlayerController p)
    {
        for (int i = 0; i < p.weapons.Count; i++)
        {
            WeaponClass wp = p.weapons[i].collider.GetComponent<WeaponClass>();

            wp.wpType = currentTypes[i];
        }

        currentTypes = new List<WeaponClass.Type>();
    }
}
