﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Scaling Removal")]
public class ScalingRemoval : Effect
{
    public override void Discard()
    {
        PlayerController player = target.PlayerController;

        if (player == null) return;

        player.playerStats.UseScaling = true;
    }

    //the player will not use the scaling of his weapon as addcitional damage
    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        PlayerController player = target.PlayerController;

        player.playerStats.UseScaling = false;
    }
}
