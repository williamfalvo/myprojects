﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Dash Up")]
public class DashUp : Effect
{
    public float dashSpeedModifier;

    public override void Discard()
    {
        if (hasBeenUsed)
        {
            PlayerController player = target.PlayerController;

            if (player == null) return;

            //reset the player dash speed on discard
            player.dashSpeed = player.tempDashSpeed;
        }             
    }

    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        hasBeenUsed = true;

        if (duration == 0f)
        {
            //apply the permanent effect
            Permanent(target.PlayerController);
        }
        else
        {
            GameManager.instance.StartCoroutine(Temporary(target.PlayerController));
        }
    }

    public void Permanent(PlayerController player)
    {
        //increase the player dash speed
        player.dashSpeed += dashSpeedModifier;
    }

    IEnumerator Temporary(PlayerController p)
    {
        Permanent(p);

        yield return new WaitForSeconds(duration);

        Discard();
    }
}
