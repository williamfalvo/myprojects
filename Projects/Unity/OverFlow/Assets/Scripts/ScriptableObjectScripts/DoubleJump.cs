﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Double Jump")]
public class DoubleJump : Effect
{
    public override void Discard()
    {
        //do the discard only if the ability has been used
        if (hasBeenUsed)
        {
            PlayerController player = target.PlayerController;

            if (player == null) return;

            player.doublejump = false;
        }        
    }

    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        hasBeenUsed = true;

        if (duration == 0f)
        {
            Permanent(target.PlayerController);
        }
        else
        {
            GameManager.instance.StartCoroutine(Temporary(target.PlayerController));
        }
    }    

    //sets the bool doublejump of the player to true so he can double jump
    public void Permanent(PlayerController player)
    {
        player.doublejump = true;
    }

    IEnumerator Temporary(PlayerController player)
    {
        Permanent(player);

        yield return new WaitForSeconds(duration);

        Discard();
    }
}
