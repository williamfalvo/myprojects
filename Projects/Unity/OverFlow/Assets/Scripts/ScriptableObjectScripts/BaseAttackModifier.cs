﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Base Attack Increase")]
public class BaseAttackModifier : Effect
{
    public float atkMod;

    public override void Discard()
    {
        if (hasBeenUsed)
        {
            PlayerController player = target.PlayerController;

            if (player == null) return;

            foreach (var weapon in player.weapons)
            {
                WeaponClass gw = weapon.collider.GetComponentInChildren<WeaponClass>();

                gw.AtkModPerc = 0f;
            }

            hasBeenUsed = false;
        }        
    }

    //add damages to the weapon
    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        hasBeenUsed = true;

        if (duration == 0)
        {
            Permanent(target.PlayerController);
        }
        else
        {
            GameManager.instance.StartCoroutine(Temporary(target.PlayerController));
        }

    }

    public void Permanent(PlayerController player)
    {
        foreach (var weapon in player.weapons)
        {
            WeaponClass gw = weapon.collider.GetComponentInChildren<WeaponClass>();

            gw.AtkModPerc = atkMod;
        }
    }

    public IEnumerator Temporary(PlayerController p)
    {
        Permanent(p);

        yield return new WaitForSeconds(duration);

        Discard();
    }
}
