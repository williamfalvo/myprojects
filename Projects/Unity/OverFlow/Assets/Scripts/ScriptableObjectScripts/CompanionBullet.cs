﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompanionBullet : MonoBehaviour
{
    public enum Stat { Strength, Agility }

    public Stat stat;

    public float baseAtk;

    float atk;

    float pStat;

    CharacterStatistics.BabScaling[] statValues;

    CharacterStatistics player;

    public float speed;

    private Rigidbody rb;

    private void Awake()
    {
        atk = baseAtk;

        player = GameManager.instance.player;

        //depends wich stacts is going to affect the damage
        switch (stat)
        {
            case Stat.Strength:
                pStat = (int)Mathf.Clamp(player.baseStrength + player.bonusStrength, 0, 100);
                statValues = player.agilityStrenghtValues;
                break;

            case Stat.Agility:
                pStat = (int)Mathf.Clamp(player.baseAgility + player.bonusAgility, 0, 100);
                statValues = player.agilityStrenghtValues;
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != 11 && other.gameObject.layer != 0)
        {
            //if collides with enemy applicates the damage
            if (other.gameObject.layer == 10)
            {
                EnemyController enemy = other.GetComponent<EnemyController>();

                if (enemy != null)
                {
                    float res = (int)Mathf.Clamp(enemy.EnemyStats.baseNeuralResistence + enemy.EnemyStats.bonusNeuralResistence, 0, 100);

                    float dmg = GameManager.instance.damager.CalculateDamage(atk, pStat, statValues, enemy.EnemyStats.baseDefense, res, enemy.EnemyStats.defenseValues, false, 0f);

                    DamageText damageT = Instantiate(enemy.damageText, other.ClosestPoint(enemy.transform.position), Quaternion.identity).GetComponent<DamageText>();
                    damageT.textMesh.text = dmg.ToString();

                    enemy.TakeDamage(dmg, true);
                }
            }

            //if the layer that collides with is not the player or default id destroys
            Destroy(gameObject);
        }
    }
}
