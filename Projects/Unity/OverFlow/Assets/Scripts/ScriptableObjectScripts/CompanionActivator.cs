using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompanionActivator : ActiveAbility
{
    public GameObject drone;
    public Transform spawnPoint;

    float duration;
    float timer;
    bool activate = false;
    GameObject currentDrone;

    void Start()
    {
        GameManager.instance.OnDungeonStart += SkillCheck;

        player = GetComponentInParent<PlayerController>();

        playerStats = GetComponentInParent<CharacterStatistics>();
    }

    void Update()
    {
        //instantiate the companion
        if (player.Pressed && canDo && !activate && (player.playerState == PlayerController.PlayerState.idle || player.playerState == PlayerController.PlayerState.walk) && player.StartCooldown == false)
        {
            TechCheck();

            GameObject dr = Instantiate(drone, spawnPoint.position, Quaternion.identity);

            Companion2 d = dr.GetComponent<Companion2>();
            d.player = gameObject;

            currentDrone = dr;

            activate = true;

            player.playerState = PlayerController.PlayerState.ability;
        }

        //decrease the companion life
        if (activate)
        {
            if (timer > 0f)
            {
                timer -= Time.deltaTime;
            }
            else if (timer <= 0f)
            {
                if (currentDrone != null)
                {
                    Destroy(currentDrone);
                }

                timer = duration;
                currentDrone = new GameObject();
                activate = false;

                player.StartCooldown = true;
            }
        }
    }

    public void TechCheck()
    {
        duration += playerStats.intellectValues[(int)(playerStats.baseIntellect + playerStats.bonusIntellect)].companionDuration;
        timer = duration;
    }

    public override void SkillCheck()
    {
        foreach (Skill s in GameManager.instance.EquippedSkills)
        {
            if (activeSkill.name == s.name)
            {
                canDo = true;

                ActiveSkillEffect f = s.trigger.effects[0] as ActiveSkillEffect;

                duration = f.totalDuration;
                timer = duration;

                player.MaxCariche = f.maxShots;
                player.SkillCooldown = f.cooldown;
                player.CurrentCooldown = player.SkillCooldown;
            }
        }
    }
}
