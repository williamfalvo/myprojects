﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Particles Attack")]
public class ParticlesAttack : Effect
{
    public GameObject fx;

    public override void Discard()
    {
        
    }

    //instantiate a particle effect (damages enemies that collides with)
    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();

        if (fx != null)
        {
            GameObject particlesAttack = Instantiate(fx, target.Player.transform.position + new Vector3(0f, 2f, 0f), Quaternion.identity);
        }        
    }
}
