﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : Effect
{
    public enum Status { Jam, Freeze, Lock}
    [HideInInspector]
    public Status status;

    public float effectDur;

    public override void Discard()
    {
        
    }

    public override void Use()
    {
        GameManager.instance.StartCoroutine(ApplyStatus());
    }

    public IEnumerator ApplyStatus()
    {
        InputManager input = GameManager.instance.inputManager;

        //set the status to the player
        switch (status)
        {
            case (Status.Freeze):
                input.freeze = true;
                break;

            case (Status.Jam):
                input.jam = true;
                break;

            case (Status.Lock):
                input.locked = true;
                break;
        }

        yield return new WaitForSeconds(effectDur);

        switch (status)
        {
            case (Status.Freeze):
                input.freeze = false;
                break;

            case (Status.Jam):
                input.jam = false;
                break;

            case (Status.Lock):
                input.locked = false;
                break;
        }
    }
}
