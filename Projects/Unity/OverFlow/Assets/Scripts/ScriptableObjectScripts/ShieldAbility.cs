﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Active Skills Effects/Shield")]
public class ShieldAbility : ActiveSkillEffect
{
    public override void Discard()
    {
        target = FindObjectOfType<TargetManager>();

        //find the shield and says the player cant activate it 
        Shield shield = target.Player.GetComponentInChildren<Shield>();

        if (shield == null) return;

        shield.canDo = false;
    }

    public override void Use()
    {
        
    }
}
