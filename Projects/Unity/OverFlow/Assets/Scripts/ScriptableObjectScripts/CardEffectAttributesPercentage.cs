﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Carta o Abilità

[CreateAssetMenu(menuName = "Card Effects/AttributesModifierPercentage")]
public class CardEffectAttributesPercentage : Effect
{
    // statistics percentage
    public float perc;


    public override void Use()
    {
        // the effect takes its target from Target Manager
        target = FindObjectOfType<TargetManager>();

        // if the effect has no duration, it will last untill its discard
        if (duration == 0)
        {
            Permanent(target.CharacterStatistics);
        }
        // if is temporary a coroutine starts and when it ends, the efect will be removed
        else
        {
            GameManager.instance.StartCoroutine(Temporary(target.CharacterStatistics));
        }
    }

    public override void Discard()
    {
        // the effect takes its target from Target Manager and if it's null exit from the method
        CharacterStatistics player = target.CharacterStatistics;
        if (player == null) return;

        // percentage decrease
        player.bonusStrength -= (player.bonusStrength * perc / 100f);
        player.bonusIntellect -= (player.bonusIntellect * perc / 100f);
        player.bonusAgility -= (player.bonusAgility * perc / 100f);
        player.bonusNeuralResistence -= (player.bonusNeuralResistence * perc / 100f);
        player.bonusLuck -= (player.bonusLuck * perc / 100f);
        player.bonusNeuralHealth -= (player.bonusNeuralHealth * perc / 100f);

        // update player's statistics UI
        target.statisticsManager.UpdateStatistics();
    }

    void Permanent(CharacterStatistics player)
    {
        // percentage increase only of the bonus strenght
        player.bonusStrength += (player.bonusStrength * perc/100f);
        player.bonusIntellect += (player.bonusIntellect * perc / 100f);
        player.bonusAgility += (player.bonusAgility * perc / 100f);
        player.bonusNeuralResistence += (player.bonusNeuralResistence * perc / 100f);
        player.bonusLuck += (player.bonusLuck * perc / 100f);
        player.bonusNeuralHealth += (player.bonusNeuralHealth * perc / 100f);

        // update player's statistics UI
        target.statisticsManager.UpdateStatistics();
    }

    IEnumerator Temporary(CharacterStatistics player)
    {
        Permanent(player);

        yield return new WaitForSeconds(duration);

        Discard();
    }
}
