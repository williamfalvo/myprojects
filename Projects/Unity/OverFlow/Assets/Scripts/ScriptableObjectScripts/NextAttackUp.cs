﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Card Effects/Next Attack Up")]
public class NextAttackUp : Effect
{
    public float perc;

    bool active = false;

    //float at = 0;

    float[] atks = new float[3];

    public override void Discard()
    {
        GameManager.instance.OnHit -= AttackDown;

        if (active)
        {
            PlayerController p = target.PlayerController;

            for (int i = 0; i < p.weapons.Count; i++)
            {
                PlayerController.Weapon wp = p.weapons[i];

                WeaponClass weapon = wp.collider.GetComponent<WeaponClass>();

                weapon.Atk = atks[i];
            }

            WeaponFlame(p, false);

            active = false;
        }
    }

    public override void Use()
    {
        //review shouldnt disinscribe on discard ?
        GameManager.instance.OnHit += AttackDown;

        target = FindObjectOfType<TargetManager>();

        WeaponFlame(target.PlayerController, true);

        AttackUp(target.PlayerController);
    }   
    
    public void WeaponFlame(PlayerController player, bool activate)
    {
        foreach (PlayerController.Weapon wp in player.weapons)
        {
            wp.w.transform.GetChild(2).gameObject.SetActive(activate);
        }
    }

    public void AttackUp(PlayerController p)
    {
        if (active == false)
        {
            for (int i = 0; i < p.weapons.Count; i++)
            {
                PlayerController.Weapon wp = p.weapons[i];

                WeaponClass weapon = wp.collider.GetComponent<WeaponClass>();

                if (weapon.Atk == 0)
                {
                    weapon.Atk = weapon.baseAtk;
                }

                atks[i] = weapon.Atk;

                weapon.Atk += (weapon.Atk * perc / 100f);
            }

            //foreach (PlayerController.Weapon wp in p.weapons)
            //{
            //    WeaponClass weapon = wp.collider.GetComponent<WeaponClass>();

            //    if (weapon.Atk == 0)
            //    {
            //        weapon.Atk = weapon.baseAtk;
            //    }

            //    at = weapon.Atk;

            //    weapon.Atk += (weapon.Atk * perc / 100f);
            //}

            active = true;
        }
        
        //WeaponClass weapon = p.currentWeapon.transform.GetChild(0).GetComponent<WeaponClass>();
        //WeaponClass weapon = p.weapons[p.indexWeapon].collider.GetComponent<WeaponClass>();
    }

    public void AttackDown(GameObject en, CharacterStatistics st)
    {
        if (active)
        {
            active = false;

            PlayerController p = target.PlayerController;

            WeaponFlame(p, false);

            for (int i = 0; i < p.weapons.Count; i++)
            {
                PlayerController.Weapon wp = p.weapons[i];

                WeaponClass weapon = wp.collider.GetComponent<WeaponClass>();

                weapon.Atk = atks[i];
            }

            //foreach (PlayerController.Weapon wp in p.weapons)
            //{
            //    WeaponClass weapon = wp.collider.GetComponent<WeaponClass>();

            //    weapon.Atk = at;
            //}           

            //WeaponClass weapon = p.currentWeapon.transform.GetChild(0).GetComponent<WeaponClass>();
            //WeaponClass weapon = p.weapons[p.indexWeapon].collider.GetComponent<WeaponClass>();           
        }

        GameManager.instance.OnHit -= AttackDown;

    }
}
