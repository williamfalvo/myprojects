﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Active Skills Effects/Companion")]
public class CompanionAbility : ActiveSkillEffect
{
    public override void Discard()
    {
        target = FindObjectOfType<TargetManager>();

        CompanionActivator companionActivator = target.Player.GetComponent<CompanionActivator>();

        if (companionActivator == null) return;

        companionActivator.canDo = false;
    }

    public override void Use()
    {
        
    }
}
