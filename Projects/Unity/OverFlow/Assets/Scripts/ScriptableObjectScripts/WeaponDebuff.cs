﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponDebuff : Effect
{
    public BuffDebuff debuff;

    public override void Discard()
    {
        
    }

    //apply the debuff on the enemy (probably hitted)
    public override void Use()
    {
        target = FindObjectOfType<TargetManager>();        

        if (enemy != null)
        {
            CharacterStatistics enemyStats = enemy.gameObject.GetComponent<CharacterStatistics>();

            enemyStats.DebuffAdd(debuff);
        }
    }        
}
