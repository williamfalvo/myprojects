﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy Parameters")]
public class EnemyParameters : ScriptableObject
{
    [Space(3), Header("Enemy Parameters")]
    [Tooltip("Enemy run speed")]
    public float speed;
    [Tooltip("Enemy patrol speed")]
    public float patrolSpeed;
    [Tooltip("Minimum distance to attack")]
    public float attackRange;
    [Tooltip("Minumum distance to see the player")]
    public float visionRange;
    [Tooltip("Max time to lose aggro")]
    public float maxAggroTime;

    [Space(3), Header("Champion Parameters")]
    [Tooltip("Red Champ: Attack % Bonus")]
    public float attackBonus;
    [Tooltip("Yellow Champ: Speed % Bonus")]
    public float speedBonus;
    [Tooltip("Green Champ: DoT time in seconds")]
    public float dotTime;
    [Tooltip("Green Champ: DoT damage")]
    public float dotDamage;

}
