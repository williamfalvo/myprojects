﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillInfo1 : MonoBehaviour
{
    MyButton button;

    string description;
    int points;
    bool canDo;

    GameObject cursor;

    void Start()
    {
        button = GetComponentInChildren<MyButton>();
        cursor = transform.GetChild(1).gameObject;
    }

    void Update()
    {
        if (button.Highlight())
        {
            //update the description
            cursor.SetActive(true);
            GameManager.instance.SkillInfos(description, points);
        }
        else
        {
            cursor.SetActive(false);
        }
    }

    public void TakeInfo(string d, int p)
    {
        description = d;
        points = p;
    }
}
