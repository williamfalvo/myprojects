using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//review usefull ?
public class AttributeController1 : AbilityController1
{
    bool start = false;
    GameObject objectToSelect;
    MyButton firstButton;

    new void Start()
    {
        base.Start();

        Init();

        Refresh();

        if (number == 1)
        {
            firstButton = objects[0].GetComponentInChildren<MyButton>();
            firstButton.Select();
            start = true;
        }
    }

    private void OnEnable()
    {
        if (start == true && number == 1)
        {
            firstButton = objects[0].GetComponentInChildren<MyButton>();
            StartCoroutine(test());
        }
    }

    IEnumerator test()
    {
        yield return new WaitForEndOfFrame();
        firstButton.Select();
    }

    public override void Equip(Skill sk, GameObject go)
    {
        Image img = go.transform.GetChild(3).GetComponent<Image>();
        GameObject border = go.transform.GetChild(0).gameObject;

        if (img.color == Color.white && border.activeSelf == false)
        {
            if (justOne)
            {
                List<Skill> skillsToRemove = new List<Skill>();

                foreach (Skill s in GameManager.instance.EquippedSkills)
                {
                    if (s.type == Skill.Types.Attribute)
                    {
                        foreach (Effect ef in s.trigger.effects)
                        {
                            ef.Discard();
                        }

                        GameObject g = objects.Find(GameObject => GameObject.name == s.name);

                        if (g != null)
                        {
                            g.transform.GetChild(0).gameObject.SetActive(false);
                            skillsToRemove.Add(s);
                        }                        

                        justOne = false;                        
                    }
                }

                foreach (Skill skill in skillsToRemove)
                {
                    GameManager.instance.UnequipSkill(skill);
                }

                skillsToRemove.Clear();
            }

            border.SetActive(true);

            GameManager.instance.EquipSkill(sk); //add it to the equipped skill

            foreach (Effect ef in sk.trigger.effects)
            {
                ef.Use();
            }

            justOne = true;
        }
    }

    public override void Init()
    {
        foreach (Skill a in tier.attributes)
        {
            GameObject g = Instantiate(slot, transform);
            g.name = a.name;
            g.transform.GetChild(3).GetComponent<Image>().sprite = a.icon;

            Button b = g.GetComponentInChildren<Button>();
            b.onClick.AddListener(() => Equip(a, g));

            SkillInfo1 info = g.GetComponent<SkillInfo1>();
            info.TakeInfo(a.description, a.abilityPointsNeeded);

            objects.Add(g);
            abilities.Add(a);
        }

        UpdateAbilities();
    }

    public override void Refresh()
    {
        if (tierController.tierUnlocked)
        {
            foreach (Skill a in tier.attributes)
            {
                GameObject g = objects.Find(GameObject => GameObject.name == a.name);
                Image img = g.transform.GetChild(3).GetComponent<Image>();

                img.color = Color.white;

                if (GameManager.instance.IsSkillEquipped(a))
                {
                    g.transform.GetChild(0).gameObject.SetActive(true);
                    justOne = true;
                }
            }
        }
        else
        {
            if (sign != null)
            {
                sign.SetActive(true);
                //Text t = sign.GetComponentInChildren<Text>();
                Text t = sign.transform.GetChild(1).GetComponent<Text>();
                t.text = tier.levelNeeded.ToString();
            }
        }

        //UpdateAbilities();
    }

    public override void UpdateAbilities()
    {
        tier.attributes = abilities;
    }
}
