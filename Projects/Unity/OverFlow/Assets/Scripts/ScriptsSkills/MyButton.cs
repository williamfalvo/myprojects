﻿using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

[System.Serializable]
public class MyButton : Button
{
    private BaseEventData eventData;

    public bool Highlight()
    {
        return IsHighlighted(eventData);
    }
}
