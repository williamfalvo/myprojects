using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Skill")]
public class Skill : ScriptableObject
{
    public enum Types { Passive, Active, Attribute }

    public new string name;

    public Types type;

    [TextArea]
    public string description;

    public Trigger trigger; //trigger that activates it

    public int abilityPointsNeeded; //how many points needed to unlock it

    public Sprite icon;
}
