﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Tier 
{
    public int index;
    public int levelNeeded;
    public List<Skill> skills;
    public List<Skill> attributes;

    public bool TierCheck()
    {
        // checks if the player can access that tier (level based)
        if (GameManager.instance.player.level >= levelNeeded)
        {
            return true;
        }

        return false;
    }

    //constructor
    //public Tier(List<Skill> skills, List<Skill> attributes, int tier, int levelNeeded)
    //{
    //    this.skills = skills;
    //    this.index = tier;
    //    this.levelNeeded = levelNeeded;
    //    this.attributes = attributes;
    //}

    public void CreateClones()
    {
        for (int i = 0; i < skills.Count; i++)
        {
            Skill s = ScriptableObject.Instantiate(skills[i]);

            for (int j = 0; j < s.trigger.effects.Count; j++)
            {
                Effect ef = ScriptableObject.Instantiate(s.trigger.effects[j]);
                s.trigger.effects[j] = ef;
            }

            skills[i] = s;
        }
    }
}
