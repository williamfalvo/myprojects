﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassiveSkill : Skill
{
    //stats to modify
    public float strengthModifier;
    public float intellectModifier;
    public float agilityModifier;
    public float neuralResistenceModifier;
    public float luckModifier;
    public float neuralHealthModifier;   
}
