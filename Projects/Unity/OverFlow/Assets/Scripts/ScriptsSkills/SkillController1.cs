using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//review usefull ?
public class SkillController1 : AbilityController1
{
    bool start = false;
    GameObject objectToSelect;
    MyButton firstButton;

    public GameObject skillsPanel;

    new void Start()
    {
        base.Start();

        Init();

        Refresh();

        if (number == 1)
        {
            firstButton = objects[0].GetComponentInChildren<MyButton>();
            firstButton.Select();
            start = true;
        }
    }

    private void OnEnable()
    {
        if (start == true && number == 1)
        {
            firstButton = objects[0].GetComponentInChildren<MyButton>();
            StartCoroutine(test());
        }
    }

    IEnumerator test()
    {
        yield return new WaitForEndOfFrame();
        firstButton.Select();
    }

    public override void Equip(Skill sk, GameObject go)
    {
        Image img = go.transform.GetChild(3).GetComponent<Image>();
        GameObject border = go.transform.GetChild(0).gameObject;
        SkillInfo1 info = go.GetComponent<SkillInfo1>();

        if (img.color == Color.white && border.activeSelf == false)
        {
            if (justOne)
            {
                List<Skill> skillsToRemove = new List<Skill>();

                foreach (Skill s in GameManager.instance.EquippedSkills)
                {
                    if (s.type != Skill.Types.Attribute)
                    {
                        justOne = false;
                                                
                        GameObject g = objects.Find(GameObject => GameObject.name == s.name);

                        if (g != null)
                        {
                            g.transform.GetChild(0).gameObject.SetActive(false);
                            skillsToRemove.Add(s);
                        }                                               
                    }
                }

                foreach(Skill skill in skillsToRemove)
                {
                    GameManager.instance.UnequipSkill(skill);
                }

                skillsToRemove.Clear();
            }

            border.SetActive(true);
            if (!GameManager.instance.IsSkillUnlocked(sk))
            {
                GameManager.instance.player.skillPoints -= sk.abilityPointsNeeded; //subtact the points needed to unlock the skill
                sk.abilityPointsNeeded = 0;
                info.TakeInfo(sk.description, sk.abilityPointsNeeded);
                GameManager.instance.UnlockSkill(sk);
            }
            GameManager.instance.EquipSkill(sk); //add it to the equipped skill
            justOne = true;
        }

        //UpdateAbilities();

        Refresh();

        ForceRefresh();
    }

    public override void Init()
    {
        foreach (Skill s in tier.skills)
        {
            GameObject g = Instantiate(slot, transform);
            g.name = s.name;
            g.transform.GetChild(3).GetComponent<Image>().sprite = s.icon;

            Button b = g.GetComponentInChildren<Button>();
            b.onClick.AddListener(() => Equip(s, g));

            SkillInfo1 info = g.GetComponent<SkillInfo1>();
            info.TakeInfo(s.description, s.abilityPointsNeeded);

            objects.Add(g);
            abilities.Add(s);
        }

        UpdateAbilities();
    }

    //checks if the next tier is available
    public override void Refresh()
    {
        if (tierController.tierUnlocked)
        {
            NextTierCheck();

            foreach (Skill s in tier.skills)
            {
                GameObject g = objects.Find(GameObject => GameObject.name == s.name);
                Image img = g.transform.GetChild(3).GetComponent<Image>();

                if (GameManager.instance.IsSkillEquipped(s))
                {
                    img.color = Color.white;
                    g.transform.GetChild(0).gameObject.SetActive(true);
                    justOne = true;
                }
                else if (GameManager.instance.IsSkillUnlocked(s))
                {
                    img.color = Color.white;
                }
                else if (!GameManager.instance.IsSkillUnlocked(s))
                {
                    if (TierCheck() && GameManager.instance.player.skillPoints >= s.abilityPointsNeeded)
                    {
                        img.color = Color.white;
                    }
                    else
                    {
                        img.color = new Color(1f, 1f, 1f, 0.5f);
                    }
                }
            }
        }

        //UpdateAbilities();
    }

    public override void UpdateAbilities()
    {
        tier.skills = abilities;
    }

    public bool TierCheck()
    {
        if (number > 1)
        {
            var gm = GameManager.instance;
            foreach (Skill s in TierControllers[number - 2].CurrentTier.skills)
            {
                if (gm.IsSkillUnlocked(s))
                {
                    return true;
                }
            }

            return false;
        }
        else
        {
            return true;
        }
    }

    public void NextTierCheck()
    {
        if (number < TierControllers.Length)
        {
            if (!TierControllers[number].tierUnlocked)
            {
                if (sign != null)
                {
                    sign.SetActive(true);

                    Text t = sign.transform.GetChild(1).GetComponent<Text>();
                    t.text = TierControllers[number].tier.levelNeeded.ToString();
                }
            }
        }        
    }

    public void ForceRefresh()
    {
        foreach (SkillController1 skillCtrl in skillsPanel.GetComponentsInChildren<SkillController1>())
        {
            if (skillCtrl.number != number)
            {
                skillCtrl.Refresh();
            }
        }
    }
}
