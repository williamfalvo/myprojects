﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AbilityTextBehaviour : MonoBehaviour
{
    public TextMeshProUGUI shieldDurationText;
    public TextMeshProUGUI projectileDamageText;
    public TextMeshProUGUI shockwaveDamageText;
    public TextMeshProUGUI shockwaveRangeText;
    public TextMeshProUGUI blackoutDurationText;
    public TextMeshProUGUI companionDamageText;
    public TextMeshProUGUI companionDurationText;

    public Material valueIncreased;
    public Material valueBase;

    CharacterStatistics player;

    private void OnEnable()
    {
        //from the second "OnEnable" starts refreshing
        if(player != null)
        {
            RefreshValues(Stat.tech, (int)player.baseIntellect);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.instance.player;

        //initialize the values
        RefreshValues(Stat.tech, (int)player.baseIntellect);
    }



    // Update is called once per frame
    void Update()
    {
        
    }

    public void RefreshValues(Stat stat , int value)
    {
        

        if(stat == Stat.tech)
        {
            Material materialToAssign;

            if(player.baseIntellect == value)
            {
                materialToAssign = valueBase;
            }
            else
            {
                materialToAssign = valueIncreased;
            }
            

            shieldDurationText.text = player.intellectValues[value-1].shieldDuration.ToString();
            shieldDurationText.fontMaterial = materialToAssign;

            projectileDamageText.text = player.intellectValues[value-1].projectileDamage.ToString();
            projectileDamageText.fontMaterial = materialToAssign;

            shockwaveDamageText.text = player.intellectValues[value-1].shockwaveDamage.ToString();
            shockwaveDamageText.fontMaterial = materialToAssign;

            shockwaveRangeText.text = player.intellectValues[value-1].shockwaveRange.ToString();
            shockwaveRangeText.fontMaterial = materialToAssign;

            blackoutDurationText.text = player.intellectValues[value-1].blackoutDuration.ToString();
            blackoutDurationText.fontMaterial = materialToAssign;

            companionDamageText.text = player.intellectValues[value-1].companionDamage.ToString();
            companionDamageText.fontMaterial = materialToAssign;

            companionDurationText.text = player.intellectValues[value-1].companionDuration.ToString();
            companionDurationText.fontMaterial = materialToAssign;
        }
    }
}
