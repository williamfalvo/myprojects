﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TierController : MonoBehaviour
{
    public Tier tier;

    [HideInInspector]
    public bool tierUnlocked;

    private Tier currentTier;

    public Tier CurrentTier
    {
        get { return currentTier; }
        set { currentTier = value; }
    }    

    private void Awake()
    {
        tier.CreateClones();

        tierUnlocked = currentTier.TierCheck(); //check every awake if the tier has been unlocked      
    }

    private void OnDisable()
    {
        UpdateTier();
    }

    public void UpdateTier()
    {
        if (currentTier == null)
        {
            if (GameManager.instance.skillTree.CurrentTiers[tier.index] != null)
            {
                currentTier = GameManager.instance.skillTree.CurrentTiers[tier.index];
            }
            else
            {
                currentTier = tier;
            }
        }
        GameManager.instance.skillTree.CurrentTiers[tier.index] = currentTier;
    }
}
