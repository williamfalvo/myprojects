﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class AbilityController : MonoBehaviour
{
    public GameObject slot; // prefab of the card shape
    public GameObject panel; // where to instantiate the cardslot

    [HideInInspector]
    public Tier tier; //what tier this controller manages

    [HideInInspector]
    public TierController tierController;

    [HideInInspector]
    public bool justOne = false;

    [HideInInspector]
    public List<GameObject> objects = new List<GameObject>(); // list of the card slots instantiated

    [HideInInspector]
    public List<Skill> abilities = new List<Skill>(); // list of the skills in that panel

    public void Start()
    {
        tierController = GetComponentInParent<TierController>();
        tier = tierController.CurrentTier;
    }

    public abstract void Init();

    public abstract void Refresh();

    public abstract void Equip(Skill s, Image img);

    public abstract void UpdateAbilities();
}
