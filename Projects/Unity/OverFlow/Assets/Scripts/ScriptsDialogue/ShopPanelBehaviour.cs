﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//scripts on the content panel of the real shop panel
public class ShopPanelBehaviour : MonoBehaviour
{
    #region Fields

    public GameObject endDialoguePanelNoExchange;
    public GameObject endDialoguePanelAfterExchange;
    public GameObject confirmExchangePanel;
    public GameObject shopPanel;
    public NPCMerchant merchant;
    public bool exchangePerformed;
    bool doInitPanelOnlyOneTime = true;

    #endregion

    #region Methods

    private void OnEnable()
    {
        StartCoroutine(SlowDown());
        //initialize all the trades

        merchant.SelectTrade();

        if (doInitPanelOnlyOneTime)
        {
            merchant.InitializePanel();
            doInitPanelOnlyOneTime = false;
        }
        //merchant.InitializePanel();


    }

    private void OnDisable()
    {
        merchant.firstSelectedFound = false;

    }

    private void Start()
    {
       
    }

    private void Update()
    {
        //chose wich endPanel has to show depending if the trade ended or not
        if (!exchangePerformed && Input.GetButtonDown("Cancel"))
        {
            if (!confirmExchangePanel.activeSelf)
            {
                shopPanel.SetActive(false);
                endDialoguePanelNoExchange.SetActive(true);
            }
            else
            {
                confirmExchangePanel.SetActive(false);
                return;
            }
        }

        if (exchangePerformed && Input.GetButtonDown("Cancel"))
        {
            if (!confirmExchangePanel.activeSelf)
            {
                shopPanel.SetActive(false);
                endDialoguePanelAfterExchange.SetActive(true);
            }
            else
            {
                confirmExchangePanel.SetActive(false);
                return;
            }
            
        }
    }

    
    public void CheckAvailableTrades()
    {
        //checks the first trade available and set it as current selected
        for (int i = 0; i < transform.childCount; i++)
        {
            Button thisButton;
            Exchange thisExchange;

            thisButton = transform.GetChild(i).transform.GetChild(5).GetComponent<Button>();
            thisExchange = transform.GetChild(i).GetComponent<Exchange>();

            thisExchange.CheckExchange();

            if(thisButton.GetComponent<Button>().interactable == true)
            {
                EventSystem.current.SetSelectedGameObject(thisButton.gameObject);
                break;
            }

            //if no trade has been found the scrollbar goes to the top and no object gets selected
            if( i == transform.childCount-1 )
            {
               
                MerchantScrollbarManager merchantBar = GetComponent<MerchantScrollbarManager>();
                merchantBar.scrollbar.value = 1;
                EventSystem.current.SetSelectedGameObject(null);
                merchantBar.currentHoveredCard = null;
                merchantBar.prevHoveredCard = null;
            }
            
        }

        //checks all the trades and activates the button if the trade is viable
        for (int i = 0; i < transform.childCount; i++)
        {
            Exchange thisExchange;
            thisExchange = transform.GetChild(i).GetComponent<Exchange>();
            thisExchange.CheckExchange();
        }

    }

    //this method gets called every "OnEnable" after the panel gets initialized (so after the first "OnEnable")
    //and checks all the available trades
    void CheckAvailableTradesOnEnable()
    {
        
        if (transform.childCount > 0)
        {
            for(int i = 0; i< transform.childCount; i++)
            {
                transform.GetChild(i).GetComponent<Exchange>().CheckExchange();
            }
        }
    }

    //to wait till the childs do the "OnEnable" 
    IEnumerator SlowDown()
    {
        yield return new WaitForSeconds(0.1f);
        CheckAvailableTradesOnEnable();
    }

    #endregion


}
