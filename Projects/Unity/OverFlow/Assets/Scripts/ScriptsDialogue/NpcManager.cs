﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct AssignNPC
{
    public int missionIndex;
    public DialogueSystem NPC;
}

public class NpcManager : MonoBehaviour
{
    public Material hologram;
    public AssignNPC[] npcList;

    void Start()
    {        
        Plot[] plots = GameManager.instance.missions;

        for(int i= 0; i < npcList.Length; i++)
        {
           // plots[npcList[i].missionIndex].Npc = npcList[i].NPC;

            if (!plots[i].missionReport && npcList[i].NPC != null)
            {
                npcList[i].NPC.GetComponentInChildren<SkinnedMeshRenderer>().material = hologram;
                npcList[i].NPC.enabled = false;
            }
        }

        //if we completed the related quest , turns the NPC on
        //for (int i = 0; i < plots.Length; i++)
        //{
        //    if(plots[i].Npc != null && !plots[i].missionReport)
        //    {
        //        plots[i].Npc.GetComponentInChildren<SkinnedMeshRenderer>().material = hologram;
        //        plots[i].Npc.enabled = false;
        //    }            
        //}
    }
}
