﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ExchangePanelInformerBehaviour : MonoBehaviour
{
    #region Fields

    public int loadingBarSpeed;
    float currentLoading;
    public GameObject loadingBar;
    public GameObject firstDialoguePanel;
    public GameObject endDialoguePanelNoExchange;
    public GameObject endDialoguePanelAfterExchange;
    public GameObject noMoreExchangesPanel;
    public Button buttonToChangeBehav; // after we finishe all the exchanges we have to force this button not to

    public int deckIncrease = 0;
    public Image cardImage;
    public NPCRigger rigger;

    bool tradeCompleted;

    #endregion

    #region Methods

    private void OnEnable()
    {
        cardImage.sprite = rigger.cardsRequired[rigger.currentExchangeIndex].cardToGiveImg.artwork;
        loadingBar.GetComponent<Image>().fillAmount = 0;
    }

    void Update()
    {
        loadingBar.GetComponent<Image>().fillAmount = 0;
        if (gameObject.activeSelf)
        {
            GameManager.instance.inputManager.enabled = false;

            if (Input.GetButton("Submit"))
            {
                currentLoading += loadingBarSpeed;
                loadingBar.GetComponent<Image>().fillAmount = currentLoading / 100;
                if ((int)currentLoading >= 100)
                {
                    
                    MakeExchange();//removes the cards from the player
                    rigger.currentExchangeAvailable = false; //reset the bool the initialize request will set it to true if we own the cards
                    rigger.currentExchangeIndex++;//goes to the next exchange

                    if(rigger.currentExchangeIndex < rigger.cardsRequired.Length)
                    {
                        rigger.InitializeRequest();//update the next exchange
                    }
                    else
                    {
                        buttonToChangeBehav.onClick.AddListener( () => noMoreExchangesPanel.SetActive(true));
                    }


                    

                    rigger.showExchangePanel.SetActive(false);
                    firstDialoguePanel.SetActive(false);
                    endDialoguePanelAfterExchange.SetActive(true);
                    gameObject.SetActive(false);



                }
            }
            else if(Input.GetButton("Cancel"))//cancel the trade
            {
                endDialoguePanelNoExchange.SetActive(true);
                gameObject.SetActive(false);
                firstDialoguePanel.SetActive(false);
            }
            else
            {
                currentLoading = 0;
            }
        }
    }

    //removes the cards from the player inventory/deck
    void MakeExchange()
    {
        int howManyFound = 0;
        tradeCompleted = false;

        List<Card> cardsToRemove = new List<Card>();

        //checks if we can find all the cards we need in the deck
        foreach (Card c in GameManager.instance.inventory.cards)
        {
            if (rigger.cardsRequired[rigger.currentExchangeIndex].cardToGiveImg.name == c.name)
            {
                cardsToRemove.Add(c);
                howManyFound++;

                if (howManyFound == rigger.cardsRequired[rigger.currentExchangeIndex].cardsToGiveN)
                {
                    //founds enough cards in the inventory
                    tradeCompleted = true;
                    break;
                }
            }
        }

        //actually removes the cards from inventory
        foreach (Card c in cardsToRemove)
        {

            GameManager.instance.inventory.RemoveCard(c);

            Dictionary<string, HubInventoryController1.CardElements> dict = GameManager.instance.Dict;
            if (dict.ContainsKey(c.name) && dict[c.name].counter > 0)
            {
               dict[c.name].counter--;
            }
        }
        cardsToRemove = new List<Card>();

        //not enough cards in the inventory to remove , then checks the deck
        if (!tradeCompleted)
        {
            foreach (Card c in GameManager.instance.deck.cards)
            {
                if (rigger.cardsRequired[rigger.currentExchangeIndex].cardToGiveImg.name == c.name)
                {
                    cardsToRemove.Add(c);
                    howManyFound++;

                    if (howManyFound == rigger.cardsRequired[rigger.currentExchangeIndex].cardsToGiveN)
                    {
                        tradeCompleted = true;
                        break;
                    }
                }
            }

            foreach (Card c in cardsToRemove)
            {
                GameManager.instance.deck.RemoveCard(c);


                Dictionary<string, HubInventoryController1.CardElements> dict = GameManager.instance.Dict;
                if (dict.ContainsKey(c.name) && dict[c.name].counter > 0)
                {
                    dict[c.name].counter--;
                }
            }


        }

        GameManager.instance.deck.maxCards += deckIncrease;
        currentLoading = 0;
    }


    #endregion
}
