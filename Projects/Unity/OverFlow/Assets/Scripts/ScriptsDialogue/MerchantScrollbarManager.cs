﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MerchantScrollbarManager : MonoBehaviour
{
    public RectTransform panelRectTransform; // panel that contain us
    public Scrollbar scrollbar; //the scrollbar we need to change the value to

    [HideInInspector]
    public GameObject currentHoveredCard;
    [HideInInspector]
    public GameObject prevHoveredCard;

    float panelHeight;
    float topThreshold;
    float botThreshold;

    public bool exchangePanelOpen;

    void Start()
    {
        scrollbar.value = 1;

        panelHeight = panelRectTransform.rect.height; // height of the panel

        topThreshold = panelRectTransform.position.y + panelHeight / 4; //if hovered trade y is bigger then this value then the scroll has to move up
        botThreshold = panelRectTransform.position.y - panelHeight / 4; //if hovered trade y is lower then this value then the scroll has to move down
    }

    void Update()
    {
        if (!exchangePanelOpen)
        {
            currentHoveredCard = EventSystem.current.currentSelectedGameObject; // get the card hovered        

            if (currentHoveredCard != null)
            {
                // move till the position of the card is visible
                if (currentHoveredCard.transform.position.y < botThreshold)
                {
                    IncreaseScrollbarValue(0.05f, scrollbar);
                }
                if (currentHoveredCard.transform.position.y > topThreshold)
                {
                    DecreaseScrollbarValue(0.05f, scrollbar);
                }

                if (currentHoveredCard == prevHoveredCard)
                {
                    return;
                }

                prevHoveredCard = currentHoveredCard; // to see if the selected card is changed
            }
        }
    }

    private void IncreaseScrollbarValue(float value, Scrollbar myScrollbar)
    {
        myScrollbar.value -= value;
    }

    private void DecreaseScrollbarValue(float value, Scrollbar myScrollbar)
    {
        myScrollbar.value += value;
    }
}

