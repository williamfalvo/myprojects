﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MerchantGridSetter : MonoBehaviour
{
    public GameObject tradeSlot;
    RectTransform tradeSlotPanel;
    RectTransform myPanel;
    GridLayoutGroup myGrid;

    float width;

    // Start is called before the first frame update
    void Start()
    {

        tradeSlotPanel = tradeSlot.GetComponent<RectTransform>();
        
        myPanel = GetComponent<RectTransform>();
        myGrid = GetComponent<GridLayoutGroup>();

        width = myPanel.rect.width;

        myGrid.cellSize = new Vector2(width, 100);

    }

}
