﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class NPCRigger : DialogueSystem
{
    public GameObject showExchangePanel;
    public GameObject confirmExchangePanel;
    public Image cardimage;

    [Header ("TEXTS") , Space (10)]
    public TextMeshProUGUI cardCount;
    public TextMeshProUGUI slotsUnlocked;
    public TextMeshProUGUI textCost;
    public Material tradeAvailable;
    public Material tradeUnvailable;


    public ExchangeSystem[] cardsRequired;
    public GameObject noMoreExchangingsPanel;

    [HideInInspector]
    public int currentExchangeIndex = 0; //says which exchange are we at
    [HideInInspector]
    public bool currentExchangeAvailable = false;
    public bool ready = false;
        
    #region Methods

    private void OnEnable()
    {
        ready = false;
        currentExchangeAvailable = false;
        if (GameManager.instance != null)
        {
            InitializeRequest();
        }
    }

    private void Start()
    {
        InitializeRequest();
    }

    protected override void Update()
    {
        base.Update();

        if(showExchangePanel.activeSelf)
        {

            //activates the exchange confirm panel
            if (Input.GetButtonDown("Submit") && currentExchangeAvailable && ready)
            {
                confirmExchangePanel.GetComponent<ExchangePanelInformerBehaviour>().deckIncrease = cardsRequired[currentExchangeIndex].cardsToReceiveN;
                confirmExchangePanel.SetActive(true);
                showExchangePanel.SetActive(false);
                ready = false;
            }
            if(Input.GetButtonDown("Cancel"))
            {
                GameManager.instance.inputManager.enabled = true;
                //GameManager.instance.SetPause();
                showExchangePanel.SetActive(false);
                canStartDialogue = true;
                if (!noMoreExchangingsPanel.activeSelf)
                {
                    endDialoguePanelNoExchange.SetActive(true);
                }
                else
                {
                    noMoreExchangingsPanel.SetActive(false);
                }
                ready = false;
            }
        }
    }

    //update the request with the current cards needed
    public void UpdateRequest()
    {
        if (currentExchangeIndex < cardsRequired.Length)
        {
            cardimage.sprite = cardsRequired[currentExchangeIndex].cardToGiveImg.artwork;
            cardCount.text = "x " + cardsRequired[currentExchangeIndex].cardsToGiveN.ToString() + "  " + cardsRequired[currentExchangeIndex].cardToGiveImg.name;
            slotsUnlocked.text = "+  " + cardsRequired[currentExchangeIndex].cardsToReceiveN.ToString() + " Deck Slots";
        }
    }

    //checks if the trade is doable
    void CheckCurrentExchange()
    {
        int howManyFound = 0;

        //checks if we own the card (first checks in the inventory)
        foreach (Card c in GameManager.instance.inventory.cards)
        {
            if (c.name == cardsRequired[currentExchangeIndex].cardToGiveImg.name)
            {
                howManyFound++;
                if (howManyFound == cardsRequired[currentExchangeIndex].cardsToGiveN)
                {
                    currentExchangeAvailable = true;
                    break;
                }
            }
        }

        //if not all the cards have been found in the inventory we check in the deck
        if (howManyFound < cardsRequired[currentExchangeIndex].cardsToGiveN)
        {
            foreach (Card c in GameManager.instance.deck.cards)
            {
                if (c.name == cardsRequired[currentExchangeIndex].cardToGiveImg.name)
                {
                    howManyFound++;
                    if (howManyFound == cardsRequired[currentExchangeIndex].cardsToGiveN)
                    {
                        //if we have all the cards needed exchange has been found
                        currentExchangeAvailable = true;
                        break;
                    }

                }
            }
        }

        if(currentExchangeAvailable)
        {
            cardCount.fontMaterial = tradeAvailable;
            textCost.fontMaterial = tradeAvailable;
            slotsUnlocked.fontMaterial = tradeAvailable;
        }
        else
        {
            cardCount.fontMaterial = tradeUnvailable;
            textCost.fontMaterial = tradeUnvailable;
            slotsUnlocked.fontMaterial = tradeUnvailable;
        }
    }

    public void GetExchangePanelReady()
    {
        StartCoroutine(WaitFrame());
    }

    //waiting a frame to not get the input that would open the next panel
    IEnumerator WaitFrame()
    {
        showExchangePanel.SetActive(true);
        yield return new WaitForEndOfFrame();
        ready = true;
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void InitializeRequest()
    {
        UpdateRequest();
        CheckCurrentExchange();            
    }


    #endregion
}
