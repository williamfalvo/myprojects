﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//script on the shop panel prefab button
public class EnableConfirmExchangePanel : MonoBehaviour
{
    #region Methods
    public Card cardToGive;
    public Card cardToReceive;
    public int cardsToGiveN;
    public int cardsToReceiveN;
    public Exchange myExchange;


    [HideInInspector]
    public GameObject confirmExchangePanel;

    private void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(PanelToActivate);
    }

    void PanelToActivate()
    {
        if (myExchange.exchangeFound)
        {
            //give the exchange panel all the infos it needs about the current exchange
            ExchangePanelBehaviour thisExchangePanel = confirmExchangePanel.GetComponent<ExchangePanelBehaviour>();

            confirmExchangePanel.SetActive(true);
            thisExchangePanel.cardToGive = cardToGive;
            thisExchangePanel.cardToReceive = cardToReceive;
            thisExchangePanel.cardToGiveN = cardsToGiveN;
            thisExchangePanel.cardToReceiveN = cardsToReceiveN;
            thisExchangePanel.UpdateImages();
        }
    }

    #endregion
}
