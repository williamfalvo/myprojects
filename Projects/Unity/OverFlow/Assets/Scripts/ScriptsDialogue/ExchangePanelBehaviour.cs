﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//script on the last panel before the exchange gets execute
public class ExchangePanelBehaviour : MonoBehaviour
{
    #region Fields

    public GameObject loadingBar;
    public GameObject shopPanel;
    public Transform contentShopPanel;
    public GameObject shopPanelSelectionButton;

    [HideInInspector]
    public Card cardToGive;
    [HideInInspector]
    public Card cardToReceive;
    [HideInInspector]
    public int cardToReceiveN;
    [HideInInspector]
    public int cardToGiveN;

  
    public Image cardToGiveImage;
    public Image cardToReceiveImage;
    public int loadingBarSpeed;

    bool exchangeFound = false;
    bool tradeCompleted = false;
    float currentLoading;


    #endregion

    #region Methods

    private void OnEnable()
    {
        contentShopPanel.GetComponent<MerchantScrollbarManager>().exchangePanelOpen = true;

        tradeCompleted = false;

    }

    private void OnDisable()
    {
        MerchantScrollbarManager merchantBar = contentShopPanel.GetComponent<MerchantScrollbarManager>();
        merchantBar.exchangePanelOpen = false;

        //set again the current selected to the first trade available
        contentShopPanel.GetComponent<ShopPanelBehaviour>().CheckAvailableTrades();
        loadingBar.GetComponent<Image>().fillAmount = 0;
        currentLoading = 0;
    }

    private void Update()
    {
        //fill bar
        loadingBar.GetComponent<Image>().fillAmount = 0;
  

        //if submit is pressed the fill circle starts charging
        if (Input.GetButton("Submit"))
        {
            currentLoading += loadingBarSpeed;
            loadingBar.GetComponent<Image>().fillAmount = currentLoading / 100;

            //if fully charge starts the exchange
            if ((int)currentLoading >= 100)
            {
                //make the exchange O_O
                MakeExchange();
            }

            if (Input.GetButton("Cancel") || tradeCompleted)
            {
                contentShopPanel.GetComponent<ShopPanelBehaviour>().exchangePerformed = true;     //non cambiare
                gameObject.SetActive(false);
            }
       
        }
        else
        {
            currentLoading = 0f;
        }
     
    }

    void MakeExchange()
    {
        int howManyFound = 0;
        tradeCompleted = false;

        List<Card> cardsToRemove = new List<Card>();

        //checks if we can find all the cards we need in the deck
        foreach (Card c in GameManager.instance.inventory.cards)
        {
            if (cardToGive.name == c.name)
            {
                cardsToRemove.Add(c);
                howManyFound++;

                if (howManyFound == cardToGiveN)
                {
                    //founds enough cards in the inventory
                    tradeCompleted = true;
                    break;
                }
            }
        }

        //actually removes the cards from inventory
        foreach (Card c in cardsToRemove)
        {
            GameManager.instance.inventory.RemoveCard(c);


            Dictionary<string, HubInventoryController1.CardElements> dict = GameManager.instance.Dict;
            if (dict.ContainsKey(c.name) && dict[c.name].counter > 0)
            {
                dict[c.name].counter--;
            }
        }
        cardsToRemove = new List<Card>();

        //not enough cards in the inventory to remove , then checks the deck
        if (!tradeCompleted)
        {
            foreach (Card c in GameManager.instance.deck.cards)
            {
                if (cardToGive.name == c.name)
                {
                    cardsToRemove.Add(c);
                    howManyFound++;

                    if (howManyFound == cardToGiveN)
                    {
                        tradeCompleted = true;
                        break;
                    }
                }
            }

            foreach (Card c in cardsToRemove)
            {
                GameManager.instance.deck.RemoveCard(c);

                Dictionary<string, HubInventoryController1.CardElements> dict = GameManager.instance.Dict;
                if (dict.ContainsKey(c.name) && dict[c.name].counter > 0)
                {
                    dict[c.name].counter--;
                }
            }


        }

        //add the cards receive to the player inventory
        for(int i = 0; i < cardToReceiveN; i++)
        {
            GameManager.instance.inventory.AddCard(cardToReceive);

            Dictionary<string, HubInventoryController1.CardElements> dict = GameManager.instance.Dict;

            GameManager.instance.temporary.AddCard(cardToReceive);
        }

        currentLoading = 0;
    }

    public void UpdateImages()
    {
        cardToGiveImage.sprite = cardToGive.artwork;
        cardToReceiveImage.sprite = cardToReceive.artwork;
    }

    #endregion
}
