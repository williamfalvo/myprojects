﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class NPCCommon : DialogueSystem
{
    #region Fields

    public string[] infoTexts;
    public Color[] textColors;
    public GameObject[] infoButtons;
    public TextMeshProUGUI toChangeWithInfoTexts;
    public GameObject infoShowPanel;
    public GameObject firstDialoguePanel;
    public GameObject infoDialogue;
    bool activeOneFrame = false;

    #endregion

    #region Methods

    protected override void Update()
    {
        base.Update();

        if(infoShowPanel.activeSelf)
        {
            if(Input.GetButtonDown("Cancel")|| Input.GetButtonDown("Submit") && activeOneFrame)
            {
                

                infoDialogue.SetActive(true);
                infoShowPanel.SetActive(false);
                EventSystem.current.SetSelectedGameObject(FirstButtonInfoDialogue);
                activeOneFrame = false;
            }
        }
    }

    public void InfoToChange(int text)
    {
        toChangeWithInfoTexts.text = infoTexts[text];
    }

    public void ColorInfoToChange(int text)
    {
    }

    public void ActivePanelDelayed()
    {
        StartCoroutine(WaitFrame());
    }

    public IEnumerator WaitFrame()
    {
        infoShowPanel.SetActive(true);
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(null);
        
        activeOneFrame = true;
    }



    #endregion
}
