﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FirstDialoguePanelBehaviour : MonoBehaviour
{
    #region Fields

    public GameObject firstButtonFirstDialogue;

    #endregion

    #region Methods

    private void Start()
    {
        EventSystem.current.SetSelectedGameObject(firstButtonFirstDialogue);
    }

    #endregion

}
