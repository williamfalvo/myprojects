﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndDialogueBehaviour : MonoBehaviour
{


    #region Methods
    
    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf)
        {
            if (Input.GetButtonDown("Submit") || Input.GetButtonDown("Cancel"))
            {

                if (transform.root.GetComponent<NPCMerchant>() != null)
                {
                    transform.root.GetComponent<NPCMerchant>().canStartDialogue = true;
                }
                else if (transform.root.GetComponent<NPCRigger>() != null)
                {
                    transform.root.GetComponent<NPCRigger>().canStartDialogue = true;
                }
                else if (transform.root.GetComponent<NPCCommon>() != null)
                {
                    transform.root.GetComponent<NPCCommon>().canStartDialogue = true;
                }

                gameObject.SetActive(false);
                GameManager.instance.inputManager.enabled = true;
                GameManager.instance.SetPause();
            }
        }
    }

    #endregion
}
