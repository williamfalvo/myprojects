﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jailer : Enemy
{
    [Header("Jailer Parameters")]
    public GameObject jailerDevice;
    [HideInInspector]
    public bool canAttack;
    [Tooltip("Time in seconds to attend to next attack")]
    public float attackDelay;

    IEnumerator CheckStun()
    {
        //Attend that player stun is finished
        yield return new WaitUntil(() => player.GetComponent<PlayerController>().playerState != PlayerController.PlayerState.stun);

        StartCoroutine(CountdownAttack());
    }

    IEnumerator CountdownAttack()
    {
        //Wait X seconds to enable next attack
        yield return new WaitForSeconds(attackDelay);

        canAttack = true;
    }

    //if the player gets hitted
    public void PlayerHitted()
    {
        StartCoroutine(CheckStun());
        //FMODManager.instance.PlayeJailerAttack();
    }

    //if the player is missed after the attack
    public void PlayerMiss()
    {
        StartCoroutine(CountdownAttack());
    }

}
