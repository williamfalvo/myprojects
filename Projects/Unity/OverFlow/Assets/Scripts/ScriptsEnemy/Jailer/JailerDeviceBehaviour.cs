﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JailerDeviceBehaviour : MonoBehaviour
{
    [HideInInspector]
    public Jailer jailer;
    [HideInInspector]
    public Rigidbody rb;

    [Tooltip("Impulse shoot")]
    public float speed;
    [Tooltip("Time that stun player")]
    public float stunTime;

    public float angle = 45f;
    public float gravity = 9.8f;

    public GameObject explosion;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnDestroy()
    {
        GameObject fx = Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(fx, 2.1f);
    }

    private void OnTriggerEnter(Collider other)
    {
        //If touch Ground or Map layer, say to jailer that the shot missed player, else hitted player isn't invulnerable
        if(other.gameObject.layer == 9 || other.gameObject.layer == 12)
        {
            jailer.PlayerMiss();
            Destroy(gameObject);
        }
        else if(other.tag == "Player")
        {
            PlayerController playerC = other.GetComponent<PlayerController>();
            if (!playerC.isInvulnerable)
            {
                //Set player stunned for "stunTime"
                playerC.Stun(stunTime);
                jailer.PlayerHitted();
                Destroy(gameObject);
            }
        }
    }

    public IEnumerator Launch(Vector3 target)
    {
        yield return null;        

        float targetDistance = Vector3.Distance(transform.position, target); // gets the distance from "target"

        float bVelocity = (targetDistance / (Mathf.Sin(2 * angle * Mathf.Deg2Rad) / gravity));//calculates the velocity

        float vx = Mathf.Sqrt(bVelocity) * Mathf.Cos(angle * Mathf.Deg2Rad); //separate it in the 2 components x,y
        float vy = Mathf.Sqrt(bVelocity) * Mathf.Sin(angle * Mathf.Deg2Rad);

        transform.rotation = Quaternion.LookRotation(target - transform.position);       

        float elapsedTime = 0f;

        while (gameObject != null)
        {
            transform.Translate(0f, (vy - (gravity * elapsedTime)) * Time.deltaTime , (vx * Time.deltaTime));//movement of the projectile

            elapsedTime += Time.deltaTime;

            yield return null;
        }
    }

    public void NewTarget(Vector3 target)
    {
        //Set direction of bullet
        float direction = Mathf.Sign(target.x - transform.position.x);
        target += new Vector3(0, 1, 0);
        //Set velocity of bullet
        rb.velocity = Vector3.Normalize(target - transform.position) * speed;
    }
}
