﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JailerBehaviour : EnemyBehaviour
{
    public float xSpeed;
    public float ySpeed;
    bool playSound = true;
    public float cooldownSound;

    public override void EnterSearchB(Enemy enemy)
    {
        base.EnterSearchB(enemy);

        //if (playSound)
        //{
        //    FMODManager.instance.HumanEnemiesTriggered();
        //    playSound = false;
        //    StartCoroutine(CooldownAfterFirstAttack());
        //}
    }

    public override void UpdateSearchB(Enemy enemy)
    {
        if (CheckPlayerNode(enemy)) return;

        Jailer jailer = enemy as Jailer;

        if (playSound)
        {
            FMODManager.instance.HumanEnemiesTriggered();
            playSound = false;
            StartCoroutine(CooldownAfterFirstAttack());
        }

        //Calculate the distance between Mirage and player
        float distance = (jailer.player.transform.position - transform.position).sqrMagnitude;        

        //Check if Jailer reach the minimum distance, else try to attack
        if (distance > jailer.attackRange * jailer.attackRange)
        {
            //Check if enemy lose the aggro
            if (distance > jailer.visionRange * jailer.visionRange)
            {
                jailer.loseAggroTimer += Time.deltaTime;

                if (jailer.loseAggroTimer >= jailer.maxAggroTime)
                {
                    //Return to patrol state
                    ExitSearchB(enemy);
                    EnterPatrolB(enemy);
                    jailer.state = Enemy.EnemyState.patrol;
                    return;
                }
            }
            else
            {
                //Reset the timer of lose aggro
                jailer.loseAggroTimer = 0;
            }

            jailer.anim.SetBool("Run", true);
            jailer.anim.SetBool("PrepareAttack", false);
            jailer.pathFollow.PathFollow();
        }
        else
        {
            //Look to player position
            float direction = Mathf.Sign(jailer.player.transform.position.x - transform.position.x);
            transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);

            jailer.anim.SetBool("PrepareAttack", true);
            jailer.anim.SetBool("Run", false);
            if (jailer.canAttack && jailer.onFloor)
            {
                ExitSearchB(jailer);
                jailer.anim.SetBool("PrepareAttack", false);
                EnterAttackB(jailer);
            }
        }
    }

    public override void EnterAttackB(Enemy enemy)
    {
        base.EnterAttackB(enemy);
    }

    public override void UpdateAttackB(Enemy enemy)
    {
        Jailer jailer = enemy as Jailer;

        if (jailer.canAttack)
        {
            //Launch jailer device
            JailerDeviceBehaviour device = Instantiate(jailer.jailerDevice, transform.position + new Vector3(0, 1, 0), Quaternion.identity).GetComponent<JailerDeviceBehaviour>();
            device.GetComponent<Rigidbody>().AddForce( new Vector3( transform.localScale.x * xSpeed, ySpeed, 0 ), ForceMode.Impulse);
            //Set all reference to jailer device
            device.jailer = jailer;

            //device.NewTarget(jailer.player.transform.position);
            //device.StartCoroutine(device.Launch(jailer.player.transform.position));
            WeaponClass weapon = device.GetComponent<WeaponClass>();
            weapon.ownerStats = GetComponent<CharacterStatistics>();
            weapon.WeaponTypeCases();

            //Don't allow jailer to attack
            jailer.canAttack = false;
        }
    }

    public IEnumerator CooldownAfterFirstAttack()
    {
        yield return new WaitForSeconds(cooldownSound);
        playSound = true;
    }
}
