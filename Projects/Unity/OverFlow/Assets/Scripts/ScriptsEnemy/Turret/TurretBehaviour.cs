﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBehaviour : EnemyBehaviour
{
    public GameObject cannon;
    bool rotationStarted;
    float cannonDirection;

    private void Awake()
    {
        cannonDirection = 1f;
    }

    public override void EnterPatrolB(Enemy enemy)
    {
        enemy.state = Enemy.EnemyState.patrol;
    }

    public override void UpdatePatrolB(Enemy enemy)
    {
        //Calculate the distance between enemy and player position        
        float distance = (enemy.player.transform.position - transform.position).sqrMagnitude;

        //Check if player is on enemy attack range
        if (distance < enemy.attackRange * enemy.attackRange)
        {
            EnterAttackB(enemy);
            return;
        }
    }

    public IEnumerator ShootFx(Turret turret, float direction)
    {
        //Instantiate new bullet
        turret.attackFinished = false;

        Vector3 bulletPos = transform.position + new Vector3(turret.offsetBulletSpawn.x * direction, turret.offsetBulletSpawn.y, 0);
        GameObject ch = Instantiate(turret.chargeFx, bulletPos, Quaternion.identity);
        float vfxTime = ch.GetComponent<ParticleSystem>().main.duration - 1f;
        Destroy(ch, 3f);

        yield return new WaitForSeconds(vfxTime);

        GameObject bullet = Instantiate(turret.bullet, bulletPos, Quaternion.identity);

        //Apply the reference of its stats
        BulletWeapon bulletBe = bullet.GetComponent<BulletWeapon>();
        bulletBe.ownerStats = GetComponent<CharacterStatistics>();
        bulletBe.WeaponTypeCases();

        //Set the rotation of bullet to player position                
        bullet.transform.localEulerAngles = new Vector3(0, 0, 90);

        //Apply a force to shoot
        bullet.GetComponent<Rigidbody>().velocity = Vector3.right * direction * turret.bulletSpeed;

        //Set next cooldown to allow shoot
        turret.cooldownAttack = 0f;

        turret.attackFinished = true;
    }

    public override void UpdateAttackB(Enemy enemy)
    {
        Turret turret = enemy as Turret;

        //Calculate the distance between enemy and player position        
        float distance = (enemy.player.transform.position - transform.position).sqrMagnitude;

        //Check if player is on enemy attack range, return to patrol state
        if (distance < enemy.attackRange * enemy.attackRange)
        {
            //Look at player position
            float direction = Mathf.Sign((enemy.player.transform.position.x - transform.position.x));
            //transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);
            if(direction != cannonDirection && turret.attackFinished && !rotationStarted)
            {
                rotationStarted = true;
                turret.canAttack = false;
                StartCoroutine(Rotate(turret, direction));
            }

            //Check if enemy can attack
            if (turret.cooldownAttack < turret.attackDelay)
            {
                turret.cooldownAttack += Time.deltaTime;
            }
            else if(turret.attackFinished && turret.canAttack)
            {
                StartCoroutine(ShootFx(turret, direction));
            }
            return;
        }
        else
        {
            //Set next cooldown to allow shoot
            turret.cooldownAttack = 0f;

            //Return to patrol state
            ExitAttackB(enemy);
            EnterPatrolB(enemy);
            enemy.state = Enemy.EnemyState.patrol;
        }
    }

    IEnumerator Rotate(Turret turret, float direction)
    {        
        WaitForEndOfFrame eof = new WaitForEndOfFrame();

        float startValue = cannon.transform.rotation.y;
                
        float animationAmount = 0.0f;
        float speed = 1.0f / turret.rotationDelay;

        while (animationAmount < 1.0f)
        {
            animationAmount = Mathf.Min(1.0f, animationAmount + Time.deltaTime * speed);

            float newValue = Mathf.Lerp(0, 180, animationAmount);

            if(direction == 1)
            {
                cannon.transform.eulerAngles = new Vector3(0, -newValue, 0);
            }
            else
            {
                cannon.transform.eulerAngles = new Vector3(0, -180+newValue, 0);
            }
            

            yield return eof;
        }

        //TO REMOVE
        //yield return new WaitForSeconds(turret.rotationDelay);
        //transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);

        yield return new WaitForSeconds(0.5f);
        turret.canAttack = true;
        rotationStarted = false;
        cannonDirection = direction;
    }

    public override void EnterAttackB(Enemy enemy)
    {
        enemy.state = Enemy.EnemyState.attack;
    }

    public override void ExitAttackB(Enemy enemy)
    {
    }
    
    public override void ExitHitB(Enemy enemy)
    {
        //Return to search state
        if (!enemy.anim.GetBool("Hit") && enemy.state != Enemy.EnemyState.patrol)
        {
            if (enemy.state != Enemy.EnemyState.stun)
            {
                enemy.state = Enemy.EnemyState.patrol;
            }
        }
    }

    public override void EnterDeadB(Enemy enemy)
    {
        StopAllCoroutines();
        base.EnterDeadB(enemy);
    }

    public override void ExitStunB(Enemy enemy)
    {
        enemy.stun = false;

        EnterPatrolB(enemy);
    }
}
