﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : Enemy
{
    [Header("Turret Parameters")]

    [Tooltip("Bullet Prefab to spawn when attack")]
    public GameObject bullet;
    [Tooltip("Shoot speed")]
    public float bulletSpeed;
    [Tooltip("Position offset where spawn bullet")]
    public Vector3 offsetBulletSpawn;
    [Tooltip("Delay to spawn next bullet")]
    public float attackDelay;
    [Tooltip("Delay to rotate turret")]
    public float rotationDelay;
    [HideInInspector]
    public float cooldownAttack;
    [HideInInspector]
    public bool canAttack = true;
    [HideInInspector]
    public bool attackFinished = true;

    public GameObject chargeFx;
}
