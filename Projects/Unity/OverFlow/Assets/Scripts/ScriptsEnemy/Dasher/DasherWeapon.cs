﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DasherWeapon : MonoBehaviour
{
    public BuffDebuff debuff;

    [HideInInspector]
    public bool playerHitted;
    PlayerController player;

    private void Start()
    {
        player = GameManager.instance.player.GetComponent<PlayerController>();
    }

    //When attack start, reset the bool that show if enemy hit player
    protected void OnEnable()
    {
        playerHitted = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        //This attack hitted player
        if(other.gameObject.layer == 11 && !player.SecLifeBar) //Player layer
        {
            playerHitted = true;

            GameManager.instance.player.DebuffAdd(debuff);
        }
    }
}
