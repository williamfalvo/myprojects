﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dasher : Enemy
{   
    
    [Space(3), Header("Dasher Parameters")]
    [Tooltip("Delay to enable next teleport attack")]
    public float teleportDelay;
    [Tooltip("Offset between player position and teleport position (X axis)")]
    public float teleportOffset;
    [HideInInspector]
    public float teleportDirection;
    [Tooltip("Time to rest in stun state when dasher don't hit the player after its attack")]
    public float stunTime;
    [Tooltip("Damage over Time (seconds) that inflict to player")]
    public float damageTime;
    [Tooltip("Time to prepare next attack")]
    public float attackDelay;
    [HideInInspector]
    public bool canAttack;
    [HideInInspector]
    public bool canTeleport;

    [HideInInspector]
    public Vector3 startPosition; //Return position after a successful attack

    public GameObject[] model;

    public Material[] materials;

    public GameObject teleportFx;

    public GameObject[] healthObjects;

    public float teleportTimeMultiplier = 1f;

    public delegate void notifyEnemy();
    public notifyEnemy notifyMyEnemy;

    public virtual IEnumerator Stun()
    {


        //Change state and animator to stun
        state = EnemyState.stun;
        anim.SetBool("Stun", true);

        GetComponent<EnemyController>().notifyStun(EnemyController.DisabledState.Freeze, true , stunTime);
        Debug.Log("STUNNNNNNN");

        //Wait X seconds to exit from stun state
        yield return new WaitForSeconds(stunTime);

        //Prepare next attack
        StartCoroutine(CountdownAttack());

        //Exit from this state
        anim.SetBool("Stun", false);
        if(state == EnemyState.stun)
        {
            state = EnemyState.search;
            //GetComponent<EnemyController>().notifyStun(EnemyController.DisabledState.Freeze, false);
            Debug.Log("NOSTUNN");


        }
    }

    public virtual IEnumerator CountdownAttack()
    {

        //Wait X seconds to enable next attack
        yield return new WaitForSeconds(attackDelay);

        canAttack = true;
    }

    public virtual IEnumerator CountdownTeleport()
    {
        gameObject.layer = 14; //Dead layer

        foreach(GameObject go in healthObjects)
        {
            go.SetActive(false);
        }

        WaitForEndOfFrame wf = new WaitForEndOfFrame();

        
        GameObject tp = Instantiate(teleportFx, transform.position, Quaternion.identity);

        Destroy(tp, .7f);

        MeshRenderer weaponRend = weaponCollider.GetComponentInParent<MeshRenderer>();
        weaponRend.enabled = false;

        foreach (GameObject g in model)
        {
            g.GetComponent<SkinnedMeshRenderer>().material = materials[1];
        }

        MaterialPropertyBlock mpb = new MaterialPropertyBlock();
        float dissolve = 0;

        while (dissolve <= 1)
        {
            mpb.SetFloat("_DissolveAmount", dissolve);
            dissolve += Time.deltaTime * teleportTimeMultiplier;

            foreach (GameObject g in model)
            {
                g.GetComponent<SkinnedMeshRenderer>().SetPropertyBlock(mpb);
            }

            yield return wf;
        }
        
        //Wait X seconds to enable teleport attack
        yield return new WaitForSeconds(teleportDelay);

        canTeleport = true;
    }

    public virtual IEnumerator TeleportFx(Vector3 pos)
    {
        gameObject.layer = 10; //Enemy layer

        foreach (GameObject go in healthObjects)
        {
            go.SetActive(true);
        }

        WaitForEndOfFrame wf = new WaitForEndOfFrame();
        MaterialPropertyBlock mp = new MaterialPropertyBlock();
        float diss = 1f;
        
        GameObject tp = Instantiate(teleportFx, pos, Quaternion.identity);
        //FMODManager.instance.PlayDasherTeleport();

        Destroy(tp, .7f);

        MeshRenderer weaponRend = weaponCollider.GetComponentInParent<MeshRenderer>();
        weaponRend.enabled = true;

        while (diss > 0)
        {
            mp.SetFloat("_DissolveAmount", diss);
            diss -= Time.deltaTime;

            foreach (GameObject g in model)
            {
                g.GetComponent<SkinnedMeshRenderer>().SetPropertyBlock(mp);
            }

            yield return wf;
        }

        foreach (GameObject g in model)
        {
            g.GetComponent<SkinnedMeshRenderer>().material = materials[0];
        }
    }

    public virtual void PlayerHitted(CharacterStatistics player)
    {
        WeaponClass thisWeapon = weaponCollider.GetComponent<WeaponClass>();
        
        //Prepare next attack
        StartCoroutine(CountdownAttack());
               
    }

    //public void MeleeAttackAudio()
    //{
    //    FMODManager.instance.PlayEnemiesMelee();
    //}
}
