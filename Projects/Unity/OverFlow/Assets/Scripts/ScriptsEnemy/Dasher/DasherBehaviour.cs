﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DasherBehaviour : EnemyBehaviour
{
    bool playSound = true;
    public float cooldownSound;


    public override void EnterSearchB(Enemy enemy)
    {
        base.EnterSearchB(enemy);

        //if (playSound)
        //{
        //    FMODManager.instance.HumanEnemiesTriggered();
        //    playSound = false;
        //    StartCoroutine(CooldownAfterFirstAttack());
        //}
    }

    public override void UpdatePatrolB(Enemy enemy)
    {

    }

    public override void UpdateSearchB(Enemy enemy)
    {
        Dasher dasher = enemy as Dasher;

        //Calculate the distance between enemy and player position
        float distance = (enemy.player.transform.position.x - transform.position.x);
        //Set direction of enemy
        float direction = Mathf.Sign(distance);
        transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);
                

        //Check if player is on enemy attack range, else go to player position
        if (Mathf.Abs(distance) < enemy.attackRange && dasher.canAttack)
        {
            if (playSound)
            {
                FMODManager.instance.HumanEnemiesTriggered();
                playSound = false;
                StartCoroutine(CooldownAfterFirstAttack());
            }

            //Set the direction of teleport
            dasher.teleportDirection = -direction;
            //Start coroutine that manage the teleport attack
            StartCoroutine(dasher.CountdownTeleport());
            EnterAttackB(enemy);
        }
        else
        {
            //Check if lose the aggro 
            if (Mathf.Abs(distance) > enemy.visionRange)
            {
                enemy.loseAggroTimer += Time.deltaTime;

                if (enemy.loseAggroTimer >= enemy.maxAggroTime)
                {
                    ExitSearchB(enemy);
                    enemy.state = Enemy.EnemyState.idle;
                    return;
                }
            }
            else
            {
                enemy.loseAggroTimer = 0;
            }
        }
    }

    public override void EnterAttackB(Enemy enemy)
    {
        //Change state on attack
        enemy.state = Enemy.EnemyState.attack;
        //Stop running animation
        enemy.anim.SetBool("Run", false);        
    }

    public override void UpdateAttackB(Enemy enemy)
    {
        Dasher dasher = enemy as Dasher;

        //Check if can teleport
        if (dasher.canTeleport)
        {
            //Change animator
            enemy.anim.SetTrigger("Attack");

            //Reset attack and teleport 
            dasher.canAttack = false;
            dasher.canTeleport = false;
            //Set the position that use if attack hit the player
            dasher.startPosition = transform.position;
            //Teleport next to player to attack him
            dasher.rb.MovePosition(dasher.player.transform.position + Vector3.right * dasher.teleportOffset * dasher.teleportDirection);

            StartCoroutine(dasher.TeleportFx(dasher.player.transform.position + Vector3.right * dasher.teleportOffset * dasher.teleportDirection));
        }

        /*
        if (dasher.weaponCollider.GetComponent<DasherWeapon>().playerHitted && !dasher.DotUsed)
        {
            dasher.DotUsed = true;
            dasher.PlayerHitted(GameManager.instance.player);
        }
        */
    }

    public override void ExitAttackB(Enemy enemy)
    {
        Dasher dasher = enemy as Dasher;
                
        //Return to position that have before to start teleport
        dasher.rb.MovePosition(dasher.startPosition);
    }

    public override void AttackFinishB(Enemy enemy)
    {
        Dasher dasher = enemy as Dasher;

        //Check if attack hitted player
        if (dasher.weaponCollider.GetComponent<DasherWeapon>().playerHitted)
        {
            dasher.PlayerHitted(GameManager.instance.player);

            if (!enemy.anim.GetBool("Attack") && enemy.state != Enemy.EnemyState.patrol)
            {
                //Change state on search
                if (enemy.state != Enemy.EnemyState.stun)
                {
                    enemy.state = Enemy.EnemyState.search;
                }
                ExitAttackB(dasher);
            }
        }
        else
        {
            //Start coroutine that manage stun phase
            StartCoroutine(dasher.Stun());
        }
    }

    public override void ExitHitB(Enemy enemy)
    {
        if (!enemy.anim.GetBool("Hit"))
        {
            //If was on hit state, return to it, else return to search state
            if (enemy.anim.GetBool("Stun"))
            {
                enemy.state = Enemy.EnemyState.stun;
            }
            else if(enemy.state != Enemy.EnemyState.patrol)
            {
                if (enemy.state != Enemy.EnemyState.stun)
                {
                    enemy.state = Enemy.EnemyState.search;
                }
            }
        }
    }

    public IEnumerator CooldownAfterFirstAttack()
    {
        yield return new WaitForSeconds(cooldownSound);
        playSound = true;
    }
}
