﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityGuardBehaviour : EnemyBehaviour
{
    bool playSound = true;
    public float cooldownSound;

    public override void EnterSearchB(Enemy enemy)
    {
        base.EnterSearchB(enemy);

        if (playSound)
        {
            FMODManager.instance.HumanEnemiesTriggered();
            playSound = false;
            StartCoroutine(CooldownAfterFirstAttack());
        }
    }

    public IEnumerator CooldownAfterFirstAttack()
    {
        yield return new WaitForSeconds(cooldownSound);
        playSound = true;
    }
}
