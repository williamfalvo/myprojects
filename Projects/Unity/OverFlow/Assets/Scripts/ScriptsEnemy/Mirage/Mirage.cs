﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mirage : Enemy
{
    // MIRAGE PARAMETERS
    #region Mirage Parameters
    [Space(3), Header("Spawn clones variables")]
    [Tooltip("Clone prefab to spawn")]
    public GameObject clonePrefab;
    [Tooltip("Range of position (X axis) to spawn clones random")]
    public float spawnRange;
    [Tooltip("Time to spawn next clone after first spawn")]
    public float delaySpawn;
    [Tooltip("Max clones that one Mirage can have in game")]
    public int maxClones;
    [Tooltip("Time X (seconds) to enable the spawn action")]
    public float countdownSpawn;
    [HideInInspector]
    public bool canSpawn;
    #endregion

    // List of in game clones that Mirage spawned
    [HideInInspector]
    public List<GameObject> clones = new List<GameObject>();

    // Remove clone passed by parameter from mirage
    public void CloneDestroyed(GameObject clone)
    {
        clones.Remove(clone);
    }
}
