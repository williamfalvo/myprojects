﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirageClone : Enemy
{
    public Mirage mirage;

    public GameObject spawnFx;

    public float spawnTimeMultiplier = 1f;

    private new void Start()
    {
        base.Start();

        StartCoroutine(Materialization());
    }

    public IEnumerator Materialization()
    {
        GameObject fx = Instantiate(spawnFx, transform);

        Destroy(fx, 2f);

        WaitForEndOfFrame wf = new WaitForEndOfFrame();

        MaterialPropertyBlock mpb = new MaterialPropertyBlock();
        float dissolve = 0f;

        while (dissolve <= 1f)
        {
            mpb.SetFloat("_Alpha", dissolve);
            dissolve += Time.deltaTime * spawnTimeMultiplier;

            foreach (GameObject g in mod)
            {
                g.GetComponent<SkinnedMeshRenderer>().SetPropertyBlock(mpb);
            }

            yield return wf;
        }
    }

    private void OnDestroy()
    {
        //Say to mirage that its clone is dead
        mirage.CloneDestroyed(gameObject);
    }

    public override IEnumerator Dead()
    {
        foreach (GameObject g in mod)
        {
            g.GetComponent<SkinnedMeshRenderer>().material = mat[1];
        }

        deathFx.SetActive(true);

        spawnEffect.enabled = true;

        // Wait X seconds to drop an object
        yield return new WaitForSeconds(spawnEffect.spawnEffectTime/*deadTime*/);

        // Destroy enemy
        Destroy(gameObject);

        //yield return new WaitForSeconds(3f);
        //Destroy(gameObject);
    }
}
