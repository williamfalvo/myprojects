﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneBehaviour : EnemyBehaviour
{
    public override void UpdateSearchB(Enemy enemy)
    {
        if (CheckPlayerNode(enemy)) return;
                
        Drone drone = enemy as Drone;

        if (!drone.attackFinished)
        {
            return;
        }

        //Calculate the distance between enemy and player
        float distance = (enemy.player.transform.position - transform.position).sqrMagnitude;

        //Check if enemy reach the minimum distance, else try to attack
        if (distance > enemy.attackRange * enemy.attackRange)
        {
            //Check if enemy lose the aggro
            if (distance > enemy.visionRange * enemy.visionRange)
            {
                enemy.loseAggroTimer += Time.deltaTime;

                if (enemy.loseAggroTimer >= enemy.maxAggroTime)
                {
                    //Return to patrol state
                    ExitSearchB(enemy);
                    EnterPatrolB(enemy);
                    return;
                }
            }
            else
            {
                //Reset the timer of lose aggro
                enemy.loseAggroTimer = 0;
            }
            
            if (drone.attackFinished)
            {
                enemy.anim.SetBool("Run", true);
                enemy.pathFollow.PathFollow();
            }
            
        }
        else
        {
            //Look at player
            float direction = Mathf.Sign(enemy.player.transform.position.x - transform.position.x);
            transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);

            enemy.anim.SetBool("Run", false);
                        
            ExitSearchB(enemy);
            EnterAttackB(enemy);
        }
    }

    public IEnumerator ShootFx(Drone drone, Enemy enemy)
    {
        GameObject ch = Instantiate(drone.chargeFx, transform.position + (drone.offsetBulletSpawn * transform.localScale.x), Quaternion.identity);
        ch.transform.parent = transform;

        float vfxTime = ch.GetComponent<ParticleSystem>().main.duration - 1f;
        Destroy(ch, 3f);

        //Calculate target direction and angle shoot
        Vector3 targetDir = enemy.player.transform.position + Vector3.up - transform.position + drone.offsetBulletSpawn;
        float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;

        yield return new WaitForSeconds(vfxTime);

        //Instantiate new bullet
        GameObject bullet = Instantiate(drone.bullet, transform.position + drone.offsetBulletSpawn, Quaternion.identity);

        //Apply the reference of its stats
        BulletWeapon bulletBe = bullet.GetComponent<BulletWeapon>();
        bulletBe.ownerStats = GetComponent<CharacterStatistics>();
        bulletBe.WeaponTypeCases();

        //Set the rotation of bullet to player position
        //Vector3 targetDir = enemy.player.transform.position + Vector3.up - transform.position + drone.offsetBulletSpawn;
        //float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
        bullet.transform.localEulerAngles = new Vector3(0, 0, 90 + angle);

        //Apply a force to shoot
        bullet.GetComponent<Rigidbody>().velocity = Vector3.Normalize(/*enemy.player.transform.position + Vector3.up - transform.position*/targetDir) * drone.bulletSpeed;

        //Set next cooldown to allow shoot
        drone.cooldownAttack = 0f;

        //Increase the counter
        drone.counterBullet++;

        yield return new WaitForSeconds(0.2f);
        drone.canAttack = true;
        drone.attackFinished = true;
    }

    public override void UpdateAttackB(Enemy enemy)
    {
        Drone drone = enemy as Drone;

        Vector3 playerPos = enemy.player.transform.position + Vector3.up;
        Vector3 dronePos = transform.position;
        
        float heightDistance = playerPos.y + drone.offsetYAttack - dronePos.y;
        float yDirection = Mathf.Sign(heightDistance);
        
        //Reach the minimum height to attack player
        if(drone.onPosition)
        {
            if (drone.canAttack)
            {
                drone.canAttack = false;
                drone.attackFinished = false;
                StartCoroutine(ShootFx(drone, enemy));
                return;
            }            

            if (drone.attackFinished)
            {
                ExitAttackB(enemy);
                EnterSearchB(enemy);
                return;
            }
            /*
            //Check if enemy can attack
            if(drone.cooldownAttack < drone.attackDelay)
            {
                drone.cooldownAttack += Time.deltaTime;
            }
            else
            {
                if (drone.attackFinished)
                {
                    StartCoroutine(ShootFx(drone, enemy));
                }
            }

            //If shoot all its bullets, return to search state
            if(drone.counterBullet == drone.nBullet)
            {
                
                //Reset counter of bullet shooted
                drone.counterBullet = 0;
                ExitAttackB(enemy);
                EnterSearchB(enemy);
            }
            */
        }
        else if(drone.canAttack)
        {
            //Check if height distance from player is correct to attack
            if(heightDistance >= -0.3f && heightDistance <= 0.3f)
            {
                drone.onPosition = true;
            }
            else
            {
                //Adjust the height to allow its attack
                Vector3 droneMovement = transform.up * (yDirection * enemy.speed * Time.deltaTime);
                enemy.rb.MovePosition(enemy.rb.position + droneMovement);
            }            
        }
    }

    public override void EnterAttackB(Enemy enemy)
    {
        Drone drone = enemy as Drone;
        drone.onPosition = false;
        drone.canAttack = true;
        drone.attackFinished = false;

        base.EnterAttackB(enemy);
        
    }

    public override void EnterDeadB(Enemy enemy)
    {
        StopAllCoroutines();
        base.EnterDeadB(enemy);
    }
}
