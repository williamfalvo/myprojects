﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : Enemy
{
    [Header("Spawner Parameters")]
    [Tooltip("Enemies that it can spawn")]
    public List<GameObject> enemies;
    [Tooltip("Max number of enemies that it can spawn")]
    public int maxEnemies;
    [Tooltip("Spawn delay in seconds")]
    public float spawnDelay;
    [Tooltip("Spawn offset")]
    public Vector3 spawnOffset;
    [Tooltip("GraphMaker to set on enimies spawned")]
    public GraphMaker graphMaker;
    [HideInInspector]
    public float cooldownSpawn;
    [HideInInspector]
    public List<GameObject> counterEnemies;
    [HideInInspector]
    public bool isEnabled;

    protected override void Awake()
    {
        base.Awake();
        isEnabled = true;
        isInvulnerable = true; //Spawner can't take damage
    }

}
