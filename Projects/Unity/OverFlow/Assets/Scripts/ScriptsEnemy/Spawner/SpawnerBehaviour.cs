﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerBehaviour : TurretBehaviour
{
    public override void UpdateAttackB(Enemy enemy)
    {
        Spawner spawner = enemy as Spawner;

        if(!spawner.isEnabled)
        {
            bool destroy = false;

            for (int i = 0; i < spawner.counterEnemies.Count; ++i)
            {
                if(spawner.counterEnemies[i] != null)
                {
                    destroy = false;
                    break;
                }
                else
                {
                    destroy = true;
                }
            }

            if (destroy)
            {
                EnterDeadB(spawner);
            }
        }

        if(spawner.isEnabled && spawner.counterEnemies.Count < spawner.maxEnemies)
        {
            //Check if enemy can spawn
            if (spawner.cooldownSpawn < spawner.spawnDelay)
            {
                spawner.cooldownSpawn += Time.deltaTime;
            }
            else
            {
                int indexEnemy = Random.Range(0, spawner.enemies.Count - 1);

                //Instantiate new enemy
                Vector3 bulletPos = new Vector3(transform.position.x,transform.position.y,0) + spawner.spawnOffset;
                GameObject enemyToSpawn = Instantiate(spawner.enemies[indexEnemy], bulletPos, Quaternion.identity);
                spawner.counterEnemies.Add(enemyToSpawn);

                //Apply the reference of its stats
                Enemy e = enemyToSpawn.GetComponent<Enemy>();
                e.patrolPosition = spawner.patrolPosition;
                e.startPathNode = spawner.startPathNode;
                e.room = spawner.room;
                e.room.enemies.Add(e);
                e.aStar.graphMaker = spawner.graphMaker;
                e.state = Enemy.EnemyState.search;

                //Set next cooldown to allow spawn
                spawner.cooldownSpawn = 0f;
            }
        }
        else
        {
            spawner.isEnabled = false;
        }
        
    }

    public override void ExitAttackB(Enemy enemy)
    {
    }

    public override void ExitHitB(Enemy enemy)
    {
        //Return to search state
        if (!enemy.anim.GetBool("Hit") && enemy.state != Enemy.EnemyState.patrol)
        {
            enemy.state = Enemy.EnemyState.attack;
        }
    }
}
