﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    protected Node lastPlayerNode;
    protected Node playerNode;

    public float tickAStar = 0.5f;

    private void Start()
    {
        StartCoroutine(UpdatePlayerNode());
    }

    #region Patrol Functions
    public virtual void EnterPatrolB(Enemy enemy)
    {
        //Stop running animation
        enemy.anim.SetBool("Run", false);

        //Calculate the path to return to start node position to start its patrol movement
        enemy.aStar.FindPath(enemy.startPathNode, enemy.startNode);
        enemy.onPatrolPosition = false;

        //Set player node reference to find a path
        playerNode = enemy.player.GetComponent<CharacterStatistics>().playerNode;

        //Change the state on patrol
        enemy.state = Enemy.EnemyState.patrol;

    }
    
    public virtual void UpdatePatrolB(Enemy enemy)
    {
        if(playerNode == null)
        {
            playerNode = enemy.player.GetComponent<CharacterStatistics>().playerNode;
        }

        if (enemy.onPatrolPosition)
        {
            //Calculate the distance between enemy and patrolEnd position
            float distance = enemy.patrolEnd - transform.position.x;
            //Invert the patrol points
            if (Mathf.Abs(distance) < 0.1f)
            {
                float temp = enemy.patrolEnd;
                enemy.patrolEnd = enemy.patrolStart;
                enemy.patrolStart = temp;
            }
            //Set direction of enemy
            float direction = Mathf.Sign(distance);
            transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);
            //Move enemy to patrolEnd position
            Vector3 enemyMovement = transform.right * (direction * enemy.patrolSpeed * Time.deltaTime);
            enemy.rb.MovePosition(enemy.rb.position + enemyMovement);
        }
        else
        {
            //Return to patrol start node position
            if(enemy.startNode != null)
            {
                //Calculate the distance to reach start node
                float distance = (enemy.transform.position - enemy.startNode.transform.position).sqrMagnitude;
                //If is near to start node, start a normal patrol move, else move to start node position
                if (distance <= 2f * 2f)
                {
                    enemy.onPatrolPosition = true;
                }
                else
                {
                    enemy.pathFollow.PathFollow();
                }
            }            
        }        

        // Control distance between enemy and player position
        if ((enemy.player.transform.position - transform.position).sqrMagnitude <= enemy.visionRange * enemy.visionRange)
        {
            if(lastPlayerNode != playerNode)
            {
                lastPlayerNode = playerNode;
                
                //Calculate new path to reach the player position
                AStar tempAStar = gameObject.AddComponent(typeof(AStar)) as AStar;
                PathFollowBehaviour tempPath = gameObject.AddComponent(typeof(PathFollowBehaviour)) as PathFollowBehaviour;
                tempPath.pathfindingAlgorithm = tempAStar;
                tempAStar.pathFollow = tempPath;
                tempAStar.graphMaker = enemy.aStar.graphMaker;

                tempAStar.FindPath(enemy.startPathNode, playerNode);

                //Check if enemy have a valid path to follow the player
                if (tempPath.canFollow)//enemy.pathFollow.canFollow)
                {
                    //enemy.aStar.temp_EndNode = tempAStar.temp_EndNode;
                    //enemy.pathFollow.path = tempPath.path;
                    //enemy.pathFollow.canFollow = true;
                    ExitPatrolB(enemy);
                    EnterSearchB(enemy);
                }

                Destroy(tempAStar);
                Destroy(tempPath);

                StartCoroutine(UpdatePlayerNode());
            }
        }
    }

    public IEnumerator UpdatePlayerNode()
    {
        playerNode = GameManager.instance.player.playerNode;

        yield return new WaitForSeconds(tickAStar);
                
        StartCoroutine(UpdatePlayerNode());
    }

    public virtual void ExitPatrolB(Enemy enemy)
    {
    }
    #endregion

    #region Attack Functions
    public virtual void EnterAttackB(Enemy enemy)
    {
        //Change animator and state to Attack
        enemy.state = Enemy.EnemyState.attack;
        enemy.anim.SetTrigger("Attack");
        enemy.anim.SetBool("Run", false);
    }

    public virtual void UpdateAttackB(Enemy enemy)
    {
    }

    public virtual void ExitAttackB(Enemy enemy)
    {
        AttackFinishB(enemy);
    }

    public virtual void AttackFinishB(Enemy enemy)
    {
        if (!enemy.anim.GetBool("Attack") && enemy.locked)
        {
            UpdateLockB(enemy);            
        }

        //Return to enemy search state
        if (!enemy.anim.GetBool("Attack") && enemy.state != Enemy.EnemyState.patrol && !enemy.locked)
        {
            if (enemy.state != Enemy.EnemyState.stun)
            {
                enemy.state = Enemy.EnemyState.search;
            }
        }

        
    }
    #endregion

    #region Search Functions
    public virtual void EnterSearchB(Enemy enemy)
    {
        Vector3 direction = Vector3.right * (enemy.player.transform.position.x - transform.position.x);

        RaycastHit[] ray = Physics.RaycastAll(transform.position, direction, enemy.visionRange);
        bool playerTouch = false;
        foreach (RaycastHit hit in ray)
        {
            if (hit.collider.gameObject.tag == "Player")
            {
                playerTouch = true;
            }
        }
        if (playerTouch)
        {
            int layerMask = (1 << 15); //Node layer
            RaycastHit hit;
            if (Physics.Raycast(transform.position, direction, out hit, enemy.visionRange, layerMask))
            {
                enemy.startPathNode = hit.collider.GetComponent<Node>();
            }
        }

        ////Calculate a path to reach the player position
        //playerNode = enemy.player.GetComponent<CharacterStatistics>().playerNode;

        if (lastPlayerNode != playerNode)
        {
            lastPlayerNode = playerNode;
        }

        enemy.aStar.FindPath(enemy.startPathNode, playerNode);

        //Check if enemy have a path to reach the player
        if (enemy.pathFollow.canFollow)
        {
            //Change to search state
            enemy.state = Enemy.EnemyState.search;
        }
        else
        {
            ExitSearchB(enemy);
            EnterPatrolB(enemy);
        }
        
    }

    public virtual void UpdateSearchB(Enemy enemy)
    {
        if(CheckPlayerNode(enemy)) return;                

        //Calculate the distance between enemy and player position        
        float distance = (enemy.player.transform.position - transform.position).sqrMagnitude;
        
        //Check if player is on enemy attack range
        if (distance < enemy.attackRange * enemy.attackRange && enemy.onFloor)
        {
            //Look at player position
            float direction = Mathf.Sign((enemy.player.transform.position.x - transform.position.x));
            transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);

            EnterAttackB(enemy);
            return;
        }
        else
        {
            //Check if enemy lose the aggro
            if(distance > enemy.visionRange * enemy.visionRange)
            {
                enemy.loseAggroTimer += Time.deltaTime;

                if(enemy.loseAggroTimer >= enemy.maxAggroTime)
                {
                    //Return to patrol state
                    ExitSearchB(enemy);
                    EnterPatrolB(enemy);
                    enemy.state = Enemy.EnemyState.patrol;
                    return;
                }
            }
            else
            {
                //Reset the timer of lose aggro
                enemy.loseAggroTimer = 0;
            }

            //Move to player position
            enemy.anim.SetBool("Run", true);
            enemy.pathFollow.PathFollow();
        }
    }

    public bool CheckPlayerNode(Enemy enemy)
    {
        //If player change node, recalculate new path 
        if (playerNode != enemy.aStar.temp_EndNode)
        {
            EnterSearchB(enemy);
            return true;
        }

        return false;
    }

    public virtual void ExitSearchB(Enemy enemy)
    {
        //Stop running animation
        enemy.anim.SetBool("Run", false);
    }
    #endregion

    #region Hit Functions
    public virtual void EnterHitB(Enemy enemy)
    {
        //Disable weapon collider
        enemy.DisableWeapon();

        //If hp is greater than 1 enemy is hitted, else the enemy is dead
        if (enemy.GetComponent<CharacterStatistics>().currentHealth > 1)
        {
            if (enemy.state != Enemy.EnemyState.stun)
            {
                //Change state and animator to hit
                enemy.anim.SetTrigger("Hit");
                enemy.state = Enemy.EnemyState.hit;
            }           

            //Disable the reference of current active effect
            GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.EnemyHit, enemy);
        }
        else
        {
            //Enter to dead state
            EnterDeadB(enemy);
        }
    }

    public virtual void UpdateHitB(Enemy enemy)
    {

    }

    public virtual void ExitHitB(Enemy enemy)
    {
        //Return to search state
        if (!enemy.anim.GetBool("Hit") && enemy.state != Enemy.EnemyState.patrol)
        {
            if (enemy.state != Enemy.EnemyState.stun)
            {
                enemy.state = Enemy.EnemyState.search;
            }
        }
    }
    #endregion

    #region Dead Functions
    public virtual void EnterDeadB(Enemy enemy)
    {
        //Change state and animator to dead
        enemy.anim.SetTrigger("Dead");
        enemy.state = Enemy.EnemyState.dead;

        //Reset basic material
        CharacterStatistics body = GetComponent<CharacterStatistics>();
        body.bodyMesh.material = body.bodyMaterial;

        //Disable the reference of current active effect
        GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.EnemyDead, null);

        //Can collide only with Ground and Map Layer
        gameObject.layer = 14; // Dead layer

        //Start the drop system using coroutine "Dead"
        StartCoroutine(enemy.Dead());
    }
    #endregion

    #region Stun Functions
    [HideInInspector]
    public float stunTime;

    public virtual void EnterStunB(Enemy enemy)
    {
        enemy.stun = true;
        //Start coroutine that manage the stun state of enemy
        StartCoroutine(StunState(stunTime, enemy));        
    }

    public virtual void UpdateStunB(Enemy enemy)
    {

    }

    public virtual void ExitStunB(Enemy enemy)
    {
        enemy.stun = false;
        //Return to search state
        EnterSearchB(enemy);
    }

    public virtual IEnumerator StunState(float time, Enemy enemy)
    {
        //Change state to stun
        enemy.state = Enemy.EnemyState.stun;
        //Wait X seconds to reset enemy state
        yield return new WaitForSeconds(time);
        //Exit from stun state
        ExitStunB(enemy);
    }
    #endregion

    #region Lock Functions
    [HideInInspector]
    public float lockTime;

    public virtual void EnterLockB(Enemy enemy)
    {
        enemy.locked = true;
        //Start a coroutine that manage the lock state
        StartCoroutine(LockState(lockTime, enemy));
    }

    public virtual void UpdateLockB(Enemy enemy)
    {
        //Calculate distance between enemy and player position
        float distance = (enemy.player.transform.position - transform.position).sqrMagnitude;

        //If is near player position and is on floor, then attack
        if (distance < enemy.attackRange * enemy.attackRange && enemy.onFloor)
        {
            EnterAttackB(enemy);
            return;
        }
    }

    public virtual void ExitLockB(Enemy enemy)
    {
        enemy.locked = false;
        //Return to search state
        EnterSearchB(enemy);
    }

    public virtual IEnumerator LockState(float time, Enemy enemy)
    {
        //Change state in locked
        enemy.state = Enemy.EnemyState.locked;
        //Wait X seconds to exit from lock state
        yield return new WaitForSeconds(time);
        //Exit from lock state
        ExitLockB(enemy);
    }
    #endregion

    public void EnemiesMelee()
    {
        if (this != GetComponent<DroneBehaviour>())
        {
            FMODManager.instance.PlayEnemiesMelee();
        }
    }

    public void JailerMelee()
    {
        FMODManager.instance.PlayJailerAttack();
    }
}
