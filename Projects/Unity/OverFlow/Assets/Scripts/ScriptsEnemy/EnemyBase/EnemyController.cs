﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public enum DisabledState {Freeze , Lock , Jam}
    public List<DisabledState> myStates = new List<DisabledState>(); //tracks the states of the enemy

    // ENEMY COMPONENTS
    #region Enemy Components
    Enemy enemy;
    EnemyBehaviour enemyBeh;
    CharacterStatistics enemyStats;

    //true if enter the state , off otherwise
    public delegate void DisableState(DisabledState state , bool onOff , float stunTime); //called whenever the enemy gets a disable state
    public DisableState disableStateUpdated ;

    public CharacterStatistics EnemyStats
    {
        get { return enemyStats; }
        set { enemyStats = value; }
    }

    bool hit;

    public bool Hit
    {
        get { return hit; }
        set { hit = value; }
    }

    float dmg = 0f;

    public float Dmg
    {
        get { return dmg; }
    }

    public bool weaponCollision;
    public Collider wpCollider;
    public GameObject hitFx;
    public GameObject damageText;
    #endregion

    // PLAYER REFERENCE
    #region Player Reference
    GameObject player;
    #endregion

    
    void Awake()
    {
        enemy = GetComponent<Enemy>();
        enemyBeh = GetComponent<EnemyBehaviour>();        
        enemyStats = GetComponent<CharacterStatistics>();
    }

    void Start()
    {
        player = GameManager.instance.player.gameObject;
    }

    // FSM
    void Update()
    {
        switch (enemy.state)
        {
            #region Idle
            case (Enemy.EnemyState.idle):
                {
                    //Check if enemy see player
                    SeePlayer();

                    break;
                }
            #endregion

            #region Patrol
            case (Enemy.EnemyState.patrol):
                {
                    UpdatePatrol();

                    break;
                }
            #endregion

            #region Search
            case (Enemy.EnemyState.search):
                {
                    UpdateSearch();

                    break;
                }
            #endregion

            #region Attack
            case (Enemy.EnemyState.attack):
                {
                    UpdateAttack();

                    break;
                }
            #endregion

            #region Hit
            case (Enemy.EnemyState.hit):
                {
                    UpdateHit();

                    break;
                }
            #endregion

            #region Stun
            case (Enemy.EnemyState.stun):
                {
                    if(enemyStats.currentHealth <= 0)
                    {
                        enemy.state = Enemy.EnemyState.dead;
                    }
                    UpdateStun();
                    break;
                }
            #endregion

            #region Lock
            case (Enemy.EnemyState.locked):
                {
                    UpdateLock();
                    break;
                }
            #endregion 

            #region DefaultCase
            default:
                {
                    break;
                }
            #endregion
        }

        if (wpCollider != null)
        {
            CheckDamage();
        }
    }

    private void OnTriggerStay(Collider other)
    {       
        //MaryVersion
        if (!enemy.isInvulnerable && other.tag == "Weapon")
        {
            wpCollider = other;
            CheckDamage();
        }
    }    

    void CheckDamage()
    {
        if (wpCollider == null || weaponCollision) return;
                
        //Get the type of enemy weapon
        WeaponClass wp = wpCollider.GetComponent<WeaponClass>();
        wp.WeaponTypeCases();
        
        weaponCollision = true;
        wp.enemyHitted.Add(this);

        if (enemy.rb.useGravity)
        {
            //Pushback
            float direction = Mathf.Sign(transform.position.x - wpCollider.transform.position.x);
            enemy.rb.AddForce(Vector3.right * direction * 1000f);
        }

        PlayerController p = wpCollider.gameObject.GetComponentInParent<PlayerController>();

        if (p.thirdAttack)
        {
            GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.ThirdComboAttack, enemy);

            Debug.Log("terza combo ! ");

            p.thirdAttack = false;
	    }

        //Initialize the amount of damage
        dmg = 0f;

        //Calculate the amount of resistence 
        float res = (int)Mathf.Clamp(enemyStats.baseNeuralResistence + enemyStats.bonusNeuralResistence, 0, 100);

        //Calculate the amount of damage to inflict
        if (wp.wpType != WeaponClass.Type.Katana)
        {
            dmg = GameManager.instance.damager.CalculateDamage(wp.Atk, wp.Stat, wp.BabScalings, enemyStats.baseDefense, res, enemyStats.defenseValues, enemyStats.UseScaling, wp.AtkModPerc);
        }
        else
        {
            dmg = GameManager.instance.damager.CalculateDamage(wp.Atk, wp.Stat, wp.LevelScalings, enemyStats.baseDefense, res, enemyStats.defenseValues, enemyStats.UseScaling, wp.AtkModPerc);
        }

        if (wp.wpType == WeaponClass.Type.Katana)
        {
            FMODManager.instance.KatanaHit();
        }

        if (wp.wpType == WeaponClass.Type.Spear)
        {
            FMODManager.instance.SpearHit();
        }

        if (wp.wpType == WeaponClass.Type.Hammer)
        {
            FMODManager.instance.HammerHit();
        }

        //Check and calculate critical damage
        CharacterStatistics pStats = p.GetComponent<CharacterStatistics>();
        float luck = (pStats.baseLuck + pStats.bonusLuck) * pStats.luckIncreasePerLevel;            
        float perc = Random.value * 100f;
        bool crit = false;

        if (perc <= luck)
        {
            dmg = Mathf.RoundToInt(dmg * 1.25f);
            crit = true;
        }

        if (wp.wpType == enemy.weaponResistance)
        {
            dmg = Mathf.RoundToInt(dmg * 0.5f);
        }

        enemy.anim.SetFloat("hitType", (float)wp.wpType * 0.5f);

        //Debug panel update
        //GameManager.instance.debugUI.danniInflitti = dmg;

        GameObject hit = Instantiate(hitFx, wpCollider.ClosestPoint(transform.position), Quaternion.identity);
        Destroy(hit, 1.5f);
        

        DamageText damageT = Instantiate(damageText, wpCollider.ClosestPoint(transform.position), Quaternion.identity).GetComponent<DamageText>();
        damageT.textMesh.text = dmg.ToString();

        if (crit)
        {
            //damageT.textMesh.color = Color.red;
            damageT.textMesh.colorGradient = damageT.colorGradient;

            foreach (ParticleSystem part in hit.GetComponentsInChildren<ParticleSystem>())
            {
                var main = part.main;
                main.startColor = Color.red;
            }
        }

        //Decrease its healt
        TakeDamage(dmg, true);

        GameManager.instance.Hit(this.gameObject, this.enemyStats);

        wpCollider = null;

    }

    // Check if enemy see the player
    void SeePlayer()
    {
        // Control distance between enemy and player position
        if ((enemy.player.transform.position - transform.position).sqrMagnitude < enemy.visionRange)
        {
            enemy.state = Enemy.EnemyState.search;
            //FMODManager.instance.HumanEnemiesTriggered();
        }
    }

    #region Attack Functions
    void EnterAttack()
    {
        enemyBeh.EnterAttackB(enemy);
    }

    void UpdateAttack()
    {
        enemyBeh.UpdateAttackB(enemy);
    }

    void ExitAttack()
    {
        enemyBeh.ExitAttackB(enemy);
    }

    public void AttackFinish()
    {
        enemyBeh.AttackFinishB(enemy);
    }
    #endregion

    #region Patrol Functions
    public void EnterPatrol()
    {
        enemyBeh.EnterPatrolB(enemy);
    }

    void UpdatePatrol()
    {
        enemyBeh.UpdatePatrolB(enemy);
    }

    void ExitPatrol()
    {
        enemyBeh.ExitPatrolB(enemy);
    }
    #endregion

    #region Search Functions
    public void EnterSearch()
    {
        enemyBeh.EnterSearchB(enemy);
    }

    void UpdateSearch()
    {
        enemyBeh.UpdateSearchB(enemy);
    }

    void ExitSearch()
    {
        enemyBeh.ExitSearchB(enemy );
    }
    #endregion

    #region Hit Functions
    void EnterHit()
    {
        enemyBeh.EnterHitB(enemy);
    }

    void UpdateHit()
    {
        enemyBeh.UpdateHitB(enemy);
    }

    void ExitHit()
    {
        enemyBeh.ExitHitB(enemy);
    }
    #endregion

    #region Stun Functions
    void EnterStun()
    {
        enemyBeh.EnterStunB(enemy);
        myStates.Add(DisabledState.Freeze);

    }

    void UpdateStun()
    {
        enemyBeh.UpdateStunB(enemy);
    }

    public void ExitStun()
    {
        enemyBeh.ExitStunB(enemy);
        myStates.Remove(DisabledState.Freeze);


    }
    #endregion

    #region Lock Functions
    void EnterLock()
    {
        enemyBeh.EnterLockB(enemy);
        myStates.Add(DisabledState.Lock);
        //disableStateUpdated(DisabledState.Lock , true);
    }

    void UpdateLock()
    {
        enemyBeh.UpdateLockB(enemy);
    }

    void ExitLock()
    {
        enemyBeh.ExitLockB(enemy);
        myStates.Remove(DisabledState.Lock);
        //disableStateUpdated(DisabledState.Lock, false);
    }
    #endregion

    public void TakeDamage(float damage, bool isHitted)
    {
        //Decrease current healt
        enemyStats.currentHealth -= damage;
        
        

        if (isHitted)
        {
            EnterHit();
        }
    }

    public void notifyStun(DisabledState state , bool stateState , float stunTime)
    {
        disableStateUpdated(state, stateState , stunTime);
    }
}
