﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HackerBehaviour : EnemyBehaviour
{
    bool playSound = true;
    public float cooldownSound;

    public override void EnterSearchB(Enemy enemy)
    {
        base.EnterSearchB(enemy);

        //if (playSound)
        //{
        //    FMODManager.instance.HumanEnemiesTriggered();
        //    playSound = false;
        //    StartCoroutine(CooldownAfterFirstAttack());
        //}
    }

    public override void UpdateSearchB(Enemy enemy)
    {
        Hacker hacker = enemy as Hacker;

        if (playSound)
        {
            FMODManager.instance.HumanEnemiesTriggered();
            playSound = false;
            StartCoroutine(CooldownAfterFirstAttack());
        }

        //Calculate the distance between enemy and player position 
        float direction = Mathf.Sign((hacker.player.transform.position.x - transform.position.x));
        float distance = (enemy.player.transform.position - transform.position).sqrMagnitude;

        //If Mirage can spawn clone change its state and start a coroutine to start the spawning phase
        if (hacker.onFloor && distance > hacker.meleeRange * hacker.meleeRange)
        {
            transform.localScale = new Vector3(direction, 1, 1);
            if (hacker.canAttackRange)
            {
                hacker.canAttackRange = false;
                hacker.state = Enemy.EnemyState.attack;
                hacker.anim.SetTrigger("Shoot");
                StartCoroutine(RangeAttack(hacker));
                return;
            }
        }

        base.UpdateSearchB(hacker);
    }

    public override void EnterAttackB(Enemy enemy)
    {
        base.EnterAttackB(enemy);
    }

    public override void UpdateAttackB(Enemy enemy)
    {
    }

    IEnumerator RangeAttack(Hacker hacker)
    {
        Vector3 bulletPos = transform.position + Vector3.up;
        GameObject bullet = Instantiate(hacker.bulletPrefab, bulletPos, Quaternion.identity);

        //Apply the reference of its stats
        BulletWeapon bulletBe = bullet.GetComponent<BulletWeapon>();
        bulletBe.ownerStats = GetComponent<CharacterStatistics>();
        bulletBe.WeaponTypeCases();

        //Set the rotation of bullet to player position                
        bullet.transform.localEulerAngles = new Vector3(0, 0, 90);

        //Apply a force to shoot
        bullet.GetComponent<Rigidbody>().velocity = Vector3.right * transform.localScale.x * hacker.bulletSpeed;
                
        yield return new WaitForSeconds(hacker.shootAnim.length);
        EnterSearchB(hacker);

        yield return new WaitForSeconds(hacker.rangeAttackDelay);
        hacker.canAttackRange = true;
    }

    public IEnumerator CooldownAfterFirstAttack()
    {
        yield return new WaitForSeconds(cooldownSound);
        playSound = true;
    }
}
