﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hacker : Enemy
{
    [Tooltip("Minimum distance to pass from range to melee search")]
    public float meleeRange;
    public bool canAttackRange;
    public GameObject bulletPrefab;
    public float bulletSpeed;
    public float rangeAttackDelay;
    public AnimationClip shootAnim;
    
}
