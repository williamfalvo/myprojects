﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChampionDebug : MonoBehaviour
{
    public Enemy enemy;

    public SpriteRenderer sprite;

    public Transform target; // character she has to follow
    float yOffSet; // y offset from the character to follow

    private void Start()
    {
        yOffSet = transform.localPosition.y;

        switch (enemy.championType)
        {
            case (Enemy.ChampionVersion.blue):
                {
                    sprite.color = Color.blue;
                    break;
                }
            case (Enemy.ChampionVersion.green):
                {
                    sprite.color = Color.green;
                    break;
                }
            case (Enemy.ChampionVersion.red):
                {
                    sprite.color = Color.red;
                    break;
                }
            case (Enemy.ChampionVersion.yellow):
                {
                    sprite.color = Color.yellow;
                    break;
                }
            case (Enemy.ChampionVersion.none):
                {
                    sprite.enabled = false;
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        //follows the target 
        transform.position = target.position + new Vector3(0, yOffSet, 0);

    }
}
