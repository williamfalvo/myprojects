﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyBe : EnemyBehaviour
{
    public float timeRefillHealth;
    public float healthToRefill;
    CharacterStatistics enemy;
    float totalHp;

    private void Start()
    {
        enemy = GetComponent<CharacterStatistics>();
        totalHp = enemy.hpValues[(int)(enemy.bonusNeuralHealth + enemy.baseNeuralHealth) - 1];

        StartCoroutine(Refill());
    }

    IEnumerator Refill()
    {
        yield return new WaitForSeconds(timeRefillHealth);

        if(enemy.currentHealth + healthToRefill > totalHp)
        {
            enemy.currentHealth = totalHp;
        }
        else
        {
            enemy.currentHealth += healthToRefill;
        }

        StartCoroutine(Refill());
    }

    public override void AttackFinishB(Enemy enemy)
    {
    }

    public override void EnterAttackB(Enemy enemy)
    {
    }

    public override void EnterDeadB(Enemy enemy)
    {
    }

    public override void EnterHitB(Enemy enemy)
    {
    }

    public override void EnterLockB(Enemy enemy)
    {
    }

    public override void EnterPatrolB(Enemy enemy)
    {
    }

    public override void EnterSearchB(Enemy enemy)
    {
    }

    public override void EnterStunB(Enemy enemy)
    {
    }

    public override void ExitAttackB(Enemy enemy)
    {
    }

    public override void ExitHitB(Enemy enemy)
    {
    }

    public override void ExitLockB(Enemy enemy)
    {
    }

    public override void ExitPatrolB(Enemy enemy)
    {
    }

    public override void ExitSearchB(Enemy enemy)
    {
    }

    public override void ExitStunB(Enemy enemy)
    {
    }

    public override void UpdateAttackB(Enemy enemy)
    {
    }

    public override void UpdateHitB(Enemy enemy)
    {
    }

    public override void UpdateLockB(Enemy enemy)
    {
    }

    public override void UpdatePatrolB(Enemy enemy)
    {
    }

    public override void UpdateSearchB(Enemy enemy)
    {
    }

    public override void UpdateStunB(Enemy enemy)
    {
    }    
}
