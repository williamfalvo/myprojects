﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityWeapon : MonoBehaviour
{
    public Boss boss;
    public float speed;
    public float time;

    private void Start()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(boss.transform.localScale.x * speed, 0f, 0f);
        Destroy(gameObject, time);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 11) //Player layer
        {
            other.GetComponent<PlayerController>().WeaponDisabled();
        }
    }
}
