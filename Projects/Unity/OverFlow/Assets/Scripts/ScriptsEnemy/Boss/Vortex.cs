﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vortex : MonoBehaviour
{
    public float damage;

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.layer == 11) //Player layer
        {
            other.GetComponent<PlayerController>().TakeDamage(damage, false);
        }
    }
}
