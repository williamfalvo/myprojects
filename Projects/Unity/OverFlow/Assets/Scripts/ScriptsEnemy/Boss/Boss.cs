﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : Enemy
{    
    [HideInInspector]
    public CharacterStatistics bossStats;
    [Header("Boss Parameters")]
    [Tooltip("Cooldown to next Whirlwind attack")]
    public float whirlwindTimer;
    [Tooltip("Stun time after whirlwind attack")]
    public float whirlwindStun;
    [Tooltip("Vortex of whirlwind attack")]
    public GameObject whirlwindVortex;
    [HideInInspector]
    public float whirlwindCooldown;
    [HideInInspector]
    public float tempHealth;
    [HideInInspector]
    public bool useRangedAttack;
    [Tooltip("% damage to enable range attack")]
    public float rangeAttack;
    [Tooltip("Shockwave prefab")]
    public GameObject shockwave;
    [Tooltip("Laser GameObject Reference")]
    public GameObject laser;
    [HideInInspector]
    public bool useAbilityAttak;
    [HideInInspector]
    public bool abilityUsed;
    [Tooltip("Magnetic impulse prefab")]
    public GameObject magneticImpulse;
    [Tooltip("% damage to enable ability attack")]
    public float abilityAttack;

    public GameObject whirlwindFx;

    protected override void Start()
    {
        base.Start();

        bossStats = GetComponent<CharacterStatistics>();
        tempHealth = bossStats.currentHealth;
    }

    void Update()
    {
        if(whirlwindCooldown < whirlwindTimer)
        {
            whirlwindCooldown += Time.deltaTime;
        }
    }
}
