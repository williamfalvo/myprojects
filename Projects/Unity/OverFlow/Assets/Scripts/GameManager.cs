using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[System.Serializable]
public struct Plot
{
    public bool missionReport;

    [SerializeField]
    Mission mission;

    [SerializeField]
    DialogueSystem npc;

    public bool MissionReport { get => missionReport; set => missionReport = value; }
    public Mission Mission { get => mission; set => mission = value; }
    public DialogueSystem Npc { get => npc; set => npc = value; }
}

public class GameManager : MonoBehaviour, IPersistentData
{
    public DebugUI debugUI; //DA RIMUOVERE POST TEST

    public delegate void Dungeon();
    public event Dungeon OnDungeonStart;

    public delegate void HitAction(GameObject hit, CharacterStatistics stats);
    public event HitAction OnHit;

    public NPCRigger rigger;
    public bool firstTimeDeath = true;

    //public delegate void CardInfoAction(List<string> descriptions);
    //public event CardInfoAction OnCardInfo;

    public delegate void CInfo(string description);
    public event CInfo OnCInfo;

    public delegate void SInfo(string description, int points);
    public event SInfo OnSInfo;

    public delegate void CounterChange(string carName, int newCounter);
    public event CounterChange OnCounterChange;

    public delegate void MissionComplete();
    public event MissionComplete OnMissionComplete;

    public delegate void ExitDungeon();
    public event ExitDungeon OnExitDungeon;

    public static GameManager instance = null;

    public GameObject playerPrefab;

    public TutorialManager tutorial;

    public RoomBehaviour currentRoom;

    public bool tutorialOn = true;

    [Space(5), Header("MISSIONS ARRAY")]
    public Plot[] missions; // list of missions

    [HideInInspector]
    public Plot CurrentMission { get; set; }
    [HideInInspector]
    public int indexMission;

    [Space(5), Header("GAME MANAGER PARAMETERS")]
    public CharacterStatistics player;
    public GameObject p;

    [HideInInspector]
    public PlayerController playerController;

    [HideInInspector]
    public bool pausable;

    Dictionary<string, HubInventoryController1.CardElements> dict = new Dictionary<string, HubInventoryController1.CardElements>();

    public Dictionary<string, HubInventoryController1.CardElements> Dict
    {
        get { return dict; }
        set { dict = value; }
    }

    #region Variables
    [Space(5), Header("CARDS")]

    public CardInventory inventory;
    public CardInventory temporary;
    public CardInventory deck;
    public CardInventory hand;
    public CardInventory inGameInventory;
    public CardInventory objectsInventory;
    public CardInventory graveyard;
    public CardInventory collection;
    public CardInventory lastCollection;

    public SkillTree skillTree;
    Skill skills;

    HashSet<string> unlockedSkillsNames;
    HashSet<string> equippedSkillsNames;
    HashSet<Skill> equippedSkills;

    public TriggerManager triggerManager;

    public InputManager inputManager;

    public Damager damager;

    [Header("GAME TIME"), Space(5)]
    public float gameTimeF;
    int seconds;
    int minutes;
    int hours;
    public string gameTimeS;

    //Game completion
    float gameCompletion;
    float missionsCompleted;
    [HideInInspector]
    public bool newMission = true;
    [HideInInspector]
    public bool newCards;

    GameObject lastSelected;
    
    public float slidersVolumeMusic;
    public float slidersVolumeSFX;

    #endregion
    
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
            DataManager.instance.AddSubscriber(this);

            p = Instantiate(playerPrefab, gameObject.transform.position, Quaternion.identity);
            player = p.GetComponent<CharacterStatistics>();

            playerController = player.GetComponent<PlayerController>();
            playerController.inputManager = inputManager;
            playerController.onHub = true;

            pausable = true;

            OrganizeCollection();
        }

        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //disable the mouse
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void OnEnable()
    {
        if (DataManager.instance.currentSlot == 0 && DataManager.instance.dataSaved[0].gTF >= 1f)
        {
            DataManager.instance.LoadData();
            gameTimeF = DataManager.instance.dataSaved[0].gTF;
        }

        if (DataManager.instance.currentSlot == 1 && DataManager.instance.dataSaved[1].gTF >= 1f)
        {
            DataManager.instance.LoadData();
            gameTimeF = DataManager.instance.dataSaved[1].gTF;
        }

        if (DataManager.instance.currentSlot == 2 && DataManager.instance.dataSaved[2].gTF >= 1f)
        {
            DataManager.instance.LoadData();
            gameTimeF = DataManager.instance.dataSaved[2].gTF;
        }

        DataManager.instance.LoadLastSlot();
        DataManager.instance.lastSlotSelected.lastSS = DataManager.instance.currentSlot;
        DataManager.instance.SaveLastSlot();
    }

    public void OnDestroy()
    {
        if (instance == this)
        {
            if (DataManager.instance != null)
            {
                DataManager.instance.RemoveSubscriber(this);
            }
            instance = null;
        }
    }

    void Update()
    {
        //Game Time
        gameTimeF += Time.deltaTime;

        seconds = (int)(gameTimeF % 60);
        minutes = (int)(gameTimeF / 60) % 60;
        hours = (int)(gameTimeF / 3600) % 24;

        gameTimeS = string.Format("{0:0}:{1:00}:{2:00}", hours, minutes, seconds);

        // if the player clicks the mouse (invisible) reset the selection to the last selected
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            EventSystem.current.SetSelectedGameObject(lastSelected);
        }

        if (EventSystem.current.currentSelectedGameObject != null)
        {
            lastSelected = EventSystem.current.currentSelectedGameObject;
        }
    }

    //Game completion
    public void CheckGameCompletion()
    {
        missionsCompleted = 0f;
        gameCompletion = 0f;
        for (int i = 0; i < missions.Length; i++)
        {
            if(newMission && gameCompletion < 100)
            {
                if (missions[i].MissionReport == true)
                {
                    missionsCompleted += 1;
                    gameCompletion = missionsCompleted / 6f * 100f;
                }
            }
        }
        newMission = false;
    }

    public void OrganizeCollection()
    {
        List<Card> newCollection = new List<Card>();

        foreach (Card c in inventory.cards)
        {
            if (newCollection.Contains(c)) continue;

            newCollection.Add(c);
        }

        collection.cards = newCollection;
        lastCollection.cards = newCollection;
    }

    //draws the entire hand
    public void DrawCards()
    {
        for (int i = 0; hand.maxCards > hand.cards.Count && deck.cards.Count > 0; i++)
        {
            Card c = ScriptableObject.Instantiate(deck.cards[0]);

            for (int j = 0; j < c.triggers.Count; j++)
            {
                for (int k = 0; k < c.triggers[j].effects.Count; k++)
                {
                    Effect ef = ScriptableObject.Instantiate(c.triggers[j].effects[k]);
                    c.triggers[j].effects[k] = ef;
                }
            }

            hand.AddCard(c);
            deck.RemoveCard(deck.cards[0]);
        }
    }

    public void SetMission(int missionIndex)
    {
        indexMission = missionIndex;
        CurrentMission = missions[missionIndex];
    }

    public void MissionCompleted()
    {
        missions[indexMission].missionReport = true;
    }

    public void DungeonStarted()
    {
        OnDungeonStart?.Invoke();
    }

    public void Hit(GameObject g, CharacterStatistics s)
    {
        OnHit?.Invoke(g, s);
    }

    //public void CardInfos(List<string> ds)
    //{
    //    OnCardInfo?.Invoke(ds);
    //}

    public void SkillInfos(string d, int p)
    {
        OnSInfo?.Invoke(d, p);
    }

    public void Infos(string d)
    {
        OnCInfo?.Invoke(d);
    }

    public void CounterChanged(string str, int count)
    {
        OnCounterChange?.Invoke(str, count);
    }

    public void DecreaseCardLife()
    {
        //Decrease a life of all card in inventory
        foreach(Card c in inGameInventory.cards)
        {
            c.cardLife--;

            //Check if card's life is under 0
            if (c.cardLife == 0)
            {
                triggerManager.CardToRemove.Add(c);
            }
        }

        //Remove all cards that have life under 0
        triggerManager.RemoveCards();
    }

    public void DecreaseCardsLife(Card.Archetypes archetype)
    {
        foreach (Card c in inGameInventory.cards)
        {
            if (c.cardArchetypes[0] != archetype)
            {
                c.cardLife--;

                triggerManager.CardConsumed(c);

                //Check if card's life is under 0
                if (c.cardLife == 0)
                {
                    triggerManager.CardToRemove.Add(c);
                }
            }
        }

        triggerManager.RemoveCards();
    }

    public void SetPause()
    {
        StartCoroutine(SetPauseCo());
    }

    IEnumerator SetPauseCo()
    {
        yield return new WaitForEndOfFrame();

        pausable = !pausable;
    }

    public bool IsPanelOver(bool b)
    {
        return b;
    }

    public void SaveDataTo(DataToSave data)
    {
        //Save cards
        data.temporaryCards = new List<string>();
        foreach (Card c in temporary.cards)
        {
            data.temporaryCards.Add(c.name);
        }

        data.inventoryCards = new List<string>();
        foreach (Card c in inventory.cards)
        {
            data.inventoryCards.Add(c.name);
        }

        data.deckCards = new List<string>();
        foreach (Card c in deck.cards)
        {
            data.deckCards.Add(c.name);
        }

        //Save stats
        data.strength = player.baseStrength;
        data.intellect = player.baseIntellect;
        data.agility = player.baseAgility;
        data.neuralHealth = player.baseNeuralHealth;
        data.luck = player.baseLuck;
        data.neuralResistance = player.baseNeuralResistence;
        data.ability = player.baseAbility;
        data.defense = player.baseDefense;
        data.exp = player.exp;
        data.level = player.level;
        data.attributeP = player.attributePoints;
        data.skillP = player.skillPoints;
        data.slotsDeck = deck.maxCards;

        //Save game time
        data.gTF = gameTimeF;
        data.gTS = gameTimeS;

        //Save game completion
        data.mission1 = missions[0].MissionReport;
        data.mission2 = missions[1].MissionReport;
        data.mission3 = missions[2].MissionReport;
        data.mission4 = missions[3].MissionReport;
        data.mission5 = missions[4].MissionReport;
        data.mission6 = missions[5].MissionReport;
        data.gCompletion = (int)gameCompletion;

        //Save talent tree
        data.unlockedSkills = unlockedSkillsNames != null ? unlockedSkillsNames.ToList() : null;
        data.equippedSkills = equippedSkills != null ? equippedSkills.Select(s => s.name).ToList() : null;

        //Save NPC Rigger exchange index
        if (rigger != null)
        {
            data.exIndex = rigger.currentExchangeIndex;
        }
        
        //Save sliders' volume
        data.sVMusic = FMODManager.instance.slidersVolumeMusic;
        data.sVSFX = FMODManager.instance.slidersVolumeSFX;
    }

    public void LoadDataFrom(DataToSave data)
    {
        //Load cards
        CardsResources resources = new CardsResources();

        temporary.cards.Clear();
        foreach (string name in data.temporaryCards)
        {
            temporary.cards.Add(resources.InstantiateCardWithName(name));
        }

        inventory.cards.Clear();
        foreach (string name in data.inventoryCards)
        {
            inventory.cards.Add(resources.InstantiateCardWithName(name));
        }

        deck.cards.Clear();
        foreach (string name in data.deckCards)
        {
            deck.cards.Add(resources.InstantiateCardWithName(name));
        }

        //Load stats
        player.baseStrength = data.strength;
        player.baseIntellect = data.intellect;
        player.baseAgility = data.agility;
        player.baseNeuralHealth = data.neuralHealth;
        player.baseLuck = data.luck;
        player.baseNeuralResistence = data.neuralResistance;
        player.baseAbility = data.ability;
        player.baseDefense = data.defense;
        player.exp = data.exp;
        player.level = data.level;
        player.attributePoints = data.attributeP;
        player.skillPoints = data.skillP;
        deck.maxCards = data.slotsDeck;

        player.NeuralHealthModified();

        //Load game time
        gameTimeF = data.gTF;
        gameTimeS = data.gTS;

        //Load game completion
        missions[0].MissionReport = data.mission1;
        missions[1].MissionReport = data.mission2;
        missions[2].MissionReport = data.mission3;
        missions[3].MissionReport = data.mission4;
        missions[4].MissionReport = data.mission5;
        missions[5].MissionReport = data.mission6;
        gameCompletion = data.gCompletion;

        //Load talent tree
        unlockedSkillsNames = new HashSet<string>(data.unlockedSkills);
        equippedSkillsNames = new HashSet<string>(data.equippedSkills);
        equippedSkills = null;

        //Load NPC Rigger exchange index
        if (rigger != null)
        {
            rigger.currentExchangeIndex = data.exIndex;
        }

        //Load sliders' volume

    }

    private void LoadEquippedSkills()
    {
        if (equippedSkills == null)
        {
            equippedSkills = new HashSet<Skill>();

            if (equippedSkillsNames != null)
            {
                foreach (Tier t in skillTree.CurrentTiers)
                {
                    foreach (Skill s in t.skills)
                    {
                        if (equippedSkillsNames.Contains(s.name))
                        {
                            equippedSkills.Add(s);
                        }
                    }

                    foreach (Skill s in t.attributes)
                    {
                        if (equippedSkillsNames.Contains(s.name))
                        {
                            equippedSkills.Add(s);
                        }
                    }
                }
            }
        }
    }

    public void EquipSkill(Skill s)
    {
        LoadEquippedSkills();
        equippedSkills.Add(s);
    }

    public void UnequipSkill(Skill s)
    {
        LoadEquippedSkills();
        equippedSkills.Remove(s);
    }

    public bool IsSkillEquipped(Skill s)
    {
        LoadEquippedSkills();
        return equippedSkills.Contains(s);
    }

    public IEnumerable<Skill> EquippedSkills
    {
        get
        {
            LoadEquippedSkills();
            return equippedSkills;
        }
    }

    public void UnlockSkill(Skill s)
    {
        if (unlockedSkillsNames == null)
        {
            unlockedSkillsNames = new HashSet<string>();
        }
        unlockedSkillsNames.Add(s.name);
    }

    public bool IsSkillUnlocked(Skill s)
    {
        return unlockedSkillsNames != null ? unlockedSkillsNames.Contains(s.name) : false;
    }

    public void MissionCompleteCall()
    {
        OnMissionComplete?.Invoke();
    }

    public void ExitDungeonCall()
    {
        OnExitDungeon?.Invoke();

        //Check game completion and save all the data
        CheckGameCompletion();
        DataManager.instance.SaveData();
        StartCoroutine(DataManager.instance.SavingFeedback());
        newMission = true;
    }
}
