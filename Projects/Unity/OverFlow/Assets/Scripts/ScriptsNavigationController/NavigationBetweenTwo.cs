﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NavigationBetweenTwo : MonoBehaviour
{
    public GameObject handPanel;
    public GameObject readyButton;

    Button button1;
    Button button2;
    Navigation myNavigation;

    void Start()
    {
        //setup the navigation between 2 buttons
        myNavigation = new Navigation();
        myNavigation.mode = Navigation.Mode.Explicit;
        myNavigation.selectOnDown = null;
        myNavigation.selectOnUp = null;
        myNavigation.selectOnLeft = button2;
        myNavigation.selectOnRight = button2;
        button1.navigation = myNavigation;

        myNavigation = new Navigation();
        myNavigation.mode = Navigation.Mode.Explicit;
        myNavigation.selectOnDown = null;
        myNavigation.selectOnUp = null;
        myNavigation.selectOnLeft = button1;
        myNavigation.selectOnRight = button1;
        button2.navigation = myNavigation;
    }

    private void OnEnable()
    {
        button1 = transform.GetChild(0).GetComponent<Button>();
        button2 = transform.GetChild(1).GetComponent<Button>();
        StartCoroutine(SelectedDelayed());
    }

    private void OnDisable()
    {
        EventSystem.current.SetSelectedGameObject(handPanel.transform.GetChild(0).GetChild(0).gameObject);
    }

    IEnumerator SelectedDelayed()
    {
        yield return new WaitForEndOfFrame();
        
        EventSystem.current.SetSelectedGameObject(button1.gameObject);
    }
}







