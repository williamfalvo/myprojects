﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicVolumeSetter : MonoBehaviour
{
    Slider mySliderMusic;
    Slider mySliderSFX;

    // Start is called before the first frame update
    void Start()
    {
        UpdateSound();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateSound()
    {
        mySliderMusic.value = GameManager.instance.slidersVolumeMusic;
        mySliderSFX.value = GameManager.instance.slidersVolumeSFX;
    }
}
