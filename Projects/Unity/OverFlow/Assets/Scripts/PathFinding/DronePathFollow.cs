﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DronePathFollow : PathFollowBehaviour
{
    protected override void MoveOnPath()
    {
        //Calculate the distance between transform and next point on X axis
        float distanceX = (currentTarget.transform.position.x - transform.position.x);
        float directionX = Mathf.Sign(distanceX);

        //Calculate the distance between transform and next point on X axis
        float distanceY = (currentTarget.transform.position.y + 0.5f - transform.position.y);
        float directionY = Mathf.Sign(distanceY);

        //Look at next point 
        transform.localScale = new Vector3(directionX, transform.localScale.y, transform.localScale.z);

        //Set the next movement
        Vector3 enemyMovementX = transform.right * (directionX * enemy.speed * Time.deltaTime);
        Vector3 enemyMovementY = transform.up * (directionY * enemy.speed * Time.deltaTime);

        //Move to next point
        if(distanceY < -0.2f || distanceY > 0.2f)
        {
            enemy.rb.MovePosition(enemy.rb.position + enemyMovementX + enemyMovementY);
        }
        else
        {
            enemy.rb.MovePosition(enemy.rb.position + enemyMovementX);
        }
        

    }
}
