﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AStar : MonoBehaviour
{
    [HideInInspector]
    public Node temp_StartNode;
    [HideInInspector]
    public Node temp_EndNode;

    public GraphMaker graphMaker;

    [HideInInspector]
    public bool isCalculatingPath;

    public PathFollowBehaviour pathFollow;

    private void Awake()
    {
        pathFollow = GetComponent<PathFollowBehaviour>();
    }

    //Calculate new path
    public void FindPath(Node startNode, Node endNode)
    {        
        //Stop all curret FindPath activated
        StopAllCoroutines();
        //Start coroutine that calculate new path
        StartCoroutine(Perform(startNode, endNode));
    }

    public IEnumerator Perform(Node startNode, Node endNode)
    {        
        isCalculatingPath = true;

        temp_StartNode = startNode;
        
        //Check if have a node to reach
        if(endNode != null)
        {
            temp_EndNode = endNode;
        }
        else
        {
            yield break;
        }
                
        List<Node> allNodes = graphMaker.nodes;

        // Search 
        List<Node> openset = new List<Node>();
        List<Node> visitedNodes = new List<Node>();

        openset.Add(startNode);
        bool endNodeFound = false;

        //check a node , if its not the end node add his neighbours to the list ("openset") and keep cycling in the same way
        while ( openset.Count > 0 && !endNodeFound)
        {
            Node current = GetBestNode(openset);
            openset.Remove(current);
            visitedNodes.Add(current);
            current.col = Color.red;

            if (current == endNode)
            {
                endNodeFound = true;
            }
            else
            {
                var neighbours = graphMaker.GetNeighbours(current);

                //add the neighbours and calculate their costs
                foreach (Node neighbour in neighbours)
                {
                    if (visitedNodes.Contains(neighbour)) continue;
                    if (openset.Contains(neighbour)) continue;

                    //cost to move to the next neighbour
                    float edgeCost = (neighbour.transform.position - current.transform.position).magnitude;
                    //calculate the heuristic cost (distance of the neighbour from the node to reach)
                    float heuristic = (neighbour.transform.position - endNode.transform.position).magnitude;
                    neighbour.cost = current.cost + edgeCost;
                    neighbour.h = heuristic;

                    openset.Add(neighbour);
                    neighbour.col = Color.yellow;
                }
            }                
        }

        //the path cant be found 
        if (!endNodeFound)
        {
            pathFollow.canFollow = false;
            yield break;
        }

        // Build the path
        List<Edge> path = new List<Edge>();
        Node tempNode = endNode;

        foundPathTransforms = new List<Transform>();
        foundPathTransforms.Add(tempNode.transform);

        Node shortestPathNode = null;
        Edge shortestPathEdge = null;
            
        //""we will change recursively the tempnode with a node one step closer to the one who is searching
        while(tempNode != startNode)
        {
            float minCost = Mathf.Infinity;

            //for each edge that links the endnode to another node
            foreach (Edge edge in graphMaker.GetEdges(tempNode))
            {
                //checks the node on the other side of the edge and gets the one with lower cost recursively
                if (tempNode == edge.n1)
                {
                    if (visitedNodes.Contains(edge.n2) && edge.n2.cost < minCost)
                    {
                        minCost = edge.n2.cost;
                        shortestPathNode = edge.n2;
                        shortestPathEdge = edge;
                    }
                }
                if (tempNode == edge.n2)
                {
                    if (visitedNodes.Contains(edge.n1) && edge.n1.cost < minCost)
                    {
                        minCost = edge.n1.cost;
                        shortestPathNode = edge.n1;
                        shortestPathEdge = edge;
                    }
                }
            }
            path.Add(shortestPathEdge);
            foundPathTransforms.Add(shortestPathNode.transform);

            //will calculate the next cycle with the closer node with the lower cost
            tempNode = shortestPathNode;
        }
        path.Reverse();
        foundPathTransforms.Reverse();

        // Color the path            
        for (int i = 0; i < path.Count; i++)
        {
            path[i].col = Color.Lerp(Color.green, Color.cyan, (float)i / path.Count);
        }

        pathFollow.ResetState();
        isCalculatingPath = false;
    }

    public List<Transform> foundPathTransforms = new List<Transform>();


    private Node GetBestNode(List<Node> openset)
    {
        float minCost = Mathf.Infinity;
        Node bestNode = null;
        foreach (var node in openset)
        {
            //the tot cost of every node is given by the actual cost + heuristic value
            float totCost = node.cost + node.h;
            if(totCost < minCost)
            {
                minCost = totCost;
                bestNode = node;
            }
        }
        return bestNode; //return the node with the lowest cost
    }
}

