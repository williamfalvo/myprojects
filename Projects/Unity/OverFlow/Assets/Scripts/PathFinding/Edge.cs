﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge : MonoBehaviour
{
    public Node n1;
    public Node n2;

    //rewrite the equals for the edge
    public override bool Equals(object other)
    {
        Edge other_edge = (Edge)other;
        if (other_edge == null) return false;

        return (n1 == other_edge.n1 && n2 == other_edge.n2)
            || (n1 == other_edge.n2 && n2 == other_edge.n1);
    }

    public Color col = Color.white;

}


