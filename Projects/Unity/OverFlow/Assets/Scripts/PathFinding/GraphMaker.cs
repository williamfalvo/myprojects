﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GraphMaker : MonoBehaviour
{
    // Graph
    public List<Node> nodes;
    public List<Edge> edges;

    // Parameters
    private void Start()
    {
        edges = new List<Edge>();
                
        // link every node
        foreach(Node n in nodes)
        {
            n.graphMaker = this;

            foreach(Node other_n in n.nodes)
            {
                if (n == other_n) continue;

                var go = new GameObject("Edge");
                var edge = go.AddComponent<Edge>();
                go.transform.SetParent(transform);
                go.hideFlags = HideFlags.HideInHierarchy;
                edge.n1 = n;
                edge.n2 = other_n;

                //assure that the link happens just once
                if (!edges.Contains(edge))
                {
                    edges.Add(edge);
                }
                else
                {
                    Destroy(go);
                }

            }
        }        
    }

    //gets the neightbours of every node
    public List<Node> GetNeighbours(Node n)
    {
        List<Node> neighbours = new List<Node>();
        foreach (var edge in edges)
        {
            if (n == edge.n1) neighbours.Add(edge.n2);
            if (n == edge.n2) neighbours.Add(edge.n1);            
        }
        return neighbours;
    }

    //add the nodes that the edge links
    public List<Edge> GetEdges(Node node)
    {
        List<Edge> foundEdges = new List<Edge>();

        foreach(var edge in edges)
        {
            if (edge.n1 == node) foundEdges.Add(edge);
            else if (edge.n2 == node) foundEdges.Add(edge);
        }
        return foundEdges;
    }

    public void ResetGraph()
    {
        Start();
    }

    [ContextMenu("Create Graph")]
    void CreateGraph()
    {
        nodes = new List<Node>(FindObjectsOfType<Node>());
    }
}

#if UNITY_EDITOR
[InitializeOnLoadAttribute]
public static class GraphControl
{
    static GraphControl()
    {
        EditorApplication.hierarchyChanged += OnHierarchyChanged;
    }

    //if something changes from editor it recreates the graph
    static void OnHierarchyChanged()
    {
        if (!EditorApplication.isPlaying)
        {
            GraphMaker graph = GameObject.FindObjectOfType<GraphMaker>();
            if (graph != null)
            {
                graph.nodes = new List<Node>(GameObject.FindObjectsOfType<Node>());
            }
        }
    }
}
#endif


