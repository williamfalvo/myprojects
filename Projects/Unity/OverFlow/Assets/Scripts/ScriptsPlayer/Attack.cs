﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : StateMachineBehaviour
{
    GameObject colliderWeapon;    
    PlayerController player;

    public float speed;
    public string nextAttackTrigger;
    public Attacks attacks;

    public enum Attacks
    {
        none,
        firstAttack,
        secondAttack,
        thirdAttack
    }

    public bool startCombo;
    public bool isHit;
    public bool finalCombo;    

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Enable the player's weapon collider
        player = animator.GetComponent<PlayerController>();
        player.GetComponent<Rigidbody>().AddForce(Vector3.right * speed * player.transform.localScale.x);

        if (nextAttackTrigger == "" && isHit)
        {
            player.thirdAttack = true;
        }

        if (isHit)
        {
            player.EnableCollider();
        }

        if (startCombo)
        {
            switch (attacks)
            {
                case (Attacks.firstAttack):
                    {
                        if (animator.GetFloat("WeaponChange") == 0)
                        {
                            FMODManager.instance.KatanaFirstAttack();
                            break;
                        }

                        if (animator.GetFloat("WeaponChange") == 0.5)
                        {
                            FMODManager.instance.SpearFirstAttack();
                            break;
                        }

                        if (animator.GetFloat("WeaponChange") == 1)
                        {
                            FMODManager.instance.HammerFirstAttack();
                        }
                        break;
                    }

                case (Attacks.secondAttack):
                    {
                        if (animator.GetFloat("WeaponChange") == 0)
                        {
                            FMODManager.instance.KatanaSecondAttack();
                            break;
                        }

                        if (animator.GetFloat("WeaponChange") == 0.5)
                        {
                            FMODManager.instance.SpearSecondAttack();
                            break;
                        }

                        if (animator.GetFloat("WeaponChange") == 1)
                        {
                            FMODManager.instance.HammerSecondAttack();
                        }
                        break;
                    }

                case (Attacks.thirdAttack):
                    {
                        if (animator.GetFloat("WeaponChange") == 0)
                        {
                            FMODManager.instance.KatanaThirdAttack();
                            break;
                        }

                        if (animator.GetFloat("WeaponChange") == 0.5)
                        {
                            FMODManager.instance.SpearThirdAttack();
                            break;
                        }

                        if (animator.GetFloat("WeaponChange") == 1)
                        {
                            FMODManager.instance.HammerThirdAttack();
                        }
                        break;
                    }
            }

            if (player.horizontalMove < -0.2f || player.horizontalMove > 0.2f)
            {
                    float direction = Mathf.Sign(player.horizontalMove);
                player.rb.transform.localScale = new Vector3(direction, 1f, 1f);
            }
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Active the next combo attack
        if (Input.GetButtonDown("Attack") && !finalCombo)
        {

            animator.SetTrigger(nextAttackTrigger);
            animator.SetBool("comboActive", true);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("comboActive", false);

        if (isHit)
        {
            player.DisableCollider();
        }
    }
}
