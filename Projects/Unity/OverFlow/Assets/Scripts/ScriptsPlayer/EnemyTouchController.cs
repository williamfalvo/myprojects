﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTouchController : MonoBehaviour
{
    public PlayerController player;
    bool touchEnemy;
    float cooldown;

    public float timeSpeedDown;
    public float multiplierSpeed = 0.5f;

    float enterDirection;
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 10 && player.gameObject.layer == 11) //Enemy layer & Player isn't on dash move
        {
            touchEnemy = true;
            cooldown = 0f;
            enterDirection = Mathf.Sign(player.horizontalMove);
        }

        if (player.isInvulnerable && other.gameObject.layer == 10)
        {
            GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.Dash, other.gameObject.GetComponent<Enemy>());
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (touchEnemy)
        {
            ExitTouch();
        }
    }

    //the player speed is reduced if on collision with enemy
    private void Update()
    {
        if(touchEnemy)
        {
            if(enterDirection != Mathf.Sign(player.horizontalMove))
            {
                ExitTouch();
                return;
            }

            if(cooldown < timeSpeedDown)
            {
                cooldown += Time.deltaTime;
                player.speed = player.modifiedSpeed * multiplierSpeed;
            }
            else
            {                
                player.speed = player.modifiedSpeed;
                touchEnemy = false;
            }
        }
    }

    void ExitTouch()
    {
        player.speed = player.modifiedSpeed;
        touchEnemy = false;
    }
}
