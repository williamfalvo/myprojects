﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idle : StateMachineBehaviour
{
    GameObject colliderWeapon;
    PlayerController player;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //security check on the trigger
        animator.ResetTrigger("Attack1");
        animator.ResetTrigger("Attack2");
        animator.ResetTrigger("Attack3");
        animator.ResetTrigger("Jump");
        animator.SetBool("Stunned", false);
        player = animator.GetComponent<PlayerController>();
        player.playerState = PlayerController.PlayerState.idle;
        colliderWeapon = player.colliderWeapon;
        colliderWeapon.SetActive(false);
    }
}
