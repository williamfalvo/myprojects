﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTechLoaderXML : MonoBehaviour
{

    [Tooltip("The XML file we get the data from")]
    public TextAsset techBabToGet; //we ll get the data from here

    CharacterStatistics playerStats;

    //private XmlDataGetter xmlDataGetter; // the xml values will be stored here
    private PlayerTechBab[] playerTechValues;

    // Start is called before the first frame update
    void Awake()
    {
        playerStats = GetComponent<CharacterStatistics>();
        playerTechValues = XmlDataGetter.LoadFromXML<PlayerTechBab>(techBabToGet);


        for (int i = 0 ; i < playerTechValues.Length ; i++)
        {
            playerStats.techBabValues[i].Bab = playerTechValues[i].BAB;
            playerStats.techBabValues[i].scaling = playerTechValues[i].SCALING;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
