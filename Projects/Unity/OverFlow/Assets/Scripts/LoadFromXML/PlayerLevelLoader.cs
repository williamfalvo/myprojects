﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLevelLoader : MonoBehaviour
{
    [Tooltip("The XML file we get the data from")]
    public TextAsset levelsToGet; //we ll get the data from here


    CharacterStatistics player;
    private CharacterStatistics.LevelScaling[] levelScaling;

    // Start is called before the first frame update
    void Start()
    {
      
        player = GetComponent<CharacterStatistics>();
        for (int i = 0; i < player.levelValues.Length; i++)
        {
            player.levelValues[i].katanaScaling = 0;
        }

        levelScaling = XmlDataGetter.LoadFromXML<CharacterStatistics.LevelScaling>(levelsToGet);

        for (int i=0 ; i < levelScaling.Length; i++)
        {
            player.levelValues[i].katanaScaling = levelScaling[i].katanaScaling;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
