﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTechAbilitiesLoaderXML : MonoBehaviour
{
    [Tooltip("The XML file we get the data from")]
    public TextAsset techAbilitiesToGet; //we ll get the data from here

    CharacterStatistics player;
    private CharacterStatistics.TechScaling[] techScalings;

    private void Start()
    {
        player = GetComponent<CharacterStatistics>();
        techScalings = XmlDataGetter.LoadFromXML<CharacterStatistics.TechScaling>(techAbilitiesToGet);

        for(int i= 0; i< techScalings.Length; i++)
        {
            for (int j = 0; j <= i; j++)
            {
                player.intellectValues[i].shieldDuration += techScalings[j].shieldDuration;
            }
            player.intellectValues[i].projectileDamage = techScalings[i].projectileDamage;
            player.intellectValues[i].shockwaveRange = techScalings[i].shockwaveRange;

            for (int j = 0; j <= i; j++)
            {
                player.intellectValues[i].blackoutDuration += techScalings[j].blackoutDuration;
            }
            for (int j = 0; j <= i; j++)
            {
                player.intellectValues[i].companionDuration += techScalings[j].companionDuration;
            }
            player.intellectValues[i].shockwaveDamage = techScalings[i].shockwaveDamage;
            player.intellectValues[i].companionDamage = techScalings[i].companionDamage;
        }
    }
}
