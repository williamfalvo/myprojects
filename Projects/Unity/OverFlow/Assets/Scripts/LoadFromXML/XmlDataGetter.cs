﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;


//struct that will incapsulate all our values
[System.Serializable]
public struct EnemyLevelValues
{
    [XmlAttribute]
    public int LIVELLO;
    public int STRENGTH;
    public int DEXTERITY;
    public int TECH;
    public int NEURALHEALTH;
    public int NEURALRESISTANCE;

}

public struct PlayerTechBab
{
    [XmlAttribute]
    public int Tech;
    public float BAB;
    public float SCALING;
}




//will read the XML file and insert the datas in his array
public class XmlDataGetter
{
    
    public EnemyLevelValues[] enemyValues;
    public PlayerTechBab[] playerTechValues;

    //static function that actually charges the data
    public static XmlDataGetter LoadFromXML(TextAsset xmlAsset)
    {
        var serializer = new XmlSerializer(typeof(XmlDataGetter));
        return serializer.Deserialize(new StringReader(xmlAsset.text)) as XmlDataGetter;
    }

    public static XmlDataGetter LoadPlayerTechBabXML(TextAsset xmlAsset)
    {
        var serializer = new XmlSerializer(typeof(XmlDataGetter));
        return serializer.Deserialize(new StringReader(xmlAsset.text)) as XmlDataGetter;
    }

    //GENERIC LOADER directly returns the array of struct needed based on the type of the caller
    public static T[] LoadFromXML<T>(TextAsset xmlAsset) where T : struct
    {
        var serializer = new XmlSerializer(typeof(T[]), new XmlRootAttribute ("XmlDataGetter"));
        return serializer.Deserialize(new StringReader(xmlAsset.text)) as T[];
    }

}
