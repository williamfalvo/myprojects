﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameCardsPanel1 : MonoBehaviour
{
    [Range(0, 5), Tooltip("Time(s) to reach final position")]
    public float time;

    [Range(0, 1), Tooltip("How much(%) panel go down in base of its height \nEs. 1 = 100%")]
    public float amountDown;

    RectTransform rect;

    [HideInInspector]
    public bool isActive;
    [HideInInspector]
    public bool isActivated;

    public Image arrow;

    Vector3 startPosition;
    float startYPosition;
    float startSize;

    float finalPosition;    

    private void Awake()
    {
        rect = GetComponent<RectTransform>();

        startSize = rect.rect.height;

        finalPosition = startSize * -amountDown;

        rect.anchoredPosition = new Vector2(0, finalPosition);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive && GameManager.instance.inputManager.oneInput)
        {
            isActive = false;
        }

        //scales up
        if (isActive && !isActivated)
        {
            StopAllCoroutines();
            //GameManager.instance.inputManager.enabled = false;
            StartCoroutine(AnimationScale(0));
            isActivated = true;
            arrow.transform.localScale = new Vector3(1, 1, 1);
        }

        //scales down
        else if (!isActive && isActivated)
        {
            StopAllCoroutines();
            //GameManager.instance.inputManager.enabled = true;
            StartCoroutine(AnimationScale(finalPosition));
            isActivated = false;
            arrow.transform.localScale = new Vector3(1, -1, 1);
        }
    }

    //scale by a value
    protected IEnumerator AnimationScale(float finalScale)
    {
        WaitForEndOfFrame eof = new WaitForEndOfFrame();
        float startValue = rect.anchoredPosition.y;
        float animationAmount = 0.0f;
        float speed = 1.0f / time;

        while (animationAmount < 1.0f)
        {
            animationAmount = Mathf.Min(1.0f, animationAmount + Time.deltaTime * speed);

            float newPosition = Mathf.Lerp(startValue, finalScale, animationAmount);
            rect.anchoredPosition = new Vector2(0, newPosition);

            yield return eof;
        }
    }
}
