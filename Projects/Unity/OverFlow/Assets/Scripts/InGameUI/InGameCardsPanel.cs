﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InGameCardsPanel : MonoBehaviour
{
    [Range(0,5), Tooltip("Time(s) to reach final position")]
    public float time;

    [Range(0,1), Tooltip("How much(%) panel go down in base of its height \nEs. 1 = 100%")]
    public float amountDown;

    public Image[] panelCards;
    public TextMeshProUGUI[] panelText;
    int[] lifeCard;

    public TriggerManager triggerManager;

    RectTransform rect;

    [HideInInspector]
    public bool isActive;
    [HideInInspector]
    public bool isActivated;

    public Image arrow;

    Vector3 startPosition;
    float startYPosition;
    float startSize;

    float finalPosition;

    public HandController hand;
    public Sprite emptySlotImage;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();

        startSize = rect.rect.height;

        finalPosition = startSize * -amountDown;

        rect.anchoredPosition = new Vector2(0, finalPosition);

        lifeCard = new int[panelCards.Length];
    }

    private void Start()
    {
        GameManager.instance.inGameInventory.OnCardAdded += UpdateCards;
        GameManager.instance.inGameInventory.OnCardRemoved += UpdateCards;
        GameManager.instance.triggerManager.OnCardUsed += UpdateCards;
    }

    private void OnDestroy()
    {
        GameManager.instance.inGameInventory.OnCardAdded -= UpdateCards;
        GameManager.instance.inGameInventory.OnCardRemoved -= UpdateCards;
        GameManager.instance.triggerManager.OnCardUsed -= UpdateCards;
    }

    // Update is called once per frame
    void Update()
    {
        if(isActive && GameManager.instance.inputManager.oneInput)
        {
            isActive = false;
        }

        //scales up
        if (isActive && !isActivated)
        {
            StopAllCoroutines();
            //GameManager.instance.inputManager.enabled = false;
            StartCoroutine(AnimationScale(0));
            isActivated = true;
            arrow.transform.localScale = new Vector3(1, 1, 1);
        }

        //scales down
        else if (!isActive && isActivated)
        {
            StopAllCoroutines();
            //GameManager.instance.inputManager.enabled = true;
            StartCoroutine(AnimationScale(finalPosition));
            isActivated = false;
            arrow.transform.localScale = new Vector3(1, -1, 1);
        }
    }

    //scale by a value
    protected IEnumerator AnimationScale(float finalScale)
    {
        WaitForEndOfFrame eof = new WaitForEndOfFrame();
        float startValue = rect.anchoredPosition.y;
        float animationAmount = 0.0f;
        float speed = 1.0f / time;

        while (animationAmount < 1.0f)
        {
            animationAmount = Mathf.Min(1.0f, animationAmount + Time.deltaTime * speed);

            float newPosition = Mathf.Lerp(startValue, finalScale, animationAmount);
            rect.anchoredPosition = new Vector2(0, newPosition);

            yield return eof;
        }
    }

    public IEnumerator DissolveFx(Image img)
    {
        Coffee.UIExtensions.UIDissolve dissolve = img.gameObject.GetComponent<Coffee.UIExtensions.UIDissolve>();

        if (img.sprite != emptySlotImage)
        {
            dissolve.enabled = true;
        }        

        yield return new WaitForSeconds(dissolve.duration + 0.1f);

        dissolve.enabled = false;
        dissolve.play = true;

        img.sprite = emptySlotImage;
        
        //dissolve.effectFactor = 0f;
    }

    public void UpdateCards(Card card)
    {
        int i = 0;
        foreach (InGameSlot slot in hand.gameSlots)
        {
            int cardCount = slot.cards.Count;
            int cardImageCount = slot.cardImages.Count;

            if(cardCount == 0)
            {
                for (int k = 0; k < cardImageCount; k++)
                {
                    
                    panelCards[i].sprite = emptySlotImage;
                    
                    //panelCards[i].sprite = emptySlotImage;
                    panelText[i].text = "0/0";
                    lifeCard[i] = 0;
                    ++i;
                }
            }
            else
            {
                int indexCard = 0;
                foreach(Image img in slot.cardImages)
                {
                    if(img.sprite == emptySlotImage)
                    {
                        if (gameObject.activeSelf)
                        {
                            StartCoroutine(DissolveFx(panelCards[i]));
                        }
                        else
                        {
                            panelCards[i].sprite = emptySlotImage;
                        }
                        //panelCards[i].sprite = emptySlotImage;
                        panelText[i].text = "0/0";
                        lifeCard[i] = 0;
                        ++i;
                        continue;
                    }

                    int life = slot.cards[indexCard].cardLife;
                    panelCards[i].sprite = img.sprite;

                    if(life == 0)
                    {
                        if (gameObject.activeSelf)
                        {
                            StartCoroutine(DissolveFx(panelCards[i]));
                        }
                        else
                        {
                            panelCards[i].sprite = emptySlotImage;
                        }
                        
                        //panelCards[i].sprite = emptySlotImage;
                        panelText[i].text = "0/0";
                        lifeCard[i] = 0;
                    }
                    else if(life > lifeCard[i])
                    {
                        panelText[i].text = life + "/" + life;
                        lifeCard[i] = life;
                    }
                    else
                    {
                        panelText[i].text = life + "/" + lifeCard[i];
                    }

                    indexCard++;
                    ++i;                    
                }                
            }
        }
    }

    public void OpenPanel(Card c)
    {
        UpdateCards(c);

        gameObject.SetActive(true);
    }
}
