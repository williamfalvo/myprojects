﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InGameCanvasManager : MonoBehaviour
{
    public GameObject readyButton; //button to close the inventory panel 
    public DungeonCreation dungeonCreator;
    public GameObject inventoryPanel;
    public GameObject pausePanel;
    public StatisticsManagerUI statisticsManager;
    public MyButton endDrawButton;
    public InGameCardsPanel1 cardsPanel;
    public GameObject endServerPanel;

    bool inventoryWasOpen;
    ScenesManager sceneManager; 

    void Start()
    {
        FMODManager.instance.PlayChunkMusic();
        FMODManager.instance.PlayRainMusic();
        dungeonCreator.onRoomCleared += EnableInventoryInGame; // reopen the inventory 
        GameManager.instance.OnMissionComplete += EnableEndServerPanel;

        GameManager.instance.DrawCards(); // draw the cards
        sceneManager = GetComponent<ScenesManager>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("ChipSet") && !pausePanel.activeSelf && !inventoryPanel.activeSelf)
        {
            if (cardsPanel.isActive) cardsPanel.isActive = false;
            else cardsPanel.isActive = true;
        }
    }

    private void OnDisable()
    {
        if (GameManager.instance != null)
        {
            GameManager.instance.inputManager.enabled = true;
        }
    }

    private void OnDestroy()
    {
        if (FMODManager.instance != null)
        {
            FMODManager.instance.StopChunkMusic();
            FMODManager.instance.StopRainMusic();
        }
        dungeonCreator.onRoomCleared -= EnableInventoryInGame;
        GameManager.instance.OnMissionComplete -= EnableEndServerPanel;
    }

    //asks the scene manager to enable the build panel
    public void EnableInventoryInGame()
    {
        cardsPanel.gameObject.SetActive(false);

        sceneManager.PanelManager(inventoryPanel);

        GameManager.instance.pausable = false;
        GameManager.instance.DrawCards(); // draw the cards       

        statisticsManager.UpdateStatistics();

        StartCoroutine(SelectButton());
    }

    public void EnableEndServerPanel()
    {
        endServerPanel.SetActive(true);

        GameManager.instance.pausable = false;
        GameManager.instance.inputManager.enabled = false;
    }

    public IEnumerator SelectButton()
    {
        yield return new WaitForEndOfFrame();

        EventSystem.current.SetSelectedGameObject(null);

        EventSystem.current.SetSelectedGameObject(endDrawButton.gameObject);
    }

    public void PauseManager()
    {
        //if the pause panel is already open or not
        if (pausePanel.activeSelf)
        {
            Time.timeScale = 1;
            GameManager.instance.inputManager.enabled = true;
            sceneManager.PanelManager(pausePanel);

            if(inventoryWasOpen)
            {
                inventoryPanel.SetActive(true);
                EventSystem.current.SetSelectedGameObject(readyButton);
            }
            inventoryWasOpen = false;
        }
        else
        {
            if(inventoryPanel.activeSelf)
            {
                inventoryWasOpen = true;
                inventoryPanel.SetActive(false);
            }

            sceneManager.PanelManager(pausePanel);
            GameManager.instance.inputManager.enabled = false;
            Time.timeScale = 0;
            EventSystem.current.SetSelectedGameObject(pausePanel.transform.GetChild(1).gameObject);
        }
    }

}
