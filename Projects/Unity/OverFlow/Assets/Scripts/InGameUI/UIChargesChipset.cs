﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIChargesChipset : MonoBehaviour
{



    public TextMeshProUGUI weaponCharges;
    public TextMeshProUGUI armorCharges;
    public TextMeshProUGUI trinketCharges;
    public TextMeshProUGUI consumableOneCharges;
    public TextMeshProUGUI consumableTwoCharges;

    [HideInInspector]
    public int weaponMaxCharges;
    [HideInInspector]
    public int armorMaxCharges;
    [HideInInspector]
    public int trinketMaxCharges;
    [HideInInspector]
    public int consumableOneMaxCharges;
    [HideInInspector]
    public int consumableTwoMaxCharges;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitializeCharges(Card c , Card.Archetypes archetype , int index)
    {
        switch (archetype)
        {
            case Card.Archetypes.Consumable:

                if (index == 0)
                {

                    if (c == null)
                    {
                        ClearCharges(consumableOneCharges);
                    }
                    else
                    {
                        consumableOneMaxCharges = c.cardLife;
                        consumableOneCharges.text = c.cardLife.ToString() + "/" + c.cardLife.ToString();
                    }
                }
                else
                {

                    if (c == null)
                    {
                        ClearCharges(consumableTwoCharges);
                    }
                    else
                    {
                        consumableTwoMaxCharges = c.cardLife;
                        consumableTwoCharges.text = c.cardLife.ToString() + "/" + c.cardLife.ToString();
                    }
                }
                break;

            case Card.Archetypes.Armor:


                if (c == null)
                {
                    ClearCharges(armorCharges);
                }
                else
                {
                    armorMaxCharges = c.cardLife;
                    armorCharges.text = c.cardLife.ToString() + "/" + c.cardLife.ToString();
                }
                break;

            case Card.Archetypes.WeaponMod:


                if (c == null)
                {
                    ClearCharges(weaponCharges);
                }
                else
                {
                    weaponMaxCharges = c.cardLife;
                    weaponCharges.text = c.cardLife.ToString() + "/" + c.cardLife.ToString();
                }
                break;

            case Card.Archetypes.Trinket:


                if (c == null)
                {
                    ClearCharges(trinketCharges);
                }
                else
                {
                    trinketMaxCharges = c.cardLife;
                    trinketCharges.text = c.cardLife.ToString() + "/" + c.cardLife.ToString();
                }
                break;

        }

    }

    public void UpdateCharges(Card c, Card.Archetypes archetype, int index)
    {



        switch (archetype)
        {
            case Card.Archetypes.Consumable:

                if (index == 0)
                {
                    if (c == null)
                    {
                        ClearCharges(consumableOneCharges);
                    }
                    else
                    {
                        consumableOneCharges.text = c.cardLife.ToString() + "/" + consumableOneMaxCharges.ToString();
                    }
                }
                else
                {
                    if (c == null)
                    {
                        ClearCharges(consumableTwoCharges);
                    }
                    else
                    {
                        consumableTwoCharges.text = c.cardLife.ToString() + "/" + consumableTwoMaxCharges.ToString();
                    }
                }
                break;

            case Card.Archetypes.Armor:

                if (c == null)
                {
                    ClearCharges(armorCharges);
                }
                else
                {
                    armorCharges.text = c.cardLife.ToString() + "/" + armorMaxCharges.ToString();
                }
                break;

            case Card.Archetypes.WeaponMod:

                if (c == null)
                {
                    ClearCharges(weaponCharges);
                }
                else
                {
                    weaponCharges.text = c.cardLife.ToString() + "/" + weaponMaxCharges.ToString();
                }
                break;

            case Card.Archetypes.Trinket:

                if (c == null)
                {
                    ClearCharges(trinketCharges);
                }
                else
                {
                    trinketCharges.text = c.cardLife.ToString() + "/" + trinketMaxCharges.ToString();
                }
                break;

        }    

    }

    public void ClearCharges(TextMeshProUGUI text)
    {
        text.text = "0/0";
    }

}