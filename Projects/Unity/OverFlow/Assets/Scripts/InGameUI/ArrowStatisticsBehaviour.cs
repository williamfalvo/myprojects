﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowStatisticsBehaviour : MonoBehaviour
{

    public float time = 0.01f;

    public void startAnimation()
    {
        StopAllCoroutines();
        float newScale = Mathf.Lerp(1, 1, 1);
        transform.localScale = new Vector3(1, 1, 1);

        StartCoroutine(AnimationScale(0.5f));

    }




    public IEnumerator AnimationScale(float finalScale)
    {
        WaitForEndOfFrame eof = new WaitForEndOfFrame();
        float startValue = transform.localScale.x;
        float animationAmount = 0.0f;
        float speed = 3.0f / time;

        while (animationAmount < 1.0f)
        {
            animationAmount = Mathf.Min(1.0f, animationAmount + Time.deltaTime * speed);

            float newScale = Mathf.Lerp(startValue, finalScale, animationAmount);
            transform.localScale = new Vector3(newScale, newScale, 1);


            yield return eof;
        }
        transform.localScale = new Vector3(1, 1, 1);

    }
}
