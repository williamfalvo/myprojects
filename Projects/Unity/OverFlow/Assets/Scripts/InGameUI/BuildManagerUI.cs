﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManagerUI : MonoBehaviour
{
    public GameObject detailsPanel;
    public GameObject inspectPanel;
    public GameObject statisticsPanel;
    public GameObject tutorialText;
    public GameObject tutorialEndPanel;



    // Start is called before the first frame update
    void Start()
    {
        if(GameManager.instance.tutorialOn)
        {
            tutorialText.SetActive(true);
            tutorialEndPanel.SetActive(true);

        }
        else
        {
            tutorialText.SetActive(false);
            tutorialEndPanel.SetActive(false);

        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Interact"))
        {
            if(statisticsPanel.activeSelf)
            {
                statisticsPanel.SetActive(false);
                inspectPanel.SetActive(true);
            }
            else
            {
                statisticsPanel.SetActive(true);
                inspectPanel.SetActive(false);
            }
        }
    }

    private void OnDisable()
    {
        if (GameManager.instance != null)
        {
            GameManager.instance.inputManager.enabled = true;

            //update the health when that statistics gets updated
            GameManager.instance.player.NeuralHealthModified();
        }
    }
}
