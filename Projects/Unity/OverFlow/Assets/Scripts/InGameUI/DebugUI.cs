﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DebugUI : MonoBehaviour
{
    public TextMeshProUGUI debugData;
    float tempHp;

    public float danniInflitti;
    public float danniRicevuti;
    float hpRecuperati;

    public CharacterStatistics playerHealth;

    PlayerController playerC;

    //FPS
    public float deltatime;

    private void Start()
    {
        GameManager.instance.debugUI = this;
        playerHealth = GameManager.instance.player;
        tempHp = playerHealth.currentHealth;
        playerC = playerHealth.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {

        deltatime += (Time.deltaTime - deltatime) * 0.1f;
        float fps = 1.0f / deltatime;

        if(GameManager.instance.currentRoom != null)
        {
            debugData.text = "HP CORRENTI: " + playerHealth.currentHealth.ToString() + "\n\n" +
                         "HP RECUPERATI: " + hpRecuperati.ToString() + "\n\n" +
                         "DANNI INFLITTI: " + danniInflitti + "\n\n" +
                         "DANNI RICEVUTI: " + danniRicevuti + "\n\n" +
                         "ROOM: " + GameManager.instance.currentRoom + "\n\n" +
                         "SPEED: " + playerC.speed + "\n\n" +
                         //"FPS:  " + Mathf.Ceil(fps);
                         "FPS:  " + (int)(1/Time.unscaledDeltaTime);

        }

        if(playerHealth.currentHealth < tempHp)
        {
            tempHp = playerHealth.currentHealth;
        }

        if(playerHealth.currentHealth > tempHp)
        {
            hpRecuperati = playerHealth.currentHealth - tempHp;
        }
    }
}
