﻿using System.Collections;
using System.Collections.Generic;
using Coffee.UIExtensions;
using TMPro;
using UnityEngine;

public class RoomEnemiesCount : MonoBehaviour
{
    public TextMeshProUGUI enemies;
    RoomBehaviour currentRoom;
    int totRoomEnemies;
    int enemiesLeft;

    private void Start()
    {
        currentRoom = GameManager.instance.currentRoom;
        GameManager.instance.triggerManager.roomEnemiesCounter = this;

        GetComponentInChildren<UIShiny>().Stop();
    }

    bool shineFx = false;

    // Update is called once per frame
    void Update()
    {
        if (currentRoom != null)
        {
            enemiesLeft = currentRoom.enemies.Count;
            if(enemiesLeft > 0)
            {
                enemies.text = "Enemies " + enemiesLeft + " / " + totRoomEnemies;

                if (shineFx)
                {
                    GetComponentInChildren<UIShiny>().Stop();

                    shineFx = false;
                }
            }
            else
            {
                enemies.text = "Chunk Completed";

                if (shineFx == false)
                {
                    GetComponentInChildren<UIShiny>().Play();

                    shineFx = true;
                }                
            }            
        }
        else
        {
            enemies.text = "";
        }                
    }

    public void UpdateEnemiesGUI()
    {
        currentRoom = GameManager.instance.currentRoom;
        totRoomEnemies = currentRoom.enemies.Count;
        enemiesLeft = totRoomEnemies;
    }
}
