using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AbilityUI : MonoBehaviour
{
    public Image myImage;
    public Image alphaImage;
    public TextMeshProUGUI skillChargesText;


    int skillCharges;
    int lastFrameCharges;
    float skillCD;
    public bool coolDownStarted = false;

    PlayerController player;

    private void Awake()
    {
        player = GameManager.instance.player.GetComponent<PlayerController>();
        player.notifyAbilityUI += StartCoolDown;
    }

    private void OnDestroy()
    {
        player.notifyAbilityUI -= StartCoolDown;
    }

    void Start()
    {
        skillCharges = player.MaxCariche;
        lastFrameCharges = skillCharges;

        skillChargesText.text = skillCharges.ToString();

        if(skillCharges <2)
        {
            skillChargesText.enabled = false;
            skillChargesText.text = skillCharges.ToString();
        }


        foreach (Skill s in GameManager.instance.EquippedSkills)
        {
            if( s.type == Skill.Types.Active)
            {

                myImage.sprite = s.icon;
                break;
            }
        }
    }

    void Update()
    {

        if (skillChargesText.enabled == true)
        {
            CheckChargeChanges();
        }

        if(coolDownStarted)
        {
            alphaImage.fillAmount -=( 1 / skillCD )* Time.deltaTime;
            alphaImage.gameObject.SetActive(true);

            if (alphaImage.fillAmount <= 0)
            {
                coolDownStarted = false;
                alphaImage.gameObject.SetActive(false);
                alphaImage.fillAmount = 1;
            }
        }

    }


    //changes the text if it changes
    public void CheckChargeChanges()
    {
        skillCharges = player.CaricheCorrenti;
        if (skillCharges != lastFrameCharges)
        {
            skillChargesText.text = skillCharges.ToString();
            lastFrameCharges = skillCharges;
        }
    }

    public void StartCoolDown(float cd)
    {
        alphaImage.gameObject.SetActive(true);
        coolDownStarted = true;
        skillCD = cd;
    }
}
