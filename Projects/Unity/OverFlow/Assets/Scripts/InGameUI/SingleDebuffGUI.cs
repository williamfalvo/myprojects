﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SingleDebuffGUI : MonoBehaviour
{
    public float duration;
    public bool permanent;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(FrameWait());
        if (!permanent)
        {
            StartCoroutine(Duration(duration));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator FrameWait()
    {
        GetComponent<Image>().enabled = false;
        yield return new WaitForSeconds(1f);
        GetComponent<Image>().enabled = true;
    }

    IEnumerator Duration(float durationTime)
    {
        yield return new WaitForSeconds(durationTime);
        Destroy(gameObject);
    }
}
