﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScalingTextBehaviour : MonoBehaviour
{
    public Stat myStat;

    CharacterStatistics playerStat;

    [Space(10), Header("MATERIALS")]

    public Material valueIncreased;
    public Material valueBase;


    [Space(10), Header("TEXTS REF")]
    #region TextsRef

    public TextMeshProUGUI strTitle;
    public TextMeshProUGUI strBab;
    public TextMeshProUGUI strScaling;

    public TextMeshProUGUI dexTitle;
    public TextMeshProUGUI dexBab;
    public TextMeshProUGUI dexScaling;

    public TextMeshProUGUI techTitle;
    public TextMeshProUGUI techBab;

    public TextMeshProUGUI nHealthTitle;
    public TextMeshProUGUI nHealthHp;

    public TextMeshProUGUI nResTitle;
    public TextMeshProUGUI nResBab;

    public TextMeshProUGUI dropRateTitle;
    public TextMeshProUGUI dropRate;

    public TextMeshProUGUI critRateTitle;
    public TextMeshProUGUI critChance;


    #endregion

    private void OnEnable()
    {
        playerStat = GameManager.instance.player;
        InitStatisticsText();
    }

    // when a value gets changed the new stat gets calculated again
    public void UpdateMyText(Stat stat , int value)
    {
        float sumValues = 0; // container of the sommatory of the BAB

        Material materialToAssign;

        switch (stat)
        {
            
            case Stat.strength:
                
                //checks if the value has been incremented of not ,
                if(value == playerStat.baseStrength)
                {
                    materialToAssign = valueBase;
                }
                else
                {
                    materialToAssign = valueIncreased;
                }

                for (int i = 0; i < value; i++)
                {
                    sumValues += playerStat.agilityStrenghtValues[i].Bab;
                }


                strBab.text = sumValues.ToString();
                strBab.fontMaterial = materialToAssign;
                strScaling.text = playerStat.agilityStrenghtValues[value-1].scaling.ToString();
                strScaling.fontMaterial = materialToAssign;

                break;

            case Stat.dexterity:

                if (value == playerStat.baseAgility)
                {
                    materialToAssign = valueBase;
                }
                else
                {
                    materialToAssign = valueIncreased;
                }


                for (int i = 0; i < value; i++)
                {
                    sumValues += playerStat.agilityStrenghtValues[i].Bab;
                }

                dexBab.text = sumValues.ToString();
                dexBab.fontMaterial = materialToAssign;

                dexScaling.text = playerStat.agilityStrenghtValues[value - 1].scaling.ToString();
                dexScaling.fontMaterial = materialToAssign;
                break;

            case Stat.tech:

                if (value == playerStat.baseIntellect)
                {
                    materialToAssign = valueBase;
                }
                else
                {
                    materialToAssign = valueIncreased;
                }

                for (int i = 0; i < value; i++)
                {
                    sumValues += playerStat.techBabValues[i].Bab;
                }

                techBab.text = sumValues.ToString();
                techBab.fontMaterial = materialToAssign;

                break;

            case Stat.neuralHealth:

                if (value == playerStat.baseNeuralHealth)
                {
                    materialToAssign = valueBase;
                }
                else
                {
                    materialToAssign = valueIncreased;
                }

                nHealthHp.text = playerStat.hpValues[value-1].ToString();
                nHealthHp.fontMaterial = materialToAssign;
                break;

            case Stat.neuralResistance:

                if (value == playerStat.baseNeuralResistence)
                {
                    materialToAssign = valueBase;
                }
                else
                {
                    materialToAssign = valueIncreased;
                }


                for (int i = 0; i < value; i++)
                {
                    sumValues += playerStat.defenseValues[i];
                }

                nResBab.text = sumValues.ToString();
                nResBab.fontMaterial = materialToAssign;

                break;

            case Stat.luck:

                if (value == playerStat.baseLuck)
                {
                    materialToAssign = valueBase;
                }
                else
                {
                    materialToAssign = valueIncreased;
                }


                dropRate.text = (value * playerStat.luckIncreasePerLevel).ToString()  + "  %";
                dropRate.fontMaterial = materialToAssign;

                critChance.text = (value * playerStat.luckIncreasePerLevel * 2).ToString() + " %";
                critChance.fontMaterial = materialToAssign;

                break;
        }
    }

    //initialize all the textes with the base values of the player
    public void InitStatisticsText()
    {
        float sum = 0;
        for(int i= 0; i< (int)playerStat.baseStrength; i++)
        {
            sum += playerStat.agilityStrenghtValues[i].Bab;
        }
        //strBab.text = playerStat.agilityStrenghtValues[(int)playerStat.baseStrength -1 ].Bab.ToString();
        strBab.text = sum.ToString();
        strBab.fontMaterial = valueBase;

        strScaling.text = playerStat.agilityStrenghtValues[(int)playerStat.baseStrength -1 ].scaling.ToString();
        strScaling.fontMaterial = valueBase;

        sum = 0;

        for (int i = 0; i < (int)playerStat.baseAgility; i++)
        {
            sum += playerStat.agilityStrenghtValues[i].Bab;
        }
        //dexBab.text = playerStat.agilityStrenghtValues[(int)playerStat.baseAgility -1 ].Bab.ToString();
        dexBab.text = sum.ToString();
        dexBab.fontMaterial = valueBase;

        dexScaling.text = playerStat.agilityStrenghtValues[(int)playerStat.baseAgility -1 ].scaling.ToString();
        dexScaling.fontMaterial = valueBase;

        sum = 0;

        for (int i = 0; i < (int)playerStat.baseIntellect; i++)
        {
            sum += playerStat.techBabValues[i].Bab;
        }
        //techBab.text = playerStat.techBabValues[(int)playerStat.baseIntellect - 1 ].Bab.ToString();
        techBab.text = sum.ToString();
        techBab.fontMaterial = valueBase;


        nHealthHp.text = playerStat.hpValues[(int)playerStat.baseNeuralHealth -1 ].ToString();
        nHealthHp.fontMaterial = valueBase;

        sum = 0;
        for (int i = 0; i < (int)playerStat.baseNeuralResistence; i++)
        {
            sum += playerStat.defenseValues[i];
        }
        //nResBab.text = playerStat.defenseValues[(int)playerStat.baseNeuralResistence -1 ].ToString();
        nResBab.text = sum.ToString();
        nResBab.fontMaterial = valueBase;


        dropRate.text = (playerStat.baseLuck * playerStat.luckIncreasePerLevel).ToString() + " %";
        dropRate.fontMaterial = valueBase;

        critChance.text = (playerStat.baseLuck * playerStat.luckIncreasePerLevel * 2).ToString() + " %";
        critChance.fontMaterial = valueBase;

    }
}