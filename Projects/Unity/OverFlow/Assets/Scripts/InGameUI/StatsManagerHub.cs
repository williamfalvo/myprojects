﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public enum Stat {strength , dexterity , tech , neuralHealth , neuralResistance , luck }

public class StatsManagerHub : MonoBehaviour
{
    #region ModifierPerStat

    [HideInInspector]
    public int strengthModifier;
    [HideInInspector]
    public int dexterityModifier;
    [HideInInspector]
    public int techModifier;
    [HideInInspector]
    public int neuralHealthModifier;
    [HideInInspector]
    public int neuralResistanceModifier;
    [HideInInspector]
    public int luckModifier;

    #endregion

    [HideInInspector]
    private int pointsLeft;
    public TextMeshProUGUI pointsLeftText;

    public Material fullPointsMaterial;
    public Material losingPointsMaterial;

    public GameObject firstButton;




    CharacterStatistics playerStats;

    public int PointsLeft { get => pointsLeft; set => pointsLeft = value; }

    private void OnEnable()
    {
        StartCoroutine(setSelected());
        PointsLeft = GameManager.instance.player.attributePoints;
        pointsLeftText.text = "Points Left  :  " + PointsLeft.ToString();
        pointsLeftText.fontMaterial = fullPointsMaterial;
        InitializeValues();
    }

    // Start is called before the first frame update
    void Start()
    {
        playerStats = GameManager.instance.player;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //when disabled apply the changes to the player
    //private void OnDisable()
    //{
    //    applyModifies();
    //}

    //everytime it gets opened resets the stats that are going to be added to the player
    public void InitializeValues()
    {
        strengthModifier = 0;
        dexterityModifier = 0;
        techModifier = 0;
        neuralHealthModifier = 0;
        neuralResistanceModifier = 0;
        luckModifier = 0;
    }

    //adds the modifies to the player stats
    public void applyModifies()
    {
        playerStats.baseStrength += strengthModifier;
        playerStats.baseAgility += dexterityModifier;
        playerStats.baseIntellect += techModifier;
        playerStats.baseNeuralHealth += neuralHealthModifier;
        playerStats.baseNeuralResistence += neuralResistanceModifier;
        playerStats.baseLuck += luckModifier;

        playerStats.NeuralHealthModified();

        GameManager.instance.player.attributePoints = PointsLeft;
    }

    //updates the counter of the updated values;
    public void UpdateValues(Stat statType , int value)
    {
        switch (statType)
        {
            case Stat.strength:
                strengthModifier += value;
                break;
            case Stat.dexterity:
                dexterityModifier += value;
                break;
            case Stat.tech:
                techModifier += value;
                break;
            case Stat.neuralHealth:
                neuralHealthModifier += value;
                break;
            case Stat.neuralResistance:
                neuralResistanceModifier += value;
                break;
            case Stat.luck:
                luckModifier += value;
                break;
        }
    }

    //updates the count of the points left
    public void UpdatePointsLeft(Stat statType, int value)
    {
        if (PointsLeft - value <= GameManager.instance.player.attributePoints && PointsLeft - value >= 0)
        {
            PointsLeft -= value;
            pointsLeftText.text = "Points Left  :  " + PointsLeft.ToString();

            if (PointsLeft < GameManager.instance.player.attributePoints)
            {
                pointsLeftText.fontMaterial = losingPointsMaterial;
            }
            else
            {
                pointsLeftText.fontMaterial = fullPointsMaterial;
            }
        }
    }

    IEnumerator setSelected()
    {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(firstButton);
    }

}