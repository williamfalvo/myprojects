﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponsUI : MonoBehaviour
{
    public int index;
    public Sprite[] images;

    PlayerController player;
    Image icon;

    private void Awake()
    {
        player = GameManager.instance.playerController;
        player.OnWeaponChange += SetWeaponIcon;

        icon = GetComponent<Image>();
    }

    private void OnDestroy()
    {
        player.OnWeaponChange -= SetWeaponIcon;
    }

    public void SetWeaponIcon(int i)
    {
        if (i == index)
        {
            icon.sprite = images[1];
        }
        else
        {
            icon.sprite = images[0];
        }
    }
}
