﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndDrawPhaseChipset : MonoBehaviour
{
    public StatisticsManagerUI statisticsManager;

    MyButton button;

    bool firstHighlighted = false;
    bool lastFrameHighlighted;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<MyButton>();
    }

    // Update is called once per frame
    void Update()
    {
        if(button.Highlight())
        {
            if (lastFrameHighlighted)
            {
                firstHighlighted = false; ///////////
                //firstNotHighLighted = false;
            }
            else
            {
                firstHighlighted = true;
            }

            if (firstHighlighted)
            {
                statisticsManager.TestUIUpdate();
            }
        }

        lastFrameHighlighted = button.GetComponent<MyButton>().Highlight();
    }
}
