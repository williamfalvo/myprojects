﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class BuffDebuff
{

    public enum Stat { Strength, Intellect, Agility, NeuralResistance, Luck, Velocity, Leak, NeuralHealth }
    public Stat stat;

    public float statInstantMod; // instant modify to the value related
    public float ricorrente; // damages every proc
    public float time; //how much time pass between every proc
    public float totDuration; // total debuff duration
    public float modifier;

    public bool permanent; //if it is a permanent debuff

    public BuffDebuff(BuffDebuff buff)
    {
        stat = buff.stat;

        statInstantMod = buff.statInstantMod;
        ricorrente = buff.ricorrente;
        time = buff.time;
        totDuration = buff.totDuration;
        modifier = buff.modifier;
    }
}
