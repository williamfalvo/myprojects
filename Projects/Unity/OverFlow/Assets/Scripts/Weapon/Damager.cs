﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Damager : MonoBehaviour
{
    //calculates the damage in case the weapon is katana becouse it takes a BABScaling value
    public float CalculateDamage(float atk, float stat , CharacterStatistics.BabScaling[] statValues, float def, float res, float[] defValues, bool useScaling, float atkMod)
    {
        float bab = 0f;

        if (statValues.Length > 0)
        {
            for (int i = 0; i < stat; i++)
            {
                bab += statValues[i].Bab; // calculates all the multiplier till the stat value
            }
        }        

        atk += (stat / 100f) * bab; // calculates the atk based on Carullo's formula

        if (atk != 0f)
        {
            atk += ((atk * atkMod) / 100f);
        }

        float defBonus = 0f;

        if (defValues.Length > 0)
        {
            for (int j = 0; j < res; j++)
            {
                defBonus += defValues[j]; // calculates all the multiplier till the res value
            }
        }        

        def += (def / 100f) * defBonus; // calculates the def based on the Carullo's formula

        float damage = 0f;

        // checks the formula to apply
        if (atk >= def)
        {
            //damage = Mathf.RoundToInt((atk - 0.8f) * (def * Mathf.Pow(2.72f, (-0.3f * (def / atk)))));
            damage = Mathf.RoundToInt(((atk * 0.8f) + (atk/def)) * (atk/def));            
        }
        else
        {
            damage = Mathf.RoundToInt((0.5f * (Mathf.Pow(atk, 3) / Mathf.Pow(def, 2))) - (0.1f * (Mathf.Pow(atk, 2) / def)) + (0.1f * atk));
        }

        //checks if we have to use the scaling (last multiplier)
        //some abilities may disable it
        if (!useScaling)
        {
            damage = Mathf.RoundToInt(damage);
            return damage;
            
        }
        else
        {
            float scaling = statValues[(int)stat - 1].scaling;           

            damage += (damage / 100f) * scaling; //apply the scaling

            damage = Mathf.RoundToInt(damage);

            return damage;
        }        
    }

    //calculates the damae in case the weapon is hammer/spear becouse it takes a BABScaling value
    //same as above
    public float CalculateDamage(float atk, float stat, CharacterStatistics.LevelScaling[] statValues, float def, float res, float[] defValues, bool useScaling, float atkMod)
    {
        float defBonus = 0f;

        if (defValues.Length > 0)
        {
            for (int j = 0; j < res; j++)
            {
                defBonus += defValues[j];
            }
        }

        if (atk != 0f)
        {
            atk += ((atk * atkMod) / 100f);
        }

        def += (def / 100f) * defBonus;

        float damage = 0f;

        if (atk >= def)
        {
            damage = Mathf.RoundToInt(((atk * 0.8f) + (atk / def)) * (atk / def));
        }
        else
        {
            damage = Mathf.RoundToInt((0.5f * (Mathf.Pow(atk, 3) / Mathf.Pow(def, 2))) - (0.1f * (Mathf.Pow(atk, 2) / def)) + (0.1f * atk));
        }

        if (!useScaling)
        {
            damage = Mathf.RoundToInt(damage);
            return damage;
        }
        else
        {
            if (statValues.Length > 0)
            {                
                float scaling = statValues[(int)stat - 1].katanaScaling;

                damage += (damage / 100f) * scaling;
                damage = Mathf.RoundToInt(damage);
            }            

            return damage;
        }
    }
}
