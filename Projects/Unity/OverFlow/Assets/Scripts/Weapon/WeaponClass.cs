﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponClass : MonoBehaviour
{
    #region Variables

    public enum Type { Katana = 0, Hammer = 1, Spear = 2, Tech = 3, None = 100 } // tells which weapon it is

    public Type wpType;

    public float baseAtk;

    public CharacterStatistics ownerStats;

    public bool canDamage;
    public List<EnemyController> enemyHitted;

    float atk;

    public float Atk
    {
        get { return atk; }
        set { atk = value; }
    }

    float stat;

    public float Stat
    {
        get { return stat; }
    }

    CharacterStatistics.BabScaling[] babScalings;

    public CharacterStatistics.BabScaling[] BabScalings
    {
        get { return babScalings; }
    }

    CharacterStatistics.LevelScaling[] levelScalings;

    public CharacterStatistics.LevelScaling[] LevelScalings
    {
        get { return levelScalings; }
    }

    bool applyDebuff = false;

    public bool ApplyDebuff
    {
        get { return applyDebuff; }
        set { applyDebuff = value; }
    }

    float atkModPerc = 0f;
    
    public float AtkModPerc
    {
        get { return atkModPerc; }
        set { atkModPerc = value; }
    }

    #endregion

    void Awake()
    {
        if (atk == 0)
        {
            atk = baseAtk;
        }   
    }

    private void OnEnable()
    {
        canDamage = true;
    }

    private void OnDisable()
    {
        NextAttack();
    }

    private void Start()
    {
        WeaponTypeCases();
    }

    public void NextAttack()
    {
        foreach (var enemy in enemyHitted)
        {
            if (enemy != null)
            {
                enemy.wpCollider = null;
                enemy.weaponCollision = false;
            }
        }
        enemyHitted.Clear();        
    }

    public void WeaponTypeCases()
    {
        switch (wpType)
        {
            case Type.Katana:
                stat = ownerStats.level;// keeps the value to search in the array 
                levelScalings = ownerStats.levelValues; // the katana scales with level
                break;

            case Type.Hammer:
                stat = (int)Mathf.Clamp(ownerStats.baseStrength + ownerStats.bonusStrength, 0, 100);
                babScalings = ownerStats.agilityStrenghtValues;
                break;

            case Type.Spear:
                stat = (int)Mathf.Clamp(ownerStats.baseAgility + ownerStats.bonusAgility, 0, 100);
                babScalings = ownerStats.agilityStrenghtValues;
                break;

            case Type.Tech:
                stat = (int)Mathf.Clamp(ownerStats.baseIntellect + ownerStats.bonusIntellect, 0, 100);
                babScalings = ownerStats.techBabValues;
                break;
        }
    }
}
