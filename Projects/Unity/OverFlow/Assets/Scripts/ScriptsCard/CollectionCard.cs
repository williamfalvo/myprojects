﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionCard : MonoBehaviour
{
    MyButton button;
    bool isHighlighted;

    public Image image;
    public Image panelImage;

    private void Awake()
    {
        button = GetComponent<MyButton>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (button.Highlight() && !isHighlighted)
        {
            isHighlighted = true;

            panelImage.sprite = image.sprite;
        }
        else if (!button.Highlight() && isHighlighted)
        {
            isHighlighted = false;
        }
    }
}
