﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;


//review serve ??
public class HubDeckController : InventoryNavigationController
{
    public GameObject panel;
    public Text deckText;

    GridLayoutGroup myGrid;
    List<GameObject> objects = new List<GameObject>();
    Navigation customNavNone = new Navigation();
    GameManager gm;

    public HubInventoryController1 hubInventoryController;

    private void Awake()
    {
        myGrid = GetComponent<GridLayoutGroup>();        
    }

    private void OnEnable()
    {
        gm = GameManager.instance;

        if(gm != null)
        {
            gm.deck.OnCardAdded += CardIn;
        }       

        InitDeck();

        UpdateDeckUI();
    }

    private void OnDisable()
    {
        if(gm != null)
        {
            gm.deck.OnCardAdded -= CardIn;
        }        

        foreach (GameObject gos in objects)
        {
            Destroy(gos);
        }

        objects = new List<GameObject>();        
    }

    void Start()
    {
        InventoryPanelInfo1 panelInfo = panel.GetComponent<InventoryPanelInfo1>();
        panelInfo.Sub();

        //GameManager.instance.deck.OnCardAdded += CardIn;

        customNavNone.mode = Navigation.Mode.None;

        //InitDeck();

        //UpdateDeckUI();
    }

    public void CardIn(Card card)
    {
        GameObject g = Instantiate(cardSlot, transform);
        g.name = card.name;

        //Image img = g.transform.GetChild(1).GetComponent<Image>();
        //img.sprite = card.artwork;

        Button b = g.GetComponentInChildren<Button>();
        b.onClick.AddListener(() => SetCard(card));

        TextMeshProUGUI name = g.GetComponentInChildren<TextMeshProUGUI>();
        name.text = card.name;

        CardInfo2 info = g.GetComponent<CardInfo2>();
        info.Panel = panel;
        info.TakeInfo(card.cardDescription);

        objects.Add(g);

        UpdateDeckUI();

        SetNavigation(objects, myGrid);
    }

    public void SetCard(Card c)
    {
        //GameObject g = objects.Find(GameObject => GameObject.name == c.name);

        int index = gm.deck.cards.IndexOf(c);

        gm.deck.RemoveCard(c);
        gm.inventory.AddCard(c);

        GameObject g = objects[index];

        objects.Remove(g);        

        Destroy(g);

        UpdateDeckUI();

        //EventSystem.current.SetSelectedGameObject(saveDeckButton.gameObject);

        SetNavigation(objects, myGrid);

        if(objects.Count > 0)
        {
            if (index > objects.Count - 1)
            {
                objects[index - 1].GetComponentInChildren<MyButton>().Select();
            }
            else
            {
                objects[index].GetComponentInChildren<MyButton>().Select();
            }
        }
        else
        {
            if(otherPanel.transform.childCount > 0)
            {
                if(hubInventoryController.lastCard != null)
                {
                    EventSystem.current.SetSelectedGameObject(hubInventoryController.lastCard);
                }
                else
                {
                    otherPanel.transform.GetChild(0).GetComponentInChildren<MyButton>().Select();
                }
                
                hubInventoryController.OnInventory = true;
                hubInventoryController.lastDeck = null;
            }
        }
        
    }

    public void InitDeck()
    {
        foreach (Card c in gm.deck.cards)
        {
            CardIn(c);
        }
    }

    //public void EmptyObjectList()
    //{
    //    objects = new List<GameObject>();
    //}

    public void UpdateDeckUI()
    {
        deckText.text = "Deck(" + gm.deck.cards.Count.ToString() + "/" + gm.deck.maxCards.ToString() + ")";
    }
}
