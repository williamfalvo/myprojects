﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//review , serve ?
public class InventoryPanelInfo : MonoBehaviour
{
    public GameObject textField;

    List<GameObject> objects = new List<GameObject>();

    private void Start()
    {
        
    }

    public void Subscribe()
    {
    }    

    public void DisplayInfos(List<string> descriptions)
    {
        if (objects.Count > 0)
        {
            objects = new List<GameObject>();

            RectTransform[] allChildren = GetComponentsInChildren<RectTransform>();

            for (int i = 1; i < allChildren.Length; i++)
            {
                Destroy(allChildren[i].gameObject);
            }
        }

        for (int i = 0; i < descriptions.Count; i++)
        {
            if (transform != null)
            {
                GameObject go = Instantiate(textField, transform);

                objects.Add(go);
                Text t = go.GetComponent<Text>();
                t.text = "Effetto " + (i + 1).ToString() + " : " + descriptions[i].ToString();
            }
        }
    }

    private void OnDestroy()
    {
    }
}
