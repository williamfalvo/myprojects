﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CollectionPanel : MonoBehaviour
{
    List<Card> cards = new List<Card>();

    public GameObject card;
    public Image panelImage;
    public float maxCards;
    public TextMeshProUGUI completationText;

    private void OnEnable()
    {
        StartCoroutine(SetFirstButton());
    }
    
    public bool checkCollection()
    {
        List<Card> collection = GameManager.instance.collection.cards;
        cards = GameManager.instance.lastCollection.cards;

        bool newCards = false;

        if (cards != collection)
        {
            cards = collection;
            GameManager.instance.lastCollection.cards = collection;            
            newCards = true;
        }
        InitializePanel();

        return newCards;
    }

    
    void InitializePanel()
    {
        foreach(Card c in cards)
        {
            GameObject go = Instantiate(card, transform);

            go.transform.GetChild(1).GetComponent<Image>().sprite = c.artwork;
            go.GetComponentInChildren<CollectionCard>().panelImage = panelImage;
        }

        int rate = Mathf.RoundToInt((100f * cards.Count) / maxCards);
        completationText.text = "Completation Rate:\n" + rate.ToString() + "%";
    }

    IEnumerator SetFirstButton()
    {
        yield return new WaitForEndOfFrame();
        if (cards.Count > 0)
        {
            transform.GetChild(0).GetComponentInChildren<MyButton>().Select();
        }
    }
}
