﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardInfo1 : MonoBehaviour
{
    private MyButton button;

    private GameObject panel;

    public GameObject Panel
    {
        get { return panel; }
        set { panel = value; }
    }

    [HideInInspector]
    public bool openInfo;

    private List<string> descriptions = new List<string>();

    bool cardInfosUpdated;

    void Start()
    {
        button = GetComponentInChildren<MyButton>();
    }
    
    void Update()
    {
        if (button.Highlight() == true)
        {
            if (descriptions.Count > 0 && cardInfosUpdated)
            {
                //GameManager.instance.CardInfos(descriptions);
                cardInfosUpdated = false;
            }            

            if (Input.GetButtonDown("Cancel"))
            {
                openInfo = !openInfo;
                panel.SetActive(openInfo);
            }

            if (Input.GetButtonDown("Jump") && openInfo)
            {
                openInfo = false;
            }
        }
        else
        {
            cardInfosUpdated = true;
        }
    }

    public void TakeCard(Card c)
    {
        foreach (Trigger t in c.triggers)
        {
            descriptions.Add(t.description);
        }
    }
}
