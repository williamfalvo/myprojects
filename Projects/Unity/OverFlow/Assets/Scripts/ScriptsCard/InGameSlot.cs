﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameSlot : MonoBehaviour
{
    public HandController hand;
    public CardSwapSecurityCheck securityPopUp;
    public CardSwapSecurityCheck slotToOverridePopUp;
    public Card.Archetypes archetype; // the slots knows which archetype can handle
    public List<Card> cards;
    public List<Image> cardImages;
    public StatisticsManagerUI statisticsUI;
    public Sprite emptyGameSlotImage;

    public UIChargesChipset chargesManager;

    public delegate void  SlotInit(Card c , Card.Archetypes archetype , int index);
    public SlotInit thisSlotInit;
    public SlotInit thisSlotUpdated;

    GameObject oldSelected;

    public delegate void UpdateGUI();

    int a = 0;

    public bool inputListener;

    private void OnEnable()
    {
        if (thisSlotInit != null)
        {
            for (int i = 0; i < cardImages.Count; i++)
            {
                if (cardImages[i].sprite == emptyGameSlotImage)
                {
                    thisSlotUpdated(null, archetype, i);
                }
                else
                {
                    if (cardImages[0].sprite == emptyGameSlotImage)
                    {
                        thisSlotUpdated(cards[i - 1], archetype, i);
                    }
                    else
                    {
                        thisSlotUpdated(cards[i], archetype, i);
                    }                    
                }
            }
        }

    }

    private void Start()
    {
        cards = new List<Card>();        
   
        hand.thisCardSelected += CheckSlotStatus; // when a card is selected , every slot listen to it 
        GameManager.instance.inGameInventory.OnCardRemoved += UpdateBuildGUI;

        if (inputListener)
        {
            GameManager.instance.p.GetComponent<PlayerController>().Consumable += ActivateConsumable;
        }

        thisSlotInit += chargesManager.InitializeCharges;
        thisSlotUpdated += chargesManager.UpdateCharges;
    }

    private void Update()
    {
        if(Input.GetButtonDown("Back") && slotToOverridePopUp.gameObject.activeSelf)
        {
            StopAllCoroutines();
            slotToOverridePopUp.gameObject.SetActive(false);
        }
    }

    public void ActivateConsumable(int index)
    {

        if (cards.Count > 0)
        {           
            //we check has been used the consumable 1 or 2
            switch (index)
            {
                case (0):
                    GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.Consumable1, null);
                    break;

                case (1):
                    GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.Consumable2, null);
                    break;
            }

            //trigger that says that a generic consumable has been used
            GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.Consumable, null);            
        }
        else
        {
            FMODManager.instance.EmptyItem();
        }
    }

    private void OnDestroy()
    {
        if (hand != null)
        {
            hand.thisCardSelected -= CheckSlotStatus;
        }
        if (GameManager.instance != null)
        {
            if (GameManager.instance.p != null)
            {
                if (GameManager.instance.p != null)
                {
                    GameManager.instance.p.GetComponent<PlayerController>().Consumable -= ActivateConsumable;
                }
            }
        }
        if (chargesManager != null)
        {
            thisSlotInit -= chargesManager.InitializeCharges;
            thisSlotUpdated -= chargesManager.UpdateCharges;
        }
    }

    public void CheckSlotStatus(Card handCard)
    {
        // check if the card has the same archetype
        if ( handCard.cardArchetypes[0] == archetype)
        {
            // resources count is 2 due to the fact that the slots share the same list , equip count is 1 
            if ( ( cards.Count != 0 && cardImages.Count ==1 ) || ( cards.Count == 2 && cardImages.Count == 2 ) )
            {
                //ask if really want to change card , slots are all already taken
                securityPopUp.gameObject.SetActive(true);
                StartCoroutine(SecurityCheck(handCard));
            }
            else
            {
                if (cardImages[0].sprite == emptyGameSlotImage)
                {
                    cards.Insert(0, handCard);
                }
                else
                {
                    //adds the card to th slot
                    cards.Add(handCard);
                }                
                

                handCard.cardLife = handCard.triggers[0].life;

                ConsumableCheck(handCard);

                //update the gamemanager
                GameManager.instance.inGameInventory.AddCard(handCard);                

                //decide what slot's image has to change
                if (cards.Count == 1)
                {
                    ChangeImage(0);
                    thisSlotInit(handCard, archetype , 0);
                }
                else if (cards.Count == 2)
                {
                    if (cardImages[0].sprite == emptyGameSlotImage)
                    {
                        ChangeImage(0);
                        thisSlotInit(handCard, archetype , 0);
                    }
                    else
                    {
                        ChangeImage(1);
                        thisSlotInit(handCard, archetype , 1);
                    }                               
                }

                //removes the card from the hand
                GameManager.instance.hand.RemoveCard(handCard);
            }
        }        
    }

    //ask if we want to override a slot
    IEnumerator SecurityCheck(Card handCard)
    {
        yield return new WaitUntil( () => securityPopUp.done );

        if ( securityPopUp.decision )
        {
            if (handCard.cardArchetypes[0] != Card.Archetypes.Consumable) // if the card can be only 1 to be overrided
            {
                SendCardToGraveyard(cards[0]);
                GameManager.instance.hand.RemoveCard(handCard);
                cards.Add(handCard); // add the new one
                handCard.cardLife = handCard.triggers[0].life;
                ConsumableCheck(handCard);
                GameManager.instance.inGameInventory.AddCard(handCard);                
                ChangeImage(0);
                thisSlotInit(handCard, archetype, 0);
            }
            else
            {
                slotToOverridePopUp.gameObject.SetActive(true);
                StartCoroutine(SlotToOverride(handCard));
            }                     
        }

        securityPopUp.done = false;
    }

    //asks which slot we should override
    IEnumerator SlotToOverride(Card handCard)
    {
        yield return new WaitUntil(() => slotToOverridePopUp.done);
        SendCardToGraveyard(cards[slotToOverridePopUp.slot]);
        GameManager.instance.hand.RemoveCard(handCard);
        cards.Insert(slotToOverridePopUp.slot, handCard);
        handCard.cardLife = handCard.triggers[0].life;
        ConsumableCheck(handCard);
        GameManager.instance.inGameInventory.AddCard(cards[slotToOverridePopUp.slot]);
        ChangeImage(slotToOverridePopUp.slot);
        thisSlotInit(handCard, archetype, slotToOverridePopUp.slot);        

        slotToOverridePopUp.done = false;
    }

    // sends the card to the graveyard and discards its effect
    public void SendCardToGraveyard(Card c)
    {
        GameManager.instance.inGameInventory.RemoveCard(c);

        GameManager.instance.graveyard.AddCard(c);        
    }

    // update the GUI
    private void ChangeImage(int index)
    {
        cardImages[index].sprite = cards[index].artwork;
        cardImages[index].color = new Color(cardImages[index].color.r, cardImages[index].color.g, cardImages[index].color.b, 1);
        TriggerEquipped();
    }

    // activates the effect 
    public void TriggerEquipped()
    {
        GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.Equipped, null);
        a++;        
    }

    //removes the card from 
    public void UpdateBuildGUI(Card c)
    {
        Card tempCard;

        if (c.cardArchetypes[0] == archetype)
        {
            if (cards.Count > 0)
            {
                foreach (Card card in cards)
                {
                    if (card == c)
                    {
                        tempCard = c;
                        if (cards.Count == 1 && cardImages[cards.IndexOf(card)].sprite == emptyGameSlotImage)
                        {
                            cardImages[1].sprite = emptyGameSlotImage;
                            cardImages[1].color = new Color(1, 1, 1, 0);

                        }
                        else
                        {

                            cardImages[cards.IndexOf(card)].sprite = emptyGameSlotImage;
                            cardImages[cards.IndexOf(card)].color = new Color(1, 1, 1, 0);

                        }
                        thisSlotUpdated(null , archetype , cards.IndexOf(card));
                        break;
                    }
                }
            }

            cards.Remove(c); // we remove the card from the list of cards in the cardSlots // TEST

            foreach (Effect effect in c.triggers[0].effects)
            {
                if (c.cardArchetypes[0] != Card.Archetypes.Consumable)
                {
                    effect.Discard();
                }
            }
        }        
    }

    public void ConsumableCheck(Card c)
    {
        if (c.cardArchetypes[0] == Card.Archetypes.Consumable)
        {
            int ind = cards.IndexOf(c);

            if (ind == 0)
            {
                c.triggers[0].trigger = Trigger.Triggers.Consumable1;
            }
            else
            {
                c.triggers[0].trigger = Trigger.Triggers.Consumable2;
            }
        }
    }
}
