﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HubInventoryController1 : InventoryNavigationController
{
    public GameObject panel;
    public GameObject blackPanel;
    public FiltersController filtersController;
    public Text totCards;

    int maxDeckCards;
    List<GameObject> objects = new List<GameObject>();
    GridLayoutGroup myGrid;
    Dictionary<string, CardElements> s = new Dictionary<string, CardElements>();

    [HideInInspector]
    public GameManager gm;

    [HideInInspector]
    public GameObject lastCard;
    [HideInInspector]
    public GameObject lastDeck;

    bool onInventory;

    public bool OnInventory
    {
        get { return onInventory; }
        set { onInventory = value; }
    }
    
    private void OnEnable()
    {
        if(gm != null)
        {
            gm.OnCounterChange += UpdateCounter;
            gm.inventory.OnCardAdded += TotCards;
            gm.inventory.OnCardRemoved += TotCards;

            filtersController.OnFilterAdded += FilterOn;
            filtersController.OnFilterRemoved += FilterOff;
        }          

        Initialize();
    }

    private void OnDisable()
    {
        if(gm != null)
        {
            gm.OnCounterChange -= UpdateCounter;
            gm.inventory.OnCardAdded -= TotCards;
            gm.inventory.OnCardRemoved -= TotCards;

            filtersController.ResetFilters();

            filtersController.OnFilterAdded -= FilterOn;
            filtersController.OnFilterRemoved -= FilterOff;            

            firstAccess = true;
        }

        SaveDictionary();
    }    

    void Start()
    {
        if (panel != null)
        {
            InventoryPanelInfo1 panelInfo = panel.GetComponent<InventoryPanelInfo1>();
            panelInfo.Sub();
        }       

        maxDeckCards = gm.deck.maxCards;       
    }

    public void TotCards(Card c)
    {
        int tot = gm.inventory.cards.Count;

        totCards.text = "You have " + tot.ToString() + " cards in your collection";
    }

    public void UpdateCounter(string name, int counter)
    {
        if (s[name] != null)
        {
            s[name].counter = counter;
        }
    }

    private void Update()
    {
        if (onInventory && Input.GetButtonDown("XButton"))
        {            
            blackPanel.SetActive(!blackPanel.activeSelf);
            filtersController.gameObject.SetActive(!filtersController.gameObject.activeSelf);

            if (!filtersController.gameObject.activeSelf)
            {
                EventSystem.current.SetSelectedGameObject(transform.GetChild(0).GetChild(1).gameObject);
            }
        }
    }

    private void OnDestroy()
    {
        if(gm != null)
        {
            gm.OnCounterChange -= UpdateCounter;
        }
    }

    public void Initialize()
    {
        myGrid = GetComponent<GridLayoutGroup>();

        if (gm.Dict.Count > 0)
        {
            if (gm.temporary.cards.Count > 0)
            {
                foreach (Card c in gm.temporary.cards)
                {
                    Card tempCard = new Card();
                    bool matched = false;

                    //if the card is already present in the dictionary
                    foreach (KeyValuePair<string, CardElements> kvp in gm.Dict)
                    {
                        if (c.name == kvp.Key)
                        {
                            kvp.Value.counter++;
                            matched = true;
                            break;
                        }                        
                    }

                    //add the new pair to the dictionary
                    if (matched == false)
                    {
                        tempCard = c;

                        gm.Dict.Add(tempCard.name, new CardElements(1, tempCard.cardDescription, tempCard.artwork, tempCard.cardArchetypes.ToArray()));
                    }                    
                }

                foreach (Card c in gm.temporary.cards)
                {
                    gm.inventory.AddCard(c);
                }

                gm.temporary.cards = new List<Card>();

                CheckCollecton();
            }

            s = gm.Dict;
        }
        else
        {
            s = gm.inventory.cards.GroupBy(card => (card.name)).ToDictionary(c => c.Key, c => new CardElements(c.Count(), c.First().cardDescription, c.First().artwork, c.First().cardArchetypes.ToArray()));
        }

        totCards.text = "You have " + gm.inventory.cards.Count.ToString() + " cards in your collection";

        //setup every element of the dictionary in GUI
        foreach (KeyValuePair<string, CardElements> kvp in s)
        {
            CardInstantiate(kvp);
        }

        tempList = objects;
    }

    public void CardInstantiate(KeyValuePair<string, CardElements> kvp)
    {
        GameObject go = Instantiate(cardSlot, transform);
        go.name = kvp.Key;

        Text t = go.GetComponentInChildren<Text>();
        t.text = kvp.Value.counter.ToString();

        Image img = go.transform.GetChild(1).GetComponent<Image>();
        img.sprite = kvp.Value.artwork;

        Button b = go.GetComponentInChildren<Button>();
        b.onClick.AddListener(() => SetC(kvp.Key, kvp.Value.counter));

        CardInfo2 info = go.GetComponent<CardInfo2>();
        info.Panel = panel;
        info.TakeInfo(kvp.Value.txt);

        objects.Add(go);

        SetNavigation(objects, myGrid);
    }

    List<Filter> filters = new List<Filter>();
    List<GameObject> tempList = new List<GameObject>();
    bool firstAccess = true;

    public void FilterOn(Filter f)
    {
        if (firstAccess)
        {
            EmptyObjectsList();
        }       

        foreach (KeyValuePair<string, CardElements> kvp in s)
        {
            bool alreadyAdded = false;
            bool matched = false;

            foreach (Card.Archetypes a in kvp.Value.archetypes)
            {
                if (filters.Count > 0)
                {
                    foreach (Filter filt in filters)
                    {
                        if (a == filt.archetype)
                        {
                            alreadyAdded = true;
                        }
                    }
                }

                if (alreadyAdded == false)
                {
                    if (a == f.archetype)
                    {
                        matched = true;
                    }
                }
            }

            if (alreadyAdded == false && matched == true)
            {
                CardInstantiate(kvp);
            }
        }        

        filters.Add(f);

        firstAccess = false;
    }

    public void FilterOff(Filter f)
    {
        filters.Remove(f);

        foreach (KeyValuePair<string, CardElements> kvp in s)
        {
            bool matched = false;

            foreach (Card.Archetypes a in kvp.Value.archetypes)
            {
                if (a == f.archetype)
                {
                    matched = true;
                    break;
                }
            }

            if (matched)
            {
                GameObject g = objects.Find(GameObject => GameObject.name == kvp.Key);

                objects.Remove(g);

                Destroy(g);
            }
        }

        if (filters.Count == 0)
        {
            foreach (KeyValuePair<string, CardElements> kvp in s)
            {
                CardInstantiate(kvp);
            }

            firstAccess = true;
        }
    }

    public void SetC(string name, int count)
    {
        if (s[name].counter > 0 && gm.deck.cards.Count < maxDeckCards && maxDeckCards > 0)
        {
            Card c = gm.inventory.cards.Find(Card => Card.name == name);

            gm.deck.AddCard(c);
            gm.inventory.RemoveCard(c);

            SetNavigation(objects, myGrid);
        }
    }

    public class CardElements
    {
        public int counter;
        public string txt;
        public Sprite artwork;
        public Card.Archetypes[] archetypes;

        public CardElements(int c, string t, Sprite artw, Card.Archetypes[] arcs)
        {
            counter = c;
            txt = t;
            artwork = artw;
            archetypes = arcs;
        }

        public CardElements()
        {

        }
    }

    public void SaveDictionary()
    {
        gm.Dict = s;

        EmptyObjectsList();
    }    

    public void EmptyObjectsList()
    {
        foreach (GameObject g in objects)
        {
            Destroy(g);
        }

        objects = new List<GameObject>();
    }

    //review used ?
    public bool CompareDictionaries(Dictionary<string, CardElements> dict1, Dictionary<string, CardElements> dict2)
    {
        if (dict1.Count == dict2.Count)
        {
            foreach (var pair in dict1)
            {
                if (dict2.TryGetValue(pair.Key, out CardElements value))
                {
                    if (value.artwork != pair.Value.artwork || value.counter != pair.Value.counter || value.txt != pair.Value.txt)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    public void CheckCollecton()
    {
        foreach(Card c in gm.inventory.cards)
        {
            if (gm.collection.cards.Contains(c)) continue;

            gm.collection.cards.Add(c);
        }
    }
}
