﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryPanelInfo1 : MonoBehaviour
{
    public Text t;

    void Start()
    {
        
    }

    public void Sub()
    {
        GameManager.instance.OnCInfo += Display;
    }

    public void Display(string s)
    {
        t.text = s;
    }

    public void Unsub()
    {
        if (GameManager.instance != null)
        {
            GameManager.instance.OnCInfo -= Display;
        }
    }
}
