﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//used to show the panel next to the card with the correct info
public class CardInfo : MonoBehaviour
{ 
    public GameObject textField;
    public GameObject panel;
    public new Text name;

    private MyButton b;

    [HideInInspector]
    public bool openInfo;

    void Start()
    {
        b = GetComponentInChildren<MyButton>();
    }

    private void Update()
    {
        //when the card is highlighted the info panel gets activated/deactivated
        if (b.Highlight() == true && Input.GetButtonDown("Cancel"))
        {
            openInfo = !openInfo;
        }

        if (b.Highlight() == false)
        {
            openInfo = false;
        }
        
        if (openInfo)
        {
            panel.SetActive(true);
        }
        else
        {
            panel.SetActive(false);
        }
    }

    public void ShowCardInfo(Card card)
    {
        name.text = card.name;

        for (int i = 0; i < card.cardArchetypes.Count; i++)
        {
            GameObject g = Instantiate(textField, panel.transform);
            Text t = g.GetComponent<Text>();
            //gets the text from the card
            //t.text = card.cardArchetypes[i].ToString() + card.triggers[i].description;
        }
    }

    
}
