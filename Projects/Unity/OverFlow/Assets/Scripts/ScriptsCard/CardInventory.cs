﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// script managing all types of invenctory
public class CardInventory : MonoBehaviour
{
    //events that are called when the card is added or removed
    //every list calls those events whenever a card is added or removed , 
    //so the relatives GUI can listen to the event and gets updated
    public delegate void CardAction(Card c);
    public event CardAction OnCardAdded;
    public event CardAction OnCardRemoved;
    public event CardAction OnNpcRequest;
    public event CardAction OnNpcGift;

    // the list of cards that currently has
    public List<Card> cards;

    // the max number of cards it can have (optional)
    public int maxCards;



    //adds the card from the invenctory is attached to
    public void AddCard(Card card)
    {
        cards.Add(card);        

        if (OnCardAdded != null)
        {
            OnCardAdded(card);
        }

        //DataManager.instance.SaveData();
        //StartCoroutine(DataManager.instance.SavingFeedback());
    }

    //remove the card from the invenctory is attached to
    public void RemoveCard(Card card)
    {
        //if(cards.Contains(card))
        //{
        //    Debug.Log("Contains the card    " + card);
        //}

        cards.Remove(card);

        if (OnCardRemoved != null)
        {
            OnCardRemoved(card);
        }
    }

    //shuffle the list of cards
    public void Shuffle()
    {
        for (int i = cards.Count - 1; i > 0; i--)
        {
            int j = Random.Range(0, i);
            Card card = cards[j];
            cards[j] = cards[i];
            cards[i] = card;
        }
    }

    // when a NPC asks for a card 
    public void OnNpcRequestMethod(Card c)
    {
        if (OnNpcRequest != null)
        {
            OnNpcRequest(c);
        }
    }

    // when a NPC gives a card
    public void OnNpcGiftMethod(Card c)
    {
        if (OnNpcGift != null)
        {
            OnNpcGift(c);
        }
    }

}
