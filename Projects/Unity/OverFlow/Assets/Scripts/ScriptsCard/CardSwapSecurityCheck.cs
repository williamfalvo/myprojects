﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSwapSecurityCheck : MonoBehaviour
{
    [HideInInspector]
    public bool done;
    [HideInInspector]
    public bool decision;
    [HideInInspector]
    public int slot;

    //the relative button is clicked in order to decide if a card must be replaced and in which slot must be put in
    public void Decision(bool dec)
    {
        done = true;
        decision = dec;

        if (dec)
        {
            slot = 0;
        }
        else
        {
            slot = 1;
        }

        // when a decision is taken, the panel is turned off
        gameObject.SetActive(false);
    }
}
