﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardInfo2 : MonoBehaviour
{
    MyButton button;

    GameObject panel;

    public GameObject Panel
    {
        get { return panel; }
        set { panel = value; }
    }

    public Text counterText;

    bool openInfo = false;
    bool canDo;
    string description;
    
    void Start()
    {  
        GameManager.instance.inventory.OnCardAdded += CardIn;
        GameManager.instance.inventory.OnCardRemoved += CardOut;

        button = GetComponentInChildren<MyButton>();
    }
    
    public void CardIn(Card c)
    {
        if (c.name == gameObject.name && counterText != null)
        {
            int currentCount = int.Parse(counterText.text);
            currentCount++;
            counterText.text = currentCount.ToString();

            GameManager.instance.CounterChanged(c.name, currentCount);
        }
    }

    public void CardOut(Card c)
    {
        if (c.name == gameObject.name && counterText != null)
        {
            int currentCount = int.Parse(counterText.text);
            currentCount--;
            counterText.text = currentCount.ToString();

            GameManager.instance.CounterChanged(c.name, currentCount);
        }
    }

    void Update()
    {
        if (panel != null)
        {
            if (button.Highlight())
            {
                if (canDo)
                {
                    GameManager.instance.Infos(description);
                    canDo = false;
                }

                if (Input.GetButtonDown("Interact"))
                {
                    openInfo = !openInfo;
                    panel.SetActive(openInfo);
                }

                if (Input.GetButtonDown("Jump") && openInfo)
                {
                    openInfo = false;
                }
            }
            else
            {
                canDo = true;
            }
        }        
    }

    private void OnDestroy()
    {
        GameManager gm = GameManager.instance;
        if (gm != null)
        {
            gm.inventory.OnCardAdded -= CardIn;
            gm.inventory.OnCardRemoved -= CardOut;
        }

        if(panel != null)
        {
            InventoryPanelInfo1 p = panel.GetComponent<InventoryPanelInfo1>();

            if (p != null)
            {
                panel.GetComponent<InventoryPanelInfo1>().Unsub();
            }
        }        
    }

    public void TakeInfo(string d)
    {
        description = d;
    }
}
