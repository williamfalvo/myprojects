﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;


//review serve ?
public class HubInventoryController : InventoryNavigationController
{
    GridLayoutGroup myGrid;

    List<GameObject> objects = new List<GameObject>();    
    public float alpha = .5f;
    private int maxDeckCards;

    public GameObject panel;

    // unsubscribes from the hub inventory events
    private void OnDestroy()
    {
        GameManager.instance.inventory.OnNpcRequest -= RemoveNpcCards;
        GameManager.instance.inventory.OnNpcGift -= AddNpcCards;
    }

    private void OnEnable()
    {
        //EventSystem.current.firstSelectedGameObject = saveDeckButton.gameObject;
    }

    private void Start()
    {
        myGrid = GetComponent<GridLayoutGroup>();

        InventoryPanelInfo panelInfo = panel.GetComponent<InventoryPanelInfo>();
        panelInfo.Subscribe();

        // subscribes to the hub inventory scripts
        GameManager.instance.inventory.OnNpcRequest += RemoveNpcCards;
        GameManager.instance.inventory.OnNpcGift += AddNpcCards;

        InitInventory();  //instantiate all the gui slots for the invenctory section based on how many cards are in the inventory list

        // set the max number of cards the deck can have
        maxDeckCards = GameManager.instance.deck.maxCards;
    }

    public void SetCard(Card c, Image img)
    {
        GameObject g = objects.Find(GameObject => GameObject.name == c.name);
        panel.SetActive(false);

        //if has been already clicked it gets removed from the deck and become available again in the inventory
        if (img.color.a == alpha)
        {
            img.color = Color.white;
            GameManager.instance.deck.RemoveCard(c); //remove the card from the deck
            GameManager.instance.inventory.AddCard(c);
        }
        //set the card as clicked ( inserted in the deck )
        else
        {                
            DeckCardsNumberCheck(c, img);
        }
        
    }
    
    // removes a card based on the NPC request
    public void RemoveNpcCards(Card card)
    {
        GameObject g = objects.Find(GameObject => GameObject.name == card.name);
        GameManager.instance.inventory.RemoveCard(card);
        objects.Remove(g);
        Destroy(g);
        SetNavigation(objects, myGrid);
    }

    // add the card given from the NPC
    public void AddNpcCards(Card card)
    {
        GameManager.instance.inventory.AddCard(card);
        CardGUI(card);
        SetNavigation(objects, myGrid);
    }

    // diplays the hub inventory cards on GUI
    public void CardGUI(Card c)
    {
        GameObject go = Instantiate(cardSlot, transform);
        go.name = c.name;
        CardInfo1 info = go.GetComponent<CardInfo1>();
        info.Panel = panel;

        Image img = go.transform.GetChild(1).GetComponent<Image>();
        //img.sprite = c.artwork;
        Button b = go.GetComponentInChildren<Button>();
        b.onClick.AddListener(() => SetCard(c, img)); // when clicked, the button will call the SetCard funcion giving it its parameters        
        info.TakeCard(c);
        objects.Add(go);
        SetNavigation(objects, myGrid);
    }

    // initializes the hub inventory
    public void InitInventory()
    {
        foreach (Card c in GameManager.instance.inventory.cards)
        {
            CardGUI(c);
        }        
    }

    // adds the card to the deck if the limit has not been reached
    public void DeckCardsNumberCheck(Card c, Image img)
    {
        if (GameManager.instance.deck.cards.Count < maxDeckCards && maxDeckCards > 0)
        {
            img.color = new Color(1, 1, 1, alpha);
            GameManager.instance.deck.AddCard(c); //add the card to the deck   
            GameManager.instance.inventory.RemoveCard(c);            
        }
    }
}
