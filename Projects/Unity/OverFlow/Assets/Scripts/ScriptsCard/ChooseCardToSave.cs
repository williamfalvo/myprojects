﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using FMODUnity;

public class ChooseCardToSave : MonoBehaviour
{
    public GameObject tutorialText;

    public GameObject button1;
    public GameObject button2;


    
    [HideInInspector]
    public int num;

    [HideInInspector]
    public bool done;

    FMOD.Studio.Bus sfxBusToMute;
    

    // the buttons display the cards to sacrifice
    public void DisplayCards(Card c1, Card c2)
    {
        Image card1 = button1.transform.GetChild(1).GetComponent<Image>();
        card1.sprite = c1.artwork;
        Coffee.UIExtensions.UIDissolve dissolve1 = card1.gameObject.GetComponent<Coffee.UIExtensions.UIDissolve>();
        dissolve1.enabled = false;
        dissolve1.play = true;

        Image card2 = button2.transform.GetChild(1).GetComponent<Image>();
        card2.sprite = c2.artwork;
        Coffee.UIExtensions.UIDissolve dissolve2 = card2.gameObject.GetComponent<Coffee.UIExtensions.UIDissolve>();
        dissolve2.enabled = false;
        dissolve2.play = true;
    }

    // the relative button is clicked in order to decide which one must be eliminated
    public void Choose(int index)
    {
        if(index == 0)
        {
            num = 1;
            button2.transform.GetChild(0).GetComponent<MyButton>().interactable = false;
        }
        else
        {
            num = 0;
            button1.transform.GetChild(0).GetComponent<MyButton>().interactable = false;

        }

        done = true;

        // when a decision is taken, the panel is turned off
        //gameObject.SetActive(false);
    }

    private void Start()
    {
        FMODManager.instance.StopChunkMusic();
        FMODManager.instance.StopRainMusic();
    }

    private void OnEnable()
    {
        sfxBusToMute = RuntimeManager.GetBus("bus:/SFX");
        sfxBusToMute.setVolume(0f);

        StartCoroutine(waitForFrame());
    }

    IEnumerator waitForFrame()
    {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(button1.transform.GetChild(0).gameObject);
    }

    private void Awake()
    {
        if (GameManager.instance.firstTimeDeath == false)
        {
            tutorialText.SetActive(false);
        }
        else
        {
            GameManager.instance.firstTimeDeath = false;
        }
    }


}
