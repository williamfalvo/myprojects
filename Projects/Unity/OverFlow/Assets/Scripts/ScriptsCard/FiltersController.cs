﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Filter
{
    public MyButton button;
    public Card.Archetypes archetype;
    public bool added;
    public Image border;

    public Filter(bool b, MyButton bt, Card.Archetypes a, Image img)
    {        
        added = b;
        button = bt;
        archetype = a;
        border = img;
    }
}

[System.Serializable]
public class FiltersController : MonoBehaviour
{
    public delegate void FilterAction(Filter f);
    public event FilterAction OnFilterAdded;
    public event FilterAction OnFilterRemoved;

    public List<Filter> filters = new List<Filter>();
    List<Filter> activeFilters = new List<Filter>();

    private void OnEnable()
    {
        StartCoroutine(Enable());
    }

    void Start()
    {
        InitFilterPanel();        
    }

    public void InitFilterPanel()
    {
        foreach (Filter f in filters)
        {
            f.button.onClick.AddListener(() => SetFilter(f));
        }
    }

    public void SetFilter(Filter filt)
    {
        if (filt.added)
        {
            filt.added = false;

            filt.border.color = Color.white;
            RemoveFilter(filt);            
        }
        else
        {
            filt.added = true;

            filt.border.color = Color.green;
            AddFilter(filt);            
        }
    }

    public void AddFilter(Filter f)
    {
        activeFilters.Add(f);

        OnFilterAdded?.Invoke(f);
    }

    public void RemoveFilter(Filter f)
    {
        activeFilters.Remove(f);

        OnFilterRemoved?.Invoke(f);
    }

    public void ResetFilters()
    {
        float filtersCount = activeFilters.Count;

        for (int i = 0; i < filtersCount; i++)
        {
            activeFilters[0].added = false;

            activeFilters[0].border.color = Color.white;

            RemoveFilter(activeFilters[0]);
        }

        //foreach(Filter f in activeFilters)
        //{
        //    f.added = false;

        //    f.border.color = Color.white;

        //    RemoveFilter(activeFilters[0]);
        //}
    }

    IEnumerator Enable()
    {
        yield return new WaitForEndOfFrame();

        filters[0].button.Select();
    }
}
