﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChooseCardType : MonoBehaviour
{
    public List<GameObject> buttons;

    Card cardTo;
    Navigation archetypeNavigation;

    [HideInInspector]
    public bool done = false;

    //shows in GUI to choose between the 2 archetypes tha card can have
    public void DisplayArchetype(Card c)
    {
        for (int i = 0; i < c.cardArchetypes.Count; i++ )
        {
            buttons[i].GetComponentInChildren<Text>().text = c.cardArchetypes[i].ToString();
        }
        EventSystem.current.SetSelectedGameObject(buttons[0]);

        cardTo = c;        
    }

    //sets the chosen archetype as the first of the list of archetypes of the card and makes the same for its effects
    public void ChooseArchetype(int archetypeIndex)
    {
        if (archetypeIndex != 0)
        {
            cardTo.cardArchetypes.Reverse();

            cardTo.triggers.Reverse();
        }
        else
        {

        }

        done = true;
        gameObject.SetActive(false);
    }


}
