﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GridLayoutGroup))]
public class DeckController : InventoryNavigationController
{
    GridLayoutGroup myGrid;    
    Navigation customNavNone = new Navigation();
    
    List<GameObject> gameObjects = new List<GameObject>();

    private void Awake()
    {
        myGrid = GetComponent<GridLayoutGroup>();
    }

    // subscribes to deck events
    void Start()
    {        
        GameManager.instance.deck.OnCardAdded += CardIn;
        GameManager.instance.deck.OnCardRemoved += CardOut;
        customNavNone.mode = Navigation.Mode.None;
        GameManager.instance.deck.OnNpcRequest += CardOut;

        // initialize deck
        InitDeck();
    }

    // unsubscribes from deck events
    private void OnDestroy()
    {
        GameManager.instance.deck.OnCardAdded -= CardIn;
        GameManager.instance.deck.OnCardRemoved -= CardOut;
        GameManager.instance.deck.OnNpcRequest -= CardOut;
    }

    //instantiated the slot on the deck gui 
    public void CardIn(Card card)
    {
        GameObject g = Instantiate(cardSlot, transform);
        g.name = card.name;     
        gameObjects.Add(g);
        Image img = g.transform.GetChild(1).GetComponent<Image>();
        img.sprite = card.artwork;
        SetNavigation(gameObjects, myGrid);
    }

    // when a card is removed from the deck, the relative object is removed from GUI
    public void CardOut(Card card)
    {
        GameObject g = gameObjects.Find(GameObject => GameObject.name == card.name);
        gameObjects.Remove(g);
        Destroy(g);
    }

    // empties the list
    public void EmptyList()
    {
        gameObjects = new List<GameObject>();
    }
    
    // initialize the deck
    public void InitDeck()
    {
        foreach (Card c in GameManager.instance.deck.cards)
        {
            CardIn(c);
        }
    }
}
