﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddListenerSaveData : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(SaveTheData);
    }

    void SaveTheData()
    {
        DataManager.instance.SaveData();
        //StartCoroutine(DataManager.instance.SavingFeedback());
    }
}
