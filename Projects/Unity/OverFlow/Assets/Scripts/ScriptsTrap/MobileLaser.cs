﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileLaser : Trap
{
    LineRenderer lr;
    GameObject laser;
    Vector3 finalPosition;
    float dist;

    public bool isIntermittent;
    public float time;
    public float damage;
    public Vector3 laserDistance;
    float maxDistance;
    public Vector3 moveDistance;
    Vector3 tempPos;
    Rigidbody rb;
    bool canDamage = true;

    public GameObject laserFx;

    public GameObject model;
    public Material[] materials;

    private void Start()
    {
        //lr = GetComponent<LineRenderer>();
        laser = transform.GetChild(0).gameObject;
        
        for (int i = 0; i < 3; i++)
        {
            if (laserDistance[i] != 0)
            {
                dist = Mathf.Abs(laserDistance[i]);
            }
        }

        //If laser is intermittent, start a coroutine that manage enable/disable state
        if (isIntermittent)
        {
            StartCoroutine(ActivatorLaser());
        }

        //Start moving position
        tempPos = transform.position;

        //Set laser max distance to check raycast collision 
        if(Mathf.Abs(laserDistance.x) > Mathf.Abs(laserDistance.y))
        {
            maxDistance = Mathf.Abs(laserDistance.x);
        }
        else
        {
            maxDistance = Mathf.Abs(laserDistance.y);
        }

        //Set initial movement
        rb = GetComponent<Rigidbody>();
        rb.velocity = moveDistance.normalized;
    }

    // Update is called once per frame
    protected void Update()
    {
        if (!blackout && !canActivate)
        {
            StopAllCoroutines();
            blackout = true;
            rb.velocity = Vector3.zero;
            //lr.enabled = false;

            model.GetComponent<MeshRenderer>().material = materials[1];

            laser.SetActive(false);
            StartCoroutine(BlackOut());
        }

        if (laser.activeSelf)
        {
            //Set laser's initial and final position
            SetLaserPositions();

            //Check if laser is touching player
            int layerMask = (1<<11);
            RaycastHit hit;
            if (Time.timeScale != 0 && canDamage && Physics.Raycast(transform.position, laserDistance, out hit, maxDistance, layerMask))
            {
                PlayerController p = hit.collider.GetComponent<PlayerController>();
                if (!p.SecLifeBar)
                {
                    p.TakeDamage(damage, true);
                    StartCoroutine(ResetDamage());
                }                
            }
        }

        //Check if reach the final position and inverts it
        if(canActivate && (transform.position - (tempPos + moveDistance)).sqrMagnitude <= 0.1f * 0.1f)
        {
            tempPos = tempPos + moveDistance;
            moveDistance *= -1;
            rb.velocity = moveDistance.normalized;
        }
    }

    protected virtual IEnumerator ActivatorLaser()
    {
        if (laser.activeSelf == false)
        {
            //Spawn particellare 
            GameObject ls = Instantiate(laserFx, transform.position, Quaternion.identity);
            ls.transform.parent = transform;
            float vfxTime = ls.GetComponent<ParticleSystem>().main.duration - 1f;
            Destroy(ls, 3f);

            //Yield return tempo particellare
            yield return new WaitForSeconds(vfxTime);
        }
        else
        {
            //yield return tempo attivazione
            yield return new WaitForSeconds(time);
        }

        laser.SetActive(!laser.activeSelf);

        //lr.enabled = !lr.enabled;

        //if (lr.enabled)
        //{
        //    SetLaserPositions();
        //}

        StartCoroutine(ActivatorLaser());
    }

    void SetLaserPositions()
    {
        //lr.SetPosition(0, transform.position);
        //finalPosition = transform.position + laserDistance;
        //lr.SetPosition(1, finalPosition);

        laser.transform.localScale = new Vector3(laser.transform.localScale.x, laser.transform.localScale.y, dist * 10f);
    }

    IEnumerator BlackOut()
    {
        yield return new WaitUntil(() => canActivate);

        blackout = false;

        model.GetComponent<MeshRenderer>().material = materials[0];

        if (isIntermittent)
        {
            StartCoroutine(ActivatorLaser());
        }
        else
        {
            laser.SetActive(true);
        }        
    }

    IEnumerator ResetDamage()
    {
        canDamage = false;

        yield return new WaitForSeconds(1f);

        canDamage = true;
    }
}
