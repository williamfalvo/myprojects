﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class MapController : MonoBehaviour
{
    public GameObject mapCamera;
    public DungeonCreation dungeon;
    public float cameraSpeed;

    bool rightStickDown;
    bool right;
    bool left;
    bool up;
    bool down;
    bool mapOn = false;

    float startPosX;
    float startPosY;

    InputManager inputManager;
    Image mapBackground;

    Vector3 lastPos;
    Vector3 lowestPos;

    float posY;
    float offsetY;

    private void Awake()
    {
        startPosX = mapCamera.transform.position.x;
        startPosY = mapCamera.transform.position.y;
        lastPos = GameManager.instance.player.transform.position;
    }

    void Start()
    {        
        inputManager = GameManager.instance.inputManager;
        mapBackground = GetComponent<Image>();

        var rs = dungeon.currentDungeonRooms.GetRange(1, dungeon.currentDungeonRooms.Count() - 2);

        var rooms = rs.OrderByDescending( room => room.transform.position.y).ToList();

        if (rooms[0].transform.position.y >= startPosY)
        {
            posY = rooms[0].transform.position.y;
            offsetY = rooms[0].GetComponent<BoxCollider>().size.y * 5f;
        }
        else
        {
            posY = startPosY;
            offsetY = 10f;
        }

        var r = dungeon.currentDungeonRooms.OrderByDescending(room => room.transform.position.y).ToList();

        lowestPos = r[r.Count() - 1].transform.position;
    }

    void Update()
    {
        //checks the commands
        rightStickDown = (inputManager.rightStick);
        left = (inputManager.righStickX < -.2f) ? true : false;
        right = (inputManager.righStickX > .2f) ? true : false;
        up = (inputManager.rightStickY > .2f) ? true : false;
        down = (inputManager.rightStickY < -.2f) ? true : false;

        //activates the camera if the stick has been clicked
        if (rightStickDown)
        {            
            transform.GetChild(0).gameObject.SetActive(!transform.GetChild(0).gameObject.activeSelf);
            mapBackground.enabled = !mapBackground.enabled;
            mapOn = !mapOn;
        }  
    }    

    private void FixedUpdate()
    {
        //if the map is open we can navigate it
        if (mapOn)
        {
            if (GameManager.instance.player.transform.position != lastPos)
            {
                mapCamera.transform.position = new Vector3(GameManager.instance.player.transform.position.x, GameManager.instance.player.transform.position.y, mapCamera.transform.position.z);
                lastPos = GameManager.instance.player.transform.position;
            }

            if (left && mapCamera.transform.position.x > startPosX)
            {
                mapCamera.transform.position -= new Vector3(cameraSpeed, 0f, 0f);
            }

            if (right && mapCamera.transform.position.x < (dungeon.currentDungeonRooms[dungeon.currentDungeonRooms.Count -1].gameObject.transform.position.x + 20f))
            {
                mapCamera.transform.position += new Vector3(cameraSpeed, 0f, 0f);
            }

            if (up && mapCamera.transform.position.y < startPosY + posY + offsetY)
            {
                mapCamera.transform.position += new Vector3(0f, cameraSpeed, 0f);
            }            

            if (down && mapCamera.transform.position.y > lowestPos.y - 5f)  /*((Mathf.Abs(lowestPos.y) + 5f) - Mathf.Abs(mapCamera.transform.position.y))*/
            {                
                mapCamera.transform.position -= new Vector3(0f, cameraSpeed, 0f);
            }            
        }
    }
}
 