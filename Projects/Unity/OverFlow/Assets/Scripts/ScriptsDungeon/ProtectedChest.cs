﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ProtectedChest : Chest
{
    public List<Card> request;

    [Space(3), Header("References Script")]
    public GameObject requestPanel;
    public Image cardRequestImage;
    public TextMeshProUGUI cardRequestText;
    //public Image cardDropImage;
    //public TextMeshProUGUI cardDropText;
    public Button requestButton;
    public GameObject changePanel;
    public Image loadingBar;
    public int loadingBarSpeed;
    public Image cardLeft;
    public Image cardLeftWarning;
    public Image cardRight;


    GameManager gameManager;

    bool requestPassed;
    bool isInside;
    float currentLoading;

    //Deck reference
    CardInventory deck;
    List<Card> removedDeck = new List<Card>();

    //Hand reference
    CardInventory hand;
    List<Card> removedHand = new List<Card>();

    //Chipset reference
    CardInventory chipset;
    List<Card> removedChipset = new List<Card>();


    protected override void Start()
    {
        base.Start();

        gameManager = GameManager.instance;

        //Set references necessary to check request
        deck = gameManager.deck;
        hand = gameManager.hand;
        chipset = gameManager.inGameInventory;

        //Set card request text looking request size list
        if (request.Count > 0)
        {
            cardRequestImage.sprite = request[0].artwork;
            cardRequestText.text = ("x" + request.Count + " " + request[0].name);
            cardRight.sprite = request[0].artwork;
        }
        
        if(dropList.Count > 0)
        {
            //cardDropImage.sprite = dropList[0].artwork;
            //cardDropText.text = ("x" + dropList.Count + " " + dropList[0].name);
            cardLeft.sprite = dropList[0].artwork;
        }
        
    }

    private void Update()
    {
        //fill bar
        loadingBar.fillAmount = 0;

        //if submit is pressed the fill circle starts charging
        if (changePanel.activeSelf)
        {
            if(Input.GetButton("Submit"))
            {
                currentLoading += loadingBarSpeed;
                loadingBar.fillAmount = currentLoading / 100;

                //if fully charge starts the exchange
                if ((int)currentLoading >= 100)
                {
                    //make the exchange
                    ChangeCard();
                }
            }
            else
            {
                currentLoading = 0f;
            }            
        }

        if (chestState == ChestState.Closed && isInside && Input.GetButtonDown("Interact"))
        {
            if (!requestPanel.activeSelf)
            {
                gameManager.pausable = false;
                gameManager.inputManager.enabled = false;
                requestPanel.SetActive(true);
                requestButton.Select();

                requestPassed = CheckRequest();
            }
        }

        if (requestPanel.activeSelf && Input.GetButtonDown("Cancel"))
        {
            gameManager.SetPause();
            gameManager.inputManager.enabled = true;
            requestPanel.SetActive(false);
            changePanel.SetActive(false);
            currentLoading = 0f;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInside = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isInside = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        
    }

    public bool CheckRequest()
    {
        List<Card> tempRequest = new List<Card>(request);

        //Search into Deck
        foreach(Card card in deck.cards)
        {
            foreach(Card requestCard in tempRequest)
            {
                if(requestCard.Equals(card))
                {
                    removedDeck.Add(card);
                    tempRequest.Remove(requestCard);
                    break;
                }
            }

            if (tempRequest.Count == 0)
            {
                return true;
            }
        }

        //Search into Hand
        foreach(Card card in hand.cards)
        {
            foreach(Card requestCard in tempRequest)
            {
                if(requestCard.Equals(card))
                {
                    removedHand.Add(card);
                    tempRequest.Remove(requestCard);
                    break;
                }
            }

            if (tempRequest.Count == 0)
            {
                return true;
            }
        }

        //Search into Chipset
        foreach (Card card in chipset.cards)
        {
            foreach (Card requestCard in tempRequest)
            {
                if (requestCard.Equals(card))
                {
                    removedChipset.Add(card);
                    tempRequest.Remove(requestCard);
                    break;
                }
            }
            
            if (tempRequest.Count == 0)
            {
                return true;
            }
        }

        return false;
    }

    public void OpenChangePanel()
    {
        if (requestPassed)
        {
            changePanel.SetActive(true);
            EventSystem.current.SetSelectedGameObject(null);
            cardLeftWarning.enabled = removedChipset.Count > 0;
        }
    }

    public void ChangeCard()
    {
        if (requestPassed && chestState == ChestState.Closed)
        {
            foreach(Card c in removedDeck)
            {
                deck.RemoveCard(c);
            }

            foreach(Card c in removedHand)
            {
                hand.RemoveCard(c);
            }

            foreach(Card c in removedChipset)
            {
                chipset.RemoveCard(c);
            }

            foreach(Card c in dropList)
            {
                GameObject drop = Instantiate(dropPrefab, transform.position, Quaternion.identity);
                DropBehaviour dropBeh = drop.GetComponent<DropBehaviour>();
                dropBeh.drop = c;
            }

            gameManager.inputManager.enabled = true;
            gameManager.SetPause();
            changePanel.SetActive(false);
            requestPanel.SetActive(false);
            chestState = ChestState.Open;
        }
    }
}
