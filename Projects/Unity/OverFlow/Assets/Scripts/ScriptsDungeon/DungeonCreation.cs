﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonCreation : MonoBehaviour
{
    public GameObject player;

    int plotIndex;
    public int maxRooms;
    public RoomBehaviour startRoom;
    public RoomBehaviour finishRoom;
    public RoomBehaviour plotRoom;
    [HideInInspector]
    public List<RoomBehaviour> rooms;
    public bool isCompleted;
    public bool isPlotInstantiated;
    public bool isPlotFinal;
    public GameObject tutorialPanel;

    //list of the current rooms in the dungeon
    public List<RoomBehaviour> currentDungeonRooms = new List<RoomBehaviour>();

    public delegate void NotifyGUI();
    public event NotifyGUI onRoomCleared;

    void Awake()
    {
        Plot plot = GameManager.instance.CurrentMission;
        Mission mission = plot.Mission;
        mission.setCardsList();

        plotIndex = mission.missionIndex;
        isCompleted = plot.MissionReport;
        startRoom = mission.startRoom;
        finishRoom = mission.finishRoom;
        plotRoom = mission.plotRoom;

        //Initialize local room list
        foreach(RoomBehaviour room in mission.pool.rooms)
        {
            rooms.Add(room);
        }

        if(mission.minRooms > rooms.Count - 1)
        {
            maxRooms = Random.Range(rooms.Count-1, rooms.Count);
        }
        else
        {
            maxRooms = mission.minRooms;
        }
        
        isPlotFinal = plotRoom.isFinal;
        RightCreation();

        //register to every room " on room clear " event
        foreach(RoomBehaviour room in currentDungeonRooms)
        {
            room.enableGUI += RoomClearedAlertGUI;
        }

        GameManager.instance.DungeonStarted();
    }

    private void Start()
    {
          
    }

    //Generate the dungeon from left to right
    void RightCreation()
    {
        //Change the random seed
        Random.InitState(System.DateTime.Now.Second);

        //Create the start room and set the next room that continue the dungeon generation
        RoomBehaviour start = Instantiate(startRoom);
        currentDungeonRooms.Add(start);

        int index = Random.Range(0, rooms.Count);
        RoomBehaviour nextRoom = start.GenerateRoom(rooms[index]);
        rooms.RemoveAt(index);
        currentDungeonRooms.Add(nextRoom);

        //Generate the rooms and a plot room into the dungeon
        for (int i = 0; i < maxRooms -1; ++i)
        {
            //if not exist a plot room and this is the room next to finish room, generate it
            if(!isCompleted && !isPlotFinal && i == maxRooms - 2 && !isPlotInstantiated)
            {
                nextRoom = nextRoom.GenerateRoom(plotRoom);
                currentDungeonRooms.Add(nextRoom);

                isPlotInstantiated = true;
                break;
            }

            //Generate random if the next room is a plot or generic room
            int rand = Random.Range(0, 100);
            if(!isCompleted && !isPlotFinal && rand < 100/maxRooms && !isPlotInstantiated)
            {
                nextRoom = nextRoom.GenerateRoom(plotRoom);
                isPlotInstantiated = true;
            }
            else
            {
                index = Random.Range(0, rooms.Count);
                nextRoom = nextRoom.GenerateRoom(rooms[index]);
                rooms.RemoveAt(index);

            }
            currentDungeonRooms.Add(nextRoom);
        }

        //Generate the finish room
        if (!isCompleted && isPlotFinal)
        {
            currentDungeonRooms.Add(nextRoom.GenerateRoom(plotRoom));  
        }
        else
        {
            currentDungeonRooms.Add(nextRoom.GenerateRoom(finishRoom));
        }


        //Instantiate the player on entry door position of start room
        GameManager.instance.playerController.EnterDungeon(startRoom.entryDoor.transform.position);
        if(currentDungeonRooms.Count == 3 )
        {
            tutorialPanel.SetActive(true);
        }
        
    }

    //function to notify the GUI when a room gets cleared
    private void RoomClearedAlertGUI()
    {
        GameManager.instance.inputManager.enabled = false;
        GameManager.instance.DecreaseCardsLife(Card.Archetypes.Consumable);
        onRoomCleared?.Invoke();
    }    
}
