﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionsManager : MonoBehaviour
{
    public Button[] missions;

    void Start()
    {
        Plot[] plots = GameManager.instance.missions;

        for (int i = 1; i < plots.Length; i++)
        {
            missions[i].GetComponent<ServerButton>().isInteractable = plots[i - 1].MissionReport; //checks wich missions are unlocked
        }
    }

    public void SetMission(int mission)
    {
        GameManager.instance.SetMission(mission);
    }
}
