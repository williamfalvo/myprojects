﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public struct Door
{
    public GameObject door;
    public Material defaultMat;
    public Material dissolveMat;    
}

public class RoomBehaviour : MonoBehaviour
{
    public Transform joint;
    public GameObject exitDoor;
    public GameObject entryDoor;
    public bool isFinal;

    public Door[] doors;

    [HideInInspector]
    public bool dissolveFxEnded = false;

    public List<Enemy> enemies;

    public delegate void RoomCleared();
    public event RoomCleared currentRoomCleared;

    public delegate void EnableGUI();
    public event EnableGUI enableGUI;

    [HideInInspector]
    public bool chunkStarted = false;

    [HideInInspector]
    public bool chunkCompleted = false;


    public IEnumerator MaterializeDoors()
    {
        WaitForEndOfFrame wf = new WaitForEndOfFrame();

        foreach (Door d in doors)
        {
            d.door.GetComponentInChildren<MeshRenderer>().material = d.dissolveMat;
            d.door.SetActive(true);
        }

        MaterialPropertyBlock mpb = new MaterialPropertyBlock();

        float dissolve = 1f;

        while (dissolve > 0f)
        {
            mpb.SetFloat("_Fill", dissolve);
            dissolve -= Time.deltaTime;

            foreach (Door d in doors)
            {
                d.door.GetComponentInChildren<MeshRenderer>().SetPropertyBlock(mpb);
            }

            yield return wf;
        }

        foreach (Door d in doors)
        {
            d.door.GetComponentInChildren<MeshRenderer>().material = d.defaultMat;
        }
    }

    public IEnumerator DissolveDoors()
    {
        WaitForEndOfFrame wf = new WaitForEndOfFrame();

        dissolveFxEnded = false;

        foreach (Door d in doors)
        {
            d.door.GetComponentInChildren<MeshRenderer>().material = d.dissolveMat;
        }

        MaterialPropertyBlock mpb = new MaterialPropertyBlock();

        float dissolve = 0f;

        while (dissolve <= 1f)
        {
            mpb.SetFloat("_Fill", dissolve);
            dissolve += Time.deltaTime;

            foreach (Door d in doors)
            {
                d.door.GetComponentInChildren<MeshRenderer>().SetPropertyBlock(mpb);
            }

            yield return wf;
        }

        foreach (Door d in doors)
        {
            d.door.SetActive(false);
            d.door.GetComponentInChildren<MeshRenderer>().material = d.defaultMat;
        }

        dissolveFxEnded = true;
    }

    //Instantiate next room and attach it at the exit door
    public virtual RoomBehaviour GenerateRoom(RoomBehaviour room)
    {
        RoomBehaviour newRoom = Instantiate(room,joint.position,Quaternion.identity);
        
        return newRoom;
    }

    private void OnTriggerEnter(Collider other)
    {
        //starts the fight , closing the player inside the room and enables the enemies
        if(other.tag == "Player" && !chunkStarted)
        {
            chunkStarted = true;

            GameManager.instance.currentRoom = this;

            foreach (Transform t in GetComponentsInChildren<Transform>())
            {
                if (t.gameObject.layer == 9)
                {
                    t.gameObject.layer = 12;
                }       
                else if (t.gameObject.layer == 18)
                {
                    t.gameObject.layer = 16;
                }
            }
            
            SpawnEnemyes();
        }
    }

    void SpawnEnemyes()
    {
        //closes the door 
        if (enemies.Count > 0)
        {
            exitDoor.SetActive(true);
            entryDoor.SetActive(true);
	    StartCoroutine(MaterializeDoors());
            FMODManager.instance.TerminalNewChunk();
            //activates the enemies pre instantiated in the room            
            foreach (Enemy enemy in enemies)
            {
                enemy.room = this;
                enemy.gameObject.SetActive(true);
            }

            GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.ChunkEnter, null);
        }
    }

    //called by the enemy to get removed from the list
    public void EnemyDestroyed(Enemy enemy)
    {

        if (enemies.Contains(enemy))
        {
            enemies.Remove(enemy);
        }

        //if there are no more enemies the room is clear and the doors get opened
        if(enemies.Count == 0)
        {
            RoomClear();
        }
    }

    //chunk completed behaviour
    void RoomClear()
    {
        FMODManager.instance.TerminalChunkC();

        chunkCompleted = true;
        chunkStarted = false;

        GameManager.instance.triggerManager.ActivateEffect(Trigger.Triggers.ChunkCompleted, null);

        GameManager.instance.triggerManager.RemoveCards();

        //notify the dungeon creator that will notify the gui
        if (GameManager.instance.player.currentHealth > 0)
        {
            currentRoomCleared?.Invoke();
        }
    }

    public void EnableGui()
    {
        if (GameManager.instance.player.currentHealth > 0)
        {
            enableGUI?.Invoke();
        }
    }

    [ContextMenu("Set Enemies")]
    void SetEnemies()
    {
        enemies = new List<Enemy>(FindObjectsOfType<Enemy>());
    }
    
}

//auto sets the enemy in every room , in the editor
#if UNITY_EDITOR
[InitializeOnLoadAttribute]
public static class RoomControl
{
    static RoomControl()
    {
        EditorApplication.hierarchyChanged += OnHierarchyChanged;
    }

    static void OnHierarchyChanged()
    {
        if (!EditorApplication.isPlaying)
        {
            RoomBehaviour room = Object.FindObjectOfType<RoomBehaviour>();
            if (room != null)
            {
                room.enemies = new List<Enemy>(Object.FindObjectsOfType<Enemy>());
            }
                
        }
        
    }
}
#endif
