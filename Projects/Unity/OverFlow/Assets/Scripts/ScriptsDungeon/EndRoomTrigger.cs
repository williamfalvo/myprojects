﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndRoomTrigger : MonoBehaviour
{
    public RoomBehaviour room;
    public GameObject inputImage;
    bool isUsed;
    bool isInside;
    bool panelOpened;


    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            isInside = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.CompareTag("Player"))
        {
            isInside = false;
        }
    }

    private void Update()
    {
        if(inputImage != null)
        {
            inputImage.SetActive(isInside && !isUsed && room.chunkCompleted);
        }        

        if(isInside && Input.GetButtonDown("Interact") && !isUsed && room.chunkCompleted)
        {
            isUsed = true;
            StartCoroutine(CoChunk());

            //GameManager.instance.inputManager.enabled = false;
            //room.EnableGui();

            ////room.exitDoor.SetActive(false);
            ////room.entryDoor.SetActive(false);

            //StartCoroutine(room.DissolveDoors());

            //isUsed = true;
        }
    }

    public IEnumerator CoChunk()
    {
        GameManager.instance.inputManager.enabled = false;

        StartCoroutine(room.DissolveDoors());

        yield return new WaitUntil(() => room.dissolveFxEnded);

        room.EnableGui();        
    }
}
