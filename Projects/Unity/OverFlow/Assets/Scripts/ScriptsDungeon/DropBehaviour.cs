﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropBehaviour : MonoBehaviour
{
    public Card drop; //enemy card drop
    public float rotationSpeed = 500;

    public GameObject dropFx;

    void Update()
    {
        transform.Rotate(new Vector3(0, Time.deltaTime * rotationSpeed, 0));
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            GameManager.instance.objectsInventory.AddCard(drop);//when the player collides with the card dropped it gets added to his inventory

            GameObject fx = Instantiate(dropFx, other.ClosestPoint(transform.position), Quaternion.identity);
            Destroy(fx, 2f);

            Destroy(gameObject);
        }
    }
}
