﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TeleportColor { Red, Green, Blue, Yellow, Purple };

public class DoorBehaviour : MonoBehaviour
{
    bool isLocked = false;
    bool isInside = false;

    public Transform nextDoor;
    public Sprite[] sprites;
    public GameObject TeleportFx;
    public GameObject TeleportExit;
    public TeleportColor color;

    public GameObject inputImage;

    GameObject player;
    GameObject telFx;
    PlayerController playerController;

    ParticleSystem.MinMaxGradient[] colors;
    Color col;


    private void Awake()
    {
        if (nextDoor == null)
        {
            telFx = Instantiate(TeleportExit, transform);
        }
        else
        {
            telFx = Instantiate(TeleportFx, transform);
        }        

        ParticleSystem[] fx = telFx.GetComponentsInChildren<ParticleSystem>();
        colors = new ParticleSystem.MinMaxGradient[fx.Length];

        ParticleSystem.MainModule[] pars = new ParticleSystem.MainModule[fx.Length];

        for (int i = 0; i < fx.Length; i++)
        {
            pars[i] = fx[i].main;
        }

        switch (color)
        {
            case (TeleportColor.Red):
                col = new Color(1f, 0f, 0f);
                break;

            case (TeleportColor.Green):
                col = new Color(0f, 1f, 0f);
                break;

            case (TeleportColor.Blue):
                col = Color.cyan;
                break;

            case (TeleportColor.Yellow):
                col = new Color(1f, 0.5f, 0.2f);
                break;

            case (TeleportColor.Purple):
                col = new Color(.5f, 0f, 1f);
                break;
        }

        for (int i = 0; i < fx.Length; i++)
        {
            //colors[i] = col;
            pars[i].startColor = new Color(col.r, col.g, col.b);
        }

        if (nextDoor == null)
        {
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = sprites[1];
        }
        else
        {
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = sprites[0];
        }
    }

    private void Update()
    {
        if (isInside && Input.GetButtonDown("Interact"))
        {
            if (!isLocked && nextDoor != null)
            {
                //makes the player "teleport" to the other part of the dungeon linked to this door and attend a 0.1 second to not make this action more then one time on this frame
                StartCoroutine(GoToNextDoor(player));
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isInside = false;
            if(nextDoor != null && inputImage != null)
            {
                inputImage.SetActive(false);
            }            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            isInside = true;
            player = other.gameObject;
            playerController = player.GetComponent<PlayerController>();
            if (nextDoor != null && inputImage != null)
            {
                inputImage.SetActive(true);
            }
        }
    }

    //send the player to the linked door position
    IEnumerator GoToNextDoor(GameObject other)
    {
        StartCoroutine(playerController.TeleportIn());

        yield return new WaitForSeconds(.6f);
        other.transform.position = new Vector3(nextDoor.position.x, nextDoor.position.y, 0);

        StartCoroutine(playerController.TeleportOut());
    }
}
