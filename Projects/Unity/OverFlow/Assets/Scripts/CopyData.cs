﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct CopyData
{
    public int level;
    public float time;
    public int completion;

    public void DataToCopy()
    {
        level = GameManager.instance.player.level;
    }
}
