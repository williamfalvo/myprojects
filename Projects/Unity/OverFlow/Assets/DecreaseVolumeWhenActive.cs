﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecreaseVolumeWhenActive : MonoBehaviour
{
    private void OnEnable()
    {
        FMODManager.instance.DecreaseChunkVolume();
        FMODManager.instance.DecreaseRainVolume();
    }

    private void OnDisable()
    {
        FMODManager.instance.ResetChunkVolume();
        FMODManager.instance.ResetRainVolume();
    }
}
