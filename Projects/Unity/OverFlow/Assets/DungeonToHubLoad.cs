﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DungeonToHubLoad : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadAsyncScene());
    }

    
    IEnumerator LoadAsyncScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Hub");
        
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    private void OnDisable()
    {
        GameManager.instance.playerController.EnterHub(GameManager.instance.transform.position);
    }


}
