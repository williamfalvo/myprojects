﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowSetup : MonoBehaviour
{
    public Material windowNoRain;

    // Start is called before the first frame update
    void Awake()
    {
        //Change the random seed
        Random.InitState(System.DateTime.Now.Second);

        float v = Random.value;
        if(v < 0.5f)
        {
            GetComponent<MeshRenderer>().material = windowNoRain;
        }
    }

}
