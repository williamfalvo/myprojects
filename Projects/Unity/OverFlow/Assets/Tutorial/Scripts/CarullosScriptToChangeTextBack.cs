﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarullosScriptToChangeTextBack : MonoBehaviour
{
    public TMPro.TextMeshProUGUI text;
    public string newText;
    string oldText;

    private void Start()
    {
        oldText = text.text;
    }

    private void OnEnable()
    {
        text.text = newText;
    }
    private void OnDisable()
    {
        text.text = oldText;
    }
}
