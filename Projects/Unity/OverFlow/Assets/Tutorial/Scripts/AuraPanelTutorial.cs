﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuraPanelTutorial : MonoBehaviour
{
    public GameObject trigger;
    public GameObject confirm;
    public float unlockTime;
    bool canClosePanel = false ;


    private void OnEnable()
    {
        Time.timeScale = 0;
        StartCoroutine(ClosePanelWait());
    }

    private void OnDisable()
    {
        canClosePanel = false;
        Time.timeScale = 1;
        Destroy(trigger);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Jump") && canClosePanel)
        {
            gameObject.SetActive(false);
        }
    }

    IEnumerator ClosePanelWait()
    {
        yield return new WaitForSecondsRealtime(unlockTime);
        confirm.SetActive(true);
        canClosePanel = true;
    }
}
