﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChipsetText : MonoBehaviour
{
    public string onCardText;
    public string onExitText;
    public TextMeshProUGUI tutorialText;
    public GameObject endButton;

    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if(EventSystem.current.currentSelectedGameObject == endButton)
        {
            tutorialText.text = onExitText;
        }
        else
        {
            tutorialText.text = onCardText;
        }
    }
}
