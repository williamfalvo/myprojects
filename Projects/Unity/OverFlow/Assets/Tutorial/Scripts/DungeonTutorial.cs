﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonTutorial : MonoBehaviour
{
    public GameObject linkedEntity;
    bool readyToDeactivate = false;

    public enum Type
    {
        Enemy, TriggerEnter, Press, noone
    }
    public Type myType;

    // Update is called once per frame
    void Update()
    {
        if(myType == Type.Enemy)
        {
            if(linkedEntity == null)
            {
                Destroy(gameObject);
            }
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (myType == Type.TriggerEnter)
            {
                Destroy(linkedEntity);
            }
            if (myType == Type.Press && !linkedEntity.activeSelf)
            {
                linkedEntity.SetActive(true);
            }
        }
    }
}
