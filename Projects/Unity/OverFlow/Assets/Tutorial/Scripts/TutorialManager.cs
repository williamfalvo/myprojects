﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public TMPro.TextMeshProUGUI tutorialText;

    public GameObject terminalMainPanel;

    public List<GameObject> tutorialsGameobjects;
    public bool tutorialOn = true;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.tutorial = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("ChipSet"))
        {
            if(tutorialOn)
            {
                tutorialText.text = "Toggle On Tutorial";
                tutorialOn = false;
                GameManager.instance.tutorialOn = false;
                //EnableTutorial();
            }
            else
            {
                tutorialText.text = "Toggle Off Tutorial";
                tutorialOn = true;
                GameManager.instance.tutorialOn = true;

                //EnableTutorial();
            }
        }
    }


    public void EnableTutorial()
    {
        foreach(GameObject obj in tutorialsGameobjects)
        {
            obj.SetActive(!obj.activeSelf);
        }
    }
}
