﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StatsTutorial : MonoBehaviour
{
    public GameObject tutorialPanel;
    TutorialManager tutorial;

    private void Awake()
    {
        tutorial = GameManager.instance.tutorial;

    }

    private void Update()
    {
        if(tutorial.tutorialOn && !tutorialPanel.activeSelf)
        {
            tutorialPanel.SetActive(true);
        }
        if(!tutorial.tutorialOn && tutorialPanel.activeSelf)
        {
            tutorialPanel.SetActive(false);
        }
    }
}
