﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EndPanelTutorial : MonoBehaviour
{
    public RoomBehaviour thisRoom;
    public TMPro.TextMeshProUGUI text;
    public string roomClearedText;

    // Start is called before the first frame update
    void Start()
    {
        thisRoom.currentRoomCleared += ChangeText;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeText()
    {
        text.text = roomClearedText;
    }
}
