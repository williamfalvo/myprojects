﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuEffects : MonoBehaviour
{
    [Space(5), Header("Streak effects")]
    [SerializeField] private float streakMaxSize;
    [SerializeField] private float streakResizeSpeed;
    public Transform streakTransform;

    public void OnEnable()
    {
        StreakEffects();
    }

    public void StreakEffects()
    {
        StartCoroutine(CoResizeStreak());
    }

    IEnumerator CoResizeStreak()
    {
        streakTransform.localScale = new Vector3(streakMaxSize, streakMaxSize, streakMaxSize);
        yield return null;

            for (float i = streakTransform.localScale.x; i >= 1; i -= streakResizeSpeed)
            {
                streakTransform.localScale = new Vector3(i, i, i);
                yield return null;
            }
        StartCoroutine(DeResizeStreak());
    }

    IEnumerator DeResizeStreak()
    {
        for (float i = streakTransform.localScale.x; i <= streakMaxSize; i += streakResizeSpeed)
        {
            streakTransform.localScale = new Vector3(i, i, i);
            yield return null;
        }
        StartCoroutine(CoResizeStreak());
    }
}
