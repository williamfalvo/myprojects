﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class SelectionMenu : MonoBehaviour
{
    [SerializeField]
    Button backButton;

    [SerializeField]
    GameObject fadeOut;

    GameObject lastButton;

    public GameObject LastButton { get => lastButton; set => lastButton = value; }

    //Do a OnClick event of back button
    private void Update()
    {
        if (backButton != null && Input.GetKeyDown(KeyCode.Escape))
        {
            backButton.onClick.Invoke();
        }
    }

    //Set a button selected to enable the navigation
    public void SetSelected(GameObject button)
    {
        EventSystem.current.SetSelectedGameObject(button);
    }

    //Change scene using the build index
    public void ChangeScene(int sceneIndex)
    {
        if (fadeOut != null)
        {
            fadeOut.SetActive(true);
            StartCoroutine(FadeOut(sceneIndex));
        }
        else
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(sceneIndex);
        }
                  
    }

    //Call a SwitchButton's method to switch the option
    public void SwitchOptions(SwitchButton button)
    {
        FindObjectOfType<AudioManager>().PlayEffect(1);
        button.Switch();
    }

    //Exit game function
    public void ExitGame()
    {
        Application.Quit();
    }

    //Retry level
    public void Retry()
    {  
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //Time.timeScale = 1;
    }

    //Resume game time scale
    public void Resume()
    {
        Time.timeScale = 1;
    }

    //Close panel and set selected the last button pressed
    public void ClosePanel()
    {
        FindObjectOfType<AudioManager>().PlayEffect(1);
        SetSelected(LastButton);
    }
    
    //Fade out animation
    IEnumerator FadeOut(int sceneIndex)
    {
        yield return new WaitForSeconds(0.3f);
        Time.timeScale = 1;
        SceneManager.LoadScene(sceneIndex);
    }
}
