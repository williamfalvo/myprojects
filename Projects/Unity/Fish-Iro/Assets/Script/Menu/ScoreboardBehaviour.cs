﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreboardBehaviour : MonoBehaviour
{
    public List<Text> scores;
    public List<Image> images;
    public List<Sprite> background;
    public List<HorizontalLayoutGroup> starsGridList;
    public GameObject starPrefab;
    public List<GameObject> stars;

    private void OnEnable()
    {
        if(!SaveSystem.FileExists())
        {
            for (int i = 0; i < 5; i++)
            {
                SaveSystem.SaveData(new Data(0, 0));
            }
        }
        else
        {
            List<Data> tempList = SaveSystem.LoadData();
            if (tempList.Count < 5)
            {
                for (int i = 0; i < tempList.Count; i++)
                {
                    SaveSystem.SaveData(new Data(0, 0));
                }
            }
        }

        List<Data> dataList = SaveSystem.LoadData();

        dataList.Sort((s1, s2) => s1.highscore.CompareTo(s2.highscore));

        for(int i = dataList.Count-1, j = 0; i > dataList.Count-scores.Count-1; i--, j++)
        {

             scores[j].text = dataList[i].highscore.ToString();

            // Scoreboard background
            if (dataList[i].levelDifficulty == 1)
            {
                images[j].sprite = background[0];
            }
            else if (dataList[i].levelDifficulty == 2)
            {
                images[j].sprite = background[1];
            }
            else if (dataList[i].levelDifficulty == 3)
            {
                images[j].sprite = background[2];
            }

            // Stars zone
            if (dataList[i].highscore >= 250)
            {
                stars.Add(Instantiate(starPrefab, starsGridList[j].transform));
            }
            if(dataList[i].highscore >= 500)
            {
                stars.Add(Instantiate(starPrefab, starsGridList[j].transform));
            }
            if(dataList[i].highscore >= 1000)
            {
                stars.Add(Instantiate(starPrefab, starsGridList[j].transform));
            }
        }
    }

    private void OnDisable()
    {
        // Destroy the stars
        for (int i = 0; i < stars.Count; i++)
        {
            Destroy(stars[i].gameObject);
        }
    }
}
