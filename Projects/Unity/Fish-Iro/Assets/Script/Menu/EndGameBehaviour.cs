﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameBehaviour : MonoBehaviour
{
    [Space(5), Header("Score parameter to choose the final star")]
    public int oneStarScore;
    public int twoStarScore;
    public int threeStarScore;

    [Space(5), Header("Score and ComboMax Text")]
    public Text scoreText;
    public Text comboMaxText;

    [Space(5), Header("Star's GridLayout")]
    public GridLayoutGroup starsGrid;

    [Space(5), Header("Star image prefab")]
    public GameObject starPrefab;
}
