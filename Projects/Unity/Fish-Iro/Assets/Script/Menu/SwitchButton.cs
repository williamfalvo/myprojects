﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SwitchButton : MonoBehaviour
{
    [Space(5), Header("ON/OFF Text")]
    [SerializeField]
    Image ONText;
    [SerializeField]
    Image OFFText;
    [SerializeField]
    Sprite[] buttonsImages;


    public string settingKey;

    private void Start()
    {
        Buttons();
    }

    public void Buttons()
    {
        if(ONText != null && OFFText != null)
        {
            if (PlayerPrefs.GetInt(settingKey) == 1)
            {
                ONText.sprite = buttonsImages[1];
                OFFText.sprite = buttonsImages[0];
            }
            else
            {
                ONText.sprite = buttonsImages[0];
                OFFText.sprite = buttonsImages[1];
            }
        } 
    }

    public void Switch()
    {
        if(PlayerPrefs.GetInt(settingKey) == 0)
        {
            PlayerPrefs.SetInt(settingKey, 1);
        }
        else if(PlayerPrefs.GetInt(settingKey) == 1)
        {
            PlayerPrefs.SetInt(settingKey, 0);
        }
        Buttons();
    }


}
