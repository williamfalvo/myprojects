﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OctopusTalk : MonoBehaviour, ISelectHandler
{
    public string octopusPhrase;
    public Text octopusText;

    [Space(5), Header("Level Difficulty")]
    public int levelDifficulty;

    //When the button is selected play an audio effect and change the main menu text
    public void OnSelect(BaseEventData eventData)
    {
        if(octopusText != null) octopusText.text = octopusPhrase;
        FindObjectOfType<AudioManager>().PlayEffect(0);
    }
    
    //Play an audio effect 
    public void ClickAudio(int audioIndex)
    {
        FindObjectOfType<AudioManager>().PlayEffect(audioIndex);
    }

    //Set the level difficulty of gameplay scene
    public void LevelDifficulty()
    {
        PlayerPrefs.SetInt("levelDifficulty", levelDifficulty);
    }

}
