﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ExtraSetter : MonoBehaviour
{
    public Image extraImage;
    public Text extraDescription;

    public GameObject[] extraButtons;
    public ExtraDescription[] extraDescriptions;
    public int lastSelected;

    public int LastSelected
    {
        get => lastSelected;
        set => lastSelected = value;
    }
    
    public void SetData(ExtraDescription extra)
    {
        if(extra != null)
        {
            extraImage.sprite = extra.image;
        }
        else
        {
            extraDescription.text = "-NO EXTRA REFERENCE-";
        }
    }

    public void ClosePanel()
    {
        FindObjectOfType<AudioManager>().PlayEffect(1);
        EventSystem.current.SetSelectedGameObject(extraButtons[lastSelected]);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if(lastSelected == 0)
            {
                lastSelected = extraButtons.Length - 1;
            }
            else
            {
                lastSelected--;
            }
            SetData(extraDescriptions[lastSelected]);
            FindObjectOfType<AudioManager>().PlayEffect(0);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if(lastSelected == extraButtons.Length -1)
            {
                lastSelected = 0;
            }
            else
            {
                lastSelected++;
            }
            SetData(extraDescriptions[lastSelected]);
            FindObjectOfType<AudioManager>().PlayEffect(0);
        }
    }
}
