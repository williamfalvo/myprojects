﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour
{
    SelectionMenu selectionMenu;

    // Start is called before the first frame update
    void Start()
    {
        selectionMenu = GetComponent<SelectionMenu>();       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            //SceneManager.LoadScene(1);
            selectionMenu.ChangeScene(1);
        }
    }
}
