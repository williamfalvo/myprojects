﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowBehaviour : MonoBehaviour
{
    [SerializeField] public Color color;
    [SerializeField] private SpriteRenderer rowMid;
    [SerializeField] private SpriteRenderer rowOn;
    private float fade = 0.2f;
    private bool fading;

    public void ApplyEffects(GameObject arrow)
    {
        // Change the color of the arrow
        arrow.GetComponent<ArrowBehaviour>().ChangeColor(color);

        //Change the row to glow
        rowOn.enabled = true;
    }

    public void LeaveEffects()
    {
        rowOn.enabled = false;
    }

    public void RowMidEffects()
    {
        rowMid.enabled = true;
        if(!fading) StartCoroutine(CoFadeOutRow());
    }

    IEnumerator CoFadeOutRow()
    {
        fading = true;
        for (float f = 1f; f >= -0.05f; f -= fade)
        {
            Color c = rowMid.material.color;
            c.a = f;
            rowMid.material.color = c;
            yield return new WaitForSeconds(0.05f);
        }
        fading = false;
        rowMid.enabled = false;
    }
}
