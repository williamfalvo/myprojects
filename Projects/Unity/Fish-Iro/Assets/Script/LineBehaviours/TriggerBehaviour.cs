﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBehaviour : MonoBehaviour
{
    [HideInInspector] public UIManager uiManager;
    [HideInInspector] public AudioManager audioManager;

    public Color colorLine;

    [Space(5), Header("Notes Point")]
    public int perfectScore;
    public int goodScore;

    [Space(5), Header("Perfect Hit Range (Crf ray)")]
    public float perfectRange;

    [Space(5), Header("Text Effect on hit notes")]
    public GameObject perfect;
    public GameObject good;
    public GameObject miss;

    [Space(5), Header("Particle Effect on hit notes")]
    public GameObject starPerfect;
    public GameObject starGood;
    public GameObject pressEffect;
    public GameObject holdEffect;
    private GameObject HoldEffect;
    [SerializeField] private SpriteRenderer fakeTriggerMask;

    [Space(5), Header("Long Note Zone")]
    public int pointsLongNote;
    public float delayLongNote;
    bool longNotePress;
    bool tailReach;

    [Space(5), Header("Input zone")]
    private KeyCode lastKey;
    private KeyCode inputKey;
    bool isPressed;

    [Space(5), Header("Reference")]
    [SerializeField] private RowBehaviour rowBehaviour;
    [SerializeField] private SpawnManager spawnManager;
    [SerializeField] private Animator mentor;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<ArrowTail>())
        {
            tailReach = true;
            collision.GetComponent<ArrowTail>().DestroyArrow();
        }
        else if (!tailReach && isPressed && inputKey == collision.GetComponent<ArrowBehaviour>().keyArrow)
        {
            NoteHit(collision);
            isPressed = false;
        }
        else if (Input.anyKey == false) { }
        else if (!tailReach && !Input.GetKey(collision.GetComponent<ArrowBehaviour>().keyArrow))
        {
            Missed(collision);
            Destroy(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //Destroy the long note when the tail exit from the trigger
        if (collision.GetComponent<ArrowTail>())
        {
            Destroy(collision.transform.parent.gameObject);
            tailReach = false;
            longNotePress = false;
        }
        else if (!collision.GetComponent<ArrowBehaviour>().longNote || !longNotePress)
        {
            Missed(collision);
        }

        Destroy(collision.gameObject);
    }

    private void Update()
    {
        isPressed = true;

        // Input zone
        if (Input.GetKeyDown(KeyCode.UpArrow)) inputKey = KeyCode.UpArrow;
        else if (Input.GetKeyDown(KeyCode.LeftArrow)) inputKey = KeyCode.LeftArrow;
        else if (Input.GetKeyDown(KeyCode.RightArrow)) inputKey = KeyCode.RightArrow;
        else if (Input.GetKeyDown(KeyCode.DownArrow)) inputKey = KeyCode.DownArrow;
        else inputKey = KeyCode.Dollar;

        //If i'm still pressing the long note
        if (longNotePress)
        {
            fakeTriggerMask.enabled = true;
            if (HoldEffect == null)
            {
                HoldEffect = Instantiate(holdEffect, transform);
            }
            if (Input.GetKeyUp(lastKey))
            {
                Destroy(HoldEffect, 0.1f);
                HoldEffect = null;
                tailReach = false;
                longNotePress = false;
            }
        }
        else
        {
            fakeTriggerMask.enabled = false;
            if (HoldEffect != null)
            {
                Destroy(HoldEffect);
            }
        }
    }

    public void NoteHit(Collider2D collision)
    {
        ArrowBehaviour tempArrow = collision.GetComponent<ArrowBehaviour>();
        lastKey = tempArrow.keyArrow;

        // Decrease count
        spawnManager.DecreaseCount(mentor, colorLine);


        // Perfect and good check
        float triggerCentre = collision.transform.position.x;
        float thisCentre = transform.position.x;
        if (triggerCentre <= thisCentre + perfectRange && triggerCentre >= thisCentre - perfectRange)
        {
            audioManager.PlayEffect(0);
            Hit(perfectScore, perfect, starPerfect, thisCentre, collision);
        }
        else
        {
            audioManager.PlayEffect(1);
            Hit(goodScore, good, starGood, thisCentre, collision);
        }

        // Destroy the note
        if (tempArrow.longNote)
        {
            longNotePress = true;
            collision.GetComponent<Collider2D>().enabled = false;
            tempArrow.TriggerLongNote(transform);
            StartCoroutine(CoLongNotePoints());                         //Start giving points while pressing the long note
        }
        else
        {
            Destroy(collision.gameObject);
        }

        //Effects
        rowBehaviour.RowMidEffects();
        uiManager.StreakEffects(colorLine);
    }

    public void Hit(int Score, GameObject prefabsScore, GameObject starEffect, float thisCentre, Collider2D collision)
    {
        if (Score == perfectScore)
        {
            uiManager.AddScore(Score, 1);
        }
        else
        {
            uiManager.AddScore(Score, 2);
        }

        GameObject hitEffect = Instantiate(prefabsScore, new Vector2(thisCentre, collision.transform.position.y + 1), Quaternion.identity);
        GameObject PressEffect = Instantiate(pressEffect, transform);
        PressEffect.GetComponent<ParticleSystem>().startColor = rowBehaviour.color;
        GameObject StarEffect = Instantiate(starEffect, transform.position, Quaternion.identity);
        Destroy(PressEffect, 1);
        Destroy(hitEffect, 0.5f);
        Destroy(StarEffect, 0.5f);
    }

    public void Missed(Collider2D collision)
    {
        if (uiManager.multiplierValue > 1)
        {
            uiManager.multiplierValue--;
        }

        spawnManager.WrongCount();
        uiManager.scoreHintsValue[3]++;
        uiManager.streakValue = 0;

        GameObject Miss = Instantiate(miss, new Vector2(transform.position.x, collision.transform.position.y + 1), Quaternion.identity);
        Destroy(Miss, 0.5f);
    }

    //Giving points every times consecutively while pressing the long note
    IEnumerator CoLongNotePoints()
    {
        while (longNotePress)
        {
            uiManager.AddScore(pointsLongNote);
            yield return new WaitForSeconds(delayLongNote);
        }
    }
}
