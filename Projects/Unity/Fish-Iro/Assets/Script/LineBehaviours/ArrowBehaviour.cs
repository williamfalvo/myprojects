﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowBehaviour : MonoBehaviour
{
    public bool longNote;
    public bool doubleNote;
    public bool tripleNote;

    [Space(5), Header("Note Parts")]
    public Transform longNoteHead;
    public SpriteRenderer[] skin;

    [Space(5), Header("Key to Press")]
    public KeyCode keyArrow;

    // Change the skin color
    public void ChangeColor(Color color)
    {
        for (int i = 0; i < skin.Length; i++)
        {
            skin[i].color = color;
        }
    }

    // Stop the long note head
    public void TriggerLongNote(Transform trigger)
    {
        if (longNote)
        {
            longNoteHead.parent = null;
            longNoteHead.position = trigger.position;
        }
    }
}
