﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SpawnManager : MonoBehaviour
{
    public delegate void CompleteSet(Animator mentor, int setWrong);
    public event CompleteSet SetComplete;

    [Space(5), Header("Tuning Offsets function")]
    public LevelValues[] levelValues;
    public AnimationCurve curve;

    [Space(5), Header("Notes Offsets (Rng bethween min/max + long || double || triple)")]
    [Range(0, 2f)] public float minNoteOffset;
    [Range(0, 2f)] public float maxNoteOffset;
    [Range(0, 2f)] public float longNoteOffset, doubleNoteOffset, tripleNoteOffset;

    [Space(5), Header("Set Spawn Delay")]
    [Range(0, 10f)] public float delaySet;

    [Space(5), Header("Arrow Velocity based on multiplier")]
    [Range(3f, 10f)] public float[] moveX;

    [Space(5), Header("Notes Number (min 4 Notes)")]
    [Range(0f, 10f)] public int minNotes;
    [Range(0f, 10f)] public int maxNotes;

    [Space(5), Header("Arrow and Where spawn them")]
    public Transform[] rawTransforms;               // Transform of Spawner on corrisponding Line 
    public List<GameObject> arrowSet;               // What notes will (possibly) be in each set 

    [HideInInspector] public SpriteRenderer backGround;
    [HideInInspector] public UIManager uiManager;
    [HideInInspector] public List<int> malusList;
    [HideInInspector] public GameObject boubleSpawnObj, boubleEffect;
    [HideInInspector] public int currentLine, notesCounts, wrongMalusCount;
    [HideInInspector] public bool notWrongMalus, Spawning;
    [HideInInspector] public AudioManager audioManager;

    public bool setEnd, malusDestroyed;
    private int wrongNoteCount, totalNotesCounts;

    private void Awake()
    {
        if (SceneManager.GetActiveScene().name != "Tutorial")
        {
            backGround.sprite = levelValues[PlayerPrefs.GetInt("levelDifficulty") - 1].backGround;
            int difficulty = PlayerPrefs.GetInt("levelDifficulty") - 1;
            moveX = levelValues[difficulty].moveX;
            minNotes = levelValues[difficulty].minNotes;
            maxNotes = levelValues[difficulty].maxNotes;
            minNoteOffset = levelValues[difficulty].minNoteOffset;
            maxNoteOffset = levelValues[difficulty].maxNoteOffset;
            longNoteOffset = levelValues[difficulty].longNoteOffset;
            doubleNoteOffset = levelValues[difficulty].doubleNoteOffset;
            tripleNoteOffset = levelValues[difficulty].tripleNoteOffset;
        }
    }

    public void InitializeSet(int setCount, List<int> malusList)
    {
        List<int> tempMalusList = malusList;
        wrongMalusCount = 0;
        if (Spawning == false)
        {
            if (boubleEffect != null)
            {
                audioManager.PlayEffect(5);
                GameObject bouble = Instantiate(boubleEffect, boubleSpawnObj.GetComponent<Transform>());
                boubleEffect.GetComponent<ParticleSystem>().Stop();
                Destroy(bouble, 3);
            }
        }
        StartCoroutine(SpawnSet(setCount, malusList));
    }

    // Start SpawnDelay Coroutine (X times based on multiplier)
    IEnumerator SpawnSet(int setCount, List<int> malusList)
    {
        // State Spawing begin
        Spawning = true;

        for (int i = 0; i < setCount; i++)
        {
            int rngLength = Random.Range(minNotes, maxNotes + 1);

            // Create & Populate the arrowRound Array
            GameObject[] arrowRound = new GameObject[rngLength];
            for (int j = 0; j < arrowRound.Length; j++)
            {
                int arrowIndex = Random.Range(0, arrowSet.Count - 1);
                arrowRound[j] = arrowSet[arrowIndex];
            }

            // Spawn notes 
            currentLine = malusList[i];
            StartCoroutine(SpawnDelay(malusList[i], rngLength, arrowRound, malusList));

            // Offset bethween set
            if ((i < setCount) && (arrowRound[arrowRound.Length - 1]))
            {
                yield return new WaitUntil(() => setEnd);
                yield return new WaitForSeconds(delaySet);
            }
            else yield return null;
        }
        // State Spawing end
        Spawning = false;
    }

    // Spawn notes at X position (rngSpawn), with X number of notes (rngLength), with X order of spawn (Souffle di arrowRound), in X line (based on malusList order).
    IEnumerator SpawnDelay(int rngSpawn, int rngLength, GameObject[] arrowRound, List<int> malusList)
    {
        int tempTotalNotes = rngLength;
        for (int i = 0; i < arrowRound.Length; i++)
        {
            if(arrowRound[i].GetComponent<ArrowBehaviour>().doubleNote)
            {
                tempTotalNotes++;
            }
            else if(arrowRound[i].GetComponent<ArrowBehaviour>().tripleNote)
            {
                tempTotalNotes += 2;
            }
        }
        notWrongMalus = true;
        wrongNoteCount = 0;
        setEnd = false;
        malusDestroyed = false;
        notesCounts = 0;                            // reset the notes count to destroy malus
        totalNotesCounts = tempTotalNotes;               // total number of note
        float tempMoveX = moveX[(uiManager != null ? uiManager.multiplierValue : 1) - 1];
        for (int i = 0; i < rngLength; i++)
        {
            float rngNotes = Random.Range(minNoteOffset + tempMoveX / ((minNoteOffset * 100) / maxNoteOffset), maxNoteOffset - tempMoveX / ((minNoteOffset * 100) / maxNoteOffset));
            float rngNotesOffset = curve.Evaluate(rngNotes / tempMoveX);

            if (i == 0)
            {
                foreach (var item in rawTransforms)
                {
                    item.GetComponent<RowBehaviour>().LeaveEffects();
                }
            }
            GameObject arrow = Instantiate(arrowRound[i], rawTransforms[rngSpawn].position, Quaternion.identity);    // Instantiete each note        
            arrow.GetComponent<Rigidbody2D>().velocity = new Vector2(tempMoveX, 0);                                  // Give velocity each note  
            rawTransforms[rngSpawn].GetComponent<RowBehaviour>().ApplyEffects(arrow);                                // Do all graphics effects

            ArrowBehaviour tempArrow = arrow.GetComponent<ArrowBehaviour>();

            if (tempArrow.longNote)
            {
                yield return new WaitForSeconds(minNoteOffset + rngNotesOffset + (longNoteOffset - tempMoveX / ((minNoteOffset * 100) / maxNoteOffset)));
            }
            else if (tempArrow.doubleNote)
            {
                yield return new WaitForSeconds(minNoteOffset + rngNotesOffset + (doubleNoteOffset - tempMoveX / ((minNoteOffset * 100) / maxNoteOffset)));
            }
            else if (tempArrow.tripleNote)
            {
                yield return new WaitForSeconds(minNoteOffset + rngNotesOffset + (tripleNoteOffset - tempMoveX / ((minNoteOffset * 100) / maxNoteOffset)));
            }
            else
            {
                yield return new WaitForSeconds(minNoteOffset + rngNotesOffset);
            }
        }
        setEnd = true;
    }

    public void DecreaseCount(Animator mentor, Color setColor)
    {
        notesCounts++;
        //Debug.Log(notesCounts);
        if (!malusDestroyed)
        {
            if (notesCounts > wrongNoteCount && notesCounts >= totalNotesCounts * 0.5)
            {
                
                malusDestroyed = true;
                SetComplete(mentor, wrongMalusCount);                //Call DestroyMalus in GameManager

                if (uiManager.multiplierValue < 4)
                {
                    uiManager.multiplierValue++;
                    uiManager.MultiplierEffects(setColor);
                }
            }
        }
    }

    public void Shuffle()
    {
        for (int i = 0; i < malusList.Count; ++i)
        {
            int index = Random.Range(0, malusList.Count);
            int copyMalus = malusList[index];
            malusList[index] = malusList[i];
            malusList[i] = copyMalus;
        }
    }

    public void StopCoroutines()
    {
        StopAllCoroutines();
    }

    public void WrongCount()
    {
        wrongNoteCount++;
        if (notWrongMalus && wrongNoteCount > totalNotesCounts * 0.5)
        {
            wrongMalusCount++;
            notWrongMalus = false;
        }
    }
}
