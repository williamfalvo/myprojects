﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{

    public AudioSource audioMusics;
    public AudioSource audioEffects;
    public AudioListener audioListener;
    public List<AudioClip> musics;
    public List<AudioClip> soundEffects;

    int caseMusic;

    // PLAYER PREF: Music,Effect

    private void Awake()
    {          
        if (SceneManager.GetActiveScene().name != "StartScreen")
        {
            PlayMusic(PlayerPrefs.GetInt("levelDifficulty") - 1);
        }       
    }

    private void Update()
    {
        audioMusics.enabled = (PlayerPrefs.GetInt("Music") == 1) ? true : false;
        audioEffects.enabled = (PlayerPrefs.GetInt("Effect") == 1) ? true : false;
    }

    public void PlayMusic(int musicsEffects)
    {
        if (audioEffects.enabled == true)
        {
            audioMusics.clip = musics[musicsEffects];
            audioMusics.Play();
        }
    }

    public void PlayEffect(int soundEffect)
    {
        if (audioEffects.enabled == true)
        {
            audioEffects.clip = soundEffects[soundEffect];
            audioEffects.Play();
        }
    }


}
