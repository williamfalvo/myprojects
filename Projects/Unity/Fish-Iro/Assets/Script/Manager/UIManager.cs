﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct Data
{
    public int highscore, levelDifficulty;

    public Data(int score, int level)
    {
        highscore = score;
        levelDifficulty = level;
    }
}

public class UIManager : MonoBehaviour
{
    [Space(5), Header("Score parameters to choose the number of stars")]
    public int threeStarScore;
    public int twoStarScore;
    public int oneStarScore;

    [Space(5), Header("Score/Multiplier/Streak")]
    [HideInInspector] public Text multiplier, streak, score;
    [HideInInspector] public int multiplierValue, streakValue, scoreValue;

    [HideInInspector] public Text[] scoreHints;
    [HideInInspector] public int[] scoreHintsValue;
    [HideInInspector] public Text bonusScore, CountDown;

    [Space(5), Header("Streak Effects")]
    public Transform[] streakTransform;
    public float streakMaxSize, streakSpeedSize, StreakSpeedColor;

    [Space(5), Header("Multiplier Effects")]
    public Transform[] multiplierTransform;
    public float multiplierMaxSize, multiplierSpeedSize, multiplierSpeedColor;

    Image[] tempColorImage;
    Text[] tempColorText;
    Quaternion startRotation, endRotation;

    //Star's Prefab
    [HideInInspector] public GameObject starEffect;
    [HideInInspector] public GameObject starPrefab;
    [HideInInspector] public GameObject bonusScoreEffect;

    void Update()
    {
        score.text = "Punteggio: " + scoreValue.ToString();
        streak.text = streakValue.ToString();
        multiplier.text = "x" + multiplierValue.ToString();

        for (int i = 0; i < scoreHints.Length; i++)
        {
            scoreHints[i].text = scoreHintsValue[i].ToString();
        }
    }

    public void AddScore(int value, int index)
    {
        streakValue++;
        if (streakValue > scoreHintsValue[4])
        {
            scoreHintsValue[4] = streakValue;
        }
        scoreHintsValue[index]++;
        if (Mathf.RoundToInt(streakValue / 15) >= 1)
        {
            if (streakValue % 10 == 0)
            {
                int bonusPoint = streakValue * 15;
                scoreValue += value * multiplierValue + bonusPoint;
                StartCoroutine(BonusScoreEffect());
                bonusScore.text = "+" + bonusPoint.ToString();
            }

        }
        else if (Mathf.RoundToInt(streakValue / 15) < 1) scoreValue += value * multiplierValue;

        scoreHintsValue[0] = scoreValue;
        StartCoroutine(CoIncreaseScore(value));
    }

    IEnumerator BonusScoreEffect()
    {
        bonusScore.enabled = true;
        GameObject bonusEffect = Instantiate(bonusScoreEffect, bonusScore.transform.position, Quaternion.identity);
        Destroy(bonusEffect, 3f);
        yield return new WaitForSeconds(2f);
        bonusScore.enabled = false;
    }

    public void AddScore(int value)
    {
        StartCoroutine(CoIncreaseScore(value));
    }

    public void StreakEffects(Color newColor)
    {
        StartCoroutine(CoResizeAndRotate(streakTransform, null, streakMaxSize, streakSpeedSize));
        StartCoroutine(CoChangeColor(streakTransform, Color.yellow, StreakSpeedColor));
    }

    IEnumerator CoResizeAndRotate(Transform[] trans, Transform rot, float size, float speed)
    {
        // Scale at max size
        foreach (Transform item in trans)
        {
            item.localScale = new Vector3(size, size, size);
        }

        // Rotation
        if (rot != null)
        {
            while (rot.rotation != endRotation)
            {
                startRotation = rot.rotation;
                endRotation = Quaternion.Euler(0, 0, 360);
                rot.rotation *= Quaternion.Euler(0, 0, 10);
                yield return null;
            }
            if (rot.rotation == Quaternion.Euler(0, 360, 0)) rot.rotation = Quaternion.Euler(0, 0, 0);
        }

        // Resize 
        for (float i = trans[0].localScale.x; i > 1; i -= speed * Time.deltaTime)
        {
            foreach (Transform item in trans)
            {
                item.localScale = new Vector3(i, i, i);
            }
            yield return null;
        }
    }

    IEnumerator CoChangeColor(Transform[] trans, Color newColor, float value)
    {
        Image[] tempColorImage = trans[0].GetComponentsInChildren<Image>();
        Text[] tempColorText = trans[0].GetComponentsInChildren<Text>();

        foreach (Image item in tempColorImage)
        {
            item.color = newColor;
        }
        foreach (Text item in tempColorText)
        {
            item.color = newColor;
        }

        while (tempColorText[0].color != Color.white)
        {
            foreach (Image item in tempColorImage)
            {
                item.color = Color.Lerp(item.color, Color.white, value);
            }
            foreach (Text item in tempColorText)
            {
                item.color = Color.Lerp(item.color, Color.white, value);
            }
            yield return null;
        }
    }

    IEnumerator CoIncreaseScore(int value)                 // Increase score gradually
    {
        for (int i = 0; i < value; i++)
        {
            scoreValue++;
            yield return null;
        }
    }

    public int SetStars()
    {
        int stars = 0;

        if (scoreValue >= threeStarScore)
        {
            stars = 3;
        }
        else if (scoreValue >= twoStarScore)
        {
            stars = 2;
        }
        else if (scoreValue >= oneStarScore)
        {
            stars = 1;
        }

        return stars;
    }

    public void MultiplierEffects(Color newColor)
    {
        Transform[] tempTransform = multiplierTransform[0].GetComponentsInChildren<Transform>();
        StartCoroutine(CoResizeAndRotate(tempTransform, multiplierTransform[0].GetChild(0).transform, multiplierMaxSize, multiplierSpeedSize));
        StartCoroutine(CoChangeColor(multiplierTransform, newColor, multiplierSpeedColor));
    }

    public void ActiveScore(HorizontalLayoutGroup starsGrid)
    {
        foreach (Text score in scoreHints)
        {
            score.gameObject.SetActive(false);
        }

        int stars = SetStars();
        StartCoroutine(AnimationScores(starsGrid, stars));
    }

    IEnumerator AnimationScores(HorizontalLayoutGroup starsGrid, int stars)
    {
        yield return new WaitForSeconds(0.75f);
        foreach(Text score in scoreHints)
        {
            score.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.75f);
        }

        for (int i = 0; i < stars; ++i)
        {
            GameObject star = Instantiate(starPrefab, starsGrid.transform);
            yield return new WaitForSeconds(0.75f);
        }
    }
}
