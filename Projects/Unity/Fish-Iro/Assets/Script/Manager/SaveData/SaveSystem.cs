﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using System.Collections.Generic;

public static class SaveSystem
{
    public static void SaveData(Data data)
    {
        List<Data> dataList = LoadData();
        dataList.Add(data);

        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/fishiro.data";
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, dataList);
        stream.Close();
    }

    public static List<Data> LoadData()
    {
        string path = Application.persistentDataPath + "/fishiro.data";

        if (File.Exists(path))
        {
            //Debug.Log("Save file found in " + path);
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            List<Data> data = formatter.Deserialize(stream) as List<Data>;
            stream.Close();


            return data;
        }
        else
        {
            Debug.Log("Save file NOT found in " + path);
            List<Data> data = new List<Data>();
            return data;
        }
    }

    public static bool FileExists()
    {
        string path = Application.persistentDataPath + "/fishiro.data";
        if (File.Exists(path))
        {
            return true;
        }
        else return false;
    }
}
