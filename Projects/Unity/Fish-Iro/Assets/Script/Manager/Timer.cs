﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Slider slider;
    public Text timer;
    public float timeGame;
    public float time;
    public UIManager uiManager;

    float t = 0.0f;

    private void Start()
    {
        StartCoroutine(TimerDelay());
    }

    private void Update()
    {
        if (uiManager.CountDown.enabled == false)
        {
            Slider();
        }      
    }

    IEnumerator TimerDelay()
    {
        for (int i = 0; i < timeGame; i++)
        {
            time++;              
            yield return new WaitForSeconds(1f);
        }
    }

    void Slider()
    {
        slider.maxValue = timeGame;
        slider.value = Mathf.Lerp(0, timeGame, t);
        t += 0.01f * Time.deltaTime;
    }


}
