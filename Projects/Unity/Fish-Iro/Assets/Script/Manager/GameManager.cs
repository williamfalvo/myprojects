﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Spine;
using Spine.Unity;

public class GameManager : MonoBehaviour
{
    [Space(5), Header("Round Settings")]
    [Range(0, 30f)] public int round;                        // ActualRound 
    [Range(1, 30f)] public int maxRound;                     // MaxRound to reach endgame

    [Space(5), Header("Reference")]
    public List<Fish> allFish;
    public List<string> allMalus;

    [HideInInspector] public Timer timer;
    [HideInInspector] public UIManager uiManager;
    [HideInInspector] public SpawnManager spawnManager;
    [HideInInspector] public GameObject fishSpawnObj, boubleSpawnObj, boubleEffect, pausePanel, endGamePanel, resumeButton, retryButton , healEffect, heartEffect, HealSpawner, healText, beginBouble;
    [HideInInspector] public Transform[] healTransform;

    private Fish tempFish;                                   // Actual round fish
    private Fish tempMalus;                                  // Actual round malus
    private bool isFinish;

    [HideInInspector] public GameState state;

    public enum GameState
    {
        preGame,
        poolChoose,
        spawnSet,
        endSpawnSet,
        endGame,
    }

    private void OnEnable()
    {
        spawnManager.SetComplete += DestroyMalus;
    }
    
    private void Start()
    {
        state = GameState.preGame;    // Initial State
        StartCoroutine(preGame());
        Time.timeScale = 1;
    }

    // FSM - Finite State Machine
    private void Update()
    {
        if (state == GameState.preGame)
        {
        }
        else if (state == GameState.poolChoose)                // Choose random fish and Initialize Set
        {
            poolChoose();
        }
        else if (state == GameState.spawnSet)                  // Intermediate State (Controll of correct/incorrect Notes)
        {
            if (spawnManager.Spawning == false)
            {
                state = GameState.endSpawnSet;
            }
            else if (spawnManager.setEnd && (timer.time >= timer.timeGame))
            {
                Invoke("endGameMusic" , 3);
                spawnManager.StopCoroutines();
            }
        }
        else if (state == GameState.endSpawnSet)               // Intermediate State (Wait end spawn of current notes set)
        {
            endGameCondition();
        }
        else if (state == GameState.endGame)                   // End of all round
        {
            if (!isFinish)
            {
                endGame();
                isFinish = true;
            }
        }

        // Pause Menu
        if ((Input.GetKeyDown(KeyCode.Escape)) && (Time.timeScale == 1) && (state != GameState.endGame))
        {
            pausePanel.SetActive(true);
            Time.timeScale = 0;
            spawnManager.audioManager.audioMusics.Pause();
            pausePanel.GetComponent<SelectionMenu>().SetSelected(resumeButton);
        }
        else if (!pausePanel.activeSelf && Time.timeScale == 1)
        {
            spawnManager.audioManager.audioMusics.UnPause();
        }
    }

    void poolChoose()
    {
        if (round > 0)
        {
            foreach (Transform child in fishSpawnObj.transform)
            {
                Destroy(child.gameObject);
            }
        }
        // Choose a random fish
        int rngFish = Random.Range(0, allFish.Count);

        spawnManager.Shuffle();                                                           // Shuffle lines               
        spawnManager.InitializeSet(uiManager.multiplierValue, spawnManager.malusList);    // Instantiate Notes

        // Spawn fish & malus
        tempFish = Instantiate(allFish[rngFish], fishSpawnObj.GetComponent<Transform>());
        tempFish.GetComponent<SkeletonAnimation>().Skeleton.SetSkin("base");
        for (int i = 0; i < uiManager.multiplierValue; i++)
        {
            tempMalus = Instantiate(allFish[rngFish], fishSpawnObj.GetComponent<Transform>());
            tempMalus.GetComponent<SkeletonAnimation>().Skeleton.SetSkin(allMalus[spawnManager.malusList[i]]);
            tempMalus.GetComponent<MeshRenderer>().sortingOrder = 1;
        }
        // Change State
        state = GameState.spawnSet;
    }

    void endGameCondition()
    {
        if ((round < maxRound) && (timer.time < timer.timeGame))
        {
            round++;
            state = GameState.poolChoose;
        }
        else if (((timer.time >= timer.timeGame) || (round >= maxRound)) )
        {
            state = GameState.endGame;
        }
    }

    void endGameMusic()
    {
        state = GameState.endGame;
    }

    void endGame()
    {
        endGamePanel.SetActive(true);
        endGamePanel.GetComponent<SelectionMenu>().SetSelected(retryButton);
        HorizontalLayoutGroup grid = endGamePanel.GetComponentInChildren<HorizontalLayoutGroup>();
        uiManager.ActiveScore(grid);
        SaveData();
    }

    // Destroy the malus when called by SetComplete in SpawnManager
    public void DestroyMalus(Animator mentor, int setWrong)
    {
        mentor.SetTrigger("action");
        mentor.GetComponent<AudioSource>().Play();
        Destroy(fishSpawnObj.transform.GetChild(1 + setWrong).gameObject);

        GameObject HealEffect = Instantiate(healEffect, healTransform[spawnManager.currentLine].transform);
        
        Destroy(HealEffect, 3f);
        StartCoroutine(HealText(HealEffect));
        //If this is the last malus healed spawn healText
        if (fishSpawnObj.transform.childCount-1 == 1)
        {            
            StartCoroutine(HeartDelay());
        }        
    }

    IEnumerator HealText(GameObject HealEffect)
    {
        yield return new WaitForSeconds(1.5f);
        HealEffect.GetComponent<ParticleSystem>().Stop();
        GameObject HeartEffect = Instantiate(heartEffect, spawnManager.boubleSpawnObj.GetComponent<Transform>());
        Destroy(HeartEffect, 1.5f);
    }

    IEnumerator HeartDelay()
    {
        yield return new WaitForSeconds(1.5f);
        GameObject HealText = Instantiate(healText, HealSpawner.transform);
        Destroy(HealText, 1);
    }

    IEnumerator preGame()
    {
        for (int i = 3; i > 0; i--)
        {
            uiManager.CountDown.text = i.ToString();
            yield return new WaitForSeconds(1f);
        }
        beginBouble.GetComponent<ParticleSystem>().Stop();
        Destroy(beginBouble, 5);
        uiManager.CountDown.enabled = false;
        state = GameState.poolChoose;
    }

    //Save the match stats such as highscore and the current scene
    public void SaveData()
    {
        Data data = new Data(uiManager.scoreValue, PlayerPrefs.GetInt("levelDifficulty"));
        SaveSystem.SaveData(data);
    }

    public void pauseMusic()
    {
        spawnManager.audioManager.audioMusics.Play();
    }


}
