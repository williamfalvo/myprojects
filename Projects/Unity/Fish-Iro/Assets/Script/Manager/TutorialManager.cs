﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{
    public SpawnManager spawnManager;

    public bool isCorrect = true;

    //Tutorial's animations
    public GameObject[] timelines;

    //Different phrases of tutorial's octopus
    public string[] phrases;
    public int maxIndexOfPhrases;
    public int indexPhrase;
    public int numberOfPhases;
    public int indexPhase;

    //Line where spawn arrows
    public Transform spawnerLine;

    //List of different arrow in game to spawn
    public List<GameObject> normalArrow;        
    public List<GameObject> longArrow;
    public List<GameObject> diagonalArrow;

    //End and pause panel whit buttons to set selected
    public GameObject endPanel;
    public GameObject endButton;
    public GameObject pausePanel;
    public GameObject pauseButton;

    //Tutorial's panels 
    public GameObject octopus;
    public Text phraseText;
    public GameObject tutorialTextBox;
    public GameObject missedTextBox;

    //Different phases of tutorial
    public TutorialState state;

    public enum TutorialState
    {
        spawnOneArrow,
        attendInput,
        spawnArrows,
        chooseResult,
        missedArrow,
        endTutorial,
    }

    //Initialize the first phase of tutorial
    private void Start()
    {
        phraseText.text = phrases[indexPhrase];
        StartCoroutine(Phase1());
    }

    // FSM - Finite State Machine
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && state != TutorialState.endTutorial)  //If the tutorial isn't finished, active the pause menu
        {
            pausePanel.SetActive(true);            
            Time.timeScale = 0;
            pausePanel.GetComponent<SelectionMenu>().SetSelected(pauseButton);
        }


        if(state == TutorialState.spawnArrows)  //Spawn the tutorial's arrow
        {
            isCorrect = true;
            spawnManager.InitializeSet(1, spawnManager.malusList);
            state = TutorialState.chooseResult;
        }
        else if(state == TutorialState.chooseResult)    //Choose if the player done the set with error 
        {
            if(spawnManager.Spawning == false && isCorrect)
            {
                if(indexPhase != 3)
                {
                    if(indexPhase == 0)
                    {
                    spawnManager.arrowSet = normalArrow;
                    }
                    else if(indexPhase == 1)
                    {
                        spawnManager.arrowSet = longArrow;
                    }
                    else if(indexPhase == 2)
                    {
                        spawnManager.arrowSet = diagonalArrow;
                    }
                    SpawnOneArrow(spawnManager.arrowSet[0]);
                    state = TutorialState.spawnOneArrow;
                }
                else
                {
                    Debug.Log("Tutorial Finished!");
                    timelines[3].SetActive(true);
                    indexPhrase++;
                    phraseText.text = phrases[indexPhrase];                    
                    state = TutorialState.endTutorial;
                    StartCoroutine(ActivateEndPanel());
                }
            }            
        }
        else if (state == TutorialState.missedArrow)    //If the player miss one arrow reset the current phase
        {
            missedTextBox.SetActive(true);
            spawnManager.StopCoroutines();
            //phraseText.text = "Ops! Hai sbagliato, ma non preoccuparti riprova...";
            if (Input.GetKeyDown(KeyCode.Return))
            {
                missedTextBox.SetActive(false);
                state = TutorialState.spawnArrows;
            }
        }
    }

    //Enable the first timeline and after player's inputs initialize the first tutorial state
    IEnumerator Phase1()
    {
        timelines[0].SetActive(true);

        yield return new WaitForSeconds(3);
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return) && Time.timeScale == 1);
        
        indexPhrase++;
        phraseText.text = phrases[indexPhrase];
        yield return new WaitForSeconds(1);
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Return) && Time.timeScale == 1);

        timelines[1].SetActive(true);
        yield return new WaitForSeconds(2);

        state = TutorialState.spawnOneArrow;
        SpawnOneArrow(spawnManager.arrowSet[0]);
    }

    //Enable and modify the tutorial text and change state 
    public IEnumerator FirstInput()
    {
        indexPhrase++;
        phraseText.text = phrases[indexPhrase];
        tutorialTextBox.SetActive(true);
        timelines[2].SetActive(true);

        yield return new WaitForSeconds(2);

        state = TutorialState.attendInput;
    }

    //Close the tutorial text
    public IEnumerator DeactiveTextBox()
    {
        yield return new WaitForSeconds(1.5f);
        tutorialTextBox.SetActive(false);
    }

    //Open the end panel
    public IEnumerator ActivateEndPanel()
    {
        yield return new WaitForSeconds(3.5f);
        tutorialTextBox.SetActive(false);
        endPanel.SetActive(true);
        endPanel.GetComponent<SelectionMenu>().SetSelected(endButton);

    }

    //Spawn the tutorial arrow
    void SpawnOneArrow(GameObject arrowPrefab)
    {
        GameObject arrow = Instantiate(arrowPrefab, spawnerLine.position, Quaternion.identity);
        arrow.GetComponent<Rigidbody2D>().velocity = new Vector2(2, 0);
        spawnerLine.GetComponent<RowBehaviour>().ApplyEffects(arrow);
        indexPhase++;
    }
}
