﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerTutorial : MonoBehaviour
{
    public TutorialManager tutorialManager;
    public AudioManager audioManager;

    public int score;
    public Text scoreText;

    ArrowBehaviour arrow;
    public Transform triggerCircle;
    [SerializeField] private RowBehaviour rowBehaviour;

    public SpriteRenderer fakeTriggerMask;
    public GameObject starPerfect;
    public GameObject pressEffect;
    public GameObject holdEffect;
    private GameObject HoldEffect;

    bool longNotePress;
    bool tailReach;
    private KeyCode lastKey;

    bool isPressed;

    public int pointsLongNote;
    public float delayLongNote;

    public int counterHit;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Block the arrow and set the tutorial to attend input
        if (tutorialManager.state == TutorialManager.TutorialState.spawnOneArrow && Time.timeScale == 1)
        {            
            arrow = collision.GetComponent<ArrowBehaviour>();
            if(arrow.doubleNote || arrow.tripleNote)
            {
                arrow.GetComponentInParent<Rigidbody2D>().velocity = Vector2.zero;
            }
            else
            {
                arrow.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            }
            
            StartCoroutine(tutorialManager.FirstInput());
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(Time.timeScale == 1 && tutorialManager.state != TutorialManager.TutorialState.spawnOneArrow)
        {
            if (collision.GetComponent<ArrowTail>())
            {
                tailReach = true;
                collision.GetComponent<ArrowTail>().DestroyArrow();
            }
            else if (!tailReach && Input.GetKeyDown(collision.GetComponent<ArrowBehaviour>().keyArrow) && isPressed)
            {
                NoteHit(collision);
                isPressed = false;
                Debug.Log("DISTRUTTA NOTA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + isPressed);
            }
            else if (Input.anyKey == false) { }
            else if (!tailReach && !Input.GetKey(collision.GetComponent<ArrowBehaviour>().keyArrow))
            {
                Missed(collision);
                Destroy(collision.gameObject);
            }
        }        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(Time.timeScale == 1)
        {
            //Destroy the long note when the tail exit from the trigger
            if (collision.GetComponent<ArrowTail>())
            {
                Destroy(collision.transform.parent.gameObject);
                tailReach = false;
                longNotePress = false;
            }
            else if (!collision.GetComponent<ArrowBehaviour>().longNote || !longNotePress)
            {
                Missed(collision);
            }
        }        
    }

    private void Update()
    {
        if(Time.timeScale == 1)
        {
            isPressed = true;

            //If i'm still pressing the long note
            if (longNotePress)
            {
                fakeTriggerMask.enabled = true;
                if (HoldEffect == null)
                {
                    HoldEffect = Instantiate(holdEffect, triggerCircle);
                }

                if (Input.GetKeyUp(lastKey))
                {
                    Destroy(HoldEffect, 0.1f);
                    HoldEffect = null;
                    tailReach = false;
                    longNotePress = false;
                }
            }
            else
            {
                fakeTriggerMask.enabled = false;
                if (HoldEffect != null)
                {
                    Destroy(HoldEffect);
                }
            }


            //Managment of input if the state is attend input
            if (tutorialManager.state == TutorialManager.TutorialState.attendInput && arrow != null)
            {
                if (Input.GetKeyDown(arrow.keyArrow))
                {
                    if (arrow.longNote)
                    {
                        arrow.GetComponent<Rigidbody2D>().velocity = new Vector2(2, 0);
                        NoteHit(arrow.GetComponent<Collider2D>());
                        Hit();
                        StartCoroutine(tutorialManager.DeactiveTextBox());
                    }
                    else if (arrow.doubleNote && counterHit < 1)
                    {
                        counterHit++;
                        StartCoroutine(ResetCounterHit());
                    }
                    else
                    {
                        Hit();
                        counterHit = 0;
                        StartCoroutine(tutorialManager.DeactiveTextBox());
                    }
                }              
            }
        }
    }

    public void NoteHit(Collider2D collision)
    {
        lastKey = collision.GetComponent<ArrowBehaviour>().keyArrow;
        float triggerCentre = collision.transform.position.x;
        float thisCentre = transform.position.x;

        //Check if it's a long note
        if (collision.GetComponent<ArrowBehaviour>().longNote)
        {
            longNotePress = true;
            collision.GetComponent<Collider2D>().enabled = false;
            Vector3 arrowPosition = collision.transform.position;
            collision.transform.position = new Vector3(triggerCircle.position.x, arrowPosition.y, arrowPosition.z);
            collision.GetComponent<ArrowBehaviour>().TriggerLongNote(collision.transform);
        }
        else
        {
            Destroy(collision.gameObject);
        }
        //else if (collision.GetComponent<ArrowBehaviour>().doubleNote)
        //{
        //    DamageNote(collision);
        //}
        

        //Audio and Visual effects
        audioManager.PlayEffect(2);
        GameObject PressEffect = Instantiate(pressEffect, triggerCircle);
        GameObject StarEffect = Instantiate(starPerfect, triggerCircle.position, Quaternion.identity);
        Destroy(PressEffect, 1);
        Destroy(StarEffect, 0.5f);

        rowBehaviour.RowMidEffects();
        
        AddScore();
    }


    public void Hit()
    {
        if (!arrow.longNote)
        {
            Destroy(arrow.gameObject);
        }
        AddScore();
        tutorialManager.state = TutorialManager.TutorialState.spawnArrows;

        //Change phase of tutorial
        if (tutorialManager.indexPhase == 1)
        {
            tutorialManager.indexPhrase++;
            tutorialManager.phraseText.text = tutorialManager.phrases[tutorialManager.indexPhrase];
        }

        audioManager.PlayEffect(2);
        GameObject PressEffect = Instantiate(pressEffect, triggerCircle);
        GameObject StarEffect = Instantiate(starPerfect, triggerCircle.position, Quaternion.identity);
        Destroy(PressEffect, 1);
        Destroy(StarEffect, 0.5f);
        rowBehaviour.RowMidEffects();
    }

    //Reset the current phase
    public void Missed(Collider2D collision)
    {
        Debug.Log("Miss");
        audioManager.PlayEffect(3);
        tutorialManager.state = TutorialManager.TutorialState.missedArrow;
        ArrowBehaviour[] arrows = GameObject.FindObjectsOfType<ArrowBehaviour>();
        foreach(ArrowBehaviour arrow in arrows)
        {
            Destroy(arrow.gameObject);
        }
    }

    //Giving points every times consecutively while pressing the long note
    IEnumerator CoLongNotePoints()
    {
        while (longNotePress)
        {
            AddScore();
            yield return new WaitForSeconds(delayLongNote);
        }
    }

    //Reset the counter hit 
    IEnumerator ResetCounterHit()
    {
        yield return new WaitForSeconds(1);
        counterHit = 0;
    }

    //Add score to UI
    public void AddScore()
    {
        score += 50;
        scoreText.text = "Punteggio: " + score.ToString();
    }

    ////Hit the double or triple arrows
    //public void DamageNote(Collider2D collision)
    //{
    //    Debug.Log("takeDamege");
    //    Destroy(collision.gameObject);
    //}
}
