﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTail : MonoBehaviour
{
    public GameObject arrow;

    public void DestroyArrow()
    {
        Destroy(arrow);
    }
}
