﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Extra", menuName = "ScriptableObject/Data", order = 1)]
public class ExtraDescription : ScriptableObject
{
    public Sprite image;
}
