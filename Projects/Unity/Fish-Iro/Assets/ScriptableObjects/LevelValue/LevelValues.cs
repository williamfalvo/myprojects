﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "LevelData", menuName = "ScriptableObject/LevelData", order = 2)]
public class LevelValues : ScriptableObject
{
    [Space(5), Header("Notes Offsets (Rng bethween min/max + long || double || triple)")]
    [Range(0, 2f)] public float minNoteOffset;
    [Range(0, 2f)] public float maxNoteOffset;
    [Range(0, 2f)] public float longNoteOffset, longNote1Offset, longNote2Offset, doubleNoteOffset, tripleNoteOffset;

    [Space(5), Header("Arrow Velocity based on multiplier")]
    [Range(3f, 10f)] public float[] moveX;

    [Space(5), Header("Notes Number (min 4 Notes)")]
    [Range(0f, 10f)] public int minNotes;
    [Range(0f, 10f)] public int maxNotes;

    [Space(5), Header("Sprite BackGround")]
    public Sprite backGround;
}
