
tartaruga.png
size: 642,642
format: RGBA8888
filter: Linear,Linear
repeat: none
bolla1
  rotate: false
  xy: 366, 33
  size: 42, 42
  orig: 42, 42
  offset: 0, 0
  index: -1
bolla2
  rotate: false
  xy: 410, 44
  size: 32, 31
  orig: 32, 31
  offset: 0, 0
  index: -1
bolla3
  rotate: false
  xy: 366, 8
  size: 25, 23
  orig: 25, 23
  offset: 0, 0
  index: -1
bolla4
  rotate: false
  xy: 228, 96
  size: 32, 31
  orig: 32, 31
  offset: 0, 0
  index: -1
petrolio
  rotate: false
  xy: 228, 2
  size: 136, 73
  orig: 136, 73
  offset: 0, 0
  index: -1
pinna dx
  rotate: false
  xy: 474, 384
  size: 156, 256
  orig: 156, 256
  offset: 0, 0
  index: -1
pinna posteriore
  rotate: false
  xy: 2, 5
  size: 177, 122
  orig: 177, 122
  offset: 0, 0
  index: -1
pinna sx
  rotate: true
  xy: 268, 77
  size: 289, 184
  orig: 289, 184
  offset: 0, 0
  index: -1
plastica
  rotate: true
  xy: 454, 187
  size: 179, 158
  orig: 179, 158
  offset: 0, 0
  index: -1
tartaruga
  rotate: false
  xy: 2, 368
  size: 470, 272
  orig: 470, 272
  offset: 0, 0
  index: -1
teschio
  rotate: true
  xy: 454, 41
  size: 144, 147
  orig: 144, 147
  offset: 0, 0
  index: -1
testa
  rotate: false
  xy: 2, 129
  size: 264, 237
  orig: 264, 237
  offset: 0, 0
  index: -1
vomito
  rotate: false
  xy: 181, 15
  size: 45, 112
  orig: 45, 112
  offset: 0, 0
  index: -1
