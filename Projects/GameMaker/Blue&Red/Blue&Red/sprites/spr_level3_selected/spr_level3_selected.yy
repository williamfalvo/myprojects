{
    "id": "4ae70c7b-6b40-408d-bb27-8d125968793e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level3_selected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "596b43a3-2915-41c0-8636-12481f82d104",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ae70c7b-6b40-408d-bb27-8d125968793e",
            "compositeImage": {
                "id": "bcd1d80c-4511-47b6-9ea0-bdf85d137ea7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "596b43a3-2915-41c0-8636-12481f82d104",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbcb4713-87b4-4677-a79b-a8f38cfd95f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "596b43a3-2915-41c0-8636-12481f82d104",
                    "LayerId": "7a82925e-eccc-47aa-be30-55fcc518df27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "7a82925e-eccc-47aa-be30-55fcc518df27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ae70c7b-6b40-408d-bb27-8d125968793e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 80
}