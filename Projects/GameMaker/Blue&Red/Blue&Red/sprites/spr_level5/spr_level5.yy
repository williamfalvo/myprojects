{
    "id": "9db7202b-12d0-427b-ac93-169c6dd61dd7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4a15fa5-3211-46d7-9ed9-28fc384bb542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9db7202b-12d0-427b-ac93-169c6dd61dd7",
            "compositeImage": {
                "id": "9c9d2381-b82d-4bb1-a58c-baafd457d58e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4a15fa5-3211-46d7-9ed9-28fc384bb542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4715bee-4ef9-4bcc-85f2-3440a59874d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4a15fa5-3211-46d7-9ed9-28fc384bb542",
                    "LayerId": "edbe0521-8eff-49de-9356-1a49e95ffb89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "edbe0521-8eff-49de-9356-1a49e95ffb89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9db7202b-12d0-427b-ac93-169c6dd61dd7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 64
}