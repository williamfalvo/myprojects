{
    "id": "05ddeffc-b863-43ad-8889-7a114b994f64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_platform_move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e9153f1-df45-4b16-b5ee-a689c050326f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05ddeffc-b863-43ad-8889-7a114b994f64",
            "compositeImage": {
                "id": "6d9bed23-93f1-4528-816a-9aada26a5159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e9153f1-df45-4b16-b5ee-a689c050326f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa64b659-36d8-453c-8053-1a8b1cd3d7cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e9153f1-df45-4b16-b5ee-a689c050326f",
                    "LayerId": "0d16fe8d-074c-46dc-914c-bac477510de3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0d16fe8d-074c-46dc-914c-bac477510de3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05ddeffc-b863-43ad-8889-7a114b994f64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}