{
    "id": "29306c9f-5a12-4845-92fe-6052fc5c20b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level_text",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 27,
    "bbox_right": 101,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5f5dcd2-8fe1-48d5-900c-14877d4e8db3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29306c9f-5a12-4845-92fe-6052fc5c20b1",
            "compositeImage": {
                "id": "1d7feab6-d934-46e7-90f0-74f5655445a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5f5dcd2-8fe1-48d5-900c-14877d4e8db3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0147a21-cc9e-470b-a118-41805ab43b56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5f5dcd2-8fe1-48d5-900c-14877d4e8db3",
                    "LayerId": "74063410-903e-4cb9-ae95-3a06c6ee165e"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "74063410-903e-4cb9-ae95-3a06c6ee165e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29306c9f-5a12-4845-92fe-6052fc5c20b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 16
}