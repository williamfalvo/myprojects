{
    "id": "b79d03e9-d833-49fd-9829-6545b19612a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b943dcf-0a68-4529-a5ae-d3818e0784ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b79d03e9-d833-49fd-9829-6545b19612a4",
            "compositeImage": {
                "id": "9333006a-cf39-4b35-abfe-759ef9031a3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b943dcf-0a68-4529-a5ae-d3818e0784ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da47af37-1079-4226-b391-b991956d5168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b943dcf-0a68-4529-a5ae-d3818e0784ff",
                    "LayerId": "3cf0bb23-1a68-4b18-9cbc-c4806c78161d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3cf0bb23-1a68-4b18-9cbc-c4806c78161d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b79d03e9-d833-49fd-9829-6545b19612a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 64
}