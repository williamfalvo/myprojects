{
    "id": "f555e53f-114c-4e4a-9346-ab9b84f17051",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4b186fa-f91b-4d28-aadb-795b681fee69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f555e53f-114c-4e4a-9346-ab9b84f17051",
            "compositeImage": {
                "id": "3d61dbe6-cf9d-4bf7-81b2-273d10b433d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4b186fa-f91b-4d28-aadb-795b681fee69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f1a54e4-6d73-4b63-b8c4-967d72f02e8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4b186fa-f91b-4d28-aadb-795b681fee69",
                    "LayerId": "cdef0f98-b31a-4fd7-bdc8-d8acb3a3e166"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "cdef0f98-b31a-4fd7-bdc8-d8acb3a3e166",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f555e53f-114c-4e4a-9346-ab9b84f17051",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}