{
    "id": "621ddc99-d340-42f7-98f7-76275878d08d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8cad70d-012a-498a-835c-6519450c4b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "621ddc99-d340-42f7-98f7-76275878d08d",
            "compositeImage": {
                "id": "ef7b9255-1cbd-4e46-bb7f-fd764f784669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8cad70d-012a-498a-835c-6519450c4b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3d4b6d6-c90f-43b9-baf1-bdd1199cad83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8cad70d-012a-498a-835c-6519450c4b1c",
                    "LayerId": "4d9d1906-2a6e-408f-9d12-47eb0b15ae27"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "4d9d1906-2a6e-408f-9d12-47eb0b15ae27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "621ddc99-d340-42f7-98f7-76275878d08d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}