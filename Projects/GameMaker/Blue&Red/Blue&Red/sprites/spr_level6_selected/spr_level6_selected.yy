{
    "id": "fa6fe5e3-ead4-4e2f-a7d7-60e485ddc156",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level6_selected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33cba480-62b4-4ddd-945f-d888c27c4e20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6fe5e3-ead4-4e2f-a7d7-60e485ddc156",
            "compositeImage": {
                "id": "e413bb44-d5cd-4868-9afb-0e742adccbbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33cba480-62b4-4ddd-945f-d888c27c4e20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "554c123e-6a8c-41cd-aed1-b9ff18c96beb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33cba480-62b4-4ddd-945f-d888c27c4e20",
                    "LayerId": "37315d77-9e8a-4684-91f8-368990265dc4"
                }
            ]
        }
    ],
    "gridX": 5,
    "gridY": 5,
    "height": 160,
    "layers": [
        {
            "id": "37315d77-9e8a-4684-91f8-368990265dc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa6fe5e3-ead4-4e2f-a7d7-60e485ddc156",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 80
}