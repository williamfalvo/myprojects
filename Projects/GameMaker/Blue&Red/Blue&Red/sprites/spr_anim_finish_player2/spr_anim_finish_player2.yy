{
    "id": "0336d671-c581-427d-b5ca-b46546ec8706",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_anim_finish_player2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c353ae1-3940-4021-b3b9-3f7e50eea39f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "347eb546-7e05-49fa-a219-fa5fa9e96be9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c353ae1-3940-4021-b3b9-3f7e50eea39f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22a18b2e-cda7-418c-b091-5d5b54cf5ce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c353ae1-3940-4021-b3b9-3f7e50eea39f",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "b14f9372-94bc-489b-8682-d2e553c0ed24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "20e33401-f06a-4f14-b617-f02fd5b6f604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b14f9372-94bc-489b-8682-d2e553c0ed24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d4f2151-46eb-4ffd-9a3c-2d2667508ef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b14f9372-94bc-489b-8682-d2e553c0ed24",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "8f16f2b9-fdfb-4a21-922b-5432374114e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "702e0bbf-d499-4015-bb1b-f5543f0b7632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f16f2b9-fdfb-4a21-922b-5432374114e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2179dfbe-02d7-4d44-b814-3dbd282d43d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f16f2b9-fdfb-4a21-922b-5432374114e4",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "d23f8b13-18a2-40fc-9a14-ca4177822439",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "86475eac-c808-4477-a220-8fc14130bc57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d23f8b13-18a2-40fc-9a14-ca4177822439",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6895b3c2-de92-4ad6-b624-eb07dab18530",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d23f8b13-18a2-40fc-9a14-ca4177822439",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "d2e64480-23fa-4877-a2ba-89530226cfd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "4d9a48e6-2ee9-4ab2-8bfd-7524bb5c90dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2e64480-23fa-4877-a2ba-89530226cfd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e31b683-12ca-4013-882b-2f93d30c1e8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2e64480-23fa-4877-a2ba-89530226cfd6",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "6337bba6-ee27-4b46-98a6-e4c27333317c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "f950257c-c1db-4f64-a3ff-b4f4aa8e695b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6337bba6-ee27-4b46-98a6-e4c27333317c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e6e95ce-7a95-48dd-b076-2c1ea7f15955",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6337bba6-ee27-4b46-98a6-e4c27333317c",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "64aebc73-87d0-4572-aafd-c0398ab0dc01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "020e64dc-2067-4bcc-a5d3-e87db84af696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64aebc73-87d0-4572-aafd-c0398ab0dc01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a39e0664-609b-4398-91cf-f6af8db1c4ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64aebc73-87d0-4572-aafd-c0398ab0dc01",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "5439ebf5-c677-45fe-b53e-9ffe27717354",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "787aed08-9d0c-447d-a533-aa82b891d61d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5439ebf5-c677-45fe-b53e-9ffe27717354",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fd5aa1e-9770-4bd4-a60a-64f97902ba16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5439ebf5-c677-45fe-b53e-9ffe27717354",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "2182c67f-5252-4c49-ad21-ab1b6beb24b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "7e765d0b-a191-4e20-82d0-5f1fcb1925b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2182c67f-5252-4c49-ad21-ab1b6beb24b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "495faec1-c19d-4ff4-840e-192ff7d19ac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2182c67f-5252-4c49-ad21-ab1b6beb24b6",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "73f4eacb-8de2-4793-85f9-50740d04208a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "a257468c-69ac-437c-be64-d59019693a95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73f4eacb-8de2-4793-85f9-50740d04208a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d9633d7-3f2d-4249-be2d-3eb83e959392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73f4eacb-8de2-4793-85f9-50740d04208a",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "4dd0e794-0364-40f1-a2dd-dbfba5cddd9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "334be2fc-4797-41b1-9b67-47b4670c8475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dd0e794-0364-40f1-a2dd-dbfba5cddd9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66f92c22-4214-46af-8536-38839a3eb742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dd0e794-0364-40f1-a2dd-dbfba5cddd9b",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "6ec22265-9085-4ce9-b239-db912898a6b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "950fec23-0cd5-4b87-bc98-bdc22aae47cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ec22265-9085-4ce9-b239-db912898a6b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3d0724f-29f4-4419-beb0-9455348f28d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ec22265-9085-4ce9-b239-db912898a6b7",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "cbbf2810-c913-4fe3-910c-1cc234675c33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "d4b21f92-e1ab-4f4b-9e2e-147acd2bf4ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbbf2810-c913-4fe3-910c-1cc234675c33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ede1ce06-5809-44ae-b2aa-557ab0770731",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbbf2810-c913-4fe3-910c-1cc234675c33",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "67400996-4709-4023-8250-a2a137ade1dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "6e72d221-0f54-4710-86ce-8bb5bc5b57e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67400996-4709-4023-8250-a2a137ade1dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18ba7a4d-f83e-48de-9c22-b0f2b82b4575",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67400996-4709-4023-8250-a2a137ade1dc",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        },
        {
            "id": "1df7d430-37a7-4586-babe-6c676dcab9f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "compositeImage": {
                "id": "90f11bc1-c028-4613-b6b1-8629ffe16e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df7d430-37a7-4586-babe-6c676dcab9f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a093d49-052d-409a-b0e8-20ded48287a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df7d430-37a7-4586-babe-6c676dcab9f3",
                    "LayerId": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "197a3e44-f4d6-4f3a-8cc6-f0b957bedb9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0336d671-c581-427d-b5ca-b46546ec8706",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}