{
    "id": "98ebd5ff-fa41-46a4-8e8c-9eeebec2a281",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wasd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 13,
    "bbox_right": 83,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fea804ce-b6fe-47dd-a467-668ebe25bdd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98ebd5ff-fa41-46a4-8e8c-9eeebec2a281",
            "compositeImage": {
                "id": "77417617-1b9d-4530-9be1-1ac8fee542d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fea804ce-b6fe-47dd-a467-668ebe25bdd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8711806-a46b-4b9b-8d6d-bf5ae5dc3e8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fea804ce-b6fe-47dd-a467-668ebe25bdd0",
                    "LayerId": "4ab1c05e-35fc-42ec-b08e-2d800deb2c3b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4ab1c05e-35fc-42ec-b08e-2d800deb2c3b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98ebd5ff-fa41-46a4-8e8c-9eeebec2a281",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}