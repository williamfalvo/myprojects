{
    "id": "136e309a-4185-4bfc-a098-97db7bdac4b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e72432f0-8bde-47ce-801d-c1f84258a323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "136e309a-4185-4bfc-a098-97db7bdac4b5",
            "compositeImage": {
                "id": "ed2ca0d0-e4f5-4f04-9450-547a2baa360a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e72432f0-8bde-47ce-801d-c1f84258a323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4838f69b-539a-4c35-be39-d79b4b19a07d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e72432f0-8bde-47ce-801d-c1f84258a323",
                    "LayerId": "5599f03d-1279-440d-a07f-236064ab01b1"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "5599f03d-1279-440d-a07f-236064ab01b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "136e309a-4185-4bfc-a098-97db7bdac4b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}