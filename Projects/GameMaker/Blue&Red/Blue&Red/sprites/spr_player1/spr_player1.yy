{
    "id": "278976cd-618a-4ad1-a751-6f9cb3eb8012",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6135151-b2b4-4b85-b126-ec093aed9343",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "278976cd-618a-4ad1-a751-6f9cb3eb8012",
            "compositeImage": {
                "id": "764fced9-44df-420c-b0c7-7fe24e05bc22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6135151-b2b4-4b85-b126-ec093aed9343",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2acb6e7b-4406-48cd-9901-2e3e132197ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6135151-b2b4-4b85-b126-ec093aed9343",
                    "LayerId": "751e33df-4d61-4d8e-890e-eaa0d907ae26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "751e33df-4d61-4d8e-890e-eaa0d907ae26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "278976cd-618a-4ad1-a751-6f9cb3eb8012",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}