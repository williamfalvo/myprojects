{
    "id": "f604771b-53fd-41c0-9931-22b10f874833",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level8_selected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b60f12fa-c2f2-45c6-b95b-83393ee6eabe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f604771b-53fd-41c0-9931-22b10f874833",
            "compositeImage": {
                "id": "7f01442b-c8b2-484a-9359-5cdb6d555801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b60f12fa-c2f2-45c6-b95b-83393ee6eabe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d0f245b-e8c4-477f-b9d0-c79bfbc3c3eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b60f12fa-c2f2-45c6-b95b-83393ee6eabe",
                    "LayerId": "a7ec3fd7-dd49-411e-b1de-55cc0e9d162b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "a7ec3fd7-dd49-411e-b1de-55cc0e9d162b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f604771b-53fd-41c0-9931-22b10f874833",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 80
}