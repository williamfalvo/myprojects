{
    "id": "a90b9bfb-1160-4261-b044-73cdcc4d8e1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7df5564c-c282-48ab-b6bd-b8d7efa0f079",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a90b9bfb-1160-4261-b044-73cdcc4d8e1b",
            "compositeImage": {
                "id": "7c13b3f0-36c1-4ca2-ae45-99ee5d31cff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7df5564c-c282-48ab-b6bd-b8d7efa0f079",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32c11966-142e-4c61-9fff-e19ef5d4c038",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7df5564c-c282-48ab-b6bd-b8d7efa0f079",
                    "LayerId": "10c9823f-1615-406c-9f10-db711d6735d5"
                }
            ]
        }
    ],
    "gridX": 5,
    "gridY": 5,
    "height": 128,
    "layers": [
        {
            "id": "10c9823f-1615-406c-9f10-db711d6735d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a90b9bfb-1160-4261-b044-73cdcc4d8e1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 64
}