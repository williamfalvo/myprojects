{
    "id": "8517a90c-eee6-4dea-88ce-1424e57272f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trigger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ede2e412-c223-4c92-a3af-07e46e504dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8517a90c-eee6-4dea-88ce-1424e57272f1",
            "compositeImage": {
                "id": "256a59b7-2c86-40e7-bbb1-1f284198be5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ede2e412-c223-4c92-a3af-07e46e504dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5f80ecd-ad8a-4c56-a2e5-a7eb60b2ef9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ede2e412-c223-4c92-a3af-07e46e504dbd",
                    "LayerId": "1773cca8-6a79-4183-8149-b218c3809af7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1773cca8-6a79-4183-8149-b218c3809af7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8517a90c-eee6-4dea-88ce-1424e57272f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}