{
    "id": "c87acb03-b333-446f-a3a5-41b59038031b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_William",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 21,
    "bbox_right": 197,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a5d4ca2-f23f-4917-b11e-65c2d616d7b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c87acb03-b333-446f-a3a5-41b59038031b",
            "compositeImage": {
                "id": "355d5d52-c6a7-4bd9-8c79-1988b0360f30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a5d4ca2-f23f-4917-b11e-65c2d616d7b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09b752d0-b90c-4285-abe4-191d8c89ecea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a5d4ca2-f23f-4917-b11e-65c2d616d7b3",
                    "LayerId": "e00a890b-27ef-49ad-a77f-cdfc72b12cd0"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "e00a890b-27ef-49ad-a77f-cdfc72b12cd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c87acb03-b333-446f-a3a5-41b59038031b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 222,
    "xorig": 111,
    "yorig": 16
}