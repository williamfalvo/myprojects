{
    "id": "be0ebad1-8103-48b1-a092-9193247c2b98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player2_animation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a9187ce-95d4-414f-ac1f-4063b538c451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be0ebad1-8103-48b1-a092-9193247c2b98",
            "compositeImage": {
                "id": "a4ce090d-cbbe-42c3-8ee7-a4594e9ffa4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a9187ce-95d4-414f-ac1f-4063b538c451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c71ecc53-bae8-4bbc-8d78-ccf2c1396c4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a9187ce-95d4-414f-ac1f-4063b538c451",
                    "LayerId": "1d9ad18f-970c-4985-b664-8855c26dfe8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1d9ad18f-970c-4985-b664-8855c26dfe8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be0ebad1-8103-48b1-a092-9193247c2b98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}