{
    "id": "985e4535-c9bd-47ef-b9bc-cc56445412a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level9_selected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5487b486-eba5-4460-b6a5-a25bfa1a5e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "985e4535-c9bd-47ef-b9bc-cc56445412a9",
            "compositeImage": {
                "id": "825213c4-81a8-44b3-b50c-740d8d0ae8e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5487b486-eba5-4460-b6a5-a25bfa1a5e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf3bf054-399d-4cc5-bd67-62efbb06ab42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5487b486-eba5-4460-b6a5-a25bfa1a5e5c",
                    "LayerId": "fce88163-e85f-4659-b502-1254615e0183"
                }
            ]
        }
    ],
    "gridX": 5,
    "gridY": 5,
    "height": 160,
    "layers": [
        {
            "id": "fce88163-e85f-4659-b502-1254615e0183",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "985e4535-c9bd-47ef-b9bc-cc56445412a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 80
}