{
    "id": "eabb96a5-226f-45a8-8cf8-3231276dad79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_restart_text",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 15,
    "bbox_right": 293,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a00d4feb-b998-40c7-817e-3fac03930cac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eabb96a5-226f-45a8-8cf8-3231276dad79",
            "compositeImage": {
                "id": "a8cc276e-5047-4c4e-85b1-6bf59bbd5319",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a00d4feb-b998-40c7-817e-3fac03930cac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bc6203c-395a-4ed0-a41f-056ac3d830a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00d4feb-b998-40c7-817e-3fac03930cac",
                    "LayerId": "9b48ae6a-b5c7-482b-afc7-5a8aaa33d915"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "9b48ae6a-b5c7-482b-afc7-5a8aaa33d915",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eabb96a5-226f-45a8-8cf8-3231276dad79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 310,
    "xorig": 155,
    "yorig": 16
}