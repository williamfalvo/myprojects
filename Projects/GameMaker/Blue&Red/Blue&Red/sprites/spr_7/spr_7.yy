{
    "id": "6da42cd1-a457-4a46-bc25-fe93b8a8c6fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12971285-5bd8-4b9a-8520-71af55798bb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da42cd1-a457-4a46-bc25-fe93b8a8c6fe",
            "compositeImage": {
                "id": "667051f6-b14f-4968-ab12-030ca32603b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12971285-5bd8-4b9a-8520-71af55798bb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd299359-1727-4078-8a16-dd86ce5a69f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12971285-5bd8-4b9a-8520-71af55798bb4",
                    "LayerId": "8113636d-7fb4-4665-b4b9-9fecd9675e24"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "8113636d-7fb4-4665-b4b9-9fecd9675e24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6da42cd1-a457-4a46-bc25-fe93b8a8c6fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}