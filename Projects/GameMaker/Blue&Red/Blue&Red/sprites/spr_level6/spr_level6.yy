{
    "id": "ad53f992-8c44-4613-9801-c011025f58d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bca527d-e9bc-409d-8c90-5bbb82a38320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad53f992-8c44-4613-9801-c011025f58d3",
            "compositeImage": {
                "id": "6a936bb7-0576-4143-ac25-d89d091cad37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bca527d-e9bc-409d-8c90-5bbb82a38320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12dd9485-8be4-4ef3-88df-ca1c042e795c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bca527d-e9bc-409d-8c90-5bbb82a38320",
                    "LayerId": "012ef784-ece2-42dc-bc0d-252ba903136f"
                }
            ]
        }
    ],
    "gridX": 5,
    "gridY": 5,
    "height": 128,
    "layers": [
        {
            "id": "012ef784-ece2-42dc-bc0d-252ba903136f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad53f992-8c44-4613-9801-c011025f58d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 64
}