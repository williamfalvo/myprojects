{
    "id": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_end_level",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 14,
    "bbox_right": 16,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d23d22e6-8843-4a1c-a930-7fe034f14c50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "10dbdf91-c9d0-45ee-bcd4-2d2c8a57da49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d23d22e6-8843-4a1c-a930-7fe034f14c50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a42ee82-0131-4da7-8970-6c2b76d0f32b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d23d22e6-8843-4a1c-a930-7fe034f14c50",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "1acdb635-f2d5-4b04-bb93-129b76fcc3f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "1b40fb58-b5ac-4dca-83ba-af32561e3d2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1acdb635-f2d5-4b04-bb93-129b76fcc3f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "807107ae-2ff5-4a1a-a504-cbbe583d2f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1acdb635-f2d5-4b04-bb93-129b76fcc3f0",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "af6c0130-03ee-4f6c-a9b0-f0dff5061fe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "a49c4df3-1340-4fbf-b26e-d30d924d86eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af6c0130-03ee-4f6c-a9b0-f0dff5061fe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce8d6518-74e9-4b6f-af4d-166a67d920d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af6c0130-03ee-4f6c-a9b0-f0dff5061fe5",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "60d0ffe7-b413-4737-b1c4-bd0692545ff5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "f128c283-0f4b-4e5d-892d-f0fa9774783a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60d0ffe7-b413-4737-b1c4-bd0692545ff5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b66de3c8-62fb-4564-b9ad-57b3ac0d7135",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60d0ffe7-b413-4737-b1c4-bd0692545ff5",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "df3e120f-2a73-49fe-a6a7-0e0d8c7bd266",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "7ed72751-ddb9-4f19-81c9-6cb663e961eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df3e120f-2a73-49fe-a6a7-0e0d8c7bd266",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0ecd454-e498-4965-9854-7c67910a375d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df3e120f-2a73-49fe-a6a7-0e0d8c7bd266",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "f54782d5-e17c-4029-8a25-d15002078c1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "1b6da678-df82-42e4-ab39-aa4a3ef0e033",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f54782d5-e17c-4029-8a25-d15002078c1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1acac47c-002f-4eb2-826b-4483aff54ace",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f54782d5-e17c-4029-8a25-d15002078c1f",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "26c66d47-beaf-4577-8841-1cf047bc98e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "9478d182-5642-405f-bae1-3b9d9b73e996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26c66d47-beaf-4577-8841-1cf047bc98e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d2e2253-9982-4c38-9699-539e1d3b1626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26c66d47-beaf-4577-8841-1cf047bc98e2",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "0ac59e78-477b-4c03-9e19-65164caf920c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "d044ea15-cbf3-4adb-8e8f-2a39aace9952",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ac59e78-477b-4c03-9e19-65164caf920c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd98901d-7cd1-49b6-8fa6-da74d40c4975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ac59e78-477b-4c03-9e19-65164caf920c",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "69437340-83ea-4e65-bb8d-28f73d0013d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "a9a6ac52-2919-4e06-b444-c6c51decb352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69437340-83ea-4e65-bb8d-28f73d0013d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "299319e1-8f10-4647-b75b-005ecd4451b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69437340-83ea-4e65-bb8d-28f73d0013d7",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "d2fb086b-da6a-41b7-abe5-ab4ae5c7ea78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "7f8eb69c-d1c3-4d6a-82f3-5a1cdc430b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2fb086b-da6a-41b7-abe5-ab4ae5c7ea78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc58bd66-f388-4750-9baf-c2d58f321c20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2fb086b-da6a-41b7-abe5-ab4ae5c7ea78",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "87d3b920-ad98-453e-a768-9613897c1850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "d0cf0a51-c805-445d-b72b-a39de4012604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87d3b920-ad98-453e-a768-9613897c1850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fb0874b-0728-4dbb-b2bc-cfa40310b3cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87d3b920-ad98-453e-a768-9613897c1850",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "91a879dd-8b4a-4e7a-a086-71d056ad9411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "79aa89a9-312c-4a90-a7f8-c7e63e0865fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91a879dd-8b4a-4e7a-a086-71d056ad9411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04e66ede-d01f-42b7-a3ee-40c5576ca38b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91a879dd-8b4a-4e7a-a086-71d056ad9411",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "c1916789-95a3-4962-a23e-948a143a6bbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "d6ab644b-1287-4468-91a3-5c6440c45aa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1916789-95a3-4962-a23e-948a143a6bbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f616b4-43e4-4a1b-afca-552466aa842e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1916789-95a3-4962-a23e-948a143a6bbb",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "bd470de1-5575-410b-a9af-65c91718d78a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "66aed9e2-693b-47f3-a947-a0f6a3cb2923",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd470de1-5575-410b-a9af-65c91718d78a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09e2a3a4-ce9e-47a5-bff1-8999cb908d6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd470de1-5575-410b-a9af-65c91718d78a",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "a19f03a1-30f3-4bc1-a040-40b222c96c3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "2bd8dbd4-9955-4f62-aa7d-a266d01337e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a19f03a1-30f3-4bc1-a040-40b222c96c3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b6af7d9-403f-40ed-835c-0c4ef62d8b68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a19f03a1-30f3-4bc1-a040-40b222c96c3f",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "e3f64763-fdd7-4365-a8c5-7edfbf1675d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "83ecf51e-03a7-47a7-b7fd-324f14d1aeb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3f64763-fdd7-4365-a8c5-7edfbf1675d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae165cad-37b1-4fdf-a445-e2160b985fe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3f64763-fdd7-4365-a8c5-7edfbf1675d1",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "9c5a9168-c1ca-4681-b8a8-c59814877a0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "509e3e4b-ab45-466f-91fd-a02d26e4dd8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c5a9168-c1ca-4681-b8a8-c59814877a0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0d383d9-b2bf-4e53-8af5-5903445531c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c5a9168-c1ca-4681-b8a8-c59814877a0b",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "c65a0dd4-36e3-4f54-9ff8-c6bc7c25d694",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "c463da78-9227-447f-886e-758c94b110ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c65a0dd4-36e3-4f54-9ff8-c6bc7c25d694",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9000dfe5-200d-4454-a44d-cdd33e3e6583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c65a0dd4-36e3-4f54-9ff8-c6bc7c25d694",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "7d90acb5-863b-47b8-a451-4d60b2b7f0c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "5704be95-9cc3-497b-bd3f-e6e7c60c6a6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d90acb5-863b-47b8-a451-4d60b2b7f0c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d2007f8-ed85-4815-acfa-5ab12058b383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d90acb5-863b-47b8-a451-4d60b2b7f0c9",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "309120fd-39cd-48bf-948f-713baab31801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "8421e6ef-b152-4806-81a9-a6ef8ef03983",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "309120fd-39cd-48bf-948f-713baab31801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9399c7f9-4536-4f7b-8db2-c1df4d2e0ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "309120fd-39cd-48bf-948f-713baab31801",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "2ff80e73-3175-40b5-a73d-dd09a0160d48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "91005290-36bf-4986-a8cc-e3d564af685e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ff80e73-3175-40b5-a73d-dd09a0160d48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e735031-7664-401b-9ee4-259344cc5d06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ff80e73-3175-40b5-a73d-dd09a0160d48",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "54829bb1-9eb6-43ae-ab69-2d5981c013da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "ebf708a3-6c37-4591-a39c-584e46de1303",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54829bb1-9eb6-43ae-ab69-2d5981c013da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08912463-004c-4701-a3ef-89f73a13c102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54829bb1-9eb6-43ae-ab69-2d5981c013da",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "dca70bfc-c56d-4b83-a479-9b00c8d9b75f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "2aec057e-f3d6-4633-97d6-7563be69f091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dca70bfc-c56d-4b83-a479-9b00c8d9b75f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54808933-5a96-4fda-a107-23f563a559d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dca70bfc-c56d-4b83-a479-9b00c8d9b75f",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "565f4d5d-9193-4f8f-b470-90a25bbfd69f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "32907d2a-7932-4ce4-9fa8-96daf46594f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "565f4d5d-9193-4f8f-b470-90a25bbfd69f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27d50da9-4df8-4c83-8c9b-37f81cea7936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "565f4d5d-9193-4f8f-b470-90a25bbfd69f",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "65ba7fb1-2d63-4843-98ac-85049b4aa50d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "76c0d963-4c40-4a65-a83d-28347973ee22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65ba7fb1-2d63-4843-98ac-85049b4aa50d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21e2dcfe-efba-48d3-8a16-cc4b32f53428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65ba7fb1-2d63-4843-98ac-85049b4aa50d",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "4d0fe40e-84e7-432d-aac0-3541da40ec17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "f1936d6e-edfc-41cf-bdf6-4da9d86e7fb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d0fe40e-84e7-432d-aac0-3541da40ec17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48715314-5a7c-42b8-b65d-7f89ec9d8dfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d0fe40e-84e7-432d-aac0-3541da40ec17",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "d4457119-a419-4ca9-973b-3695c1400fe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "109f436f-d1c4-4a54-b961-4ddc67393d75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4457119-a419-4ca9-973b-3695c1400fe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80876b18-98ee-4b51-8eeb-dee4d8140508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4457119-a419-4ca9-973b-3695c1400fe8",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "3be7268e-bf64-46a5-b242-f60592024cdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "323da385-5129-45c7-87d3-38c13d9c8138",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3be7268e-bf64-46a5-b242-f60592024cdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36d0e329-e374-4b1e-b83b-0a307e0cca70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3be7268e-bf64-46a5-b242-f60592024cdc",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "a2ea77d9-1110-40a8-ae27-054cf00c944b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "63346efe-e474-41ed-a36f-5a58ba6e548a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2ea77d9-1110-40a8-ae27-054cf00c944b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3acc3306-ca55-4d82-8fbc-c3e2946a914f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2ea77d9-1110-40a8-ae27-054cf00c944b",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "2d462aff-2aa7-4682-b4a4-5641ee6144fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "df9c8763-8b85-4ec9-a1c8-bcb376780a02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d462aff-2aa7-4682-b4a4-5641ee6144fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e50a10f-4bd6-4a5a-a42f-a3a7a51708a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d462aff-2aa7-4682-b4a4-5641ee6144fc",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "c86f6830-64c7-4c89-8103-05597839667b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "2916dfda-b8d9-4e5a-8cbc-df81ce3fbba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c86f6830-64c7-4c89-8103-05597839667b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cee6ff4e-0f57-4d6a-b127-26428a143633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c86f6830-64c7-4c89-8103-05597839667b",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "9c2e530c-80d9-4e10-a5dd-def4d95d27df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "b2203252-f7b8-4db8-adcc-1aeecf51a4ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c2e530c-80d9-4e10-a5dd-def4d95d27df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92ee4307-5d39-49e7-8c74-03f6504ace28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c2e530c-80d9-4e10-a5dd-def4d95d27df",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "7542f156-4430-4e5a-9aa2-d0a2637bc035",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "82ef6f3d-706f-4e71-80ef-fb2e5ed03d9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7542f156-4430-4e5a-9aa2-d0a2637bc035",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9533ac4-8bc2-490d-8b7b-8e7190780bd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7542f156-4430-4e5a-9aa2-d0a2637bc035",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "6db9b8bc-a927-4529-8d0e-1602165f31ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "38509f8d-c5fa-4d1d-b83b-d9abb64f0f6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6db9b8bc-a927-4529-8d0e-1602165f31ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "968dafcb-191c-4410-935e-65b3c6f28971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6db9b8bc-a927-4529-8d0e-1602165f31ef",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "f4befc18-902d-4259-afce-1aefdfe4772e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "f82eec02-5522-438b-9d7e-c7af91af9146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4befc18-902d-4259-afce-1aefdfe4772e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f734e376-629e-4636-b442-39d98f84a504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4befc18-902d-4259-afce-1aefdfe4772e",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "4c249d40-bfd0-4bbe-b43d-e741937b9eb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "f422a3cd-8d01-46d0-bb2f-68932703cd6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c249d40-bfd0-4bbe-b43d-e741937b9eb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a5deb2c-961f-4308-ad67-b22b75955a00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c249d40-bfd0-4bbe-b43d-e741937b9eb1",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "2b3b5dc1-2234-4447-b269-b960099bc5b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "3557b214-af67-4d6a-9e58-ad2eee5b4cd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b3b5dc1-2234-4447-b269-b960099bc5b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b81ab8e5-530c-4dbf-b96d-a6ccdceac3ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b3b5dc1-2234-4447-b269-b960099bc5b6",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "cdf32a40-121a-4992-b7db-3ecd8559a737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "606f10c6-606d-4bf5-a2ea-f01a1f5386a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdf32a40-121a-4992-b7db-3ecd8559a737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab50b070-20c9-49c6-a682-95ebf8ae1504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdf32a40-121a-4992-b7db-3ecd8559a737",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "43f82a4f-9565-4e38-bbec-7c6ee31bf899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "f541c4f9-cc3a-45d4-9097-86bcba37c1ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43f82a4f-9565-4e38-bbec-7c6ee31bf899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6642efdc-d8a0-47e1-85d8-81717c5bcfdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43f82a4f-9565-4e38-bbec-7c6ee31bf899",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "6c3d68ce-d515-4581-bdf5-5b3ab4c92462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "b73f3477-9f23-4d75-bcf7-b040427b22a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c3d68ce-d515-4581-bdf5-5b3ab4c92462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17025a33-6429-454e-980b-5164db39f1e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c3d68ce-d515-4581-bdf5-5b3ab4c92462",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "25cec074-2033-4e80-b3f4-0969b1718efd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "d993def8-629f-420a-aa17-6a86f958c09f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25cec074-2033-4e80-b3f4-0969b1718efd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76594c6b-39f2-46f7-a014-240be92804bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25cec074-2033-4e80-b3f4-0969b1718efd",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "c40c3b6c-d62b-49c1-ba04-17f0fd203bcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "d28a44b0-4869-4e64-b669-72aa22d4c925",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c40c3b6c-d62b-49c1-ba04-17f0fd203bcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a53043f7-ac1c-43bb-bb6e-b97bde0fa096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c40c3b6c-d62b-49c1-ba04-17f0fd203bcb",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "8e6fe875-f2c3-4f88-98a8-734fe2352442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "c2d340ad-8bd0-49f6-a054-7c5121b01ee8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e6fe875-f2c3-4f88-98a8-734fe2352442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96e99bec-b82a-4874-845f-a421ad10706f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e6fe875-f2c3-4f88-98a8-734fe2352442",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "541fb9dd-e994-489f-9354-c0c2821ab333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "62172fce-ed04-49cc-98db-823141b4a217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "541fb9dd-e994-489f-9354-c0c2821ab333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6abe5f5-893e-4d59-92c2-d5c7574c1f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "541fb9dd-e994-489f-9354-c0c2821ab333",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "2ab3f70e-f98d-473d-8c48-d9cc17f79da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "f0710d2c-7897-404d-95e1-125f0033e548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ab3f70e-f98d-473d-8c48-d9cc17f79da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37922e0d-476f-425d-a6f6-552e959b5ea4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ab3f70e-f98d-473d-8c48-d9cc17f79da1",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "8fef8aac-b28f-43e6-b12a-b25e9c450e2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "283f7180-fdca-4ca8-a238-e496e97ae769",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fef8aac-b28f-43e6-b12a-b25e9c450e2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d9fcf39-f7d9-49bb-93a9-105d0cfc9851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fef8aac-b28f-43e6-b12a-b25e9c450e2f",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "249d247d-9368-405d-9e8a-81757d5d790a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "da2ec981-8faf-4abe-ad79-9ac13f9b6b03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "249d247d-9368-405d-9e8a-81757d5d790a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7411305a-84b4-4335-99fa-50e09ae77404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "249d247d-9368-405d-9e8a-81757d5d790a",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "39cb6b9c-7254-4c0b-8dff-b33aba75af52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "fc3dee53-a20a-428e-b4ba-40a8e18cc129",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39cb6b9c-7254-4c0b-8dff-b33aba75af52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77c9a790-eb8b-4497-9d3d-0ba4bb039b6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39cb6b9c-7254-4c0b-8dff-b33aba75af52",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "134b9cf4-1789-4dbc-9b4e-3a95d99178f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "b70e44c0-7001-4800-82dd-084fb177496e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "134b9cf4-1789-4dbc-9b4e-3a95d99178f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c4c68f1-a8b2-496b-8f6e-9d7b537ffbb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "134b9cf4-1789-4dbc-9b4e-3a95d99178f7",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "58dcce31-cc4a-4ec3-b7a0-c113e7be421e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "6942f575-df96-402d-a3da-057a9bf40d78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58dcce31-cc4a-4ec3-b7a0-c113e7be421e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8f174ef-61b6-40ec-804c-afc081648c39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58dcce31-cc4a-4ec3-b7a0-c113e7be421e",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "28edeed1-be82-4e19-b52b-a7cb53ae0958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "b8d3680a-b9a0-419a-b1dc-cfa810fec24c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28edeed1-be82-4e19-b52b-a7cb53ae0958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4e2d100-2bf3-4231-bbcc-ac0087eb0c55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28edeed1-be82-4e19-b52b-a7cb53ae0958",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "f6e598c8-59f5-4f45-9e87-92084c72f625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "81e37575-d677-4d98-af33-ffe6eba757dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6e598c8-59f5-4f45-9e87-92084c72f625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62349ce7-71ac-46a5-85e0-2eedaa4254b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6e598c8-59f5-4f45-9e87-92084c72f625",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "48639fb8-1e8e-4ab3-9b65-9b502953fad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "ec540851-6acd-484c-aa3c-f72ebd90e896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48639fb8-1e8e-4ab3-9b65-9b502953fad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d44cd50-08c2-44d7-8529-6ced998a7c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48639fb8-1e8e-4ab3-9b65-9b502953fad9",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "02ef937b-7487-423c-86b6-61359908746b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "fe31b262-d8c2-420f-ab94-3277c947c780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02ef937b-7487-423c-86b6-61359908746b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc0f87fd-d9b6-4a8a-9c7d-fa1e38b3fe17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02ef937b-7487-423c-86b6-61359908746b",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "36ff2688-c011-407c-8ce4-9975759a9765",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "2b92e32b-7c62-43d3-b849-3bb95fe5e08e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36ff2688-c011-407c-8ce4-9975759a9765",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69c7f7b9-39a6-41e1-be4c-c9dc1099b63f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36ff2688-c011-407c-8ce4-9975759a9765",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "7e09be97-ce68-4b3b-8e8f-8c2137a7281d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "cb506f6e-644b-42b0-8857-7fe48d49988f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e09be97-ce68-4b3b-8e8f-8c2137a7281d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6a28d8e-0753-4a78-81bc-9b5779ce6198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e09be97-ce68-4b3b-8e8f-8c2137a7281d",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "58cd8237-3ce2-41a7-96f5-8b5548d19fe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "f103d8b0-7f5a-4ddf-ba65-cbd7e6195ea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58cd8237-3ce2-41a7-96f5-8b5548d19fe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c40bcd1-c7b1-4193-a672-02edf1bf1d88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58cd8237-3ce2-41a7-96f5-8b5548d19fe6",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "77cd9388-933a-4cee-b61b-9f06883c43d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "e36191bd-2969-4e78-b22c-83b8c91f2a39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77cd9388-933a-4cee-b61b-9f06883c43d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae9ed3b5-f256-40d0-a02c-1cf3e31cf392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77cd9388-933a-4cee-b61b-9f06883c43d0",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "c6840456-608f-4645-b848-0776d66dcf53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "b7761316-f532-4046-a004-f3e8e4e3bd87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6840456-608f-4645-b848-0776d66dcf53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67b634ad-eb1b-4c5d-85fd-533c6fd20b49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6840456-608f-4645-b848-0776d66dcf53",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "0b6ce10c-69b0-4224-be5e-384849d46464",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "c88162a9-ad7e-4eb6-a479-768687c90aba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b6ce10c-69b0-4224-be5e-384849d46464",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55ad9933-015b-4ebc-b835-f3102633ef03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b6ce10c-69b0-4224-be5e-384849d46464",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "968f7b71-a4a0-4085-9054-b0da06047576",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "62b6295d-cf44-486c-a7ca-0cac8508a91e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "968f7b71-a4a0-4085-9054-b0da06047576",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cbb2a8e-67fb-42b1-9ab3-303be5f4c44f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "968f7b71-a4a0-4085-9054-b0da06047576",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "0528559a-e53b-4050-9f89-947163fba54e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "8aa7484c-cb13-48b0-8864-c5a7372259f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0528559a-e53b-4050-9f89-947163fba54e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97aecfc9-a741-41f2-8a66-4352df608ea5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0528559a-e53b-4050-9f89-947163fba54e",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "8c4f4fc2-ac20-4323-aa1f-be1170d78ca0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "24fa0239-87be-42bd-9df8-f526d81a797c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c4f4fc2-ac20-4323-aa1f-be1170d78ca0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaf8a778-06a9-48ea-b895-e669f8c94083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c4f4fc2-ac20-4323-aa1f-be1170d78ca0",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "2ecc5ed5-8d32-4746-94f7-ae5664402ce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "397004fb-6a33-4618-a48c-1058b075ba27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ecc5ed5-8d32-4746-94f7-ae5664402ce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ca1e471-d247-48b7-bb11-2063c9c49679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ecc5ed5-8d32-4746-94f7-ae5664402ce4",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "8c5e54d2-515b-4fae-ab8a-f98fbb548b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "c2e512db-5750-478a-a5da-dc9b08b9a797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c5e54d2-515b-4fae-ab8a-f98fbb548b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d7d94e2-1efb-4595-9beb-7042039b2e28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c5e54d2-515b-4fae-ab8a-f98fbb548b1c",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "ca6803cf-a6c8-4c7b-9aeb-cc411aa19e5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "4d9ec539-01a7-4328-a763-9193cfd3728d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca6803cf-a6c8-4c7b-9aeb-cc411aa19e5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43493915-1f25-42d2-ac12-bb9cd59d4c77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca6803cf-a6c8-4c7b-9aeb-cc411aa19e5a",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "623672a5-1655-4a45-bb14-3686a4c2c473",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "749d390b-9979-42af-975e-82442d86ceba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "623672a5-1655-4a45-bb14-3686a4c2c473",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf3056df-d53a-4209-925c-5e030fd4d3e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "623672a5-1655-4a45-bb14-3686a4c2c473",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "dc1e8635-92bc-442a-b0ee-662f33033ec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "1397d6b2-9722-48d8-825f-ee29f57b1f2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc1e8635-92bc-442a-b0ee-662f33033ec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8852ce1a-85a3-4b6d-9187-e985bc5c1054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc1e8635-92bc-442a-b0ee-662f33033ec2",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "d198af9f-75dd-42b9-a91d-486f86fd84a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "44cad4a6-5956-4a9d-9a14-02fea52abd55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d198af9f-75dd-42b9-a91d-486f86fd84a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1020d8eb-9855-42ba-ad1e-f16f8dc82215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d198af9f-75dd-42b9-a91d-486f86fd84a5",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "d8435259-2a9e-4637-9a04-4428a0c6cc71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "b6e1a854-94da-46f4-846a-e20986a5b0f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8435259-2a9e-4637-9a04-4428a0c6cc71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ad28d0d-46b1-4e0c-88a6-2050ef7ccd94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8435259-2a9e-4637-9a04-4428a0c6cc71",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "76553080-845f-4e52-a633-0d43425c5801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "f5270c93-3f5f-412f-901e-3e70fe3075e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76553080-845f-4e52-a633-0d43425c5801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92a0b350-cf00-4af3-aec3-840eafee4f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76553080-845f-4e52-a633-0d43425c5801",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "0df13b97-a6e2-4e84-a755-86a9a4f0def1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "2a04fc72-83aa-464e-80a2-2fe8ad47e88f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0df13b97-a6e2-4e84-a755-86a9a4f0def1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80355c6a-0c65-4c32-855e-88f8ca3dcad2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0df13b97-a6e2-4e84-a755-86a9a4f0def1",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "209a0130-be33-4cc5-bb02-3c3aeae5571d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "1a64ac57-a30b-4906-8c5c-cda89fe7b8c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "209a0130-be33-4cc5-bb02-3c3aeae5571d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeeb9dec-44a5-466a-9647-c1e20fa13dce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "209a0130-be33-4cc5-bb02-3c3aeae5571d",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "3fbce239-ef41-409a-9f19-e6f605e2654c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "85beda26-91aa-4791-80ed-23398ebf2c9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fbce239-ef41-409a-9f19-e6f605e2654c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0047af43-c5f4-4aa1-93d2-7d44bbcfbb1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fbce239-ef41-409a-9f19-e6f605e2654c",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "14aea4a0-7375-4ed4-86fa-07e030f6cbd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "a3578adb-bea9-4713-932b-aa45be3dfc89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14aea4a0-7375-4ed4-86fa-07e030f6cbd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3a14a46-666c-4bdc-8969-a5447f4e9446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14aea4a0-7375-4ed4-86fa-07e030f6cbd6",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "4abc2bff-ef82-40d1-8bcb-807893f9312f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "bde7b70b-29c2-4e53-824f-f0f72b0c1528",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4abc2bff-ef82-40d1-8bcb-807893f9312f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd558ebd-8b8c-49b4-b439-86a1b7d7e083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4abc2bff-ef82-40d1-8bcb-807893f9312f",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "c8cb7289-ab1c-4901-8f99-331393f994e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "ddbc1620-c64e-410e-ab6c-1c2e772e6ed3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8cb7289-ab1c-4901-8f99-331393f994e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db2a325f-fb11-4adf-9403-c0ec438e30f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8cb7289-ab1c-4901-8f99-331393f994e7",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "501c3206-6fd8-4980-9b19-3d36c1470648",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "5620c4d2-3f2b-45b3-95b6-71e4792b7330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "501c3206-6fd8-4980-9b19-3d36c1470648",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a52a262-14b7-4bb7-a96f-6da6b273b37e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "501c3206-6fd8-4980-9b19-3d36c1470648",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "c299df68-a10e-41c9-b816-f6c8a2ba418d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "d98b1649-1c21-4e2f-b0d6-12ed22b06574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c299df68-a10e-41c9-b816-f6c8a2ba418d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e413d9f-91d7-44b0-82e8-4ab0710c4fc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c299df68-a10e-41c9-b816-f6c8a2ba418d",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "3ac333ea-636d-4c91-a3ac-0750cdea1019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "4274b53e-e4c3-444e-9ca8-f24d9663d33c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ac333ea-636d-4c91-a3ac-0750cdea1019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbb3f110-7bc8-444d-874a-9b5c70825f31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ac333ea-636d-4c91-a3ac-0750cdea1019",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "ad0a871c-66ae-416d-ab7d-2e67670400a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "d51f7863-241a-4a44-a9ac-634a1179f95b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad0a871c-66ae-416d-ab7d-2e67670400a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdabfa48-4e04-4a66-aa6a-1a5c91a8a772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad0a871c-66ae-416d-ab7d-2e67670400a7",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "b4f869b6-9acc-4819-acf5-4779e6bf13e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "a1fc65dc-9827-47d9-acf9-f34d760485e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4f869b6-9acc-4819-acf5-4779e6bf13e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bae62654-f38f-45e5-9a2a-e8cf78ccb769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4f869b6-9acc-4819-acf5-4779e6bf13e9",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "19946d9c-8247-4658-a539-b09aca1e05bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "57a66a77-7326-44f0-ae88-5b79436bbbce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19946d9c-8247-4658-a539-b09aca1e05bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63169f7e-8b07-4740-ba42-c7b2177096eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19946d9c-8247-4658-a539-b09aca1e05bd",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "f63d901d-9d61-4382-bc0f-9fa66a184a46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "9797a818-5ef9-46c9-9054-432460438d7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f63d901d-9d61-4382-bc0f-9fa66a184a46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09c12505-bacf-4206-9528-dc3f504fdd24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f63d901d-9d61-4382-bc0f-9fa66a184a46",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        },
        {
            "id": "e60e741d-3daa-49b8-b87e-5a1e6f1cf2ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "compositeImage": {
                "id": "4cfd7f49-2ae2-40b6-b901-67b5d575f33a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e60e741d-3daa-49b8-b87e-5a1e6f1cf2ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85232c6d-81ac-46cf-8e57-458d25a448d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e60e741d-3daa-49b8-b87e-5a1e6f1cf2ba",
                    "LayerId": "bcb89a39-2e94-4330-8afc-21543708e10d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bcb89a39-2e94-4330-8afc-21543708e10d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a37e3f0-6d93-4aee-bb29-ff7934711074",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}