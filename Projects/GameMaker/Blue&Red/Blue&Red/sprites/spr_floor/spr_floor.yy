{
    "id": "a569f0de-abf9-4926-a875-8658b58ea24e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b91e1fc6-0c56-48c7-a918-7b1808934af3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a569f0de-abf9-4926-a875-8658b58ea24e",
            "compositeImage": {
                "id": "9cbf23fe-aeeb-4333-81bb-b6720339d213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b91e1fc6-0c56-48c7-a918-7b1808934af3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dd13a5f-bd08-4cc9-985c-b718981317aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b91e1fc6-0c56-48c7-a918-7b1808934af3",
                    "LayerId": "7c656b54-6591-4d4d-bcfc-1ca1bebee366"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7c656b54-6591-4d4d-bcfc-1ca1bebee366",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a569f0de-abf9-4926-a875-8658b58ea24e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}