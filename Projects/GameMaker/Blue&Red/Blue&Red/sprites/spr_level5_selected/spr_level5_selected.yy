{
    "id": "197732f3-e6d4-4e6d-81f3-7cd0817b4f73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level5_selected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bee98cb5-db28-4ce2-85df-358e7088c283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "197732f3-e6d4-4e6d-81f3-7cd0817b4f73",
            "compositeImage": {
                "id": "055a17c5-b1eb-46eb-8750-1f9f0304a679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bee98cb5-db28-4ce2-85df-358e7088c283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06fda447-bb97-41d3-ba6c-82b7a388ec22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bee98cb5-db28-4ce2-85df-358e7088c283",
                    "LayerId": "0822bdc1-f780-4c47-b5fe-742fbbb3136a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "0822bdc1-f780-4c47-b5fe-742fbbb3136a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "197732f3-e6d4-4e6d-81f3-7cd0817b4f73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 80
}