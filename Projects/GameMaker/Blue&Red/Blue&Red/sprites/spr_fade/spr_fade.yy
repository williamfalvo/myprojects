{
    "id": "8042c552-e732-439e-b07c-19a8efdbd3b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd96de88-2b5d-4c6a-9b80-b0ec2534f52a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8042c552-e732-439e-b07c-19a8efdbd3b3",
            "compositeImage": {
                "id": "0d72ae84-a416-4433-8aac-ac5b474a6e42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd96de88-2b5d-4c6a-9b80-b0ec2534f52a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80775486-e43b-43a7-81bc-b386323e9877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd96de88-2b5d-4c6a-9b80-b0ec2534f52a",
                    "LayerId": "16ae1bd5-c1cd-4575-be26-e91973aaa4e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "16ae1bd5-c1cd-4575-be26-e91973aaa4e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8042c552-e732-439e-b07c-19a8efdbd3b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}