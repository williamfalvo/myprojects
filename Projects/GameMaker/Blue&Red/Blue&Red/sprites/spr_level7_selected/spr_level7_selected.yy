{
    "id": "0ec5bba6-bbba-4c0f-a686-aae95e74aaee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level7_selected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f582aa4-36a7-4690-ba10-35e1e5a0b239",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ec5bba6-bbba-4c0f-a686-aae95e74aaee",
            "compositeImage": {
                "id": "dde700ba-0b17-4726-8183-31a89cd1b5d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f582aa4-36a7-4690-ba10-35e1e5a0b239",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90a1c1ac-f233-4514-849c-661b9d43afa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f582aa4-36a7-4690-ba10-35e1e5a0b239",
                    "LayerId": "79694b05-541c-4193-a7f4-5f766db335b5"
                }
            ]
        }
    ],
    "gridX": 5,
    "gridY": 5,
    "height": 160,
    "layers": [
        {
            "id": "79694b05-541c-4193-a7f4-5f766db335b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ec5bba6-bbba-4c0f-a686-aae95e74aaee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 80
}