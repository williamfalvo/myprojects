{
    "id": "334a2c87-ca5c-4103-a759-4cd3ed95ae4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b5843f8-6206-4837-9040-478137f2b406",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "334a2c87-ca5c-4103-a759-4cd3ed95ae4c",
            "compositeImage": {
                "id": "a77edfa4-168f-4149-bfea-8ae57220168a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b5843f8-6206-4837-9040-478137f2b406",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02c6fb01-7b8f-4be8-a840-59d291216a28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b5843f8-6206-4837-9040-478137f2b406",
                    "LayerId": "c5b51b35-bca2-42b6-9c3f-40ca852824d5"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "c5b51b35-bca2-42b6-9c3f-40ca852824d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "334a2c87-ca5c-4103-a759-4cd3ed95ae4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}