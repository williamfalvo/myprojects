{
    "id": "6d6ce92a-ffd8-4713-87cd-819003e21f26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level1_selected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9cb5725-8e64-44b4-a365-7e8368405af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d6ce92a-ffd8-4713-87cd-819003e21f26",
            "compositeImage": {
                "id": "1573cb9c-ca2c-4cbd-a4c8-18c96b3b53b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9cb5725-8e64-44b4-a365-7e8368405af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7103dbd1-647e-47df-9add-e47e99e616c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9cb5725-8e64-44b4-a365-7e8368405af7",
                    "LayerId": "967d3615-bc91-4ed5-91c5-fcb90e5891a4"
                }
            ]
        }
    ],
    "gridX": 5,
    "gridY": 5,
    "height": 160,
    "layers": [
        {
            "id": "967d3615-bc91-4ed5-91c5-fcb90e5891a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d6ce92a-ffd8-4713-87cd-819003e21f26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 80
}