{
    "id": "fad32d1e-9b03-44de-b4d3-fcd9d4b71618",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20db9766-e1f0-488c-914a-f7e41e6691db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fad32d1e-9b03-44de-b4d3-fcd9d4b71618",
            "compositeImage": {
                "id": "6d956ae3-b96b-4b5c-89ee-d1b5c3120683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20db9766-e1f0-488c-914a-f7e41e6691db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e767178-331e-4213-95d7-86a1e46999c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20db9766-e1f0-488c-914a-f7e41e6691db",
                    "LayerId": "137638a6-0476-4c8c-9e85-109faa490a87"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "137638a6-0476-4c8c-9e85-109faa490a87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fad32d1e-9b03-44de-b4d3-fcd9d4b71618",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}