{
    "id": "f465aba2-b1b5-4370-94f4-37fd7671217a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_leva_platform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68c3f2fc-d1e3-452b-9088-4f254929f4c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f465aba2-b1b5-4370-94f4-37fd7671217a",
            "compositeImage": {
                "id": "80a56385-2f2d-49c0-bc1f-15644fddb324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68c3f2fc-d1e3-452b-9088-4f254929f4c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7a7839d-627f-4c90-80ef-26283f424658",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68c3f2fc-d1e3-452b-9088-4f254929f4c3",
                    "LayerId": "a75b0db2-fc40-41b8-8e2b-3f54c7aa0dec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a75b0db2-fc40-41b8-8e2b-3f54c7aa0dec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f465aba2-b1b5-4370-94f4-37fd7671217a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}