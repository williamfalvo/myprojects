{
    "id": "4f8eb28d-30b9-4711-b610-e8ad66902b41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player1_animation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f995b456-6e58-4787-b938-b5c2aa91ad21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f8eb28d-30b9-4711-b610-e8ad66902b41",
            "compositeImage": {
                "id": "16395cac-937a-48b7-8496-e0cae970a839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f995b456-6e58-4787-b938-b5c2aa91ad21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9635ce1e-a761-4e3b-9f57-253bcfb4b063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f995b456-6e58-4787-b938-b5c2aa91ad21",
                    "LayerId": "3c9fedcb-a6a9-4825-a560-5d78a2b0fa73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3c9fedcb-a6a9-4825-a560-5d78a2b0fa73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f8eb28d-30b9-4711-b610-e8ad66902b41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}