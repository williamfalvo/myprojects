{
    "id": "1b90870d-302b-4c40-864e-c02a79988220",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_leva_player1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c7350cb-259b-4c51-b9e7-83a17b0d2d46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b90870d-302b-4c40-864e-c02a79988220",
            "compositeImage": {
                "id": "95f0fc21-004a-4c0d-a09b-f60288f76877",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c7350cb-259b-4c51-b9e7-83a17b0d2d46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "538a2f2c-b584-429c-89e2-7fa2a11c00f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c7350cb-259b-4c51-b9e7-83a17b0d2d46",
                    "LayerId": "6aa606e7-d69c-4a3e-8505-f3993c9a9b14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6aa606e7-d69c-4a3e-8505-f3993c9a9b14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b90870d-302b-4c40-864e-c02a79988220",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}