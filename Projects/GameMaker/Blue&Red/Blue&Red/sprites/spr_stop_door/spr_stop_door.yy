{
    "id": "31e39f19-61d4-4450-bfd5-32b465979d7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stop_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c765dd2d-e6e9-46ba-9e73-5a524a387f4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31e39f19-61d4-4450-bfd5-32b465979d7b",
            "compositeImage": {
                "id": "5038fa64-a099-49fc-90cf-2fcc36d324f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c765dd2d-e6e9-46ba-9e73-5a524a387f4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f9caa24-cca3-4237-900e-aceb28b8ba98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c765dd2d-e6e9-46ba-9e73-5a524a387f4b",
                    "LayerId": "fe8525a6-4af3-4e3f-9c3d-09b605617b41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fe8525a6-4af3-4e3f-9c3d-09b605617b41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31e39f19-61d4-4450-bfd5-32b465979d7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}