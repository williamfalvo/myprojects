{
    "id": "4c3859e0-8944-456f-b84a-c2001ea096d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_frecce",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 72,
    "bbox_left": 10,
    "bbox_right": 87,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c8cce44-3666-4167-84be-348f5afc7e11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c3859e0-8944-456f-b84a-c2001ea096d1",
            "compositeImage": {
                "id": "aed1656c-a3e6-4ff1-8936-11b527556907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c8cce44-3666-4167-84be-348f5afc7e11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47ff4c68-51a7-49be-ab70-09dac2a87d1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c8cce44-3666-4167-84be-348f5afc7e11",
                    "LayerId": "d933d2d9-c021-44e4-8319-4aed8f3dd028"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "d933d2d9-c021-44e4-8319-4aed8f3dd028",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c3859e0-8944-456f-b84a-c2001ea096d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}