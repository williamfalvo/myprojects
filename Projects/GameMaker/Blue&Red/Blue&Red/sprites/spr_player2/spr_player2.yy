{
    "id": "6b1f73c8-534a-41b2-bbc4-90f345f544e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5b7ef12-4a88-4f60-ae96-928f7d0c35a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b1f73c8-534a-41b2-bbc4-90f345f544e9",
            "compositeImage": {
                "id": "158848c3-35ab-4c6c-a2f2-62c916e9103d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5b7ef12-4a88-4f60-ae96-928f7d0c35a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "340d3fd8-cc48-44d0-8317-84904cb583d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5b7ef12-4a88-4f60-ae96-928f7d0c35a9",
                    "LayerId": "2c41e936-3925-4bae-9b01-ba62fcc72b6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2c41e936-3925-4bae-9b01-ba62fcc72b6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b1f73c8-534a-41b2-bbc4-90f345f544e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}