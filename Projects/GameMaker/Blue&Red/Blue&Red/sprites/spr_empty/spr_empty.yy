{
    "id": "ba7da6e3-c52a-4479-a359-1e4023eb5bba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_empty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41ca1537-a984-41be-b90e-e767b77c2ac8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba7da6e3-c52a-4479-a359-1e4023eb5bba",
            "compositeImage": {
                "id": "680a6315-7afd-4bee-8557-7d310011d4c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41ca1537-a984-41be-b90e-e767b77c2ac8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e0b1e21-e9b3-48c8-a01b-7d01330ffe0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41ca1537-a984-41be-b90e-e767b77c2ac8",
                    "LayerId": "fafcd5e2-c64f-46c0-b86a-fa8d782ca785"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fafcd5e2-c64f-46c0-b86a-fa8d782ca785",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba7da6e3-c52a-4479-a359-1e4023eb5bba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}