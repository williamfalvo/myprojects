{
    "id": "30612450-f2be-4a84-98a1-a7ff7b0fd5c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0f188ef-06d1-43d9-93f9-11cfd7e461e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30612450-f2be-4a84-98a1-a7ff7b0fd5c9",
            "compositeImage": {
                "id": "d6c7d1fd-af80-4b1d-8d3d-cb28cb9d7b15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0f188ef-06d1-43d9-93f9-11cfd7e461e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aba9af12-bc19-479e-a929-a9ad04c0b1b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0f188ef-06d1-43d9-93f9-11cfd7e461e1",
                    "LayerId": "a942c9ac-a713-4e47-a3cb-9525a2d3896e"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "a942c9ac-a713-4e47-a3cb-9525a2d3896e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30612450-f2be-4a84-98a1-a7ff7b0fd5c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}