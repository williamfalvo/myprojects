{
    "id": "941f32a9-5146-438c-acc3-851a114721ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4a3a31c-a95c-43ca-8a1c-55fc80e59704",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "941f32a9-5146-438c-acc3-851a114721ca",
            "compositeImage": {
                "id": "74d3a1f3-932d-4a38-83fb-755c09cca9f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4a3a31c-a95c-43ca-8a1c-55fc80e59704",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8384b4a-6cc2-456e-9ecd-467ce4337edf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4a3a31c-a95c-43ca-8a1c-55fc80e59704",
                    "LayerId": "30e1ad84-6588-42e7-9219-7cd276bb51c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "30e1ad84-6588-42e7-9219-7cd276bb51c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "941f32a9-5146-438c-acc3-851a114721ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 64
}