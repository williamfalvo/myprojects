{
    "id": "5884c80c-ea23-498c-a9db-613a17086af4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level4_selected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6feebeb-8055-4b85-a2fb-771722360847",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5884c80c-ea23-498c-a9db-613a17086af4",
            "compositeImage": {
                "id": "520044d4-91ef-429e-844e-e111c3aac057",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6feebeb-8055-4b85-a2fb-771722360847",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6807c186-95eb-4368-87b5-d064e2a9fddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6feebeb-8055-4b85-a2fb-771722360847",
                    "LayerId": "f5c50acc-a969-45d9-9fa8-602c29b75243"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "f5c50acc-a969-45d9-9fa8-602c29b75243",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5884c80c-ea23-498c-a9db-613a17086af4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 80
}