{
    "id": "71d8093e-bd2f-46d4-bf2f-f3093d55b0fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level2_selected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74fd304a-2cda-4875-be96-9fa1199b1d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71d8093e-bd2f-46d4-bf2f-f3093d55b0fd",
            "compositeImage": {
                "id": "b787d03f-0a57-404d-b66c-9e67162efa57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74fd304a-2cda-4875-be96-9fa1199b1d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be02b3b8-a0e2-4910-bde6-3755669e3290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74fd304a-2cda-4875-be96-9fa1199b1d1f",
                    "LayerId": "e17f0942-f5a0-4283-88fa-bb26229ee9fa"
                }
            ]
        }
    ],
    "gridX": 5,
    "gridY": 5,
    "height": 160,
    "layers": [
        {
            "id": "e17f0942-f5a0-4283-88fa-bb26229ee9fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71d8093e-bd2f-46d4-bf2f-f3093d55b0fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 80
}