{
    "id": "dd50e7ae-39bc-43b2-ab82-922020874044",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cc81c5e-75b3-49b0-8906-3ce6a3b8adb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd50e7ae-39bc-43b2-ab82-922020874044",
            "compositeImage": {
                "id": "97b63277-9d40-4e75-8919-233c3874e7cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cc81c5e-75b3-49b0-8906-3ce6a3b8adb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6709754-9bac-46ad-96d4-e42e3477c0c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc81c5e-75b3-49b0-8906-3ce6a3b8adb5",
                    "LayerId": "e0eaf027-dd97-4fbe-af4f-e72cf7aad65c"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "e0eaf027-dd97-4fbe-af4f-e72cf7aad65c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd50e7ae-39bc-43b2-ab82-922020874044",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}