{
    "id": "bc104c43-5240-4050-8493-45a9b2ad3a91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_restart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 15,
    "bbox_right": 108,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8467e177-2d3f-4f97-8b12-8aa73585044f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc104c43-5240-4050-8493-45a9b2ad3a91",
            "compositeImage": {
                "id": "d509d30f-3ac4-4ca3-9503-d3f7f8ddde8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8467e177-2d3f-4f97-8b12-8aa73585044f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3be2fd49-1934-43ab-89b3-6d4d93905acd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8467e177-2d3f-4f97-8b12-8aa73585044f",
                    "LayerId": "ba6d02ae-78da-47ef-8674-854eabd08817"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ba6d02ae-78da-47ef-8674-854eabd08817",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc104c43-5240-4050-8493-45a9b2ad3a91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 32
}