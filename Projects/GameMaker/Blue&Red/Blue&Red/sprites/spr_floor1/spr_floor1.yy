{
    "id": "03783c91-45c6-47ba-a664-2400b26130f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floor1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f6d72f1-b272-40d5-a5f8-c26659841c2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03783c91-45c6-47ba-a664-2400b26130f2",
            "compositeImage": {
                "id": "c82b27de-0151-4f18-96cd-9caad4e87470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f6d72f1-b272-40d5-a5f8-c26659841c2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "674c31c0-1041-43aa-924f-463bcc8df322",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f6d72f1-b272-40d5-a5f8-c26659841c2f",
                    "LayerId": "89dc5ff2-3e0e-403a-bd88-21c461027915"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "89dc5ff2-3e0e-403a-bd88-21c461027915",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03783c91-45c6-47ba-a664-2400b26130f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}