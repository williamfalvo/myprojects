{
    "id": "c2eea1e7-db94-437c-aa40-7d0df2f607c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79884388-4173-4984-8fea-5ee25a418134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2eea1e7-db94-437c-aa40-7d0df2f607c4",
            "compositeImage": {
                "id": "f859e9e3-8655-4f0d-8d32-c9ab476805ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79884388-4173-4984-8fea-5ee25a418134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78c1ed3e-ae4a-4a6e-a5ee-e59a732c67c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79884388-4173-4984-8fea-5ee25a418134",
                    "LayerId": "893c953c-5e49-4974-b422-65468dadfc83"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "893c953c-5e49-4974-b422-65468dadfc83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2eea1e7-db94-437c-aa40-7d0df2f607c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}