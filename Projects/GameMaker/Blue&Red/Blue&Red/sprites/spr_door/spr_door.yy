{
    "id": "b50aab55-b5d7-4c21-a379-4aaae3952a7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72fb951f-0e73-4086-a5ff-69d517569342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b50aab55-b5d7-4c21-a379-4aaae3952a7b",
            "compositeImage": {
                "id": "b1caddcf-efd4-4906-86fb-1f1732f42361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72fb951f-0e73-4086-a5ff-69d517569342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07f2c307-6429-4c7b-a8a4-057e876e03e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72fb951f-0e73-4086-a5ff-69d517569342",
                    "LayerId": "68e0f768-a833-4847-948d-041a67143358"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "68e0f768-a833-4847-948d-041a67143358",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b50aab55-b5d7-4c21-a379-4aaae3952a7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}