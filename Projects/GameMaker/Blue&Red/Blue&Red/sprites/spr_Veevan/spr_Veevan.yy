{
    "id": "c98f674a-6f17-4c69-a968-4bb4757b3753",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Veevan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 3,
    "bbox_right": 272,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "993e6535-a7c7-4407-8bb8-1c9e35f60331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98f674a-6f17-4c69-a968-4bb4757b3753",
            "compositeImage": {
                "id": "c0afb6fd-2038-4ea8-b61a-e1001a603488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "993e6535-a7c7-4407-8bb8-1c9e35f60331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff9b60d-8a85-4523-816d-4641f2c6e71f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "993e6535-a7c7-4407-8bb8-1c9e35f60331",
                    "LayerId": "ace7beb3-cc67-4093-8209-da94334a09cb"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "ace7beb3-cc67-4093-8209-da94334a09cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c98f674a-6f17-4c69-a968-4bb4757b3753",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 275,
    "xorig": 137,
    "yorig": 16
}