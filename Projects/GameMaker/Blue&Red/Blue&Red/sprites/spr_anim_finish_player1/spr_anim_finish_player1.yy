{
    "id": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_anim_finish_player1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6445958-aee4-4e90-b5a8-be323619b052",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "3b49bf70-b34c-410e-97af-ac37e85a9b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6445958-aee4-4e90-b5a8-be323619b052",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0906069f-7f74-4bd4-9638-1f9970c61119",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6445958-aee4-4e90-b5a8-be323619b052",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "4b5304b3-fc71-45bc-8697-6819954c42a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "a018ebc0-f4be-4a4c-b66f-745b81c6fc7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b5304b3-fc71-45bc-8697-6819954c42a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edb54120-e76e-4146-aa49-1304d68efb4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b5304b3-fc71-45bc-8697-6819954c42a1",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "00025d7b-eb9b-494f-b534-ea00a334f39a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "51d485ba-60e0-4f35-a861-fe6c6f81f369",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00025d7b-eb9b-494f-b534-ea00a334f39a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f45eb42d-f9ff-4416-8ae1-a20e2be0673e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00025d7b-eb9b-494f-b534-ea00a334f39a",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "91931360-4d86-4842-b7af-6f01148bd96a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "017ed3a8-c6f3-4be1-84b7-b622fe88bc3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91931360-4d86-4842-b7af-6f01148bd96a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89893183-77fa-4c42-95e1-e7a1a80ce5db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91931360-4d86-4842-b7af-6f01148bd96a",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "b8ea0e53-1118-4434-a246-f75a6b9b60a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "85e6686b-4bb0-4c4c-8d91-21bf4ea2a7d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8ea0e53-1118-4434-a246-f75a6b9b60a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ef897d7-4793-4f5e-b34f-8d7e87807a72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8ea0e53-1118-4434-a246-f75a6b9b60a5",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "481df48e-b721-4081-876a-56b1feda5616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "2b217b20-6f4d-4a82-aaf8-80e153ab9cd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "481df48e-b721-4081-876a-56b1feda5616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8620df83-6ec6-4fde-9f43-9eff4b5facde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "481df48e-b721-4081-876a-56b1feda5616",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "b7f026e8-720f-44da-af80-d9f175b8d7c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "db52204c-ce72-4eee-9edf-132d73bf1371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7f026e8-720f-44da-af80-d9f175b8d7c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "959a8385-82ab-4fdc-ba8e-efe7bb0bc97a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7f026e8-720f-44da-af80-d9f175b8d7c2",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "fb109390-da44-46c2-8813-3e67e3533e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "b2988a9f-d887-492a-9b2c-3581933051ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb109390-da44-46c2-8813-3e67e3533e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a34c96b0-66de-4641-b0dc-b3d6e8285140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb109390-da44-46c2-8813-3e67e3533e27",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "028761b7-b9fa-4b54-8fd8-a640a05df124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "435589e1-b178-42dc-9a72-d363189681b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "028761b7-b9fa-4b54-8fd8-a640a05df124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5a0f65c-78b8-4577-ad87-90b043b3c1b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "028761b7-b9fa-4b54-8fd8-a640a05df124",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "5bb0bd01-95c7-47ce-a4d0-b1a8c778cc36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "cbb156f1-6481-4cd0-8669-b6477ab0bec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bb0bd01-95c7-47ce-a4d0-b1a8c778cc36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a45f1887-6c09-4fe4-8a16-e9cc75e749be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bb0bd01-95c7-47ce-a4d0-b1a8c778cc36",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "48614cc4-1cc5-463f-9881-85fdb66a2205",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "0df3fa81-d692-4a55-9417-b121d39697bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48614cc4-1cc5-463f-9881-85fdb66a2205",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2de3d03d-56b1-4005-ac07-fda9f0aa591c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48614cc4-1cc5-463f-9881-85fdb66a2205",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "8338c8d8-bebb-416b-b073-35116c25d44d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "d20e0d40-0e76-4f94-80bb-dac65fa37338",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8338c8d8-bebb-416b-b073-35116c25d44d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18434b98-1af4-4ca3-95a7-12010194eeb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8338c8d8-bebb-416b-b073-35116c25d44d",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "0d6b8bbb-7bf8-4832-8c1c-d34e7757d19a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "e42d1568-2560-4763-b101-e507c025d159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d6b8bbb-7bf8-4832-8c1c-d34e7757d19a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1592b07-635d-47d3-b22d-7b6b06dc2a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d6b8bbb-7bf8-4832-8c1c-d34e7757d19a",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "a8827072-bba4-438f-8f29-801cb4c37f12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "b087d8e9-a8a3-47f9-8bdf-6ab6dc0430bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8827072-bba4-438f-8f29-801cb4c37f12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54d24f66-bba1-490c-b377-363b3931a259",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8827072-bba4-438f-8f29-801cb4c37f12",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        },
        {
            "id": "65282f93-13e5-4740-b6d9-4e326353076b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "compositeImage": {
                "id": "9aeb0964-1c87-48c0-bd13-151c0bf6be56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65282f93-13e5-4740-b6d9-4e326353076b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "583fad8c-5eaa-48f8-afce-0b9c2330f667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65282f93-13e5-4740-b6d9-4e326353076b",
                    "LayerId": "72a51fe5-5f0e-437c-961a-184d45e7513d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "72a51fe5-5f0e-437c-961a-184d45e7513d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5fe3445-c1b0-4f5b-9521-c57cb4f65cb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}