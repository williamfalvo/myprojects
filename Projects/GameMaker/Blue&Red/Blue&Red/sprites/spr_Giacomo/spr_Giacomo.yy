{
    "id": "543131c6-092f-4ab5-a6af-2218fd1b9d5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Giacomo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 6,
    "bbox_right": 245,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95df706f-6ed0-4a1c-a7b6-e3ff6bc891ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "543131c6-092f-4ab5-a6af-2218fd1b9d5a",
            "compositeImage": {
                "id": "38ad22b2-d60e-484c-916e-b47549a19d5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95df706f-6ed0-4a1c-a7b6-e3ff6bc891ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77b88d04-0080-4cbe-90e0-e6b50313e850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95df706f-6ed0-4a1c-a7b6-e3ff6bc891ba",
                    "LayerId": "568efa2b-32d9-4a0e-b9fc-6a002d9cf90e"
                }
            ]
        }
    ],
    "gridX": 3,
    "gridY": 3,
    "height": 32,
    "layers": [
        {
            "id": "568efa2b-32d9-4a0e-b9fc-6a002d9cf90e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "543131c6-092f-4ab5-a6af-2218fd1b9d5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 253,
    "xorig": 126,
    "yorig": 16
}