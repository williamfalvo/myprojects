{
    "id": "630fae3f-18d6-4b40-9156-c11728c46ccf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_level9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6bee711-c444-40af-8638-1926fe6fa63f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630fae3f-18d6-4b40-9156-c11728c46ccf",
            "compositeImage": {
                "id": "39eecdd0-c541-4976-8dcb-6813e231d284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6bee711-c444-40af-8638-1926fe6fa63f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef8b446d-5586-4e12-8382-ee09726b0b2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6bee711-c444-40af-8638-1926fe6fa63f",
                    "LayerId": "97f5678b-4056-4ead-b9f9-4ca5b8aca3c4"
                }
            ]
        }
    ],
    "gridX": 5,
    "gridY": 5,
    "height": 128,
    "layers": [
        {
            "id": "97f5678b-4056-4ead-b9f9-4ca5b8aca3c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "630fae3f-18d6-4b40-9156-c11728c46ccf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 64
}