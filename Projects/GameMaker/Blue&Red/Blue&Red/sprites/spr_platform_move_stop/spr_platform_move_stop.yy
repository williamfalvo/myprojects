{
    "id": "60ada2da-ee51-4cac-bf58-a861a9fac787",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_platform_move_stop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7796ccd-c6cd-48ce-9d72-cecec7ef7462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60ada2da-ee51-4cac-bf58-a861a9fac787",
            "compositeImage": {
                "id": "fbc0d064-0c94-4537-814f-8db2e8850299",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7796ccd-c6cd-48ce-9d72-cecec7ef7462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eaab1c6-042e-4989-924f-20d94fbe262d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7796ccd-c6cd-48ce-9d72-cecec7ef7462",
                    "LayerId": "7317e43e-f0cd-48d9-ab34-29fef3486916"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7317e43e-f0cd-48d9-ab34-29fef3486916",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60ada2da-ee51-4cac-bf58-a861a9fac787",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}