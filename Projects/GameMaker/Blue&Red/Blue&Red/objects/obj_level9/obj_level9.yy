{
    "id": "f06440ea-c01b-45e4-bd82-4e32c9373c33",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level9",
    "eventList": [
        {
            "id": "cb5bf24a-00b2-4ce0-905a-c341022c1df8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f06440ea-c01b-45e4-bd82-4e32c9373c33"
        },
        {
            "id": "99f35fac-2611-4992-82c4-d6e7192f236a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f06440ea-c01b-45e4-bd82-4e32c9373c33"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "630fae3f-18d6-4b40-9156-c11728c46ccf",
    "visible": true
}