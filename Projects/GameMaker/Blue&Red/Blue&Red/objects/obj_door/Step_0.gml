if(obj_leva_door.isUsed)
{
	if(isMoving)
	{
		vsp = door_speed;
	}
}

y += vsp;

if(place_meeting(x,y+5,obj_platform_move) || place_meeting(x,y-5,obj_platform_move))
{
	isMoving = false;
	vsp = 0;
	if(door_speed < 0)
	{
		y += offset_door;
	}
	else
	{
		y -= offset_door;
	}
	door_speed *= -1;
	
	audio_play_sound(Platform_Stop,1,false);
}

if(place_meeting(x,y-1,obj_player1) && isMoving)
{
	with(obj_player1)
	{
		y += other.door_speed;
	}
}
if(place_meeting(x,y-1,obj_player2) && isMoving)
{
	with(obj_player2)
	{
		y += other.door_speed;
	}
}
