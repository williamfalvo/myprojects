if(!place_meeting(x,y+5,obj_stop_door) && !place_meeting(x,y-5,obj_stop_door))
{
	if(door_speed < 0)
	{
		y = other.y + (other.sprite_height/2 + sprite_height/2 + offset_door);
	}
	else
	{
		y = other.y - (other.sprite_height/2 + sprite_height/2 + offset_door);
	}	
	isMoving = false;
	vsp = 0;
	door_speed *= -1;
	
	audio_play_sound(Platform_Stop,1,false);
}
