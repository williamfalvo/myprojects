{
    "id": "a3bc490e-01fc-4d5f-bb24-02cf4ddd106a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_finish_player2",
    "eventList": [
        {
            "id": "ee27a4cc-1100-4e15-aba8-b5c0e16807f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a3bc490e-01fc-4d5f-bb24-02cf4ddd106a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6b1f73c8-534a-41b2-bbc4-90f345f544e9",
    "visible": true
}