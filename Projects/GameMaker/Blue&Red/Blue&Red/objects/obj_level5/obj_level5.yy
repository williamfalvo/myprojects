{
    "id": "e35ba07a-ce10-4cef-a955-36dc59428a25",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level5",
    "eventList": [
        {
            "id": "d933da6f-1eeb-416e-9df1-e7cf1f216262",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e35ba07a-ce10-4cef-a955-36dc59428a25"
        },
        {
            "id": "36b5bb58-87b8-4773-98f6-064b6cf1264a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "e35ba07a-ce10-4cef-a955-36dc59428a25"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9db7202b-12d0-427b-ac93-169c6dd61dd7",
    "visible": true
}