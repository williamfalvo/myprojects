{
    "id": "8ea13018-4807-4cd3-a729-939108952960",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player1_death",
    "eventList": [
        {
            "id": "260de389-6737-43d4-b1dd-a733ec995158",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8ea13018-4807-4cd3-a729-939108952960"
        },
        {
            "id": "d4dd4432-fbbd-4ae9-a56f-6031cebb2373",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8ea13018-4807-4cd3-a729-939108952960"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "6f4a2f73-5029-4d86-99d4-b1244d77d3c7",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.025",
            "varName": "animation_ratio",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "278976cd-618a-4ad1-a751-6f9cb3eb8012",
    "visible": true
}