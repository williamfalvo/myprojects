{
    "id": "4571da6a-3b48-4f04-9e81-e856055d5f82",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player2",
    "eventList": [
        {
            "id": "89ec549f-aff8-4719-9724-0847fb128662",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4571da6a-3b48-4f04-9e81-e856055d5f82"
        },
        {
            "id": "916bd31d-3f8a-4f80-a074-b022a7e6bf6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5152f885-f682-45aa-b53b-82f13e212077",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4571da6a-3b48-4f04-9e81-e856055d5f82"
        },
        {
            "id": "726a564e-ae25-402c-b33a-c37a3b86e371",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8cbf68e6-bec0-4818-be61-35d1e323d197",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4571da6a-3b48-4f04-9e81-e856055d5f82"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3dcc04b2-f998-480e-8183-fa8c94f6be2f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "6b1f73c8-534a-41b2-bbc4-90f345f544e9",
    "visible": true
}