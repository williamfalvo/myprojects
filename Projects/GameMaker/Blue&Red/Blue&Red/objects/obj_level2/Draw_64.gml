draw_set_font(fnt_pausa);
draw_set_color(c_gray);

if(obj_levelSelection.index_selection == 2)
{
	draw_set_font(fnt_level_selected);
	draw_text_color(x-sprite_width/4,y+70,"Level 2",c_white,c_white,c_white,c_white,1);
}
else
{
	draw_set_font(fnt_level);
	draw_text_color(x-sprite_width/4,y+64,"Level 2",c_gray,c_gray,c_gray,c_gray,1);
}