{
    "id": "4b42b25a-4291-41f4-ae9c-8642def3716a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level2",
    "eventList": [
        {
            "id": "606bcd8b-bb94-4a34-984b-afa128014a93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4b42b25a-4291-41f4-ae9c-8642def3716a"
        },
        {
            "id": "b6f1ddd5-b9fa-4e16-9cf3-84c46a796452",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4b42b25a-4291-41f4-ae9c-8642def3716a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "913f764b-ab79-4313-a562-d9aa573feb1d",
    "visible": true
}