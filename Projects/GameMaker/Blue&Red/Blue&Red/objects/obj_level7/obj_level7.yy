{
    "id": "f14ccc3d-e6c8-4b5b-b39f-c3346a989420",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level7",
    "eventList": [
        {
            "id": "80ad97fe-2aa8-40a2-a37a-7b592f8cb522",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f14ccc3d-e6c8-4b5b-b39f-c3346a989420"
        },
        {
            "id": "c3b1508f-3d27-4f4a-ae68-5d82651b5c66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f14ccc3d-e6c8-4b5b-b39f-c3346a989420"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "77c3e74c-6808-4e16-9c65-743ef1fc04f6",
    "visible": true
}