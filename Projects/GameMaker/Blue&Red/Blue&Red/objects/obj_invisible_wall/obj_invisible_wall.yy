{
    "id": "b6e11205-7dbf-4326-abbf-a04cb6a3bb2a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_invisible_wall",
    "eventList": [
        {
            "id": "f68f6b05-284c-45df-ad76-3079fecd43b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b6e11205-7dbf-4326-abbf-a04cb6a3bb2a"
        },
        {
            "id": "7945389e-004c-4cc3-8fdd-e1f72fab7f1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b6e11205-7dbf-4326-abbf-a04cb6a3bb2a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "ba7da6e3-c52a-4479-a359-1e4023eb5bba",
    "visible": true
}