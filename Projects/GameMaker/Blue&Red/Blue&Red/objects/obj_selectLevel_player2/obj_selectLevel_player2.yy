{
    "id": "5f95e385-66c5-471c-b289-0a5d11ae0ea5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_selectLevel_player2",
    "eventList": [
        {
            "id": "73f8caf5-44ef-4d18-8802-2ff71767d975",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5f95e385-66c5-471c-b289-0a5d11ae0ea5"
        },
        {
            "id": "bd5020a6-006b-45ce-8bcb-36f61b941329",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5f95e385-66c5-471c-b289-0a5d11ae0ea5"
        },
        {
            "id": "5fa30028-1300-4406-970d-a69fb75570c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5f95e385-66c5-471c-b289-0a5d11ae0ea5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6b1f73c8-534a-41b2-bbc4-90f345f544e9",
    "visible": true
}