{
    "id": "6217d379-c4e0-446e-818d-ea5e9d50221b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fade_in_out",
    "eventList": [
        {
            "id": "db59736f-d383-4055-b8b2-503a1409a4b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6217d379-c4e0-446e-818d-ea5e9d50221b"
        },
        {
            "id": "09b9c0ad-586a-4568-be84-e11d1ad7f6fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6217d379-c4e0-446e-818d-ea5e9d50221b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "79476a23-c8dc-4db6-9f92-be9aeb256244",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.025",
            "varName": "anim_ratio",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "8042c552-e732-439e-b07c-19a8efdbd3b3",
    "visible": true
}