{
    "id": "265bde42-9847-4ed9-9b72-ea1f75eea936",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_quit_player2",
    "eventList": [
        {
            "id": "d71a427d-86f9-4a1c-860e-deabea9bca60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "265bde42-9847-4ed9-9b72-ea1f75eea936"
        },
        {
            "id": "13bf4c6a-bb84-4cf1-8529-ceb46d8fb88b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "265bde42-9847-4ed9-9b72-ea1f75eea936"
        },
        {
            "id": "f1c8e8a7-fd91-4885-92a8-0c9f8298c6be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "265bde42-9847-4ed9-9b72-ea1f75eea936"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6b1f73c8-534a-41b2-bbc4-90f345f544e9",
    "visible": true
}