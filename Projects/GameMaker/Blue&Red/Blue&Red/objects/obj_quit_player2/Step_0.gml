if(obj_quit.isSelected)
{
	sprite_index = spr_end_level;
}
else
{
	if(place_meeting(x,y-2,obj_player2))
	{
		obj_quit.isPlayer2_Inside = true;
		if(sprite_index != spr_anim_finish_player2)
		{
			sprite_index = spr_anim_finish_player2;
		}
	}
	else
	{
		obj_quit.isPlayer2_Inside = false;
		if(sprite_index != spr_player2)
		{
			sprite_index = spr_player2;
		}
	}
}