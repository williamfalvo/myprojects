{
    "id": "6b219f6c-6c6c-4fd2-84fc-d626155fd23b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_floor_title",
    "eventList": [
        {
            "id": "46493ede-c1b2-4803-91b9-4bb8664c5093",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6b219f6c-6c6c-4fd2-84fc-d626155fd23b"
        },
        {
            "id": "d27f98f9-9964-493e-86fe-6cddfd69f179",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6b219f6c-6c6c-4fd2-84fc-d626155fd23b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9817956b-9b77-454b-a3b9-6071430c5994",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "a569f0de-abf9-4926-a875-8658b58ea24e",
    "visible": true
}