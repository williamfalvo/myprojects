{
    "id": "ca8447d6-4dcf-4093-8620-dd5f50aa8b77",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level6",
    "eventList": [
        {
            "id": "f147e3d6-fd6b-4c28-b3c2-8bd517d0fa94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca8447d6-4dcf-4093-8620-dd5f50aa8b77"
        },
        {
            "id": "249e6078-b699-4aca-8f18-23da23ac8503",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ca8447d6-4dcf-4093-8620-dd5f50aa8b77"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ad53f992-8c44-4613-9801-c011025f58d3",
    "visible": true
}