if(obj_leva_platform.isUsed)
{
	if(isMoving)
	{
		hsp = platform_speed;
	}
}

x += hsp;

if(place_meeting(x+5,y,obj_door) || place_meeting(x-5,y,obj_door))
{
	audio_play_sound(Platform_Stop,1,false);
	
	isMoving = false;
	hsp = 0;
	if(platform_speed < 0)
	{
		x += offset_platform;
	}
	else
	{
		x -= offset_platform;
	}
	platform_speed *= -1;
}
