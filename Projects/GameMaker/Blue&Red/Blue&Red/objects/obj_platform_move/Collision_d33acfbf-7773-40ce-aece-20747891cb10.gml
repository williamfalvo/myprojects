if(!place_meeting(x+5,y,obj_platform_move_stop) && !place_meeting(x-5,y,obj_platform_move_stop))
{
	audio_play_sound(Platform_Stop,1,false);
	
	if(platform_speed < 0)
	{
		x = other.x + (other.sprite_width/2 + sprite_width/2 + offset_platform);
	}
	else
	{
		x = other.x - (other.sprite_width/2 + sprite_width/2 + offset_platform);
	}	
	isMoving = false;
	hsp = 0;
	platform_speed *= -1;
}
