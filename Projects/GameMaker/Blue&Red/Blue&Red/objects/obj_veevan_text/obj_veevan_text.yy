{
    "id": "28fca2e9-6038-4a4f-9d0f-c99a33768a89",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_veevan_text",
    "eventList": [
        {
            "id": "3b7fa344-48e4-4cc3-937d-3916979819b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "28fca2e9-6038-4a4f-9d0f-c99a33768a89"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "c98f674a-6f17-4c69-a968-4bb4757b3753",
    "visible": true
}