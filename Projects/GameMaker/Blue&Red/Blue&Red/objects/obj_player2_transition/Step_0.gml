image_xscale = max(image_xscale-animation_ratio, 0);
image_yscale = image_xscale;

if ( image_xscale == 0 )
{
	with(obj_fade_in_out)
	{
		image_alpha = min(image_alpha+anim_ratio,1);
		if(image_alpha == 1)
		{
			room_goto_next();
		}
	}
}

