{
    "id": "fa8f772c-ab56-4534-a0d4-070b44fb2bea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level1",
    "eventList": [
        {
            "id": "1857e749-7bad-4357-a71a-da8c6e2583f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fa8f772c-ab56-4534-a0d4-070b44fb2bea"
        },
        {
            "id": "8ec61779-4a45-4f8f-b04f-756d9de635a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "fa8f772c-ab56-4534-a0d4-070b44fb2bea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a90b9bfb-1160-4261-b044-73cdcc4d8e1b",
    "visible": true
}