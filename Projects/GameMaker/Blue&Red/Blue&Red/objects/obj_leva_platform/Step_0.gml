if(isUsed)
{
	if(goDown)
	{
		image_angle = max(image_angle-angle_animation_down,-90);
	}
	if(image_angle == -90)
	{
		goUp = true;
	}
	if(goUp)
	{
		goDown = false;
		image_angle = min(image_angle+angle_animation_up, 0);
	}
}

if(!isUsed && place_meeting(x,y,obj_player))
{
	audio_play_sound(LevaActive,1,false);
	with(obj_leva_platform)
	{
		image_alpha = 0.5;
		isUsed = true;
		alarm_set(0,cooldown_leva);
	}
	with(obj_platform_move)
	{
		isMoving = true;
	}
}