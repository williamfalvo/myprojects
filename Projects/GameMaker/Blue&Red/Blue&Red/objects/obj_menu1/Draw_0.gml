/// @description While game is paused
draw_set_font(fnt_pausa);
draw_set_halign(fa_right);
draw_set_valign(fa_bottom);
draw_set_color(c_gray);

if(index_selection == 0)
{
	draw_set_font(fnt_pausa_selected);
	draw_text_color(x_menu,620, "New Game", c_white,c_white,c_white,c_white,1);
	draw_set_font(fnt_pausa);
	draw_text(x_menu,660, "Select level");
	draw_text(x_menu,700, "Quit");
}
else if(index_selection == 1)
{
	draw_text(x_menu,600, "New Game");
	draw_set_font(fnt_pausa_selected);
	draw_text_color(x_menu,660, "Select level", c_white,c_white,c_white,c_white,1);
	draw_set_font(fnt_pausa)
	draw_text(x_menu,700, "Quit");
}
else if(index_selection == 2)
{
	draw_text(x_menu,600, "New Game");
	draw_text(x_menu,640, "Select level");
	draw_set_font(fnt_pausa_selected);
	draw_text_color(x_menu,700, "Quit", c_white,c_white,c_white,c_white,1);
}

