//Animation Menu
if(x_menu > x_finish_position)
{
	x_menu -= menu_speed;
}




//Selection Menu
if(keyboard_check_pressed(vk_up))
{
	if(index_selection == 0)
	{
		index_selection = 2;
	}
	else
	{
		index_selection --;
	}
}
if(keyboard_check_pressed(vk_down))
{
	if(index_selection == 2)
	{
		index_selection = 0;
	}
	else
	{
		index_selection ++;
	}
}

//Press Enter Action
if(index_selection == 0 && keyboard_check_pressed(vk_enter))
{
	room_goto(rm_level1);
}
if(index_selection == 1 && keyboard_check_pressed(vk_enter))
{
	room_goto(rm_levelSelection);
}
if((index_selection == 2 && keyboard_check_pressed(vk_enter)) || keyboard_check_pressed(vk_escape))
{
	game_end();
}

	
