
gameIsPaused = false;
index_selection = 0;

gui_width = display_get_gui_width();
gui_height = display_get_gui_height();
gui_margin = 32;

pause_text_x = gui_width;
pause_text_y = gui_height - gui_margin;

x_menu = room_width + 300;
x_finish_position = room_width - 50;
