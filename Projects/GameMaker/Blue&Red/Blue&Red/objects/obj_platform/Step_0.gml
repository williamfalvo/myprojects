/// @description Set Movement of Platform
if(isMoveHorizontal)
{
	x += horizontal_speed;
}

if(isMoveVertical)
{
	y += vertical_speed;
}