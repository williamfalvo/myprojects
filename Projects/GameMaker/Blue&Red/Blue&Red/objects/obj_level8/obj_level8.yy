{
    "id": "6925c756-0260-45d1-a525-19018672c752",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level8",
    "eventList": [
        {
            "id": "5f313e7e-ae47-43f2-a8c1-c8ab88d8a804",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6925c756-0260-45d1-a525-19018672c752"
        },
        {
            "id": "626e8165-a051-48b1-9cf2-33fa3b405e6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6925c756-0260-45d1-a525-19018672c752"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c299c287-dca1-4cef-a2d1-554630b97e74",
    "visible": true
}