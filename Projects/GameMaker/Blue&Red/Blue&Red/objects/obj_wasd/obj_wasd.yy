{
    "id": "e12d9c31-c893-4c15-a82a-f1520290a1ca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wasd",
    "eventList": [
        {
            "id": "70dd0614-76df-48e5-9bbe-9ec74ea2db05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e12d9c31-c893-4c15-a82a-f1520290a1ca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98ebd5ff-fa41-46a4-8e8c-9eeebec2a281",
    "visible": true
}