/// @description Pause

if (gameIsPaused == false)
{	
	audio_play_sound(Pause_escape_pause,1,false);
	audio_pause_sound(Theme_Level);
	instance_activate_layer("Pause");
	instance_deactivate_all(true);
	gameIsPaused = true;
}
else
{
	audio_play_sound(Pause_escape_continue,1,false);
	audio_resume_sound(Theme_Level);
	instance_deactivate_layer("Pause");
	instance_activate_all();
	gameIsPaused = false;
}
