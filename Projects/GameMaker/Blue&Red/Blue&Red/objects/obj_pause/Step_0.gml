if(gameIsPaused)
{
	if(keyboard_check_pressed(vk_up))
	{
		audio_play_sound(Pause_move,1,false);
		if(index_selection == 0)
		{
			index_selection = 2;
		}
		else
		{
			index_selection --;
		}
	}
	if(keyboard_check_pressed(vk_down))
	{
		audio_play_sound(Pause_move,1,false);
		if(index_selection == 2)
		{
			index_selection = 0;
		}
		else
		{
			index_selection ++;
		}
	}

	if(index_selection == 0 && keyboard_check_pressed(vk_enter))
	{
		audio_play_sound(Pause_selection,1,false);
		audio_resume_sound(Theme_Level);
		instance_deactivate_layer("Pause");
		instance_activate_all();
		gameIsPaused = false;
	}
	if(index_selection == 1 && keyboard_check_pressed(vk_enter))
	{
		audio_play_sound(Pause_selection,1,false);
		audio_resume_sound(Theme_Level);
		room_restart();
	}
	if(index_selection == 2 && keyboard_check_pressed(vk_enter))
	{
		audio_play_sound(Pause_selection,1,false);
		room_goto(rm_startScreen);
	}
}
	
