{
    "id": "552ecf21-b9d3-47c1-8842-04e301697c8b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pause",
    "eventList": [
        {
            "id": "c87dd885-42bd-457c-a065-62997245e157",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "552ecf21-b9d3-47c1-8842-04e301697c8b"
        },
        {
            "id": "fdaa707e-87f8-4c9b-97df-52efc3a5d734",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "552ecf21-b9d3-47c1-8842-04e301697c8b"
        },
        {
            "id": "1fe8065d-de47-4b7b-b0cf-01b4fd42857b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "552ecf21-b9d3-47c1-8842-04e301697c8b"
        },
        {
            "id": "fb5cff2e-a039-468b-896e-276009379701",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "552ecf21-b9d3-47c1-8842-04e301697c8b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "533011d1-7881-418d-9397-d436e3ad3ed6",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "480",
            "varName": "x_pause",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}