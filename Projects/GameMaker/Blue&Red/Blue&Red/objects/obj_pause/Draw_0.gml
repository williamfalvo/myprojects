/// @description While game is paused
draw_set_font(fnt_pausa);
draw_set_color(c_gray);


if (gameIsPaused)
{
	if(index_selection == 0)
	{
		draw_text(x_pause,100,"Game is paused");
		draw_set_font(fnt_pausa_selected);
		draw_text_color(x_pause,300, "Continue", c_white,c_white,c_white,c_white,1);
		draw_set_font(fnt_pausa);
		draw_text(x_pause,360, "Restart");
		draw_text(x_pause,400, "Back to menu");
	}
	else if(index_selection == 1)
	{
		draw_text(x_pause,100,"Game is paused");
		draw_text(x_pause,300, "Continue");
		draw_set_font(fnt_pausa_selected);
		draw_text_color(x_pause,340, "Restart", c_white,c_white,c_white,c_white,1);
		draw_set_font(fnt_pausa)
		draw_text(x_pause,410, "Back to menu");
	}
	else if(index_selection == 2)
	{
		draw_text(x_pause,100,"Game is paused");
		draw_text(x_pause,300, "Continue");
		draw_text(x_pause,340, "Restart");
		draw_set_font(fnt_pausa_selected);
		draw_text_color(x_pause,380, "Back to menu", c_white,c_white,c_white,c_white,1);
	}
	
}

