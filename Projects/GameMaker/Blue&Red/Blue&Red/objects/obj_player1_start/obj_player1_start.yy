{
    "id": "0c2d60fd-667c-4a12-8dbb-e343efbd2396",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player1_start",
    "eventList": [
        {
            "id": "be331420-5fa8-4ba5-a070-9c42a4545306",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0c2d60fd-667c-4a12-8dbb-e343efbd2396"
        },
        {
            "id": "c2ad5fad-4c69-4838-a67d-13660f434c86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0c2d60fd-667c-4a12-8dbb-e343efbd2396"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "a3da7568-9a10-40b8-9a73-db24331d1e2d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.025",
            "varName": "animation_ratio",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "4f8eb28d-30b9-4711-b610-e8ad66902b41",
    "visible": true
}