{
    "id": "b44e52f3-42ec-4c93-95a2-1b71ce0225e9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_newGame_player1",
    "eventList": [
        {
            "id": "d94001ac-b4d4-450d-8ec6-3bf84e124edc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b44e52f3-42ec-4c93-95a2-1b71ce0225e9"
        },
        {
            "id": "0be5e781-3065-4a21-bdf8-ddec3a421295",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b44e52f3-42ec-4c93-95a2-1b71ce0225e9"
        },
        {
            "id": "96faf956-9996-45a6-86d0-3d25ff96cc5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b44e52f3-42ec-4c93-95a2-1b71ce0225e9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "278976cd-618a-4ad1-a751-6f9cb3eb8012",
    "visible": true
}