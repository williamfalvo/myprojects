{
    "id": "a2e17631-2f5b-40d4-b35f-469dcd64cdc2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player2_start",
    "eventList": [
        {
            "id": "4a7cff0b-1661-4322-bd70-4e2048eccc6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a2e17631-2f5b-40d4-b35f-469dcd64cdc2"
        },
        {
            "id": "6b8f2f5a-0413-4f56-bf16-5a1e5ec9ffdf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a2e17631-2f5b-40d4-b35f-469dcd64cdc2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "59f578e7-8e23-4e3e-b9e2-c680f1be2abf",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.025",
            "varName": "animation_ratio",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "be0ebad1-8103-48b1-a092-9193247c2b98",
    "visible": true
}