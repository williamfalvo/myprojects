{
    "id": "2fa661ff-fe7c-4ba3-ae0e-f54be103797d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_levelSelection",
    "eventList": [
        {
            "id": "3d6a57e8-3382-4c63-8b38-1b255507661c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2fa661ff-fe7c-4ba3-ae0e-f54be103797d"
        },
        {
            "id": "26faff4f-63e3-4da8-909f-5287c12b8fd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2fa661ff-fe7c-4ba3-ae0e-f54be103797d"
        },
        {
            "id": "23e6532d-62f1-45bb-8f19-94f2ed76bec8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2fa661ff-fe7c-4ba3-ae0e-f54be103797d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}