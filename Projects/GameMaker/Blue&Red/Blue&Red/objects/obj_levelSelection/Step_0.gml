if(keyboard_check_pressed(vk_escape))
{
	index_selection = 10;
}

if(keyboard_check_pressed(vk_down))
{
	audio_play_sound(Pause_move,1,false);
	if(index_selection > 6 && index_selection < 9)
	{
		index_selection -= 6;
	}
	else if(index_selection == 9)
	{
		index_selection = 10;
	}
	else if(index_selection == 10)
	{
		index_selection = 3;
	}
	else
	{
		index_selection += 3;
	}
}

if(keyboard_check_pressed(vk_up))
{
	audio_play_sound(Pause_move,1,false);
	if(index_selection > 0 && index_selection < 3)
	{
		index_selection += 6;
	}
	else if(index_selection == 3)
	{
		index_selection = 10;
	}
	else if(index_selection == 10)
	{
		index_selection = 9;
	}
	else
	{
		index_selection -= 3;
	}
}

if(keyboard_check_pressed(vk_left))
{
	audio_play_sound(Pause_move,1,false);
	if(index_selection == 1)
	{
		index_selection = 10;
	}
	else
	{
		index_selection --;
	}
}

if(keyboard_check_pressed(vk_right))
{
	audio_play_sound(Pause_move,1,false);
	if(index_selection == 10)
	{
		index_selection = 1;
	}
	else
	{
		index_selection ++;
	}
}

if(keyboard_check_pressed(vk_enter))
{
	audio_stop_sound(Theme_Menu);
	
	switch(index_selection)
	{
		case 1 : room_goto(rm_level1);break;
		case 2 : room_goto(rm_level2);break;
		case 3 : room_goto(rm_level3);break;
		case 4 : room_goto(rm_level4);break;
		case 5 : room_goto(rm_level5);break;
		case 6 : room_goto(rm_level6);break;
		case 7 : room_goto(rm_level7);break;
		case 8 : room_goto(rm_level8);break;
		case 9 : room_goto(rm_level9);break;
		case 10 : room_goto(rm_startScreen);break;
	}
}


