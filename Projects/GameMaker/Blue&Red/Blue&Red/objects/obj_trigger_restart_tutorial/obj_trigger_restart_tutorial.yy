{
    "id": "00a302dc-d556-474e-9399-cf5be5e88d65",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_trigger_restart_tutorial",
    "eventList": [
        {
            "id": "082d111c-7057-4e7e-aa80-4bb708f64add",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "00a302dc-d556-474e-9399-cf5be5e88d65"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8517a90c-eee6-4dea-88ce-1424e57272f1",
    "visible": true
}