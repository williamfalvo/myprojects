{
    "id": "74d52616-90e0-4188-96c5-7c92d1bc47ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_floor",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9817956b-9b77-454b-a3b9-6071430c5994",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "a569f0de-abf9-4926-a875-8658b58ea24e",
    "visible": true
}