{
    "id": "b69d846e-b80e-4bdd-bb53-845c7af26bd3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player2_death",
    "eventList": [
        {
            "id": "475977ce-fc83-4bab-97ec-2e3ffe6a8bcd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b69d846e-b80e-4bdd-bb53-845c7af26bd3"
        },
        {
            "id": "ebce8603-cff6-484a-9a02-3a5296bcc513",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b69d846e-b80e-4bdd-bb53-845c7af26bd3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "696f5a83-f0a7-4c69-9ad9-13d3d561205b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.025",
            "varName": "animation_ratio",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "6b1f73c8-534a-41b2-bbc4-90f345f544e9",
    "visible": true
}