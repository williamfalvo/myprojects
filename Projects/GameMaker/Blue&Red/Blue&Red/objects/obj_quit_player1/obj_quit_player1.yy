{
    "id": "c7181028-fbe6-400d-b24c-120ea61b7108",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_quit_player1",
    "eventList": [
        {
            "id": "c009281f-b7db-49e0-ad0b-675b722889bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c7181028-fbe6-400d-b24c-120ea61b7108"
        },
        {
            "id": "da51800f-6ed5-47dc-adf3-95ba1ce8059b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c7181028-fbe6-400d-b24c-120ea61b7108"
        },
        {
            "id": "31fd46e8-959e-4de8-bb1b-b9652379cb50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c7181028-fbe6-400d-b24c-120ea61b7108"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "278976cd-618a-4ad1-a751-6f9cb3eb8012",
    "visible": true
}