if(obj_selectLevel.isSelected)
{
	sprite_index = spr_end_level;
}
else
{
	if(place_meeting(x,y-2,obj_player1))
	{
		obj_selectLevel.isPlayer1_Inside = true;
		if(sprite_index != spr_anim_finish_player1)
		{
			sprite_index = spr_anim_finish_player1;
		}
	}
	else
	{
		obj_selectLevel.isPlayer1_Inside = false;
		if(sprite_index != spr_player1)
		{
			sprite_index = spr_player1;
		}
	}
}