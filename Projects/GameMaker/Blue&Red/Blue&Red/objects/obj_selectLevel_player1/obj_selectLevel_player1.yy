{
    "id": "8d7159bf-d4f0-43aa-8105-b08a321b13e1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_selectLevel_player1",
    "eventList": [
        {
            "id": "a7f88ef0-ad6a-4d31-8a7d-731b4c2352c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8d7159bf-d4f0-43aa-8105-b08a321b13e1"
        },
        {
            "id": "f4c3fd40-97bc-4101-a32d-2d4d72748efd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d7159bf-d4f0-43aa-8105-b08a321b13e1"
        },
        {
            "id": "d35edb41-7357-4c0b-903e-49d55e7589fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8d7159bf-d4f0-43aa-8105-b08a321b13e1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "278976cd-618a-4ad1-a751-6f9cb3eb8012",
    "visible": true
}