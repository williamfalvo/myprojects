{
    "id": "5c6fad8c-d93e-4247-800d-670bb2610a04",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player1_menu_selected",
    "eventList": [
        {
            "id": "d5a4bc7d-3429-473d-85e0-c9c90464dfe5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5c6fad8c-d93e-4247-800d-670bb2610a04"
        },
        {
            "id": "6ea91f23-8af6-40d1-bd31-0aeec89d0520",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5c6fad8c-d93e-4247-800d-670bb2610a04"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "4884e3de-4f17-483f-a060-fe2c80aed052",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.025",
            "varName": "animation_ratio",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "278976cd-618a-4ad1-a751-6f9cb3eb8012",
    "visible": true
}