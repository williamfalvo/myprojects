{
    "id": "fd0ccd1e-c9c6-4ceb-86e6-ac3f79f817a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level4",
    "eventList": [
        {
            "id": "3a208246-e5da-46ba-94c4-a7a9832411eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fd0ccd1e-c9c6-4ceb-86e6-ac3f79f817a6"
        },
        {
            "id": "36e3673d-f77f-4b1d-9a69-281f1c69be51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "fd0ccd1e-c9c6-4ceb-86e6-ac3f79f817a6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b79d03e9-d833-49fd-9829-6545b19612a4",
    "visible": true
}