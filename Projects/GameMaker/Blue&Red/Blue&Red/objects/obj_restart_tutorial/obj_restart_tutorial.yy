{
    "id": "cd438aea-0a96-4a59-8b36-580ea8015d4b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_restart_tutorial",
    "eventList": [
        {
            "id": "eea5aa44-40ae-4213-98c9-461056ca65ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd438aea-0a96-4a59-8b36-580ea8015d4b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bc104c43-5240-4050-8493-45a9b2ad3a91",
    "visible": true
}