{
    "id": "bc3031d5-ada5-4335-b28c-1f8432811ef9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_finish_player1",
    "eventList": [
        {
            "id": "651f0b5a-02be-4508-80dc-b2e4fd8efc36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bc3031d5-ada5-4335-b28c-1f8432811ef9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "278976cd-618a-4ad1-a751-6f9cb3eb8012",
    "visible": true
}