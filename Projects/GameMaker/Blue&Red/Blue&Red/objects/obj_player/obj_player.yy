{
    "id": "3dcc04b2-f998-480e-8183-fa8c94f6be2f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "d0037290-9271-4a56-9802-23d9a322f659",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3dcc04b2-f998-480e-8183-fa8c94f6be2f"
        },
        {
            "id": "0e294b5e-e7ad-47fe-854a-45066f312e61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3dcc04b2-f998-480e-8183-fa8c94f6be2f"
        },
        {
            "id": "d5f44f67-113b-4d14-90fb-d71934cf0824",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8cbf68e6-bec0-4818-be61-35d1e323d197",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3dcc04b2-f998-480e-8183-fa8c94f6be2f"
        },
        {
            "id": "5906f8a3-4615-4348-ab32-fcaec2b5f365",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9817956b-9b77-454b-a3b9-6071430c5994",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3dcc04b2-f998-480e-8183-fa8c94f6be2f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "50d168b0-c396-4164-b909-a460c0334996",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "2e6f5285-3eee-4864-8ce6-cb048251ef94",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "8d325019-41b9-4a7d-aa29-1e80e8872ab9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "a0aa54f7-17d2-4ad2-be2d-929dd967b423",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "e239a6f1-2ba8-459f-abef-7501e1f35556",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.8",
            "varName": "grav_speed",
            "varType": 0
        },
        {
            "id": "1b980563-da79-4ba7-aefe-045d71b20e9a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "5",
            "varName": "player_speed",
            "varType": 0
        },
        {
            "id": "037b8ee4-e447-42d1-8ad8-2093470db5f0",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "11",
            "varName": "jump_speed",
            "varType": 0
        },
        {
            "id": "12eeeab2-2a59-43f1-a120-1fbc64ac0962",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "vsp",
            "varType": 0
        },
        {
            "id": "76911a14-65d4-448e-9227-c6c60942f38e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "hsp",
            "varType": 0
        },
        {
            "id": "1d455a52-eb8c-4df3-8d1e-0ae019752020",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "20",
            "varName": "platform_jump",
            "varType": 0
        },
        {
            "id": "697e97c4-a404-48d5-897e-63d53f783720",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "player_speed_reduction",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}