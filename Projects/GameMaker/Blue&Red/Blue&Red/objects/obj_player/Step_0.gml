/// @description Behaviour of a player

//

//Gravità
if(!place_meeting(x,y+1,obj_ground) && !place_meeting(x,y+1,obj_platform_move) && !place_meeting(x,y+1,obj_player) && !place_meeting(x,y+5,obj_door))
{
	vsp = vsp + grav_speed;
}

if((place_meeting(x,y+vsp,obj_ground) || place_meeting(x,y+vsp,obj_door)) && vsp!=0)
{
	vsp = 0;
}
if(position_meeting(x,y-vsp,obj_ground))
{
	vsp = 0;
}

//Movimento Player sulla platform_move
if(place_meeting(x,y+1,obj_platform_move))
{
	
	inst = instance_nearest(x,y,obj_platform_move);
	if(inst.isMoving)
	{
		x += (inst.platform_speed + hsp);
	}
}

//BEHAVIOUR WITH DOOR

inst = instance_nearest(x,y,obj_door);
//Se c'è la collisione con la porta mentre si è su una piattaforma
if(instance_exists(obj_platform_move))
{
	if((position_meeting(x+sprite_width/2,y-sprite_height/2,obj_door) || position_meeting(x-sprite_width/2,y-sprite_height/2,obj_door)) &&obj_platform_move.isMoving )
	{
		hsp = 0;
		x -= obj_platform_move.platform_speed;
	}
}
/*
if(place_meeting(x,y,obj_door))
{
	inst = instance_nearest(x,y,obj_door);
	if(inst.isMoving)
	{
		x += hsp;
		y += (inst.door_speed + vsp);
		if(y < inst.y-inst.sprite_height)
		{
			y = inst.y - inst.sprite_height/2;
		}
	}
}
*/
if(place_meeting(x,y,obj_door) && (position_meeting(x-sprite_width/2-5,y,obj_ground) && inst.isMoving))
{
	x += 5;
}
if(place_meeting(x,y,obj_door) && (position_meeting(x+sprite_width/2+5,y,obj_ground) && inst.isMoving))
{
	x -= 5;
}
if(place_meeting(x,y,obj_door) && inst.isMoving)
{
	y -= 1;
}