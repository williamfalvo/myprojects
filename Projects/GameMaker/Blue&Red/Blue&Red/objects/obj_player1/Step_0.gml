/// @description Behaviour of player1
key_right = keyboard_check(ord("D"));
key_left = keyboard_check(ord("A"));

// Inherit the parent event
event_inherited();

//Movement
move = key_right - key_left;
if(keyboard_check_pressed(ord("W")) && (!place_meeting(x,y-1,obj_player)) && (place_meeting(x,y+1,obj_ground) || place_meeting(x,y+1,obj_platform_move) || place_meeting(x,y+1,obj_player2) || place_meeting(x,y+5,obj_door)))
{
	vsp = -jump_speed;
	audio_play_sound(Jump,1,false);
}

hsp = move * player_speed;



//Behaviuor with player 2

if(place_meeting(x,y+vsp,obj_player2))
{
	vsp = 0;
}

//Action of Player
if(hsp < 0)
{
	mp_linear_step(x - hsp, y, hsp, false);
}
else if(hsp >0)
{
	mp_linear_step(x + hsp, y, hsp, false);
}

y += vsp;

//Player1 Death

if(position_meeting(x+5+sprite_width/2,y-sprite_height/2,obj_platform_move) && position_meeting(x-5-sprite_width/2,y-sprite_height/2,obj_platform_move) && vsp == 0)
{
	scr_death();
	instance_change(obj_player1_death,true);
}

//Schiacciamento tra due door
if(position_meeting(x,y,obj_door) && position_meeting(x,y-sprite_height,obj_door))
{
	scr_death();
	instance_change(obj_player1_death,true);
}
//Schiacciamento tra door sopra e ground giù
if(position_meeting(x-sprite_width/2,y+5,obj_ground) && position_meeting(x-sprite_width/2,y-sprite_height-5,obj_door) || (position_meeting(x+sprite_width/2,y+5,obj_ground) && position_meeting(x+sprite_width/2,y-sprite_height-5,obj_door)))
{
	scr_death();
	instance_change(obj_player1_death,true);
}
//Schiacciamento tra door giù e ground su
if((position_meeting(x-sprite_width/2,y+5,obj_door) && position_meeting(x-sprite_width/2,y-sprite_height-5,obj_ground)) || (position_meeting(x+sprite_width/2,y+5,obj_door) && position_meeting(x+sprite_width/2,y-sprite_height-5,obj_ground)))
{
	if(obj_door.isMoving)
	{
		scr_death();
		instance_change(obj_player1_death,true);
	}
}
//Schiacciamento tra door giù e ground su durante ascensore
if((position_meeting(x-sprite_width/2,y+5,obj_door) && position_meeting(x+sprite_width/2-10,y-sprite_height-5,obj_ground)) || (position_meeting(x+sprite_width/2,y+5,obj_door) && position_meeting(x-sprite_width/2+5,y-sprite_height-5,obj_ground)))
{
	scr_death();
	instance_change(obj_player1_death,true);
}
//Schiacciamento tra platform sinistra e ground destra
if(position_meeting(x+5+sprite_width/2,y-sprite_height/2,obj_platform_move) && position_meeting(x-5-sprite_width/2,y-sprite_height/2,obj_ground))
{
	scr_death();
	instance_change(obj_player1_death,true);
}
//Schiacciamento tra platform destra e ground sinistra
if(position_meeting(x+5+sprite_width/2,y-sprite_height/2,obj_ground) && position_meeting(x-5-sprite_width/2,y-sprite_height/2,obj_platform_move))
{
	scr_death();
	instance_change(obj_player1_death,true);
}
//Schiacciamento tra platform sopra e door giù
if(position_meeting(x-sprite_width/2,y+5,obj_door) && position_meeting(x-sprite_width/2,y-sprite_height-5,obj_platform_move) || (position_meeting(x+sprite_width/2,y+5,obj_door) && position_meeting(x+sprite_width/2,y-sprite_height-5,obj_platform_move)))
{
	scr_death();
	instance_change(obj_player1_death,true);
}
//Schiacciamento tra door sopra e platform giù
if(position_meeting(x-sprite_width/2,y+5,obj_platform_move) && position_meeting(x-sprite_width/2,y-sprite_height-5,obj_door) || (position_meeting(x+sprite_width/2,y+5,obj_platform_move) && position_meeting(x+sprite_width/2,y-sprite_height-5,obj_door)))
{
	scr_death();
	instance_change(obj_player1_death,true);
}
//Schiacciamento tra platform destra e door sinistra
if((position_meeting(x+5+sprite_width/2,y,obj_door) && position_meeting(x-5-sprite_width/2,y-5,obj_platform_move)) || (position_meeting(x+5+sprite_width/2,y-sprite_height,obj_door) && position_meeting(x-5-sprite_width/2,y-sprite_height+5,obj_platform_move)))
{
	scr_death();
	instance_change(obj_player1_death,true);
}
//Schiacciamento tra door destra e platform sinistra
if((position_meeting(x+5+sprite_width/2,y-5,obj_platform_move) && position_meeting(x-5-sprite_width/2,y,obj_door)) || (position_meeting(x+5+sprite_width/2,y-sprite_height+5,obj_platform_move) && position_meeting(x-5-sprite_width/2,y-sprite_height,obj_door)))
{
	scr_death();
	instance_change(obj_player1_death,true);
}