{
    "id": "5152f885-f682-45aa-b53b-82f13e212077",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player1",
    "eventList": [
        {
            "id": "3f2423d6-1d5b-477e-a0ef-40309022a9c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5152f885-f682-45aa-b53b-82f13e212077"
        },
        {
            "id": "d65f8dad-cc1e-4870-a64f-88b865a01b6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4571da6a-3b48-4f04-9e81-e856055d5f82",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5152f885-f682-45aa-b53b-82f13e212077"
        },
        {
            "id": "f3d03f2a-0ba1-423e-ba50-7de81a5420fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8cbf68e6-bec0-4818-be61-35d1e323d197",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5152f885-f682-45aa-b53b-82f13e212077"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3dcc04b2-f998-480e-8183-fa8c94f6be2f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": true,
    "spriteId": "278976cd-618a-4ad1-a751-6f9cb3eb8012",
    "visible": true
}