{
    "id": "b5d5b60c-9b6a-4d7c-a3d6-99018bfb71e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level3",
    "eventList": [
        {
            "id": "7261ec5a-cb39-45ed-b9e9-4a2bdec9f89b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b5d5b60c-9b6a-4d7c-a3d6-99018bfb71e6"
        },
        {
            "id": "83dc27cf-dd5e-46d8-81c8-8bac3b9f0424",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b5d5b60c-9b6a-4d7c-a3d6-99018bfb71e6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "941f32a9-5146-438c-acc3-851a114721ca",
    "visible": true
}