draw_set_font(fnt_level);


if((obj_quit.isPlayer1_Inside || obj_quit.isPlayer2_Inside) && (obj_newGame.isPlayer1_Inside || obj_newGame.isPlayer2_Inside))
{
	draw_set_font(fnt_level_selected);
	draw_text_color(270,400,"New Game",c_white,c_white,c_white,c_white,1);
	draw_set_font(fnt_level);
	draw_text_color(560,410,"Select Level",c_gray,c_gray,c_gray,c_gray,1);
	draw_set_font(fnt_level_selected);
	draw_text_color(870,400,"Quit",c_white,c_white,c_white,c_white,1);
}
else if((obj_quit.isPlayer1_Inside || obj_quit.isPlayer2_Inside) && (obj_selectLevel.isPlayer1_Inside || obj_selectLevel.isPlayer2_Inside))
{
	draw_set_font(fnt_level)
	draw_text_color(290,410,"New Game",c_gray,c_gray,c_gray,c_gray,1);
	draw_set_font(fnt_level_selected);
	draw_text_color(540,400,"Select Level",c_white,c_white,c_white,c_white,1);
	draw_text_color(870,400,"Quit",c_white,c_white,c_white,c_white,1);
}
else if((obj_selectLevel.isPlayer1_Inside || obj_selectLevel.isPlayer2_Inside) && (obj_newGame.isPlayer1_Inside || obj_newGame.isPlayer2_Inside))
{
	draw_set_font(fnt_level_selected);
	draw_text_color(270,400,"New Game",c_white,c_white,c_white,c_white,1);
	draw_text_color(540,400,"Select Level",c_white,c_white,c_white,c_white,1);
	draw_set_font(fnt_level);
	draw_text_color(880,410,"Quit",c_gray,c_gray,c_gray,c_gray,1);
}
else if(obj_newGame.isPlayer1_Inside || obj_newGame.isPlayer2_Inside)
{
	draw_set_font(fnt_level_selected);
	draw_text_color(270,400,"New Game",c_white,c_white,c_white,c_white,1);
	draw_set_font(fnt_level);
	draw_text_color(560,410,"Select Level",c_gray,c_gray,c_gray,c_gray,1);
	draw_text_color(880,410,"Quit",c_gray,c_gray,c_gray,c_gray,1);
}
else if(obj_selectLevel.isPlayer1_Inside || obj_selectLevel.isPlayer2_Inside)
{
	draw_text_color(290,410,"New Game",c_gray,c_gray,c_gray,c_gray,1);
	draw_set_font(fnt_level_selected);
	draw_text_color(540,400,"Select Level",c_white,c_white,c_white,c_white,1);
	draw_set_font(fnt_level);
	draw_text_color(880,410,"Quit",c_gray,c_gray,c_gray,c_gray,1);
}
else if(obj_quit.isPlayer1_Inside || obj_quit.isPlayer2_Inside)
{
	draw_text_color(290,410,"New Game",c_gray,c_gray,c_gray,c_gray,1);
	draw_text_color(560,410,"Select Level",c_gray,c_gray,c_gray,c_gray,1);
	draw_set_font(fnt_level_selected);
	draw_text_color(870,400,"Quit",c_white,c_white,c_white,c_white,1);
}
else
{
	draw_text_color(290,410,"New Game",c_gray,c_gray,c_gray,c_gray,1);
	draw_text_color(560,410,"Select Level",c_gray,c_gray,c_gray,c_gray,1);
	draw_text_color(880,410,"Quit",c_gray,c_gray,c_gray,c_gray,1);
}