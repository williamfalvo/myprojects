{
    "id": "4e44d82f-bc94-44f3-aa2c-c8650093308b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_number_text",
    "eventList": [
        {
            "id": "919a5a21-4e58-486b-8d0c-b7c23c3c98ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4e44d82f-bc94-44f3-aa2c-c8650093308b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "621ddc99-d340-42f7-98f7-76275878d08d",
    "visible": true
}