{
    "id": "a83a3ac1-1ec5-428a-b890-c9174b7d67de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_giacomo_text",
    "eventList": [
        {
            "id": "0f0135fa-9ef9-4684-940d-814e3fe9a2b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a83a3ac1-1ec5-428a-b890-c9174b7d67de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "543131c6-092f-4ab5-a6af-2218fd1b9d5a",
    "visible": true
}