if(isFinish)
{
	if(audioFinish)
	{
		audio_play_sound(Jingle_EndLevel,1,false);
		audioFinish = false;
	}
	with(obj_finish_player1)
	{
		sprite_index = spr_end_level;
	}
	with(obj_finish_player2)
	{
		sprite_index= spr_end_level;
	}
}
else
{
	if(isPlayer1_Inside && isPlayer2_Inside)
	{
		alarm_set(0,120);
		isFinish = true;
		audioPlayer1 = false;
		audioPlayer2 = false;
		with(obj_player1)
		{
			instance_change(obj_player1_finish,true);
		}
		with(obj_player2)
		{
			instance_change(obj_player2_finish,true);
		}
	}
	
	if(isPlayer1_Inside)
	{
		if(audioPlayer1)
		{
			audio_play_sound(Finish_Player1,1,false);
			audioPlayer1 = false;
		}
		with(obj_finish_player1)
		{
			sprite_index = spr_anim_finish_player1;
		}
	}
	else
	{
		audioPlayer1 = true;
		with(obj_finish_player1)
		{
			sprite_index = spr_player1;
		}
	}

	if(isPlayer2_Inside)
	{
		if(audioPlayer2)
		{
			audio_play_sound(Finish_Player2,1,false);
			audioPlayer2 = false;
		}
		with(obj_finish_player2)
		{
			sprite_index = spr_anim_finish_player2;
		}
	}
	else
	{
		audioPlayer2 = true;
		with(obj_finish_player2)
		{
			sprite_index = spr_player2;
		}
	}
}
	