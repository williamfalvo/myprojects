
{
    "name": "rm_level4",
    "id": "b8a9a4f7-dbce-4850-9342-4045b59361c9",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "a6b1a207-2b07-462a-b62e-34f5aba1795e",
        "16ba68e7-6ef0-4167-804f-f1d1e202998b",
        "6a008367-f0bb-43ac-8190-c18856d8b5d2",
        "05a2b6ea-41d1-4dcf-aea2-9a30ce16c6e0",
        "4cb728ed-f346-40f9-bb68-460c3b03c9f6",
        "a3ed197b-6fde-457b-b738-464d01509b52",
        "a8d47781-ac90-4eb2-81b1-2768d60a1376",
        "71522e62-d177-448d-a683-91bb824c3d27",
        "09eb0c5a-cd00-44b0-9384-68e324ee6ea3",
        "0ea71bc5-4960-453b-9149-bdb8962abc80",
        "d97fcba8-d795-4e91-8921-33f3124dad67",
        "81de7c99-fdcb-406a-8d6b-27567cf4cf95",
        "54d7c53e-1693-46bd-a839-b17525646688",
        "a7d3f921-d3ae-42d2-860c-8edfa733b3cc",
        "aa7dd3da-3910-4505-9175-8acdcf61aa77",
        "137e3d37-d234-4c0f-b65c-7d1e6db879dc",
        "6776f50c-bb2c-46e4-bc2a-e1b35443d66b",
        "0551e7e5-f158-4d26-aa6f-7d5dc487b6a8",
        "9ab9de52-fc4d-4af6-8398-fdb23a922d59",
        "fea43b6a-fcdf-4eed-a948-2c886ce17c14",
        "a02648a3-3b99-42c5-9756-b9e5da909d8a",
        "cf26dca9-2a39-47be-b464-177bdc759d7b",
        "f223ce88-dba5-43ed-840d-53721d987e77",
        "645461ff-ad04-4f07-b677-f97100447f7c",
        "a614c78b-b22a-4996-96fe-263de114f65f",
        "9d9382a3-cdf5-4dfb-a57e-a29145cde5e4",
        "d468952a-46d0-4d1a-b01c-f04536e19be1",
        "cde7ef43-665b-4adc-a4c6-827ab7f61a96",
        "d81f6e11-84ad-4d87-b487-aa810e8e3d82",
        "d62791fc-42fc-4d7e-b49e-d335e78a7ce0",
        "293d53a6-76da-4b08-9f6c-a112ca47e84a",
        "d35171f5-9e4e-4d7e-903a-2847713fdc51",
        "c9f2b01e-1901-46d2-8628-41972d695ca7"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Pause",
            "id": "5ee37935-d5ae-4939-b7a6-30a9294cc5af",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_7B001A6D","id": "cde7ef43-665b-4adc-a4c6-827ab7f61a96","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7B001A6D","objId": "552ecf21-b9d3-47c1-8842-04e301697c8b","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 64,"y": 32}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "GameManager",
            "id": "63efc6f0-862c-4baa-9a60-b007c77e0de0",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_4681F04D","id": "d35171f5-9e4e-4d7e-903a-2847713fdc51","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4681F04D","objId": "f6b5bfaf-7e9a-4aa1-a936-b23b2088ecc7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 32,"y": 32},
{"name": "inst_4131E6F1","id": "c9f2b01e-1901-46d2-8628-41972d695ca7","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4131E6F1","objId": "c47c5c55-b136-492f-ab1c-d4d29fd4361c","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 96,"y": 32}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Fade",
            "id": "a125707b-4dc9-4b61-9a1d-564d08ee1f07",
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_288C0E5","id": "4cb728ed-f346-40f9-bb68-460c3b03c9f6","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_288C0E5","objId": "6217d379-c4e0-446e-818d-ea5e9d50221b","properties": null,"rotation": 0,"scaleX": 20,"scaleY": 12,"mvc": "1.0","x": 640,"y": 384}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "UI",
            "id": "cd772f3d-bb4b-4eca-bf5d-f560f1c60e88",
            "depth": 300,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_23486C9","id": "d81f6e11-84ad-4d87-b487-aa810e8e3d82","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_23486C9","objId": "fd670e55-7a12-4f0a-8a34-2ac76677c51d","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 96,"y": 16},
{"name": "inst_19F9C782","id": "d62791fc-42fc-4d7e-b49e-d335e78a7ce0","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_19F9C782","objId": "4e44d82f-bc94-44f3-aa2c-c8650093308b","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 160,"y": 16},
{"name": "inst_430E5361","id": "293d53a6-76da-4b08-9f6c-a112ca47e84a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_430E5361","objId": "1cee57e6-7c8b-4616-9296-5d834f31bd11","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 1072,"y": 16}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Finish",
            "id": "b38fe18a-ccaa-46f7-a678-c3dd5df1771b",
            "depth": 400,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_62D5FD3A","id": "6a008367-f0bb-43ac-8190-c18856d8b5d2","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_62D5FD3A","objId": "bc3031d5-ada5-4335-b28c-1f8432811ef9","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 1120,"y": 720},
{"name": "inst_7A1DF47B","id": "05a2b6ea-41d1-4dcf-aea2-9a30ce16c6e0","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7A1DF47B","objId": "a3bc490e-01fc-4d5f-bb24-02cf4ddd106a","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 1120,"y": 544}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Room",
            "id": "ba421c37-6cd8-4e7a-ac71-97d3258335e2",
            "depth": 500,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_7445E692","id": "09eb0c5a-cd00-44b0-9384-68e324ee6ea3","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7445E692","objId": "74d52616-90e0-4188-96c5-7c92d1bc47ce","properties": null,"rotation": 0,"scaleX": 40,"scaleY": 1,"mvc": "1.0","x": 640,"y": 752},
{"name": "inst_348781FC","id": "a6b1a207-2b07-462a-b62e-34f5aba1795e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_348781FC","objId": "74d52616-90e0-4188-96c5-7c92d1bc47ce","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 24,"mvc": "1.0","x": 16,"y": 384},
{"name": "inst_8AE072","id": "16ba68e7-6ef0-4167-804f-f1d1e202998b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_8AE072","objId": "74d52616-90e0-4188-96c5-7c92d1bc47ce","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 23,"mvc": "1.0","x": 1264,"y": 384},
{"name": "inst_575745CA","id": "71522e62-d177-448d-a683-91bb824c3d27","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_575745CA","objId": "74d52616-90e0-4188-96c5-7c92d1bc47ce","properties": null,"rotation": 0,"scaleX": 40,"scaleY": 1,"mvc": "1.0","x": 640,"y": 16},
{"name": "inst_AF8577D","id": "0ea71bc5-4960-453b-9149-bdb8962abc80","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_AF8577D","objId": "990c2b3c-f0da-4ce0-9c98-a1075a00249c","properties": [{"id": "bfad2860-e5d1-4c22-87d6-cfbfb6be4d1f","modelName": "GMOverriddenProperty","objectId": "990c2b3c-f0da-4ce0-9c98-a1075a00249c","propertyId": "d4ce8e39-fca8-479b-8927-9d412959388d","mvc": "1.0","value": "5"}],"rotation": 0,"scaleX": 4,"scaleY": 1,"mvc": "1.0","x": 192,"y": 384},
{"name": "inst_10604D6D","id": "d97fcba8-d795-4e91-8921-33f3124dad67","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_10604D6D","objId": "74d52616-90e0-4188-96c5-7c92d1bc47ce","properties": null,"rotation": 0,"scaleX": 3.5,"scaleY": 1,"mvc": "1.0","x": 56,"y": 384},
{"name": "inst_37DF80EC","id": "81de7c99-fdcb-406a-8d6b-27567cf4cf95","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_37DF80EC","objId": "02588167-e88b-4578-aef7-56ce5c95c104","properties": null,"rotation": 0,"scaleX": 3,"scaleY": 1,"mvc": "1.0","x": 528,"y": 576},
{"name": "inst_279D4F46","id": "54d7c53e-1693-46bd-a839-b17525646688","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_279D4F46","objId": "763ba341-2924-4909-9455-5dd6e6597410","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 192,"y": 352},
{"name": "inst_2C0EBB22","id": "a7d3f921-d3ae-42d2-860c-8edfa733b3cc","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2C0EBB22","objId": "763ba341-2924-4909-9455-5dd6e6597410","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 176,"y": 752},
{"name": "inst_1B60D48B","id": "aa7dd3da-3910-4505-9175-8acdcf61aa77","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1B60D48B","objId": "6d35e328-c5d4-4148-81f2-cf60b67ca312","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 512,"y": 464},
{"name": "inst_CA18C1C","id": "137e3d37-d234-4c0f-b65c-7d1e6db879dc","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_CA18C1C","objId": "8cbf68e6-bec0-4818-be61-35d1e323d197","properties": null,"rotation": 0,"scaleX": 3,"scaleY": 1,"mvc": "1.0","x": 912,"y": 576},
{"name": "inst_497C2D71","id": "6776f50c-bb2c-46e4-bc2a-e1b35443d66b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_497C2D71","objId": "02588167-e88b-4578-aef7-56ce5c95c104","properties": null,"rotation": 0,"scaleX": 3,"scaleY": 1.5,"mvc": "1.0","x": 1120,"y": 536},
{"name": "inst_679C1E0F","id": "0551e7e5-f158-4d26-aa6f-7d5dc487b6a8","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_679C1E0F","objId": "85d6e841-7e93-49ff-8fbd-a4a487fffe81","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 512,"y": 272},
{"name": "inst_1C15D03A","id": "9ab9de52-fc4d-4af6-8398-fdb23a922d59","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1C15D03A","objId": "c5924eeb-5920-4d2c-aada-d1a198c4516e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 576,"y": 576},
{"name": "inst_2E4E7B3E","id": "fea43b6a-fcdf-4eed-a948-2c886ce17c14","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2E4E7B3E","objId": "74d52616-90e0-4188-96c5-7c92d1bc47ce","properties": null,"rotation": 0,"scaleX": 7,"scaleY": 1,"mvc": "1.0","x": 1136,"y": 576},
{"name": "inst_79931409","id": "a02648a3-3b99-42c5-9756-b9e5da909d8a","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_79931409","objId": "02588167-e88b-4578-aef7-56ce5c95c104","properties": null,"rotation": 0,"scaleX": 3,"scaleY": 1.5,"mvc": "1.0","x": 1120,"y": 712},
{"name": "inst_16C25804","id": "cf26dca9-2a39-47be-b464-177bdc759d7b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_16C25804","objId": "990c2b3c-f0da-4ce0-9c98-a1075a00249c","properties": [{"id": "8ff9e003-a98e-4709-828b-29cf53f0d593","modelName": "GMOverriddenProperty","objectId": "990c2b3c-f0da-4ce0-9c98-a1075a00249c","propertyId": "d4ce8e39-fca8-479b-8927-9d412959388d","mvc": "1.0","value": "-5"}],"rotation": 0,"scaleX": 1,"scaleY": 5.5,"mvc": "1.0","x": 992,"y": 648},
{"name": "inst_5F405165","id": "f223ce88-dba5-43ed-840d-53721d987e77","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5F405165","objId": "763ba341-2924-4909-9455-5dd6e6597410","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 992,"y": 752},
{"name": "inst_226236DC","id": "645461ff-ad04-4f07-b677-f97100447f7c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_226236DC","objId": "763ba341-2924-4909-9455-5dd6e6597410","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 992,"y": 384},
{"name": "inst_153F23C1","id": "a614c78b-b22a-4996-96fe-263de114f65f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_153F23C1","objId": "c5924eeb-5920-4d2c-aada-d1a198c4516e","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 976,"y": 576},
{"name": "inst_3C733886","id": "9d9382a3-cdf5-4dfb-a57e-a29145cde5e4","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3C733886","objId": "74d52616-90e0-4188-96c5-7c92d1bc47ce","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 4,"mvc": "1.0","x": 576,"y": 432},
{"name": "inst_4C5294E0","id": "d468952a-46d0-4d1a-b01c-f04536e19be1","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4C5294E0","objId": "74d52616-90e0-4188-96c5-7c92d1bc47ce","properties": null,"rotation": 0,"scaleX": 9,"scaleY": 1,"mvc": "1.0","x": 416,"y": 384}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "fb2c2514-bdd9-4725-9738-f35f711aa3e1",
            "depth": 600,
            "grid_x": 16,
            "grid_y": 16,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_15D2DFC7","id": "a3ed197b-6fde-457b-b738-464d01509b52","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_15D2DFC7","objId": "0c2d60fd-667c-4a12-8dbb-e343efbd2396","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 80,"y": 336},
{"name": "inst_4C665C97","id": "a8d47781-ac90-4eb2-81b1-2768d60a1376","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4C665C97","objId": "a2e17631-2f5b-40d4-b35f-469dcd64cdc2","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 80,"y": 704}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "caf4b28e-23ec-47c4-acde-87e65227c8fd",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4278190080 },
            "depth": 700,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "989fb9d8-08c3-475f-a934-cb1dcebb4429",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "057864a1-1ac2-47fd-ac14-eb9c717c27f2",
        "Height": 768,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 1280
    },
    "mvc": "1.0",
    "views": [
{"id": "fe4fb377-71ea-4bb7-9ab7-2a9c31db8f6f","hborder": 500,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "5152f885-f682-45aa-b53b-82f13e212077","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1280,"wview": 1280,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "06ae477a-eb6a-4dcb-acb9-57a17e3f98fb","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "9cfcd864-8118-4ccb-b442-e0645f649335","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "07ed3546-360f-4de0-afc2-e4bb88082265","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ed0056e0-b52c-4656-be8f-7d0f0476af1f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "702c0545-03a0-4a42-9dad-66454fd55077","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "f18def90-ce53-422e-aa7a-fd0e7f32053f","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "f2870fed-7a55-499b-9741-bb61e5f45ab0","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "9202b636-b49f-4170-a046-f6966bddcf54",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}