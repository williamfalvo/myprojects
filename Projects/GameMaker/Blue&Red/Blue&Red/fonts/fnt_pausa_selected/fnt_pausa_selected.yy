{
    "id": "d5167737-d426-40fa-b5cb-669a00902858",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_pausa_selected",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "bd8a1934-e0db-4ba2-96a3-1a0ec41ec28a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 93,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 688,
                "y": 192
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c5018e33-5961-4e56-ae6c-990fe74dc89b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 93,
                "offset": 6,
                "shift": 28,
                "w": 16,
                "x": 652,
                "y": 192
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e02e76d6-4f06-42c2-a533-80915032a832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 93,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 234,
                "y": 192
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1d17efc9-8989-4347-b146-3bb7c6d17be3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 93,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 678,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1b07c91e-8b8f-4021-8c96-e4709aa8a05d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 93,
                "offset": 7,
                "shift": 36,
                "w": 23,
                "x": 311,
                "y": 192
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7f11825b-15e0-4fc2-95a7-fb83da087114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 93,
                "offset": 4,
                "shift": 59,
                "w": 52,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "95471f04-0a89-4005-9950-f97549f18dc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 93,
                "offset": 7,
                "shift": 49,
                "w": 35,
                "x": 717,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "46155969-43d0-4ef8-bee9-d3fbfa91f47a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 93,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 868,
                "y": 192
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d38f63b2-acde-4b84-af3b-fa5656db88ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 93,
                "offset": 4,
                "shift": 23,
                "w": 17,
                "x": 633,
                "y": 192
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6b61622f-1b98-4b20-b17f-4fd973ff71b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 93,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 613,
                "y": 192
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8a9a3a8e-e20d-41d1-a71a-9ecc5343707c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 93,
                "offset": 5,
                "shift": 32,
                "w": 22,
                "x": 361,
                "y": 192
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "44a58b86-bade-4c90-87cd-acec0f20c34d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 93,
                "offset": 7,
                "shift": 48,
                "w": 33,
                "x": 182,
                "y": 97
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "191ef6af-2be2-4f05-92f8-4ac27f72982c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 93,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 706,
                "y": 192
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e7714a32-7a48-4754-908a-27d3931b6cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 93,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 760,
                "y": 192
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "10a7ff74-26c9-4601-8795-17a28bf2c0dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 93,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 670,
                "y": 192
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6e0c6e80-7bfe-424f-ad9b-bf03e4fce8e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 93,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 450,
                "y": 192
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2f183e9c-d35b-4454-b368-aef55e5cd37e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 93,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 629,
                "y": 97
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2083078a-f120-46a7-959d-a3b46184b45e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 93,
                "offset": 12,
                "shift": 36,
                "w": 13,
                "x": 824,
                "y": 192
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "71b4a218-c203-4982-bc75-f4afdd088c18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 93,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 797,
                "y": 97
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f2fffa25-51a0-41fa-b276-a495230e3f5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 93,
                "offset": 6,
                "shift": 36,
                "w": 24,
                "x": 260,
                "y": 192
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "24e4d818-61f0-4057-85e5-a98d9e4e23f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 93,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 595,
                "y": 97
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "16cb4db1-d134-4f4c-94af-d6d63288bff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 93,
                "offset": 5,
                "shift": 36,
                "w": 26,
                "x": 152,
                "y": 192
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5ccae5ca-00b0-4f3f-9853-e389629aa37d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 93,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 38,
                "y": 97
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "37843eb3-6d9d-469d-a325-d4a4b501afb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 93,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 862,
                "y": 97
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "dca1e4fd-274e-4595-954a-5affca3b4e4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 93,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 731,
                "y": 97
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "292c0a1c-67f0-4e36-91b0-483bd54e1f9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 93,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 971,
                "y": 2
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9fe92dbd-45d4-4738-bab1-7494d09d818a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 93,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 724,
                "y": 192
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9ec2121a-c7c4-4d15-932e-c145aa4940d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 93,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 742,
                "y": 192
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6f4c5bd7-fd27-4376-a9c4-9aa818c14fa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 93,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 122,
                "y": 192
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4e5fce59-930a-4a77-bda5-70f7e17aaa3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 93,
                "offset": 7,
                "shift": 48,
                "w": 33,
                "x": 217,
                "y": 97
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "39131be0-5451-430d-8edd-5390dc319152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 93,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 2,
                "y": 192
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "86b6e003-879c-493a-ba0c-8efc340dc118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 93,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 988,
                "y": 97
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "85932e1b-fd1b-4788-a468-da304b1bcb5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 93,
                "offset": 2,
                "shift": 48,
                "w": 44,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ef98fb6b-b337-432e-91fa-43df169146c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 93,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 697,
                "y": 97
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c331c744-3e42-40cc-bf66-7b4e9b20f0dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 93,
                "offset": 4,
                "shift": 37,
                "w": 32,
                "x": 663,
                "y": 97
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1125d0e1-d3ac-480a-8975-e14e72770d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 93,
                "offset": 1,
                "shift": 48,
                "w": 46,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a402c6e5-c18d-4763-84e8-7267195c71a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 93,
                "offset": 4,
                "shift": 47,
                "w": 42,
                "x": 434,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "052a996b-077b-40ea-a0b2-bc9aa1b6c6b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 93,
                "offset": 3,
                "shift": 31,
                "w": 25,
                "x": 207,
                "y": 192
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c96d3357-7cca-4f8e-970c-7ac32c2e20a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 93,
                "offset": 3,
                "shift": 31,
                "w": 30,
                "x": 894,
                "y": 97
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1611f569-427d-4708-a945-05e4c085bd4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 93,
                "offset": 1,
                "shift": 39,
                "w": 38,
                "x": 638,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "152c032d-7453-4b5a-b187-6bc74d3ad29f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 93,
                "offset": 4,
                "shift": 40,
                "w": 32,
                "x": 459,
                "y": 97
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2a0da94f-908c-4f4b-b7f2-7f23075c0210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 93,
                "offset": 4,
                "shift": 21,
                "w": 13,
                "x": 839,
                "y": 192
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c9de8b81-74f1-481d-9c50-aba213798dbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 93,
                "offset": -1,
                "shift": 19,
                "w": 18,
                "x": 593,
                "y": 192
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f8eb820b-d7cb-49f1-a363-93bb206e102b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 93,
                "offset": 4,
                "shift": 41,
                "w": 38,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9bc1a841-6651-4c89-80d6-8520b5fe21f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 93,
                "offset": 3,
                "shift": 21,
                "w": 19,
                "x": 471,
                "y": 192
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "345d1f05-b2d4-4911-833e-cb92a7dab7e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 93,
                "offset": 3,
                "shift": 52,
                "w": 46,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d9dd3ade-a2f4-4402-8d5f-a375a6e49547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 93,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 425,
                "y": 97
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ecfe564f-2a02-42f9-b566-35e2272c808a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 93,
                "offset": 1,
                "shift": 48,
                "w": 46,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "349fc63c-ef38-4193-9a21-c63cb5d299f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 93,
                "offset": 3,
                "shift": 36,
                "w": 32,
                "x": 391,
                "y": 97
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a2af914a-5200-4a2c-9925-6172e5e27398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 93,
                "offset": 1,
                "shift": 48,
                "w": 46,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8fc9cc7a-d702-4c88-a8cf-adc4514bd4d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 93,
                "offset": 4,
                "shift": 38,
                "w": 35,
                "x": 754,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "79a9ec56-d736-4f1d-b5e1-9c1750c2ca20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 93,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 286,
                "y": 192
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a6d6349c-a718-484e-895e-4ed99f5042cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 93,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 322,
                "y": 97
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "47b4f5d2-441c-4791-8c79-ee7f4bec0841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 93,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 357,
                "y": 97
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9ed60eec-1c59-4fba-8c03-c7c3b46c28f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 93,
                "offset": -3,
                "shift": 33,
                "w": 38,
                "x": 518,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "337ff12e-d77e-4208-a1b9-6c0606b76af9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 93,
                "offset": 3,
                "shift": 52,
                "w": 46,
                "x": 248,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ab179841-8a4e-4b3f-8d5e-174e0d15df7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 93,
                "offset": -3,
                "shift": 32,
                "w": 38,
                "x": 558,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c13bc696-0273-43a8-9fab-7b0970a8b8b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 93,
                "offset": -3,
                "shift": 32,
                "w": 38,
                "x": 598,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e860a614-bfac-4796-9a42-172772b3a614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 93,
                "offset": 0,
                "shift": 33,
                "w": 34,
                "x": 2,
                "y": 97
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7bb81387-b6dd-4b28-a7df-87a61257a9bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 93,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 429,
                "y": 192
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d456bbbd-8676-4717-a3a2-4eb478db2d59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 93,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 573,
                "y": 192
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "d007b8eb-e431-41c3-8509-f0f91e62bde7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 93,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 492,
                "y": 192
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7462934c-78e0-4dd6-8979-8b648e9c1fbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 93,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 493,
                "y": 97
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "23c0a539-40e1-41e4-a928-ae4e6bf9ee3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 93,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 527,
                "y": 97
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e3ff735a-98cf-4a14-9a0d-fff6709dc42d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 93,
                "offset": 6,
                "shift": 32,
                "w": 15,
                "x": 777,
                "y": 192
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "16381172-4b29-48f8-aeec-5209ca3ac01c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 93,
                "offset": 1,
                "shift": 38,
                "w": 34,
                "x": 146,
                "y": 97
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "da6e1d61-4087-4195-8875-0a34ea278f24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 93,
                "offset": 2,
                "shift": 37,
                "w": 34,
                "x": 791,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "52c8fc78-ef2e-48e7-be0c-dc82b74d244e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 93,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 935,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "483a68ad-973f-41ef-b31a-41241a1f98e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 93,
                "offset": 1,
                "shift": 37,
                "w": 34,
                "x": 110,
                "y": 97
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a90a2693-e796-4cbe-8f7e-f0fe6a94508a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 93,
                "offset": 1,
                "shift": 35,
                "w": 34,
                "x": 74,
                "y": 97
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b600e504-b8af-4adb-8cbc-aa4ed411490c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 93,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 553,
                "y": 192
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ff9d0c76-d7fe-4e0c-b78a-0d6cfd312c26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 93,
                "offset": 1,
                "shift": 37,
                "w": 33,
                "x": 287,
                "y": 97
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "43f0de00-f46f-4fe0-8b59-f0c55f3c8fb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 93,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 92,
                "y": 192
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "931fa41c-aff5-4b94-a636-0aa8e3f33f14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 93,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 809,
                "y": 192
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "730ac093-126a-416d-aeba-2170d5ec899a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 93,
                "offset": -5,
                "shift": 23,
                "w": 25,
                "x": 180,
                "y": 192
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ad3bb1fe-1eb0-47da-a7dc-79517a966339",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 93,
                "offset": 3,
                "shift": 35,
                "w": 33,
                "x": 252,
                "y": 97
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "ccb90c23-bdd1-4e46-bded-bbdfe1bdac46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 93,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 794,
                "y": 192
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9b29e77d-2c52-4a37-b82c-7fe4aa22f341",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 93,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 388,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d2636b47-091f-41ca-89f9-38a45e53d418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 93,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 926,
                "y": 97
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "842fa699-b84a-433d-a4d8-ff535de94998",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 93,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 899,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a8273ebd-789d-4453-a3ed-f581ecdd27f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 93,
                "offset": 2,
                "shift": 37,
                "w": 34,
                "x": 863,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "eb748347-471c-41b6-908b-9446ea7d3aba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 93,
                "offset": 1,
                "shift": 37,
                "w": 34,
                "x": 827,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "75f0193e-b5e4-43a2-beb5-ec7116cef4a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 93,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 533,
                "y": 192
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f77baaaa-e972-41b1-86d4-920bd853cff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 93,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 336,
                "y": 192
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "92f08cf8-b8f6-4e78-a7c2-c2a8797c67d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 93,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 513,
                "y": 192
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "20cbc83b-0940-4f1e-917f-ab6d6c552528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 93,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 957,
                "y": 97
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "693dd63f-18a6-4c93-a51d-8ad5d844262c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 93,
                "offset": -3,
                "shift": 25,
                "w": 30,
                "x": 830,
                "y": 97
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4d5dba2a-1e52-4c3c-b4cd-4126461afbfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 93,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 296,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "62937fd1-40d8-4556-ba83-cababac01ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 93,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 62,
                "y": 192
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2f6a144d-f913-40b2-abf0-7d8daabf4312",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 93,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 32,
                "y": 192
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f9beebd2-1737-41b9-8e18-064a01de69fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 93,
                "offset": 0,
                "shift": 32,
                "w": 31,
                "x": 764,
                "y": 97
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ce9b1303-ef2c-4023-8008-643ec1bdc9ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 93,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 385,
                "y": 192
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b0705473-6efe-4fd0-94ab-3a4744cf0879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 93,
                "offset": 10,
                "shift": 32,
                "w": 12,
                "x": 854,
                "y": 192
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b5d5bb0d-9513-4e31-b395-cd3be85d5de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 93,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 407,
                "y": 192
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "fcbd25b8-9646-4356-be20-f86eb7cf181a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 93,
                "offset": 5,
                "shift": 43,
                "w": 32,
                "x": 561,
                "y": 97
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 48,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}