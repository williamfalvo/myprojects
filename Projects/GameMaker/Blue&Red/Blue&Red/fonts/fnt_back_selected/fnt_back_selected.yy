{
    "id": "169a1e0c-8873-4707-bc33-50afdd533d1d",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_back_selected",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3786d779-e78c-4f6a-9b43-7c5b9f5a427d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 97,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 801,
                "y": 200
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "436bab0a-71ad-4221-ac4d-70e21e2732fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 97,
                "offset": 6,
                "shift": 29,
                "w": 17,
                "x": 782,
                "y": 200
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a73bc23a-9a83-485c-a169-f8d3592910c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 97,
                "offset": 4,
                "shift": 34,
                "w": 26,
                "x": 345,
                "y": 200
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "562aaa2b-ec18-45c4-837d-bab0e8247093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 97,
                "offset": 3,
                "shift": 45,
                "w": 39,
                "x": 624,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ba5d06b1-967e-4bcf-9865-2480bcdabce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 97,
                "offset": 7,
                "shift": 38,
                "w": 24,
                "x": 427,
                "y": 200
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d293e3b8-9ede-45a4-be51-0435f36ae9de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 97,
                "offset": 4,
                "shift": 62,
                "w": 54,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2b451663-7e7b-4abb-ae1a-40edce7fd4cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 97,
                "offset": 7,
                "shift": 51,
                "w": 37,
                "x": 747,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "63e9caaa-3ed5-42ca-9ad8-501cacd75a1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 97,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 1007,
                "y": 200
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "fb875cbb-bd4f-4701-ac6f-d5364e194b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 97,
                "offset": 4,
                "shift": 25,
                "w": 18,
                "x": 762,
                "y": 200
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "89f0bd3d-e261-41f3-aecb-be112d6f45b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 97,
                "offset": 2,
                "shift": 25,
                "w": 18,
                "x": 742,
                "y": 200
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3d1ab39e-2b8c-449b-96f7-27562b824bbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 97,
                "offset": 6,
                "shift": 34,
                "w": 22,
                "x": 479,
                "y": 200
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e9faaaf8-2395-4dca-b5f3-1e9ed2345aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 97,
                "offset": 8,
                "shift": 50,
                "w": 34,
                "x": 550,
                "y": 101
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bc566a19-a732-44df-936b-7e0623a4357b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 97,
                "offset": 4,
                "shift": 25,
                "w": 17,
                "x": 820,
                "y": 200
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "06ad189e-e0de-448d-bcf1-3091db160179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 97,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 913,
                "y": 200
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d898b659-985f-4fa1-b9e1-7ab3223bcc0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 97,
                "offset": 4,
                "shift": 25,
                "w": 17,
                "x": 839,
                "y": 200
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5566f275-1e0b-4337-837f-6251bc51a2c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 97,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 571,
                "y": 200
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "fdebd81a-fe94-49ec-b3e6-bb83bb291faf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 97,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 796,
                "y": 101
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "62c97940-6abf-46b7-87d0-05dd9d6d9fd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 97,
                "offset": 12,
                "shift": 38,
                "w": 14,
                "x": 930,
                "y": 200
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "617d9965-4d38-4760-8285-9ff4cfb1f6ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 97,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 966,
                "y": 101
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6a9d0935-2d85-4d15-a92a-5af7f20c131a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 97,
                "offset": 7,
                "shift": 38,
                "w": 25,
                "x": 400,
                "y": 200
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "90d34645-9eb6-4598-a7b9-8b026eb760ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 97,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 478,
                "y": 101
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "408603c6-cbb8-4178-a01f-d6b8077f1675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 97,
                "offset": 5,
                "shift": 38,
                "w": 28,
                "x": 258,
                "y": 200
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b9d5fdb1-5316-480b-a477-92d749f5445f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 97,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 224,
                "y": 101
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1818c3f7-d450-49e3-b822-113e8b659265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 97,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 898,
                "y": 101
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "86af5970-c831-479c-9692-b6b89f5fd691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 97,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 932,
                "y": 101
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6cda96d4-e01d-45f4-b084-0e4699197e42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 97,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 2,
                "y": 101
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "471fe1b0-e544-4572-8f12-b2165a5786be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 97,
                "offset": 4,
                "shift": 25,
                "w": 17,
                "x": 858,
                "y": 200
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0e5ac283-2655-4eb2-9283-17ce072a6835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 97,
                "offset": 4,
                "shift": 25,
                "w": 17,
                "x": 877,
                "y": 200
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "777968cd-aa18-44e0-a32d-88a37625722e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 97,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 226,
                "y": 200
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ba9ec250-c816-4bec-b9f3-5e6243d4edc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 97,
                "offset": 8,
                "shift": 50,
                "w": 34,
                "x": 406,
                "y": 101
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d8c7b9a6-99fa-43af-a7d7-dfc748844055",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 97,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 130,
                "y": 200
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "38106c5e-a184-40fb-9821-2326fac7f862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 97,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 162,
                "y": 200
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ffe82fb5-946f-41ca-929d-07b0843b2305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 97,
                "offset": 2,
                "shift": 50,
                "w": 46,
                "x": 356,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "582c86bc-a0c6-4712-bd7b-08072112fb5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 97,
                "offset": 3,
                "shift": 40,
                "w": 34,
                "x": 370,
                "y": 101
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ad8088a8-4c52-4a83-acd7-4c1c5780dc5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 97,
                "offset": 4,
                "shift": 38,
                "w": 34,
                "x": 334,
                "y": 101
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "399fb180-db27-4505-9cd9-b2d9b1eb7cc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 97,
                "offset": 1,
                "shift": 50,
                "w": 48,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "828249f5-bf51-4340-b234-688af271dd4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 97,
                "offset": 4,
                "shift": 49,
                "w": 44,
                "x": 452,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a70e6461-45f0-40c6-af8e-aceb920773ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 97,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 288,
                "y": 200
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e4eb00f6-079c-44f6-92c2-2f767586dee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 97,
                "offset": 3,
                "shift": 32,
                "w": 32,
                "x": 864,
                "y": 101
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "bec0c5b3-bb18-4c11-bef6-58132f17adbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 97,
                "offset": 1,
                "shift": 41,
                "w": 40,
                "x": 498,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8e35bfbd-07d4-4213-977f-68533112862f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 97,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 761,
                "y": 101
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4f21ff2f-6b20-44a1-b2ab-4eca35c7d2ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 97,
                "offset": 4,
                "shift": 22,
                "w": 14,
                "x": 946,
                "y": 200
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ed55ce70-7ea4-4f4a-bd7e-794b8a3f10bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 97,
                "offset": -1,
                "shift": 20,
                "w": 19,
                "x": 658,
                "y": 200
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d4f9c9de-d02c-4721-b578-78edff17127e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 97,
                "offset": 4,
                "shift": 43,
                "w": 40,
                "x": 582,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0bae7086-8c55-490a-a396-0070a9c8c14a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 97,
                "offset": 3,
                "shift": 22,
                "w": 20,
                "x": 615,
                "y": 200
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "bc6b88bd-f2d3-4242-a083-3381ce61a45c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 97,
                "offset": 3,
                "shift": 54,
                "w": 48,
                "x": 258,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "22c23760-a9e1-4cfb-9d3d-f89d46b21e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 97,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 691,
                "y": 101
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "62b5d8e4-57ce-4396-a2c4-ac7fdceea10a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 97,
                "offset": 1,
                "shift": 50,
                "w": 48,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "02083a65-dc3e-486a-9550-002988e299e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 97,
                "offset": 3,
                "shift": 38,
                "w": 34,
                "x": 298,
                "y": 101
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "aa0caef0-0565-48a9-b9f2-bffffc56464b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 97,
                "offset": 1,
                "shift": 50,
                "w": 48,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b2f73571-7096-4840-9597-d4cb13fba19f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 97,
                "offset": 4,
                "shift": 40,
                "w": 37,
                "x": 786,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c047552a-6573-4c46-b8b5-3460bc0387e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 97,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 453,
                "y": 200
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "455263e9-eb9c-44e1-837e-2d25a4497d43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 97,
                "offset": 0,
                "shift": 34,
                "w": 35,
                "x": 936,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "991b4db9-ca11-4a1d-93ba-928baa30f8cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 97,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 621,
                "y": 101
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a7dafaae-47dd-49ea-a510-bb939c2dc669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 97,
                "offset": -3,
                "shift": 35,
                "w": 40,
                "x": 540,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "84ef76c3-8085-4662-ab69-98f31c811e4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 97,
                "offset": 3,
                "shift": 54,
                "w": 48,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4adec73d-f231-4159-a071-f3ed1ff6a9c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 97,
                "offset": -3,
                "shift": 33,
                "w": 39,
                "x": 665,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b0f439e7-a911-43fa-aff7-2e2726b7f8ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 97,
                "offset": -3,
                "shift": 34,
                "w": 39,
                "x": 706,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7f2f6c18-bdd1-475a-9518-b8030b13eb7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 97,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 76,
                "y": 101
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "210c501f-8346-4cf3-ac29-d9efd10a727f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 97,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 593,
                "y": 200
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2ab878e4-aade-4ad7-a4b4-4e290715da32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 97,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 637,
                "y": 200
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "393c7edf-4aba-4699-9ffc-84e2431a9c44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 97,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 549,
                "y": 200
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7008d0bd-9c74-4695-b72d-5f6fe2e13ee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 97,
                "offset": 0,
                "shift": 34,
                "w": 33,
                "x": 586,
                "y": 101
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1a1a8802-a587-4b12-a73b-e597412cb7e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 97,
                "offset": 0,
                "shift": 34,
                "w": 34,
                "x": 514,
                "y": 101
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "fbf68590-ede9-4a4d-a8b9-591a6651b203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 97,
                "offset": 7,
                "shift": 34,
                "w": 15,
                "x": 896,
                "y": 200
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "47d86042-a311-4d7d-83f4-3c273253e931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 97,
                "offset": 1,
                "shift": 39,
                "w": 35,
                "x": 261,
                "y": 101
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f1fc0d98-a0f1-47ce-837d-457e27a9ab8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 97,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 187,
                "y": 101
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "79844d2d-3d31-419d-a940-c94d5e7614d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 97,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 150,
                "y": 101
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "16e09175-cb3b-4f2f-a3bd-c415b368afea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 97,
                "offset": 1,
                "shift": 39,
                "w": 35,
                "x": 113,
                "y": 101
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5b4ff8c6-8984-4f1e-880b-035560b003ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 97,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 825,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2888b006-1a56-421f-9454-ed0e410cc12d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 97,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 679,
                "y": 200
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5958c956-f343-4e32-ad63-9cdb7ea938f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 97,
                "offset": 1,
                "shift": 39,
                "w": 34,
                "x": 442,
                "y": 101
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d4d43bdd-0049-4d12-ad6f-66522074174d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 97,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 2,
                "y": 200
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a78e7453-9024-4dfb-8b94-28506698cd7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 97,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 977,
                "y": 200
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ab04ec06-d884-4acc-a01c-952217e5912d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 97,
                "offset": -5,
                "shift": 24,
                "w": 26,
                "x": 317,
                "y": 200
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "63e1084b-22dc-4ed5-8d65-5069c3d1048a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 97,
                "offset": 3,
                "shift": 36,
                "w": 35,
                "x": 973,
                "y": 2
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "23860089-5c99-4f51-8539-d923e606aaea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 97,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 962,
                "y": 200
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a78fe012-fbd4-4504-9eeb-bb91975f5661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 97,
                "offset": 3,
                "shift": 52,
                "w": 46,
                "x": 404,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "dba5bb31-2607-428e-b8ee-7feff026f190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 97,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 194,
                "y": 200
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "acc9b817-dbde-4264-810c-972b25c2b21c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 97,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 39,
                "y": 101
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "cddb05e0-0337-48d1-9cbb-6bc4d0cdf777",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 97,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 899,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c9b6baf4-fb6a-488e-85ca-ed6f589cd797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 97,
                "offset": 1,
                "shift": 39,
                "w": 35,
                "x": 862,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "608c9a90-af5f-4869-b1a6-c75e4252d3fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 97,
                "offset": 3,
                "shift": 22,
                "w": 19,
                "x": 721,
                "y": 200
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2a31e913-e9f5-461e-886c-a243e9f51a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 97,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 373,
                "y": 200
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "1c4e3185-445f-4b09-9900-c9acbcf70251",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 97,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 700,
                "y": 200
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "381bca93-0289-456b-a76a-56e399a4d65e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 97,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 34,
                "y": 200
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "67401752-efce-495a-b863-c38259d2c3b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 97,
                "offset": -3,
                "shift": 26,
                "w": 32,
                "x": 830,
                "y": 101
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f13c7d52-7e60-421f-a5cf-101818702a44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 97,
                "offset": 3,
                "shift": 52,
                "w": 46,
                "x": 308,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d5e6cddb-06c5-4cc9-9018-72bc2e68b376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 97,
                "offset": 3,
                "shift": 35,
                "w": 30,
                "x": 98,
                "y": 200
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "49728b08-e4ee-4997-b076-6e27e4f4fb61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 97,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 66,
                "y": 200
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "337d8015-859e-47ec-8d09-8d43417ea606",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 97,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 656,
                "y": 101
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "de1f93f6-a680-4a3e-959c-c2c845643fab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 97,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 526,
                "y": 200
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "fea4aaf6-ee62-4f5b-93e4-f8f0114a6138",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 97,
                "offset": 10,
                "shift": 34,
                "w": 13,
                "x": 992,
                "y": 200
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c0e74946-5b51-4594-9157-b8be46c5fd95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 97,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 503,
                "y": 200
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c1fe51d7-8093-436c-a995-71478d9b0861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 97,
                "offset": 6,
                "shift": 45,
                "w": 33,
                "x": 726,
                "y": 101
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 50,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}