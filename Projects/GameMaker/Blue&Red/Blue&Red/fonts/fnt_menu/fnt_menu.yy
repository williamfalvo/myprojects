{
    "id": "d25b7165-39b4-446a-bd30-bd7e6e80c379",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4be9a218-357e-42a0-93df-f823c05a51e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 93,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 688,
                "y": 192
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4fcfff7c-c183-4f34-9796-4c088df71d5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 93,
                "offset": 6,
                "shift": 28,
                "w": 16,
                "x": 652,
                "y": 192
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9be2957a-cfdf-435e-bf18-ec7761da3a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 93,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 234,
                "y": 192
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cc726872-8d44-4c7f-82b4-8264fd811916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 93,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 678,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "45dad0a8-8f4a-46ed-b309-6433a1dc3a47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 93,
                "offset": 7,
                "shift": 36,
                "w": 23,
                "x": 311,
                "y": 192
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8e663ce6-9181-43f6-99e4-3bd4c05d2128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 93,
                "offset": 4,
                "shift": 59,
                "w": 52,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1dcdf761-700e-428b-b554-a47d4de897c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 93,
                "offset": 7,
                "shift": 49,
                "w": 35,
                "x": 717,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "99583891-0f9d-4065-b1ab-4e15cd76b70f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 93,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 868,
                "y": 192
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9f473504-0c58-4911-9b72-e665e28f8153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 93,
                "offset": 4,
                "shift": 23,
                "w": 17,
                "x": 633,
                "y": 192
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7c2193e3-c7b1-4512-b29c-dc3bb1a7c370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 93,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 613,
                "y": 192
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e257475e-bb8f-43c0-987c-a968af947947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 93,
                "offset": 5,
                "shift": 32,
                "w": 22,
                "x": 361,
                "y": 192
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0669422e-d120-4408-9e4b-5e696cb43161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 93,
                "offset": 7,
                "shift": 48,
                "w": 33,
                "x": 182,
                "y": 97
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e2b2f351-d2f6-493d-9b19-9d8adb26cd22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 93,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 706,
                "y": 192
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d88f959c-f74e-4993-9b87-ade8b05f4c17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 93,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 760,
                "y": 192
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7deeba4c-543b-4e45-828a-a980283b6911",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 93,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 670,
                "y": 192
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "79ce2e32-173e-4398-8aa2-737b9a0cb223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 93,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 450,
                "y": 192
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "26649d7f-d040-4e4d-b597-36ab2e161a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 93,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 629,
                "y": 97
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "70fba39e-dc09-43c2-b233-85952c25bc27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 93,
                "offset": 12,
                "shift": 36,
                "w": 13,
                "x": 824,
                "y": 192
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8af74156-820f-45a9-900f-27ffff850b82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 93,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 797,
                "y": 97
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b46c347e-759a-4017-bd15-318136bca6a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 93,
                "offset": 6,
                "shift": 36,
                "w": 24,
                "x": 260,
                "y": 192
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d5929916-0887-4366-8ea8-152acc02a838",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 93,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 595,
                "y": 97
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "06263d99-07e1-4091-a4bb-9ab2cb501061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 93,
                "offset": 5,
                "shift": 36,
                "w": 26,
                "x": 152,
                "y": 192
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9c114bc6-bdf3-441c-8bbf-334d5705c761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 93,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 38,
                "y": 97
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "bfe0eb9f-fca1-4087-ab35-6172d6de4ab6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 93,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 862,
                "y": 97
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f0fed732-0e93-4c5d-a369-ccae647e4f57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 93,
                "offset": 3,
                "shift": 36,
                "w": 31,
                "x": 731,
                "y": 97
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "37af76df-0127-4e36-b0e4-aff9e134687f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 93,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 971,
                "y": 2
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8258e418-a264-4f48-8ec0-8248f9a09b95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 93,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 724,
                "y": 192
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "10c3644d-9f87-4f1f-92f1-32810c5d3794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 93,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 742,
                "y": 192
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "50e0cdaa-167e-4bba-ba94-30dccfce2dd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 93,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 122,
                "y": 192
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "923c0762-7d8d-40b0-a8ce-9c8eb1d2c148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 93,
                "offset": 7,
                "shift": 48,
                "w": 33,
                "x": 217,
                "y": 97
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6a15bdde-8ea6-4d87-98fb-7e236c1b6d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 93,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 2,
                "y": 192
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6d3b5db4-3d88-43a4-b733-49ab2b2ffeda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 93,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 988,
                "y": 97
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a24f9bfd-9136-4493-b815-0cfbd59baf0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 93,
                "offset": 2,
                "shift": 48,
                "w": 44,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f72fbd4c-88b3-4bfb-9c4e-d437df6ee0b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 93,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 697,
                "y": 97
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "642d03ca-94b0-49b8-9a8d-51dda7cf96a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 93,
                "offset": 4,
                "shift": 37,
                "w": 32,
                "x": 663,
                "y": 97
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8aaf9baf-5265-4255-8f41-1a6291e523e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 93,
                "offset": 1,
                "shift": 48,
                "w": 46,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "dc825e08-5785-407d-99b3-7ed480bbbb33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 93,
                "offset": 4,
                "shift": 47,
                "w": 42,
                "x": 434,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d14a5544-9bd2-4158-adfc-27ed44f9d85e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 93,
                "offset": 3,
                "shift": 31,
                "w": 25,
                "x": 207,
                "y": 192
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "17ff888b-5c14-4e70-9975-251af881805e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 93,
                "offset": 3,
                "shift": 31,
                "w": 30,
                "x": 894,
                "y": 97
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "987244da-ed23-43a4-8d4b-e26958afa7a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 93,
                "offset": 1,
                "shift": 39,
                "w": 38,
                "x": 638,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fbedd937-5198-4f12-ae5e-d21f9aac136a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 93,
                "offset": 4,
                "shift": 40,
                "w": 32,
                "x": 459,
                "y": 97
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "90122740-7b3a-410f-a2f8-54ea094a5f58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 93,
                "offset": 4,
                "shift": 21,
                "w": 13,
                "x": 839,
                "y": 192
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1c3e5764-798c-4ae0-9328-091240177f87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 93,
                "offset": -1,
                "shift": 19,
                "w": 18,
                "x": 593,
                "y": 192
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "97434f07-cf69-4fca-a06e-e65de2318756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 93,
                "offset": 4,
                "shift": 41,
                "w": 38,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c5341257-2450-433a-a6e0-ce8a297d3fe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 93,
                "offset": 3,
                "shift": 21,
                "w": 19,
                "x": 471,
                "y": 192
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "955d26c8-c16d-4e10-a81d-2edc3abcc2f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 93,
                "offset": 3,
                "shift": 52,
                "w": 46,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7cef96d9-163e-4f0a-8d3d-fc61742bd557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 93,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 425,
                "y": 97
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2d924b13-6cdf-448e-af75-d8b68fc2eab9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 93,
                "offset": 1,
                "shift": 48,
                "w": 46,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3310a9cc-3cf7-4da8-923e-4c2e5a8f2551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 93,
                "offset": 3,
                "shift": 36,
                "w": 32,
                "x": 391,
                "y": 97
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a0c804cb-2195-454d-8c04-312be23b7256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 93,
                "offset": 1,
                "shift": 48,
                "w": 46,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1785d692-6082-487d-9802-0330f5c7002d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 93,
                "offset": 4,
                "shift": 38,
                "w": 35,
                "x": 754,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e705f120-7a3d-450d-934c-e3da5dc1022c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 93,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 286,
                "y": 192
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d1109c7c-702d-4dd0-bc61-adf3a47651d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 93,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 322,
                "y": 97
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c9e99066-9939-4657-b0f2-152a7a6e2a59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 93,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 357,
                "y": 97
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a0677aa4-4744-4748-a7d3-7cb18a26dcb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 93,
                "offset": -3,
                "shift": 33,
                "w": 38,
                "x": 518,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8e69fcab-c809-4e8f-9590-b4c9bd96d904",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 93,
                "offset": 3,
                "shift": 52,
                "w": 46,
                "x": 248,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4c00093b-7782-4dbd-9e82-42d9a1fab4f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 93,
                "offset": -3,
                "shift": 32,
                "w": 38,
                "x": 558,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "465124f1-4210-40bf-81c6-21d61fa786ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 93,
                "offset": -3,
                "shift": 32,
                "w": 38,
                "x": 598,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9b09a5da-5cf7-40a2-a99c-e74ab22a21b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 93,
                "offset": 0,
                "shift": 33,
                "w": 34,
                "x": 2,
                "y": 97
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "bb5ab173-8716-4087-8e04-6705726cc1e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 93,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 429,
                "y": 192
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d5e53b24-95c9-4c46-bc9c-957d36e100c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 93,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 573,
                "y": 192
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "49535911-a9df-4d8a-9bb4-b345b49382e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 93,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 492,
                "y": 192
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ba76db72-a489-4a8c-a094-47113d77cad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 93,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 493,
                "y": 97
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4132a0ea-756f-4146-99a9-55b03536b66b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 93,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 527,
                "y": 97
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2e330ae9-e6dc-444a-9f3a-10add6b46b95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 93,
                "offset": 6,
                "shift": 32,
                "w": 15,
                "x": 777,
                "y": 192
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2d96083c-eacf-449e-8873-8fa147a00c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 93,
                "offset": 1,
                "shift": 38,
                "w": 34,
                "x": 146,
                "y": 97
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f8727a66-5744-46c2-b88c-f8d219fcf86d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 93,
                "offset": 2,
                "shift": 37,
                "w": 34,
                "x": 791,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "457af2e4-7b73-4469-b1f5-59c4fa3a3503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 93,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 935,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f23174fb-0de7-4c00-a4f9-83ae2e2abbfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 93,
                "offset": 1,
                "shift": 37,
                "w": 34,
                "x": 110,
                "y": 97
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8108976a-f93b-4cc3-9f2d-f11a5de66d30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 93,
                "offset": 1,
                "shift": 35,
                "w": 34,
                "x": 74,
                "y": 97
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c49c70c2-3ac9-4da2-9f46-78dc42d3b153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 93,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 553,
                "y": 192
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d46af781-1367-4a63-a9be-4cddb3130e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 93,
                "offset": 1,
                "shift": 37,
                "w": 33,
                "x": 287,
                "y": 97
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5cd4e8fd-e5b4-495f-a340-8b699b77e688",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 93,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 92,
                "y": 192
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "04bdff5c-5b6f-4992-9c24-472da4412645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 93,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 809,
                "y": 192
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d99aa9b8-070a-4e5e-9037-979100daa1d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 93,
                "offset": -5,
                "shift": 23,
                "w": 25,
                "x": 180,
                "y": 192
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7921f8de-e615-4c39-8072-bb3cd9f8584c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 93,
                "offset": 3,
                "shift": 35,
                "w": 33,
                "x": 252,
                "y": 97
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5b12d6f6-1e7f-4968-aad9-96411628a964",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 93,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 794,
                "y": 192
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0928ef15-686f-47fe-859f-2d27aac391bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 93,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 388,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "aef8120b-8cbc-41b2-9cfd-51923e5fe237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 93,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 926,
                "y": 97
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "28e43c68-5531-47fc-9d39-5ecb82b8bbd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 93,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 899,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "716edb87-32c0-4ac6-b048-67a724d0a5fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 93,
                "offset": 2,
                "shift": 37,
                "w": 34,
                "x": 863,
                "y": 2
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "379a7dd3-dc7a-4174-88f7-cddcb2890c04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 93,
                "offset": 1,
                "shift": 37,
                "w": 34,
                "x": 827,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d1778619-42c4-4eba-8aa0-27b528d45f48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 93,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 533,
                "y": 192
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "20131432-6db8-41f6-a193-db4ca202a3a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 93,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 336,
                "y": 192
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8504fb91-f6cd-4a8b-a0f0-0f83e3686778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 93,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 513,
                "y": 192
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "41ffcf83-a3cc-43b0-b958-ce7dd8ce0878",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 93,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 957,
                "y": 97
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0158fb69-85dc-4f41-a52c-11d46e413c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 93,
                "offset": -3,
                "shift": 25,
                "w": 30,
                "x": 830,
                "y": 97
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d0372df9-2ca8-404b-81f1-a242f85bc439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 93,
                "offset": 3,
                "shift": 50,
                "w": 44,
                "x": 296,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fbecf168-d0d3-40a5-bd8e-cafc06bb8043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 93,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 62,
                "y": 192
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "37ae5fe8-fb2e-4b97-aa8c-0e5ddac43159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 93,
                "offset": 3,
                "shift": 34,
                "w": 28,
                "x": 32,
                "y": 192
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "13370a11-cdb8-49ed-9e82-80dc433bd3f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 93,
                "offset": 0,
                "shift": 32,
                "w": 31,
                "x": 764,
                "y": 97
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f5979032-fff3-49f2-a68d-09b24bae9061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 93,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 385,
                "y": 192
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "77290271-9b22-4f6a-ac7d-a6c494e11d27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 93,
                "offset": 10,
                "shift": 32,
                "w": 12,
                "x": 854,
                "y": 192
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6a44b2b6-2d29-4187-9833-4a48f26727bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 93,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 407,
                "y": 192
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ac36f5b6-8270-4e52-bed7-2bdd8e24ddf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 93,
                "offset": 5,
                "shift": 43,
                "w": 32,
                "x": 561,
                "y": 97
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 48,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}