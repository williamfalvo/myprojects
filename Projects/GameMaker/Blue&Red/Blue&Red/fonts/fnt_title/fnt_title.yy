{
    "id": "64fc7523-f2c8-4861-8bac-80afe667c6c8",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_title",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "44236d82-2227-44b0-9be3-1fc3401d00e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 156,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 351,
                "y": 634
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ab4582e9-dc7b-4603-b69e-904818942b26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 156,
                "offset": 10,
                "shift": 46,
                "w": 26,
                "x": 380,
                "y": 634
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "88ab7d19-52b9-4b56-96ed-c40860a7a6cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 156,
                "offset": 7,
                "shift": 54,
                "w": 40,
                "x": 683,
                "y": 476
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "be2a97e3-77bd-486d-90de-ce9454d0af44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 156,
                "offset": 6,
                "shift": 72,
                "w": 61,
                "x": 67,
                "y": 160
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ac402492-9925-411c-bafa-95aded5c9655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 156,
                "offset": 11,
                "shift": 61,
                "w": 39,
                "x": 807,
                "y": 476
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "94f397b2-ec4b-42a5-9458-e9e6c25deae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 156,
                "offset": 6,
                "shift": 99,
                "w": 87,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "95922fd4-f43f-41e5-94ca-23e050e0335f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 156,
                "offset": 12,
                "shift": 82,
                "w": 58,
                "x": 193,
                "y": 160
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0d5dbaaf-be47-4900-b978-244307894757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 156,
                "offset": 4,
                "shift": 27,
                "w": 19,
                "x": 687,
                "y": 634
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f92e817c-f3c9-4b0e-a22b-6e4e52de355f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 156,
                "offset": 7,
                "shift": 39,
                "w": 28,
                "x": 321,
                "y": 634
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4cd3779c-215e-43da-931a-3522eeb13e87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 156,
                "offset": 4,
                "shift": 39,
                "w": 28,
                "x": 291,
                "y": 634
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "fa3cbb1e-2c90-4166-87bb-f8747a353d83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 156,
                "offset": 9,
                "shift": 54,
                "w": 35,
                "x": 888,
                "y": 476
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3eba232a-601e-43a1-8dc3-2a90f2c6db55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 156,
                "offset": 13,
                "shift": 80,
                "w": 54,
                "x": 283,
                "y": 318
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c3dafb31-ba2f-48f9-b282-9d9a0be66626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 156,
                "offset": 7,
                "shift": 40,
                "w": 26,
                "x": 436,
                "y": 634
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a824c20f-8b44-4384-82c1-dba7773272c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 156,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 520,
                "y": 634
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "895fd4c7-8cc1-4925-ac6a-ca2028e1915b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 156,
                "offset": 7,
                "shift": 40,
                "w": 26,
                "x": 408,
                "y": 634
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "33a6a01b-92a4-48f6-a63f-addd22baa207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 156,
                "offset": 3,
                "shift": 37,
                "w": 30,
                "x": 165,
                "y": 634
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "454dd1a4-5917-475b-a020-991d983c2db7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 156,
                "offset": 4,
                "shift": 61,
                "w": 52,
                "x": 779,
                "y": 318
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "17ef9795-8f62-45c1-a2fa-737bcfb0fc35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 156,
                "offset": 20,
                "shift": 61,
                "w": 21,
                "x": 642,
                "y": 634
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c922c991-5092-4653-94d3-578e74059a02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 156,
                "offset": 5,
                "shift": 61,
                "w": 51,
                "x": 887,
                "y": 318
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "92eba3be-40b2-41ac-94ca-e8f0db13b056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 156,
                "offset": 11,
                "shift": 61,
                "w": 39,
                "x": 725,
                "y": 476
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7c6ac798-c33d-422c-a806-6f093a044510",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 156,
                "offset": 4,
                "shift": 61,
                "w": 53,
                "x": 614,
                "y": 318
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c6ab850f-98da-43c7-87cf-de2e596c90c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 156,
                "offset": 8,
                "shift": 61,
                "w": 44,
                "x": 550,
                "y": 476
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "24284852-518b-4b1b-b93f-4d2148ffdfeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 156,
                "offset": 3,
                "shift": 61,
                "w": 55,
                "x": 892,
                "y": 160
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "cf77347d-3d9a-4f16-b85c-ed40a0a63f6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 156,
                "offset": 6,
                "shift": 61,
                "w": 49,
                "x": 105,
                "y": 476
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c3e1502d-14c2-43e4-a7a3-40fc9f43befc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 156,
                "offset": 5,
                "shift": 61,
                "w": 51,
                "x": 940,
                "y": 318
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9cbb44cc-de15-4982-b201-ad89c3cd3939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 156,
                "offset": 3,
                "shift": 61,
                "w": 55,
                "x": 835,
                "y": 160
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c914479e-fc20-49f7-bf50-f829dd77b7c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 156,
                "offset": 7,
                "shift": 40,
                "w": 26,
                "x": 464,
                "y": 634
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "3634c13c-b2ff-4086-8b8c-61d48fff0e31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 156,
                "offset": 7,
                "shift": 40,
                "w": 26,
                "x": 492,
                "y": 634
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f18cb8c8-a4a5-4795-a1f1-8e69537d71ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 156,
                "offset": 3,
                "shift": 54,
                "w": 47,
                "x": 305,
                "y": 476
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a3cd71a9-88d1-414d-aad7-49d1257622ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 156,
                "offset": 13,
                "shift": 80,
                "w": 54,
                "x": 115,
                "y": 318
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "32980f16-79a0-4b02-b162-27a29dbf51de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 156,
                "offset": 3,
                "shift": 54,
                "w": 47,
                "x": 354,
                "y": 476
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "cc9fa94d-a580-4f9c-a6ab-2ae3f464a6f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 156,
                "offset": 5,
                "shift": 57,
                "w": 47,
                "x": 452,
                "y": 476
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e6dd7d35-d37e-466b-8bb1-7afe571b4883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 156,
                "offset": 4,
                "shift": 80,
                "w": 73,
                "x": 558,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ad49cce5-6fff-4fc2-aaad-590dd959985a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 156,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 724,
                "y": 318
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0b38ca02-b442-40ac-9cef-53cb75fdf918",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 156,
                "offset": 7,
                "shift": 61,
                "w": 53,
                "x": 669,
                "y": 318
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4c51cb8e-bac7-408d-ad56-def6ae742d6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 156,
                "offset": 2,
                "shift": 80,
                "w": 76,
                "x": 327,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2e3df11d-2e04-4a92-8e93-38e383b866b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 156,
                "offset": 7,
                "shift": 78,
                "w": 69,
                "x": 708,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "37efc51d-f311-4755-9cb3-5acbe452340b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 156,
                "offset": 5,
                "shift": 52,
                "w": 42,
                "x": 596,
                "y": 476
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2ca64604-69b4-4561-bc08-abc8be301207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 156,
                "offset": 5,
                "shift": 52,
                "w": 50,
                "x": 2,
                "y": 476
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "5ccb5b00-1769-4f62-bc04-f39f305dc2dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 156,
                "offset": 2,
                "shift": 66,
                "w": 63,
                "x": 2,
                "y": 160
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4e599816-ee78-425e-88a5-ba8d53605d82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 156,
                "offset": 7,
                "shift": 66,
                "w": 53,
                "x": 339,
                "y": 318
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c461e191-64b4-4b04-a918-f49413153929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 156,
                "offset": 7,
                "shift": 35,
                "w": 22,
                "x": 572,
                "y": 634
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c091f034-9a8f-4329-a21b-6063412d99a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 156,
                "offset": -1,
                "shift": 33,
                "w": 29,
                "x": 229,
                "y": 634
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3c8bd802-c30d-4dea-9734-c67844134bb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 156,
                "offset": 7,
                "shift": 69,
                "w": 63,
                "x": 779,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f8033939-30ca-44a2-8e81-71372deb82d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 156,
                "offset": 5,
                "shift": 36,
                "w": 31,
                "x": 35,
                "y": 634
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "76a2ce1c-137e-4022-a2a0-d08871c9cbca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 156,
                "offset": 5,
                "shift": 87,
                "w": 77,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8403149e-57e6-4bbe-9925-a88075742ea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 156,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 394,
                "y": 318
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c7acb102-d048-4c77-abba-377a5fdf888d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 156,
                "offset": 2,
                "shift": 81,
                "w": 76,
                "x": 249,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7e65378d-e224-4ea9-b188-5fbcb7b3b9a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 156,
                "offset": 5,
                "shift": 60,
                "w": 54,
                "x": 171,
                "y": 318
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "94a0a844-5552-4bdd-989f-916caa9407ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 156,
                "offset": 2,
                "shift": 81,
                "w": 76,
                "x": 405,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5d768a05-eb22-43f5-b2ef-a743a19b9b7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 156,
                "offset": 7,
                "shift": 64,
                "w": 58,
                "x": 253,
                "y": 160
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "efca5659-3f6c-4103-8ca0-54a20564c03e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 156,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 848,
                "y": 476
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a1699be5-9f39-4281-9074-989f434ac9d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 156,
                "offset": 0,
                "shift": 55,
                "w": 55,
                "x": 949,
                "y": 160
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "81bebdb0-ed0b-4fa5-b31d-a570e0a8f97c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 156,
                "offset": 5,
                "shift": 63,
                "w": 53,
                "x": 504,
                "y": 318
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c5ec02ad-38e6-4b5c-991c-4d00f4295a01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 156,
                "offset": -4,
                "shift": 56,
                "w": 63,
                "x": 844,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9cbcbc11-bb57-43c3-836f-d355e6c05744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 156,
                "offset": 5,
                "shift": 87,
                "w": 77,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4a410cb8-afcb-4f88-a7ef-2523c7afe927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 156,
                "offset": -4,
                "shift": 53,
                "w": 61,
                "x": 130,
                "y": 160
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6b4c1a37-14fb-4eb1-b663-9389c335e3eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 156,
                "offset": -5,
                "shift": 54,
                "w": 63,
                "x": 909,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "bd7db965-04a5-4e34-b13d-3eba0e23eb9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 156,
                "offset": 0,
                "shift": 55,
                "w": 56,
                "x": 603,
                "y": 160
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5813ff29-8a80-4044-a336-b84b736e20b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 156,
                "offset": 4,
                "shift": 39,
                "w": 31,
                "x": 2,
                "y": 634
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b3197e98-3139-4e40-80e2-a4064a2215e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 156,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 197,
                "y": 634
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7a672406-9eac-47c2-ba3d-2d51ba23b0a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 156,
                "offset": 4,
                "shift": 39,
                "w": 31,
                "x": 68,
                "y": 634
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f3d47f43-7288-4c3c-833d-98ef3941e186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 156,
                "offset": 0,
                "shift": 54,
                "w": 53,
                "x": 449,
                "y": 318
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c5b89c35-5a64-4262-be24-bc58d57e7e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 156,
                "offset": 0,
                "shift": 54,
                "w": 54,
                "x": 227,
                "y": 318
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "bc6d9b08-da48-4917-8b27-7ec86f375050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 156,
                "offset": 11,
                "shift": 54,
                "w": 24,
                "x": 546,
                "y": 634
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1c6ec4cd-738e-43c5-81dc-8381ffd1a635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 156,
                "offset": 2,
                "shift": 63,
                "w": 56,
                "x": 719,
                "y": 160
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "07021bf4-980d-4ff5-8dbf-b67568a0a965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 156,
                "offset": 4,
                "shift": 62,
                "w": 56,
                "x": 777,
                "y": 160
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1d9f7249-10df-46bf-988c-d91a911033c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 156,
                "offset": 2,
                "shift": 60,
                "w": 56,
                "x": 313,
                "y": 160
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9511d488-01a1-4ac7-ab0a-7d93be8657af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 156,
                "offset": 2,
                "shift": 62,
                "w": 56,
                "x": 545,
                "y": 160
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a00672ca-848d-46dd-a1cd-5f24487aaf8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 156,
                "offset": 2,
                "shift": 59,
                "w": 56,
                "x": 661,
                "y": 160
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "63db47cb-51e2-4d81-83af-1c5c35949edb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 156,
                "offset": 5,
                "shift": 37,
                "w": 29,
                "x": 260,
                "y": 634
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3daba599-40c8-4a59-bbf8-7668ba6628f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 156,
                "offset": 2,
                "shift": 62,
                "w": 54,
                "x": 59,
                "y": 318
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "219494a5-2d19-45cb-a632-17909ffe0cb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 156,
                "offset": 5,
                "shift": 57,
                "w": 47,
                "x": 403,
                "y": 476
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d16ab707-9310-4c77-a195-a1bed1239894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 156,
                "offset": 5,
                "shift": 31,
                "w": 21,
                "x": 619,
                "y": 634
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "42121e18-3978-4694-a678-536cfa4de012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 156,
                "offset": -8,
                "shift": 38,
                "w": 41,
                "x": 640,
                "y": 476
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a4476d0b-577e-44cb-870c-4efc0a0ae51c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 156,
                "offset": 5,
                "shift": 58,
                "w": 55,
                "x": 2,
                "y": 318
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c430c910-691b-421d-8240-1421752828ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 156,
                "offset": 5,
                "shift": 31,
                "w": 21,
                "x": 596,
                "y": 634
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9d93b98a-64c5-4d87-87b9-b3d9c719af4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 156,
                "offset": 5,
                "shift": 83,
                "w": 73,
                "x": 633,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "87c04bb4-a910-4484-ab40-6ae8c48847c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 156,
                "offset": 5,
                "shift": 58,
                "w": 48,
                "x": 156,
                "y": 476
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "af257c82-1a65-45c9-a429-2cd0e063bc9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 156,
                "offset": 2,
                "shift": 60,
                "w": 56,
                "x": 487,
                "y": 160
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6992c384-1d11-4c20-b429-9c402df083f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 156,
                "offset": 4,
                "shift": 62,
                "w": 56,
                "x": 429,
                "y": 160
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9f4fcbe9-2193-48de-b73a-ec258a207aac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 156,
                "offset": 2,
                "shift": 62,
                "w": 56,
                "x": 371,
                "y": 160
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6f1f2f26-6efc-47c0-9ff9-e4641e0ac4de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 156,
                "offset": 5,
                "shift": 34,
                "w": 30,
                "x": 133,
                "y": 634
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "43639e1f-5bc5-4d13-936b-55923741b778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 156,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 766,
                "y": 476
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "95eaa6c5-a59e-4b01-928d-664d0d5640f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 156,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 101,
                "y": 634
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2847b7fc-7215-4986-bbd1-dd3001d596f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 156,
                "offset": 5,
                "shift": 58,
                "w": 48,
                "x": 206,
                "y": 476
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0d1db816-d119-4922-b418-e6862807a14c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 156,
                "offset": -4,
                "shift": 41,
                "w": 49,
                "x": 54,
                "y": 476
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e4944353-d073-4008-a3b4-65380c8755a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 156,
                "offset": 5,
                "shift": 83,
                "w": 73,
                "x": 483,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4d4d7acc-d897-40bc-99db-1f0894be3e18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 156,
                "offset": 5,
                "shift": 56,
                "w": 47,
                "x": 501,
                "y": 476
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4d987e03-6bb9-4cc4-8290-1c3d50128715",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 156,
                "offset": 5,
                "shift": 57,
                "w": 47,
                "x": 256,
                "y": 476
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c0ed67af-dfb9-4be1-a73c-956b272213b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 156,
                "offset": 0,
                "shift": 53,
                "w": 52,
                "x": 833,
                "y": 318
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "156171cb-52c6-453e-b856-a191a402ecad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 156,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 960,
                "y": 476
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "67b5675f-e074-4c23-833a-c84fd1dc294e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 156,
                "offset": 17,
                "shift": 54,
                "w": 20,
                "x": 665,
                "y": 634
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6c8cfde1-6b03-40cc-a08b-8f2fbbcbc090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 156,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 925,
                "y": 476
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5831b097-221c-4a39-97d6-9d4cb21890f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 156,
                "offset": 9,
                "shift": 71,
                "w": 53,
                "x": 559,
                "y": 318
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 80,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}