{
    "id": "e62a26d8-100c-49f0-a96f-2acad5b5b16b",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_back",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0f063054-60bb-4361-a91f-ce1c5a421a4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 78,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 333,
                "y": 322
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c4e4ea3a-54c8-476d-9f88-5d78325a0317",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 78,
                "offset": 5,
                "shift": 23,
                "w": 13,
                "x": 318,
                "y": 322
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "29399f06-3536-484c-8a02-208625cac592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 78,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 394,
                "y": 242
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d76cc235-6b33-4342-9b92-01b0553fee63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 78,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 68,
                "y": 82
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a3d3093e-e7bb-48e4-bf7a-634631ea74a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 78,
                "offset": 5,
                "shift": 30,
                "w": 20,
                "x": 461,
                "y": 242
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9de92751-a788-4554-b1d0-9907482c16a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 78,
                "offset": 3,
                "shift": 49,
                "w": 43,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a69ff528-cb3a-4415-93d9-eb1604d144b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 78,
                "offset": 6,
                "shift": 40,
                "w": 29,
                "x": 100,
                "y": 82
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ce7c545c-6e90-430a-a3cc-595f1bcdee57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 78,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 458,
                "y": 322
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f42a8ceb-53bf-45ec-86f5-62ba7f3f1683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 78,
                "offset": 3,
                "shift": 19,
                "w": 15,
                "x": 152,
                "y": 322
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "cdb5f0a2-d35a-4911-81c9-fcabe0b57e8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 78,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 286,
                "y": 322
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5f2da30e-ea85-43c4-a6c8-faf199490d26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 78,
                "offset": 4,
                "shift": 27,
                "w": 18,
                "x": 23,
                "y": 322
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f5ccc06e-bb0f-4a4d-96df-8c5e75c9fe37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 78,
                "offset": 6,
                "shift": 40,
                "w": 28,
                "x": 402,
                "y": 82
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "483953ae-b1e9-41ab-bd6e-e88e3acf5564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 78,
                "offset": 3,
                "shift": 20,
                "w": 13,
                "x": 348,
                "y": 322
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "354b5d8a-0ad4-48ce-8cc6-827a3277a69b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 78,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 378,
                "y": 322
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "fea31bd1-3a61-431e-8033-987c117afffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 78,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 302,
                "y": 322
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9e8bb7f3-45a6-489a-96c0-70127b222c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 78,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 203,
                "y": 322
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "cb161367-35b9-47e8-8e60-e12b3f1ceceb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 78,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 440,
                "y": 162
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "aafc8d5a-3220-45bb-9e6d-d1a732c96dfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 78,
                "offset": 10,
                "shift": 30,
                "w": 11,
                "x": 432,
                "y": 322
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "734cbff8-25d1-4085-b953-9bf35ea608ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 78,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 30,
                "y": 242
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a9bcad21-6fcc-4ce1-8173-9db3473dfc0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 78,
                "offset": 5,
                "shift": 30,
                "w": 20,
                "x": 439,
                "y": 242
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3ff8efe5-da44-457c-9fa0-4d3681f9669a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 78,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 353,
                "y": 162
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "421a3b8d-b056-498a-8231-855e1c5193d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 78,
                "offset": 4,
                "shift": 30,
                "w": 22,
                "x": 346,
                "y": 242
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6785561b-f911-4975-9c23-6818aa408b2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 78,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 432,
                "y": 82
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8460d9af-8a01-49ad-ab9d-1389751e3764",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 78,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 268,
                "y": 242
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0a72b221-070f-4d22-924e-ba9a90b52e13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 78,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 2,
                "y": 242
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7d04215b-0e48-4a2f-bf07-6a6213c32c5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 78,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 62,
                "y": 162
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e99bed0b-4110-46e1-a404-4eafd112d189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 78,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 254,
                "y": 322
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "643dc5ee-c878-43cb-9950-6924ccf8907d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 78,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 270,
                "y": 322
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "bc03b7a3-02ee-4118-93ec-2cd313a0666d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 78,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 164,
                "y": 242
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bd8507dc-98dc-48fc-9819-0b4fa9775ad1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 78,
                "offset": 6,
                "shift": 40,
                "w": 28,
                "x": 312,
                "y": 82
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8006701b-1ac3-426f-b794-a2fc80eaa050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 78,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 138,
                "y": 242
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "66fc19a6-32c0-4f3f-9ce3-462251800005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 78,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 112,
                "y": 242
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ca43e055-17b7-4b57-87ef-3bdbcdd859dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 78,
                "offset": 2,
                "shift": 40,
                "w": 36,
                "x": 327,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "cfc44c02-212c-4e46-bdb9-15ce3495564e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 78,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 411,
                "y": 162
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "471b7ef2-54c5-49fe-b41e-88c91c975b00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 78,
                "offset": 3,
                "shift": 30,
                "w": 27,
                "x": 382,
                "y": 162
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ae3e101b-1872-4ac2-b715-43425dfb9da6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 78,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a2585a0c-9bae-4114-b56e-6472d21474ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 78,
                "offset": 3,
                "shift": 39,
                "w": 35,
                "x": 365,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "61e06b1c-5988-4f4c-9970-5f8a555da133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 78,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 370,
                "y": 242
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "57124b33-a85c-4ae7-94f5-71a348ac83d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 78,
                "offset": 2,
                "shift": 26,
                "w": 25,
                "x": 85,
                "y": 242
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b50f1240-7908-4fff-b4b3-32f6fa638f35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 78,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 35,
                "y": 82
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "79f17577-3d7e-4845-8868-9e580a9b55ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 78,
                "offset": 3,
                "shift": 33,
                "w": 27,
                "x": 208,
                "y": 162
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "15dec339-0ff6-4aaf-bf29-4d5781688c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 78,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 392,
                "y": 322
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b4e839e9-82d9-4440-982b-48a599a61bc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 78,
                "offset": -1,
                "shift": 16,
                "w": 15,
                "x": 237,
                "y": 322
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9e7e45cc-9800-4ead-8254-507c58cd148c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 78,
                "offset": 3,
                "shift": 34,
                "w": 32,
                "x": 402,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fbaa7e70-a0ee-4fef-a9a4-96fb637fd298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 78,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 99,
                "y": 322
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "bbe99560-7a4c-4e3a-a068-bf93a11fef7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 78,
                "offset": 2,
                "shift": 43,
                "w": 39,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7a65b8e9-dd09-4f08-8aa9-3facdde399da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 78,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 179,
                "y": 162
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4e0354e0-2bd4-4073-91b0-24dfcb77fac2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 78,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8a246636-447f-4593-9d71-992d61ec920c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 78,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 150,
                "y": 162
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8dc2e6f2-0f3a-4b33-a2c5-575b1286410b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 78,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "6a8f94cb-a9ab-47fc-a782-7e4044e6a77a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 78,
                "offset": 3,
                "shift": 32,
                "w": 29,
                "x": 131,
                "y": 82
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f08ce2e5-c451-433f-b731-a98c7e1cbf6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 78,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 483,
                "y": 242
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f1e3a0f3-a2f0-42cf-a66a-a4bf07d69245",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 78,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 295,
                "y": 162
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c8f5f72b-1c9c-41b9-bd60-b91b3b2c8e86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 78,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 92,
                "y": 162
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "61858a9a-d684-4a70-9033-03d0a2ddd706",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 78,
                "offset": -2,
                "shift": 28,
                "w": 31,
                "x": 470,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "39604d01-e12f-40d6-aa4a-99808483bdb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 78,
                "offset": 2,
                "shift": 43,
                "w": 39,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "36f6586b-4e6d-49ca-ac62-8db04ea01e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 78,
                "offset": -2,
                "shift": 26,
                "w": 31,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8a7ff67b-8871-489a-8126-445212667e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 78,
                "offset": -3,
                "shift": 27,
                "w": 32,
                "x": 436,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "abfbc3de-1f07-4d91-9876-a88b18751d23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 78,
                "offset": 0,
                "shift": 27,
                "w": 28,
                "x": 372,
                "y": 82
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3468e859-4d89-4d68-bf74-db18916d7a2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 78,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 81,
                "y": 322
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6c312e5b-70bd-4637-ae33-8a6ebe344d60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 78,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 220,
                "y": 322
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c45729e5-2802-4f8c-b074-4ee7a97f5f41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 78,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 117,
                "y": 322
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d9d2a37b-44d5-4b19-abc1-05bc454b76d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 78,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 237,
                "y": 162
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e6a2a148-46e1-4607-b81b-a25b672ebcd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 78,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 266,
                "y": 162
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8b421425-6e31-4e5f-b959-cabd0c3cd999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 78,
                "offset": 5,
                "shift": 27,
                "w": 13,
                "x": 363,
                "y": 322
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8b10c643-90bc-4789-8864-a34ec046f867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 78,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 32,
                "y": 162
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "85e1c9ab-acfa-4c19-baec-d673963c70f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 78,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 162,
                "y": 82
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8b3ad4c8-3d88-44af-96bc-890e843279ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 78,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 342,
                "y": 82
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2fd2c1c7-6d4a-4278-bc90-bd4fe84ce982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 78,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "502087c4-6b96-43d6-99b6-cafc12031c43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 78,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 462,
                "y": 82
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "67c22015-69eb-4a77-a943-5f33787b1f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 78,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 186,
                "y": 322
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e4e1ec32-a4a8-4563-8e03-58606754280f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 78,
                "offset": 1,
                "shift": 31,
                "w": 27,
                "x": 121,
                "y": 162
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8dd8581c-3caf-45b9-a7cb-0003adac03fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 78,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 190,
                "y": 242
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c73a9f0b-390b-45df-9abd-26053e241a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 78,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 419,
                "y": 322
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "045898ce-a985-4754-8761-069ea24a5b53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 78,
                "offset": -4,
                "shift": 19,
                "w": 20,
                "x": 417,
                "y": 242
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "19c45666-b8fb-4dcf-b759-51b0571d82b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 78,
                "offset": 2,
                "shift": 29,
                "w": 28,
                "x": 282,
                "y": 82
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "4db961fc-5ceb-4efb-bfc5-5ab3cff53aa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 78,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 406,
                "y": 322
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "faabf168-434f-4658-9af5-7223be26d5c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 78,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 288,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b18ce860-afae-4863-8f4b-06b376502030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 78,
                "offset": 2,
                "shift": 29,
                "w": 24,
                "x": 294,
                "y": 242
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b57c83e2-27b1-468f-a6bd-113997614e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 78,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 252,
                "y": 82
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "52c02dc8-40a3-42d7-b385-4b67b3ece5b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 78,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 222,
                "y": 82
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b42638a8-367f-419b-a5b8-a555ff5eb789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 78,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 192,
                "y": 82
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "dd718a20-0469-417d-9554-b608a5071bd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 78,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 135,
                "y": 322
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "120c3b4b-2a37-4292-b5ba-c6337d76fd7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 78,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 322
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6ed68354-c867-46ab-906c-4091813f3d53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 78,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 169,
                "y": 322
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "efaf4e25-32d0-4442-a88a-d0ae100c0e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 78,
                "offset": 2,
                "shift": 29,
                "w": 24,
                "x": 320,
                "y": 242
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "44cd05c8-335f-4a92-964e-adcc70ad2c8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 78,
                "offset": -2,
                "shift": 20,
                "w": 25,
                "x": 58,
                "y": 242
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c813fef7-11e1-4d63-a387-7eb878774a9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 78,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 249,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "94e2d270-d040-4325-8e06-772477cb67c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 78,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 216,
                "y": 242
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e7a7f895-ac93-4cd7-ba58-c6db938e01ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 78,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 242,
                "y": 242
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ae30b779-df29-4782-aa03-8c791d4c5648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 78,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 468,
                "y": 162
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7d15527e-912a-473e-bbbe-9948551a3af7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 78,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 43,
                "y": 322
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e12f5167-2125-4960-8217-3cc00d011029",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 78,
                "offset": 8,
                "shift": 27,
                "w": 11,
                "x": 445,
                "y": 322
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b08677ef-f8ea-437d-a54d-c007afcb525e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 78,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 62,
                "y": 322
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1ad998fb-244d-4b96-a5b9-bf28372ccfc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 78,
                "offset": 4,
                "shift": 35,
                "w": 27,
                "x": 324,
                "y": 162
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 40,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}