{
    "id": "312039e8-6148-42ea-b36e-51b0b211008e",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_level",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "24db60f5-3065-417b-828a-803e59f70502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 47,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 149
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "02fda805-675e-42ca-bb80-8cb50aae0fd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 47,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 455,
                "y": 100
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "377b2a4d-db2a-44f1-ae7e-9f910e9fb5ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 47,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 232,
                "y": 100
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e9356c9d-ca4c-4291-86be-003b8cc02d73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 47,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f7c05824-e6c2-4f16-99e5-ef5b47fb54cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 47,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 246,
                "y": 100
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ed826c18-025c-498a-b368-b7b0dd06e412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 47,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e87e345e-5732-4b68-83c7-dd58fa3af721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 2,
                "y": 51
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "5c8db72c-418a-4b01-93c9-ae9fda75a185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 47,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 68,
                "y": 149
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "261c8d6c-59e0-4a1e-be81-c2d84abae905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 433,
                "y": 100
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a44f70b6-666c-45ef-954a-7192512572cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 422,
                "y": 100
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "39fa42ec-2b3f-4826-9d1e-453d879aee7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 47,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 260,
                "y": 100
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8793659a-978f-49ea-9ef2-8705e49945e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 17,
                "x": 310,
                "y": 51
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "94135f85-bd34-45bd-bd5b-6284bd559d9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 485,
                "y": 100
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c7630887-78fa-4620-b89b-3d624c785b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 47,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 12,
                "y": 149
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a44e887d-359c-492d-ac01-27f1caeb3a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 2,
                "y": 149
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ac97baa0-7d57-43fc-adfa-52879d3a51e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 47,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 326,
                "y": 100
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "5cc52c0a-ff16-440d-b02f-32727614643b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 401,
                "y": 51
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f4553d33-85cd-4c73-b39f-c633dc33616c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 47,
                "offset": 6,
                "shift": 18,
                "w": 7,
                "x": 59,
                "y": 149
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "97e447e2-ad8b-48f2-8220-3d65e34ee140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 419,
                "y": 51
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e7c07fca-67d7-4814-9f69-0ff522776c96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 47,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 288,
                "y": 100
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0f25482a-fcd7-477e-a156-69d46a1d8bd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 329,
                "y": 51
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "03c95b8b-1bf5-44d5-a761-dd53ee6ab6aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 47,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 156,
                "y": 100
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e2caed39-d5df-4c86-a448-40a30676ab64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 487,
                "y": 2
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "819b025b-9c6a-48c8-bd17-0e66037849f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 491,
                "y": 51
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4fd74897-42f9-40ab-a942-7412a4318c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 455,
                "y": 51
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "635d21cc-52af-4e8b-b3c2-0abe1428c9c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 447,
                "y": 2
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "aeb2b4d0-7480-4d20-9da5-0303fb6ea068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 475,
                "y": 100
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "8e91b53c-4efd-43f6-8630-5928421032e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 465,
                "y": 100
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "dd855d17-be8e-461f-a22b-098551c809d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 47,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 172,
                "y": 100
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "94bfd9f1-6c31-4e72-bb86-4852566cec33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 17,
                "x": 291,
                "y": 51
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "9b4b74aa-1a39-4104-bed1-2a3bc6f29b43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 47,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 140,
                "y": 100
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "0001f244-b817-40f3-83bf-f2157a22e0a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 123,
                "y": 100
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8d717260-171f-4e7f-8810-b6f6268ab882",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 47,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "eac796b7-4548-4328-b78f-be575478e989",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 272,
                "y": 51
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c9f47bad-bdf8-430c-963c-f69ad88717b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 47,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 347,
                "y": 51
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "cbff207b-06e4-44a5-81a7-49ad5ea0362e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "17cdb3c7-e62d-451d-bf34-5c2580c50c71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 47,
                "offset": 2,
                "shift": 23,
                "w": 21,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "01c2ca4d-6917-4f1c-952c-7e3551cc4dc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 47,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 188,
                "y": 100
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "dc9ff599-fa40-495e-876b-db29337f590a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 47,
                "offset": 1,
                "shift": 15,
                "w": 16,
                "x": 437,
                "y": 51
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "17f006fe-6359-443f-84e6-6fa7c729909c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 47,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 279,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4a1d7514-d4f2-4b21-9147-227b31fc5614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 47,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 2,
                "y": 100
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a416dbe9-9f66-4d82-9f2e-f171e9ac3829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 47,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 50,
                "y": 149
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "aa5609d8-b33c-49a8-963c-ed201817f597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 47,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 350,
                "y": 100
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "36e9ffeb-195d-4638-b4bf-269d8ebadada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 345,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4f9400f3-24a4-427b-867a-1004150fca8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 362,
                "y": 100
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1e618f66-c2df-473c-b2cd-6b1c86f91a96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 47,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "73078ab7-6a63-4c02-882a-57d8123d490e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 139,
                "y": 51
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1f0ab7f5-8067-43c2-b11f-0e079fa16852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "111f40f4-3a67-4db6-b227-fd64594d4978",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 120,
                "y": 51
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "26458d0c-4142-4511-8920-68c171f760bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cfc5ca62-f9b8-490b-9a39-4d2ce9f45c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 47,
                "offset": 2,
                "shift": 19,
                "w": 18,
                "x": 387,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "80177261-8bfd-4f22-ae96-0176142337c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 47,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 274,
                "y": 100
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4b0c2d02-a50b-47ad-a805-25461950befd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 82,
                "y": 51
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "fdcbbbc4-4e69-4afb-a486-9268c40c3b27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 215,
                "y": 51
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "644d2a10-3c5b-420d-944b-8ba4b506e073",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 47,
                "offset": -2,
                "shift": 17,
                "w": 20,
                "x": 257,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1e87f415-2738-492d-8997-6c083518259b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 47,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b67224dd-8574-48e2-af3f-2da477a6b608",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 47,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 301,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d5685420-1faa-4518-909d-f5ceaa81f8fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 47,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 323,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7a6ce8fe-6f8d-43a3-a602-bdde6508f78b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 47,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 177,
                "y": 51
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1d5f351f-eaca-4c76-a944-ff712953ece5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 338,
                "y": 100
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "365950e7-793e-42db-bdba-0f186e1ca314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 398,
                "y": 100
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4c5920bf-97f8-45d1-9aeb-ff509d44da0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 386,
                "y": 100
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "3963884d-f9b5-4c18-9e7d-e51858723ebc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 383,
                "y": 51
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "be59ea1c-5ef3-4cef-9239-ea1d9e6527bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 365,
                "y": 51
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c725602a-4e34-4038-b4d2-e94dd9d2d855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 47,
                "offset": 3,
                "shift": 16,
                "w": 8,
                "x": 495,
                "y": 100
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a6939705-c7fa-45f5-a9dd-b5cc8be69341",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 62,
                "y": 51
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "66c1774b-0acb-48c2-88f0-82012879cdb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 234,
                "y": 51
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8e99980d-a29a-4835-b06f-7609e7b8bddb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 467,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "baba7d4a-ae16-4a48-abba-48b2c82544aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 42,
                "y": 51
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8e4c498a-356c-4223-9d06-fe089c8d5c00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 22,
                "y": 51
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "23d1917c-e76a-4dd5-884d-8d3c980f04b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 410,
                "y": 100
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "217e1fae-562c-4a66-9117-d9f4001e95fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 158,
                "y": 51
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "49f3235e-4e36-4f80-8ffd-2a1a27ea6428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 55,
                "y": 100
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "57b854e7-2bb5-41c0-ac71-525da84051eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 47,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 41,
                "y": 149
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e8a7a7b0-ecdc-494a-b1a6-9c96ae77d9f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 47,
                "offset": -3,
                "shift": 11,
                "w": 13,
                "x": 203,
                "y": 100
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c25d227a-2243-4c77-a2b7-b5c4ec6f72c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 196,
                "y": 51
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e7e59a12-8687-4e26-abca-95195286b821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 47,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 32,
                "y": 149
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "875fc849-b3e6-45f3-b601-cf0cba9b05e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 47,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "69f45799-151b-475c-be4c-785535462e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 72,
                "y": 100
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5147256b-460c-4d2e-9abe-983744fbb643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 407,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b11e92c7-b8cd-4eb9-ae82-aee27ab467da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 101,
                "y": 51
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "224b9ed7-65fd-4f8b-95d1-0fbb34b129a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 427,
                "y": 2
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "572df931-7fed-41ad-82ef-ed42a91e19c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 47,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 314,
                "y": 100
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2053dce0-2652-4acb-a57e-0a20fa43e9c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 47,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 218,
                "y": 100
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5053c8d8-f593-4cec-806f-86d517910359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 444,
                "y": 100
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "79ce9c31-7078-463e-8c87-0dc1f89124d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 38,
                "y": 100
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a0240688-0d20-4d38-b5d9-006437122651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 47,
                "offset": -2,
                "shift": 12,
                "w": 16,
                "x": 473,
                "y": 51
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "94183649-9898-41c5-8f7e-6b5060f535fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 47,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2c09c4b8-de9d-479a-bacd-7b024c93c4ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 89,
                "y": 100
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0022f276-ed5e-48ce-bfc9-24b8232a378b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 106,
                "y": 100
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "61339e94-33ae-4a07-9586-ecdea266e0eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 20,
                "y": 100
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c32778d9-e27e-47e8-814e-db5f33b71e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 302,
                "y": 100
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a9ba164a-b6ce-4bb5-a684-7a7e61c2448c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 47,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 76,
                "y": 149
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "11a73df7-e710-4b59-b930-bbf6f9f35877",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 374,
                "y": 100
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4ce31db6-a040-4086-ad62-957a1e5282b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 253,
                "y": 51
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}