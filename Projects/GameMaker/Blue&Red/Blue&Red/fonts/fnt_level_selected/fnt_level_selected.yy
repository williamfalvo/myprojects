{
    "id": "49e52842-27e2-4584-a34e-777eafd6be10",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_level_selected",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "06713efe-fd48-4baf-85c5-cfe5888b8133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 58,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 283,
                "y": 182
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "afffe3de-bf87-41ad-adcf-895075f1c7ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 58,
                "offset": 4,
                "shift": 17,
                "w": 10,
                "x": 271,
                "y": 182
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2c8a4c2c-6efc-4ff0-b313-f47178472c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 423,
                "y": 122
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "fce43be2-3eb5-4ca2-8a2b-40a8903e1440",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 58,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 437,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d837cb42-cd5f-479f-830c-ed0d5d73b954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 58,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 477,
                "y": 122
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "cefc5488-c853-4e56-b9ec-638f2e8b271c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 58,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "543c6ebd-32b2-47fc-9adc-7b51418935a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 58,
                "offset": 4,
                "shift": 30,
                "w": 22,
                "x": 50,
                "y": 62
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "295aa1dc-cf7a-46dc-b6fa-e0da1c71614b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 58,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 352,
                "y": 182
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0ca7ce58-f16b-4172-a1fa-e0d5942d35ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 58,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 219,
                "y": 182
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2ea06c1a-7cfc-4b9e-af7c-953fc29748a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 58,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 206,
                "y": 182
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c74be65a-c2fb-4a6c-99af-f904b2268a4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 58,
                "offset": 3,
                "shift": 20,
                "w": 14,
                "x": 36,
                "y": 182
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "db23f8d8-b36f-41fa-b551-6a4dd02542d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 58,
                "offset": 4,
                "shift": 30,
                "w": 21,
                "x": 373,
                "y": 62
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8f5f22b7-a1b6-407c-a99a-28cb9c5d026b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 58,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 295,
                "y": 182
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "33441b12-b711-496c-82ef-be8114726b2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 58,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 330,
                "y": 182
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "72c2db2c-782d-4278-bf81-3bed7e9b2ff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 58,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 193,
                "y": 182
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "81c9a12c-bc92-4056-9da3-33ac5fd145ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 58,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 110,
                "y": 182
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8be8573f-44a3-475f-b6fa-443c6f23b2be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 90,
                "y": 122
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "59df3634-7720-4950-9cb9-0b92ebde4c35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 58,
                "offset": 7,
                "shift": 23,
                "w": 9,
                "x": 319,
                "y": 182
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4b2b2356-f5bb-4624-964c-654e3f66d2dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 68,
                "y": 122
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5dc34b30-27c7-4d29-b142-f870ac349a29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 58,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 494,
                "y": 122
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "016015bc-52c0-404e-ade2-e44a070863b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 350,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fcaa0513-3ccb-4b80-bf15-196eee6f0461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 58,
                "offset": 3,
                "shift": 23,
                "w": 17,
                "x": 404,
                "y": 122
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0941ca45-3d7b-45f6-8875-bfbc7e425281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 396,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "81f5576c-3c6c-4068-9242-309960c1d5c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 58,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 178,
                "y": 122
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "27fb9584-8621-4bdb-8f47-07f24e5759da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 156,
                "y": 122
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a5d9371e-2061-4db7-9bf2-749c1e0e3a2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 419,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0967e83f-0333-4367-9fc8-812422bc06fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 58,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 232,
                "y": 182
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "850d1144-dd13-4351-a0b9-0e377f807053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 58,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 180,
                "y": 182
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "34292ca9-166f-4b07-9b1c-1912e7bb64eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 58,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 384,
                "y": 122
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f94607d9-3a4a-4020-a10b-93e0003c1d10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 58,
                "offset": 4,
                "shift": 30,
                "w": 21,
                "x": 327,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7026ee3c-6690-4f41-a9db-88d894ad47ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 58,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 364,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7a3cedb2-2132-4b18-b282-a2938d4a01ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 58,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 262,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9e934b69-7236-4d82-ab4f-04035a627ba1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 58,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d76fa2f2-d8fc-413d-8d70-ec3b6804f084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 58,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 134,
                "y": 122
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "250e951d-975d-40a4-878f-c2d78475437f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 58,
                "offset": 2,
                "shift": 23,
                "w": 21,
                "x": 74,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b925598f-0ffe-4a5c-90c1-c4afa386663b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 58,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "02247bc8-5387-4b83-8ddd-b1b689cc5b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 58,
                "offset": 2,
                "shift": 29,
                "w": 27,
                "x": 279,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a455eed4-a394-4cec-89d3-e95594dcc5e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 58,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 459,
                "y": 122
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f4f0675a-4ede-4e5b-9d2b-359296c040cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 58,
                "offset": 2,
                "shift": 19,
                "w": 19,
                "x": 220,
                "y": 122
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0d243913-b768-4283-8cf4-19b152a7927a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 58,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 412,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "db9f9bf1-e3bd-46bc-8158-f1f48f8e7818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 58,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 189,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5717ece5-9c57-40f0-a6b1-51254a810fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 58,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 341,
                "y": 182
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b58678a7-7ac4-4555-a396-ce6b82b2e52c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 58,
                "offset": -1,
                "shift": 12,
                "w": 12,
                "x": 82,
                "y": 182
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "43dfabac-2ee4-4893-920d-3ce73abefbae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 58,
                "offset": 2,
                "shift": 26,
                "w": 24,
                "x": 308,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "312c5849-e1e4-4490-a3e8-8914653958bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 58,
                "offset": 2,
                "shift": 13,
                "w": 12,
                "x": 96,
                "y": 182
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "13bfdd55-cce6-4023-8d2a-5e36c6d20c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 58,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bb305b9c-045d-4932-bf65-05b0819f1a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 58,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 46,
                "y": 122
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9522fd91-af95-4cf1-8aea-53f7999c8b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 58,
                "offset": 1,
                "shift": 30,
                "w": 29,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "af5189e1-3e99-4db9-b466-a1c3f3d7b567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 58,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a9bc33af-cafd-44a4-b9b2-a56dc092796c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 58,
                "offset": 1,
                "shift": 30,
                "w": 29,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "60107aa6-fd9d-439e-bd54-27d10c09232c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 58,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0d111502-080d-4d4a-ac0a-a2f9d97f40b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 58,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 182
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "02cf4352-31bb-4983-9bcf-d5f430fbfdab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 97,
                "y": 62
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "be8993e4-2d53-4489-9f0c-fd92ea44253d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 58,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 486,
                "y": 62
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "35f113b5-2ac1-4085-8fc4-3ba77c9f26e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 58,
                "offset": -2,
                "shift": 21,
                "w": 24,
                "x": 334,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4ae0f453-c9b1-4666-9f9b-527225b252a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 58,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5a3f1a87-5678-4573-872c-3dd97bd382c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 58,
                "offset": -2,
                "shift": 20,
                "w": 24,
                "x": 386,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bdaafa75-fd53-4525-a84f-ab3bb14fb815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 58,
                "offset": -2,
                "shift": 20,
                "w": 24,
                "x": 360,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8bcf932c-ad2c-4047-bff8-e4d4e07a45d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 281,
                "y": 62
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8171eaad-4d60-4135-ad95-acbdfd0120ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 58,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 152,
                "y": 182
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d9e2bd49-409a-4dff-949a-0f3c1da4b2d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 58,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 138,
                "y": 182
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "95c2d005-57ae-4dab-92cf-84cf81212bd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 58,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 124,
                "y": 182
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "065e4ff6-31ca-42bb-a0dd-e1006ad732ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 464,
                "y": 62
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0da3317f-5e0a-48b9-a46b-8f3708b18173",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 442,
                "y": 62
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e96e02ee-53a4-4b49-9046-083ade5d5acd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 58,
                "offset": 4,
                "shift": 20,
                "w": 10,
                "x": 307,
                "y": 182
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "866a61e5-a87e-44a5-a97c-b8f830d97fdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 58,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 120,
                "y": 62
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5d0dbaca-de5b-43c9-9a2a-7bec68d36d5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 26,
                "y": 62
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e3df886d-3ee1-4ce2-9eac-34f756f1ce2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 58,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 166,
                "y": 62
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1c5e7d93-e275-4272-a67a-f81c4918f541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 212,
                "y": 62
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5b6d72bc-65e7-4512-9bd6-eeec5ceba5db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 58,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 235,
                "y": 62
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "88452125-8b04-4205-9167-ebb1dc4534de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 58,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 258,
                "y": 182
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1d69c977-9ea0-4b9e-80f0-88e6a3407d4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 24,
                "y": 122
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8b2863fe-148c-4159-9fa9-4f591c5a40fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 344,
                "y": 122
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6e273e31-a101-48bd-860b-180a4d7731c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 58,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 362,
                "y": 182
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "26654e67-64a5-4def-88ea-9a3a7f071db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 58,
                "offset": -3,
                "shift": 14,
                "w": 16,
                "x": 441,
                "y": 122
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "826df985-b0c5-4936-95b1-8c0869953530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 58,
                "offset": 2,
                "shift": 22,
                "w": 21,
                "x": 258,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "31f4d8f8-182b-4088-981d-6ba10b550ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 58,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 382,
                "y": 182
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "399df041-7bb3-463c-b39d-124bc5c803ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 58,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "9392d60f-ee8a-424c-b989-1865a12b7138",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 58,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 304,
                "y": 122
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "80bf643b-9a7b-46fb-b5e8-1bf7a19656f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 58,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 462,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f50745e7-3454-4ea7-ac1a-4c4b9366df99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "336a5e2f-508f-41b8-854c-0dec7ff51667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 143,
                "y": 62
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9c847efd-aa92-4153-9968-c4f60e00864d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 58,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 245,
                "y": 182
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f380bb7e-66e4-4059-8902-73fb6609b0fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 58,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 19,
                "y": 182
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c1d10fe3-e556-460e-9e22-1148949068aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 58,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 166,
                "y": 182
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "aad1c369-4510-4485-b4fd-aedefd0289b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 58,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 324,
                "y": 122
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "91d0c054-9be7-4686-80a0-cd90c01d9636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 58,
                "offset": -2,
                "shift": 15,
                "w": 19,
                "x": 199,
                "y": 122
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5e1f43bf-4fdc-4587-91e3-e890e25cdd3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 58,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 250,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9ccde2b4-29ef-499c-bb17-a8ac1601082c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 58,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 241,
                "y": 122
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "637decde-7f69-4c7f-a0c6-2ef864b81b8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 58,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 283,
                "y": 122
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ea3734ce-7b95-40ae-8d88-4e88394ea8be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 112,
                "y": 122
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "28a9aac4-854c-4113-a857-ef0330af373a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 58,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 67,
                "y": 182
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0d8d89ff-c39d-4bfe-b2b3-a8c9ffd8a624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 58,
                "offset": 6,
                "shift": 20,
                "w": 8,
                "x": 372,
                "y": 182
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "11a23314-14c7-4f30-8543-152a3e94d748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 58,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 52,
                "y": 182
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "95895acc-1638-4045-931f-3c0515649a4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 58,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 304,
                "y": 62
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}