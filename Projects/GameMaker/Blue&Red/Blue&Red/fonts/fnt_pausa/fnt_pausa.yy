{
    "id": "6051c593-d53d-439c-9eb5-aa5a80a456ab",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_pausa",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "fb9139ea-e24f-4e9a-90e5-4c95ae337930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 70,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 150,
                "y": 290
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0f11c1ff-75a6-4432-b39b-b5775921d32e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 70,
                "offset": 4,
                "shift": 21,
                "w": 12,
                "x": 80,
                "y": 290
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "72d2f551-b9b8-49ee-9919-b5c9df90fe65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 70,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 318,
                "y": 218
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "eb86b556-8d2e-4007-87de-bfeaf6167a3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 70,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b4a7b0bf-3c81-4a86-b812-0eaf17213567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 70,
                "offset": 5,
                "shift": 27,
                "w": 18,
                "x": 338,
                "y": 218
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "680885b2-a370-4de7-8ad7-86ffd2c3c9d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 70,
                "offset": 3,
                "shift": 44,
                "w": 39,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d4761912-8909-4c33-8da1-58741f47b7e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 70,
                "offset": 5,
                "shift": 37,
                "w": 26,
                "x": 62,
                "y": 74
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "596bcaaf-7caa-4865-840c-59aa326cff5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 70,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 250,
                "y": 290
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0924a84a-1473-4404-aada-038de6dc4c6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 70,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 65,
                "y": 290
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "79ac9a06-41b6-478e-af6d-43c35ce1b4e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 70,
                "offset": 2,
                "shift": 18,
                "w": 13,
                "x": 50,
                "y": 290
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c472a2bb-902b-4fdf-a851-54f02e8ade43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 70,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 358,
                "y": 218
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "98ce57a0-469a-4181-aa45-6a623a01bc04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 70,
                "offset": 5,
                "shift": 36,
                "w": 25,
                "x": 361,
                "y": 74
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "43ac39d3-65fe-40a7-a2c7-bfb2f877227a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 70,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 108,
                "y": 290
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3b07566c-c195-4b5a-84e5-2689002968ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 70,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 164,
                "y": 290
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9d8f54cf-117a-4f6c-a324-a5745888d254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 70,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 94,
                "y": 290
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a8be3b85-eeb5-4f8e-9a1d-050b8f84c00b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 70,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 459,
                "y": 218
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "159f6213-d871-493a-8223-8d653f4fdfad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 70,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 292,
                "y": 146
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a4862feb-557f-4b4c-be68-d16c685a32b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 70,
                "offset": 9,
                "shift": 27,
                "w": 10,
                "x": 226,
                "y": 290
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f390d7c5-9327-4652-9d1f-2407c33e1c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 70,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 445,
                "y": 146
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8d9445ab-cb6b-4764-bf6e-d184f14f948f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 70,
                "offset": 5,
                "shift": 27,
                "w": 18,
                "x": 278,
                "y": 218
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9cc2e04b-d243-468f-bccf-d1706ba02dbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 70,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 83,
                "y": 146
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5c714ac0-5cec-43a4-bbb0-756f22b3fc30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 70,
                "offset": 4,
                "shift": 27,
                "w": 20,
                "x": 194,
                "y": 218
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "718c9a99-04e4-432d-bb15-c2a2ad89d7f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 70,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 415,
                "y": 74
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b8dbdf8f-454a-4d08-9a95-25dd790a4248",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 70,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 470,
                "y": 146
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "fc498bea-1e8d-4ecc-b0ba-7f8cd3de471e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 70,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 370,
                "y": 146
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7e71aca2-de50-4e4e-ac8d-f672ac971e94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 70,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 56,
                "y": 146
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6d979d62-0842-4ae8-aca4-4e76c520dd5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 70,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 122,
                "y": 290
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "cb809c27-3ad6-4b22-9e46-49a625e16d66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 70,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 136,
                "y": 290
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "be027737-a4af-4410-8e6b-aa419b9caade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 70,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 122,
                "y": 218
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d5d772cf-82b5-4fae-b404-ef8c3fcf3707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 70,
                "offset": 5,
                "shift": 36,
                "w": 25,
                "x": 253,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "72e3bdb6-7707-47c8-bd79-525c9fa367ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 70,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 146,
                "y": 218
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c02562f6-169a-4fc6-a9c4-6bcb42f82666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 70,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 170,
                "y": 218
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "84cb7093-6287-4e42-8211-76f1b2c35f03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 70,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a63bd3f1-a095-48f6-82c3-e563321fb68c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 70,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 344,
                "y": 146
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2b81b403-520f-4922-a8d6-00fb703a0536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 70,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 318,
                "y": 146
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ca67c054-2176-4153-8ea1-95f4bea5a2df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 70,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1f86cbad-f6f3-4c75-943e-689aad0ba669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 70,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 331,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "69c72fed-c273-471a-8cfe-3aa472f8cc60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 70,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 237,
                "y": 218
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "603c4d12-bdf8-49f8-b163-218717b18635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 70,
                "offset": 2,
                "shift": 23,
                "w": 23,
                "x": 420,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fd14bf37-142c-4e9f-a03d-3bc9b2b0ad41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 70,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 455,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "36e008fa-8b55-4aeb-a15b-d32428c33b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 70,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 266,
                "y": 146
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "023565de-f6db-492a-a41f-6b27cabdd498",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 70,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 214,
                "y": 290
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3352d294-5fb0-4ab8-ba4e-0fd7e082d10a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 70,
                "offset": -1,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 290
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "37eb8142-379d-40e2-938c-63ad565f4fc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 70,
                "offset": 3,
                "shift": 31,
                "w": 28,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9ddfee3f-3e00-401c-9770-0a8b9780ff93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 70,
                "offset": 2,
                "shift": 16,
                "w": 15,
                "x": 393,
                "y": 218
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "153da1c5-4804-43ab-b740-907c0b113e1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 70,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0f834283-dc9d-4a2c-97ad-9f89481021fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 70,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 188,
                "y": 146
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9611aaec-67e6-4193-9285-6e8484924ec8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 70,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5cf65a13-d9bb-40a6-bef9-c7638ff41ce8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 70,
                "offset": 2,
                "shift": 27,
                "w": 25,
                "x": 172,
                "y": 74
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e9654ce4-e23d-43b7-9b9d-d34c515dc124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 70,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "bfafb1b7-3795-4429-941a-77ab525bce69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 70,
                "offset": 3,
                "shift": 29,
                "w": 26,
                "x": 90,
                "y": 74
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d138de7a-6f95-4fc7-bf70-5c6c8bdd1588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 70,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 298,
                "y": 218
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f13095bc-79d8-4ca7-9af4-1575c5fac721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 70,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 226,
                "y": 74
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9614f7b8-230a-4f99-8178-91de13338c4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 70,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 110,
                "y": 146
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8b8d87c5-4ed4-447a-af14-798a862f3321",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 70,
                "offset": -2,
                "shift": 25,
                "w": 29,
                "x": 364,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "58ab2169-56e1-4013-966a-a818929dcd1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 70,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a97cfe28-5be4-4b46-bdbc-80ff06935c8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 70,
                "offset": -2,
                "shift": 24,
                "w": 28,
                "x": 395,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "93386f98-cb2f-42cb-a9a4-6288f2e5186e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 70,
                "offset": -2,
                "shift": 24,
                "w": 28,
                "x": 425,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4d891e50-720d-4815-a02b-bfef3df41913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 70,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 442,
                "y": 74
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4947bd93-e51d-4c8a-9e69-a45b1b1d7db1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 70,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 2,
                "y": 290
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1460b374-19f7-4368-97bc-6dd55d983e57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 70,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 491,
                "y": 218
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "de789fb5-74d4-41ad-8d86-696f07d1fc63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 70,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 443,
                "y": 218
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b4e551b7-1278-4d51-859d-3f607eb78f30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 70,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 136,
                "y": 146
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a93d0ca4-5c6b-4eae-b576-4fccab118fcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 70,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 162,
                "y": 146
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0eb944ff-c6a3-4302-9211-7c0900b97eed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 70,
                "offset": 5,
                "shift": 24,
                "w": 11,
                "x": 177,
                "y": 290
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "57d73bf2-be62-4588-9338-c99caacfcb25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 70,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8a6ebb3c-28d6-4c05-af25-75e241781364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 70,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 29,
                "y": 146
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "20ef7b93-d009-4c4c-ae50-844dd4af9e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 70,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 118,
                "y": 74
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "16eff8b0-a106-4299-94c5-6719e2a340b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 70,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 280,
                "y": 74
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "cc6fd346-7e46-4aef-b8b3-9a5918e6fdcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 70,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 469,
                "y": 74
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b0066223-0e0a-4a2e-b7af-93aa75efb6ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 70,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 18,
                "y": 290
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e0c3f3c5-dc88-497d-8e9c-50e70d6867a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 70,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 388,
                "y": 74
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0dc90271-4414-4063-90ae-dc7a1e9bc6c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 70,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 50,
                "y": 218
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3e9f7d51-d1d0-4a01-8049-3a586e24c9c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 70,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 202,
                "y": 290
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b0dfbab0-ec0c-46f0-8a47-011c523213ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 70,
                "offset": -4,
                "shift": 17,
                "w": 19,
                "x": 216,
                "y": 218
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d59560d0-362a-4e81-94f6-1d3c1e3029f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 70,
                "offset": 2,
                "shift": 26,
                "w": 25,
                "x": 307,
                "y": 74
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "43446a96-72a7-4467-80ae-952b37046393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 70,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 190,
                "y": 290
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "02845331-752c-4006-a5eb-d219cf344632",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 70,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 296,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5671c89e-87ee-4986-ae59-eb9437bf1ed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 70,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 74,
                "y": 218
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "732788b9-5df5-4425-aa09-02ac02b7270d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 70,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 334,
                "y": 74
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b5394fa4-407a-4790-931a-2994722e153a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 70,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 199,
                "y": 74
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "76581c4e-36cf-4c4a-a865-c4674fb00939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 70,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 145,
                "y": 74
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5aee8280-94d0-4d33-bf02-ceb1a7520b27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 70,
                "offset": 2,
                "shift": 15,
                "w": 14,
                "x": 475,
                "y": 218
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a6d8a414-7604-46af-98db-c67921357035",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 70,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 258,
                "y": 218
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "616c7869-df3e-4d11-83a6-ef98c59655f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 70,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 427,
                "y": 218
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f1c8ec60-e6bb-43f8-aed5-fc10ebd64e3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 70,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 98,
                "y": 218
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f21d03b2-a7fe-4426-84b6-e4b4a910ee93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 70,
                "offset": -2,
                "shift": 19,
                "w": 23,
                "x": 395,
                "y": 146
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0e74bb8a-acf5-4819-b8b4-01b76862c6c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 70,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 261,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "002aba61-55c4-4969-93df-796466f18ef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 70,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 2,
                "y": 218
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "86c6865b-2b6d-4ad6-8c3f-510dc231ee84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 70,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 26,
                "y": 218
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1f71f15e-18f6-42b6-bf54-003f06ac60f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 70,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 214,
                "y": 146
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "35a1aff8-6b33-4546-b8e5-fab8559de696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 70,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 410,
                "y": 218
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9c694195-7061-4d2d-b530-1a431f4ab2bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 70,
                "offset": 7,
                "shift": 24,
                "w": 10,
                "x": 238,
                "y": 290
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "16a9cd2d-5c8a-4d9f-8168-f77bb9e91743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 70,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 376,
                "y": 218
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ac981de0-122c-4aa2-accc-5c738c18714f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 70,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 240,
                "y": 146
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 36,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}