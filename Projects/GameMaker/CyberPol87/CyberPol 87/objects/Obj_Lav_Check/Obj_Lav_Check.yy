{
    "id": "507e0bb2-a772-4afc-83e1-e2308996c42f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_Lav_Check",
    "eventList": [
        {
            "id": "fdce2e87-0bc9-4a6e-bced-61dc45d324d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "507e0bb2-a772-4afc-83e1-e2308996c42f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9f317ddb-7b49-4e5e-9e3a-db0fe9c43bb9",
    "visible": true
}