{
    "id": "f84867af-54f6-4e9e-a3bb-1fff9e4430c6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_big",
    "eventList": [
        {
            "id": "1f99e6d9-56aa-4ee7-afab-6fdc9786f9be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f84867af-54f6-4e9e-a3bb-1fff9e4430c6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "1556f295-234c-4f56-8879-ed693cf338e0",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "0317d83c-d448-483f-843c-accc132ab067",
            "propertyId": "8da72806-ea26-4183-bab0-259f968e8bbb",
            "value": "2"
        }
    ],
    "parentObjectId": "0317d83c-d448-483f-843c-accc132ab067",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 6,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "f07b54ac-8936-467b-9da0-4dcea823cf33",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "49eeb099-b700-43fc-a9d0-7c89e76f33a2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "f3f4bc6c-643b-4120-8d1b-f257e62ddae5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 16
        },
        {
            "id": "166bc614-6f0f-4d92-8e7e-fef3f920cb1a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "d41fe891-edb6-49fb-85fc-9db29c26e1dc",
    "visible": true
}