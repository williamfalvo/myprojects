{
    "id": "68e3fc43-996f-4f4e-819d-230f1489e43e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fat_idle",
    "eventList": [
        {
            "id": "550de9e0-7aa1-4b73-98de-3028f6ae3506",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68e3fc43-996f-4f4e-819d-230f1489e43e"
        },
        {
            "id": "b502535a-24e1-4b84-b5c4-07e8714f8bac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "68e3fc43-996f-4f4e-819d-230f1489e43e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d69f68e-96fe-4108-938b-6d282eda61bc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "14d8a56d-6f44-4692-a904-a10ad6043411",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 18,
            "y": 5
        },
        {
            "id": "d0517cf8-6296-4fc7-b282-71955b95c808",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 5
        },
        {
            "id": "2e08ae46-3d2b-4a0a-a9cf-826ff7a90d56",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 64
        },
        {
            "id": "75bc9772-7ee1-4065-b4a2-ccc8eea36c78",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 18,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "6178b4ed-5f95-4fa9-93bb-929ad286808b",
    "visible": true
}