{
    "id": "ff7e2479-cf0e-4b48-bbf7-69c1ba43d67a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cassa_equip",
    "eventList": [
        {
            "id": "0c78f6f0-2f02-455b-aca4-a675b971297d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ff7e2479-cf0e-4b48-bbf7-69c1ba43d67a"
        },
        {
            "id": "c6df1e7f-3f02-4800-adc1-cd5e9c5c64a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "ff7e2479-cf0e-4b48-bbf7-69c1ba43d67a"
        },
        {
            "id": "79e0c420-cf52-4ff3-8aa0-d07be75ec11a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ff7e2479-cf0e-4b48-bbf7-69c1ba43d67a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 3,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": true,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "850b039b-bf74-4ab4-aa42-54bf8d7fab8f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 5,
            "y": 11
        },
        {
            "id": "8f46c75c-7c6b-4bbe-9991-451f060e4018",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 60,
            "y": 11
        },
        {
            "id": "b9b4619f-5335-489d-b965-bd8dc5805748",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 60,
            "y": 64
        },
        {
            "id": "2a38331c-ad0e-452f-85f2-4b8248a897b3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 5,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "9378dbae-ce30-45b7-92fd-2e310b3a7bc0",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "hp",
            "varType": 0
        },
        {
            "id": "6dafbc25-8f68-4863-bc9b-cb2c9b8346fc",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 256,
            "value": "obj_munizioni",
            "varName": "equip_inside",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "7047a7e2-6ad7-45ce-b898-4ad2efc4667f",
    "visible": true
}