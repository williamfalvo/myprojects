audio_play_sound(snd_shield_block,0,false);

if(time_in_use <= parry_range && other.sprite_index != spr_shotgun_bullet)
{
	obj_player.shield_durability -= parry_used;
	bullet = instance_create_layer(other.x,other.y,layer,obj_bullet);
	bullet.direction = other.direction + 180;
	bullet.phy_rotation = -bullet.direction;
	instance_destroy(other);
}
else
{
	if(other.sprite_index != spr_shotgun_bullet)
	{
		obj_player.shield_durability -= other.damage;
		instance_destroy(other);
	}
	else
	{
		if(!other.player_hitted)
		{
			obj_player.shield_durability -= other.damage;
			other.player_hitted = true;
		}
	}
	
	
}