{
    "id": "93e4c87e-4d5b-42fc-ba6a-12dcfbe4cd87",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shield",
    "eventList": [
        {
            "id": "51a69fb8-58d9-4ebd-ad8d-f117230e5597",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0317d83c-d448-483f-843c-accc132ab067",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "93e4c87e-4d5b-42fc-ba6a-12dcfbe4cd87"
        },
        {
            "id": "a8eee76e-b14c-40f5-8df1-6e8fbb2be4a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "93e4c87e-4d5b-42fc-ba6a-12dcfbe4cd87"
        },
        {
            "id": "b9cde9b1-0be1-4dd3-a675-4c646f8b8b70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a56b2e07-b084-46bf-a767-0c9745022d11",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "93e4c87e-4d5b-42fc-ba6a-12dcfbe4cd87"
        },
        {
            "id": "858ba212-4305-4a59-b701-81ad56f4589f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "93e4c87e-4d5b-42fc-ba6a-12dcfbe4cd87"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.1,
    "physicsFriction": 0.2,
    "physicsGroup": 3,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "75698575-e196-4abb-aa73-b8a06e683863",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 34,
            "y": 17
        },
        {
            "id": "0fc5b088-11eb-410d-bcba-2339a4aaccd2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 46,
            "y": 17
        },
        {
            "id": "d9ffef0a-0307-4631-ac16-859b86269883",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 46,
            "y": 82
        },
        {
            "id": "64f714d4-251c-4baf-b5dd-ff1edc15420f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 34,
            "y": 82
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "c7450164-20a4-42a5-bfdc-977d79a362bc",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "time_in_use",
            "varType": 0
        },
        {
            "id": "814a0a68-eb73-4fdd-a795-708a4771aa61",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "parry_range",
            "varType": 0
        },
        {
            "id": "a5533856-4df3-4126-b049-43353e0ea3b5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "35",
            "varName": "parry_used",
            "varType": 0
        }
    ],
    "solid": true,
    "spriteId": "0cc6bc4a-f446-490f-b220-015d8d9d28f3",
    "visible": false
}