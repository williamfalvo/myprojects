{
    "id": "aeeda0c3-d6d9-4352-b2c9-79b4dcde3d4d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_Lav_Scudo",
    "eventList": [
        {
            "id": "c3248bd8-2e0c-4f02-9fd8-06bc254eef4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aeeda0c3-d6d9-4352-b2c9-79b4dcde3d4d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "80cc80f5-04ef-4193-ab27-b0fae07546ab",
    "visible": true
}