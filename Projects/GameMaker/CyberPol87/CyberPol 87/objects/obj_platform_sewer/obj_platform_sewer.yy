{
    "id": "108f3102-712f-4f21-9f20-928f9b99331d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_platform_sewer",
    "eventList": [
        {
            "id": "c65b913c-6d6c-4fb2-ab2b-096a1a2302d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "108f3102-712f-4f21-9f20-928f9b99331d"
        },
        {
            "id": "02d71a4f-c112-447f-9c53-6621c34dff35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "391591f8-290b-4bb3-96b9-c4eb515f315c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "108f3102-712f-4f21-9f20-928f9b99331d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a56b2e07-b084-46bf-a767-0c9745022d11",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "6c12240a-ff26-4e24-bab8-46e74c513cdb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 5
        },
        {
            "id": "25603611-a785-4e02-aa3a-4ec9acaa5564",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 5
        },
        {
            "id": "f39de0b3-7c1d-4f8b-ac57-fad7523c5edb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 12
        },
        {
            "id": "d6933def-e882-48a5-bca7-8bbe3fe563aa",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 12
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "e9ebe6d8-4c92-4f41-abb1-ef1870cd2e5e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "120",
            "varName": "change_time",
            "varType": 1
        },
        {
            "id": "da161611-bf08-4a1b-a5a4-b7d8ac5e9c71",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "time_move",
            "varType": 0
        },
        {
            "id": "2be9e6ce-9322-4604-81d0-0f3f1d742cf3",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.5",
            "varName": "speed_x",
            "varType": 0
        },
        {
            "id": "c870c57a-f3e8-4598-a6e5-2566f65a7cd5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.5",
            "varName": "speed_y",
            "varType": 0
        },
        {
            "id": "5ebf58c6-bf1b-4bdc-924e-da9f9123ccca",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "isMoving",
            "varType": 3
        },
        {
            "id": "9470ffc9-a1f1-40f6-a7ef-65f8e8480e10",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "canChange",
            "varType": 3
        }
    ],
    "solid": true,
    "spriteId": "1483fc32-a1f8-4c6d-987d-9c969c152251",
    "visible": true
}