{
    "id": "0cbc2cef-0923-40e7-b1e3-bf62d5d127ef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ground_death",
    "eventList": [
        {
            "id": "b004f3b1-8cc5-46cd-908d-7f94cd4f7fa1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "391591f8-290b-4bb3-96b9-c4eb515f315c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0cbc2cef-0923-40e7-b1e3-bf62d5d127ef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "17d7331a-4725-49dd-9e24-9fabb41dc970",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "86a49ad9-a3a4-4f11-8bee-c4299e4d2490",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "a58ba320-c7dc-4ce0-b555-f8dbd614a89f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "80aab734-5783-459f-91b1-79f39669bff1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "4878a5a0-367e-4cb6-accf-e12d83194d6d",
    "visible": false
}