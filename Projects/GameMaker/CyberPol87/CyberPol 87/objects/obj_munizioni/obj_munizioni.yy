{
    "id": "084c3c90-d451-4cbc-9218-6ac1736f6e1c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_munizioni",
    "eventList": [
        {
            "id": "e2b6a580-4d35-4b9c-a0af-3929aabcaf4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "084c3c90-d451-4cbc-9218-6ac1736f6e1c"
        },
        {
            "id": "7d58c70d-cab8-405f-9eca-ea617c4593bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "084c3c90-d451-4cbc-9218-6ac1736f6e1c"
        },
        {
            "id": "a004a94a-8e3a-496d-bb66-22b9963abf39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a56b2e07-b084-46bf-a767-0c9745022d11",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "084c3c90-d451-4cbc-9218-6ac1736f6e1c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.1,
    "physicsFriction": 0.2,
    "physicsGroup": 4,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "a3f12d44-41c5-4d09-8207-5e55d186b62e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 5,
            "y": 9
        },
        {
            "id": "1252b3bd-f100-47b5-8a58-2e2992c40fee",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 27,
            "y": 9
        },
        {
            "id": "c2d96fe4-3659-41d3-a72f-5081159925b3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 27,
            "y": 26
        },
        {
            "id": "ecd96470-7d5a-495a-af23-72c15d26fc4c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 5,
            "y": 26
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fad9a9f1-c096-4a67-8980-fb7151f525d3",
    "visible": true
}