if(obj_gameManager.gameIsPaused) exit;


phy_position_x += lengthdir_x(bullet_speed,direction);
phy_position_y += lengthdir_y(bullet_speed,direction);

var player = instance_place(x,y,obj_player_commands);
if(player != noone && !player_hitted)
{
	if(obj_player.hp - damage < 0)
	{
		obj_player.hp = 0;
	}
	else
	{
		obj_player.hp -= damage;
	}
	show_debug_message("enemy hitted");
	player_hitted = true;
}



