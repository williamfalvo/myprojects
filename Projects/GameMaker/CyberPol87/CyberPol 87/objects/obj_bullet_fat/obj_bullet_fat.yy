{
    "id": "483573a7-9b1d-496c-914b-6f0febcb29ea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_fat",
    "eventList": [
        {
            "id": "1a9119c6-1a99-4f08-a1e9-ff1bbe00317f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "483573a7-9b1d-496c-914b-6f0febcb29ea"
        },
        {
            "id": "ffac0e68-4611-4a57-b50e-72dcac099d89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "483573a7-9b1d-496c-914b-6f0febcb29ea"
        },
        {
            "id": "a4bab327-68e8-4085-9d99-baef8f32da23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "483573a7-9b1d-496c-914b-6f0febcb29ea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "54f84cb1-4d81-4464-a4e6-3883d93dd04c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "0317d83c-d448-483f-843c-accc132ab067",
            "propertyId": "8da72806-ea26-4183-bab0-259f968e8bbb",
            "value": "3"
        },
        {
            "id": "e45817e8-1d99-42a2-b771-bb428e1c1f3c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "0317d83c-d448-483f-843c-accc132ab067",
            "propertyId": "0eb08564-2f19-4df5-b26a-b30f0d33106a",
            "value": "0"
        }
    ],
    "parentObjectId": "0317d83c-d448-483f-843c-accc132ab067",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 6,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "12598939-e055-4a12-a8b7-06849173d5cf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "b74eb945-5db2-41fc-a428-a49df4fb729a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 0
        },
        {
            "id": "33cab5f6-40f5-4bb7-bf08-2826734220ce",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 64
        },
        {
            "id": "3ce5f9b8-df68-42ec-b256-fb40abf5befc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "6cbbb56e-17a2-42c8-9931-8dbe9d4f2c42",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "player_hitted",
            "varType": 3
        }
    ],
    "solid": true,
    "spriteId": "c98f6357-5533-4390-8b6c-8bd9598779bf",
    "visible": true
}