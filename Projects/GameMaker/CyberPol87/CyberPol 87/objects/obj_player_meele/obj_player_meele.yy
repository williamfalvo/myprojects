{
    "id": "968c10ee-0adf-4807-911a-82e68f3d12d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_meele",
    "eventList": [
        {
            "id": "79150c49-1000-41d5-827b-0b8e62ca8d00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "968c10ee-0adf-4807-911a-82e68f3d12d9"
        },
        {
            "id": "5020520f-3a51-4c96-9a85-8ecf42d615f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "968c10ee-0adf-4807-911a-82e68f3d12d9"
        },
        {
            "id": "a08ada76-a63b-414d-be18-80b0b8394d6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "968c10ee-0adf-4807-911a-82e68f3d12d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "391591f8-290b-4bb3-96b9-c4eb515f315c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "219b52ee-7929-4a89-853a-512b010fe71f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 21
        },
        {
            "id": "1416b987-d85b-4cdf-91ac-431b7bc0f9cf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 42,
            "y": 21
        },
        {
            "id": "af69c639-4f90-4625-b381-9a8a61313fd6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 42,
            "y": 82
        },
        {
            "id": "48229855-f0c1-4919-9aef-3780f0a25f50",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 82
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "fbc31252-06c8-4767-a2d5-7a694be1db26",
    "visible": true
}