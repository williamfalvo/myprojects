{
    "id": "6a008094-ae80-45a0-896d-140a2e06f4dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_idle",
    "eventList": [
        {
            "id": "442578a7-21e3-4349-996d-299cf5151b1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6a008094-ae80-45a0-896d-140a2e06f4dc"
        },
        {
            "id": "fb7d80b7-f581-41f0-bf4e-988085b429ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6a008094-ae80-45a0-896d-140a2e06f4dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "391591f8-290b-4bb3-96b9-c4eb515f315c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "f7638ac4-6ad1-417a-8bd1-7c51027065d4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 13,
            "y": 21
        },
        {
            "id": "365e4a6d-6dbe-4625-aa02-135f0f4d5d88",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 52,
            "y": 21
        },
        {
            "id": "32c06f23-1a77-4371-ac32-88f9193aba08",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 52,
            "y": 82
        },
        {
            "id": "ea704e23-0475-4c5d-a66e-ce01ccc032c9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 13,
            "y": 82
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "08e1ff3b-1b61-4a01-b937-79783110f774",
    "visible": true
}