if(obj_gameManager.gameIsPaused) exit;


// Inherit the parent event
event_inherited();

// Movimento
if (Right) ^^ (Left)
{
	scr_player_set_state(obj_player_walk);
}

if (Up)
{
	direction = 90;
	sprite_index = player.gunpoint_up;
	if(player.bullet = obj_bullet)
	{
		player.x_shoot = -7;
		player.y_shoot = -44;
	}
	else
	{
		player.x_shoot = 0;
		player.y_shoot = -42;
	}
}
else if (Down) 
{
	direction = 270;
	sprite_index = player.gunpoint_down;
	if(player.bullet = obj_bullet)
	{
		player.x_shoot = 0;
		player.y_shoot = 38;
	}
	else
	{
		player.x_shoot = 5;
		player.y_shoot = 42;
	}
}
else 
{
	sprite_index = player.idle_right;
	if ( image_xscale == 1 )
	{
		direction = 0
	}
	else
	{
		direction = 180
	}
	if(player.bullet = obj_bullet)
	{
		player.x_shoot = 32;
		player.y_shoot = -7;
	}
	else
	{
		player.x_shoot = 40;
		player.y_shoot = -2;
	}	
}

// Salto
if (Jump) && (!Down)
{
	phy_speed_y -= player.jump_height;
	scr_player_set_state(obj_player_jump);		
}

// Scendere dalla piattaforma
if ( place_meeting(x, y+1, obj_platform) )
{
	if (Down) && (Jump)
	{
		scr_player_set_state(obj_player_fall);
		player.isFallin = false;
	}
}

// Sparo
if(Shoot && !player.Shooting && !Shield)
{
	enemy_near = collision_line(x,y,x+(player.meele_distance*image_xscale),y,obj_enemy_dad,false,true);
	if(enemy_near == noone)
	{
		player.Shooting = true;
		bullet = instance_create_layer(x+(player.x_shoot*image_xscale), y+player.y_shoot,layer, player.bullet);
		bullet.direction = direction;
		bullet.phy_rotation = -direction;
	}
	else
	{
		player.Shooting = true;
		scr_player_set_state(obj_player_meele);
	}
}	

if(Shield && player.shield_durability > 0)
{
	sprite_index = spr_player_defense;
	player.isDefence = true;
	if(!instance_exists(obj_shield))
	{
		instance_create_layer(x,y,layer,obj_shield);
	}
}
else
{
	instance_destroy(obj_shield);
	player.isDefence = false;
}


