{
    "id": "0124e28c-f18f-41b6-b1af-511fdf33599d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_death",
    "eventList": [
        {
            "id": "9e0f4ca2-b531-4b12-83f5-01a5f6171390",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "0124e28c-f18f-41b6-b1af-511fdf33599d"
        },
        {
            "id": "bddc47ce-b7f6-4bd9-b015-f287bb0c7a82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0124e28c-f18f-41b6-b1af-511fdf33599d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "391591f8-290b-4bb3-96b9-c4eb515f315c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "5e5f3141-5dd7-4398-8b50-92e5be444274",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 22,
            "y": 66
        },
        {
            "id": "bf356b94-cd03-4c5d-a9c6-40ead13a9138",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 36,
            "y": 66
        },
        {
            "id": "c99db4d5-0096-4579-8ebe-dc4c37b481c7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 36,
            "y": 82
        },
        {
            "id": "235edafa-c912-4cf7-8fc9-83115133f0cd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 22,
            "y": 82
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "474dc093-506d-412d-b4c4-573d26f6e929",
    "visible": true
}