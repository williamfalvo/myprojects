{
    "id": "ebf91079-9387-4a54-8bb8-8737e5740457",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_platform_move2",
    "eventList": [
        {
            "id": "72acd355-518d-4fd3-a470-25fafd3112d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ebf91079-9387-4a54-8bb8-8737e5740457"
        },
        {
            "id": "d0635731-2f2c-4ed3-ae69-e7e3d7483376",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "391591f8-290b-4bb3-96b9-c4eb515f315c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ebf91079-9387-4a54-8bb8-8737e5740457"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a56b2e07-b084-46bf-a767-0c9745022d11",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "ff8bbdcc-771c-49f0-b366-87a97f7ff414",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "953344a5-e30d-41fa-9b2f-cbf5cfc67af2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "c7ab974f-a499-4076-99c9-fda80dc94d76",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 14
        },
        {
            "id": "4f49667b-256b-4e27-8ae7-959aa3162229",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 14
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "4d174df9-cd8e-4058-8fa3-0f86416c0d70",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "120",
            "varName": "change_time",
            "varType": 1
        },
        {
            "id": "79694994-c699-41e1-9fab-c1feda68fde5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "time_move",
            "varType": 0
        },
        {
            "id": "f0059fed-b884-4889-9a1b-55c57ce2a960",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.5",
            "varName": "speed_x",
            "varType": 0
        },
        {
            "id": "75920272-9cfc-4c75-92bd-9d458a2b6dd5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.5",
            "varName": "speed_y",
            "varType": 0
        },
        {
            "id": "8b91b42c-ff6b-4e92-8bd7-170a87a51d14",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "isMoving",
            "varType": 3
        },
        {
            "id": "00a41296-2d86-434e-8785-2f631a5d113d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "canChange",
            "varType": 3
        }
    ],
    "solid": true,
    "spriteId": "546d065d-5ded-4d3e-abca-09ee904b46cd",
    "visible": true
}