if(obj_gameManager.gameIsPaused) exit;


// Inherit the parent event
event_inherited();

//Movimento
if ( Lock )
{
	if (Right) && (Left)
	{
		scr_player_set_state(obj_player_idle);
	}
	else if (Right) && (Up)
	{
		direction = 45;
		sprite_index = player.gunpoint_rightUp;
		image_xscale = 1;
		if(player.bullet = obj_bullet)
		{
			player.x_shoot = 24;
			player.y_shoot = -42;
		}
		else
		{
			player.x_shoot = 24;
			player.y_shoot = -38;
		}
	}
	else if (Right) && (Down)
	{
		direction = -45;
		sprite_index = player.gunpoint_rightDown;
		image_xscale = 1;
		if(player.bullet = obj_bullet)
		{
			player.x_shoot = 20;
			player.y_shoot = 26;
		}
		else
		{
			player.x_shoot = 20;
			player.y_shoot = 26;
		}
	}
	else if (Left) && (Up)
	{
		direction = 135;
		sprite_index = player.gunpoint_rightUp;
		image_xscale = -1;
		if(player.bullet = obj_bullet)
		{
			player.x_shoot = 24;
			player.y_shoot = -42;
		}
		else
		{
			player.x_shoot = 24;
			player.y_shoot = -38;
		}
	}
	else if (Left) && (Down)
	{
		direction = -135;
		sprite_index = player.gunpoint_rightDown;
		image_xscale = -1;
		if(player.bullet = obj_bullet)
		{
			player.x_shoot = 20;
			player.y_shoot = 26;
		}
		else
		{
			player.x_shoot = 20;
			player.y_shoot = 26;
		}
	}
	else if (Right)
	{
		sprite_index = player.gunpoint_right;
		image_xscale = 1;
		direction = 0;
		if(player.bullet = obj_bullet)
		{
			player.x_shoot = 32;
			player.y_shoot = -7;
		}
		else
		{
			player.x_shoot = 40;
			player.y_shoot = -2;
		}
	}
	else if (Left)
	{
		sprite_index = player.gunpoint_right;
		image_xscale = -1;
		direction = 180;
		if(player.bullet = obj_bullet)
		{
			player.x_shoot = 32;
			player.y_shoot = -7;
		}
		else
		{
			player.x_shoot = 40;
			player.y_shoot = -2;
		}
	}
	else if (!Right) && (!Left)
	{
		scr_player_set_state(obj_player_idle);
	}
}
else
{
	if (Right) && (Left)
	{
		scr_player_set_state(obj_player_idle);
	}
	else if (Right) && (Up)
	{
		phy_position_x += player.move_speed;
		direction = 45;
		sprite_index = player.walk_rightUp;
		image_xscale = 1;
		if(player.bullet = obj_bullet)
		{
			player.x_shoot = 24;
			player.y_shoot = -42;
		}
		else
		{
			player.x_shoot = 24;
			player.y_shoot = -38;
		}
	}
	else if (Right) && (Down)
	{
		phy_position_x += player.move_speed;
		direction = -45;
		sprite_index = player.walk_rightDown;
		image_xscale = 1;
		if(player.bullet = obj_bullet)
		{
			player.x_shoot = 20;
			player.y_shoot = 26;
		}
		else
		{
			player.x_shoot = 20;
			player.y_shoot = 26;
		}
	}
	else if (Left) && (Up)
	{
		phy_position_x -= player.move_speed;
		direction = 135;
		sprite_index = player.walk_rightUp;
		image_xscale = -1;
		if(player.bullet = obj_bullet)
		{
			player.x_shoot = 24;
			player.y_shoot = -42;
		}
		else
		{
			player.x_shoot = 24;
			player.y_shoot = -38;
		}
	}
	else if (Left) && (Down)
	{
		phy_position_x -= player.move_speed;
		direction = -135;
		sprite_index = player.walk_rightDown;
		image_xscale = -1;
		if(player.bullet = obj_bullet)
		{
			player.x_shoot = 20;
			player.y_shoot = 26;
		}
		else
		{
			player.x_shoot = 20;
			player.y_shoot = 26;
		}
	}
	else if (Right)
	{
		phy_position_x += player.move_speed;
		sprite_index = player.walk_right;
		image_xscale = 1;
		direction = 0;
		if(player.bullet = obj_bullet)
		{
			player.x_shoot = 32;
			player.y_shoot = -7;
		}
		else
		{
			player.x_shoot = 40;
			player.y_shoot = -2;
		}
	}
	else if (Left)
	{
		phy_position_x -= player.move_speed;
		sprite_index = player.walk_right;
		image_xscale = -1;
		direction = 180;
		if(player.bullet = obj_bullet)
		{
			player.x_shoot = 32;
			player.y_shoot = -7;
		}
		else
		{
			player.x_shoot = 40;
			player.y_shoot = -2;
		}
	}
	else if (!Right) && (!Left)
	{
		scr_player_set_state(obj_player_idle);
	}
}

//Salto
if (Jump) && (place_meeting(x, y+1, obj_ground))
{
	scr_player_set_state(obj_player_jump);	
}

if ( !place_meeting(x, y+1, obj_ground) )
{
	scr_player_set_state(obj_player_fall);
	player.isFallin = true;
}

//Attacco
if(Shoot && !player.Shooting && !Shield)
{
	enemy_near = collision_line(x,y,x+(player.meele_distance*image_xscale),y,obj_enemy_dad,false,true);
	if(enemy_near == noone)
	{
		player.Shooting = true;
		bullet = instance_create_layer(x+(player.x_shoot*image_xscale), y+player.y_shoot,layer, player.bullet);
		bullet.direction = direction;
		bullet.phy_rotation = -direction;
	}
	else
	{
		player.Shooting = true;
		scr_player_set_state(obj_player_meele);
	}
}	

if(Shield && player.shield_durability > 0)
{
	sprite_index = spr_player_defence_move;
	player.isDefence = true;
	if(!instance_exists(obj_shield))
	{
		instance_create_layer(x,y,layer,obj_shield);
	}
}
else
{
	instance_destroy(obj_shield);
	player.isDefence = false;
}





