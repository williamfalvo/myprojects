{
    "id": "bf835297-ff61-44bd-a882-0bfd92d480d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_walk",
    "eventList": [
        {
            "id": "2b7c6f75-3654-4514-b430-120cc8c7bb55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bf835297-ff61-44bd-a882-0bfd92d480d3"
        },
        {
            "id": "0a7b449c-e162-4182-afab-f03c524e964c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf835297-ff61-44bd-a882-0bfd92d480d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "391591f8-290b-4bb3-96b9-c4eb515f315c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "37dc3ee9-6cd4-4d89-8528-14d99067e1b7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 10,
            "y": 21
        },
        {
            "id": "99c41afe-31d7-4530-a68b-bc11266a2c09",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 49,
            "y": 21
        },
        {
            "id": "753d65f0-3e4e-4795-a648-519e6b206620",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 49,
            "y": 82
        },
        {
            "id": "1fa2743e-1f25-4fdd-84d8-5061ae379f5a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 10,
            "y": 82
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": true,
    "spriteId": "b285d13a-d3b4-4b74-9fe3-f3def6c97184",
    "visible": true
}