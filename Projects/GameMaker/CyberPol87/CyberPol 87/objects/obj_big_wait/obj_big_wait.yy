{
    "id": "556d2dd5-60ff-44eb-859d-2160839f3e90",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_big_wait",
    "eventList": [
        {
            "id": "821dcb6a-cece-4e01-8a1e-e66c82a5b8b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "556d2dd5-60ff-44eb-859d-2160839f3e90"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b691593-2856-4dcd-8481-d1e5806bb6d7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "f92d9cc4-ca4b-41e7-819a-831c7bafc5c5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 17,
            "y": 1
        },
        {
            "id": "2966d1a4-139b-458e-b218-eeb0b3a3b6ab",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 1
        },
        {
            "id": "ed4ea92b-a194-407a-bba7-ce69e67465b5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 64
        },
        {
            "id": "a2f766a8-c271-47ae-9087-9f9cc10144f9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 17,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "c0a4c2c3-51c1-4f4b-b49d-9d0ee08d25fd",
    "visible": true
}