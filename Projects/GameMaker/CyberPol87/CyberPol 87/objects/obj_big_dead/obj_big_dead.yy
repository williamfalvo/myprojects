{
    "id": "301620f3-11b3-4806-b26e-aaea63ea7b9a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_big_dead",
    "eventList": [
        {
            "id": "b206183f-bf60-4193-87a9-de43537df104",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "301620f3-11b3-4806-b26e-aaea63ea7b9a"
        },
        {
            "id": "f50beb4d-4baf-4d98-823a-e2bfe8b57b7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "301620f3-11b3-4806-b26e-aaea63ea7b9a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b691593-2856-4dcd-8481-d1e5806bb6d7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "fb834caa-951c-454d-bc8b-913c69f288f5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 10
        },
        {
            "id": "e89a5e60-57f1-47d5-bd7e-2edb867ac7c5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 42,
            "y": 10
        },
        {
            "id": "b8b10be7-d465-429e-8c3d-ccd5779db18d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 42,
            "y": 64
        },
        {
            "id": "e1716dcc-8107-4121-a092-3615701c1e4e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "b387afc0-6efe-4855-bbe9-eb61a01e244c",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2.5",
            "varName": "animation_dead",
            "varType": 0
        }
    ],
    "solid": true,
    "spriteId": "5d85a362-c31a-440a-ae91-5833c26a0543",
    "visible": true
}