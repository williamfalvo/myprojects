{
    "id": "78b5dd06-c26c-4ba5-b2d8-970c8b302c9e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_platform",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a56b2e07-b084-46bf-a767-0c9745022d11",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "46f54890-8b5d-4053-8b21-93f31ef37511",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "9e70d954-ebf8-4da4-840f-44cadeb2559f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "22de1200-277e-49f6-a755-ebf77e40a7f3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "55ec37eb-44f1-4ae5-9d41-76190e7fd0ac",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "a3485abd-1dfc-42e2-892b-2981d6990815",
    "visible": false
}