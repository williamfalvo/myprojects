Down = keyboard_check(vk_down) || (gamepad_axis_value(0,gp_axislv)>0.3) || gamepad_button_check(0,gp_padd);
Up = keyboard_check(vk_up) || (gamepad_axis_value(0,gp_axislv)<-0.3) || gamepad_button_check(0,gp_padu);
Conferma = keyboard_check_pressed(vk_enter) || gamepad_button_check_pressed(0,gp_face1);


if(gamepad_button_check_pressed(0,gp_start) || keyboard_check_pressed(vk_escape))
{
	if (!gameIsPaused)
	{	
		instance_activate_layer("Pause");
		instance_deactivate_all(true);
		gameIsPaused = true;
		indexPause = 0;
	}
	else
	{
		instance_deactivate_layer("Pause");
		instance_activate_all();
		gameIsPaused = false;
	}
}



if(gameIsPaused && !GameOver)
{
	if(Down && indexPause == 0)
	{
		indexPause = 1;
	}
	else if(Up && indexPause == 1)
	{
		indexPause = 0;
	}
	
	if(Conferma)
	{
		switch(indexPause)
		{
			case 0:
				if(!Exit)
				{
					instance_deactivate_layer("Pause");
					instance_activate_all();
					gameIsPaused = false;
				}
				else
				{
					Exit = false;
				}
				break;
			case 1:
				if(!Exit)
				{
					Exit = true;
				}
				else
				{
					room_goto(rm_main_menu);
					audio_stop_all();
					audio_play_sound(snd_cutscene,0,true);
					instance_destroy();
					gameIsPaused = false;
					Exit = false;
				}
				break;
		}
	}
}

if(GameOver)
{
	instance_activate_layer("Pause");
	instance_deactivate_all(true);
	
	if(Down && indexPause == 0)
	{
		indexPause = 1;
	}
	else if(Up && indexPause == 1)
	{
		indexPause = 0;
	}
	
	if(Conferma)
	{
		switch(indexPause)
		{
			case 0:
				GameOver = false;
				live_player = 3;
				instance_deactivate_layer("Pause");
				instance_activate_all();
				instance_destroy(obj_checkPoint);
				room_restart();
				break;
			case 1:
				room_goto(rm_main_menu);
				audio_stop_all();
				audio_play_sound(snd_cutscene,0,true);
				instance_destroy();
				break;
		}
	}
}




