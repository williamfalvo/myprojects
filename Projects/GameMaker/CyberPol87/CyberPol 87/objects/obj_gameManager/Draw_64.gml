if(gameIsPaused && !GameOver)
{
	if(!Exit)
	{
		draw_sprite_ext(spr_pause_txt,0,980,300,3,3,0,c_white,1);
		if(indexPause == 0)
		{
			draw_sprite_ext(spr_continua_txt,1,980,500,2.5,2.5,0,c_white,1);
			draw_sprite_ext(spr_ESCi,0,980,625,3,3,0,c_white,1);
		}
		else
		{
			draw_sprite_ext(spr_continua_txt,0,980,500,2.5,2.5,0,c_white,1);
			draw_sprite_ext(spr_ESCi,1,980,625,3,3,0,c_white,1);
		}
	}
	else
	{
		draw_sprite_ext(spr_pause_txt,0,980,300,3,3,0,c_white,1);
		draw_sprite_ext(spr_rm_exit,0,980,400,3,3,0,c_white,1);
		if(indexPause == 0)
		{
			draw_sprite_ext(spr_NO,1,980,550,2.5,2.5,0,c_white,1);
			draw_sprite_ext(spr_SI,0,980,650,3,3,0,c_white,1);
		}
		else
		{
			draw_sprite_ext(spr_NO,0,980,550,2.5,2.5,0,c_white,1);
			draw_sprite_ext(spr_SI,1,980,650,3,3,0,c_white,1);
		}
	}
}
else if(GameOver)
{
		draw_sprite_ext(spr_GameOver,0,980,300,3,3,0,c_white,1);
		draw_sprite_ext(spr_VuoiRiprovare,0,980,400,3,3,0,c_white,1);
		if(indexPause == 0)
		{
			draw_sprite_ext(spr_SI,1,980,550,3,3,0,c_white,1);
			draw_sprite_ext(spr_NO,0,980,650,2.5,2.5,0,c_white,1);
		}
		else
		{
			draw_sprite_ext(spr_SI,0,980,550,3,3,0,c_white,1);
			draw_sprite_ext(spr_NO,1,980,650,2.5,2.5,0,c_white,1);
		}
}
