{
    "id": "4f8c4f6a-73ca-48c3-8f45-05dc29bc6070",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_switch_story_menu",
    "eventList": [
        {
            "id": "3bf23291-f20f-4532-937e-ac40bf4b9f63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4f8c4f6a-73ca-48c3-8f45-05dc29bc6070"
        },
        {
            "id": "e6b22c58-c32d-4b32-aa63-614c3a2938ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4f8c4f6a-73ca-48c3-8f45-05dc29bc6070"
        },
        {
            "id": "615e8b64-4608-4f1b-8c18-fb7e4847aeec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4f8c4f6a-73ca-48c3-8f45-05dc29bc6070"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}