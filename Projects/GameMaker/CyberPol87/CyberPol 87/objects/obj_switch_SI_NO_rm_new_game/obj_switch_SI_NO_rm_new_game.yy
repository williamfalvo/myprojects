{
    "id": "df8a6969-8d82-4f53-ba74-473737f23655",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_switch_SI_NO_rm_new_game",
    "eventList": [
        {
            "id": "09bea9fd-e235-4f67-97f2-363f99316104",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df8a6969-8d82-4f53-ba74-473737f23655"
        },
        {
            "id": "7610a4a8-b534-45af-bb72-d96e0495ea01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "df8a6969-8d82-4f53-ba74-473737f23655"
        },
        {
            "id": "f3d6b641-3d6f-4a26-842e-0165a5802f9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "df8a6969-8d82-4f53-ba74-473737f23655"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}