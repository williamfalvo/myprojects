obj_SI.image_index = 1;
menu_x = room_width/2;
menu_y = y;
button_h = 24;
button_w = 256;
button_padding = 18;

// Buttons
button[0] = "Si";
button[1] = "No";
buttons = array_length_1d(button);

menu_index = 0;
last_selected = 0;

var i = 0;
repeat(buttons) {
	flying[i] = 0;
	i++;
}

particle_effects = part_system_create();
part_system_depth(particle_effects, -1000);

box_flash = part_type_create();
part_type_shape(box_flash, pt_shape_line);
part_type_size(box_flash, 4.5, 0, -0.01, 0);
part_type_color3(box_flash, c_blue, c_white, c_red);
part_type_alpha3(box_flash, 0.2, 0.5, 0);
part_type_blend(box_flash, true);
part_type_life(box_flash, 20, 20);