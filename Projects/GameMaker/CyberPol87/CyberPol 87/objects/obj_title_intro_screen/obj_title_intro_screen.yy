{
    "id": "ca9fc627-0d3a-4002-a2f9-04755366773f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_title_intro_screen",
    "eventList": [
        {
            "id": "2789625d-c873-4847-afc7-94e48eb565e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca9fc627-0d3a-4002-a2f9-04755366773f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5d5c5da6-ab06-4672-a0c1-a1d2beaec4b3",
    "visible": true
}