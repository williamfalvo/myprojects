if(obj_gameManager.gameIsPaused) exit;


phy_position_x += lengthdir_x(bullet_speed,direction);
phy_position_y += lengthdir_y(bullet_speed,direction);

if(place_meeting(x,y,obj_player_commands))
{
	if(obj_player.hp - damage < 0)
	{
		obj_player.hp = 0;
		
	}
	else
	{
		audio_play_sound(snd_damage,0,false);
		obj_player.hp -= damage;
		obj_player_commands.timeline_index = tml_player_hit;
		obj_player_commands.timeline_position = 0;
		obj_player_commands.timeline_running = true;
	}
	instance_destroy();
}

if(place_meeting(x,y,obj_floor))
{
	instance_destroy();
}