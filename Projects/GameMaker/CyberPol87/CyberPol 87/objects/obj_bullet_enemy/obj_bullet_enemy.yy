{
    "id": "0317d83c-d448-483f-843c-accc132ab067",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_enemy",
    "eventList": [
        {
            "id": "45c61eed-b3ed-49c1-863f-5bd94861bd35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0317d83c-d448-483f-843c-accc132ab067"
        },
        {
            "id": "4af1c279-32aa-41c5-82fb-62ccb48419ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0317d83c-d448-483f-843c-accc132ab067"
        },
        {
            "id": "f1a4b3c7-6ada-40f4-97b6-f698c6a26842",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "0317d83c-d448-483f-843c-accc132ab067"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 6,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "fd9e0460-8b4a-4274-8958-5dbf944c6c01",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 5
        },
        {
            "id": "910590fa-283c-4eb0-b941-234734841e70",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 5
        },
        {
            "id": "328696c6-4504-4dc4-af0b-07beca644bb3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 11
        },
        {
            "id": "897c2d05-b585-472e-845c-d3c9aca0b722",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 11
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "0eb08564-2f19-4df5-b26a-b30f0d33106a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "3",
            "varName": "bullet_speed",
            "varType": 0
        },
        {
            "id": "8da72806-ea26-4183-bab0-259f968e8bbb",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "damage",
            "varType": 0
        }
    ],
    "solid": true,
    "spriteId": "c3982258-691d-4829-888e-359f1a934931",
    "visible": true
}