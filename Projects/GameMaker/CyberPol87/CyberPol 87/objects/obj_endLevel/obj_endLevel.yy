{
    "id": "15feba62-dcd2-4239-9790-55b2ca0ed5ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_endLevel",
    "eventList": [
        {
            "id": "7ba8f399-378c-48a9-9e31-7ed654b33fe6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "15feba62-dcd2-4239-9790-55b2ca0ed5ee"
        },
        {
            "id": "9f3397b4-3ba9-49e2-9075-92ca897b26aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "15feba62-dcd2-4239-9790-55b2ca0ed5ee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "e5137b89-28e7-41c3-88bc-71984c49c75d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "onPlayer",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "c4b92eaa-4c53-413d-95ca-1c7442a1d007",
    "visible": true
}