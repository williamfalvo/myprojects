if(obj_gameManager.gameIsPaused) exit;


//////////////////// Fix Rotazione ////////////////////

phy_fixed_rotation = true;
phy_rotation = 0

var bullet = instance_place(x,y,obj_bullet);
if( bullet != noone )
{
	if(obj_player.bullet != obj_shotgun_bullet) 
	{
		audio_play_sound(snd_damage_enemy,0,false);
		enemy_fat.hp -= bullet.damage;
		instance_destroy(bullet);
	}
	else if( !bullet.enemy_hitted )
	{
		audio_play_sound(snd_damage_enemy,0,false);
		enemy_fat.hp -= bullet.damage;
		bullet.enemy_hitted = true;
	}
}

if (instance_exists(obj_esplosione_barile))
{
	if ( place_meeting(x,y,obj_esplosione_barile) && !enemy_fat.is_explode)
	{
		enemy_fat.hp -= obj_esplosione_barile.damage
		enemy_fat.is_explode = true
	}
}
