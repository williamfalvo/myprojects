{
    "id": "6d69f68e-96fe-4108-938b-6d282eda61bc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fat_commands",
    "eventList": [
        {
            "id": "c62148ab-ae85-4a4d-bd68-d90d77c7ea23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6d69f68e-96fe-4108-938b-6d282eda61bc"
        },
        {
            "id": "252a638a-a994-441e-ba6d-3be8e0f8e354",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "6d69f68e-96fe-4108-938b-6d282eda61bc"
        },
        {
            "id": "69f71556-ca52-4779-8280-8e595cc6f9f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "6d69f68e-96fe-4108-938b-6d282eda61bc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        
    ],
    "parentObjectId": "7c572097-4bf4-4928-9803-6d565f3bd51b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "9240e5ab-d1f5-4b57-8544-464d57c0bec2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "c6ffd070-a507-4d5b-a84b-c90c11a02a5f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "b8a2c2d3-35c6-4a55-bf04-0ef1c69580e1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "abbfe302-21a2-4337-b775-3dd5e172156c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "27891a3e-f3d2-436e-aa32-674c380bbca5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "noone",
            "varName": "nextState",
            "varType": 1
        }
    ],
    "solid": true,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}