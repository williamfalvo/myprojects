var viewport = view_camera[0];
draw_set_font(fnt_munitions);
//Lives UI
draw_sprite_ext(spr_lives_txt, 0, lives_x, lives_y, 1, 1, 0, c_white, 1);
draw_sprite_ext(spr_x_txt, 0, lives_x + 100, lives_y, 1, 1, 0, c_white, 1);
switch(obj_gameManager.live_player)
{
	case(3):
		draw_sprite_ext(spr_3, 0, lives_x + 135, lives_y, scale-0.5, scale-0.5, 0, c_white, 1);
		break;
	case(2):
		draw_sprite_ext(spr_2, 0, lives_x + 135, lives_y, scale-0.5, scale-0.5, 0, c_white, 1);
		break;
	case(1):
		draw_sprite_ext(spr_1, 0, lives_x + 135, lives_y, scale-0.5, scale-0.5, 0, c_white, 1);
		break;
	case(0):
		draw_sprite_ext(spr_0, 0, lives_x + 135, lives_y, scale-0.5, scale-0.5, 0, c_white, 1);
		break;	
}

//Health UI
draw_sprite_ext(spr_healtBox, 0, health_x, health_y, box_scale*scale, scale, 0, c_white, 1);
draw_sprite_ext(spr_healtBar, 0, health_x, health_y, health_xscale*scale, scale, 0, c_white, 1);
draw_sprite_ext(spr_health_txt, 0, health_x-115, health_y, 1.5, 1.5, 0, c_white, 1);
//Shield UI
draw_sprite_ext(spr_shieldBox, 0, shield_x, shield_y, box_scale*scale, scale, 0, c_white, 1);
draw_sprite_ext(spr_shieldBar,0, shield_x, shield_y, shield_xscale*scale, scale, 0, c_white, 1);
draw_sprite_ext(spr_shield_txt, 0, shield_x-75, shield_y, 1, 1, 0, c_white, 1);
//Weapon UI
draw_sprite_ext(spr_armaBox, 0, weapon_x, weapon_y, scale+0.25, scale+0.25, 0, c_white, 1);
draw_sprite_ext(weapon, 0, weapon_x, weapon_y-5, scale-0.25, scale-0.25, 45, c_white, 1);
draw_sprite_ext(spr_munitions, 0, weapon_x+250, weapon_y-50, 2, 2, 0, c_white, 1);
draw_text(weapon_x+250,weapon_y-10,obj_player.munitions);

