//menu_move = keyboard_check_pressed(vk_down) - keyboard_check_pressed(vk_up);
//menu_index += menu_move;
if (scr_check_menu_command_select_level_screen(vk_right, gp_padr))
{
	menu_index += 1;
	if(menu_index == 0) {
		obj_SELECT_TUTORIAL_OSPEDALE.image_index = 1;
	}
	else {
		obj_SELECT_TUTORIAL_OSPEDALE.image_index = 0;
	}
	
	if(menu_index == 1) {
		obj_SELECT_LEVEL_1_MAGAZZINO.image_index = 1;
	}
	else {
		obj_SELECT_LEVEL_1_MAGAZZINO.image_index = 0;
	}
	
	if(menu_index == 2) {
		obj_SELECT_LEVEL_2_FOGNE.image_index = 1;
	}
	else {
		obj_SELECT_LEVEL_2_FOGNE.image_index = 0;
	}
	if(menu_index == 3) {
		obj_SELECT_LEVEL_3_VILLA.image_index = 1;
	}
	else {
		obj_SELECT_LEVEL_3_VILLA.image_index = 0;
	}
}

if (scr_check_menu_command_select_level_screen(vk_left, gp_padl))
{
	menu_index -= 1;
	if(menu_index == 0) {
		obj_SELECT_TUTORIAL_OSPEDALE.image_index = 1;
	}
	else {
		obj_SELECT_TUTORIAL_OSPEDALE.image_index = 0;
	}
	
	if(menu_index == 1) {
		obj_SELECT_LEVEL_1_MAGAZZINO.image_index = 1;
	}
	else {
		obj_SELECT_LEVEL_1_MAGAZZINO.image_index = 0;
	}
	
	if(menu_index == 2) {
		obj_SELECT_LEVEL_2_FOGNE.image_index = 1;
	}
	else {
		obj_SELECT_LEVEL_2_FOGNE.image_index = 0;
	}
	if(menu_index == 3) {
		obj_SELECT_LEVEL_3_VILLA.image_index = 1;
	}
	else {
		obj_SELECT_LEVEL_3_VILLA.image_index = 0;
	}
}

if (menu_index < 0) {
	menu_index = buttons - 1;
	obj_SELECT_LEVEL_3_VILLA.image_index = 1;
}
if (menu_index > buttons - 1) {
	menu_index = 0;
	obj_SELECT_TUTORIAL_OSPEDALE.image_index = 1;
}

var i = 0;
repeat(buttons) {
	
	if(flying[i] == 1) {
	i++;
	}
	
	if(i < buttons) {
		flying[i] = min(1, flying[i] + 0.02);
	}
	
	if(i + 1 < buttons) {
		flying[i + 1] = min(1, flying[i + 1] + 0.005);
	}
}

if (menu_index != last_selected) {
	//part_particles_create(particle_effects, menu_x, menu_y - 78 + (button_h + button_padding) * menu_index, box_flash, 1);
	audio_play_sound(snd_menu_switch, 1, false);
}

last_selected = menu_index;

if(scr_check_menu_command(vk_enter, gp_face1)) {
switch(menu_index) {
	case 0:
		room_goto(rm_Cutscene_Ospedale);
		break;
	case 1:
	    room_goto(Room_Cutscene_Magazzino);        //aggiungere collegamento con schermata di gioco
		break;
	case 2:
	    room_goto(Room_Cutscene_Fogne);     // stessa cosa di sopra ma per schermata di selezione livello
		break;
	case 3:
	    room_goto(Room_Cutscene_Villa);
		}
}


if(scr_check_menu_command(vk_escape, gp_face2)) {
	room_goto(rm_story_menu);
}

//Codice per passaggio al relativo livello
/*if(scr_check_menu_command(vk_enter, gp_face1)) {

}*/

obj_ENTER_A_per_continuare.image_index = global.last_device;
obj_ESC_B_indietro.image_index = global.last_device;
