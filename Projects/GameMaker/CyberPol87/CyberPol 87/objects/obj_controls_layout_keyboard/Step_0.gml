if(gamepad_is_connected(0)) {
	obj_ENTER_A_per_continuare.image_index = 1;
	obj_ESC_B_indietro.image_index = 1;
}

else {
	obj_ENTER_A_per_continuare.image_index = 0;
	obj_ESC_B_indietro.image_index = 0;
}

if(scr_check_menu_command(vk_escape, gp_face2)) {
	room_goto(rm_controls_gamepad);
}

if(scr_check_menu_command(vk_enter, gp_face1)) {
	room_goto(rm_main_menu);
}