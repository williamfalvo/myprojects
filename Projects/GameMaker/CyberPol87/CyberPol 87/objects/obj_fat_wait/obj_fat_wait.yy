{
    "id": "8e705dab-3123-42c1-bf5d-01959a88f60d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fat_wait",
    "eventList": [
        {
            "id": "4fcb3549-3160-4cbf-9fa3-4d9553956227",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8e705dab-3123-42c1-bf5d-01959a88f60d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d69f68e-96fe-4108-938b-6d282eda61bc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "a97915c6-aa89-46bc-bab7-512d902091f3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 18,
            "y": 6
        },
        {
            "id": "50487eff-6f39-4213-8a18-008d50838cfa",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 38,
            "y": 6
        },
        {
            "id": "01d3000e-7160-44c8-a0e1-94d052f4a756",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 38,
            "y": 64
        },
        {
            "id": "96c63aec-24c9-43d4-af73-18830c6bcbff",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 18,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "661eae07-fba8-472a-bce9-44987a7ba0fa",
    "visible": true
}