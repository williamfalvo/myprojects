{
    "id": "8d318d66-d9af-4c74-a7c8-772714df6a75",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_camera_wall",
    "eventList": [
        {
            "id": "131a375e-402d-45e4-853c-6925205d871d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8d318d66-d9af-4c74-a7c8-772714df6a75"
        },
        {
            "id": "d4d7b388-99e9-4995-9da1-4c8b893238aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d318d66-d9af-4c74-a7c8-772714df6a75"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "aa6f7c34-4669-4fda-9eaf-2fe189b468ac",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "64597600-b4e1-4d79-aeb7-c8de22557bfa",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "90ba09db-2c6c-4f9c-b523-e4a99c0566fe",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 360
        },
        {
            "id": "7b98e1cd-6af5-4d77-93aa-9e6c93a45608",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 360
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "82691acb-8279-4020-a9f6-ea9e40f74d66",
    "visible": false
}