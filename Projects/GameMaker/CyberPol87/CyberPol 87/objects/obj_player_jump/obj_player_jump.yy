{
    "id": "53ffa5a0-7603-43c4-895d-8614e7b3a036",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_jump",
    "eventList": [
        {
            "id": "4262016e-5659-4250-b18f-7e748b20a05f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "53ffa5a0-7603-43c4-895d-8614e7b3a036"
        },
        {
            "id": "ad4af5f1-3ab4-4f47-9a5a-ae8afac6e0b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "53ffa5a0-7603-43c4-895d-8614e7b3a036"
        },
        {
            "id": "a962b04e-1dff-4c4c-9499-f3c6969f8724",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "53ffa5a0-7603-43c4-895d-8614e7b3a036"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "391591f8-290b-4bb3-96b9-c4eb515f315c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "9824221f-fab2-4d16-9205-d21fa6fb26a9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 15,
            "y": 26
        },
        {
            "id": "e1e45841-28ce-432d-bb66-9eb3759436d4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 30,
            "y": 26
        },
        {
            "id": "35f8c5bc-522f-4f31-a12a-2a5a051e4e80",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 30,
            "y": 82
        },
        {
            "id": "d557235f-8c3b-4fd6-a49f-e1c1ab3427af",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 15,
            "y": 82
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": true,
    "spriteId": "6e49e9ec-03ce-4446-8943-5c41a1954625",
    "visible": true
}