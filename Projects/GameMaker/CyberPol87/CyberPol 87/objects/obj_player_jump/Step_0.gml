// Inherit the parent event
event_inherited();

//Controllo se ho già passato una piattaforma, e se vera riattivo la fisica delle piattaforme 
if(!player.touchPlatform)
{
	if(place_meeting(x,y,obj_platform))
	{
		player.touchPlatform = true;
	}
}
else
{
	if(!place_meeting(x,y,obj_platform))
	{
		obj_platform.phy_active = true;
	}
}

//Se sto cadendo controllo se ho sotto di me il ground così da passare in idle
if (phy_speed_y >= 0 ) && (position_meeting(bbox_left,bbox_bottom+2,obj_ground) || position_meeting(bbox_right,bbox_bottom+2,obj_ground))
{
	scr_player_set_state(obj_player_idle);
}


//Faccio si che il player non possa fare azioni ma controlla lo stesso la caduta
if(obj_gameManager.gameIsPaused) exit;



//Movimento
if (Right) && (Up)
{
	phy_position_x += player.move_speed;
	direction = 45;
	prev_frame = image_index;
	sprite_index = player.jump_rightUp;
	image_index = prev_frame;
	image_xscale = 1;
	if(player.bullet = obj_bullet)
	{
		player.x_shoot = 24;
		player.y_shoot = -42;
	}
	else
	{
		player.x_shoot = 24;
		player.y_shoot = -38;
	}
}
else if (Right) && (Down)
{
	phy_position_x += player.move_speed;
	direction = -45;
	prev_frame = image_index;
	sprite_index = player.jump_rightDown;
	image_index = prev_frame;
	image_xscale = 1;
	if(player.bullet = obj_bullet)
	{
		player.x_shoot = 20;
		player.y_shoot = 26;
	}
	else
	{
		player.x_shoot = 20;
		player.y_shoot = 26;
	}
}
else if (Left) && (Up)
{
	phy_position_x -= player.move_speed;
	direction = 135;
	prev_frame = image_index;
	sprite_index = player.jump_rightUp;
	image_index = prev_frame;
	image_xscale = -1;
	if(player.bullet = obj_bullet)
	{
		player.x_shoot = 24;
		player.y_shoot = -42;
	}
	else
	{
		player.x_shoot = 24;
		player.y_shoot = -38;
	}
}
else if (Left) && (Down)
{
	phy_position_x -= player.move_speed;
	direction = -135;
	prev_frame = image_index;
	sprite_index = player.jump_rightDown;
	image_index = prev_frame;
	image_xscale = -1;
	if(player.bullet = obj_bullet)
	{
		player.x_shoot = 20;
		player.y_shoot = 26;
	}
	else
	{
		player.x_shoot = 20;
		player.y_shoot = 26;
	}
}
else if (Right) && (!Left)
{
	phy_position_x += player.move_speed;
	prev_frame = image_index;
	sprite_index = player.jump_right;
	image_index = prev_frame;
	image_xscale = 1;
	direction = 0;
	if(player.bullet = obj_bullet)
	{
		player.x_shoot = 32;
		player.y_shoot = -7;
	}
	else
	{
		player.x_shoot = 40;
		player.y_shoot = -2;
	}
}
else if (Left) && (!Right)
{
	phy_position_x -= player.move_speed;
	prev_frame = image_index;
	sprite_index = player.jump_right;
	image_index = prev_frame;
	image_xscale = -1;
	direction = 180;
	if(player.bullet = obj_bullet)
	{
		player.x_shoot = 32;
		player.y_shoot = -7;
	}
	else
	{
		player.x_shoot = 40;
		player.y_shoot = -2;
	}
}
else if(Down)
{
	direction = 270;
	prev_frame = image_index;
	sprite_index = player.jump_down;
	image_index = prev_frame;
	if(player.bullet = obj_bullet)
	{
		player.x_shoot = 0;
		player.y_shoot = 38;
	}
	else
	{
		player.x_shoot = 5;
		player.y_shoot = 42;
	}
}
else if(Up)
{
	direction = 90;
	prev_frame = image_index;
	sprite_index = player.jump_up;
	image_index = prev_frame;
	if(player.bullet = obj_bullet)
	{
		player.x_shoot = -7;
		player.y_shoot = -44;
	}
	else
	{
		player.x_shoot = 0;
		player.y_shoot = -42;
	}
}
else if( !Right && !Left )
{
	prev_frame = image_index;
	sprite_index = player.jump_right;
	image_index = prev_frame;
	if ( image_xscale == 1 )
	{
		direction = 0
	}
	else
	{
		direction = 180
	}
	if(player.bullet = obj_bullet)
	{
		player.x_shoot = 32;
		player.y_shoot = -7;
	}
	else
	{
		player.x_shoot = 40;
		player.y_shoot = -2;
	}	
}

//Sparo
if(Shoot && !player.Shooting && !Shield)
{
	enemy_near = collision_line(x,y,x+(player.meele_distance*image_xscale),y,obj_enemy_dad,false,true);
	if(enemy_near == noone)
	{
		player.Shooting = true;
		bullet = instance_create_layer(x+(player.x_shoot*image_xscale), y+player.y_shoot,layer, player.bullet);
		bullet.direction = direction;
		bullet.phy_rotation = -direction;
	}
	else
	{
		player.Shooting = true;
		scr_player_set_state(obj_player_meele);
	}
}	

if(Shield && player.shield_durability > 0)
{
	sprite_index = spr_player_defense;
	player.isDefence = true;
	if(!instance_exists(obj_shield))
	{
		instance_create_layer(x,y,layer,obj_shield);
	}
}
else
{
	instance_destroy(obj_shield);
	player.isDefence = false;
}






