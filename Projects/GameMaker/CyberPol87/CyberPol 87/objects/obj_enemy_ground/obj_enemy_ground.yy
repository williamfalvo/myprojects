{
    "id": "37f5c343-00e9-4fb4-8826-ecb63f9f1352",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_ground",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "1b9e8392-3955-46a7-950d-d3f5b2baa2f1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "5b8aca45-4ee6-42bf-8cf4-5efc1bee2757",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "3002863e-1cc6-48fc-8df1-8c6e4d324085",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "3e1efc00-3fa4-48f8-86c4-66735b51d6f8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "502bba73-089b-446b-8ee9-83bda9c65106",
    "visible": false
}