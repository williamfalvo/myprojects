{
    "id": "990f62cf-e6cf-4255-8ff4-9a58a534a50b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_esplosione_barile",
    "eventList": [
        {
            "id": "4ea74b0a-7c7d-44d2-864a-77cd4d3dd410",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "990f62cf-e6cf-4255-8ff4-9a58a534a50b"
        },
        {
            "id": "76dbc607-04f4-4674-bddd-d334aa80fed4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "990f62cf-e6cf-4255-8ff4-9a58a534a50b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "3fb08818-360a-41ea-8b0b-cdb59be60496",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 22
        },
        {
            "id": "bab2505b-e181-4162-8eee-719c10caa1eb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 116,
            "y": 22
        },
        {
            "id": "cc57f0b3-9ae3-47f7-aaca-b3c49f1fad4d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 116,
            "y": 126
        },
        {
            "id": "4ee68c0d-5776-4595-ad8b-f4e1d1f27859",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 126
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "fcbd8c5a-eeef-4eed-99f1-eaa1601d6eab",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2000",
            "varName": "damage",
            "varType": 1
        }
    ],
    "solid": true,
    "spriteId": "eca525c7-f337-4333-9c23-8bfc38e407ec",
    "visible": true
}