var barile = instance_place(x,y,obj_barile);
if(barile != noone)
{
	with(barile)
	{
		instance_create_layer(x,y,layer,obj_esplosione_barile);
		instance_destroy();
	}
}