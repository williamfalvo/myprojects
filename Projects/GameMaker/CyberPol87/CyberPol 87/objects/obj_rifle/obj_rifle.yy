{
    "id": "517a23f8-0389-4e90-ac0c-71788480b217",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rifle",
    "eventList": [
        {
            "id": "aa8f6cb1-ac93-4dc6-9814-be4c9e53023f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "517a23f8-0389-4e90-ac0c-71788480b217"
        },
        {
            "id": "03278e21-0447-45de-8dfc-6a8458bcecca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a56b2e07-b084-46bf-a767-0c9745022d11",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "517a23f8-0389-4e90-ac0c-71788480b217"
        },
        {
            "id": "5700964c-77e5-493e-a13c-2bac82c3d341",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "517a23f8-0389-4e90-ac0c-71788480b217"
        },
        {
            "id": "f9439e6f-2941-41b9-b798-415b95007a1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "517a23f8-0389-4e90-ac0c-71788480b217"
        },
        {
            "id": "947a7b3e-595e-4c65-8ff3-1b11b25ecbae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "37f5c343-00e9-4fb4-8826-ecb63f9f1352",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "517a23f8-0389-4e90-ac0c-71788480b217"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.1,
    "physicsFriction": 0.2,
    "physicsGroup": 3,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "e87ff07f-84f6-4862-8fbe-98d7ab1e3259",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 24
        },
        {
            "id": "1856309f-0179-4edc-a61d-bf60706698f4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 61,
            "y": 24
        },
        {
            "id": "5c9ef89e-51f3-439b-94fc-4b11cfbe79f8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 61,
            "y": 49
        },
        {
            "id": "ceb620c9-bb58-4d9b-87e7-5b84da35d59c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 49
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "2e39bb94-b511-4528-95e9-3927e88021b0",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "7",
            "varName": "cooldown_rifle",
            "varType": 1
        },
        {
            "id": "f87488c7-b284-4c8d-8bca-ba203ec7bd5e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "150",
            "varName": "munitions",
            "varType": 1
        },
        {
            "id": "2ef46c35-fbff-40b3-9c50-c2552d3fb66a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "onPlayer",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "3932fdb7-0bb5-4712-a474-a4d89354e714",
    "visible": true
}