if(place_meeting(x,y,obj_player_commands))
{
	onPlayer = true;
	if ( obj_player_commands.Pick )
	{
		audio_play_sound(snd_pick_weapon,0,false);
		obj_player.cooldown_shoot = cooldown_rifle;
		obj_player.bullet = obj_rifle_bullet;
		obj_player.arma = obj_rifle;
		obj_player.munitions = munitions;
		obj_player.max_munitions = munitions;
		obj_player.walk_right = spr_walk_right_rifle;
		obj_player.walk_rightUp = spr_walk_rightUp_rifle;
		obj_player.walk_rightDown = spr_walk_rightDown_rifle;
		obj_player.idle_right = spr_idle_right_rifle;
		obj_player.jump_right = spr_jump_right_rifle;
		obj_player.jump_rightUp = spr_jump_rightUp_rifle;
		obj_player.jump_rightDown = spr_jump_rightDown_rifle;
		obj_player.jump_up = spr_jump_up_rifle;
		obj_player.jump_down = spr_jump_down_rifle;
		obj_player.gunpoint_right = spr_gunpoint_right_rifle;
		obj_player.gunpoint_rightUp = spr_gunpoint_rightUp_rifle;
		obj_player.gunpoint_rightDown = spr_gunpoint_rightDown_rifle;
		obj_player.gunpoint_up = spr_gunpoint_up_rifle;
		obj_player.gunpoint_down = spr_gunpoint_down_rifle;
		obj_UI.weapon = spr_rifle;
		instance_destroy();
	}		
}
else
{
	onPlayer = false;
}