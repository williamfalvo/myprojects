{
    "id": "bf2647b9-5324-4bc2-adc5-df8392ac4216",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_Lav_Discesa",
    "eventList": [
        {
            "id": "fc980141-c8ff-4b7f-a914-b83e9bf95ba5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bf2647b9-5324-4bc2-adc5-df8392ac4216"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3fdd4791-4226-4130-82fa-bec716589a01",
    "visible": true
}