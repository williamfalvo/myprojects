
Right = keyboard_check(vk_right) || (gamepad_axis_value(0,gp_axislh)>0.3) || gamepad_button_check(0,gp_padr);
Left = keyboard_check(vk_left) || (gamepad_axis_value(0,gp_axislh)<-0.3) || gamepad_button_check(0,gp_padl);
Jump = keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(0,gp_face1);
Down = keyboard_check(vk_down) || (gamepad_axis_value(0,gp_axislv)>0.3) || gamepad_button_check(0,gp_padd);
Shoot = keyboard_check(ord("S")) || gamepad_button_check(0,gp_face3);
Up = keyboard_check(vk_up) || (gamepad_axis_value(0,gp_axislv)<-0.3) || gamepad_button_check(0,gp_padu);
Shield = keyboard_check(ord("D")) || gamepad_button_check(0,gp_shoulderrb);
Lock = keyboard_check(ord("A")) || gamepad_button_check(0,gp_shoulderlb);
Pick = keyboard_check_pressed(ord("E")) || gamepad_button_check(0,gp_face2);
phy_fixed_rotation = true;
