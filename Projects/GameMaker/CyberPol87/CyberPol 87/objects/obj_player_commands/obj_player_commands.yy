{
    "id": "391591f8-290b-4bb3-96b9-c4eb515f315c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_commands",
    "eventList": [
        {
            "id": "32996baa-935a-4ebc-bc64-f1dba2ef538b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "391591f8-290b-4bb3-96b9-c4eb515f315c"
        },
        {
            "id": "4d458f3c-d90d-43a5-85bc-db21569ae21b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "391591f8-290b-4bb3-96b9-c4eb515f315c"
        },
        {
            "id": "9267f9e1-e427-4033-ac94-38d33a21f16f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "391591f8-290b-4bb3-96b9-c4eb515f315c"
        },
        {
            "id": "3c1f50c0-a79a-49e3-8de1-d0abc404f506",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "391591f8-290b-4bb3-96b9-c4eb515f315c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0,
    "physicsObject": true,
    "physicsRestitution": 0,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "78bd8dc4-16a5-468a-a671-5821739b9bcf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "e53ad57e-26da-49de-8cf6-6311f1197af1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "e9e610cb-9fcf-4c51-9bc5-5b784b365efb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "8b7a6ebd-2eef-41a6-af7c-72011a9dcf59",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "09eebe9f-1a8a-40ec-ae44-4197e4bb8b9b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "noone",
            "varName": "nextState",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}