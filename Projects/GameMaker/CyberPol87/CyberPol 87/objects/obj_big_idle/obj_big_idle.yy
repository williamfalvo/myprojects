{
    "id": "0e16e567-d682-4e5a-9c7a-cbf164285e7a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_big_idle",
    "eventList": [
        {
            "id": "94df74a7-64a3-4bfd-bc01-59c7c3b24b61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0e16e567-d682-4e5a-9c7a-cbf164285e7a"
        },
        {
            "id": "cd6956c6-5033-452e-86f0-09abef12ac4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e16e567-d682-4e5a-9c7a-cbf164285e7a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b691593-2856-4dcd-8481-d1e5806bb6d7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "99ed933c-0deb-4ba9-8ace-1012266618cc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 17,
            "y": 1
        },
        {
            "id": "e697bf1f-3c06-4b6f-b292-2ca7160355a4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 1
        },
        {
            "id": "d59609ae-68f5-4971-abdf-56eef29028e1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 64
        },
        {
            "id": "7e9274d9-71e8-4079-ac8e-fe71b4e2f2c1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 17,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "64530070-bfd4-444f-b705-03303f9fce85",
    "visible": true
}