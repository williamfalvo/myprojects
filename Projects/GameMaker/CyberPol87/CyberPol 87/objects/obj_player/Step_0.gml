//Se le munizioni dell'arma arrivano a 0 equipaggio la pistola base

if(bullet != obj_bullet)
{
	if(munitions == 0)
	{
		cooldown_shoot = cooldown_gun;
		max_munitions = 0;
		bullet = obj_bullet;
		walk_right = spr_walk_right;
		walk_rightUp = spr_walk_rightUp;
		walk_rightDown = spr_walk_rightDown;
		idle_right = spr_idle_right;
		jump_right = spr_jump_right;
		jump_rightUp = spr_jump_rightUp;
		jump_rightDown = spr_jump_rightDown;
		jump_up = spr_jump_up;
		jump_down = spr_jump_down;
		gunpoint_right = spr_gunpoint_right;
		gunpoint_rightUp = spr_gunpoint_rightUp;
		gunpoint_rightDown = spr_gunpoint_rightDown;
		gunpoint_up = spr_gunpoint_up;
		gunpoint_down = spr_gunpoint_down;
		obj_UI.weapon = spr_gun;
	}
}

//Controllo il cooldown dell'azione di sparo
if(Shooting)
{
	time_shoot ++;
	if(time_shoot >= cooldown_shoot)
	{
		Shooting = false;
		time_shoot = 0;
	}
}

//Controllo se ha lo shield carico per attivarlo
if(isDefence && shield_durability > 0)
{
	shield_durability --;
	if(shield_durability <= 0)
	{
		audio_play_sound(snd_shield_broke,0,false);
	}
}

//Controllo se la vita del player arriva a 0
if(hp <= 0)
{
	with(current_state)
	{
		scr_player_set_state(obj_player_death);
	}
}