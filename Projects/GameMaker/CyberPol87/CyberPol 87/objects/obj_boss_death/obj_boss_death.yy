{
    "id": "f85c3fa7-e71e-488c-8507-c30ef60429fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss_death",
    "eventList": [
        {
            "id": "50e88be6-8a4b-4be7-a3bf-e0e5581b8364",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f85c3fa7-e71e-488c-8507-c30ef60429fa"
        },
        {
            "id": "68d1aa98-6161-4289-8711-8bb159d5aac3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f85c3fa7-e71e-488c-8507-c30ef60429fa"
        },
        {
            "id": "b0719b16-e725-4206-99d0-94a439106232",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f85c3fa7-e71e-488c-8507-c30ef60429fa"
        },
        {
            "id": "0d95b680-6bb1-4426-be87-c14f759ed315",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f85c3fa7-e71e-488c-8507-c30ef60429fa"
        },
        {
            "id": "65d4e4d1-24bf-4a55-9d43-1f3d40bff03b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "f85c3fa7-e71e-488c-8507-c30ef60429fa"
        },
        {
            "id": "5fef012d-eabb-4809-89ea-c2d88f0172d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "f85c3fa7-e71e-488c-8507-c30ef60429fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6a927b9c-72c6-46e3-b592-77de612c6648",
    "visible": true
}