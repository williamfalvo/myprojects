if(obj_gameManager.gameIsPaused) exit;


phy_position_x += lengthdir_x(bullet_speed,direction);
phy_position_y += lengthdir_y(bullet_speed,direction);

if(place_meeting(x,y,obj_player_commands))
{
	if(obj_player.hp - damage < 0)
	{
		obj_player.hp = 0;
	}
	else
	{
		obj_player.hp -= damage;
	}
	instance_destroy();
}

if(place_meeting(x,y,obj_floor))
{
	instance_destroy();
}