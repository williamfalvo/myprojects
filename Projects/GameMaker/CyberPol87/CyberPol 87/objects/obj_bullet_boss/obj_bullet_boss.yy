{
    "id": "8fdc1b44-a10b-4c57-9601-7425d2693fe7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_boss",
    "eventList": [
        {
            "id": "47eef508-10e1-4868-857e-f5b18397d10a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8fdc1b44-a10b-4c57-9601-7425d2693fe7"
        },
        {
            "id": "ffdda97c-37a5-4dfe-acda-67d44d2dee7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8fdc1b44-a10b-4c57-9601-7425d2693fe7"
        },
        {
            "id": "d1056a74-9544-419f-b101-b4964ca620a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "8fdc1b44-a10b-4c57-9601-7425d2693fe7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "2dfb9fd4-a7bd-4aa4-99cb-d0c7fe6cd875",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "0317d83c-d448-483f-843c-accc132ab067",
            "propertyId": "8da72806-ea26-4183-bab0-259f968e8bbb",
            "value": "3"
        }
    ],
    "parentObjectId": "0317d83c-d448-483f-843c-accc132ab067",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 6,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "2524061c-e883-481c-a01b-231ede9f61bf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 5
        },
        {
            "id": "3451c8c3-6af1-4443-b921-2d527ebfeaf4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 5
        },
        {
            "id": "b383000d-77b8-4a69-9da8-0b2332097b6e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 11
        },
        {
            "id": "69af83f6-e6ad-40e5-9dbf-de24efe0f787",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 11
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "5db04a79-4040-47de-88a5-7d270fc3e81f",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "3",
            "varName": "bullet_speed",
            "varType": 0
        },
        {
            "id": "a813d075-1f3b-4038-b70f-c2db70e791c5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "damage",
            "varType": 0
        }
    ],
    "solid": true,
    "spriteId": "c3982258-691d-4829-888e-359f1a934931",
    "visible": true
}