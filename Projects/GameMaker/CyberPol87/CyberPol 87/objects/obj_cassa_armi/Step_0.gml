var bullet = instance_place(x,y,obj_bullet)
if(bullet != noone)
{
	if(bullet != obj_shotgun_bullet) 
	{
		audio_play_sound(snd_cassa,0,false);
		instance_destroy(bullet);
	}
	image_speed = 1.5;
}	