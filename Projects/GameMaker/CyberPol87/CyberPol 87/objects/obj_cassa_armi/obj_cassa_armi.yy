{
    "id": "7045d4e8-7ab7-4e17-9420-21300d622866",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cassa_armi",
    "eventList": [
        {
            "id": "e76a3cb3-d043-4353-b762-4837b36e8ca4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7045d4e8-7ab7-4e17-9420-21300d622866"
        },
        {
            "id": "7d10a697-267e-4e49-9cd0-0e14ab20b4ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7045d4e8-7ab7-4e17-9420-21300d622866"
        },
        {
            "id": "8657e63e-f050-4af5-b662-392c38bb9b7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "7045d4e8-7ab7-4e17-9420-21300d622866"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 3,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": true,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "c3aeb25b-8a5a-4605-bd0e-92a9634adcbf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 5,
            "y": 11
        },
        {
            "id": "2db61ff5-9ca1-4aea-8743-8b9f189e54b5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 60,
            "y": 11
        },
        {
            "id": "cc8ce5cc-55c1-4f2b-81bf-00c9da3bd738",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 60,
            "y": 64
        },
        {
            "id": "562909b3-ab0a-47af-8346-60dbc9eacbf8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 5,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "40eee4d4-210c-4137-b44d-050dc805d333",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "hp",
            "varType": 0
        },
        {
            "id": "0231b71b-0ce5-45bc-ad94-b687ca6b0004",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 256,
            "value": "obj_rifle",
            "varName": "arma_inside",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "67ec747d-e116-45c7-b4b7-9a4551df6b56",
    "visible": true
}