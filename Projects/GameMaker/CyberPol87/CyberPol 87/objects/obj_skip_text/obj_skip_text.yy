{
    "id": "e10e00fd-4975-4b07-9314-9acbf4436887",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_skip_text",
    "eventList": [
        {
            "id": "e8ec4e7f-e662-4caf-a1bf-0fb088cdcc15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e10e00fd-4975-4b07-9314-9acbf4436887"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1f75eea7-8f8a-4e0d-97cf-410350372a9e",
    "visible": true
}