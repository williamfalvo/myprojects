{
    "id": "07c1b5e0-e257-4d20-8a5f-4c3d7c99383f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_Lav_Casse",
    "eventList": [
        {
            "id": "97007b8a-a4c1-4b99-8cfe-d3b0d781a53f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "07c1b5e0-e257-4d20-8a5f-4c3d7c99383f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ccb5b25d-c867-4c20-9cf8-e589dc754838",
    "visible": true
}