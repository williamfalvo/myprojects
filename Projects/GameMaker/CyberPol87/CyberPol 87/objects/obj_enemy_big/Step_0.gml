if(hp <= 0)
{
	with(current_state)
	{
		scr_player_set_state(obj_big_dead);
	}
}

if(x <= obj_camera_wall.x)
{
	instance_destroy(current_state);
	instance_destroy();
}