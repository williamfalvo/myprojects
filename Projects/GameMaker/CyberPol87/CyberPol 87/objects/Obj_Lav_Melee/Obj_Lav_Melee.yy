{
    "id": "14a266ff-757d-41cb-a095-b91b0335e4e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_Lav_Melee",
    "eventList": [
        {
            "id": "eb22f7a0-91b2-430a-8db9-455d32f71964",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "14a266ff-757d-41cb-a095-b91b0335e4e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "13e561fc-47aa-4fd4-9591-8c9a4b32139f",
    "visible": true
}