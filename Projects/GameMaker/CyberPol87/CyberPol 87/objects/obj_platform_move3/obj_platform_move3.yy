{
    "id": "09f90b5e-2728-4d1c-8ce6-6f163f66b2fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_platform_move3",
    "eventList": [
        {
            "id": "ea2aa739-757d-4cd2-9b1f-5d77d2ddfb81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "09f90b5e-2728-4d1c-8ce6-6f163f66b2fa"
        },
        {
            "id": "9843b775-19bf-4404-8468-5ec12c759846",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "391591f8-290b-4bb3-96b9-c4eb515f315c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "09f90b5e-2728-4d1c-8ce6-6f163f66b2fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a56b2e07-b084-46bf-a767-0c9745022d11",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "d7a99030-47fc-4ced-88e3-e4197b82cf46",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 5
        },
        {
            "id": "499173e4-526b-47b8-aa80-243e44f4af7f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 5
        },
        {
            "id": "175f515f-1069-42bf-934a-698964b1e033",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 12
        },
        {
            "id": "aa1d5327-b498-414b-8060-c730606eb166",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 12
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "aa9142e8-e8ce-43e8-a20b-c2f66cc7fe74",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "120",
            "varName": "change_time",
            "varType": 1
        },
        {
            "id": "16c4ada9-df56-4bb4-af59-639b7c53c30a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "time_move",
            "varType": 0
        },
        {
            "id": "476d1b51-cdfe-4f57-a8fc-5cf9c17ff63d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.5",
            "varName": "speed_x",
            "varType": 0
        },
        {
            "id": "7daa3ff5-7d88-4a43-adaa-50e7f7479173",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.5",
            "varName": "speed_y",
            "varType": 0
        },
        {
            "id": "a43f740b-271c-490b-a86d-4d8838a9b301",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "isMoving",
            "varType": 3
        },
        {
            "id": "c8e44e64-8980-408b-98f4-224ccbfa62b8",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "canChange",
            "varType": 3
        }
    ],
    "solid": true,
    "spriteId": "1483fc32-a1f8-4c6d-987d-9c969c152251",
    "visible": true
}