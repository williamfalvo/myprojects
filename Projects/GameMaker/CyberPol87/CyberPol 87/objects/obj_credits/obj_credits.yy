{
    "id": "91a040c0-a5f1-4eef-8c57-b82b341eb624",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_credits",
    "eventList": [
        {
            "id": "3669890c-5866-4053-94da-cb0230587225",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "91a040c0-a5f1-4eef-8c57-b82b341eb624"
        },
        {
            "id": "7ce0e1e5-8f47-4e48-989b-0dcdfaeff887",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "91a040c0-a5f1-4eef-8c57-b82b341eb624"
        },
        {
            "id": "2ebf7fb8-220e-4628-9f31-47183818b53a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "91a040c0-a5f1-4eef-8c57-b82b341eb624"
        },
        {
            "id": "96835419-5f4d-406b-b2c2-4f77672716e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "91a040c0-a5f1-4eef-8c57-b82b341eb624"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "63cdffc0-4035-43a6-a3af-1c0f9389c895",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "Destroy",
            "varType": 3
        },
        {
            "id": "2b614ad5-0be6-48cc-90ce-1d2dab396d9e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "lastCredits",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "f143a3cf-6ef4-439e-b38a-60d303a0c2e8",
    "visible": true
}