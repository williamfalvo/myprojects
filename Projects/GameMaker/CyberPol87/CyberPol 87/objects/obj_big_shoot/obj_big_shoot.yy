{
    "id": "edb29e69-c8be-4127-b27c-07cd3508efa3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_big_shoot",
    "eventList": [
        {
            "id": "f2a22a34-d550-443f-89ce-6ca1012078c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "edb29e69-c8be-4127-b27c-07cd3508efa3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b691593-2856-4dcd-8481-d1e5806bb6d7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "d933c64b-5961-4f4b-be70-98c3f5da1ab2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 18,
            "y": 2
        },
        {
            "id": "92b70bf2-d82e-46c7-9758-ac5f90ef05b5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 2
        },
        {
            "id": "f0f79df5-6cbf-4758-8a3d-a144cd619505",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 64
        },
        {
            "id": "f4a5f351-3246-42cd-a40c-b4c13eee00d3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 18,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "c0a4c2c3-51c1-4f4b-b49d-9d0ee08d25fd",
    "visible": true
}