if(obj_gameManager.gameIsPaused) exit;


//////////////////// Fix rotazione ////////////////////
event_inherited();

//////////////////// Quando nota il player rimane sempre girato verso di lui ////////////////////

if (obj_player_commands.x < x )
{
	image_xscale = -1
}
else
{
	image_xscale = 1
}

//////////////////// Shoot Direction ////////////////////
#macro lineLenght 600

if (!enemy_big.is_shooting)
{
	if ( collision_line(x, y, x- lineLenght, y + lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x-28, y+32,layer, obj_bullet_big);
		bullet.direction = 235
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_big_downr
		image_xscale = -1
		enemy_big.is_shooting = true
		enemy_big.time_shooting = 0
		
	}

	else if ( collision_line(x , y, x- lineLenght, y-lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x-27, y-28,layer, obj_bullet_big);
		bullet.direction = 125
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_big_upr
		image_xscale = -1
		enemy_big.is_shooting = true
		enemy_big.time_shooting = 0
		
	}

	else if ( collision_line(x , y, x-lineLenght, y, obj_player_commands,false,false) && collision_circle(x,y,enemy_big.player_distance_shoot,obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x-40, y-3,layer, obj_bullet_big);
		bullet.direction = 180;
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_big_right
		image_xscale = -1	
		enemy_big.is_shooting = true
		enemy_big.time_shooting = 0
		
	}

	else if ( collision_line(x, y, x+ lineLenght, y + lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x+28, y+31,layer, obj_bullet_big);
		bullet.direction = -45
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_big_downr
		image_xscale = 1
		enemy_big.is_shooting = true
		enemy_big.time_shooting = 0
		
	}

	else if ( collision_line(x , y, x+ lineLenght, y-lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x+27, y-28,layer, obj_bullet_big);
		bullet.direction = 45
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_big_upr
		image_xscale = 1
		enemy_big.is_shooting = true
		enemy_big.time_shooting = 0
		
	}

	else if ( collision_line(x , y, x+lineLenght, y, obj_player_commands,false,false) && collision_circle(x,y,enemy_big.player_distance_shoot,obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x+40, y-3,layer, obj_bullet_big);
		bullet.direction = 0;
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_big_right
		image_xscale = 1	
		enemy_big.is_shooting = true
		enemy_big.time_shooting = 0
		
	}
	
	else if ( collision_line(x, y, x, y-lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x+4, y-32,layer, obj_bullet_big);
		bullet.direction = 90;
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_big_up
		image_xscale = 1	
		enemy_big.is_shooting = true
		enemy_big.time_shooting = 0
		
	}
	
	else if ( collision_line(x, y, x, y+lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x-3, y+40,layer, obj_bullet_big);
		bullet.direction = 270;
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_big_down
		image_xscale = 1	
		enemy_big.is_shooting = true
		enemy_big.time_shooting = 0
		
	}
	else
	{
		scr_player_set_state(obj_big_walk)
	}
}
else
{
	enemy_big.time_shooting ++
}

if (enemy_big.time_shooting > enemy_big.cooldown_max)
{
	enemy_big.is_shooting = false
}

