{
    "id": "4a4a8c00-a1fb-4d4e-bc00-813b37e1619c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_Lav_MirStab",
    "eventList": [
        {
            "id": "d9d214cf-24ec-4a68-b986-d353b4f78c79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4a4a8c00-a1fb-4d4e-bc00-813b37e1619c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7a7b2343-1ef3-40ea-ac00-06c837695607",
    "visible": true
}