{
    "id": "4094d817-8bf2-43cf-ae81-ab881f857ac1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_big_boss",
    "eventList": [
        {
            "id": "e01fe1c2-7e40-43b9-bce7-add9d93cecc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4094d817-8bf2-43cf-ae81-ab881f857ac1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "61334fb8-45c2-4c7c-a01b-45f60fdc7cd0",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "0317d83c-d448-483f-843c-accc132ab067",
            "propertyId": "8da72806-ea26-4183-bab0-259f968e8bbb",
            "value": "2"
        }
    ],
    "parentObjectId": "0317d83c-d448-483f-843c-accc132ab067",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 6,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "4389062f-2127-432d-ad5b-f9de444fe667",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "5e68cb5b-0c24-492e-a987-a7b4f9c92ad2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "bc9cf16f-8697-4a0c-a372-7e7b0a565cbf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 16
        },
        {
            "id": "93a4a80c-e476-4275-b8ec-442ea93aa0eb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "d41fe891-edb6-49fb-85fc-9db29c26e1dc",
    "visible": true
}