{
    "id": "76023d9c-0d77-4a52-a66e-4a48f4139f4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shotgun_bullet",
    "eventList": [
        {
            "id": "def3307a-89ff-471d-9533-8775f2e315cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "76023d9c-0d77-4a52-a66e-4a48f4139f4f"
        },
        {
            "id": "b81a4f18-67d8-45e7-a363-0e4cbdc723c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "76023d9c-0d77-4a52-a66e-4a48f4139f4f"
        },
        {
            "id": "de88935c-ac3c-437b-8d3b-ed348e70d1fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "76023d9c-0d77-4a52-a66e-4a48f4139f4f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "2ae4a744-c6d8-48c8-84c0-16813e81bf8f",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "999d01e9-da97-4053-9028-d62968d66410",
            "propertyId": "4851cca6-5c85-4207-9fc0-e1377ed20fef",
            "value": "0"
        },
        {
            "id": "a180dc95-6659-4e7b-922c-ddd44761b813",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "999d01e9-da97-4053-9028-d62968d66410",
            "propertyId": "7f5c0621-32fb-4208-b3dc-5a8ddc5153f5",
            "value": "30"
        }
    ],
    "parentObjectId": "999d01e9-da97-4053-9028-d62968d66410",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 5,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "793a7c1e-acdb-4406-a6fe-093edb8b82e1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        },
        {
            "id": "356bd344-f8b8-458a-a89e-d6f35ffe66dc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 123,
            "y": 16
        },
        {
            "id": "2a125b6b-cc88-431d-acca-0c48652164c0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 123,
            "y": 48
        },
        {
            "id": "f6744870-b5be-4e10-b148-630c00fa0abf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 48
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": true,
    "spriteId": "c98f6357-5533-4390-8b6c-8bd9598779bf",
    "visible": true
}