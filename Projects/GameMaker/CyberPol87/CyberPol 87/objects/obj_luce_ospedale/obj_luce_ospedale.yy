{
    "id": "630b9857-5c7a-401b-8d8d-268accd5f631",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_luce_ospedale",
    "eventList": [
        {
            "id": "c3187447-997f-4e2b-b5f6-24040793d11e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "630b9857-5c7a-401b-8d8d-268accd5f631"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3f686063-3486-4f98-9de7-26b4a927122d",
    "visible": true
}