{
    "id": "d782911a-0cc2-4c2a-8a85-276bc60627ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_fat",
    "eventList": [
        {
            "id": "0f483244-8d38-4697-867d-97fc05c0e30f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d782911a-0cc2-4c2a-8a85-276bc60627ba"
        },
        {
            "id": "7bfe1814-cc50-4d1a-914e-eb73a35502d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d782911a-0cc2-4c2a-8a85-276bc60627ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        
    ],
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "565493ea-2556-46b9-a8cd-950ea87f71a1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "754829c5-c2ae-4243-a74c-0788e2e2adcc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "80c4e48c-6b84-4ed0-8123-e5b020828ce9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "5fa6c1b5-6915-4200-9098-2b35631e26ab",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "2c22d3df-b0b7-42b3-a655-1f56fe012d5b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "400",
            "varName": "player_distance",
            "varType": 0
        },
        {
            "id": "e4b678e1-12d1-47ee-ba4f-3e7e6f3ee501",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "250",
            "varName": "player_distance_shoot",
            "varType": 0
        },
        {
            "id": "f034f417-e5be-49af-94a4-ddd58c2a57ec",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "enemy_speed",
            "varType": 0
        },
        {
            "id": "3d46945a-b7e6-4f75-937c-a7d91b31dd93",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "is_shooting",
            "varType": 3
        },
        {
            "id": "332d56c9-97bc-44fd-9773-8c5322a92316",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "60",
            "varName": "cooldown_max",
            "varType": 0
        },
        {
            "id": "313c4a5f-df05-4439-bfcd-83517e0626a5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "time_shooting",
            "varType": 0
        },
        {
            "id": "6c5a1168-1c58-400f-8675-ac5c02a5160e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "20",
            "varName": "hp",
            "varType": 0
        },
        {
            "id": "4bd8f925-e507-4aad-b0ab-af53c68090df",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "is_explode",
            "varType": 3
        }
    ],
    "solid": true,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}