{
    "id": "9db07cad-378d-46e7-b8ef-a40e8eb2a04d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_wait",
    "eventList": [
        {
            "id": "667a2b8c-1d73-49da-844c-299ea1ecba30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9db07cad-378d-46e7-b8ef-a40e8eb2a04d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f3b9e6ec-dab0-4e96-8701-2c5970236dca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "a145df16-286d-48a3-8573-3620515954d9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 21,
            "y": 6
        },
        {
            "id": "dbae2623-c4a7-47ac-9284-3fae7c9bb4c9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 34,
            "y": 6
        },
        {
            "id": "438f1db8-96c8-4fdc-b990-1b24437c1a03",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 34,
            "y": 64
        },
        {
            "id": "f7fedeeb-2a46-4e65-befb-d2931977c6ae",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 21,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "e7e62ced-d5d5-47e0-8102-323241ef8b6e",
    "visible": true
}