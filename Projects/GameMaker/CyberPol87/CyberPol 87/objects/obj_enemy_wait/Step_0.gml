if(obj_gameManager.gameIsPaused) exit;


//////////////////// Fix rotazione ////////////////////
event_inherited();

//////////////////// Controlli direzionali per entrare nello stato Shoot ////////////////////
#macro lineLenght 600

if ( collision_line(x, y, x- lineLenght, y + lineLenght, obj_player_commands,false,false))
{
	scr_player_set_state(obj_enemy_shoot)
}

else if ( collision_line(x , y, x- lineLenght, y-lineLenght, obj_player_commands,false,false))
{
	scr_player_set_state(obj_enemy_shoot)
}

else if ( collision_line(x , y, x-lineLenght, y, obj_player_commands,false,false) && collision_circle(x,y,enemy_small.player_distance_shoot,obj_player_commands,false,false))
{
	scr_player_set_state(obj_enemy_shoot)
}

else if ( collision_line(x, y, x+ lineLenght, y + lineLenght, obj_player_commands,false,false))
{
	scr_player_set_state(obj_enemy_shoot)
}

else if ( collision_line(x , y, x+ lineLenght, y-lineLenght, obj_player_commands,false,false))
{
	scr_player_set_state(obj_enemy_shoot)
}

else if ( collision_line(x , y, x+lineLenght, y, obj_player_commands,false,false) && collision_circle(x,y,enemy_small.player_distance_shoot,obj_player_commands,false,false))
{
	scr_player_set_state(obj_enemy_shoot)
}

else if ( collision_line(x, y, x, y-lineLenght, obj_player_commands,false,false))
{
	scr_player_set_state(obj_enemy_shoot)
}

else if ( collision_line(x, y, x, y+lineLenght, obj_player_commands,false,false))
{
	scr_player_set_state(obj_enemy_shoot)
}

