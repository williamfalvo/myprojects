{
    "id": "beba1592-d845-4edd-84c1-81611c97009c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shotgun",
    "eventList": [
        {
            "id": "1e430633-999b-4f6c-8cf5-93fc7fa17dd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "beba1592-d845-4edd-84c1-81611c97009c"
        },
        {
            "id": "e6259b1e-6d2f-4865-a443-44a9bfcde38d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "beba1592-d845-4edd-84c1-81611c97009c"
        },
        {
            "id": "bd40d3fc-3014-4a79-a9ba-2b6f1943e366",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a56b2e07-b084-46bf-a767-0c9745022d11",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "beba1592-d845-4edd-84c1-81611c97009c"
        },
        {
            "id": "138184e6-0fed-479b-955f-9971ac569b60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "beba1592-d845-4edd-84c1-81611c97009c"
        },
        {
            "id": "7d682fdb-8bb6-45e8-a267-f31f05534229",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "37f5c343-00e9-4fb4-8826-ecb63f9f1352",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "beba1592-d845-4edd-84c1-81611c97009c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.1,
    "physicsFriction": 0.2,
    "physicsGroup": 3,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "9641b53e-1664-47a5-8df1-aef9b9746c6d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 23
        },
        {
            "id": "214c746b-7a52-448d-a658-4773a88f7183",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 23
        },
        {
            "id": "01d3abc4-b821-409b-bcb5-75a7deee96dd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 38
        },
        {
            "id": "d858d92d-e29a-4ee7-ab4a-2ed9ed78da0c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 38
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "5c894d18-7854-417e-bdb4-992e7aa6d592",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "30",
            "varName": "cooldown_shotgun",
            "varType": 1
        },
        {
            "id": "efa955af-1e3e-4122-8327-8c7b1c9d4c75",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "20",
            "varName": "munitions",
            "varType": 1
        },
        {
            "id": "ca57eba1-bd6b-4e73-997f-eeac5b742742",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "20",
            "varName": "max_munitions",
            "varType": 1
        },
        {
            "id": "8f5d5bcb-b0d9-488a-b746-62190490ce7d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "onPlayer",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "cc8f1ae0-70c3-4de6-a229-18c7d59716ce",
    "visible": true
}