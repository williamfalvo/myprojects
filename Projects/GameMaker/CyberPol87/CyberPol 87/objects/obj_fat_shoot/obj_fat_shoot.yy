{
    "id": "b605a8fe-9f0f-46b1-84af-51d9fed1cc4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fat_shoot",
    "eventList": [
        {
            "id": "1b6698cc-15ff-4653-9860-09af5894d205",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b605a8fe-9f0f-46b1-84af-51d9fed1cc4f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d69f68e-96fe-4108-938b-6d282eda61bc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "67380d2a-bf7c-4773-bb11-8b9aff729d89",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 18,
            "y": 6
        },
        {
            "id": "672b87ea-5ede-4ba4-b5c5-bf4d80c359df",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 6
        },
        {
            "id": "49a9b0de-01a5-4286-8ced-2adbeb1daf45",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 64
        },
        {
            "id": "421e37ef-6007-4390-a23f-8fee122c3c80",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 18,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "661eae07-fba8-472a-bce9-44987a7ba0fa",
    "visible": true
}