if(obj_gameManager.gameIsPaused) exit;


//////////////////// Fix rotazione ////////////////////
event_inherited();

//////////////////// Quando nota il player rimane sempre girato verso di lui ////////////////////

if (obj_player_commands.x < x )
{
	image_xscale = -1
}
else
{
	image_xscale = 1
}

//////////////////// Shoot Direction ////////////////////
#macro lineLenght 600

if (!enemy_fat.is_shooting)
{
	if ( collision_line(x, y, x- lineLenght, y + lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x-5, y+25,layer, obj_bullet_fat);
		bullet.direction = 235
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_fat_downr
		image_xscale = -1
		enemy_fat.is_shooting = true
		enemy_fat.time_shooting = 0
		
	}

	else if ( collision_line(x , y, x- lineLenght, y-lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x-17, y-23,layer, obj_bullet_fat);
		bullet.direction = 125
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_fat_upr
		image_xscale = -1
		enemy_fat.is_shooting = true
		enemy_fat.time_shooting = 0
		
	}

	else if ( collision_line(x , y, x-lineLenght, y, obj_player_commands,false,false) && collision_circle(x,y,enemy_fat.player_distance_shoot,obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x-20, y,layer, obj_bullet_fat);
		bullet.direction = 180;
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_fat_right
		image_xscale = -1	
		enemy_fat.is_shooting = true
		enemy_fat.time_shooting = 0
		
	}

	else if ( collision_line(x, y, x+ lineLenght, y + lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x+5, y+25,layer, obj_bullet_fat);
		bullet.direction = -45
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_fat_downr
		image_xscale = 1
		enemy_fat.is_shooting = true
		enemy_fat.time_shooting = 0
		
	}

	else if ( collision_line(x , y, x+ lineLenght, y-lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x+17, y-23,layer, obj_bullet_fat);
		bullet.direction = 45
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_fat_upr
		image_xscale = 1
		enemy_fat.is_shooting = true
		enemy_fat.time_shooting = 0
		
	}

	else if ( collision_line(x , y, x+lineLenght, y, obj_player_commands,false,false) && collision_circle(x,y,enemy_fat.player_distance_shoot,obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x+20, y,layer, obj_bullet_fat);
		bullet.direction = 0;
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_fat_right
		image_xscale = 1	
		enemy_fat.is_shooting = true
		enemy_fat.time_shooting = 0
		
	}
	
	else if ( collision_line(x, y, x, y-lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x, y-30,layer, obj_bullet_fat);
		bullet.direction = 90;
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_fat_up
		image_xscale = 1	
		enemy_fat.is_shooting = true
		enemy_fat.time_shooting = 0
		
	}
	
	else if ( collision_line(x, y, x, y+lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x+(-10*image_xscale), y+33, layer, obj_bullet_fat);
		bullet.direction = 270;
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_fat_down
		image_xscale = 1	
		enemy_fat.is_shooting = true
		enemy_fat.time_shooting = 0
		
	}
	else
	{
		scr_player_set_state(obj_fat_walk)
	}
}
else
{
	enemy_fat.time_shooting ++
}

if (enemy_fat.time_shooting > enemy_fat.cooldown_max)
{
	enemy_fat.is_shooting = false
}

