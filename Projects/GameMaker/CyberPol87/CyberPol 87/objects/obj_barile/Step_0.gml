var bullet = instance_place(x,y,obj_bullet);
if(bullet != noone)
{
	if(obj_player.bullet != obj_shotgun_bullet) 
	{
		instance_destroy(bullet);
	}
	instance_create_layer(x,y,layer,obj_esplosione_barile);
	audio_play_sound(snd_explosion,0,false);
	instance_destroy();
}