{
    "id": "07a88212-5c6c-4ce5-b56b-f830ccc36f2f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_barile",
    "eventList": [
        {
            "id": "3f47244a-cc0a-4473-a687-4d49c37d1a2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a56b2e07-b084-46bf-a767-0c9745022d11",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "07a88212-5c6c-4ce5-b56b-f830ccc36f2f"
        },
        {
            "id": "53becff5-b1de-4740-90b8-18f09ce0d2bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "07a88212-5c6c-4ce5-b56b-f830ccc36f2f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.1,
    "physicsFriction": 0.2,
    "physicsGroup": 3,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "17b24436-d93f-4265-82ce-606d27e2bb2f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "4292e8dc-f884-4621-b9b7-594097341fc5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "0018bac9-3182-4299-b12c-519ce6a9968f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "0273239a-a5ac-4646-97e7-69378722b977",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "5dfde7b7-19a7-4331-aaf3-d23a17730b23",
    "visible": true
}