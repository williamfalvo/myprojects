{
    "id": "b7a14318-c1b5-4065-977a-850e7a756412",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_floor",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a56b2e07-b084-46bf-a767-0c9745022d11",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "c9fb074b-1f1b-422a-94ba-4e8f5409f9f0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "4e944037-8c69-4789-819d-4399fdf9b9ec",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "5f8010ab-c039-42c2-8ae1-b018ba286837",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "f9b0edbf-ac6c-48ee-b11f-1dda4911d8ce",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "4878a5a0-367e-4cb6-accf-e12d83194d6d",
    "visible": false
}