{
    "id": "fe23c289-4692-45ee-b298-180e4e44aab4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_walk",
    "eventList": [
        {
            "id": "fc79fa42-ac97-4c80-bb82-c673e53a6006",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fe23c289-4692-45ee-b298-180e4e44aab4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f3b9e6ec-dab0-4e96-8701-2c5970236dca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "9f0e43be-3f79-4df2-b2f0-7dd79bfc48c4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 21,
            "y": 6
        },
        {
            "id": "542f6237-c6bd-4209-916e-bbb71541d61c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 34,
            "y": 6
        },
        {
            "id": "06b43d82-3001-459f-b530-2c349c5d58c8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 34,
            "y": 64
        },
        {
            "id": "1ca55bd5-1a23-40bb-820b-09d601a173b2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 21,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "cc29a2b9-57e7-44cb-9315-9b420826cf90",
    "visible": true
}