if(isMoving)
{
	phy_speed_x = speed_x;
	phy_speed_y = speed_y;
	
	time_move++;
	
	if(time_move >= change_time)
	{
		speed_x *= -1;
		speed_y *= -1;
		time_move = 0;
	}
}
else
{
	phy_speed_x = 0;
	phy_speed_y = 0;
}

