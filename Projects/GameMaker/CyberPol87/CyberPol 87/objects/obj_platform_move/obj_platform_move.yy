{
    "id": "6486fd36-be68-4a2c-893c-dc5086527676",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_platform_move",
    "eventList": [
        {
            "id": "79f0580c-64c2-4c56-a6bc-e64e9c6af56d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6486fd36-be68-4a2c-893c-dc5086527676"
        },
        {
            "id": "b4283ce7-a170-4949-88fe-b247208b6c3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "391591f8-290b-4bb3-96b9-c4eb515f315c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6486fd36-be68-4a2c-893c-dc5086527676"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a56b2e07-b084-46bf-a767-0c9745022d11",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "3b5ec70d-1ea6-4942-a883-502ecb85ac70",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "4bb9f43b-66fe-4e6a-8e5e-6722f8a7849b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "eae3b588-ec4e-4cfc-bee2-cfa069e159a4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 14
        },
        {
            "id": "1c525acc-65a4-4e40-94a4-9e35f674a0aa",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 14
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "8f1ddd05-4b3b-4b68-b0f5-53dd282edbe9",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "120",
            "varName": "change_time",
            "varType": 1
        },
        {
            "id": "651f1f0d-662c-4202-8f55-f27a9842fc1e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "time_move",
            "varType": 0
        },
        {
            "id": "dd16d107-1936-44cc-bbf6-5b613a40e951",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.5",
            "varName": "speed_x",
            "varType": 0
        },
        {
            "id": "7d9791ba-72df-471b-a235-330cfe4d40a5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0.5",
            "varName": "speed_y",
            "varType": 0
        },
        {
            "id": "d2c5ef26-2dac-43c0-b82d-db5704f9866a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "isMoving",
            "varType": 3
        },
        {
            "id": "57d6d353-8ee8-4cca-8c63-a0edda17410b",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "canChange",
            "varType": 3
        }
    ],
    "solid": true,
    "spriteId": "92c8b584-7a1e-4c7a-b8b5-ba2857ca90f0",
    "visible": true
}