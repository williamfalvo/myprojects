{
    "id": "a2bbf293-6739-4962-969f-be24f9f72929",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rifle_bullet",
    "eventList": [
        {
            "id": "f901b03c-4322-442b-8230-a396f051c8d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a2bbf293-6739-4962-969f-be24f9f72929"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "df6cac97-e4bd-4fdd-97bd-281c6f855c57",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "999d01e9-da97-4053-9028-d62968d66410",
            "propertyId": "7f5c0621-32fb-4208-b3dc-5a8ddc5153f5",
            "value": "7"
        },
        {
            "id": "2d95844d-7178-4fb6-b035-1f7ee50c92e2",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "999d01e9-da97-4053-9028-d62968d66410",
            "propertyId": "4851cca6-5c85-4207-9fc0-e1377ed20fef",
            "value": "15"
        }
    ],
    "parentObjectId": "999d01e9-da97-4053-9028-d62968d66410",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 5,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "ffe134c0-06d3-4a9b-8867-e3c1e885aa18",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 6,
            "y": 4
        },
        {
            "id": "59b20daa-7d75-46f5-83ee-9052e2966cc6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 4
        },
        {
            "id": "db1cd9b4-5ff5-4ce0-9204-b2169dcc8702",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 12
        },
        {
            "id": "fd636213-2c2d-4e33-baa9-b50c72901a29",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 6,
            "y": 12
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": true,
    "spriteId": "801e7f83-0fad-405a-9663-88afe0f2a4da",
    "visible": true
}