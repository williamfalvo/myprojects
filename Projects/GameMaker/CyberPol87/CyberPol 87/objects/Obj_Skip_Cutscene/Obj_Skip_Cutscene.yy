{
    "id": "539b370d-9359-4667-bbcb-966d1e3d8d45",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_Skip_Cutscene",
    "eventList": [
        {
            "id": "348a4c83-fe51-4ea7-b347-96b150bdc1f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "539b370d-9359-4667-bbcb-966d1e3d8d45"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1f75eea7-8f8a-4e0d-97cf-410350372a9e",
    "visible": true
}