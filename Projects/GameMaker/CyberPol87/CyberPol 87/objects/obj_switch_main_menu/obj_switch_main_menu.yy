{
    "id": "08c9c4d4-4cfc-4019-93b6-fbbb67f5cc95",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_switch_main_menu",
    "eventList": [
        {
            "id": "68d4d5e4-5e9d-4610-b4ca-18d2e2b3d9fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "08c9c4d4-4cfc-4019-93b6-fbbb67f5cc95"
        },
        {
            "id": "27f4056e-bbc3-44e4-981c-118a35ed632e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "08c9c4d4-4cfc-4019-93b6-fbbb67f5cc95"
        },
        {
            "id": "344710cc-79c9-430a-a275-f56765585583",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "08c9c4d4-4cfc-4019-93b6-fbbb67f5cc95"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}