//menu_move = keyboard_check_pressed(vk_down) - keyboard_check_pressed(vk_up);
//menu_index += menu_move;
if (scr_check_menu_command(vk_down, gp_padd))
{
	menu_index += 1;
	if(menu_index == 0) {
		obj_STORIA.image_index = 1;
	}
	else {
		obj_STORIA.image_index = 0;
	}
	
	if(menu_index == 1) {
		obj_COMANDI.image_index = 1;
	}
	else {
		obj_COMANDI.image_index = 0;
	}
	
	if(menu_index == 2) {
		obj_ESC.image_index = 1;
	}
	else {
		obj_ESC.image_index = 0;
	}
}

if (scr_check_menu_command(vk_up, gp_padu))
{
	menu_index -= 1;
	if(menu_index == 0) {
		obj_STORIA.image_index = 1;
	}
	else {
		obj_STORIA.image_index = 0;
	}
	
	if(menu_index == 1) {
		obj_COMANDI.image_index = 1;
	}
	else {
		obj_COMANDI.image_index = 0;
	}
	
	if(menu_index == 2) {
		obj_ESC.image_index = 1;
	}
	else {
		obj_ESC.image_index = 0;
	}
}

if (menu_index < 0) {
	menu_index = buttons - 1;
	obj_ESC.image_index = 1;
}
if (menu_index > buttons - 1) {
	menu_index = 0;
	obj_STORIA.image_index = 1;
}

var i = 0;
repeat(buttons) {
	
	if(flying[i] == 1) {
	i++;
	}
	
	if(i < buttons) {
		flying[i] = min(1, flying[i] + 0.02);
	}
	
	if(i + 1 < buttons) {
		flying[i + 1] = min(1, flying[i + 1] + 0.005);
	}
}

if (menu_index != last_selected) {
	//part_particles_create(particle_effects, menu_x, menu_y - 78 + (button_h + button_padding) * menu_index, box_flash, 1);
	audio_play_sound(snd_menu_switch, 1, false);
}

last_selected = menu_index;

if(scr_check_menu_command(vk_enter, gp_face1)) {
switch(menu_index) {
	case 0:
		room_goto(rm_story_menu);
		break;
	case 1:
	    room_goto(rm_controls_gamepad);
		break;
	case 2:
	    room_goto(rm_exit_screen);
		break;
		}
}

if(scr_check_menu_command(vk_escape, gp_face2)) {
	room_goto(rm_exit_screen);
}

obj_ENTER_A_per_continuare.image_index = global.last_device;
obj_ESC_B_per_uscire.image_index = global.last_device;

