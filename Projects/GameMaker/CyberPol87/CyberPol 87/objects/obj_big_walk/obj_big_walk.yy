{
    "id": "5a65923e-0f3b-41d7-a228-0d1c3e2d88b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_big_walk",
    "eventList": [
        {
            "id": "c780001e-6e92-47ab-ac83-56d2751ac2e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5a65923e-0f3b-41d7-a228-0d1c3e2d88b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8b691593-2856-4dcd-8481-d1e5806bb6d7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "25039fea-9bdb-4ed9-add7-b27a917af0c0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 19,
            "y": 1
        },
        {
            "id": "4ef3623c-6d7a-4ad3-a0f1-1b1264a9fb9a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 39,
            "y": 1
        },
        {
            "id": "3a5d6e5c-d365-4195-b9f6-b8066d036dbe",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 39,
            "y": 64
        },
        {
            "id": "fd17497b-f9f1-4b18-8f51-cf2a86beae98",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 19,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "516e78f4-62a8-4945-b4e1-9fc5b5d87781",
    "visible": true
}