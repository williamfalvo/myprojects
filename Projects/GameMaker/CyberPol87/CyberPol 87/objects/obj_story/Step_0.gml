Conferma = keyboard_check_pressed(vk_enter) || gamepad_button_check_pressed(0,gp_face1);

if(Conferma)
{
	if(index_story == 0)
	{
		sprite_index = spr_testo_finale2;
		index_story ++;
	}
	else if(index_story == 1)
	{
		sprite_index = spr_testo_finale3;
		index_story ++;
	}
	else if(index_story == 2)
	{
		sprite_index = spr_Cutscene_Finale;
		index_story ++;
	}
	else if(index_story == 3)
	{
		instance_create_layer(x,1500,layer,obj_credits);
		var art = instance_create_layer(x,2500,layer,obj_credits);
		art.sprite_index = spr_credits_artist;
		var design = instance_create_layer(x,3500,layer,obj_credits);
		design.sprite_index = spr_credits_design;
		design.lastCredits = true;
		instance_destroy(obj_skip_text);
		instance_destroy();
	}
}