{
    "id": "04c5b253-a758-42b4-9de7-933d0f503eae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_story",
    "eventList": [
        {
            "id": "62b06803-76e9-4a82-a63b-e4277583b258",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "04c5b253-a758-42b4-9de7-933d0f503eae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "f49b44c7-1abe-4690-86c8-46e219103d81",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "index_story",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "6c740754-7d39-4550-841f-1066b2d07823",
    "visible": true
}