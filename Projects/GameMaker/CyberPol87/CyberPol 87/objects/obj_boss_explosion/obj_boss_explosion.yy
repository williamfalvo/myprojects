{
    "id": "2ad5167f-5f51-4470-9cfa-fd0af33fe0b0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss_explosion",
    "eventList": [
        {
            "id": "131ca547-6144-4436-a848-75ef3e88c20f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2ad5167f-5f51-4470-9cfa-fd0af33fe0b0"
        },
        {
            "id": "ead167a9-b100-4995-9145-2832fa2e03c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ad5167f-5f51-4470-9cfa-fd0af33fe0b0"
        },
        {
            "id": "c98ce84e-0770-4325-aaab-697ae1d05802",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "2ad5167f-5f51-4470-9cfa-fd0af33fe0b0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e631297c-4ae0-4e29-a364-e6a598169084",
    "visible": true
}