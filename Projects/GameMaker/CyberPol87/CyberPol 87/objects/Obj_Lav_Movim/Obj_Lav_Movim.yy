{
    "id": "718c5e24-04e1-465d-ae10-7ffa3fb38004",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_Lav_Movim",
    "eventList": [
        {
            "id": "bb1a5a96-b3c2-41a8-8303-48c1b8f8eeb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "718c5e24-04e1-465d-ae10-7ffa3fb38004"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a1db55dc-e4e4-4a80-a2e8-41f365846733",
    "visible": true
}