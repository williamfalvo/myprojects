{
    "id": "d6d403af-f510-4344-963e-ed1cd8dfd387",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_batteria",
    "eventList": [
        {
            "id": "5a6785d0-eeff-45f8-8fb8-5917d05628b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d6d403af-f510-4344-963e-ed1cd8dfd387"
        },
        {
            "id": "b7d0836a-4135-4f7a-9542-20243d635228",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d6d403af-f510-4344-963e-ed1cd8dfd387"
        },
        {
            "id": "439db05f-c963-4a45-9fc6-8009a7ec8e57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a56b2e07-b084-46bf-a767-0c9745022d11",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d6d403af-f510-4344-963e-ed1cd8dfd387"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.1,
    "physicsFriction": 0.2,
    "physicsGroup": 4,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "179162e4-559b-414c-940a-8cb2bc2f0e2a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 7,
            "y": 8
        },
        {
            "id": "d0de6531-69e8-4050-806d-d43d873cbe6e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 24,
            "y": 8
        },
        {
            "id": "2dec61be-5dfa-4d2f-828f-e3bff1680f56",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 24,
            "y": 31
        },
        {
            "id": "77a0ef87-5e67-4ba1-82a3-3aa012ec52ae",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 7,
            "y": 31
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "77bf07ab-4b89-4de5-b6d8-3edd73b3b9cc",
    "visible": true
}