{
    "id": "ce6192c0-e0dd-4629-8c89-2cdd70757f79",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_shoot",
    "eventList": [
        {
            "id": "8b266127-bd0c-40d2-b6a3-762d0379be6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ce6192c0-e0dd-4629-8c89-2cdd70757f79"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f3b9e6ec-dab0-4e96-8701-2c5970236dca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "ddffc5c9-9ced-4d81-ab37-ffa39321bcbd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 21,
            "y": 6
        },
        {
            "id": "fc0c2dd5-b948-4c5b-9a21-ff05fd4e8e3e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 34,
            "y": 6
        },
        {
            "id": "33f4b03b-e059-4f46-b870-478f517ec656",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 34,
            "y": 64
        },
        {
            "id": "cb0b23c2-a324-4e7a-b2cb-3482893d78d1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 21,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "e7e62ced-d5d5-47e0-8102-323241ef8b6e",
    "visible": true
}