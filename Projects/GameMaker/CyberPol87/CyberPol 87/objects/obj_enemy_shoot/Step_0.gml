if(obj_gameManager.gameIsPaused) exit;


//////////////////// Fix rotazione ////////////////////
event_inherited();

//////////////////// Quando nota il player rimane sempre girato verso di lui ////////////////////

if (obj_player_commands.x < x )
{
	image_xscale = -1
}
else
{
	image_xscale = 1
}

//////////////////// Shoot Direction ////////////////////
#macro lineLenght 600

if (!enemy_small.is_shooting)
{
	if ( collision_line(x, y, x- lineLenght, y + lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x-20, y+5,layer, obj_bullet_enemy);
		bullet.direction = 235
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_enemy_downr
		image_xscale = -1
		enemy_small.is_shooting = true
		enemy_small.time_shooting = 0
		
	}

	else if ( collision_line(x , y, x- lineLenght, y-lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x-20, y-35,layer, obj_bullet_enemy);
		bullet.direction = 125
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_enemy_upr
		image_xscale = -1
		enemy_small.is_shooting = true
		enemy_small.time_shooting = 0
		
	}

	else if ( collision_line(x , y, x-lineLenght, y, obj_player_commands,false,false) && collision_circle(x,y,enemy_small.player_distance_shoot,obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x-20, y-10,layer, obj_bullet_enemy);
		bullet.direction = 180;
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_enemy_right
		image_xscale = -1	
		enemy_small.is_shooting = true
		enemy_small.time_shooting = 0
		
	}

	else if ( collision_line(x, y, x+ lineLenght, y + lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x+20, y+5,layer, obj_bullet_enemy);
		bullet.direction = -45
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_enemy_downr
		image_xscale = 1
		enemy_small.is_shooting = true
		enemy_small.time_shooting = 0
		
	}

	else if ( collision_line(x , y, x+ lineLenght, y-lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x+20, y-35,layer, obj_bullet_enemy);
		bullet.direction = 45
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_enemy_upr
		image_xscale = 1
		enemy_small.is_shooting = true
		enemy_small.time_shooting = 0
		
	}

	else if ( collision_line(x , y, x+lineLenght, y, obj_player_commands,false,false) && collision_circle(x,y,enemy_small.player_distance_shoot,obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x+20, y-10,layer, obj_bullet_enemy);
		bullet.direction = 0;
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_enemy_right
		image_xscale = 1	
		enemy_small.is_shooting = true
		enemy_small.time_shooting = 0
		
	}
	
	else if ( collision_line(x, y, x, y-lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x-3, y-30,layer, obj_bullet_enemy);
		bullet.direction = 90;
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_enemy_up
		image_xscale = 1	
		enemy_small.is_shooting = true
		enemy_small.time_shooting = 0
		
	}
	
	else if ( collision_line(x, y, x, y+lineLenght, obj_player_commands,false,false))
	{
		bullet = instance_create_layer(x-3, y+30,layer, obj_bullet_enemy);
		bullet.direction = 270;
		bullet.phy_rotation = -bullet.direction;
		sprite_index = spr_enemy_down
		image_xscale = 1	
		enemy_small.is_shooting = true
		enemy_small.time_shooting = 0
		
	}
	else
	{
		scr_player_set_state(obj_enemy_walk)
	}
}
else
{
	enemy_small.time_shooting ++
}

if (enemy_small.time_shooting > enemy_small.cooldown_max)
{
	enemy_small.is_shooting = false
}




