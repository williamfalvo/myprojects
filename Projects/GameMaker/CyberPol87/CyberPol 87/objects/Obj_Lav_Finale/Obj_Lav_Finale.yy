{
    "id": "e3b9b078-8c3f-46b6-8e77-d5dab6437200",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_Lav_Finale",
    "eventList": [
        {
            "id": "9e1075a1-27b0-42c2-a7c7-091aa656c4a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e3b9b078-8c3f-46b6-8e77-d5dab6437200"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "458aec77-8212-4fde-9d25-93d2a9b36c91",
    "visible": true
}