{
    "id": "ac1f0693-e4ff-4736-87db-be8aeff61a63",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fat_dead",
    "eventList": [
        {
            "id": "a7f58a3d-f31b-4d45-8496-7a13b1738c53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ac1f0693-e4ff-4736-87db-be8aeff61a63"
        },
        {
            "id": "32948e49-a36b-4263-be0a-be9bf9af3393",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "ac1f0693-e4ff-4736-87db-be8aeff61a63"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d69f68e-96fe-4108-938b-6d282eda61bc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "65c0b8f3-dd5b-4237-9c5f-d428e371a15a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 36
        },
        {
            "id": "66c20dba-bed3-49a7-abf6-be2e6663a42b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 62,
            "y": 36
        },
        {
            "id": "f8ea2167-be9c-4497-81fa-7585db001545",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 62,
            "y": 64
        },
        {
            "id": "700fc6bd-2830-488e-9bd1-e0d9662967f5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "0159f7a8-950d-4871-b1d5-afd58dfbb6f9",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2.5",
            "varName": "animation_dead",
            "varType": 0
        }
    ],
    "solid": true,
    "spriteId": "6b1bdb7a-697f-45c2-83a9-fdceb51bdbe2",
    "visible": true
}