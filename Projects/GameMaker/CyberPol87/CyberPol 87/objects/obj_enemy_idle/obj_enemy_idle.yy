{
    "id": "9e09d121-7625-47e1-9ba3-4bc038d519c7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_idle",
    "eventList": [
        {
            "id": "a4ef9d02-d92c-4bdd-914e-943ded109b16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e09d121-7625-47e1-9ba3-4bc038d519c7"
        },
        {
            "id": "d0f3db15-5407-4773-95b3-11f5bea697e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9e09d121-7625-47e1-9ba3-4bc038d519c7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f3b9e6ec-dab0-4e96-8701-2c5970236dca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "b7dc7127-aff1-4745-b064-5ba3a3605354",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 21,
            "y": 6
        },
        {
            "id": "2eb25238-1efb-4e5d-bd42-eef4cdcd1d6f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 34,
            "y": 6
        },
        {
            "id": "d43c77a9-6c11-4749-9637-dc04c5ca4c73",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 34,
            "y": 64
        },
        {
            "id": "cce5eb95-acbc-4c15-9922-f5317a49cc7c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 21,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "aca5e03d-4b7e-4050-b258-98a7e66e201d",
    "visible": true
}