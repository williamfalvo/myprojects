{
    "id": "1955a59d-a9bf-4a6b-baf6-e8c93f5ff013",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fat_walk",
    "eventList": [
        {
            "id": "2e0bf3dc-81a9-4670-8193-0babc45e6086",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1955a59d-a9bf-4a6b-baf6-e8c93f5ff013"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d69f68e-96fe-4108-938b-6d282eda61bc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "ea7845c4-6106-46f0-9de6-23426861d635",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 18,
            "y": 5
        },
        {
            "id": "f5b88510-be78-404b-92c8-c0a000257ccc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 38,
            "y": 5
        },
        {
            "id": "869d4598-281c-46ed-b866-ac6e7d399d25",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 38,
            "y": 65
        },
        {
            "id": "d8d2e3a3-821a-4f5b-ba33-de3dfae1ec00",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 18,
            "y": 65
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "93df8eed-8904-4bb3-9bff-83b2c8c1a4a2",
    "visible": true
}