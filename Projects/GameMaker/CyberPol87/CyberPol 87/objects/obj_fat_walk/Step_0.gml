if(obj_gameManager.gameIsPaused) exit;


//////////////////// Fix rotazione ////////////////////
event_inherited();

//////////////////// Quando nota il player rimane sempre girato verso di lui ////////////////////
//////////////////// Non scende dalla piattaforma ////////////////////

if (obj_player_commands.x < x && place_meeting ( x-20, y+1, obj_enemy_ground))
{
	image_xscale = -1
	phy_position_x -= enemy_fat.enemy_speed
}
else if (obj_player_commands.x > x && place_meeting ( x+20, y+1, obj_enemy_ground))
{
	image_xscale = 1
	phy_position_x += enemy_fat.enemy_speed
}

else if (!place_meeting ( x-20, y+1, obj_enemy_ground) || !place_meeting ( x+20, y+1, obj_enemy_ground))
{
	scr_player_set_state(obj_fat_wait)	
}

//////////////////// Entra nello stato Shoot ////////////////////

#macro lineLenght 600

if ( collision_line(x, y, x- lineLenght, y + lineLenght, obj_player_commands,false,false))
{
	scr_player_set_state(obj_fat_shoot)
}

else if ( collision_line(x , y, x- lineLenght, y-lineLenght, obj_player_commands,false,false))
{
	scr_player_set_state(obj_fat_shoot)
}

else if ( collision_line(x , y, x-lineLenght, y, obj_player_commands,false,false) && collision_circle(x,y,enemy_fat.player_distance_shoot,obj_player_commands,false,false))
{
	scr_player_set_state(obj_fat_shoot)
}

else if ( collision_line(x, y, x+ lineLenght, y + lineLenght, obj_player_commands,false,false))
{
	scr_player_set_state(obj_fat_shoot)
}

else if ( collision_line(x , y, x+ lineLenght, y-lineLenght, obj_player_commands,false,false))
{
	scr_player_set_state(obj_fat_shoot)
}

else if ( collision_line(x , y, x+lineLenght, y, obj_player_commands,false,false) && collision_circle(x,y,enemy_fat.player_distance_shoot,obj_player_commands,false,false))
{
	scr_player_set_state(obj_fat_shoot)
}

else if ( collision_line(x, y, x, y-lineLenght, obj_player_commands,false,false))
{
	scr_player_set_state(obj_fat_shoot)
}

else if ( collision_line(x, y, x, y+lineLenght, obj_player_commands,false,false))
{
	scr_player_set_state(obj_fat_shoot)
}



