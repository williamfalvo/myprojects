{
    "id": "8b691593-2856-4dcd-8481-d1e5806bb6d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_big_commands",
    "eventList": [
        {
            "id": "b821a4a1-152b-4796-9130-db013ce00901",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8b691593-2856-4dcd-8481-d1e5806bb6d7"
        },
        {
            "id": "cef91203-1438-4d58-9b1e-9ab105f74473",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "8b691593-2856-4dcd-8481-d1e5806bb6d7"
        },
        {
            "id": "88af86d9-e820-4a55-9c76-569e6f332d75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "8b691593-2856-4dcd-8481-d1e5806bb6d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        
    ],
    "parentObjectId": "7c572097-4bf4-4928-9803-6d565f3bd51b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "71e8ddd3-a48c-499e-a9b8-74794f6b7553",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "54487d51-ca9b-4d81-90bd-87b4927b7e2e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "bed7f458-5cbf-4f11-a067-a6203ce0b4a8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "229c9361-c54e-4f41-b5f2-3af06f0a1675",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "83521e92-c9fd-49ad-a607-8e79a9ea285d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "noone",
            "varName": "nextState",
            "varType": 1
        }
    ],
    "solid": true,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}