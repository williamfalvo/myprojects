if (nextState != noone)
{
	var state = nextState;
	nextState = noone;
	instance_change(state, true);
}
