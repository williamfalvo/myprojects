{
    "id": "0a68bbd1-e588-401a-ac9c-c2cd5c169115",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_barile_tossico",
    "eventList": [
        {
            "id": "bf84e550-607b-4ac9-87e8-86307839106a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0a68bbd1-e588-401a-ac9c-c2cd5c169115"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ab317226-b9fd-4c2b-882a-74436d26b491",
    "visible": true
}