{
    "id": "999d01e9-da97-4053-9028-d62968d66410",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "ed2e0531-ac42-4341-a7dd-961c5818c2e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "999d01e9-da97-4053-9028-d62968d66410"
        },
        {
            "id": "d1648577-bf6b-47d0-aef6-b71bb00863d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "999d01e9-da97-4053-9028-d62968d66410"
        },
        {
            "id": "0ce9e06b-0f70-43ba-9ef7-602e9014fd9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "999d01e9-da97-4053-9028-d62968d66410"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 5,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "4bcb2830-da74-491c-86d3-7c1c5490f8d0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "8c2056be-c74e-4909-b509-b00d9695b645",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "4101f31d-6ec2-4628-9b74-c528ba3e8650",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "70f14e0a-0821-437c-82f5-cc1b7768ecab",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "4851cca6-5c85-4207-9fc0-e1377ed20fef",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "10",
            "varName": "bullet_speed",
            "varType": 0
        },
        {
            "id": "7f5c0621-32fb-4208-b3dc-5a8ddc5153f5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "10",
            "varName": "damage",
            "varType": 0
        },
        {
            "id": "e28755df-65a1-4ebc-a158-e2a0c0f55d2d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "enemy_hitted",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "004babb2-fc1b-4f41-aac3-954d1e8b52eb",
    "visible": true
}