if(obj_gameManager.gameIsPaused) exit;

phy_position_x += lengthdir_x(bullet_speed,direction);
phy_position_y += lengthdir_y(bullet_speed,direction);


if(place_meeting(x,y,obj_floor))
{
	if(obj_player.bullet != obj_shotgun_bullet)
	{
		instance_destroy();
	}
}