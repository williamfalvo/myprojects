{
    "id": "f3b9e6ec-dab0-4e96-8701-2c5970236dca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_small_commands",
    "eventList": [
        {
            "id": "b578480a-6a8d-4910-8bd2-a60203298b51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f3b9e6ec-dab0-4e96-8701-2c5970236dca"
        },
        {
            "id": "fcbcfded-d77a-4a96-a70f-04a5557f052e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "f3b9e6ec-dab0-4e96-8701-2c5970236dca"
        },
        {
            "id": "334944ec-bae5-41db-80f0-0c3a662eef76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "f3b9e6ec-dab0-4e96-8701-2c5970236dca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7c572097-4bf4-4928-9803-6d565f3bd51b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "7d522c7e-9589-44ba-97d4-2f40931955b7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "f88aa5b4-e20c-41ec-a6fc-df101ff7be14",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "1b21861e-50d5-4866-86cd-3091e1766069",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "25108fb9-ec93-4a11-bc3e-913a40a0c3c7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "99c4df21-d632-464e-9c9f-81a2d8082eaa",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "noone",
            "varName": "nextState",
            "varType": 1
        }
    ],
    "solid": true,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}