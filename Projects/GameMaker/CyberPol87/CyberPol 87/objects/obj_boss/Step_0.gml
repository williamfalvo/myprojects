if ( hp <= 0 )
{
	instance_change(obj_boss_death,true)
}

if ( start )
{
	if (!Shooting)
	{
			bullet1 = instance_create_layer(x, y, layer, obj_bullet_boss)
			bullet2 = instance_create_layer(x, y, layer, obj_bullet_boss)
			bullet3 = instance_create_layer(x, y, layer, obj_bullet_boss)
			bullet1.direction = 270
			bullet1.phy_rotation = -bullet1.direction;
			bullet2.direction = 225
			bullet2.phy_rotation = -bullet2.direction;
			bullet3.direction = 315
			bullet3.phy_rotation = -bullet3.direction;
			Shooting = true;
			time_shooting = 0
	}
	else
	{
		time_shooting ++
	}

	if (time_shooting > cooldown_max)
	{
		Shooting = false
	}
}


var bullet = instance_place(x,y,obj_bullet);
if( bullet != noone )
{
	if ( hp > 0 )
	{
		if(obj_player.bullet != obj_shotgun_bullet) 
		{
			audio_play_sound(snd_damage_enemy,0,false);
			hp -= bullet.damage;
			instance_destroy(bullet);
		}
		else if( !bullet.enemy_hitted )
		{
			audio_play_sound(snd_damage_enemy,0,false);
			hp -= bullet.damage;
			bullet.enemy_hitted = true;
		}
	}
	else
	{
		instance_destroy(bullet)
	}
}

