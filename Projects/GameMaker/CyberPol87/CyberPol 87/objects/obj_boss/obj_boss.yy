{
    "id": "b6cf26cd-99c7-4e36-86eb-a702e8d720f8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss",
    "eventList": [
        {
            "id": "bb0d24e3-5123-4d39-a0c2-6d04fd5386f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b6cf26cd-99c7-4e36-86eb-a702e8d720f8"
        },
        {
            "id": "1e3c729e-0995-43d6-aeac-3674914c72ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b6cf26cd-99c7-4e36-86eb-a702e8d720f8"
        },
        {
            "id": "02344265-6d06-4a51-9843-11cdd0cf7ee2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b6cf26cd-99c7-4e36-86eb-a702e8d720f8"
        },
        {
            "id": "03ce1e25-866b-426a-a253-e794ff98fbe3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "b6cf26cd-99c7-4e36-86eb-a702e8d720f8"
        },
        {
            "id": "8b6ce3a9-e479-4e2a-ace9-1d1b9c88d45a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b6cf26cd-99c7-4e36-86eb-a702e8d720f8"
        },
        {
            "id": "e6da4269-fe00-42d6-ac5e-328c44fb7124",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b6cf26cd-99c7-4e36-86eb-a702e8d720f8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "bcccaab1-91bf-44db-863d-6f4e7c4f11e9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": -1,
            "y": 0
        },
        {
            "id": "f24e7b02-4581-4801-9b8e-efba3f7a04fe",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 65,
            "y": 0
        },
        {
            "id": "353c1e9f-f13e-4e2f-8aa5-c3c637ff1352",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 65,
            "y": 94
        },
        {
            "id": "21248e00-a98b-4f12-b9aa-323d29efe37e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": -1,
            "y": 94
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "44354693-7737-463d-98e9-e0dc00d8d2f6",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "400",
            "varName": "hp",
            "varType": 0
        },
        {
            "id": "1096855f-2129-46fc-b30d-f8830dddabb2",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "Shooting",
            "varType": 3
        },
        {
            "id": "9d1c0249-75ce-4c40-ba4d-bcc110c90c31",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "time_shooting",
            "varType": 0
        },
        {
            "id": "b01e8f5c-1b47-4386-ab70-7dd6776fddcb",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "70",
            "varName": "cooldown_max",
            "varType": 0
        },
        {
            "id": "a2584938-f64d-45dc-b8a9-38edc507c686",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "False",
            "varName": "start",
            "varType": 3
        },
        {
            "id": "1c8c6e18-c686-4c8e-9adb-a5685f39e5ba",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "180",
            "varName": "boss_start",
            "varType": 0
        }
    ],
    "solid": true,
    "spriteId": "6a927b9c-72c6-46e3-b592-77de612c6648",
    "visible": true
}