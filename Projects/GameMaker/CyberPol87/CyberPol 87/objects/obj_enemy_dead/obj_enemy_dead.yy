{
    "id": "c518a4fa-8493-4041-bbad-35d9e59bd724",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_dead",
    "eventList": [
        {
            "id": "26efd853-abb8-4cf7-924b-c7837f7aa5a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "c518a4fa-8493-4041-bbad-35d9e59bd724"
        },
        {
            "id": "8b4b42e4-eb9e-4d11-8742-7dd4aca81539",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c518a4fa-8493-4041-bbad-35d9e59bd724"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f3b9e6ec-dab0-4e96-8701-2c5970236dca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 2,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "938a0e21-7965-4604-9a6e-16eda4734715",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 1,
            "y": 36
        },
        {
            "id": "aa5af92b-e38b-41ad-9a1b-ea99297fc18b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 38,
            "y": 36
        },
        {
            "id": "2fea73b7-83f5-4cbf-8d1f-72170dab4832",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 38,
            "y": 64
        },
        {
            "id": "c86d1c57-fcfb-4470-875b-34ca0d7736ae",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 1,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "0fa9e5de-a024-40e6-9ea7-736142d4c45f",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2.5",
            "varName": "animation_dead",
            "varType": 0
        }
    ],
    "solid": true,
    "spriteId": "beec8625-ee82-4e9e-9a3d-f4f4c835e3d0",
    "visible": true
}