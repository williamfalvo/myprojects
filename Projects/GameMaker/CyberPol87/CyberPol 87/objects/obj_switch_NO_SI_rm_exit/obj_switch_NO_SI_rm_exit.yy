{
    "id": "d33313f3-057a-41da-a63f-b1fd0ffb86b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_switch_NO_SI_rm_exit",
    "eventList": [
        {
            "id": "2e18e621-48ca-4975-ae20-804bd6fff665",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d33313f3-057a-41da-a63f-b1fd0ffb86b4"
        },
        {
            "id": "ca8cc3dd-551a-443d-9e2e-d23f0d780433",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d33313f3-057a-41da-a63f-b1fd0ffb86b4"
        },
        {
            "id": "2b0cd8f0-6d6b-4a62-bf67-5fe24245649b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d33313f3-057a-41da-a63f-b1fd0ffb86b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}