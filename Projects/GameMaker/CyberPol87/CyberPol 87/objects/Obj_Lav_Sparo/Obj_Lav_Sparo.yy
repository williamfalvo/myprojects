{
    "id": "32306bf6-9779-4f5f-a7e8-5195ea6ce6da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_Lav_Sparo",
    "eventList": [
        {
            "id": "6a2e3bfc-52f2-4ddb-9483-48f8af9c5f0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "32306bf6-9779-4f5f-a7e8-5195ea6ce6da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ad778e5a-2b20-4d43-95c4-e0e30ccd6d9b",
    "visible": true
}