{
    "id": "d3512df8-ca11-46f7-84dd-159d875f4268",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Obj_Lav_Salto",
    "eventList": [
        {
            "id": "98b76e2b-0a7b-4eb5-b437-23ce402f67fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d3512df8-ca11-46f7-84dd-159d875f4268"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7c1102e-c23f-435a-a32b-31dbcbfdfde9",
    "visible": true
}