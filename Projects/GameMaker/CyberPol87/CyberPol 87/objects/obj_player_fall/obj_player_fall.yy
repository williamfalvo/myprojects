{
    "id": "bcbf4a98-ecf1-40d0-bf03-e76484dd5cac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_fall",
    "eventList": [
        {
            "id": "00e54056-0192-4cf5-b028-3dfb5c9b8f08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bcbf4a98-ecf1-40d0-bf03-e76484dd5cac"
        },
        {
            "id": "e119ce81-b0a4-4eec-befd-72ec843f35b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bcbf4a98-ecf1-40d0-bf03-e76484dd5cac"
        },
        {
            "id": "ff54fbfc-6e23-4392-8607-c242499e1527",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "bcbf4a98-ecf1-40d0-bf03-e76484dd5cac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "391591f8-290b-4bb3-96b9-c4eb515f315c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.05,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "534c2d48-866a-4dc9-ab3a-c6a48db75d35",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 15,
            "y": 26
        },
        {
            "id": "b073a5e7-196d-4e3a-942e-062b399805d3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 30,
            "y": 26
        },
        {
            "id": "dda3f342-8a43-4105-98dd-265502422629",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 30,
            "y": 82
        },
        {
            "id": "63b17a94-c785-435e-b713-af24afd5fe39",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 15,
            "y": 82
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "6e49e9ec-03ce-4446-8943-5c41a1954625",
    "visible": true
}