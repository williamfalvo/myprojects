{
    "id": "f5e0bb11-5be6-45fd-bee7-cb88f3656039",
    "modelName": "GMTimeline",
    "mvc": "1.0",
    "name": "tml_player_hit",
    "momentList": [
        {
            "id": "1caee5f7-f933-46a7-8acc-844f4f47712d",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "5bf086ff-39ff-4da4-8f7e-2be25a1ee031",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 0,
                "eventtype": 0,
                "m_owner": "f5e0bb11-5be6-45fd-bee7-cb88f3656039"
            },
            "moment": 0
        },
        {
            "id": "3bc98311-2088-4e2a-9fd5-cee77c108299",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "8d38a448-6b30-47bc-a57b-a9970cf1db9f",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 15,
                "eventtype": 0,
                "m_owner": "f5e0bb11-5be6-45fd-bee7-cb88f3656039"
            },
            "moment": 15
        },
        {
            "id": "a027707f-341c-4139-8476-c90cf669963d",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "7512c8e1-331b-46a0-a1ac-826aca5e48e7",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 25,
                "eventtype": 0,
                "m_owner": "f5e0bb11-5be6-45fd-bee7-cb88f3656039"
            },
            "moment": 25
        },
        {
            "id": "0cc5b5a1-21d4-4a6e-a4bb-92c977f29283",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "0b5fb53e-2ac8-4a92-bb8a-cd3f0d65410c",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 35,
                "eventtype": 0,
                "m_owner": "f5e0bb11-5be6-45fd-bee7-cb88f3656039"
            },
            "moment": 35
        },
        {
            "id": "badcf4c5-e83b-410c-9b19-8b2c206e43cd",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "77803ce2-e9db-4e43-b645-d21161439f9f",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 45,
                "eventtype": 0,
                "m_owner": "f5e0bb11-5be6-45fd-bee7-cb88f3656039"
            },
            "moment": 45
        },
        {
            "id": "2c6356b6-2566-4b3a-bc17-d58211d49e0a",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "4556f00f-b6d8-404b-aeae-868ebeac2321",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 55,
                "eventtype": 0,
                "m_owner": "f5e0bb11-5be6-45fd-bee7-cb88f3656039"
            },
            "moment": 55
        }
    ]
}