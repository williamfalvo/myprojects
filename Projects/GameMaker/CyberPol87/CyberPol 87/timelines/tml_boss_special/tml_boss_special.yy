{
    "id": "25e2764e-722d-42a0-9583-1825866428e4",
    "modelName": "GMTimeline",
    "mvc": "1.0",
    "name": "tml_boss_special",
    "momentList": [
        {
            "id": "6b0fc41f-f3e2-4709-aa50-d80ef4a1df6a",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "f15722c2-779b-4ee2-adef-f087555ced1b",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 0,
                "eventtype": 0,
                "m_owner": "25e2764e-722d-42a0-9583-1825866428e4"
            },
            "moment": 0
        },
        {
            "id": "158c9374-7363-467f-ab19-19a62bf19e18",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "923b5086-c3e4-4c19-8d5c-8b5d998fd86f",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 15,
                "eventtype": 0,
                "m_owner": "25e2764e-722d-42a0-9583-1825866428e4"
            },
            "moment": 15
        },
        {
            "id": "76db4fe4-5648-4ba4-9dca-c5e359990522",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "5eb5138f-05fb-4b56-b654-6f87c1228f82",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 30,
                "eventtype": 0,
                "m_owner": "25e2764e-722d-42a0-9583-1825866428e4"
            },
            "moment": 30
        },
        {
            "id": "b3c9fd6c-1137-4c75-a495-fad6e45ec84f",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "16cbc66b-3ead-4588-a321-318fc0155dea",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 45,
                "eventtype": 0,
                "m_owner": "25e2764e-722d-42a0-9583-1825866428e4"
            },
            "moment": 45
        },
        {
            "id": "787318ea-0745-48c3-9f75-bbae0716594d",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "319bb5d8-06be-4234-a9a9-d7824055b28e",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 60,
                "eventtype": 0,
                "m_owner": "25e2764e-722d-42a0-9583-1825866428e4"
            },
            "moment": 60
        },
        {
            "id": "ef9d6d10-e26a-450e-94f1-9809952e2f3b",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "c318165c-a56c-49b3-9e0c-ddd85d6a95ec",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 75,
                "eventtype": 0,
                "m_owner": "25e2764e-722d-42a0-9583-1825866428e4"
            },
            "moment": 75
        },
        {
            "id": "ee17a90e-b112-4b1c-9d46-a604e0a95279",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "0e12029d-2f72-4558-b09d-f878deac6791",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 90,
                "eventtype": 0,
                "m_owner": "25e2764e-722d-42a0-9583-1825866428e4"
            },
            "moment": 90
        },
        {
            "id": "e2567d0f-bf9a-4f11-b1d1-c8bc176385e1",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "7b151ca5-d2e5-4741-8f5d-51c17f352093",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 105,
                "eventtype": 0,
                "m_owner": "25e2764e-722d-42a0-9583-1825866428e4"
            },
            "moment": 105
        },
        {
            "id": "468d53ee-737e-48e0-bf97-dad9c0c3222d",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "d6ee237a-4dbb-40a4-bbd9-234642391d42",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 120,
                "eventtype": 0,
                "m_owner": "25e2764e-722d-42a0-9583-1825866428e4"
            },
            "moment": 120
        },
        {
            "id": "d566f6e1-01a3-48b8-9661-35d54eb65ca6",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "6f5ce234-282a-4571-92e1-33bc8b244533",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 135,
                "eventtype": 0,
                "m_owner": "25e2764e-722d-42a0-9583-1825866428e4"
            },
            "moment": 135
        },
        {
            "id": "8150ba75-0f75-4856-8ae0-ed2d72c22829",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "0c237296-ad95-4e32-86e5-3b4cdb71e81e",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 150,
                "eventtype": 0,
                "m_owner": "25e2764e-722d-42a0-9583-1825866428e4"
            },
            "moment": 150
        },
        {
            "id": "147e828c-debd-40da-8ce4-c43ec343b975",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "cf097437-5884-42b0-a94e-44823236c3c3",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 165,
                "eventtype": 0,
                "m_owner": "25e2764e-722d-42a0-9583-1825866428e4"
            },
            "moment": 165
        }
    ]
}