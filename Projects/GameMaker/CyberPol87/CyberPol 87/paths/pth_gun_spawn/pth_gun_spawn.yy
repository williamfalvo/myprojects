{
    "id": "9fc35a21-12c8-41b7-9203-be8fa5c8b162",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pth_gun_spawn",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "2d0896e6-4fc3-4d91-93c0-192d45e12167",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 478,
            "speed": 100
        },
        {
            "id": "be70fb3b-46ac-432f-b7bc-2150790bd1b2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 7,
            "y": 449,
            "speed": 100
        },
        {
            "id": "60fe5bf3-9d2c-42f9-811d-5bbe0ebde0a1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 435,
            "speed": 100
        },
        {
            "id": "a7806717-b323-4f73-9be2-96a08e5401f0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 27,
            "y": 430,
            "speed": 100
        },
        {
            "id": "ad492834-ceee-4247-99e7-e318ac2fa701",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 41,
            "y": 433,
            "speed": 100
        },
        {
            "id": "99e71fca-35aa-4eab-82c6-a0e1981ac6b7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 54,
            "y": 446,
            "speed": 100
        },
        {
            "id": "e410ef80-482a-4e49-8afb-007c38a8556e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 61,
            "y": 461,
            "speed": 100
        },
        {
            "id": "4e87630d-1ecb-4886-af15-7b2051f4256e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 67,
            "y": 479,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}