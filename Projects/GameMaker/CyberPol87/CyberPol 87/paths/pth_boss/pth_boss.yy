{
    "id": "21b68b00-74cf-401a-a572-f6cdc693f698",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pth_boss",
    "closed": true,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "c91f7dfd-f929-4364-9db1-47e9ee51131f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 160,
            "speed": 100
        },
        {
            "id": "c5bef2c6-5705-4790-bd4d-271d795c8a3d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 255,
            "y": 190,
            "speed": 100
        },
        {
            "id": "157c8c4e-b5d3-467c-a4b4-110c279e9dfe",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 159,
            "y": 213,
            "speed": 100
        },
        {
            "id": "ca62e52f-668a-494b-b754-8a1cef2c61da",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 95,
            "y": 190,
            "speed": 100
        },
        {
            "id": "ca0afbda-4d33-4a5e-acf4-ed90213e97d4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 63,
            "y": 158,
            "speed": 100
        },
        {
            "id": "7788927f-c873-4ea7-95a1-f380b34517f1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 95,
            "y": 126,
            "speed": 100
        },
        {
            "id": "62d37091-b91a-46d6-abf8-70460a7705d0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 159,
            "y": 94,
            "speed": 100
        },
        {
            "id": "08073d50-c5e3-4e75-8d93-a1f25597c0c0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 255,
            "y": 126,
            "speed": 100
        },
        {
            "id": "7f418bd3-44e5-4be1-8b41-c4b20f265b99",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 320,
            "y": 160,
            "speed": 100
        },
        {
            "id": "84fb578c-324b-4cdf-b62c-b7dcbe9f4ea4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 383,
            "y": 190,
            "speed": 100
        },
        {
            "id": "9d55b657-2ce3-46d3-b0f8-82fb3a48a88c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 479,
            "y": 213,
            "speed": 100
        },
        {
            "id": "3141da35-4fbb-46a9-a113-334cef24d434",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 543,
            "y": 190,
            "speed": 100
        },
        {
            "id": "043ded20-aa2b-40f6-b985-c72cb4caeef7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 575,
            "y": 158,
            "speed": 100
        },
        {
            "id": "f7ac93e5-2585-4c68-a592-c782091fd1f2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 543,
            "y": 126,
            "speed": 100
        },
        {
            "id": "e7a5dfe5-2bf8-4a06-a88f-8410889f1579",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 479,
            "y": 94,
            "speed": 100
        },
        {
            "id": "7cfb79e9-638e-4457-a536-2cb8845cf362",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 383,
            "y": 126,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}