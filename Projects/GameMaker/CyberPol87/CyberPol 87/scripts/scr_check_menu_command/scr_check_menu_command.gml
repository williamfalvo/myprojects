//Controlla device in utilizzo

enum device {
	keyboard, pad
}
///////////////////////////////////////////
if (!variable_global_exists("pad_stick")) {
	global.pad_stick = false;
}//////////////////////////////////////////

if (keyboard_check_pressed(argument0)) {
	global.last_device = device.keyboard;
	return true;
}

else if (gamepad_button_check_pressed(0, argument1)) {
	global.last_device = device.pad;
	return true;
}
///////////////////////////////////////////////
else if (global.pad_stick == false)
{
	if (argument1 == gp_padd && gamepad_axis_value(0, gp_axislv) > 0.5) {
		global.last_device = device.pad;
		global.pad_stick = true;
		return true;	
	}

	else if (argument1 == gp_padu && gamepad_axis_value(0, gp_axislv) < -0.5) {
		global.last_device = device.pad;
		global.pad_stick = true;
		return true;
	}
}
else {
	if (abs(gamepad_axis_value(0, gp_axislv)) <= 0.5)
	{
		global.pad_stick = false;
	}
	return false;
}//////////////////////////////////////////////


