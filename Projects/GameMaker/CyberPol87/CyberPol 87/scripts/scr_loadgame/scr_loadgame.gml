//Load function
//If don't exists a Save data, the game start on the first level of the game

if(file_exists("Save.ini"))
{
	ini_open("Save.ini");
	var LoadedRoom = ini_read_real("Save1","room",rm_Cutscene_Ospedale) - 1;
	ini_close();
	room_goto(LoadedRoom);
}
else
{
	room_goto(rm_Cutscene_Ospedale);
}
