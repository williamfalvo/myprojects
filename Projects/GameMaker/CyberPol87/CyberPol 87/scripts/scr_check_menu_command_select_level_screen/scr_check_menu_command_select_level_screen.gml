//Controlla device in utilizzo

enum device2 {
	keyboard2, pad2
}
///////////////////////////////////////////
if (!variable_global_exists("pad_stick2")) {
	global.pad_stick2 = false;
}//////////////////////////////////////////

if (keyboard_check_pressed(argument0)) {
	global.last_device = device2.keyboard2;
	return true;
}

else if (gamepad_button_check_pressed(0, argument1)) {
	global.last_device = device2.pad2;
	return true;
}
///////////////////////////////////////////////
else if (global.pad_stick2 == false)
{
	if (argument1 == gp_padr && gamepad_axis_value(0, gp_axislh) > 0.5) {
		global.last_device = device2.pad2;
		global.pad_stick2 = true;
		return true;	
	}

	else if (argument1 == gp_padl && gamepad_axis_value(0, gp_axislh) < -0.5) {
		global.last_device = device2.pad2;
		global.pad_stick2 = true;
		return true;
	}
}
else {
	if (abs(gamepad_axis_value(0, gp_axislh)) <= 0.5)
	{
		global.pad_stick2 = false;
	}
	return false;
}//////////////////////////////////////////////


