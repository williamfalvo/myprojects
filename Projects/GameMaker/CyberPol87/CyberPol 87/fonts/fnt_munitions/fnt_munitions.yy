{
    "id": "85d85f52-c0a7-4da8-9214-045af13c8d86",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_munitions",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "64818294-cade-453b-ba3a-0143f091d44b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 70,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 150,
                "y": 290
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "da9a5c82-48ec-4edb-a3a8-a73b4c4cf42e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 70,
                "offset": 4,
                "shift": 21,
                "w": 12,
                "x": 80,
                "y": 290
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2fa4f57d-7859-4233-bd34-8879c2ff7ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 70,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 318,
                "y": 218
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f9333e6e-ad74-48d2-8f9c-08778865127c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 70,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c2768b3d-01e8-4c85-afa8-c063716cf539",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 70,
                "offset": 5,
                "shift": 27,
                "w": 18,
                "x": 338,
                "y": 218
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "12f21122-45fb-417d-8ae7-cca63594617e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 70,
                "offset": 3,
                "shift": 44,
                "w": 39,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8ec39163-73e1-4301-b4f6-7c85e7094b0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 70,
                "offset": 5,
                "shift": 37,
                "w": 26,
                "x": 62,
                "y": 74
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d6aec0b6-7a76-4c77-b11d-82c6c6c66490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 70,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 250,
                "y": 290
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "971c2dfe-7dd0-4790-b6c8-82d1546ba2e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 70,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 65,
                "y": 290
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "647e4d51-eb17-421b-8ab0-58e79df6ed3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 70,
                "offset": 2,
                "shift": 18,
                "w": 13,
                "x": 50,
                "y": 290
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c2b75d9b-8019-4fe7-bbfc-58ae2ddb9ed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 70,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 358,
                "y": 218
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "70b2b66e-c71b-4c45-8040-bd322ac93260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 70,
                "offset": 5,
                "shift": 36,
                "w": 25,
                "x": 361,
                "y": 74
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "df541510-da2a-45ce-bc24-ce024c8668c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 70,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 108,
                "y": 290
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "30bed7ad-16a9-408c-b2ec-adf9410a6b1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 70,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 164,
                "y": 290
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a7a102ef-1ad4-465b-aa19-eb033e1d5c2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 70,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 94,
                "y": 290
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5502c302-1819-4957-819c-fd1d7f286122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 70,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 459,
                "y": 218
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "772e9d29-a0a6-4828-9d93-46e18a9ee845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 70,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 292,
                "y": 146
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c4a67610-fe54-4bc9-87b9-e228a5b7482c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 70,
                "offset": 9,
                "shift": 27,
                "w": 10,
                "x": 226,
                "y": 290
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a9311af7-9694-46bf-ad04-d5e5ca1098fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 70,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 445,
                "y": 146
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "76121a4b-cfb4-45aa-bfdf-eefd97af79d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 70,
                "offset": 5,
                "shift": 27,
                "w": 18,
                "x": 278,
                "y": 218
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7779e8f8-2ea5-4f90-91b5-c3c62af2cac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 70,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 83,
                "y": 146
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1a665f54-1d5b-441f-873b-a515fbed7799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 70,
                "offset": 4,
                "shift": 27,
                "w": 20,
                "x": 194,
                "y": 218
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "cf690b03-474e-4880-8e8c-f8622ae95101",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 70,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 415,
                "y": 74
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d52d211c-23d1-4afc-888e-df0c630c05a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 70,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 470,
                "y": 146
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3b95c50e-9c25-4f7e-b13c-367b9932a902",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 70,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 370,
                "y": 146
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "293e0518-d21c-4d8e-b982-9c4ba10d9bb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 70,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 56,
                "y": 146
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f0806422-ea49-4f30-a7da-46418f826b42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 70,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 122,
                "y": 290
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4b479f3e-ebe5-48c7-ba5c-9e3d3a2fb753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 70,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 136,
                "y": 290
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "54735f1f-1f52-4ede-b4c8-c03f0de6f190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 70,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 122,
                "y": 218
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f5fb7ca8-a87d-463a-8694-3e872445879f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 70,
                "offset": 5,
                "shift": 36,
                "w": 25,
                "x": 253,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4a6fd207-f073-42ff-91fd-6d5c774da1b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 70,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 146,
                "y": 218
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "cea51672-1347-4645-8add-9dac693a9fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 70,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 170,
                "y": 218
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4e3bf51d-6e58-4095-b213-7f1d7ebf3c50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 70,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "94d532a5-a71c-4cb8-b01d-b55f4efeabac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 70,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 344,
                "y": 146
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e8560eac-0269-4d0f-aad8-45a4ab508998",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 70,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 318,
                "y": 146
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c6942e70-f4cf-4acf-9be1-51b335d4966a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 70,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1b1b26c9-6a3c-4c4d-a9a4-00b80cbb470a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 70,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 331,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "05626427-3e82-40af-8e12-cde463500b02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 70,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 237,
                "y": 218
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f6f32e11-713e-40d5-944e-c3d3ba50d691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 70,
                "offset": 2,
                "shift": 23,
                "w": 23,
                "x": 420,
                "y": 146
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6536d7e3-73ef-4772-9abe-b03793f4c942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 70,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 455,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "178a1740-083a-4684-9202-cd28f93588d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 70,
                "offset": 3,
                "shift": 30,
                "w": 24,
                "x": 266,
                "y": 146
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "18b17eb0-9166-4fd9-90f8-04158934128d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 70,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 214,
                "y": 290
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a614f99f-53a2-4bbd-bd77-2f464e05a448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 70,
                "offset": -1,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 290
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6ae4932c-c8a4-4b81-9956-11520a0e2dc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 70,
                "offset": 3,
                "shift": 31,
                "w": 28,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "59289264-c161-4f9b-855f-a57035fe4f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 70,
                "offset": 2,
                "shift": 16,
                "w": 15,
                "x": 393,
                "y": 218
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "859460e1-f207-472d-9149-91b8971de2b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 70,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e48cb93b-0533-4937-ad46-a7ac203041e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 70,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 188,
                "y": 146
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4af892bd-886a-4afc-99ec-bb8035083290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 70,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "faed82be-4cd5-4213-9851-055cdb1baf99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 70,
                "offset": 2,
                "shift": 27,
                "w": 25,
                "x": 172,
                "y": 74
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5c3b384b-0a4d-4aab-a370-87e7fd66f128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 70,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "3cffc2ff-23c8-4625-8439-af5af8fd77ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 70,
                "offset": 3,
                "shift": 29,
                "w": 26,
                "x": 90,
                "y": 74
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "cb1763fe-d7ae-4012-8949-2cc4bc8e306a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 70,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 298,
                "y": 218
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "edbd53bc-fbd7-430c-abc4-47e124804d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 70,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 226,
                "y": 74
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "96f81ac2-6cc2-40b0-b9c5-c47d726baeda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 70,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 110,
                "y": 146
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "47301ec1-b43c-464e-bb08-8979cbc37782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 70,
                "offset": -2,
                "shift": 25,
                "w": 29,
                "x": 364,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d40c04bf-347c-4f67-a9d6-1f5a6bd8b11d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 70,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f47aa817-2c9f-4c90-a0a9-1be85c2ae170",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 70,
                "offset": -2,
                "shift": 24,
                "w": 28,
                "x": 395,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0651eaa6-0e15-4ea3-a18e-3a00815c6015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 70,
                "offset": -2,
                "shift": 24,
                "w": 28,
                "x": 425,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "df749c3a-f1bb-43ba-9d20-d5598c0a4ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 70,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 442,
                "y": 74
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8f5146bc-4f7c-46b9-babb-2d7cd496deab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 70,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 2,
                "y": 290
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c3439ee0-438a-43d1-abf4-398ca1da6a57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 70,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 491,
                "y": 218
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "225fad33-a30c-4710-b0a2-efb4983756f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 70,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 443,
                "y": 218
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a0b85b5b-c9a3-4255-bd3c-3409de1970e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 70,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 136,
                "y": 146
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7e53d3b4-a7a8-49c0-a189-c05ca5b0ef31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 70,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 162,
                "y": 146
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "49a49a0c-7ccf-4e01-88ab-eb1074a36412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 70,
                "offset": 5,
                "shift": 24,
                "w": 11,
                "x": 177,
                "y": 290
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ae7000c5-35a6-4ba8-90ce-bbc0184f7e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 70,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "eedadf16-b6bc-4a15-a78c-6a002788f9f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 70,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 29,
                "y": 146
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "fd391caf-4a5e-4aa1-bcab-763640c92b5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 70,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 118,
                "y": 74
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2e5306ea-f691-4e06-8566-2ac66b86ed9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 70,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 280,
                "y": 74
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1c805c7f-b8b5-4326-9b12-a569ece38240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 70,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 469,
                "y": 74
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4d7da243-262f-43fa-a07a-08bd36551a46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 70,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 18,
                "y": 290
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8d24a405-0628-4c9c-9ad0-8c82db465995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 70,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 388,
                "y": 74
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3813acc3-6e8a-4ed9-958b-c380c0531594",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 70,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 50,
                "y": 218
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "21aab72d-9951-4ca5-81f7-2aed900c2b9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 70,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 202,
                "y": 290
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a9930144-a326-4e30-abd0-fff91d03c357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 70,
                "offset": -4,
                "shift": 17,
                "w": 19,
                "x": 216,
                "y": 218
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "46c0f973-b3e4-4353-a137-1bc350380149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 70,
                "offset": 2,
                "shift": 26,
                "w": 25,
                "x": 307,
                "y": 74
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "eaf88509-73db-48f5-abc5-8bf6de4bf4d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 70,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 190,
                "y": 290
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "23565c1b-aaed-4e65-abfe-825ce887ef38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 70,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 296,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b28c71da-03b1-4c3c-b43c-533194f3ebbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 70,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 74,
                "y": 218
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "159cb823-14cc-4873-8417-74d189f4c360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 70,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 334,
                "y": 74
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1b422a4d-2b2e-4f37-9e50-150a0b989100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 70,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 199,
                "y": 74
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "034e51e1-c989-4551-9b9c-10d2983fa6a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 70,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 145,
                "y": 74
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "56605886-214f-40d5-a162-06ac52edd154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 70,
                "offset": 2,
                "shift": 15,
                "w": 14,
                "x": 475,
                "y": 218
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6df8e0e7-5958-4f4f-85da-dd86898ad2b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 70,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 258,
                "y": 218
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "7cbf8899-4161-4a61-b95c-504ff41f774b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 70,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 427,
                "y": 218
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3444be5b-f772-4929-bf9c-66b2d45df2d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 70,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 98,
                "y": 218
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3dbf7fc3-a032-4186-b29b-c56920a72cea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 70,
                "offset": -2,
                "shift": 19,
                "w": 23,
                "x": 395,
                "y": 146
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0b8a80fe-3173-4cb6-bc10-5355155871cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 70,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 261,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1a07ec85-25f6-4717-8877-54e03f168afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 70,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 2,
                "y": 218
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8125da5a-ccd4-4de6-917c-11208da73fad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 70,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 26,
                "y": 218
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d6c4bdc1-da59-4c9d-8bbf-d67f2ecc8cc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 70,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 214,
                "y": 146
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4bf80e79-f89d-4dc2-8af6-18f535977c38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 70,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 410,
                "y": 218
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c9767dee-16b6-4fcc-a4c1-a1a0454416c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 70,
                "offset": 7,
                "shift": 24,
                "w": 10,
                "x": 238,
                "y": 290
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2f6f6ac6-0320-44d0-aac4-c64b1b2fee7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 70,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 376,
                "y": 218
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "eece6b1f-ccf2-4718-b7dc-35b51bd6d5a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 70,
                "offset": 4,
                "shift": 32,
                "w": 24,
                "x": 240,
                "y": 146
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 36,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}