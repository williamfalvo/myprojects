{
    "id": "61e00855-f468-4340-b4ca-dc049dd082cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_right_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 16,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "debf1f23-a09a-4aed-af42-c70a0af33dfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61e00855-f468-4340-b4ca-dc049dd082cd",
            "compositeImage": {
                "id": "9c741716-a3f9-4a45-9067-eb33dad24209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "debf1f23-a09a-4aed-af42-c70a0af33dfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69d2bc0b-e26a-4d3f-a2a0-c48799678426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "debf1f23-a09a-4aed-af42-c70a0af33dfa",
                    "LayerId": "668fc21d-4708-480b-8db3-4258cddba8e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "668fc21d-4708-480b-8db3-4258cddba8e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61e00855-f468-4340-b4ca-dc049dd082cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}