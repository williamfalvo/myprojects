{
    "id": "b785504b-6f75-45c1-9d07-692076ee0bb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 38,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2975c145-81b7-452e-8a20-724142982529",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785504b-6f75-45c1-9d07-692076ee0bb0",
            "compositeImage": {
                "id": "4b20d92a-4086-43f0-b7c4-cd50fcd4f707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2975c145-81b7-452e-8a20-724142982529",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "535b668a-5517-4e57-bd08-3f2a7e6b0d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2975c145-81b7-452e-8a20-724142982529",
                    "LayerId": "9c60943a-6edf-4234-8737-c5f767286457"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9c60943a-6edf-4234-8737-c5f767286457",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b785504b-6f75-45c1-9d07-692076ee0bb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}