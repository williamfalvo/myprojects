{
    "id": "08e9b063-3c57-46c9-a3d4-f896f24e5634",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 8,
    "bbox_right": 53,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f81df5e-47df-49c4-979e-69d76848dc78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08e9b063-3c57-46c9-a3d4-f896f24e5634",
            "compositeImage": {
                "id": "8072a681-190a-4bbe-aa90-90073ef9949b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f81df5e-47df-49c4-979e-69d76848dc78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b2b4cbe-7d3e-4fcb-8c15-af3ef00afdbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f81df5e-47df-49c4-979e-69d76848dc78",
                    "LayerId": "d1eee2f0-dcfe-4e2f-817a-758825f79121"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d1eee2f0-dcfe-4e2f-817a-758825f79121",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08e9b063-3c57-46c9-a3d4-f896f24e5634",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}