{
    "id": "68b00d8b-6220-45b1-8ba1-f3834493cac2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_E",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c010bb00-db06-4ead-b7b0-d9cc3c14cccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68b00d8b-6220-45b1-8ba1-f3834493cac2",
            "compositeImage": {
                "id": "0966253a-7810-4761-ad2e-f8acc64617fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c010bb00-db06-4ead-b7b0-d9cc3c14cccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc24c8b-f283-4cb7-9654-1eb0920035b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c010bb00-db06-4ead-b7b0-d9cc3c14cccf",
                    "LayerId": "292e5c46-890c-4e34-a805-ee5fb1b4d164"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "292e5c46-890c-4e34-a805-ee5fb1b4d164",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68b00d8b-6220-45b1-8ba1-f3834493cac2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}