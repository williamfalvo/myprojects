{
    "id": "ccb5b25d-c867-4c20-9cf8-e589dc754838",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Rifornimenti",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d71b218-59c0-4f1c-ae22-c09c0f7f5bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccb5b25d-c867-4c20-9cf8-e589dc754838",
            "compositeImage": {
                "id": "e4f36cc0-982e-4e0d-a7e4-b97aca62a301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d71b218-59c0-4f1c-ae22-c09c0f7f5bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82810197-adc9-43ee-a4d6-e6dfa71b23d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d71b218-59c0-4f1c-ae22-c09c0f7f5bfd",
                    "LayerId": "98d197ad-727b-485f-9fd9-21a2c702b2c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "98d197ad-727b-485f-9fd9-21a2c702b2c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccb5b25d-c867-4c20-9cf8-e589dc754838",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}