{
    "id": "775877b5-e4fe-4ff1-9c54-b0cd45c5d269",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controls_gamepad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3611111-8744-4055-a227-6168805097ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "775877b5-e4fe-4ff1-9c54-b0cd45c5d269",
            "compositeImage": {
                "id": "380094bc-851f-4025-93a4-69f85414d684",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3611111-8744-4055-a227-6168805097ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12084932-e9ce-49ee-ad18-77213fb7db9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3611111-8744-4055-a227-6168805097ff",
                    "LayerId": "738fb4e9-bc26-4de3-afbb-3bc57fd543f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "738fb4e9-bc26-4de3-afbb-3bc57fd543f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "775877b5-e4fe-4ff1-9c54-b0cd45c5d269",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 180
}