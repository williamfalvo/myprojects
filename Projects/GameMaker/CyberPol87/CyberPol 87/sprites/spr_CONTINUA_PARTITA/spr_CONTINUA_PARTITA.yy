{
    "id": "3740708f-0d0b-4725-91fd-ba316f815921",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CONTINUA_PARTITA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 17,
    "bbox_right": 232,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49c5bd55-7137-474d-87e5-c94c606ee09c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3740708f-0d0b-4725-91fd-ba316f815921",
            "compositeImage": {
                "id": "7e9b58e1-1057-46fc-b5d5-074c3a497087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49c5bd55-7137-474d-87e5-c94c606ee09c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca049769-deaa-4caf-842b-f5da775e72c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49c5bd55-7137-474d-87e5-c94c606ee09c",
                    "LayerId": "d11331f7-e4a2-41da-8948-6f68a1937d7b"
                }
            ]
        },
        {
            "id": "db7026ae-4c82-4d4b-b76e-d7311282c095",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3740708f-0d0b-4725-91fd-ba316f815921",
            "compositeImage": {
                "id": "310a3cd9-ce77-4852-904f-b4ca3fd6d193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db7026ae-4c82-4d4b-b76e-d7311282c095",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1edd9b-2882-4d74-9f36-d4042d78c22d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db7026ae-4c82-4d4b-b76e-d7311282c095",
                    "LayerId": "d11331f7-e4a2-41da-8948-6f68a1937d7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "d11331f7-e4a2-41da-8948-6f68a1937d7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3740708f-0d0b-4725-91fd-ba316f815921",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 245,
    "xorig": 122,
    "yorig": 30
}