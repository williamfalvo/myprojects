{
    "id": "cf3ee67c-7417-4fee-af0c-b0f167b16d1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CUTSCENE_OSPEDALE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a836aab-f423-4c27-b4f6-1c6df24d532f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf3ee67c-7417-4fee-af0c-b0f167b16d1d",
            "compositeImage": {
                "id": "88aaab6d-5429-4ddf-9031-8f962b73f903",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a836aab-f423-4c27-b4f6-1c6df24d532f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c98f87bf-3030-45f0-8120-275e6c049313",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a836aab-f423-4c27-b4f6-1c6df24d532f",
                    "LayerId": "ef11e3be-2dcc-4cc3-a487-d941a79e4614"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "ef11e3be-2dcc-4cc3-a487-d941a79e4614",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf3ee67c-7417-4fee-af0c-b0f167b16d1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}