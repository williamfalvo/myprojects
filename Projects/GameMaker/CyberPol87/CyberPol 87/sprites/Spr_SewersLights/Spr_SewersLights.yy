{
    "id": "fb072218-21bf-42a3-b89f-82e3ad3dddf7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_SewersLights",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b38691e4-8d97-47b6-85fb-7e6b22a7741b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb072218-21bf-42a3-b89f-82e3ad3dddf7",
            "compositeImage": {
                "id": "cc9411ca-e6dd-4545-b9ef-9b932d407d23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b38691e4-8d97-47b6-85fb-7e6b22a7741b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "626687d8-437d-41c6-8c0f-133ed4ac523d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b38691e4-8d97-47b6-85fb-7e6b22a7741b",
                    "LayerId": "34883a98-b21c-47a4-915a-5eacbda9b2f4"
                }
            ]
        },
        {
            "id": "93202356-fe98-41ea-b484-2e0c135bd5d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb072218-21bf-42a3-b89f-82e3ad3dddf7",
            "compositeImage": {
                "id": "fc9aea21-350e-4369-ae33-45c902b60998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93202356-fe98-41ea-b484-2e0c135bd5d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70c3b805-fe07-4387-ae44-8a01cba313b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93202356-fe98-41ea-b484-2e0c135bd5d2",
                    "LayerId": "34883a98-b21c-47a4-915a-5eacbda9b2f4"
                }
            ]
        },
        {
            "id": "f7ed12b5-871a-4cba-9b50-190375380008",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb072218-21bf-42a3-b89f-82e3ad3dddf7",
            "compositeImage": {
                "id": "4b74b93d-ff10-4019-b1c5-023c704c6aa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ed12b5-871a-4cba-9b50-190375380008",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7413968b-798d-4699-8c8c-cdad31f413e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ed12b5-871a-4cba-9b50-190375380008",
                    "LayerId": "34883a98-b21c-47a4-915a-5eacbda9b2f4"
                }
            ]
        },
        {
            "id": "3b34790a-b0ca-477b-8b53-43d761240cf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb072218-21bf-42a3-b89f-82e3ad3dddf7",
            "compositeImage": {
                "id": "d4e6c131-fc54-46a2-86af-1fc5afa2a02e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b34790a-b0ca-477b-8b53-43d761240cf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac545ebd-5195-4633-ab1f-ed945913295a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b34790a-b0ca-477b-8b53-43d761240cf6",
                    "LayerId": "34883a98-b21c-47a4-915a-5eacbda9b2f4"
                }
            ]
        },
        {
            "id": "4487ac70-ff1b-4ef6-bcba-a2e833cd507b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb072218-21bf-42a3-b89f-82e3ad3dddf7",
            "compositeImage": {
                "id": "d1d9e183-4d36-41d4-bde5-a46325fa7b49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4487ac70-ff1b-4ef6-bcba-a2e833cd507b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82a56856-4697-49d3-aef6-4d8ebf4e6769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4487ac70-ff1b-4ef6-bcba-a2e833cd507b",
                    "LayerId": "34883a98-b21c-47a4-915a-5eacbda9b2f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "34883a98-b21c-47a4-915a-5eacbda9b2f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb072218-21bf-42a3-b89f-82e3ad3dddf7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}