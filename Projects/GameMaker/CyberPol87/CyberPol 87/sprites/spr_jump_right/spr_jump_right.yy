{
    "id": "6e49e9ec-03ce-4446-8943-5c41a1954625",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7eaec63c-ec06-45c9-916a-7c6036f32daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e49e9ec-03ce-4446-8943-5c41a1954625",
            "compositeImage": {
                "id": "5ff9f157-1a0c-4186-9198-6e204d8087bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eaec63c-ec06-45c9-916a-7c6036f32daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75a3577b-559f-443d-a154-2aef81b88045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eaec63c-ec06-45c9-916a-7c6036f32daf",
                    "LayerId": "58e8a48d-2702-408c-afbf-5596c2be4377"
                }
            ]
        },
        {
            "id": "2c8c4087-7f40-4097-960f-c63121c82ada",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e49e9ec-03ce-4446-8943-5c41a1954625",
            "compositeImage": {
                "id": "8b8ec24f-434f-49c8-885f-1353919d3226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c8c4087-7f40-4097-960f-c63121c82ada",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52c8d5e4-046f-4215-b64f-1706f2bc2e7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c8c4087-7f40-4097-960f-c63121c82ada",
                    "LayerId": "58e8a48d-2702-408c-afbf-5596c2be4377"
                }
            ]
        },
        {
            "id": "d452008a-e469-4068-9924-cb473a1ef6c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e49e9ec-03ce-4446-8943-5c41a1954625",
            "compositeImage": {
                "id": "374e806e-43fe-42b0-ab20-de69f85de114",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d452008a-e469-4068-9924-cb473a1ef6c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f3a4641-ab60-4a58-bd46-cfeb1949b78c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d452008a-e469-4068-9924-cb473a1ef6c6",
                    "LayerId": "58e8a48d-2702-408c-afbf-5596c2be4377"
                }
            ]
        },
        {
            "id": "907f3404-38cd-4b67-9231-09b6be7de3de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e49e9ec-03ce-4446-8943-5c41a1954625",
            "compositeImage": {
                "id": "6b4d1dbd-7f9e-4182-b923-642deca25779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "907f3404-38cd-4b67-9231-09b6be7de3de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6196d9d1-0908-43a3-936a-b70a81866f46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "907f3404-38cd-4b67-9231-09b6be7de3de",
                    "LayerId": "58e8a48d-2702-408c-afbf-5596c2be4377"
                }
            ]
        },
        {
            "id": "c97c9436-e7aa-40e1-aa0c-e37279711db9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e49e9ec-03ce-4446-8943-5c41a1954625",
            "compositeImage": {
                "id": "0863d925-99bf-46b7-a045-e723d48c12fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c97c9436-e7aa-40e1-aa0c-e37279711db9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b853595f-6bde-4162-910e-e5169068f312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c97c9436-e7aa-40e1-aa0c-e37279711db9",
                    "LayerId": "58e8a48d-2702-408c-afbf-5596c2be4377"
                }
            ]
        },
        {
            "id": "c3dd4197-232c-48a4-a745-44a7080df13a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e49e9ec-03ce-4446-8943-5c41a1954625",
            "compositeImage": {
                "id": "2031fbe6-68da-4647-8c0f-164dede328c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3dd4197-232c-48a4-a745-44a7080df13a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b253a56-e070-45eb-a61b-dca1db1ae5dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3dd4197-232c-48a4-a745-44a7080df13a",
                    "LayerId": "58e8a48d-2702-408c-afbf-5596c2be4377"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "58e8a48d-2702-408c-afbf-5596c2be4377",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e49e9ec-03ce-4446-8943-5c41a1954625",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}