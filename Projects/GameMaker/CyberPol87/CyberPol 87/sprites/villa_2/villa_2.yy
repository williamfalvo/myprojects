{
    "id": "8c149365-7364-478f-b99a-575bdc025af3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "villa_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 927,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0498f61e-ad3c-4b7a-8ea3-6ece058e1adc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c149365-7364-478f-b99a-575bdc025af3",
            "compositeImage": {
                "id": "399d1e5c-d2a4-4f7b-9a6e-6bf493c2e2bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0498f61e-ad3c-4b7a-8ea3-6ece058e1adc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f72c57b3-dde3-41f9-b5f5-063a198b2710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0498f61e-ad3c-4b7a-8ea3-6ece058e1adc",
                    "LayerId": "fa918843-6410-4817-9062-b40b6ab89c27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 992,
    "layers": [
        {
            "id": "fa918843-6410-4817-9062-b40b6ab89c27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c149365-7364-478f-b99a-575bdc025af3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}