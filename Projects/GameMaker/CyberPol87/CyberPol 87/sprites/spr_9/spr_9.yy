{
    "id": "eda321b6-a8ad-4728-a5ba-038e6821e427",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6ab09f6-6a6c-4feb-8ded-dcd6971a06ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eda321b6-a8ad-4728-a5ba-038e6821e427",
            "compositeImage": {
                "id": "669c8704-25c0-44ef-91d4-164f615aa856",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6ab09f6-6a6c-4feb-8ded-dcd6971a06ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9eb6166-e58e-4440-8a9d-28e075664e8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6ab09f6-6a6c-4feb-8ded-dcd6971a06ea",
                    "LayerId": "2d56e3c6-0aa3-411d-b389-3e7cf7f11dfd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "2d56e3c6-0aa3-411d-b389-3e7cf7f11dfd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eda321b6-a8ad-4728-a5ba-038e6821e427",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 9
}