{
    "id": "a712364e-f72b-4725-9e09-e890ff1015c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_munitions",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 59,
    "bbox_right": 196,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "319d4bd5-70dd-465d-8f66-dd2d6e2b040d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a712364e-f72b-4725-9e09-e890ff1015c8",
            "compositeImage": {
                "id": "5328753b-aacc-4b6b-8452-2e4b8fcb24f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "319d4bd5-70dd-465d-8f66-dd2d6e2b040d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19236738-e9f1-49fc-a5cb-215081c80bd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "319d4bd5-70dd-465d-8f66-dd2d6e2b040d",
                    "LayerId": "534b785d-d093-4f43-bfd9-7608c5499e72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "534b785d-d093-4f43-bfd9-7608c5499e72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a712364e-f72b-4725-9e09-e890ff1015c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 16
}