{
    "id": "45f2a226-a905-47be-9dce-b23dc691533b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Cutscene_Finale",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56452c43-bcfd-43de-88ef-6e77f8f3724d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f2a226-a905-47be-9dce-b23dc691533b",
            "compositeImage": {
                "id": "dea01234-62df-46f9-b7e7-767bd5032a0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56452c43-bcfd-43de-88ef-6e77f8f3724d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "febb4000-8c94-4a79-8b35-a3bb6d1b4566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56452c43-bcfd-43de-88ef-6e77f8f3724d",
                    "LayerId": "fbc543af-9a8d-4849-9123-5effab319c65"
                }
            ]
        },
        {
            "id": "2352b564-e9e1-4ea4-b0d4-a4863a9265b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f2a226-a905-47be-9dce-b23dc691533b",
            "compositeImage": {
                "id": "4961b47e-11ed-4f3f-8b9b-42d17ca7bab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2352b564-e9e1-4ea4-b0d4-a4863a9265b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "681e7da9-c8cb-4456-929e-965425cd1a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2352b564-e9e1-4ea4-b0d4-a4863a9265b7",
                    "LayerId": "fbc543af-9a8d-4849-9123-5effab319c65"
                }
            ]
        },
        {
            "id": "a444c0bf-56cd-4034-915a-d4548264471f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f2a226-a905-47be-9dce-b23dc691533b",
            "compositeImage": {
                "id": "d0f14404-23b7-4fdb-98a7-e41679b53c5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a444c0bf-56cd-4034-915a-d4548264471f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12ab7a43-20ff-422e-af90-f48293ac307b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a444c0bf-56cd-4034-915a-d4548264471f",
                    "LayerId": "fbc543af-9a8d-4849-9123-5effab319c65"
                }
            ]
        },
        {
            "id": "4fe9e7f1-deda-433b-a339-40d3531872d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f2a226-a905-47be-9dce-b23dc691533b",
            "compositeImage": {
                "id": "f415107c-04f3-433b-b6f0-00e41b410123",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fe9e7f1-deda-433b-a339-40d3531872d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92043ad7-98fe-4d92-9a18-753755a2c849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fe9e7f1-deda-433b-a339-40d3531872d2",
                    "LayerId": "fbc543af-9a8d-4849-9123-5effab319c65"
                }
            ]
        },
        {
            "id": "638ad379-0f03-449d-83f0-cfc8a6b61545",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f2a226-a905-47be-9dce-b23dc691533b",
            "compositeImage": {
                "id": "2c159363-ca29-4072-8e0d-60eab2102202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "638ad379-0f03-449d-83f0-cfc8a6b61545",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64f3c3cd-c350-43d3-8ceb-10d2e9b82931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "638ad379-0f03-449d-83f0-cfc8a6b61545",
                    "LayerId": "fbc543af-9a8d-4849-9123-5effab319c65"
                }
            ]
        },
        {
            "id": "822fe0c9-2bfe-48ba-ad84-e9987e4a8c21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f2a226-a905-47be-9dce-b23dc691533b",
            "compositeImage": {
                "id": "64524b59-bd05-4f10-b43d-1ccfb358bbae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "822fe0c9-2bfe-48ba-ad84-e9987e4a8c21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b62a77e9-ed7c-4c61-94cd-660845251686",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "822fe0c9-2bfe-48ba-ad84-e9987e4a8c21",
                    "LayerId": "fbc543af-9a8d-4849-9123-5effab319c65"
                }
            ]
        },
        {
            "id": "dbb73e9a-27f3-47b8-9cb5-22ba0fa2cdea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f2a226-a905-47be-9dce-b23dc691533b",
            "compositeImage": {
                "id": "0acd3fd0-cd98-46d3-a1d7-cce2ee4db18a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbb73e9a-27f3-47b8-9cb5-22ba0fa2cdea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fe2a77e-cc3b-43ce-85ca-857f1e40b57c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbb73e9a-27f3-47b8-9cb5-22ba0fa2cdea",
                    "LayerId": "fbc543af-9a8d-4849-9123-5effab319c65"
                }
            ]
        },
        {
            "id": "e3606488-e42a-4da7-929b-ac3dd8db9097",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f2a226-a905-47be-9dce-b23dc691533b",
            "compositeImage": {
                "id": "19715123-8bb9-4958-8045-5ab145df2c9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3606488-e42a-4da7-929b-ac3dd8db9097",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde1006c-f6eb-4c36-a560-9ed07c8a454d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3606488-e42a-4da7-929b-ac3dd8db9097",
                    "LayerId": "fbc543af-9a8d-4849-9123-5effab319c65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "fbc543af-9a8d-4849-9123-5effab319c65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45f2a226-a905-47be-9dce-b23dc691533b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 180
}