{
    "id": "744a3633-17ab-4ba4-8f07-a5753331eadc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_rightUp_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fbad6e00-12fb-475b-aa78-7b39ba436d93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "744a3633-17ab-4ba4-8f07-a5753331eadc",
            "compositeImage": {
                "id": "fe83aa08-ec5b-4485-9a80-377696000688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbad6e00-12fb-475b-aa78-7b39ba436d93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "732c5eb1-74d6-48c5-852d-894196117543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbad6e00-12fb-475b-aa78-7b39ba436d93",
                    "LayerId": "9c71625f-2fa8-4379-af3f-f0122c4118da"
                }
            ]
        },
        {
            "id": "647e0b20-2dbc-444c-bf59-120fcbf45dba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "744a3633-17ab-4ba4-8f07-a5753331eadc",
            "compositeImage": {
                "id": "c813f450-ba1d-4b7b-a223-72e6554ebd29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647e0b20-2dbc-444c-bf59-120fcbf45dba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a811942-5388-45b1-bd46-55bfdc480519",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647e0b20-2dbc-444c-bf59-120fcbf45dba",
                    "LayerId": "9c71625f-2fa8-4379-af3f-f0122c4118da"
                }
            ]
        },
        {
            "id": "00b25a46-eeed-4c31-8f83-3b0465a91180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "744a3633-17ab-4ba4-8f07-a5753331eadc",
            "compositeImage": {
                "id": "87297f89-7b61-457d-b6db-a1ce317c2f73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00b25a46-eeed-4c31-8f83-3b0465a91180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d7252cb-786d-40a7-8d24-54211cd9a065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00b25a46-eeed-4c31-8f83-3b0465a91180",
                    "LayerId": "9c71625f-2fa8-4379-af3f-f0122c4118da"
                }
            ]
        },
        {
            "id": "23797c2f-052b-4e27-85e3-dae1a174c7b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "744a3633-17ab-4ba4-8f07-a5753331eadc",
            "compositeImage": {
                "id": "96880cb8-f2da-42e1-9f87-da53051dedc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23797c2f-052b-4e27-85e3-dae1a174c7b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "709f24cd-7922-45fa-a4f5-4f2c00f4bb98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23797c2f-052b-4e27-85e3-dae1a174c7b7",
                    "LayerId": "9c71625f-2fa8-4379-af3f-f0122c4118da"
                }
            ]
        },
        {
            "id": "a4a96045-29ca-44e7-a517-6ce15ee408ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "744a3633-17ab-4ba4-8f07-a5753331eadc",
            "compositeImage": {
                "id": "1e209d5d-867f-44a4-bf89-0c82e30da9e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4a96045-29ca-44e7-a517-6ce15ee408ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7288fffe-360f-495a-97c8-29f9d9887bd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4a96045-29ca-44e7-a517-6ce15ee408ce",
                    "LayerId": "9c71625f-2fa8-4379-af3f-f0122c4118da"
                }
            ]
        },
        {
            "id": "74133a6e-a130-4c00-a66f-7919e3342cef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "744a3633-17ab-4ba4-8f07-a5753331eadc",
            "compositeImage": {
                "id": "1753e7f4-3cb3-4239-b91b-f68976c836de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74133a6e-a130-4c00-a66f-7919e3342cef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "625760b3-7cc4-4764-8684-2fb6aa4f6039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74133a6e-a130-4c00-a66f-7919e3342cef",
                    "LayerId": "9c71625f-2fa8-4379-af3f-f0122c4118da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "9c71625f-2fa8-4379-af3f-f0122c4118da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "744a3633-17ab-4ba4-8f07-a5753331eadc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}