{
    "id": "e631297c-4ae0-4e29-a364-e6a598169084",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exposion_boss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 59,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d5424ec-2b8e-4291-9b1c-a376d02d5884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e631297c-4ae0-4e29-a364-e6a598169084",
            "compositeImage": {
                "id": "c47209f2-8c5a-412e-a7f1-de7fae4e883d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d5424ec-2b8e-4291-9b1c-a376d02d5884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f07b128-3cbd-471a-a77e-49bc431e64a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d5424ec-2b8e-4291-9b1c-a376d02d5884",
                    "LayerId": "517bbbbc-1ad4-428d-aee3-1ec2d94e310b"
                }
            ]
        },
        {
            "id": "e7112b09-04dd-4e92-963c-e76814c59f71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e631297c-4ae0-4e29-a364-e6a598169084",
            "compositeImage": {
                "id": "67d39271-81b0-45b7-899e-2d2dc4894239",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7112b09-04dd-4e92-963c-e76814c59f71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e55d92d1-a17a-4991-8a90-0d90b57664a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7112b09-04dd-4e92-963c-e76814c59f71",
                    "LayerId": "517bbbbc-1ad4-428d-aee3-1ec2d94e310b"
                }
            ]
        },
        {
            "id": "1ef72af7-be21-43e9-8adb-c58da560fb41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e631297c-4ae0-4e29-a364-e6a598169084",
            "compositeImage": {
                "id": "28814ad2-74dd-4b78-bade-0eeed32d047b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ef72af7-be21-43e9-8adb-c58da560fb41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caa22d50-ae3a-42b6-aa8a-19ccb0dc1f28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ef72af7-be21-43e9-8adb-c58da560fb41",
                    "LayerId": "517bbbbc-1ad4-428d-aee3-1ec2d94e310b"
                }
            ]
        },
        {
            "id": "15f27a37-294b-41fe-aa67-3edac22d8b29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e631297c-4ae0-4e29-a364-e6a598169084",
            "compositeImage": {
                "id": "83a785bb-73bd-44d1-be4c-10d006dec5df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15f27a37-294b-41fe-aa67-3edac22d8b29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cd90bef-fc99-4a85-848c-71833ed83079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15f27a37-294b-41fe-aa67-3edac22d8b29",
                    "LayerId": "517bbbbc-1ad4-428d-aee3-1ec2d94e310b"
                }
            ]
        },
        {
            "id": "84f86a89-163f-4604-9cf5-ec3e2e91c237",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e631297c-4ae0-4e29-a364-e6a598169084",
            "compositeImage": {
                "id": "7d475bc6-7e4d-49d2-a113-a533ff826fea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84f86a89-163f-4604-9cf5-ec3e2e91c237",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da056145-a3f6-442b-8686-6e9d6b519002",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84f86a89-163f-4604-9cf5-ec3e2e91c237",
                    "LayerId": "517bbbbc-1ad4-428d-aee3-1ec2d94e310b"
                }
            ]
        },
        {
            "id": "76378cc1-0c3e-43d3-ab2d-93410d83d934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e631297c-4ae0-4e29-a364-e6a598169084",
            "compositeImage": {
                "id": "db7007d5-e2d0-4193-ac0e-643156d6c5eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76378cc1-0c3e-43d3-ab2d-93410d83d934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb2effa2-7bfa-40aa-97e8-edea6a043a9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76378cc1-0c3e-43d3-ab2d-93410d83d934",
                    "LayerId": "517bbbbc-1ad4-428d-aee3-1ec2d94e310b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "517bbbbc-1ad4-428d-aee3-1ec2d94e310b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e631297c-4ae0-4e29-a364-e6a598169084",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}