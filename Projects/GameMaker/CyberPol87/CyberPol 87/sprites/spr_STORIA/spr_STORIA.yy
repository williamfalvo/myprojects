{
    "id": "40f0e7c8-614a-426e-a5e6-8ae8b53710f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_STORIA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 13,
    "bbox_right": 92,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9adfed9c-cc08-4690-a60a-751cf8cc39a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40f0e7c8-614a-426e-a5e6-8ae8b53710f5",
            "compositeImage": {
                "id": "e0c03a9c-ff2b-4004-8c56-2319b9cce6d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9adfed9c-cc08-4690-a60a-751cf8cc39a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd8e2a4f-0e1f-4d40-829f-bdd5dced4822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9adfed9c-cc08-4690-a60a-751cf8cc39a2",
                    "LayerId": "3cccb389-6658-4b55-b218-3139c1851599"
                }
            ]
        },
        {
            "id": "0f7dd95b-f18f-444d-b746-31a1a38383f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40f0e7c8-614a-426e-a5e6-8ae8b53710f5",
            "compositeImage": {
                "id": "84606c81-7600-4cac-882f-7978b8674763",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f7dd95b-f18f-444d-b746-31a1a38383f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "053fff05-8d55-4654-8826-03e192e9b32d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f7dd95b-f18f-444d-b746-31a1a38383f5",
                    "LayerId": "3cccb389-6658-4b55-b218-3139c1851599"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "3cccb389-6658-4b55-b218-3139c1851599",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40f0e7c8-614a-426e-a5e6-8ae8b53710f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 106,
    "xorig": 53,
    "yorig": 25
}