{
    "id": "896d35a1-d74b-4d6e-b837-c4906af80b5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle_right_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 17,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79b3250a-6ecd-407c-aa51-96a41ae36c20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "896d35a1-d74b-4d6e-b837-c4906af80b5b",
            "compositeImage": {
                "id": "ff8330dc-a253-4e3b-a6f2-f5918884b5d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79b3250a-6ecd-407c-aa51-96a41ae36c20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c85e7cbe-55a0-4371-b1fd-19560f128a1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79b3250a-6ecd-407c-aa51-96a41ae36c20",
                    "LayerId": "61f8242e-b340-4cdf-a5f7-becfed5d21bf"
                }
            ]
        },
        {
            "id": "ce6a4eb4-4d61-4324-be6f-f8650ad9e210",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "896d35a1-d74b-4d6e-b837-c4906af80b5b",
            "compositeImage": {
                "id": "0fccde75-91aa-4735-8f54-b2fbf33c6e67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce6a4eb4-4d61-4324-be6f-f8650ad9e210",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0ac7a24-cf06-494e-b4f9-679d7942f84a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce6a4eb4-4d61-4324-be6f-f8650ad9e210",
                    "LayerId": "61f8242e-b340-4cdf-a5f7-becfed5d21bf"
                }
            ]
        },
        {
            "id": "f652cfb5-a1e9-4abd-81b6-6fb34ecb7861",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "896d35a1-d74b-4d6e-b837-c4906af80b5b",
            "compositeImage": {
                "id": "ddd0fbc0-8e16-42b6-afe3-9c6a105813eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f652cfb5-a1e9-4abd-81b6-6fb34ecb7861",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93689474-f441-4cfa-abea-0c4c7d29acd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f652cfb5-a1e9-4abd-81b6-6fb34ecb7861",
                    "LayerId": "61f8242e-b340-4cdf-a5f7-becfed5d21bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "61f8242e-b340-4cdf-a5f7-becfed5d21bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "896d35a1-d74b-4d6e-b837-c4906af80b5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}