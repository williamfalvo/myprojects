{
    "id": "c147ac0c-52ec-4584-925c-2269e62524e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Ricarica_Scudo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca6f5004-f24f-4efb-8eb1-36077acc1b19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c147ac0c-52ec-4584-925c-2269e62524e5",
            "compositeImage": {
                "id": "3ab1c0b2-0794-43c2-97f2-51df4727c0e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca6f5004-f24f-4efb-8eb1-36077acc1b19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b38fd084-466e-4fda-b79f-64ab92697e4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca6f5004-f24f-4efb-8eb1-36077acc1b19",
                    "LayerId": "6693c2ad-e358-43b4-ae89-9d91db31534e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "6693c2ad-e358-43b4-ae89-9d91db31534e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c147ac0c-52ec-4584-925c-2269e62524e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}