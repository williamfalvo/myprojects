{
    "id": "80cc80f5-04ef-4193-ab27-b0fae07546ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Scudo_Pad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07ccb734-69b3-421c-b522-945653f4244e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80cc80f5-04ef-4193-ab27-b0fae07546ab",
            "compositeImage": {
                "id": "430bb9d7-6a41-49fa-8be4-6fbc7c01eb69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07ccb734-69b3-421c-b522-945653f4244e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c73ac68-6b97-4a6a-ba46-6c0d0a5d3dca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07ccb734-69b3-421c-b522-945653f4244e",
                    "LayerId": "23468038-1671-4397-8b06-93c8fab3ffe1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "23468038-1671-4397-8b06-93c8fab3ffe1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80cc80f5-04ef-4193-ab27-b0fae07546ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}