{
    "id": "3932fdb7-0bb5-4712-a474-a4d89354e714",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 2,
    "bbox_right": 60,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ea73ee6-6563-429f-8c3c-ad64bf76e468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3932fdb7-0bb5-4712-a474-a4d89354e714",
            "compositeImage": {
                "id": "f61bceab-8e94-4e5c-9f4e-9a9a59b840ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea73ee6-6563-429f-8c3c-ad64bf76e468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "424c938a-8771-485f-bcd3-716fafb14d68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea73ee6-6563-429f-8c3c-ad64bf76e468",
                    "LayerId": "08006629-0011-4ffa-9d4f-1100fbe73494"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "08006629-0011-4ffa-9d4f-1100fbe73494",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3932fdb7-0bb5-4712-a474-a4d89354e714",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}