{
    "id": "ce63142e-de7e-4905-8196-1e259ab7164c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tombino",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 17,
    "bbox_right": 47,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1c33f50-89b8-4254-880b-754c9490641a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce63142e-de7e-4905-8196-1e259ab7164c",
            "compositeImage": {
                "id": "5931d0d3-1c7c-44f4-9fe2-3a7beeb0e2b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1c33f50-89b8-4254-880b-754c9490641a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb328080-e0b2-4636-91b9-fc4bdf08babf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1c33f50-89b8-4254-880b-754c9490641a",
                    "LayerId": "4b3d422b-39b1-4f12-9821-494c536871cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4b3d422b-39b1-4f12-9821-494c536871cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce63142e-de7e-4905-8196-1e259ab7164c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}