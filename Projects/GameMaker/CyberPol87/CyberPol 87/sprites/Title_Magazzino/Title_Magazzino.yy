{
    "id": "ec88879b-412f-43df-b3f8-c461484a5db5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Title_Magazzino",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 14,
    "bbox_right": 104,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2c54d9b-82b4-4ff0-8143-14f2a954bf71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec88879b-412f-43df-b3f8-c461484a5db5",
            "compositeImage": {
                "id": "b56de9e3-4228-4140-818c-748f51755fad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2c54d9b-82b4-4ff0-8143-14f2a954bf71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c68d8445-b293-4a5c-a0db-94f69072e41f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2c54d9b-82b4-4ff0-8143-14f2a954bf71",
                    "LayerId": "90d540f1-8662-4821-9935-ffc152f63e2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "90d540f1-8662-4821-9935-ffc152f63e2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec88879b-412f-43df-b3f8-c461484a5db5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 12
}