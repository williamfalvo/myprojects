{
    "id": "f263274c-4f20-4cae-8147-b4b11bb56ea3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_B",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9454a758-56c0-4e9d-958a-3fbd9ad83cf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f263274c-4f20-4cae-8147-b4b11bb56ea3",
            "compositeImage": {
                "id": "199ade30-c23e-4cd3-b418-613463e48dbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9454a758-56c0-4e9d-958a-3fbd9ad83cf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a486e57d-c830-48ff-9e2a-e1287b734a3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9454a758-56c0-4e9d-958a-3fbd9ad83cf7",
                    "LayerId": "a696f2e0-1a83-4a25-9fea-8f10309a64a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a696f2e0-1a83-4a25-9fea-8f10309a64a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f263274c-4f20-4cae-8147-b4b11bb56ea3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}