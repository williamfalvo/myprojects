{
    "id": "6c740754-7d39-4550-841f-1066b2d07823",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_testo_finale1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 20,
    "bbox_right": 570,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c92faa29-c8b6-4d2c-851f-cb1df47434ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c740754-7d39-4550-841f-1066b2d07823",
            "compositeImage": {
                "id": "a625aca4-5ff1-49c5-8187-ffe223b9a315",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c92faa29-c8b6-4d2c-851f-cb1df47434ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a2cb77a-a0d1-48cd-9e80-99dfc4e234e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c92faa29-c8b6-4d2c-851f-cb1df47434ff",
                    "LayerId": "31e2f1e3-c290-4962-a587-aa29fd5554b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 310,
    "layers": [
        {
            "id": "31e2f1e3-c290-4962-a587-aa29fd5554b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c740754-7d39-4550-841f-1066b2d07823",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 610,
    "xorig": 305,
    "yorig": 155
}