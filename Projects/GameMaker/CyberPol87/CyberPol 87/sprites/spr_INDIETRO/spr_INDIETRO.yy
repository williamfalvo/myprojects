{
    "id": "366134b0-dded-4fd7-8c1e-33ae98d7904c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_INDIETRO",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 23,
    "bbox_right": 150,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fa35dbb-6060-4a36-9ada-b3d875712cf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "366134b0-dded-4fd7-8c1e-33ae98d7904c",
            "compositeImage": {
                "id": "a6eea14d-6c99-431b-ae56-9f8ec0b0a6da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fa35dbb-6060-4a36-9ada-b3d875712cf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f45936a8-98b1-4a07-97ab-e9cbcbacc828",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fa35dbb-6060-4a36-9ada-b3d875712cf2",
                    "LayerId": "4a54efed-771d-4e5d-b85a-8c9d473ec983"
                }
            ]
        },
        {
            "id": "5c86f18b-4c63-4169-828c-49f12071c4bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "366134b0-dded-4fd7-8c1e-33ae98d7904c",
            "compositeImage": {
                "id": "f3bfd11c-622a-46c9-b06d-573e77ba512e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c86f18b-4c63-4169-828c-49f12071c4bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "814871bf-9f9c-466b-8936-bdc01e2dedec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c86f18b-4c63-4169-828c-49f12071c4bb",
                    "LayerId": "4a54efed-771d-4e5d-b85a-8c9d473ec983"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "4a54efed-771d-4e5d-b85a-8c9d473ec983",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "366134b0-dded-4fd7-8c1e-33ae98d7904c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 178,
    "xorig": 89,
    "yorig": 27
}