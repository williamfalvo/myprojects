{
    "id": "bb611a81-b80d-48a8-a323-207f13b22b08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_down_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 16,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4dc4431-afed-42c1-971d-e2d1b6f76f97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb611a81-b80d-48a8-a323-207f13b22b08",
            "compositeImage": {
                "id": "cd75672f-3b1b-4685-b373-a2fd0fe33f5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4dc4431-afed-42c1-971d-e2d1b6f76f97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95e915c4-3afc-4c6a-984e-80020bfa35e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4dc4431-afed-42c1-971d-e2d1b6f76f97",
                    "LayerId": "a29100c4-a553-4bb9-91ca-5966068bb34b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "a29100c4-a553-4bb9-91ca-5966068bb34b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb611a81-b80d-48a8-a323-207f13b22b08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}