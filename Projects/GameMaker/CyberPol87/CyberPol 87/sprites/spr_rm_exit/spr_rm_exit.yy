{
    "id": "bf3480cf-be0a-417c-83be-202ed73f2f8f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rm_exit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 25,
    "bbox_right": 265,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fd09840-f682-4317-a238-834f75e3995b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf3480cf-be0a-417c-83be-202ed73f2f8f",
            "compositeImage": {
                "id": "b12572ab-26d4-4b0d-9214-fcdf2583fe83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fd09840-f682-4317-a238-834f75e3995b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1627f30b-29ac-458d-9a7f-4eecdc61a69b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fd09840-f682-4317-a238-834f75e3995b",
                    "LayerId": "8d925c5d-47eb-4fe4-bea7-5a169e3a84ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 59,
    "layers": [
        {
            "id": "8d925c5d-47eb-4fe4-bea7-5a169e3a84ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf3480cf-be0a-417c-83be-202ed73f2f8f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 282,
    "xorig": 141,
    "yorig": 29
}