{
    "id": "9f317ddb-7b49-4e5e-9e3a-db0fe9c43bb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Checkpoint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c106bbe-6544-467b-b11c-3318e5d73af8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f317ddb-7b49-4e5e-9e3a-db0fe9c43bb9",
            "compositeImage": {
                "id": "18cc2949-6dfa-4dc1-b613-763dafe56280",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c106bbe-6544-467b-b11c-3318e5d73af8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0394b0a1-f9dd-4bf0-bc55-72b2469f7004",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c106bbe-6544-467b-b11c-3318e5d73af8",
                    "LayerId": "4a223b7b-ebe8-4136-9d8a-15543fa6779a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "4a223b7b-ebe8-4136-9d8a-15543fa6779a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f317ddb-7b49-4e5e-9e3a-db0fe9c43bb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}