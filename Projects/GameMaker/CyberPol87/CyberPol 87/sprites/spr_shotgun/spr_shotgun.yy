{
    "id": "cc8f1ae0-70c3-4de6-a229-18c7d59716ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a253f23-4e5e-46e0-83cc-fca504bfe4ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc8f1ae0-70c3-4de6-a229-18c7d59716ce",
            "compositeImage": {
                "id": "d7948ff6-6ff3-47dd-bbb7-e39824b5ecc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a253f23-4e5e-46e0-83cc-fca504bfe4ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c4d06b0-dada-4052-a08b-e76e0ac8f191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a253f23-4e5e-46e0-83cc-fca504bfe4ff",
                    "LayerId": "88a6507e-aab3-4d28-a482-cf721853f01b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "88a6507e-aab3-4d28-a482-cf721853f01b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc8f1ae0-70c3-4de6-a229-18c7d59716ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}