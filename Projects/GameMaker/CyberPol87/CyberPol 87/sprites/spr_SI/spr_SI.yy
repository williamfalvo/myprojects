{
    "id": "3799a0dc-1806-4b9e-8a73-7417281731f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 12,
    "bbox_right": 61,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93bbe075-5ba2-430c-a75d-651749b249ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3799a0dc-1806-4b9e-8a73-7417281731f1",
            "compositeImage": {
                "id": "e3682263-f70b-463d-a3e8-ea7433cccc70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93bbe075-5ba2-430c-a75d-651749b249ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "038913e3-ed3d-42e4-953f-6e601231f849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93bbe075-5ba2-430c-a75d-651749b249ec",
                    "LayerId": "0c50e9d6-a113-4a4b-b3bd-cee6901d31ba"
                }
            ]
        },
        {
            "id": "ecf2b11c-2f2a-444e-bc14-1c25ab4114b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3799a0dc-1806-4b9e-8a73-7417281731f1",
            "compositeImage": {
                "id": "9158bc5d-e286-4ef0-bea5-8692d89ec4f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf2b11c-2f2a-444e-bc14-1c25ab4114b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfdb243e-0593-4392-a40a-8df0ea8b23fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf2b11c-2f2a-444e-bc14-1c25ab4114b6",
                    "LayerId": "0c50e9d6-a113-4a4b-b3bd-cee6901d31ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "0c50e9d6-a113-4a4b-b3bd-cee6901d31ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3799a0dc-1806-4b9e-8a73-7417281731f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 74,
    "xorig": 37,
    "yorig": 23
}