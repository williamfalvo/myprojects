{
    "id": "244fb6fa-49d8-4855-9ae6-c8b088c4f580",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "929984ef-bda8-4dfd-9707-e6e5a8cdda0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244fb6fa-49d8-4855-9ae6-c8b088c4f580",
            "compositeImage": {
                "id": "ff7ab18b-9014-4674-8648-24f6d9dcb7c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "929984ef-bda8-4dfd-9707-e6e5a8cdda0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a7fefff-c89b-40b9-b8c8-cfabc8924e2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "929984ef-bda8-4dfd-9707-e6e5a8cdda0e",
                    "LayerId": "235801a5-a752-484d-88d5-ac86e3b2cdc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "235801a5-a752-484d-88d5-ac86e3b2cdc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "244fb6fa-49d8-4855-9ae6-c8b088c4f580",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 9
}