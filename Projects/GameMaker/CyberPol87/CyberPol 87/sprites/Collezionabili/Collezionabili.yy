{
    "id": "0d75cca0-f0db-482e-ae9d-4da62029cb68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Collezionabili",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a5ca56d-0eef-4a72-a949-7e149062a5af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d75cca0-f0db-482e-ae9d-4da62029cb68",
            "compositeImage": {
                "id": "220deb3a-9692-4497-9697-36e19fd1854d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a5ca56d-0eef-4a72-a949-7e149062a5af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f691e935-9a49-4ea9-a6a3-2fd0cd5bd3ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a5ca56d-0eef-4a72-a949-7e149062a5af",
                    "LayerId": "e1526847-479e-4a93-8ad4-221b9f9a87ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "e1526847-479e-4a93-8ad4-221b9f9a87ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d75cca0-f0db-482e-ae9d-4da62029cb68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}