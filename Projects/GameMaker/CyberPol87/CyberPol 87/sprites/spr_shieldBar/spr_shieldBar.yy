{
    "id": "30a07c93-1b86-420f-80aa-6fc823e3c06c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shieldBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 7,
    "bbox_right": 56,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b8d7d64-32eb-4d1a-a682-4163380c996e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30a07c93-1b86-420f-80aa-6fc823e3c06c",
            "compositeImage": {
                "id": "f3cf14eb-07c4-45b2-ac6e-d8354ec0d1f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b8d7d64-32eb-4d1a-a682-4163380c996e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "744dc27c-d2ad-45ad-969b-d0b812aa420b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b8d7d64-32eb-4d1a-a682-4163380c996e",
                    "LayerId": "a26e4b8d-c314-420c-91da-cc11b3fb9cb4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a26e4b8d-c314-420c-91da-cc11b3fb9cb4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30a07c93-1b86-420f-80aa-6fc823e3c06c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 7,
    "yorig": 32
}