{
    "id": "c98f6357-5533-4390-8b6c-8bd9598779bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shotgun_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a90e691d-e200-4809-ad79-f4db55f92f38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98f6357-5533-4390-8b6c-8bd9598779bf",
            "compositeImage": {
                "id": "6fa36ee8-5c13-49f2-9dc3-c593f8deda52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a90e691d-e200-4809-ad79-f4db55f92f38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f9ee737-1ca9-47eb-b38e-ff4a638fff8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a90e691d-e200-4809-ad79-f4db55f92f38",
                    "LayerId": "b8b9d3e2-01b9-45ee-86a6-eb3505436ac4"
                }
            ]
        },
        {
            "id": "e8a845b4-b234-4860-adef-ec1874cbdc69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98f6357-5533-4390-8b6c-8bd9598779bf",
            "compositeImage": {
                "id": "993af96e-bb96-4e44-8f81-227c5db83f55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8a845b4-b234-4860-adef-ec1874cbdc69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8608e5fa-35f0-4b8f-9a67-57627f6f6bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8a845b4-b234-4860-adef-ec1874cbdc69",
                    "LayerId": "b8b9d3e2-01b9-45ee-86a6-eb3505436ac4"
                }
            ]
        },
        {
            "id": "05dfd307-383e-45e5-996f-ce941d7c39a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98f6357-5533-4390-8b6c-8bd9598779bf",
            "compositeImage": {
                "id": "568cbb1b-b070-41e2-b32b-7d50c57ff2c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05dfd307-383e-45e5-996f-ce941d7c39a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fc7242e-072a-4db7-adfb-026df62b9414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05dfd307-383e-45e5-996f-ce941d7c39a4",
                    "LayerId": "b8b9d3e2-01b9-45ee-86a6-eb3505436ac4"
                }
            ]
        },
        {
            "id": "d1082e25-1957-45e2-ac47-fff6b20b17ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98f6357-5533-4390-8b6c-8bd9598779bf",
            "compositeImage": {
                "id": "91f321be-09b8-4d14-8f67-cca16eddb100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1082e25-1957-45e2-ac47-fff6b20b17ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b1a30c6-0bd8-46a1-a5fb-edb5352bfa6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1082e25-1957-45e2-ac47-fff6b20b17ea",
                    "LayerId": "b8b9d3e2-01b9-45ee-86a6-eb3505436ac4"
                }
            ]
        },
        {
            "id": "e74c47f3-b615-43ad-9b78-2bb0a72dd620",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98f6357-5533-4390-8b6c-8bd9598779bf",
            "compositeImage": {
                "id": "d089846f-3d00-4741-8a42-9ae56d9c90cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e74c47f3-b615-43ad-9b78-2bb0a72dd620",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bd8a394-0c1e-4b1c-8bd8-24b442890d3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e74c47f3-b615-43ad-9b78-2bb0a72dd620",
                    "LayerId": "b8b9d3e2-01b9-45ee-86a6-eb3505436ac4"
                }
            ]
        },
        {
            "id": "8b74d0db-ebc8-4de4-8596-d7813ca68cdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98f6357-5533-4390-8b6c-8bd9598779bf",
            "compositeImage": {
                "id": "9108ea57-f21f-44a0-a74e-b1ba33b5cd84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b74d0db-ebc8-4de4-8596-d7813ca68cdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46dab6db-0265-4195-95cf-354c2e48376a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b74d0db-ebc8-4de4-8596-d7813ca68cdd",
                    "LayerId": "b8b9d3e2-01b9-45ee-86a6-eb3505436ac4"
                }
            ]
        },
        {
            "id": "be3310f9-97fe-41f8-ad94-36aca67c21d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c98f6357-5533-4390-8b6c-8bd9598779bf",
            "compositeImage": {
                "id": "865d44f0-b47e-4f79-8174-44250ff3ac9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be3310f9-97fe-41f8-ad94-36aca67c21d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "744553af-182a-41f6-9f44-4f6c5e8d55ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be3310f9-97fe-41f8-ad94-36aca67c21d4",
                    "LayerId": "b8b9d3e2-01b9-45ee-86a6-eb3505436ac4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b8b9d3e2-01b9-45ee-86a6-eb3505436ac4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c98f6357-5533-4390-8b6c-8bd9598779bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 32
}