{
    "id": "072bf3bc-a121-4c8f-a87b-262864a90f88",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_rightDown_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dce6ba52-081c-40e3-882d-34c21b588070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "072bf3bc-a121-4c8f-a87b-262864a90f88",
            "compositeImage": {
                "id": "ccf42e9d-86cd-4407-b997-465129d8af8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce6ba52-081c-40e3-882d-34c21b588070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb233fae-d835-423e-8bbc-038ac9c43369",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce6ba52-081c-40e3-882d-34c21b588070",
                    "LayerId": "c038c2e7-4ee5-4d69-b231-3c356c8135de"
                }
            ]
        },
        {
            "id": "0bb2ae61-c8ea-400a-8dba-74b74de03a88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "072bf3bc-a121-4c8f-a87b-262864a90f88",
            "compositeImage": {
                "id": "1e5354f8-8253-4109-944b-cdd81726bedc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bb2ae61-c8ea-400a-8dba-74b74de03a88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afd0f7c7-1ff7-415f-a2ea-6a7980face68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bb2ae61-c8ea-400a-8dba-74b74de03a88",
                    "LayerId": "c038c2e7-4ee5-4d69-b231-3c356c8135de"
                }
            ]
        },
        {
            "id": "22672d70-1619-4e89-98f3-e8377bbf81d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "072bf3bc-a121-4c8f-a87b-262864a90f88",
            "compositeImage": {
                "id": "c50dbce2-e4d1-4231-b864-5cd8e28c0c52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22672d70-1619-4e89-98f3-e8377bbf81d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38675493-19a2-4c0a-ac0e-c8940c4eba2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22672d70-1619-4e89-98f3-e8377bbf81d9",
                    "LayerId": "c038c2e7-4ee5-4d69-b231-3c356c8135de"
                }
            ]
        },
        {
            "id": "f02c417a-ef79-419a-9f32-432a4566428c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "072bf3bc-a121-4c8f-a87b-262864a90f88",
            "compositeImage": {
                "id": "68e3cffe-f32c-470f-913f-ad7b3e9e97d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f02c417a-ef79-419a-9f32-432a4566428c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ad1b1c2-d4cc-4701-ad3f-ae699bdd0257",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f02c417a-ef79-419a-9f32-432a4566428c",
                    "LayerId": "c038c2e7-4ee5-4d69-b231-3c356c8135de"
                }
            ]
        },
        {
            "id": "6f34392e-c3fa-4395-a559-1806a09e5aa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "072bf3bc-a121-4c8f-a87b-262864a90f88",
            "compositeImage": {
                "id": "1a687315-9e8d-4a9e-9388-99f7af7fa72f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f34392e-c3fa-4395-a559-1806a09e5aa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e4e6391-14ba-4d4c-9eaa-b67d7ed208e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f34392e-c3fa-4395-a559-1806a09e5aa4",
                    "LayerId": "c038c2e7-4ee5-4d69-b231-3c356c8135de"
                }
            ]
        },
        {
            "id": "f0f5be81-3384-4212-aa7c-a31686121a57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "072bf3bc-a121-4c8f-a87b-262864a90f88",
            "compositeImage": {
                "id": "0a11836a-4ece-4854-b9b4-4322c790524a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0f5be81-3384-4212-aa7c-a31686121a57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a5b4a3-fe40-4a7c-8389-72e89d1a8f60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0f5be81-3384-4212-aa7c-a31686121a57",
                    "LayerId": "c038c2e7-4ee5-4d69-b231-3c356c8135de"
                }
            ]
        },
        {
            "id": "1860294b-800d-47b2-a47a-cb5815de3637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "072bf3bc-a121-4c8f-a87b-262864a90f88",
            "compositeImage": {
                "id": "5c5cec9e-6dbb-4b0e-8eb0-9677691ade38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1860294b-800d-47b2-a47a-cb5815de3637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f118461-8344-4870-a8f2-d85922db711d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1860294b-800d-47b2-a47a-cb5815de3637",
                    "LayerId": "c038c2e7-4ee5-4d69-b231-3c356c8135de"
                }
            ]
        },
        {
            "id": "5d978c7f-306c-4e49-966d-39e1edbccf76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "072bf3bc-a121-4c8f-a87b-262864a90f88",
            "compositeImage": {
                "id": "fedfabd6-5dfd-4c4d-8977-bb06183273b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d978c7f-306c-4e49-966d-39e1edbccf76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b2cb31c-062a-4504-97db-3f93349e06dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d978c7f-306c-4e49-966d-39e1edbccf76",
                    "LayerId": "c038c2e7-4ee5-4d69-b231-3c356c8135de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "c038c2e7-4ee5-4d69-b231-3c356c8135de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "072bf3bc-a121-4c8f-a87b-262864a90f88",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}