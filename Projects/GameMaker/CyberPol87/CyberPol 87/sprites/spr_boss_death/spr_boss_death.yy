{
    "id": "f5e0f06e-5c8a-4575-9a1f-06fe916faaac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b754d35d-c7bd-4a49-bce9-c2f51cb04f06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e0f06e-5c8a-4575-9a1f-06fe916faaac",
            "compositeImage": {
                "id": "957fc6b6-7ced-446f-b717-fffa99e2de05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b754d35d-c7bd-4a49-bce9-c2f51cb04f06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a58e549-62e7-4372-83ba-f6dadeea5d70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b754d35d-c7bd-4a49-bce9-c2f51cb04f06",
                    "LayerId": "091c6d1e-efdc-4b21-94c2-dae880eec93c"
                }
            ]
        },
        {
            "id": "1312bd70-3062-4484-b0ac-c2782191f334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e0f06e-5c8a-4575-9a1f-06fe916faaac",
            "compositeImage": {
                "id": "5448ce90-fe74-4b81-b6b7-9f18afe7a2d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1312bd70-3062-4484-b0ac-c2782191f334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11e0625b-cff8-496e-8089-554ac46ad833",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1312bd70-3062-4484-b0ac-c2782191f334",
                    "LayerId": "091c6d1e-efdc-4b21-94c2-dae880eec93c"
                }
            ]
        },
        {
            "id": "c0278bff-38db-495a-a65c-e3797e72cec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e0f06e-5c8a-4575-9a1f-06fe916faaac",
            "compositeImage": {
                "id": "05a15e74-33a9-4197-a0e8-f5aab878e174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0278bff-38db-495a-a65c-e3797e72cec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb6446d9-6b1f-45a1-8d77-c1b31d8cac52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0278bff-38db-495a-a65c-e3797e72cec1",
                    "LayerId": "091c6d1e-efdc-4b21-94c2-dae880eec93c"
                }
            ]
        },
        {
            "id": "5afa084e-fe84-4a39-8751-7aab57ef681a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e0f06e-5c8a-4575-9a1f-06fe916faaac",
            "compositeImage": {
                "id": "861b957c-3cd3-4c48-bede-bf54b08aaccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5afa084e-fe84-4a39-8751-7aab57ef681a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d2a3f48-8a3d-4df0-bed6-dd2ce6c85247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5afa084e-fe84-4a39-8751-7aab57ef681a",
                    "LayerId": "091c6d1e-efdc-4b21-94c2-dae880eec93c"
                }
            ]
        },
        {
            "id": "afd60ef5-b95a-44dc-ae8f-a3c7f19de8fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e0f06e-5c8a-4575-9a1f-06fe916faaac",
            "compositeImage": {
                "id": "b7d18710-aa82-4d03-8ba9-a77ee8f56be2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afd60ef5-b95a-44dc-ae8f-a3c7f19de8fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48878b3d-a53a-43ed-afe8-cf1e5a82d035",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afd60ef5-b95a-44dc-ae8f-a3c7f19de8fd",
                    "LayerId": "091c6d1e-efdc-4b21-94c2-dae880eec93c"
                }
            ]
        },
        {
            "id": "fefb2131-b6e7-4acf-9a0c-fb1d7adec370",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e0f06e-5c8a-4575-9a1f-06fe916faaac",
            "compositeImage": {
                "id": "d3be6032-f3e3-4f6b-b899-91ae1f70816e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fefb2131-b6e7-4acf-9a0c-fb1d7adec370",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c650448c-c450-4320-85b5-26b19b3de021",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fefb2131-b6e7-4acf-9a0c-fb1d7adec370",
                    "LayerId": "091c6d1e-efdc-4b21-94c2-dae880eec93c"
                }
            ]
        },
        {
            "id": "e6bfdd95-e0a6-4cb4-a3bc-0a828cd71f8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e0f06e-5c8a-4575-9a1f-06fe916faaac",
            "compositeImage": {
                "id": "bcef4a8d-fe5e-472f-aa00-0f149a92f693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6bfdd95-e0a6-4cb4-a3bc-0a828cd71f8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91f5392c-b56e-47fa-8eb8-1d6222f2acef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6bfdd95-e0a6-4cb4-a3bc-0a828cd71f8f",
                    "LayerId": "091c6d1e-efdc-4b21-94c2-dae880eec93c"
                }
            ]
        },
        {
            "id": "d3ecc741-a2d6-4565-95df-4d7278654419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5e0f06e-5c8a-4575-9a1f-06fe916faaac",
            "compositeImage": {
                "id": "ae78d17b-4ac1-4daa-ae22-f119e6524ec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3ecc741-a2d6-4565-95df-4d7278654419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f45a429f-a091-41e4-a198-668a9c40449f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3ecc741-a2d6-4565-95df-4d7278654419",
                    "LayerId": "091c6d1e-efdc-4b21-94c2-dae880eec93c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "091c6d1e-efdc-4b21-94c2-dae880eec93c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5e0f06e-5c8a-4575-9a1f-06fe916faaac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}