{
    "id": "b244e25c-bf13-4a24-8966-88be4df11e97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Testo_Ospedale",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 10,
    "bbox_right": 596,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2633836-90da-45e1-8470-c9a2d5b43694",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b244e25c-bf13-4a24-8966-88be4df11e97",
            "compositeImage": {
                "id": "b7f56c05-8c51-4609-a26e-beedc954767f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2633836-90da-45e1-8470-c9a2d5b43694",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "550d21c5-d327-4095-8d84-6fcd171b8e05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2633836-90da-45e1-8470-c9a2d5b43694",
                    "LayerId": "3d228d7e-cbc3-4328-8aaa-c65626d9cc00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "3d228d7e-cbc3-4328-8aaa-c65626d9cc00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b244e25c-bf13-4a24-8966-88be4df11e97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 610,
    "xorig": 305,
    "yorig": 55
}