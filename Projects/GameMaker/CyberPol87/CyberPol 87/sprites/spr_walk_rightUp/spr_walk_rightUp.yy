{
    "id": "11a78aa4-9170-4a9e-b28c-2a82c759cd57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_rightUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 15,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cbbf0e9-d765-4e75-8c31-64d48363c091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a78aa4-9170-4a9e-b28c-2a82c759cd57",
            "compositeImage": {
                "id": "a3ae1925-80c6-4ca4-8593-855f82e34a15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cbbf0e9-d765-4e75-8c31-64d48363c091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6841d60a-c849-4e72-9af2-13d965f05c06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cbbf0e9-d765-4e75-8c31-64d48363c091",
                    "LayerId": "facbc061-dbf0-4f86-b077-d210765138bd"
                }
            ]
        },
        {
            "id": "57db24c4-ba82-496f-98a4-98cf93fb967e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a78aa4-9170-4a9e-b28c-2a82c759cd57",
            "compositeImage": {
                "id": "ce921355-633f-408d-9b30-20b0e1b0508a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57db24c4-ba82-496f-98a4-98cf93fb967e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3539081-ea97-45ba-b9e2-99f113f1c9f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57db24c4-ba82-496f-98a4-98cf93fb967e",
                    "LayerId": "facbc061-dbf0-4f86-b077-d210765138bd"
                }
            ]
        },
        {
            "id": "cda2c3e9-c2c3-404e-8e7f-65efbb03d257",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a78aa4-9170-4a9e-b28c-2a82c759cd57",
            "compositeImage": {
                "id": "3f9cc42a-b385-4191-8cf8-69df20df8dc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cda2c3e9-c2c3-404e-8e7f-65efbb03d257",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfcaa576-72e8-4f38-a089-aa6ea1bcbf71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cda2c3e9-c2c3-404e-8e7f-65efbb03d257",
                    "LayerId": "facbc061-dbf0-4f86-b077-d210765138bd"
                }
            ]
        },
        {
            "id": "d9ed9529-90da-4df6-a4d7-dd75c5459a06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a78aa4-9170-4a9e-b28c-2a82c759cd57",
            "compositeImage": {
                "id": "750c304f-69ef-4889-b190-970bfe547c95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9ed9529-90da-4df6-a4d7-dd75c5459a06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38e0898f-0a12-4545-ace5-9a1a2b65e185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9ed9529-90da-4df6-a4d7-dd75c5459a06",
                    "LayerId": "facbc061-dbf0-4f86-b077-d210765138bd"
                }
            ]
        },
        {
            "id": "ebbacd90-3449-4a6e-aa10-97212ba18de8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a78aa4-9170-4a9e-b28c-2a82c759cd57",
            "compositeImage": {
                "id": "553ee859-b4d7-46b1-9c23-8f1043dee906",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebbacd90-3449-4a6e-aa10-97212ba18de8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a569ed67-11e0-4fb7-be65-16d8cfa4044d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebbacd90-3449-4a6e-aa10-97212ba18de8",
                    "LayerId": "facbc061-dbf0-4f86-b077-d210765138bd"
                }
            ]
        },
        {
            "id": "5ee1a4c8-dd6c-4953-b29c-35d3e63a2a07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a78aa4-9170-4a9e-b28c-2a82c759cd57",
            "compositeImage": {
                "id": "f2286460-6527-4fe0-baae-9c4366ce40f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee1a4c8-dd6c-4953-b29c-35d3e63a2a07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "441b9585-3fa1-47ab-af8e-1c8378d448e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee1a4c8-dd6c-4953-b29c-35d3e63a2a07",
                    "LayerId": "facbc061-dbf0-4f86-b077-d210765138bd"
                }
            ]
        },
        {
            "id": "c392b31b-1cbe-402a-885b-abb611791956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a78aa4-9170-4a9e-b28c-2a82c759cd57",
            "compositeImage": {
                "id": "a976693d-f057-4577-9286-f5207f5f37cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c392b31b-1cbe-402a-885b-abb611791956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ec7b99d-7bdb-4696-8f5b-53fb70d982cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c392b31b-1cbe-402a-885b-abb611791956",
                    "LayerId": "facbc061-dbf0-4f86-b077-d210765138bd"
                }
            ]
        },
        {
            "id": "4a37ef43-56ed-4cde-86eb-0074cdf136ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a78aa4-9170-4a9e-b28c-2a82c759cd57",
            "compositeImage": {
                "id": "0b9a510d-c265-4082-bd84-da06bf220f8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a37ef43-56ed-4cde-86eb-0074cdf136ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5706f3c8-527a-4c51-aa6a-a91e8599fabc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a37ef43-56ed-4cde-86eb-0074cdf136ab",
                    "LayerId": "facbc061-dbf0-4f86-b077-d210765138bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "facbc061-dbf0-4f86-b077-d210765138bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11a78aa4-9170-4a9e-b28c-2a82c759cd57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}