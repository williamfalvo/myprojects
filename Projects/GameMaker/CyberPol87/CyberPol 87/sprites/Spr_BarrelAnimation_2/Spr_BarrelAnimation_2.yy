{
    "id": "ab317226-b9fd-4c2b-882a-74436d26b491",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_BarrelAnimation_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 7,
    "bbox_right": 48,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6eabc1b-ed6f-44b0-b5bc-c4a15e550e0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab317226-b9fd-4c2b-882a-74436d26b491",
            "compositeImage": {
                "id": "e9a0d65c-63f7-4f38-aca3-7a4f3b61a546",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6eabc1b-ed6f-44b0-b5bc-c4a15e550e0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6f00c3c-b27c-4f95-87eb-c0a8a251be49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6eabc1b-ed6f-44b0-b5bc-c4a15e550e0e",
                    "LayerId": "834739b6-6730-4dcd-913f-6e3f255df274"
                }
            ]
        },
        {
            "id": "88ff2fbd-abee-47b7-b2f0-f81e00424de5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab317226-b9fd-4c2b-882a-74436d26b491",
            "compositeImage": {
                "id": "f35b48ab-de86-4138-8085-b3d7fae63e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88ff2fbd-abee-47b7-b2f0-f81e00424de5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9498ba57-6870-455a-880d-6ba100899946",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88ff2fbd-abee-47b7-b2f0-f81e00424de5",
                    "LayerId": "834739b6-6730-4dcd-913f-6e3f255df274"
                }
            ]
        },
        {
            "id": "0a922d3a-2ece-4ecd-91e0-2b518100cc5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab317226-b9fd-4c2b-882a-74436d26b491",
            "compositeImage": {
                "id": "91d050d5-80f6-49a8-9905-1353dde911b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a922d3a-2ece-4ecd-91e0-2b518100cc5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67e73856-b710-4850-bfaf-952684e2e58e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a922d3a-2ece-4ecd-91e0-2b518100cc5f",
                    "LayerId": "834739b6-6730-4dcd-913f-6e3f255df274"
                }
            ]
        },
        {
            "id": "a21a1ad9-06e3-4cf4-ba46-2afd5b0b5bcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab317226-b9fd-4c2b-882a-74436d26b491",
            "compositeImage": {
                "id": "84870921-c234-41af-9b97-58992d423f6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a21a1ad9-06e3-4cf4-ba46-2afd5b0b5bcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31b382d4-c1e6-46fd-aca3-a4331399e063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a21a1ad9-06e3-4cf4-ba46-2afd5b0b5bcd",
                    "LayerId": "834739b6-6730-4dcd-913f-6e3f255df274"
                }
            ]
        },
        {
            "id": "7f3a2797-9f03-4a63-a0cb-5d52900b9cc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab317226-b9fd-4c2b-882a-74436d26b491",
            "compositeImage": {
                "id": "0eef3390-4f78-46c5-b9ac-d8d1c90db529",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f3a2797-9f03-4a63-a0cb-5d52900b9cc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dba990d6-ff97-4e3c-b63f-483cff273132",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f3a2797-9f03-4a63-a0cb-5d52900b9cc5",
                    "LayerId": "834739b6-6730-4dcd-913f-6e3f255df274"
                }
            ]
        },
        {
            "id": "29ca7f61-0bb7-4e65-8c4b-5793811cbd02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab317226-b9fd-4c2b-882a-74436d26b491",
            "compositeImage": {
                "id": "c170581e-eb22-4368-bd70-51b86455fa8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29ca7f61-0bb7-4e65-8c4b-5793811cbd02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16fe1f2c-3f29-431d-a4e3-e1c08fc7a518",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29ca7f61-0bb7-4e65-8c4b-5793811cbd02",
                    "LayerId": "834739b6-6730-4dcd-913f-6e3f255df274"
                }
            ]
        },
        {
            "id": "6814f063-5384-4a6d-8559-41b4dcd960a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab317226-b9fd-4c2b-882a-74436d26b491",
            "compositeImage": {
                "id": "e1f119e2-1ad8-480f-80f0-acd9caa569dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6814f063-5384-4a6d-8559-41b4dcd960a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ca498dd-ae19-4cb3-a7f4-cf4df61d5de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6814f063-5384-4a6d-8559-41b4dcd960a4",
                    "LayerId": "834739b6-6730-4dcd-913f-6e3f255df274"
                }
            ]
        },
        {
            "id": "ebb51877-23ee-4d0c-aef1-4ccf24cc7842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab317226-b9fd-4c2b-882a-74436d26b491",
            "compositeImage": {
                "id": "f820943c-1761-47e6-a48a-2be9c71e2826",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebb51877-23ee-4d0c-aef1-4ccf24cc7842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd6cedfe-87cf-40cd-9f87-499aed980a87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebb51877-23ee-4d0c-aef1-4ccf24cc7842",
                    "LayerId": "834739b6-6730-4dcd-913f-6e3f255df274"
                }
            ]
        },
        {
            "id": "6b05cd97-77d1-4e84-a68e-6888dad57c66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab317226-b9fd-4c2b-882a-74436d26b491",
            "compositeImage": {
                "id": "b5c29b35-0b67-456f-a3eb-4e6087529791",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b05cd97-77d1-4e84-a68e-6888dad57c66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9dd526a-1f4a-49b7-963d-77c308237cb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b05cd97-77d1-4e84-a68e-6888dad57c66",
                    "LayerId": "834739b6-6730-4dcd-913f-6e3f255df274"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "834739b6-6730-4dcd-913f-6e3f255df274",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab317226-b9fd-4c2b-882a-74436d26b491",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}