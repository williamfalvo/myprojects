{
    "id": "6199ca92-10b6-4437-b63f-26f0e759d4ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pause_txt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 11,
    "bbox_right": 97,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08f1e2ff-93f4-47af-a9e1-d9e05b57899e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6199ca92-10b6-4437-b63f-26f0e759d4ba",
            "compositeImage": {
                "id": "487f3429-0479-467e-84ac-85f9c38aefef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08f1e2ff-93f4-47af-a9e1-d9e05b57899e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaa11bfa-4084-45c3-915a-5d36e5d2fef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f1e2ff-93f4-47af-a9e1-d9e05b57899e",
                    "LayerId": "3c7eb34c-3d70-4dd2-b9d6-9d7d88ca5cf5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "3c7eb34c-3d70-4dd2-b9d6-9d7d88ca5cf5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6199ca92-10b6-4437-b63f-26f0e759d4ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 109,
    "xorig": 54,
    "yorig": 20
}