{
    "id": "a1bed3b8-a773-4853-9aac-849b3abd3bf0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_up_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b41d7d2e-5988-4445-873b-96c37098959c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1bed3b8-a773-4853-9aac-849b3abd3bf0",
            "compositeImage": {
                "id": "ede56bf0-505f-4406-89b0-232029b93e3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b41d7d2e-5988-4445-873b-96c37098959c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c3fd33c-e0fc-4257-8618-1a40f3a50f09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b41d7d2e-5988-4445-873b-96c37098959c",
                    "LayerId": "bca83577-9208-4ae3-b184-f9b5f1f8d18b"
                }
            ]
        },
        {
            "id": "69d622ad-74f2-4861-86c8-bdd6ee8d09a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1bed3b8-a773-4853-9aac-849b3abd3bf0",
            "compositeImage": {
                "id": "998330cf-f9db-4dd4-865d-6dba42d2cb7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69d622ad-74f2-4861-86c8-bdd6ee8d09a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6271bffe-872c-427a-9257-369d567da596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69d622ad-74f2-4861-86c8-bdd6ee8d09a5",
                    "LayerId": "bca83577-9208-4ae3-b184-f9b5f1f8d18b"
                }
            ]
        },
        {
            "id": "f6225225-4d44-4cac-9395-6b73c09b7361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1bed3b8-a773-4853-9aac-849b3abd3bf0",
            "compositeImage": {
                "id": "32b6c5bb-1dc9-424a-9513-6b3f209d4356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6225225-4d44-4cac-9395-6b73c09b7361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2462d446-3566-4b7a-a039-27a629098809",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6225225-4d44-4cac-9395-6b73c09b7361",
                    "LayerId": "bca83577-9208-4ae3-b184-f9b5f1f8d18b"
                }
            ]
        },
        {
            "id": "ba8bbf83-1d66-4a9f-8032-e29a17bed1f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1bed3b8-a773-4853-9aac-849b3abd3bf0",
            "compositeImage": {
                "id": "1309a2f6-2e60-46c2-b110-970d02b395fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba8bbf83-1d66-4a9f-8032-e29a17bed1f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03e3e28a-5797-4a19-a741-22fcf968fcde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba8bbf83-1d66-4a9f-8032-e29a17bed1f6",
                    "LayerId": "bca83577-9208-4ae3-b184-f9b5f1f8d18b"
                }
            ]
        },
        {
            "id": "6df7e39f-51d7-4fcc-8827-541f0f87bb4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1bed3b8-a773-4853-9aac-849b3abd3bf0",
            "compositeImage": {
                "id": "684e38d0-2c14-4f45-8ae7-40d379f3de3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6df7e39f-51d7-4fcc-8827-541f0f87bb4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1980a3f8-2c95-469e-877b-d95413ae6ed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6df7e39f-51d7-4fcc-8827-541f0f87bb4e",
                    "LayerId": "bca83577-9208-4ae3-b184-f9b5f1f8d18b"
                }
            ]
        },
        {
            "id": "f9659f17-1c08-4136-9165-df716d84d07d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1bed3b8-a773-4853-9aac-849b3abd3bf0",
            "compositeImage": {
                "id": "407965ec-d72c-4d18-ba06-4d08ff72c253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9659f17-1c08-4136-9165-df716d84d07d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e193697c-de47-4093-ac80-25edfdd97be5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9659f17-1c08-4136-9165-df716d84d07d",
                    "LayerId": "bca83577-9208-4ae3-b184-f9b5f1f8d18b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "bca83577-9208-4ae3-b184-f9b5f1f8d18b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1bed3b8-a773-4853-9aac-849b3abd3bf0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}