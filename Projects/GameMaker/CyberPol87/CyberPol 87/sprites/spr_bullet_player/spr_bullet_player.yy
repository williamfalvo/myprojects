{
    "id": "004babb2-fc1b-4f41-aac3-954d1e8b52eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc8c50c0-6a61-4c60-949e-12a6b75d8527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004babb2-fc1b-4f41-aac3-954d1e8b52eb",
            "compositeImage": {
                "id": "4735739a-1fa0-40c0-acc0-c1832dde11f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc8c50c0-6a61-4c60-949e-12a6b75d8527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b27128f-651b-4b6c-a7b6-4e2d42190c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc8c50c0-6a61-4c60-949e-12a6b75d8527",
                    "LayerId": "efdf9292-e552-46b1-a3f0-c148c3b10080"
                }
            ]
        },
        {
            "id": "c6742367-eb5a-4251-b77b-f33324d5df6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004babb2-fc1b-4f41-aac3-954d1e8b52eb",
            "compositeImage": {
                "id": "7dd8ae90-d4d0-4c80-8d95-6bae58a284c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6742367-eb5a-4251-b77b-f33324d5df6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaa53820-4665-4d00-a26d-0cd1d7f0cf25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6742367-eb5a-4251-b77b-f33324d5df6d",
                    "LayerId": "efdf9292-e552-46b1-a3f0-c148c3b10080"
                }
            ]
        },
        {
            "id": "87668ae5-7b28-48e8-9c56-94a8e2d65331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004babb2-fc1b-4f41-aac3-954d1e8b52eb",
            "compositeImage": {
                "id": "274596f8-aa53-49f6-9d56-e6ed486733b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87668ae5-7b28-48e8-9c56-94a8e2d65331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9231b38f-5af9-4c8c-92f8-6e5e1cf0c732",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87668ae5-7b28-48e8-9c56-94a8e2d65331",
                    "LayerId": "efdf9292-e552-46b1-a3f0-c148c3b10080"
                }
            ]
        },
        {
            "id": "a48ea255-f008-4b3f-a903-cad6e5e353cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004babb2-fc1b-4f41-aac3-954d1e8b52eb",
            "compositeImage": {
                "id": "68a92fe0-0b90-4923-8345-807246877007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a48ea255-f008-4b3f-a903-cad6e5e353cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4764345-44f2-4b8f-8ca2-e6b30a4d182f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48ea255-f008-4b3f-a903-cad6e5e353cc",
                    "LayerId": "efdf9292-e552-46b1-a3f0-c148c3b10080"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "efdf9292-e552-46b1-a3f0-c148c3b10080",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "004babb2-fc1b-4f41-aac3-954d1e8b52eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}