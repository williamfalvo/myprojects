{
    "id": "ebe827f3-c432-4580-a031-f2340a33a7cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "villa_Tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 818,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "188a4ddd-36f9-47e5-8a61-3fe1e8cc300d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebe827f3-c432-4580-a031-f2340a33a7cc",
            "compositeImage": {
                "id": "11d5a61d-11a5-429d-ab1d-0d0ceffb880f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "188a4ddd-36f9-47e5-8a61-3fe1e8cc300d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b199000-f31d-4a4d-a837-04d3813b40e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "188a4ddd-36f9-47e5-8a61-3fe1e8cc300d",
                    "LayerId": "737b4663-3180-486e-996a-7e5ddb93fc4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 832,
    "layers": [
        {
            "id": "737b4663-3180-486e-996a-7e5ddb93fc4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebe827f3-c432-4580-a031-f2340a33a7cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}