{
    "id": "77bf07ab-4b89-4de5-b6d8-3edd73b3b9cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_scudo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 7,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e6a4e94-7c35-4f33-8956-471131a0b36c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77bf07ab-4b89-4de5-b6d8-3edd73b3b9cc",
            "compositeImage": {
                "id": "5a562610-2547-47d0-bdf3-3dd2f79a540d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e6a4e94-7c35-4f33-8956-471131a0b36c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f05a34e4-2c98-40d4-a01c-536a423a0d22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e6a4e94-7c35-4f33-8956-471131a0b36c",
                    "LayerId": "5c4b0e51-7b21-44ef-8b86-0bf8a6f40888"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "5c4b0e51-7b21-44ef-8b86-0bf8a6f40888",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77bf07ab-4b89-4de5-b6d8-3edd73b3b9cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}