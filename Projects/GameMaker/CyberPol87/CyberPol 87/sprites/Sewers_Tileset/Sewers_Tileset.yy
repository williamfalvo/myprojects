{
    "id": "f63cca6d-48e0-43a4-80ad-072d4131f402",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Sewers_Tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 32,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62ac55f9-1fee-452c-ac14-699ec801089d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f63cca6d-48e0-43a4-80ad-072d4131f402",
            "compositeImage": {
                "id": "c0876ade-aa70-46a2-86d8-5ea083cd0a06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62ac55f9-1fee-452c-ac14-699ec801089d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80903e93-33a1-4e64-a1e0-342da8275ed3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62ac55f9-1fee-452c-ac14-699ec801089d",
                    "LayerId": "7b045cab-89f3-4514-8b2e-2d244e05fa61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7b045cab-89f3-4514-8b2e-2d244e05fa61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f63cca6d-48e0-43a4-80ad-072d4131f402",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 64
}