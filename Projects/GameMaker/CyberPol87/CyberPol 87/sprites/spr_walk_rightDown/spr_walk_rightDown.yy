{
    "id": "64bf61b2-c0e1-48a7-9575-962c60bef9b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_rightDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 15,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4ea14bf-d3cb-4686-b9ad-aa0e080ca85a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64bf61b2-c0e1-48a7-9575-962c60bef9b1",
            "compositeImage": {
                "id": "f58f93ee-4efc-4bbc-a875-ebec2b653883",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4ea14bf-d3cb-4686-b9ad-aa0e080ca85a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcc24328-a46d-490e-b1af-a1d3a87b6b7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4ea14bf-d3cb-4686-b9ad-aa0e080ca85a",
                    "LayerId": "bb185b09-82f1-4d6a-a2ae-3765927dd054"
                }
            ]
        },
        {
            "id": "fe9fad38-b3ef-45f9-b351-9719fc430586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64bf61b2-c0e1-48a7-9575-962c60bef9b1",
            "compositeImage": {
                "id": "032a76a0-79f8-4e24-aa24-86bc4aeb88c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe9fad38-b3ef-45f9-b351-9719fc430586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f43d237e-f669-4e7b-8948-e4810f725aab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe9fad38-b3ef-45f9-b351-9719fc430586",
                    "LayerId": "bb185b09-82f1-4d6a-a2ae-3765927dd054"
                }
            ]
        },
        {
            "id": "866022f9-7e50-4376-a7a0-e308e9f9890c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64bf61b2-c0e1-48a7-9575-962c60bef9b1",
            "compositeImage": {
                "id": "36a1dff2-217c-49ff-b272-7dbc6b1e448a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "866022f9-7e50-4376-a7a0-e308e9f9890c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6cbe507-2ee1-423f-9607-4b0fdcfbb6d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "866022f9-7e50-4376-a7a0-e308e9f9890c",
                    "LayerId": "bb185b09-82f1-4d6a-a2ae-3765927dd054"
                }
            ]
        },
        {
            "id": "b711a2af-a3a3-4ec1-af8c-5d124a6a46c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64bf61b2-c0e1-48a7-9575-962c60bef9b1",
            "compositeImage": {
                "id": "e77c125f-b0e0-4173-ad66-ad646fa6e4ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b711a2af-a3a3-4ec1-af8c-5d124a6a46c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "901d752b-bdf4-4b2b-b6e5-34994c9bee4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b711a2af-a3a3-4ec1-af8c-5d124a6a46c2",
                    "LayerId": "bb185b09-82f1-4d6a-a2ae-3765927dd054"
                }
            ]
        },
        {
            "id": "ac7180bd-7753-46a9-a131-67ef2b21cf31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64bf61b2-c0e1-48a7-9575-962c60bef9b1",
            "compositeImage": {
                "id": "b89162bd-d8b9-45f7-95a0-c145cf007d70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac7180bd-7753-46a9-a131-67ef2b21cf31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3f87151-2ea3-4451-81f1-9c60cadcdde7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac7180bd-7753-46a9-a131-67ef2b21cf31",
                    "LayerId": "bb185b09-82f1-4d6a-a2ae-3765927dd054"
                }
            ]
        },
        {
            "id": "274c85fe-990e-49c8-bd44-2148cc5c58e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64bf61b2-c0e1-48a7-9575-962c60bef9b1",
            "compositeImage": {
                "id": "7f2c32ed-4b2f-4d22-94af-73148119838c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "274c85fe-990e-49c8-bd44-2148cc5c58e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "638db3cc-8624-430f-a3dc-17d2c563da18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "274c85fe-990e-49c8-bd44-2148cc5c58e6",
                    "LayerId": "bb185b09-82f1-4d6a-a2ae-3765927dd054"
                }
            ]
        },
        {
            "id": "545e9d9b-462b-45a6-aea7-8314d0316394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64bf61b2-c0e1-48a7-9575-962c60bef9b1",
            "compositeImage": {
                "id": "ea8620d6-72c4-4798-ac40-acfd5337c107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "545e9d9b-462b-45a6-aea7-8314d0316394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0afd517f-b040-4cd1-a69f-fcb23f992d50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "545e9d9b-462b-45a6-aea7-8314d0316394",
                    "LayerId": "bb185b09-82f1-4d6a-a2ae-3765927dd054"
                }
            ]
        },
        {
            "id": "da6f0e44-3a3a-48e2-8b4d-164663e8f5e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64bf61b2-c0e1-48a7-9575-962c60bef9b1",
            "compositeImage": {
                "id": "0e10f7c6-f8f4-4d3a-9884-1de597839a28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da6f0e44-3a3a-48e2-8b4d-164663e8f5e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8702baf9-6588-460f-bc26-49b2dd9df6d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da6f0e44-3a3a-48e2-8b4d-164663e8f5e0",
                    "LayerId": "bb185b09-82f1-4d6a-a2ae-3765927dd054"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "bb185b09-82f1-4d6a-a2ae-3765927dd054",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64bf61b2-c0e1-48a7-9575-962c60bef9b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}