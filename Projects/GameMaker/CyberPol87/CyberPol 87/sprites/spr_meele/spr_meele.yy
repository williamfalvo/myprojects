{
    "id": "fbc31252-06c8-4767-a2d5-7a694be1db26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_meele",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 8,
    "bbox_right": 50,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab117e16-6cb0-4aec-b1ae-02c2a35955d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc31252-06c8-4767-a2d5-7a694be1db26",
            "compositeImage": {
                "id": "e1931be5-1c36-4ffc-abb4-198a8a024a58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab117e16-6cb0-4aec-b1ae-02c2a35955d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17a58ca8-9b67-4828-adf2-5cbd7b343f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab117e16-6cb0-4aec-b1ae-02c2a35955d1",
                    "LayerId": "652bc7a1-074c-47ff-96f2-dcd6b7b6dccf"
                }
            ]
        },
        {
            "id": "65e2fe43-258a-4637-b6a2-0437d6f4b08b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc31252-06c8-4767-a2d5-7a694be1db26",
            "compositeImage": {
                "id": "43cb1cb1-639a-4e96-b09c-cbae3822bc21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65e2fe43-258a-4637-b6a2-0437d6f4b08b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d553919-3f19-4af2-b8c6-62c4b685d9f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65e2fe43-258a-4637-b6a2-0437d6f4b08b",
                    "LayerId": "652bc7a1-074c-47ff-96f2-dcd6b7b6dccf"
                }
            ]
        },
        {
            "id": "7fa7c081-6ea3-45d2-8ae2-6a7a2ef072c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc31252-06c8-4767-a2d5-7a694be1db26",
            "compositeImage": {
                "id": "2d888bc4-9c8d-4c5e-9bba-6859fa8d1bdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa7c081-6ea3-45d2-8ae2-6a7a2ef072c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e233936-caef-4829-80e4-8a6a884e30c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa7c081-6ea3-45d2-8ae2-6a7a2ef072c2",
                    "LayerId": "652bc7a1-074c-47ff-96f2-dcd6b7b6dccf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "652bc7a1-074c-47ff-96f2-dcd6b7b6dccf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbc31252-06c8-4767-a2d5-7a694be1db26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}