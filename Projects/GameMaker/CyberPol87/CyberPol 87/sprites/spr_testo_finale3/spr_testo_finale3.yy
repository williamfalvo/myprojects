{
    "id": "3f29ae6e-32cc-4fd8-9831-04559d5a7b21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_testo_finale3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 27,
    "bbox_right": 554,
    "bbox_top": 39,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "377aa647-b430-4200-9bbf-5409b0d8bad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f29ae6e-32cc-4fd8-9831-04559d5a7b21",
            "compositeImage": {
                "id": "64188714-8c95-478b-b572-016fa9492a37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "377aa647-b430-4200-9bbf-5409b0d8bad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88e043c1-4c67-48e6-9c71-101fbf109190",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "377aa647-b430-4200-9bbf-5409b0d8bad9",
                    "LayerId": "8d2bf848-64da-49a7-9670-ba6ea9265aea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 310,
    "layers": [
        {
            "id": "8d2bf848-64da-49a7-9670-ba6ea9265aea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f29ae6e-32cc-4fd8-9831-04559d5a7b21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 610,
    "xorig": 305,
    "yorig": 155
}