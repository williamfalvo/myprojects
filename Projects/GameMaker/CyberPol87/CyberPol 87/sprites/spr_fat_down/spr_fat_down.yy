{
    "id": "eec4b3b2-9130-4640-ac79-10cb21c11b2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fat_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 38,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09271d61-2a6a-4567-b533-d4b2e823b9ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eec4b3b2-9130-4640-ac79-10cb21c11b2c",
            "compositeImage": {
                "id": "e954b78f-530f-4b97-a17b-541c7ed6392f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09271d61-2a6a-4567-b533-d4b2e823b9ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "411d7935-3453-4056-ad45-62499ca2a54b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09271d61-2a6a-4567-b533-d4b2e823b9ca",
                    "LayerId": "46f28b2b-c6f9-4125-a004-0b92501d0e29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "46f28b2b-c6f9-4125-a004-0b92501d0e29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eec4b3b2-9130-4640-ac79-10cb21c11b2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}