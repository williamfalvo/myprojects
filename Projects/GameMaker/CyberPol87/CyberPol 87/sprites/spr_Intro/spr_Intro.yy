{
    "id": "5d961856-f47c-4c98-8593-aba9e77052a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Intro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4fd7230-d9a5-4f81-9378-1828cb9a06d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d961856-f47c-4c98-8593-aba9e77052a1",
            "compositeImage": {
                "id": "b73bbb9c-a0e1-4253-8184-b7ade71544a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4fd7230-d9a5-4f81-9378-1828cb9a06d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49580bd7-7999-4855-9d55-9ef25e7e6874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4fd7230-d9a5-4f81-9378-1828cb9a06d8",
                    "LayerId": "c955b411-a94e-4a02-9864-09aeb8aacb53"
                }
            ]
        },
        {
            "id": "b1f4cc3a-e45d-4058-b28f-a5eb5b300334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d961856-f47c-4c98-8593-aba9e77052a1",
            "compositeImage": {
                "id": "eff73e8c-2dda-49e8-b556-cc79ff956cd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1f4cc3a-e45d-4058-b28f-a5eb5b300334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b4e06d3-5225-489a-9a23-9828685d351b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1f4cc3a-e45d-4058-b28f-a5eb5b300334",
                    "LayerId": "c955b411-a94e-4a02-9864-09aeb8aacb53"
                }
            ]
        },
        {
            "id": "d94e7afa-6778-4753-8659-df2e2a2ed94c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d961856-f47c-4c98-8593-aba9e77052a1",
            "compositeImage": {
                "id": "a8133f4e-bcc1-40dc-b471-3d06ee62a660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d94e7afa-6778-4753-8659-df2e2a2ed94c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c450cfb2-861c-46a5-a188-fb0bd0883001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d94e7afa-6778-4753-8659-df2e2a2ed94c",
                    "LayerId": "c955b411-a94e-4a02-9864-09aeb8aacb53"
                }
            ]
        },
        {
            "id": "76356e86-2982-4317-8bcc-00ff7bfb0105",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d961856-f47c-4c98-8593-aba9e77052a1",
            "compositeImage": {
                "id": "bbf6db58-a6a2-4442-b987-e93564284a24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76356e86-2982-4317-8bcc-00ff7bfb0105",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f387904f-2217-4b25-80aa-a32a30e91fb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76356e86-2982-4317-8bcc-00ff7bfb0105",
                    "LayerId": "c955b411-a94e-4a02-9864-09aeb8aacb53"
                }
            ]
        },
        {
            "id": "60611e53-e4f4-4e58-9a8e-eb7c7093d5be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d961856-f47c-4c98-8593-aba9e77052a1",
            "compositeImage": {
                "id": "cf57e398-7bd1-4446-b1b2-16050d8407fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60611e53-e4f4-4e58-9a8e-eb7c7093d5be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6c6ce47-cd98-4267-9151-ad6f94569c24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60611e53-e4f4-4e58-9a8e-eb7c7093d5be",
                    "LayerId": "c955b411-a94e-4a02-9864-09aeb8aacb53"
                }
            ]
        },
        {
            "id": "28b55bde-526e-41a9-a07b-f9f32d4b01b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d961856-f47c-4c98-8593-aba9e77052a1",
            "compositeImage": {
                "id": "e33d6cc1-bf92-4c33-9e58-066214f5aa29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28b55bde-526e-41a9-a07b-f9f32d4b01b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ea036c5-7115-4101-9f3e-44f9ad07bcce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28b55bde-526e-41a9-a07b-f9f32d4b01b4",
                    "LayerId": "c955b411-a94e-4a02-9864-09aeb8aacb53"
                }
            ]
        },
        {
            "id": "56e0ffb7-a60f-4b20-8ac5-ada54d1ca432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d961856-f47c-4c98-8593-aba9e77052a1",
            "compositeImage": {
                "id": "fd8cb7b1-fb93-438e-9418-69ca2a343477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e0ffb7-a60f-4b20-8ac5-ada54d1ca432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b76f5790-19a5-40d6-8216-b4a33e340fc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e0ffb7-a60f-4b20-8ac5-ada54d1ca432",
                    "LayerId": "c955b411-a94e-4a02-9864-09aeb8aacb53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "c955b411-a94e-4a02-9864-09aeb8aacb53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d961856-f47c-4c98-8593-aba9e77052a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 180
}