{
    "id": "213592d9-7884-48a6-a698-5a23333f8c4f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_rightDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 17,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aaba6327-0a16-44bc-93e1-b31b31168cb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "213592d9-7884-48a6-a698-5a23333f8c4f",
            "compositeImage": {
                "id": "df0341cc-9e3f-4ab8-9625-f45a780dcbe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaba6327-0a16-44bc-93e1-b31b31168cb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d5a87d8-dd31-4fa9-ab3b-a46dc71f42ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaba6327-0a16-44bc-93e1-b31b31168cb8",
                    "LayerId": "6ba0ab12-eacc-43aa-9d7c-cfaa33b9111a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "6ba0ab12-eacc-43aa-9d7c-cfaa33b9111a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "213592d9-7884-48a6-a698-5a23333f8c4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}