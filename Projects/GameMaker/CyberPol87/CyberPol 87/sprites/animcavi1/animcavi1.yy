{
    "id": "287a786f-4f9d-4592-ba8a-c5096c569716",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "animcavi1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60fcb9a8-af01-40b5-b3dc-b9323fa05261",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "287a786f-4f9d-4592-ba8a-c5096c569716",
            "compositeImage": {
                "id": "2a5dd4df-4b3b-4191-ab75-bcd048d58c46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60fcb9a8-af01-40b5-b3dc-b9323fa05261",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9330f102-317e-4724-b7bb-0a781e2b25d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60fcb9a8-af01-40b5-b3dc-b9323fa05261",
                    "LayerId": "fd85c3d5-7c03-4930-a920-5ed9da2caf22"
                }
            ]
        },
        {
            "id": "64ad027f-b95a-4f94-8421-aeb96872236c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "287a786f-4f9d-4592-ba8a-c5096c569716",
            "compositeImage": {
                "id": "b7997e1a-5a4b-41d6-ba08-c4c2a04b36c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64ad027f-b95a-4f94-8421-aeb96872236c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5196f8c-67b4-4be5-9315-4c8167cc435a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64ad027f-b95a-4f94-8421-aeb96872236c",
                    "LayerId": "fd85c3d5-7c03-4930-a920-5ed9da2caf22"
                }
            ]
        },
        {
            "id": "5d466100-cfa1-4cba-9258-aa9daa93fcce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "287a786f-4f9d-4592-ba8a-c5096c569716",
            "compositeImage": {
                "id": "3df24b37-7950-47cd-bcfe-aca57a52599a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d466100-cfa1-4cba-9258-aa9daa93fcce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77a2702f-8d7c-492c-aa8e-7a795c9f4d6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d466100-cfa1-4cba-9258-aa9daa93fcce",
                    "LayerId": "fd85c3d5-7c03-4930-a920-5ed9da2caf22"
                }
            ]
        },
        {
            "id": "4cfc0a7c-86c3-4afe-9664-9d3aedac14ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "287a786f-4f9d-4592-ba8a-c5096c569716",
            "compositeImage": {
                "id": "424af883-69c8-4628-a457-54acff2d89c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cfc0a7c-86c3-4afe-9664-9d3aedac14ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4c543a6-cb6a-404f-80b1-2fe35a91324c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cfc0a7c-86c3-4afe-9664-9d3aedac14ab",
                    "LayerId": "fd85c3d5-7c03-4930-a920-5ed9da2caf22"
                }
            ]
        },
        {
            "id": "b9606ca5-5cc3-41b8-94f7-21f0498d9fbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "287a786f-4f9d-4592-ba8a-c5096c569716",
            "compositeImage": {
                "id": "b40f1371-9704-429c-b9ca-a1516a8becfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9606ca5-5cc3-41b8-94f7-21f0498d9fbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24b1bebb-40e4-411c-8732-eee3bccc3f4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9606ca5-5cc3-41b8-94f7-21f0498d9fbf",
                    "LayerId": "fd85c3d5-7c03-4930-a920-5ed9da2caf22"
                }
            ]
        },
        {
            "id": "d9ae6763-c989-4f15-9a15-0f917f6514cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "287a786f-4f9d-4592-ba8a-c5096c569716",
            "compositeImage": {
                "id": "4d6a6a25-3781-4215-8b82-abb5295503b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9ae6763-c989-4f15-9a15-0f917f6514cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f32ff98-83de-43cd-ab50-974a78f2a433",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9ae6763-c989-4f15-9a15-0f917f6514cd",
                    "LayerId": "fd85c3d5-7c03-4930-a920-5ed9da2caf22"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fd85c3d5-7c03-4930-a920-5ed9da2caf22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "287a786f-4f9d-4592-ba8a-c5096c569716",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}