{
    "id": "d41fe891-edb6-49fb-85fc-9db29c26e1dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet_big",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4d7c7b2-7cdd-4c63-a50d-3bc93cfb74dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d41fe891-edb6-49fb-85fc-9db29c26e1dc",
            "compositeImage": {
                "id": "e6f813dd-ffbd-470c-a734-b971cc1c073b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4d7c7b2-7cdd-4c63-a50d-3bc93cfb74dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ed1eb00-0a57-4aa0-a63b-2cbdd3ff03a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4d7c7b2-7cdd-4c63-a50d-3bc93cfb74dc",
                    "LayerId": "76799646-2b62-4dbd-8ec0-10e5326ce199"
                }
            ]
        },
        {
            "id": "57b6169a-1432-41f0-ae5a-f50f3fdd1f74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d41fe891-edb6-49fb-85fc-9db29c26e1dc",
            "compositeImage": {
                "id": "dfdc73c5-6bda-4b77-9e4c-a46b2ff77782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57b6169a-1432-41f0-ae5a-f50f3fdd1f74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9213dbe2-9700-4ce0-8f72-a1d9934f1248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57b6169a-1432-41f0-ae5a-f50f3fdd1f74",
                    "LayerId": "76799646-2b62-4dbd-8ec0-10e5326ce199"
                }
            ]
        },
        {
            "id": "a5104e9c-25dd-48ee-8d6f-694b247fd206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d41fe891-edb6-49fb-85fc-9db29c26e1dc",
            "compositeImage": {
                "id": "916ed1cd-ba59-4d47-b7ff-3d7f40e8028b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5104e9c-25dd-48ee-8d6f-694b247fd206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "921384d7-20dc-402b-88a7-37781d533451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5104e9c-25dd-48ee-8d6f-694b247fd206",
                    "LayerId": "76799646-2b62-4dbd-8ec0-10e5326ce199"
                }
            ]
        },
        {
            "id": "3cd4bb2e-a8c7-4b95-82c1-57c180f48190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d41fe891-edb6-49fb-85fc-9db29c26e1dc",
            "compositeImage": {
                "id": "08dc5bed-342c-4cc6-9f37-15c01717321f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cd4bb2e-a8c7-4b95-82c1-57c180f48190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a61beb0-10d4-40c9-aece-47be81e1dcbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cd4bb2e-a8c7-4b95-82c1-57c180f48190",
                    "LayerId": "76799646-2b62-4dbd-8ec0-10e5326ce199"
                }
            ]
        },
        {
            "id": "ee652731-3e9c-44e8-99da-c07a29192bd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d41fe891-edb6-49fb-85fc-9db29c26e1dc",
            "compositeImage": {
                "id": "54ff23d5-9005-493e-af00-8a0fef924a52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee652731-3e9c-44e8-99da-c07a29192bd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91a1819b-02ff-43fa-ad4e-6c10a89b67ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee652731-3e9c-44e8-99da-c07a29192bd4",
                    "LayerId": "76799646-2b62-4dbd-8ec0-10e5326ce199"
                }
            ]
        },
        {
            "id": "b6701238-c2ba-4f05-9293-a228b034f110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d41fe891-edb6-49fb-85fc-9db29c26e1dc",
            "compositeImage": {
                "id": "92de3b31-0798-4add-a43b-8436729b20b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6701238-c2ba-4f05-9293-a228b034f110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29774f0c-a58a-49ad-bd06-30422dd73ffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6701238-c2ba-4f05-9293-a228b034f110",
                    "LayerId": "76799646-2b62-4dbd-8ec0-10e5326ce199"
                }
            ]
        },
        {
            "id": "1faeb0a5-3236-400a-81fc-b1e3070b7091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d41fe891-edb6-49fb-85fc-9db29c26e1dc",
            "compositeImage": {
                "id": "2acfbaf0-bb40-4d95-a653-c673538fa52c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1faeb0a5-3236-400a-81fc-b1e3070b7091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a58b48a-0160-4828-861f-7b248850d37f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1faeb0a5-3236-400a-81fc-b1e3070b7091",
                    "LayerId": "76799646-2b62-4dbd-8ec0-10e5326ce199"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "76799646-2b62-4dbd-8ec0-10e5326ce199",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d41fe891-edb6-49fb-85fc-9db29c26e1dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}