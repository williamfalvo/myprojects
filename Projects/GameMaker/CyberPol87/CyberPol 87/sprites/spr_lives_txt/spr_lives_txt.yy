{
    "id": "816a2943-93a9-4162-8ca2-f68e86ef08a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lives_txt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 126,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6442fc00-d7ba-4497-82ea-8478445bc4a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "816a2943-93a9-4162-8ca2-f68e86ef08a4",
            "compositeImage": {
                "id": "2a0d8ec7-d1af-450b-8738-94c3a47868f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6442fc00-d7ba-4497-82ea-8478445bc4a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e494486a-e61d-41c7-b3e6-1ff508f2a3f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6442fc00-d7ba-4497-82ea-8478445bc4a7",
                    "LayerId": "896c13e0-8852-4844-9dc6-5b4a6e9f426e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "896c13e0-8852-4844-9dc6-5b4a6e9f426e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "816a2943-93a9-4162-8ca2-f68e86ef08a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 16
}