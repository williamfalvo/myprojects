{
    "id": "28c8f97e-b246-483d-86d2-1074cb9a8154",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b639678a-55a3-442e-a20e-12dc2e63f602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28c8f97e-b246-483d-86d2-1074cb9a8154",
            "compositeImage": {
                "id": "cf7d2e0c-fe53-466b-96d1-5b0849c9c2e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b639678a-55a3-442e-a20e-12dc2e63f602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e15a273-8cba-4f81-82a7-ed26218c55fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b639678a-55a3-442e-a20e-12dc2e63f602",
                    "LayerId": "69306350-3a0d-4118-8478-c785da91288c"
                }
            ]
        },
        {
            "id": "1315ff6a-15fa-4a31-b9f3-583a425e6b61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28c8f97e-b246-483d-86d2-1074cb9a8154",
            "compositeImage": {
                "id": "b8a90315-062e-4773-bb92-ecf5f741fc39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1315ff6a-15fa-4a31-b9f3-583a425e6b61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc71d891-7f6c-4c58-9535-10c0150fff7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1315ff6a-15fa-4a31-b9f3-583a425e6b61",
                    "LayerId": "69306350-3a0d-4118-8478-c785da91288c"
                }
            ]
        },
        {
            "id": "a691a559-f9e4-4df2-b975-05ae3462ec2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28c8f97e-b246-483d-86d2-1074cb9a8154",
            "compositeImage": {
                "id": "3a46c771-fc49-493a-8587-21aab331e860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a691a559-f9e4-4df2-b975-05ae3462ec2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a21ced28-c8f5-4a4f-8c55-046471461869",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a691a559-f9e4-4df2-b975-05ae3462ec2a",
                    "LayerId": "69306350-3a0d-4118-8478-c785da91288c"
                }
            ]
        },
        {
            "id": "561614a7-5812-4829-8df3-ca3eae9644d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28c8f97e-b246-483d-86d2-1074cb9a8154",
            "compositeImage": {
                "id": "f908d587-79f0-4273-aaf4-02d15ef4cb38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "561614a7-5812-4829-8df3-ca3eae9644d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f06cc08c-9c8c-445d-a1f1-fbd1e81df38c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "561614a7-5812-4829-8df3-ca3eae9644d8",
                    "LayerId": "69306350-3a0d-4118-8478-c785da91288c"
                }
            ]
        },
        {
            "id": "16523de0-14d4-4953-bb4d-ad1fd513daf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28c8f97e-b246-483d-86d2-1074cb9a8154",
            "compositeImage": {
                "id": "372a0882-f4a4-42f8-b06b-f6d6fbbe50d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16523de0-14d4-4953-bb4d-ad1fd513daf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "126ba0f5-e0e6-4efb-a7b0-c3b9130adfbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16523de0-14d4-4953-bb4d-ad1fd513daf0",
                    "LayerId": "69306350-3a0d-4118-8478-c785da91288c"
                }
            ]
        },
        {
            "id": "cfda85d6-b332-4a6d-b52f-78f12bcdd13f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28c8f97e-b246-483d-86d2-1074cb9a8154",
            "compositeImage": {
                "id": "b771f636-ff46-43d4-bb0e-3c5b889227f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfda85d6-b332-4a6d-b52f-78f12bcdd13f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36732de5-61b0-4514-b5ec-aa55ed464b1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfda85d6-b332-4a6d-b52f-78f12bcdd13f",
                    "LayerId": "69306350-3a0d-4118-8478-c785da91288c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "69306350-3a0d-4118-8478-c785da91288c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28c8f97e-b246-483d-86d2-1074cb9a8154",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}