{
    "id": "516e78f4-62a8-4945-b4e1-9fc5b5d87781",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_big_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 11,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4d6d2d1-0339-4e9e-b9b3-cfdfb8b64470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516e78f4-62a8-4945-b4e1-9fc5b5d87781",
            "compositeImage": {
                "id": "0ca0db98-af40-4b2d-8161-7f0f738889ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4d6d2d1-0339-4e9e-b9b3-cfdfb8b64470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34862185-0383-4e47-b26d-a4f49d2598a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4d6d2d1-0339-4e9e-b9b3-cfdfb8b64470",
                    "LayerId": "010b7fa7-f615-47ca-aff9-15ea2f94e9de"
                }
            ]
        },
        {
            "id": "27498a53-81af-452e-8750-3fba530e48fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516e78f4-62a8-4945-b4e1-9fc5b5d87781",
            "compositeImage": {
                "id": "6970f1ef-1292-4cd9-97da-7638b3b70aad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27498a53-81af-452e-8750-3fba530e48fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ff645d3-2ed1-49c9-bbb5-a93befd23f57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27498a53-81af-452e-8750-3fba530e48fd",
                    "LayerId": "010b7fa7-f615-47ca-aff9-15ea2f94e9de"
                }
            ]
        },
        {
            "id": "b00f0b10-2195-4c74-8e9e-c8ccaeffc5fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516e78f4-62a8-4945-b4e1-9fc5b5d87781",
            "compositeImage": {
                "id": "8542cb00-a507-43c6-922c-e798d1b61dd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b00f0b10-2195-4c74-8e9e-c8ccaeffc5fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eaab28a-c0a0-4322-b24e-f91e4b0f1f69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b00f0b10-2195-4c74-8e9e-c8ccaeffc5fb",
                    "LayerId": "010b7fa7-f615-47ca-aff9-15ea2f94e9de"
                }
            ]
        },
        {
            "id": "41a58b34-4415-4cf1-8d60-62ea8f740c17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516e78f4-62a8-4945-b4e1-9fc5b5d87781",
            "compositeImage": {
                "id": "ce51e124-52eb-4148-8d59-14398c480b97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a58b34-4415-4cf1-8d60-62ea8f740c17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "303899a8-9d26-495a-90c4-02641a0e5c9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a58b34-4415-4cf1-8d60-62ea8f740c17",
                    "LayerId": "010b7fa7-f615-47ca-aff9-15ea2f94e9de"
                }
            ]
        },
        {
            "id": "3c34c10a-d0ed-49eb-aac5-261973f0a7e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516e78f4-62a8-4945-b4e1-9fc5b5d87781",
            "compositeImage": {
                "id": "2da16191-dc3b-4ab1-8072-0fafc1e9e007",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c34c10a-d0ed-49eb-aac5-261973f0a7e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14742c79-d0c3-4a98-9fd0-570eb9f785a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c34c10a-d0ed-49eb-aac5-261973f0a7e6",
                    "LayerId": "010b7fa7-f615-47ca-aff9-15ea2f94e9de"
                }
            ]
        },
        {
            "id": "74c857c0-41e9-4387-90cd-db93b54ac03b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516e78f4-62a8-4945-b4e1-9fc5b5d87781",
            "compositeImage": {
                "id": "b3be8c38-d7fc-4785-a923-0712e21e9dbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74c857c0-41e9-4387-90cd-db93b54ac03b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5e89e56-2925-4d3e-9ba9-4e123389eca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74c857c0-41e9-4387-90cd-db93b54ac03b",
                    "LayerId": "010b7fa7-f615-47ca-aff9-15ea2f94e9de"
                }
            ]
        },
        {
            "id": "d402f3e6-849e-4130-8a2f-9fbabe09da68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516e78f4-62a8-4945-b4e1-9fc5b5d87781",
            "compositeImage": {
                "id": "510fcc34-0270-49a4-a9f9-7436f3f784df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d402f3e6-849e-4130-8a2f-9fbabe09da68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01f8ac38-78ef-4812-8e0c-ec22b551c815",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d402f3e6-849e-4130-8a2f-9fbabe09da68",
                    "LayerId": "010b7fa7-f615-47ca-aff9-15ea2f94e9de"
                }
            ]
        },
        {
            "id": "5f645d0e-0047-44bf-bb39-72a9e8c04686",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "516e78f4-62a8-4945-b4e1-9fc5b5d87781",
            "compositeImage": {
                "id": "025d791e-6637-4431-a1d3-cd5ffcfdeae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f645d0e-0047-44bf-bb39-72a9e8c04686",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57480d6e-696c-4803-9e82-eaca649038bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f645d0e-0047-44bf-bb39-72a9e8c04686",
                    "LayerId": "010b7fa7-f615-47ca-aff9-15ea2f94e9de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "010b7fa7-f615-47ca-aff9-15ea2f94e9de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "516e78f4-62a8-4945-b4e1-9fc5b5d87781",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}