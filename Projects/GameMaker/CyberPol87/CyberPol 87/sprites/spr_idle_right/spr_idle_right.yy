{
    "id": "08e1ff3b-1b61-4a01-b937-79783110f774",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 16,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d317dcd8-8d2a-423b-a623-31c9a75a61d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08e1ff3b-1b61-4a01-b937-79783110f774",
            "compositeImage": {
                "id": "fa0aa72f-66c0-47a1-aa1b-2620b25d7ce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d317dcd8-8d2a-423b-a623-31c9a75a61d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2df17534-7bc6-49a6-89fd-e6c483377b7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d317dcd8-8d2a-423b-a623-31c9a75a61d7",
                    "LayerId": "f74ad397-7770-4ee5-8e6f-1ff1bbe4d59a"
                }
            ]
        },
        {
            "id": "d31901ab-837b-4a2b-a1d0-f7191c031179",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08e1ff3b-1b61-4a01-b937-79783110f774",
            "compositeImage": {
                "id": "b6faddcb-52ce-4305-a3af-c226a4517d90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d31901ab-837b-4a2b-a1d0-f7191c031179",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa1ac261-7b7d-460a-b652-01ce00abc017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d31901ab-837b-4a2b-a1d0-f7191c031179",
                    "LayerId": "f74ad397-7770-4ee5-8e6f-1ff1bbe4d59a"
                }
            ]
        },
        {
            "id": "2b54bfab-f302-42c8-a474-3b2ead3a62c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08e1ff3b-1b61-4a01-b937-79783110f774",
            "compositeImage": {
                "id": "596a1858-6ad8-4673-92b2-182ab65dea83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b54bfab-f302-42c8-a474-3b2ead3a62c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d94d3db6-14bb-478b-8f68-bf33c87b83b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b54bfab-f302-42c8-a474-3b2ead3a62c6",
                    "LayerId": "f74ad397-7770-4ee5-8e6f-1ff1bbe4d59a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "f74ad397-7770-4ee5-8e6f-1ff1bbe4d59a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08e1ff3b-1b61-4a01-b937-79783110f774",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}