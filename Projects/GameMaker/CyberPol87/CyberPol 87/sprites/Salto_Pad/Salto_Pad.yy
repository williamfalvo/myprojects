{
    "id": "f7c1102e-c23f-435a-a32b-31dbcbfdfde9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Salto_Pad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7ec1e16-445c-420c-8245-593d4a81ac13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7c1102e-c23f-435a-a32b-31dbcbfdfde9",
            "compositeImage": {
                "id": "a1983c76-b15d-4cd1-91a9-503086f80b18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7ec1e16-445c-420c-8245-593d4a81ac13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bf7be3b-96d0-4ca4-85e7-73ea76a795bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7ec1e16-445c-420c-8245-593d4a81ac13",
                    "LayerId": "1bcceaca-37c8-49ce-be05-fa4bcb988ffa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "1bcceaca-37c8-49ce-be05-fa4bcb988ffa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7c1102e-c23f-435a-a32b-31dbcbfdfde9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}