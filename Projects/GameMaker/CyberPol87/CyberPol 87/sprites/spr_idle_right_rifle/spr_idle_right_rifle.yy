{
    "id": "9f7b65ab-7a21-42fb-8940-4867df6e5d81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle_right_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 16,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f346c3c4-0959-4b9c-8f9d-6b4b6e741127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f7b65ab-7a21-42fb-8940-4867df6e5d81",
            "compositeImage": {
                "id": "3016e81a-21c2-4c3c-8d11-7623f243d209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f346c3c4-0959-4b9c-8f9d-6b4b6e741127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68636a1d-c2a4-4c70-8eb5-21c011896468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f346c3c4-0959-4b9c-8f9d-6b4b6e741127",
                    "LayerId": "7f17a72c-3cbc-4614-b737-7fe4bdf3f621"
                }
            ]
        },
        {
            "id": "b3c31095-5fdc-48e0-a26c-cb958c600a4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f7b65ab-7a21-42fb-8940-4867df6e5d81",
            "compositeImage": {
                "id": "f6e26580-e312-46e0-8770-bbc84cba517c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3c31095-5fdc-48e0-a26c-cb958c600a4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10e5afe6-bb1d-4da9-b32a-19882caec273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3c31095-5fdc-48e0-a26c-cb958c600a4d",
                    "LayerId": "7f17a72c-3cbc-4614-b737-7fe4bdf3f621"
                }
            ]
        },
        {
            "id": "756d5495-ac83-47ce-9ab5-2843e3117e31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f7b65ab-7a21-42fb-8940-4867df6e5d81",
            "compositeImage": {
                "id": "c82c0d25-e92b-4e5d-b2a4-1b15c5624f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "756d5495-ac83-47ce-9ab5-2843e3117e31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7de3f037-68c7-4b7a-838c-e4ddf0f5eac9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "756d5495-ac83-47ce-9ab5-2843e3117e31",
                    "LayerId": "7f17a72c-3cbc-4614-b737-7fe4bdf3f621"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "7f17a72c-3cbc-4614-b737-7fe4bdf3f621",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f7b65ab-7a21-42fb-8940-4867df6e5d81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}