{
    "id": "aa768fec-9114-4cdc-9d36-18d1b84a26e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Ospedale_Luci",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 1,
    "bbox_right": 190,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "645313dc-33c6-417b-806c-7ba9a1078024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa768fec-9114-4cdc-9d36-18d1b84a26e4",
            "compositeImage": {
                "id": "6fc6b6e8-f474-4a72-b306-86fc41cdca37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "645313dc-33c6-417b-806c-7ba9a1078024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13025cab-c3dd-4113-8644-3cf6560c905f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "645313dc-33c6-417b-806c-7ba9a1078024",
                    "LayerId": "873837ee-13bf-44f7-8916-5924916212fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "873837ee-13bf-44f7-8916-5924916212fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa768fec-9114-4cdc-9d36-18d1b84a26e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}