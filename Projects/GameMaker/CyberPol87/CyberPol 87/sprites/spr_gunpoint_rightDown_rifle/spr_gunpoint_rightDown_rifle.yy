{
    "id": "bf5df187-34e5-47e7-9975-42b8cd3e100a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_rightDown_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 16,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70494caf-108c-4d36-a3a9-ce27b1748fdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf5df187-34e5-47e7-9975-42b8cd3e100a",
            "compositeImage": {
                "id": "a71bf80c-53d2-4cfe-b225-a703655a56d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70494caf-108c-4d36-a3a9-ce27b1748fdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa17439d-f386-48e3-ab51-2e6ca4423366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70494caf-108c-4d36-a3a9-ce27b1748fdf",
                    "LayerId": "02f89f4a-56b5-4d82-9901-f9925318a1eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "02f89f4a-56b5-4d82-9901-f9925318a1eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf5df187-34e5-47e7-9975-42b8cd3e100a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}