{
    "id": "458aec77-8212-4fde-9d25-93d2a9b36c91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Finale",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a1a8b0d-e6cd-4e99-a634-b8351b92fb83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "458aec77-8212-4fde-9d25-93d2a9b36c91",
            "compositeImage": {
                "id": "5dd10fe4-bf29-426e-97f4-9c3e488c51c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a1a8b0d-e6cd-4e99-a634-b8351b92fb83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5fc5c58-8238-4dfc-abb0-67b18da04fdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a1a8b0d-e6cd-4e99-a634-b8351b92fb83",
                    "LayerId": "df9287f9-d0fd-4317-bde4-ed3074092d74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "df9287f9-d0fd-4317-bde4-ed3074092d74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "458aec77-8212-4fde-9d25-93d2a9b36c91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}