{
    "id": "6fded0fd-0749-46ab-a895-da0488009f11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mobile_destra1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac63a513-08c7-4d0e-9225-771171167d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fded0fd-0749-46ab-a895-da0488009f11",
            "compositeImage": {
                "id": "25fba093-71e4-42ed-94c3-9651592be95c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac63a513-08c7-4d0e-9225-771171167d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edd9ae4a-049b-41ab-ac79-6d7f4e2fbe98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac63a513-08c7-4d0e-9225-771171167d3a",
                    "LayerId": "1365d50e-686f-4dc0-a76c-b637af3a73d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1365d50e-686f-4dc0-a76c-b637af3a73d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fded0fd-0749-46ab-a895-da0488009f11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}