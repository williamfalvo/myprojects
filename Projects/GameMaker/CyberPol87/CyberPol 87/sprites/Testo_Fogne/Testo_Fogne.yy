{
    "id": "bfdb8321-c53b-48fd-88eb-d508f8d7bb74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Testo_Fogne",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 6,
    "bbox_right": 591,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b4eed9f-c74f-4c98-a50c-e0665e2a003c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfdb8321-c53b-48fd-88eb-d508f8d7bb74",
            "compositeImage": {
                "id": "9aedfe3d-a8a6-48f1-bd2c-ae78ceadee67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b4eed9f-c74f-4c98-a50c-e0665e2a003c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "826a1f42-3bc3-45d8-9e52-75772af5b106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b4eed9f-c74f-4c98-a50c-e0665e2a003c",
                    "LayerId": "af309499-0a58-4317-9d98-b8f13842aa33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "af309499-0a58-4317-9d98-b8f13842aa33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfdb8321-c53b-48fd-88eb-d508f8d7bb74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 610,
    "xorig": 305,
    "yorig": 55
}