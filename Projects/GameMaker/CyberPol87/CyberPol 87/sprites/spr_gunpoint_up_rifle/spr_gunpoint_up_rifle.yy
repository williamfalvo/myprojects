{
    "id": "d199f782-0813-4278-bf76-31e47439169a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_up_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 16,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "220cfd79-e7bc-4cbe-b13b-b6b5c0df1e90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d199f782-0813-4278-bf76-31e47439169a",
            "compositeImage": {
                "id": "d78af07b-ce2a-4946-b83f-ed43c6280eeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "220cfd79-e7bc-4cbe-b13b-b6b5c0df1e90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f22961c-d14e-4ef5-b66f-bce6f4a9ee76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "220cfd79-e7bc-4cbe-b13b-b6b5c0df1e90",
                    "LayerId": "4cab07f2-3e9f-4004-ab14-b0518e991ca3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "4cab07f2-3e9f-4004-ab14-b0518e991ca3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d199f782-0813-4278-bf76-31e47439169a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}