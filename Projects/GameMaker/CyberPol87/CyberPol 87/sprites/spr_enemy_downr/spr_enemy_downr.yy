{
    "id": "12a91e67-9e4f-4357-a736-a42e9e120cd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_downr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 47,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9d23a4d-c65a-4d47-ac59-db59a4507259",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12a91e67-9e4f-4357-a736-a42e9e120cd1",
            "compositeImage": {
                "id": "d00bbc92-675c-4b86-beb7-2a6c3303302d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d23a4d-c65a-4d47-ac59-db59a4507259",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7caa396-9cff-4419-b28b-6f7fbef38cca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d23a4d-c65a-4d47-ac59-db59a4507259",
                    "LayerId": "bf07050b-c5b2-4da4-81f5-0e72a7622647"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bf07050b-c5b2-4da4-81f5-0e72a7622647",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12a91e67-9e4f-4357-a736-a42e9e120cd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}