{
    "id": "27056b1c-ed2c-4976-9b29-a57a48fc12c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fat_upr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 46,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db1f9a04-8fdc-4763-81b6-c10cb4bb5e6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27056b1c-ed2c-4976-9b29-a57a48fc12c3",
            "compositeImage": {
                "id": "973dece8-6be2-4a63-b118-666e4e5914a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db1f9a04-8fdc-4763-81b6-c10cb4bb5e6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "087aff6d-06c3-4f54-98c6-35c6c1745355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db1f9a04-8fdc-4763-81b6-c10cb4bb5e6b",
                    "LayerId": "b2062d2b-a681-48c3-984e-d6ebb5f0fdff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b2062d2b-a681-48c3-984e-d6ebb5f0fdff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27056b1c-ed2c-4976-9b29-a57a48fc12c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}