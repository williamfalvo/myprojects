{
    "id": "ce52665a-c136-4d56-a436-12a4712e0185",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "RadioactiveBarrel_2_0_Animation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 9,
    "bbox_right": 46,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0de60583-bf03-442f-980f-9ad2d5d8b62a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce52665a-c136-4d56-a436-12a4712e0185",
            "compositeImage": {
                "id": "314f7f5c-0783-471f-a2ca-f7f4d4e3972f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0de60583-bf03-442f-980f-9ad2d5d8b62a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffbbe1ed-7dde-4560-b8c3-966d7ff1cce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0de60583-bf03-442f-980f-9ad2d5d8b62a",
                    "LayerId": "385d4680-0d9c-4b17-821c-71232facdf81"
                }
            ]
        },
        {
            "id": "a0f1bb5a-b709-473e-bd1e-8f8ec6336447",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce52665a-c136-4d56-a436-12a4712e0185",
            "compositeImage": {
                "id": "f5824603-8177-454a-b3f8-a93f5070884f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0f1bb5a-b709-473e-bd1e-8f8ec6336447",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29e560ab-4a10-4555-9c56-b57166e23711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0f1bb5a-b709-473e-bd1e-8f8ec6336447",
                    "LayerId": "385d4680-0d9c-4b17-821c-71232facdf81"
                }
            ]
        },
        {
            "id": "16751b3d-29ad-4423-bd59-93a1650d8130",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce52665a-c136-4d56-a436-12a4712e0185",
            "compositeImage": {
                "id": "9a272288-7de8-4a3d-94e4-b9dc1c83a361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16751b3d-29ad-4423-bd59-93a1650d8130",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a370119-0dde-4aca-bbf6-b7de4516bfa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16751b3d-29ad-4423-bd59-93a1650d8130",
                    "LayerId": "385d4680-0d9c-4b17-821c-71232facdf81"
                }
            ]
        },
        {
            "id": "ef5f4d2b-7cc1-4d74-b7c1-766f9d1645fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce52665a-c136-4d56-a436-12a4712e0185",
            "compositeImage": {
                "id": "ac095bea-6782-4ac4-a0f9-1f9033819c92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef5f4d2b-7cc1-4d74-b7c1-766f9d1645fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d914c0e1-d2f1-45ad-90ef-bfc74d5c6f62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef5f4d2b-7cc1-4d74-b7c1-766f9d1645fd",
                    "LayerId": "385d4680-0d9c-4b17-821c-71232facdf81"
                }
            ]
        },
        {
            "id": "74894010-067e-4eee-9707-e941f36969a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce52665a-c136-4d56-a436-12a4712e0185",
            "compositeImage": {
                "id": "d94e7175-650a-48a0-b3df-f95d9efed95c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74894010-067e-4eee-9707-e941f36969a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01c6cba5-4291-461f-b6a1-4b4e560a080a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74894010-067e-4eee-9707-e941f36969a1",
                    "LayerId": "385d4680-0d9c-4b17-821c-71232facdf81"
                }
            ]
        },
        {
            "id": "5a2f0d4e-8040-4209-a00b-6c1dcbaf06ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce52665a-c136-4d56-a436-12a4712e0185",
            "compositeImage": {
                "id": "74b23e73-e170-4794-8ded-50e56ea6c140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a2f0d4e-8040-4209-a00b-6c1dcbaf06ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b34ff10-365d-460d-86e3-9a30b7bef846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a2f0d4e-8040-4209-a00b-6c1dcbaf06ce",
                    "LayerId": "385d4680-0d9c-4b17-821c-71232facdf81"
                }
            ]
        },
        {
            "id": "bd4c1437-e965-4c5d-a1c0-72c1b5623efd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce52665a-c136-4d56-a436-12a4712e0185",
            "compositeImage": {
                "id": "9e8c76de-0b98-4f61-96e5-15346dc43014",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd4c1437-e965-4c5d-a1c0-72c1b5623efd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9012ea43-9254-4b7b-8839-ff2052ebbc4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd4c1437-e965-4c5d-a1c0-72c1b5623efd",
                    "LayerId": "385d4680-0d9c-4b17-821c-71232facdf81"
                }
            ]
        },
        {
            "id": "0afb1ce6-c671-46bf-9f04-94abc017bcbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce52665a-c136-4d56-a436-12a4712e0185",
            "compositeImage": {
                "id": "857c1ced-09aa-46ac-8b00-8dc565da01bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0afb1ce6-c671-46bf-9f04-94abc017bcbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "404b3df6-5c16-4358-9ef7-41991570f6f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0afb1ce6-c671-46bf-9f04-94abc017bcbb",
                    "LayerId": "385d4680-0d9c-4b17-821c-71232facdf81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "385d4680-0d9c-4b17-821c-71232facdf81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce52665a-c136-4d56-a436-12a4712e0185",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}