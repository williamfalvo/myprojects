{
    "id": "3fdd4791-4226-4130-82fa-bec716589a01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Discesa_Pad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e287e45-3f42-41f8-9859-b5780e70661c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fdd4791-4226-4130-82fa-bec716589a01",
            "compositeImage": {
                "id": "2646708e-554e-418c-88e6-1a38bdf7ad28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e287e45-3f42-41f8-9859-b5780e70661c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55b8e317-4388-4b87-b88f-8a3b4cfe2983",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e287e45-3f42-41f8-9859-b5780e70661c",
                    "LayerId": "de27e7d6-2682-4b47-8615-84184593d328"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "de27e7d6-2682-4b47-8615-84184593d328",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fdd4791-4226-4130-82fa-bec716589a01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}