{
    "id": "ed63815b-32c2-42c5-8a6e-e080dd9e8f4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_ultra_finale_del_destino_distruttore",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50895365-1b57-4a3a-9ea5-cf24fa3c7ec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed63815b-32c2-42c5-8a6e-e080dd9e8f4b",
            "compositeImage": {
                "id": "e6bf636f-dca9-48e1-8d02-e89c05f20c59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50895365-1b57-4a3a-9ea5-cf24fa3c7ec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f57391e3-15b4-4ef6-85c7-df410958b90f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50895365-1b57-4a3a-9ea5-cf24fa3c7ec9",
                    "LayerId": "9cf5c228-a398-4234-93b2-7e0bbdcd77f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "9cf5c228-a398-4234-93b2-7e0bbdcd77f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed63815b-32c2-42c5-8a6e-e080dd9e8f4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}