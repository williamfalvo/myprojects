{
    "id": "9265684d-5bcc-4470-90ef-5930cd4e4c88",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_up_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 16,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f32b466a-8a51-4fdf-a4d8-13c941a18c3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9265684d-5bcc-4470-90ef-5930cd4e4c88",
            "compositeImage": {
                "id": "997ec851-12ee-45e1-91c1-6e17c7d8407c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f32b466a-8a51-4fdf-a4d8-13c941a18c3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0dbd341-488e-4d48-9e49-00c4e73bcae8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f32b466a-8a51-4fdf-a4d8-13c941a18c3e",
                    "LayerId": "b0a74a90-415d-48a6-93ab-da6eb45bfce5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "b0a74a90-415d-48a6-93ab-da6eb45bfce5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9265684d-5bcc-4470-90ef-5930cd4e4c88",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}