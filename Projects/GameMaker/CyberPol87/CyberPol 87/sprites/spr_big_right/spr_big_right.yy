{
    "id": "c0a4c2c3-51c1-4f4b-b49d-9d0ee08d25fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_big_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 60,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac24506a-d7e5-493e-ba65-1878a1d32390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0a4c2c3-51c1-4f4b-b49d-9d0ee08d25fd",
            "compositeImage": {
                "id": "2c175567-c345-4aa0-a829-4d59b934b790",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac24506a-d7e5-493e-ba65-1878a1d32390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec3f0162-9109-40e9-a54a-7b7275ee9de6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac24506a-d7e5-493e-ba65-1878a1d32390",
                    "LayerId": "3cd50588-fc4b-4f87-91fb-6493cb9c60f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3cd50588-fc4b-4f87-91fb-6493cb9c60f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0a4c2c3-51c1-4f4b-b49d-9d0ee08d25fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}