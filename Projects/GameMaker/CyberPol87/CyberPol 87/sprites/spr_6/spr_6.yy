{
    "id": "848f321d-b837-4c4b-8311-28287c490d8b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4490e3de-c442-4509-b4dd-bb4ca08f531d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "848f321d-b837-4c4b-8311-28287c490d8b",
            "compositeImage": {
                "id": "e7c8402d-9599-4c24-b494-bea50c02c384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4490e3de-c442-4509-b4dd-bb4ca08f531d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daba13b7-6dff-45d1-a3e0-cfeb71cc4fcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4490e3de-c442-4509-b4dd-bb4ca08f531d",
                    "LayerId": "7f988291-7ac1-4576-bc4a-6036f65f3be0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "7f988291-7ac1-4576-bc4a-6036f65f3be0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "848f321d-b837-4c4b-8311-28287c490d8b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 9
}