{
    "id": "7b2684bd-ca1b-4ea7-85d0-7ecba2ca6c7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mobile_destra",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b3fec7f-037c-4b7b-bec8-7753ba1f604a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b2684bd-ca1b-4ea7-85d0-7ecba2ca6c7a",
            "compositeImage": {
                "id": "27c9e0c2-91eb-4c15-ab21-705d8814cadc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b3fec7f-037c-4b7b-bec8-7753ba1f604a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f38eb2a1-2989-4c5b-9f60-d1c6493b77f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b3fec7f-037c-4b7b-bec8-7753ba1f604a",
                    "LayerId": "3eb47b2e-326a-4099-9d24-00e5221f02ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3eb47b2e-326a-4099-9d24-00e5221f02ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b2684bd-ca1b-4ea7-85d0-7ecba2ca6c7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}