{
    "id": "ea23072a-0381-44d3-8fa5-499ac2f47d91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SEWERSTILESETTUBITUTTO__1_",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cec68c1-1663-46ac-bf56-fcf1d9e8bd9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea23072a-0381-44d3-8fa5-499ac2f47d91",
            "compositeImage": {
                "id": "4437ddfc-18e3-4f91-b1f7-dc7671270c6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cec68c1-1663-46ac-bf56-fcf1d9e8bd9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af70375d-acbe-4c6d-9c9a-78c0e24ba77f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cec68c1-1663-46ac-bf56-fcf1d9e8bd9b",
                    "LayerId": "cf74dd16-f4e8-44a3-9e9a-d4edc90197d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "cf74dd16-f4e8-44a3-9e9a-d4edc90197d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea23072a-0381-44d3-8fa5-499ac2f47d91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}