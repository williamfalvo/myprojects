{
    "id": "6b1bdb7a-697f-45c2-83a9-fdceb51bdbe2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fat_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 61,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba21903d-36ca-495a-8d89-0edb22040188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b1bdb7a-697f-45c2-83a9-fdceb51bdbe2",
            "compositeImage": {
                "id": "6de40401-ce6e-4c94-8c16-a94dcf805310",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba21903d-36ca-495a-8d89-0edb22040188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ad9460d-bfe3-4769-96fb-a92162c56868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba21903d-36ca-495a-8d89-0edb22040188",
                    "LayerId": "54a6d21a-4334-4bc8-a94d-c377629db88a"
                }
            ]
        },
        {
            "id": "4799e7de-47c3-44e1-8588-803bf343663f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b1bdb7a-697f-45c2-83a9-fdceb51bdbe2",
            "compositeImage": {
                "id": "4f007756-f558-4225-88bc-dd4a5dec17de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4799e7de-47c3-44e1-8588-803bf343663f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60d1e94d-88aa-4e45-86aa-091732fa6d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4799e7de-47c3-44e1-8588-803bf343663f",
                    "LayerId": "54a6d21a-4334-4bc8-a94d-c377629db88a"
                }
            ]
        },
        {
            "id": "ca20cfa0-1754-46dc-89b8-7919278c5467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b1bdb7a-697f-45c2-83a9-fdceb51bdbe2",
            "compositeImage": {
                "id": "378ab613-5a19-40fe-88b5-012accacc504",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca20cfa0-1754-46dc-89b8-7919278c5467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c785c08a-136e-4fdc-bb89-184175d837a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca20cfa0-1754-46dc-89b8-7919278c5467",
                    "LayerId": "54a6d21a-4334-4bc8-a94d-c377629db88a"
                }
            ]
        },
        {
            "id": "bf993b69-9ff2-43b6-921a-7ea67cbe6fb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b1bdb7a-697f-45c2-83a9-fdceb51bdbe2",
            "compositeImage": {
                "id": "ecb20023-9eed-482a-be70-3495d70e6a30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf993b69-9ff2-43b6-921a-7ea67cbe6fb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0b3d60d-5933-4254-bbeb-e1c63a1fdc59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf993b69-9ff2-43b6-921a-7ea67cbe6fb3",
                    "LayerId": "54a6d21a-4334-4bc8-a94d-c377629db88a"
                }
            ]
        },
        {
            "id": "df6742e0-aa9b-4c7a-bf14-f922536ca773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b1bdb7a-697f-45c2-83a9-fdceb51bdbe2",
            "compositeImage": {
                "id": "7a950e3b-029f-4c4a-aeb2-8bd0abdd322f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df6742e0-aa9b-4c7a-bf14-f922536ca773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "676a64ac-8009-4839-9e73-aaa2d9a5cc90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df6742e0-aa9b-4c7a-bf14-f922536ca773",
                    "LayerId": "54a6d21a-4334-4bc8-a94d-c377629db88a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "54a6d21a-4334-4bc8-a94d-c377629db88a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b1bdb7a-697f-45c2-83a9-fdceb51bdbe2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}