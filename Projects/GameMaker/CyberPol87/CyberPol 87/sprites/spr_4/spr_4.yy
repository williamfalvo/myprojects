{
    "id": "eca57f78-89a0-47a4-980e-f4834fd3efab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1007db91-b6d5-402c-af4b-7c8059d93138",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eca57f78-89a0-47a4-980e-f4834fd3efab",
            "compositeImage": {
                "id": "9c407e25-23f1-4692-916f-b692a09182f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1007db91-b6d5-402c-af4b-7c8059d93138",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47d2c283-1327-498b-8d28-c01f7274636b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1007db91-b6d5-402c-af4b-7c8059d93138",
                    "LayerId": "0643daff-1bf2-48a4-a2f5-74cb1e36030e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "0643daff-1bf2-48a4-a2f5-74cb1e36030e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eca57f78-89a0-47a4-980e-f4834fd3efab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 9
}