{
    "id": "ba0d6990-fcb0-4621-93bb-577c629ea926",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bandiera_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca279e02-b42c-47cd-93dd-75c1d794ffc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba0d6990-fcb0-4621-93bb-577c629ea926",
            "compositeImage": {
                "id": "08ab5666-1d40-4202-bde4-1dcf44bfef7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca279e02-b42c-47cd-93dd-75c1d794ffc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56abf71a-64e6-4d51-9b22-c289bf7494d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca279e02-b42c-47cd-93dd-75c1d794ffc7",
                    "LayerId": "d9b31c0f-65ab-4048-86f7-e312fc8f134e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d9b31c0f-65ab-4048-86f7-e312fc8f134e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba0d6990-fcb0-4621-93bb-577c629ea926",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}