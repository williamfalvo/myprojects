{
    "id": "58b5a505-a59a-4dfa-b4ec-03fadeff511a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_rightUp_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 16,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7bdca4c-9dd8-4cce-9614-9929c42a5330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58b5a505-a59a-4dfa-b4ec-03fadeff511a",
            "compositeImage": {
                "id": "f29737e7-cd8a-4939-a0a8-80a3b34e573b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7bdca4c-9dd8-4cce-9614-9929c42a5330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f3b0123-c69e-48be-b837-cebfa57b502a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7bdca4c-9dd8-4cce-9614-9929c42a5330",
                    "LayerId": "fcf68f08-71bb-498d-9a6f-67f33d56c567"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "fcf68f08-71bb-498d-9a6f-67f33d56c567",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58b5a505-a59a-4dfa-b4ec-03fadeff511a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}