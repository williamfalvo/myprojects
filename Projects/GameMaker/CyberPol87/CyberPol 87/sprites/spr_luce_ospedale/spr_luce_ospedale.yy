{
    "id": "3f686063-3486-4f98-9de7-26b4a927122d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_luce_ospedale",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08923be2-8afb-40b3-adae-2fe8aa2488bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f686063-3486-4f98-9de7-26b4a927122d",
            "compositeImage": {
                "id": "fb923572-73cf-4d02-856a-77df87ac2526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08923be2-8afb-40b3-adae-2fe8aa2488bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "804368a3-20c4-4724-9f2b-4bb3381ac890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08923be2-8afb-40b3-adae-2fe8aa2488bf",
                    "LayerId": "a767225d-b7b0-4c3e-bf72-1675a4614940"
                }
            ]
        },
        {
            "id": "5b7f14fb-86aa-4a05-b606-bee87e87cd7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f686063-3486-4f98-9de7-26b4a927122d",
            "compositeImage": {
                "id": "bd11d9f5-bfd7-4d22-b03d-fbf53300f71a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b7f14fb-86aa-4a05-b606-bee87e87cd7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ba35766-b67c-48c9-ac4e-8dc41cd82e9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b7f14fb-86aa-4a05-b606-bee87e87cd7f",
                    "LayerId": "a767225d-b7b0-4c3e-bf72-1675a4614940"
                }
            ]
        },
        {
            "id": "244c900c-b498-43ac-8672-0ff1d74b0246",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f686063-3486-4f98-9de7-26b4a927122d",
            "compositeImage": {
                "id": "11b2ee8d-8d3a-45a0-bb3e-c6ff661a49c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "244c900c-b498-43ac-8672-0ff1d74b0246",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eda4341-9d45-4b81-b2aa-f2b908a896ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "244c900c-b498-43ac-8672-0ff1d74b0246",
                    "LayerId": "a767225d-b7b0-4c3e-bf72-1675a4614940"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a767225d-b7b0-4c3e-bf72-1675a4614940",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f686063-3486-4f98-9de7-26b4a927122d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}