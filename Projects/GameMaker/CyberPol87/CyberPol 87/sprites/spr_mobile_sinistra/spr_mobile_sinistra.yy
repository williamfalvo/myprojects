{
    "id": "3fb12503-6d06-4c82-bf3e-b6eb414a4f2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mobile_sinistra",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d05a4887-42fa-47a1-8134-94911a9c2a37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fb12503-6d06-4c82-bf3e-b6eb414a4f2c",
            "compositeImage": {
                "id": "149a4d0a-3779-46f5-91aa-974d37ccba4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d05a4887-42fa-47a1-8134-94911a9c2a37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "270d1d14-25d3-4644-8c2e-c555b5dc4f5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d05a4887-42fa-47a1-8134-94911a9c2a37",
                    "LayerId": "b05c8a59-1bb3-43b3-924a-cc2cc104956d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b05c8a59-1bb3-43b3-924a-cc2cc104956d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fb12503-6d06-4c82-bf3e-b6eb414a4f2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}