{
    "id": "17f94f24-a55e-4243-8ee4-d29253a43d24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ospedale",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 543,
    "bbox_left": 15,
    "bbox_right": 253,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8d9a4d9-0d83-4d98-aced-883a2709b44b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17f94f24-a55e-4243-8ee4-d29253a43d24",
            "compositeImage": {
                "id": "768e9864-74c5-4b7d-80f2-ac91fee2f7c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8d9a4d9-0d83-4d98-aced-883a2709b44b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2905056b-2d34-4b34-a719-3ec02a223a1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d9a4d9-0d83-4d98-aced-883a2709b44b",
                    "LayerId": "fdb08afe-90e5-4f56-8116-c378f0a20174"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 608,
    "layers": [
        {
            "id": "fdb08afe-90e5-4f56-8116-c378f0a20174",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17f94f24-a55e-4243-8ee4-d29253a43d24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 165,
    "yorig": 382
}