{
    "id": "b91484c0-3c0f-4e3c-a7f5-777d4acdef94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_right_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "281a6476-97fc-4f32-b51f-b92416a81c1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b91484c0-3c0f-4e3c-a7f5-777d4acdef94",
            "compositeImage": {
                "id": "926527b2-7da3-4ac5-9de4-f48b0d756803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "281a6476-97fc-4f32-b51f-b92416a81c1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcde3f92-76b2-4939-a3d4-0c29bc1cd0ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "281a6476-97fc-4f32-b51f-b92416a81c1a",
                    "LayerId": "a2e5aed2-ea23-42d4-b6c8-8bdc2fe5823d"
                }
            ]
        },
        {
            "id": "0cbb880f-8902-4894-a636-88580ae446cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b91484c0-3c0f-4e3c-a7f5-777d4acdef94",
            "compositeImage": {
                "id": "5353d2fe-08c9-425c-92fa-0010a521f7b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cbb880f-8902-4894-a636-88580ae446cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7dd1a65-5aae-47b5-b6df-998974723ef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cbb880f-8902-4894-a636-88580ae446cc",
                    "LayerId": "a2e5aed2-ea23-42d4-b6c8-8bdc2fe5823d"
                }
            ]
        },
        {
            "id": "c276eb44-caea-4676-b048-a972c19a7d79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b91484c0-3c0f-4e3c-a7f5-777d4acdef94",
            "compositeImage": {
                "id": "a1ecd6fe-9c0e-4307-ae24-8a80558cad63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c276eb44-caea-4676-b048-a972c19a7d79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eed54b3-3061-4762-9ae5-624169d8a3d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c276eb44-caea-4676-b048-a972c19a7d79",
                    "LayerId": "a2e5aed2-ea23-42d4-b6c8-8bdc2fe5823d"
                }
            ]
        },
        {
            "id": "49c1545d-e4e7-4863-a299-4894ecf5647e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b91484c0-3c0f-4e3c-a7f5-777d4acdef94",
            "compositeImage": {
                "id": "4b50ce50-8859-4657-be61-df031c6e1e21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49c1545d-e4e7-4863-a299-4894ecf5647e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47108262-58ec-4e89-a2f2-376d85fe0bf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49c1545d-e4e7-4863-a299-4894ecf5647e",
                    "LayerId": "a2e5aed2-ea23-42d4-b6c8-8bdc2fe5823d"
                }
            ]
        },
        {
            "id": "a5428ba1-07d4-4e40-930b-b3002cc91b40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b91484c0-3c0f-4e3c-a7f5-777d4acdef94",
            "compositeImage": {
                "id": "dcacb1fc-6d45-4b1f-956c-676f2470b6a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5428ba1-07d4-4e40-930b-b3002cc91b40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9763cbba-cd44-4c40-9495-caeae8df9dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5428ba1-07d4-4e40-930b-b3002cc91b40",
                    "LayerId": "a2e5aed2-ea23-42d4-b6c8-8bdc2fe5823d"
                }
            ]
        },
        {
            "id": "493b3261-c37b-4498-985e-a0e62ddab24d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b91484c0-3c0f-4e3c-a7f5-777d4acdef94",
            "compositeImage": {
                "id": "78a1ac4d-c238-434b-8866-c3470bcb9c76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "493b3261-c37b-4498-985e-a0e62ddab24d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35f3ac2c-66e7-4179-9f0a-5d5cea96b786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "493b3261-c37b-4498-985e-a0e62ddab24d",
                    "LayerId": "a2e5aed2-ea23-42d4-b6c8-8bdc2fe5823d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "a2e5aed2-ea23-42d4-b6c8-8bdc2fe5823d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b91484c0-3c0f-4e3c-a7f5-777d4acdef94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}