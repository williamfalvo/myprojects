{
    "id": "eca525c7-f337-4333-9c23-8bfc38e407ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_esplosione_barile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 7,
    "bbox_right": 116,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12171a6e-906d-45eb-8666-a7da0787ba73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eca525c7-f337-4333-9c23-8bfc38e407ec",
            "compositeImage": {
                "id": "0bdd4c8b-2651-4f66-aaa7-74d9134c7d0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12171a6e-906d-45eb-8666-a7da0787ba73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "535b07f6-c21f-466c-bd0a-6d03feffedae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12171a6e-906d-45eb-8666-a7da0787ba73",
                    "LayerId": "2d5354bb-9a1c-4b5c-a915-98066a031947"
                }
            ]
        },
        {
            "id": "91178c8d-3d35-4878-8429-df76a4612ce0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eca525c7-f337-4333-9c23-8bfc38e407ec",
            "compositeImage": {
                "id": "67f84c8b-5c4e-4365-adbe-84a78dc8c8b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91178c8d-3d35-4878-8429-df76a4612ce0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc0ff39f-f353-4327-b57b-77d04b18995b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91178c8d-3d35-4878-8429-df76a4612ce0",
                    "LayerId": "2d5354bb-9a1c-4b5c-a915-98066a031947"
                }
            ]
        },
        {
            "id": "b1091787-7489-4d0f-a395-da8bdfea4a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eca525c7-f337-4333-9c23-8bfc38e407ec",
            "compositeImage": {
                "id": "6aba7b84-485c-4515-9f15-f22832a34761",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1091787-7489-4d0f-a395-da8bdfea4a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16d38fdd-e3b5-4042-88df-8a8dba2661d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1091787-7489-4d0f-a395-da8bdfea4a86",
                    "LayerId": "2d5354bb-9a1c-4b5c-a915-98066a031947"
                }
            ]
        },
        {
            "id": "288c393d-16af-49df-8eb9-457a88b7cd20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eca525c7-f337-4333-9c23-8bfc38e407ec",
            "compositeImage": {
                "id": "c4e8f8ef-4708-464e-a56e-c88e4fa52e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "288c393d-16af-49df-8eb9-457a88b7cd20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50084949-b982-499b-b6bb-07a98beddf0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "288c393d-16af-49df-8eb9-457a88b7cd20",
                    "LayerId": "2d5354bb-9a1c-4b5c-a915-98066a031947"
                }
            ]
        },
        {
            "id": "4cfe696c-28ab-40d7-86ce-942b4fbf0325",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eca525c7-f337-4333-9c23-8bfc38e407ec",
            "compositeImage": {
                "id": "638670a3-e74c-4d14-8873-f2ddbc472ba1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cfe696c-28ab-40d7-86ce-942b4fbf0325",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19520c3b-ae5c-4e5e-9dce-6a50060b1a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cfe696c-28ab-40d7-86ce-942b4fbf0325",
                    "LayerId": "2d5354bb-9a1c-4b5c-a915-98066a031947"
                }
            ]
        },
        {
            "id": "40031558-dbaa-4a85-b51f-00f94d10fa44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eca525c7-f337-4333-9c23-8bfc38e407ec",
            "compositeImage": {
                "id": "3ea6c56a-4235-4fb6-bd0e-acdb1ad2415d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40031558-dbaa-4a85-b51f-00f94d10fa44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd4376f-3e7c-451a-b430-94f5154fda55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40031558-dbaa-4a85-b51f-00f94d10fa44",
                    "LayerId": "2d5354bb-9a1c-4b5c-a915-98066a031947"
                }
            ]
        },
        {
            "id": "238348bf-dd43-4c92-b178-194a5370138c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eca525c7-f337-4333-9c23-8bfc38e407ec",
            "compositeImage": {
                "id": "d90b4da1-fbb5-4e30-8025-33139c36b57c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "238348bf-dd43-4c92-b178-194a5370138c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daa97117-2472-4cd7-bb82-5acba0237ad2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "238348bf-dd43-4c92-b178-194a5370138c",
                    "LayerId": "2d5354bb-9a1c-4b5c-a915-98066a031947"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2d5354bb-9a1c-4b5c-a915-98066a031947",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eca525c7-f337-4333-9c23-8bfc38e407ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}