{
    "id": "66476da9-af25-4c49-9d0d-4ed46da3754c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_right_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 15,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "656c90ed-c1db-4f7b-8abc-68662153e780",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66476da9-af25-4c49-9d0d-4ed46da3754c",
            "compositeImage": {
                "id": "f9863c46-cf81-4d57-8022-e1cff170eae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "656c90ed-c1db-4f7b-8abc-68662153e780",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eb81e72-fd78-4276-9abb-43d24c7a3a4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "656c90ed-c1db-4f7b-8abc-68662153e780",
                    "LayerId": "e345f7bd-d406-42a5-9997-0aefa27860d7"
                }
            ]
        },
        {
            "id": "e00e0fe6-ab97-473a-9e28-52b566201a6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66476da9-af25-4c49-9d0d-4ed46da3754c",
            "compositeImage": {
                "id": "c3522f26-f2f0-4eb6-b72a-b392a001d567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e00e0fe6-ab97-473a-9e28-52b566201a6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "673e3f3a-b9d7-456b-b434-404a0452d213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e00e0fe6-ab97-473a-9e28-52b566201a6b",
                    "LayerId": "e345f7bd-d406-42a5-9997-0aefa27860d7"
                }
            ]
        },
        {
            "id": "38c57b5c-f69d-40b4-96cb-a718c63f9c66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66476da9-af25-4c49-9d0d-4ed46da3754c",
            "compositeImage": {
                "id": "cb3c2aac-1b99-4f2c-96e0-1406eeaec4df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38c57b5c-f69d-40b4-96cb-a718c63f9c66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "315e7a2c-1be4-495b-bca4-4aac8e8bc67a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38c57b5c-f69d-40b4-96cb-a718c63f9c66",
                    "LayerId": "e345f7bd-d406-42a5-9997-0aefa27860d7"
                }
            ]
        },
        {
            "id": "ba09538f-b55c-48e8-a57f-7fd21f705db1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66476da9-af25-4c49-9d0d-4ed46da3754c",
            "compositeImage": {
                "id": "872d8485-d1b4-4e4b-b700-0c1cf19ffbbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba09538f-b55c-48e8-a57f-7fd21f705db1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf82c1eb-ad33-436a-9b8a-0913b576512b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba09538f-b55c-48e8-a57f-7fd21f705db1",
                    "LayerId": "e345f7bd-d406-42a5-9997-0aefa27860d7"
                }
            ]
        },
        {
            "id": "29275f63-c87e-4515-8c52-feca5b32732b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66476da9-af25-4c49-9d0d-4ed46da3754c",
            "compositeImage": {
                "id": "d00faebc-b6a3-41c6-b988-e52bcfb81de4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29275f63-c87e-4515-8c52-feca5b32732b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6236686d-19f7-4401-a1b0-38b8a4435925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29275f63-c87e-4515-8c52-feca5b32732b",
                    "LayerId": "e345f7bd-d406-42a5-9997-0aefa27860d7"
                }
            ]
        },
        {
            "id": "270c1fd1-3567-4bcc-9536-68dbb2848764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66476da9-af25-4c49-9d0d-4ed46da3754c",
            "compositeImage": {
                "id": "a59e7bb6-8e3e-41c9-8c07-ae6b1976b91c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "270c1fd1-3567-4bcc-9536-68dbb2848764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca14b388-9e15-4415-b843-4d5c7b886ebc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "270c1fd1-3567-4bcc-9536-68dbb2848764",
                    "LayerId": "e345f7bd-d406-42a5-9997-0aefa27860d7"
                }
            ]
        },
        {
            "id": "37700a5e-d24c-4683-b573-379a32e36219",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66476da9-af25-4c49-9d0d-4ed46da3754c",
            "compositeImage": {
                "id": "fa956796-df81-4164-a275-e1519a9209b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37700a5e-d24c-4683-b573-379a32e36219",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d48c8f55-3fa0-4f1e-84e3-58b162a4c117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37700a5e-d24c-4683-b573-379a32e36219",
                    "LayerId": "e345f7bd-d406-42a5-9997-0aefa27860d7"
                }
            ]
        },
        {
            "id": "c5086dcb-3006-4d8b-97f6-1ef6436d456a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66476da9-af25-4c49-9d0d-4ed46da3754c",
            "compositeImage": {
                "id": "e22303f9-cbf0-4d1c-8158-85667a57fbec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5086dcb-3006-4d8b-97f6-1ef6436d456a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56011177-02dd-4d9c-8b06-20f750c2b37b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5086dcb-3006-4d8b-97f6-1ef6436d456a",
                    "LayerId": "e345f7bd-d406-42a5-9997-0aefa27860d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "e345f7bd-d406-42a5-9997-0aefa27860d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66476da9-af25-4c49-9d0d-4ed46da3754c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}