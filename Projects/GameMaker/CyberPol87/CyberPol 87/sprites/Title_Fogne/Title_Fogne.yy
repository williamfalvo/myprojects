{
    "id": "aa528dc4-e597-4026-8608-ecdcbab20c7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Title_Fogne",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 5,
    "bbox_right": 74,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce441d1c-1196-44c1-897a-be22e543cb14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa528dc4-e597-4026-8608-ecdcbab20c7d",
            "compositeImage": {
                "id": "19974b54-1690-4ecd-a161-890653541da4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce441d1c-1196-44c1-897a-be22e543cb14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db1fa090-6527-4b08-b498-f36b20a61000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce441d1c-1196-44c1-897a-be22e543cb14",
                    "LayerId": "cefc8a68-3f3b-4056-b7cc-dc4093fd939b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "cefc8a68-3f3b-4056-b7cc-dc4093fd939b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa528dc4-e597-4026-8608-ecdcbab20c7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 12
}