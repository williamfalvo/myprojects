{
    "id": "e9ed53a9-adac-4a61-bfd6-28a30bc4e07e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_LIVELLO_3_VILLA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 13,
    "bbox_right": 141,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4361479-cf51-4ecf-85e9-c080c6222747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ed53a9-adac-4a61-bfd6-28a30bc4e07e",
            "compositeImage": {
                "id": "f201ee8d-48e8-419d-86c9-27fb745ec331",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4361479-cf51-4ecf-85e9-c080c6222747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3313c4bc-cfac-479b-ab3e-3a2414606cb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4361479-cf51-4ecf-85e9-c080c6222747",
                    "LayerId": "61c7d215-0110-46f5-b1a0-83b1db305373"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "61c7d215-0110-46f5-b1a0-83b1db305373",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9ed53a9-adac-4a61-bfd6-28a30bc4e07e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 78,
    "yorig": 24
}