{
    "id": "be1508f1-9d99-466b-a2a1-9e1a99b693b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_continua_txt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 15,
    "bbox_right": 137,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f969c365-5c45-46dc-b567-ec41d937c56d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be1508f1-9d99-466b-a2a1-9e1a99b693b1",
            "compositeImage": {
                "id": "37a097e9-40f8-4929-938c-d545ea06ebbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f969c365-5c45-46dc-b567-ec41d937c56d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f504bddf-8bae-4e0d-9699-b4543bc8cee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f969c365-5c45-46dc-b567-ec41d937c56d",
                    "LayerId": "76c87d01-055d-4a49-8002-44598af4e6fb"
                }
            ]
        },
        {
            "id": "2d29dc5d-2f6c-482d-96cf-f3e8f6163828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be1508f1-9d99-466b-a2a1-9e1a99b693b1",
            "compositeImage": {
                "id": "ae4005eb-f7ec-4650-b696-8ef13cf38357",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d29dc5d-2f6c-482d-96cf-f3e8f6163828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cadbac41-d72a-4bc3-bbb6-a8688cd72c5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d29dc5d-2f6c-482d-96cf-f3e8f6163828",
                    "LayerId": "76c87d01-055d-4a49-8002-44598af4e6fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "76c87d01-055d-4a49-8002-44598af4e6fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be1508f1-9d99-466b-a2a1-9e1a99b693b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 151,
    "xorig": 75,
    "yorig": 28
}