{
    "id": "11a9b227-06ea-42eb-bcbc-44e32ee84e0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Raccolta_Armi_Tast",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2db3e91-0cd2-4ff6-ba8c-73660ee09369",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11a9b227-06ea-42eb-bcbc-44e32ee84e0c",
            "compositeImage": {
                "id": "95442a01-3d77-44eb-81e5-5b164a3ba9ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2db3e91-0cd2-4ff6-ba8c-73660ee09369",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b773fc8-0046-49e2-9065-8d5854ffa2d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2db3e91-0cd2-4ff6-ba8c-73660ee09369",
                    "LayerId": "b7fe14ae-f6d5-4a74-94ab-296fc62f3098"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "b7fe14ae-f6d5-4a74-94ab-296fc62f3098",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11a9b227-06ea-42eb-bcbc-44e32ee84e0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}