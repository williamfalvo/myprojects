{
    "id": "670245e9-793e-4097-9f47-b33876f3d843",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 17,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc9207bb-517e-4002-a2eb-ab77845e3016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "670245e9-793e-4097-9f47-b33876f3d843",
            "compositeImage": {
                "id": "d3820f67-7935-4c48-aa29-85da50d6dce0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc9207bb-517e-4002-a2eb-ab77845e3016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e787731-44e3-4a03-b5da-44f893fbc01d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc9207bb-517e-4002-a2eb-ab77845e3016",
                    "LayerId": "8de33b77-5a73-4635-96a5-480146ec7cd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "8de33b77-5a73-4635-96a5-480146ec7cd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "670245e9-793e-4097-9f47-b33876f3d843",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}