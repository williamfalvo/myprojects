{
    "id": "91bbee11-e603-4491-8626-d0e4b5a9a31b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ospedale_Tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 543,
    "bbox_left": 15,
    "bbox_right": 253,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecc873b9-fd1b-41e3-8b86-b9a9f47019cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91bbee11-e603-4491-8626-d0e4b5a9a31b",
            "compositeImage": {
                "id": "6cb9de02-ca1a-4c23-bb2d-832bcc98121a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecc873b9-fd1b-41e3-8b86-b9a9f47019cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d46a75b-df8a-4e0d-9578-2e81255529ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecc873b9-fd1b-41e3-8b86-b9a9f47019cb",
                    "LayerId": "4f1c9681-4381-4b08-89fd-e40b0b551dbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 608,
    "layers": [
        {
            "id": "4f1c9681-4381-4b08-89fd-e40b0b551dbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91bbee11-e603-4491-8626-d0e4b5a9a31b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 304
}