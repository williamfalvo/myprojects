{
    "id": "fad9a9f1-c096-4a67-8980-fb7151f525d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_munizioni",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e817785-8a0b-4d58-b936-1e2cfa4ad467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fad9a9f1-c096-4a67-8980-fb7151f525d3",
            "compositeImage": {
                "id": "8f4b3a96-f196-4d22-8643-48f15803c710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e817785-8a0b-4d58-b936-1e2cfa4ad467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52a7498f-78f6-4dcc-ba09-6f7858f619c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e817785-8a0b-4d58-b936-1e2cfa4ad467",
                    "LayerId": "59cff8e1-32ef-407f-96d1-01aba55c1d97"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "59cff8e1-32ef-407f-96d1-01aba55c1d97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fad9a9f1-c096-4a67-8980-fb7151f525d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}