{
    "id": "c6fa8854-c5dc-445e-a09c-34354bd67e56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f96fc89-a10d-44ab-affe-268ad11a8a8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6fa8854-c5dc-445e-a09c-34354bd67e56",
            "compositeImage": {
                "id": "b29bcadd-87ec-48d2-b298-218b4959afea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f96fc89-a10d-44ab-affe-268ad11a8a8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58d646a4-7316-45ef-a07b-df98cd5a5e6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f96fc89-a10d-44ab-affe-268ad11a8a8b",
                    "LayerId": "0388f8bf-0733-402e-8fa0-9ae3d5e2006e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "0388f8bf-0733-402e-8fa0-9ae3d5e2006e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6fa8854-c5dc-445e-a09c-34354bd67e56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}