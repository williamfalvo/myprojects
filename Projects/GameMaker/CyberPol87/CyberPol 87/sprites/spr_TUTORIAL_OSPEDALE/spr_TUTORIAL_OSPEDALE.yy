{
    "id": "778cd385-1009-4ef8-834a-12e2a97c2fcb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_TUTORIAL_OSPEDALE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 9,
    "bbox_right": 177,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "606704c0-81e1-4b98-9dd1-feea9b2cdc8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778cd385-1009-4ef8-834a-12e2a97c2fcb",
            "compositeImage": {
                "id": "5b928b62-b29c-49cc-baf3-d6dd38ff5b69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "606704c0-81e1-4b98-9dd1-feea9b2cdc8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf24ac44-a657-4ccf-9c6c-1630a976a9a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "606704c0-81e1-4b98-9dd1-feea9b2cdc8e",
                    "LayerId": "d744e0dc-96b4-4232-acd8-cc2bf59fc736"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "d744e0dc-96b4-4232-acd8-cc2bf59fc736",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "778cd385-1009-4ef8-834a-12e2a97c2fcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 188,
    "xorig": 94,
    "yorig": 17
}