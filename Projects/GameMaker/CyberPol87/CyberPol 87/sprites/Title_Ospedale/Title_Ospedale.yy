{
    "id": "7d4f312c-f46c-492c-888d-07435db50f7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Title_Ospedale",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 13,
    "bbox_right": 120,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c489dd17-31f1-4f72-ae27-7f9bae51913d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d4f312c-f46c-492c-888d-07435db50f7f",
            "compositeImage": {
                "id": "13c1bbb9-37c3-4049-9bea-dbc5cca2e01a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c489dd17-31f1-4f72-ae27-7f9bae51913d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b45694e8-9590-46c8-94c0-232c48ea95f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c489dd17-31f1-4f72-ae27-7f9bae51913d",
                    "LayerId": "14869e47-a13b-4566-92bd-d92246853daf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "14869e47-a13b-4566-92bd-d92246853daf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d4f312c-f46c-492c-888d-07435db50f7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 12
}