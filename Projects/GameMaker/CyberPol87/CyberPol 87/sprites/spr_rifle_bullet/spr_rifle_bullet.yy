{
    "id": "801e7f83-0fad-405a-9663-88afe0f2a4da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rifle_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59bdabbe-726f-4aac-a274-5e3e2531e0c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801e7f83-0fad-405a-9663-88afe0f2a4da",
            "compositeImage": {
                "id": "e058f45b-ef5c-4ad2-89a2-da19dfe95864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59bdabbe-726f-4aac-a274-5e3e2531e0c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b06d40ca-b5a8-4b66-9310-54d3e875f0e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59bdabbe-726f-4aac-a274-5e3e2531e0c5",
                    "LayerId": "3f140c43-f732-414b-9ae1-2e2d417a20b0"
                }
            ]
        },
        {
            "id": "f5225605-a90c-4e34-aacd-9f4e2a6db7cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801e7f83-0fad-405a-9663-88afe0f2a4da",
            "compositeImage": {
                "id": "ba511881-24bb-4180-bd7c-0567662e2767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5225605-a90c-4e34-aacd-9f4e2a6db7cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7875aa11-ec2a-4fc3-b36b-3a93db20cbfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5225605-a90c-4e34-aacd-9f4e2a6db7cc",
                    "LayerId": "3f140c43-f732-414b-9ae1-2e2d417a20b0"
                }
            ]
        },
        {
            "id": "3f70c32f-fa13-4c97-ac07-019bd46f3edf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801e7f83-0fad-405a-9663-88afe0f2a4da",
            "compositeImage": {
                "id": "3a772b5e-b3d7-431d-989f-4a2d09e9258e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f70c32f-fa13-4c97-ac07-019bd46f3edf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35beefdc-0a72-47c2-b933-34724d86d9f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f70c32f-fa13-4c97-ac07-019bd46f3edf",
                    "LayerId": "3f140c43-f732-414b-9ae1-2e2d417a20b0"
                }
            ]
        },
        {
            "id": "832b22a2-20ba-467e-9b28-21bd9e27e6a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801e7f83-0fad-405a-9663-88afe0f2a4da",
            "compositeImage": {
                "id": "0c4dd9b1-65ac-4e71-8f0e-b765680016cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "832b22a2-20ba-467e-9b28-21bd9e27e6a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b41cacff-97e7-4a12-a1e7-af8a0930ff80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "832b22a2-20ba-467e-9b28-21bd9e27e6a9",
                    "LayerId": "3f140c43-f732-414b-9ae1-2e2d417a20b0"
                }
            ]
        },
        {
            "id": "13ee0fd6-2623-4883-b66f-7debe27c26e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801e7f83-0fad-405a-9663-88afe0f2a4da",
            "compositeImage": {
                "id": "8e8c45a2-0f76-43cd-8694-c7eae3f50ff9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13ee0fd6-2623-4883-b66f-7debe27c26e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7e045fe-be3c-4e2f-acad-f1f56f8c3e46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13ee0fd6-2623-4883-b66f-7debe27c26e0",
                    "LayerId": "3f140c43-f732-414b-9ae1-2e2d417a20b0"
                }
            ]
        },
        {
            "id": "112abf19-6842-4122-bdbd-e3b5b789fd71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801e7f83-0fad-405a-9663-88afe0f2a4da",
            "compositeImage": {
                "id": "0c9bbe14-54db-4ca6-804e-cd8c67e586b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "112abf19-6842-4122-bdbd-e3b5b789fd71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb429a5b-f236-4c36-97a6-060e65d6ec60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "112abf19-6842-4122-bdbd-e3b5b789fd71",
                    "LayerId": "3f140c43-f732-414b-9ae1-2e2d417a20b0"
                }
            ]
        },
        {
            "id": "9ad75fe5-2530-437b-ac4b-5c64611db101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "801e7f83-0fad-405a-9663-88afe0f2a4da",
            "compositeImage": {
                "id": "4548a68e-5080-4913-b5a3-1f1372ac4187",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ad75fe5-2530-437b-ac4b-5c64611db101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e2362f9-8a28-4124-9d98-33f4c3b8f23c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad75fe5-2530-437b-ac4b-5c64611db101",
                    "LayerId": "3f140c43-f732-414b-9ae1-2e2d417a20b0"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 16,
    "layers": [
        {
            "id": "3f140c43-f732-414b-9ae1-2e2d417a20b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "801e7f83-0fad-405a-9663-88afe0f2a4da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}