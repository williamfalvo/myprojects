{
    "id": "7cdda59c-e110-4a81-a814-ec89376121c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "magazzino",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 335,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a757e40-2f4a-49a7-9050-7d52be824cc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7cdda59c-e110-4a81-a814-ec89376121c4",
            "compositeImage": {
                "id": "b0c94aa9-ce49-4150-8110-6e4c63f7c7da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a757e40-2f4a-49a7-9050-7d52be824cc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1e3874b-1232-4606-9796-a805fa6d7a52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a757e40-2f4a-49a7-9050-7d52be824cc8",
                    "LayerId": "a03c4946-cd5a-4ee6-9c69-cc88bc230590"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 352,
    "layers": [
        {
            "id": "a03c4946-cd5a-4ee6-9c69-cc88bc230590",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7cdda59c-e110-4a81-a814-ec89376121c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}