{
    "id": "93df8eed-8904-4bb3-9bff-83b2c8c1a4a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fat_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 13,
    "bbox_right": 51,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef01634a-3c47-4dd2-b17e-85d0ce71065c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93df8eed-8904-4bb3-9bff-83b2c8c1a4a2",
            "compositeImage": {
                "id": "7351480b-41ea-4341-9240-6f1937638642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef01634a-3c47-4dd2-b17e-85d0ce71065c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7275d37e-ff58-4b12-af06-347b840b298e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef01634a-3c47-4dd2-b17e-85d0ce71065c",
                    "LayerId": "92656be9-c3c9-4b2f-9e72-7cbcd1912eb7"
                }
            ]
        },
        {
            "id": "5a530274-89bd-427a-93b7-4c2c0d162493",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93df8eed-8904-4bb3-9bff-83b2c8c1a4a2",
            "compositeImage": {
                "id": "9e62f2ff-56db-4a6c-8e62-4481b80850c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a530274-89bd-427a-93b7-4c2c0d162493",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb2ad53f-f0fb-48d6-b3af-67cb1dca44e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a530274-89bd-427a-93b7-4c2c0d162493",
                    "LayerId": "92656be9-c3c9-4b2f-9e72-7cbcd1912eb7"
                }
            ]
        },
        {
            "id": "dce0cacf-b532-4f7d-8b92-2c3bf9619fc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93df8eed-8904-4bb3-9bff-83b2c8c1a4a2",
            "compositeImage": {
                "id": "acf1295c-ae47-4d41-8c24-b6c0cf6102a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce0cacf-b532-4f7d-8b92-2c3bf9619fc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6855ee0a-2e5a-4a04-853c-b4f27c03c591",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce0cacf-b532-4f7d-8b92-2c3bf9619fc8",
                    "LayerId": "92656be9-c3c9-4b2f-9e72-7cbcd1912eb7"
                }
            ]
        },
        {
            "id": "7e22f189-f0e5-4527-a782-d24fc7002633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93df8eed-8904-4bb3-9bff-83b2c8c1a4a2",
            "compositeImage": {
                "id": "1513956b-2088-4e32-8ec9-763a6a53e774",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e22f189-f0e5-4527-a782-d24fc7002633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42466032-6f3a-470a-9df9-9a2e9b5f35bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e22f189-f0e5-4527-a782-d24fc7002633",
                    "LayerId": "92656be9-c3c9-4b2f-9e72-7cbcd1912eb7"
                }
            ]
        },
        {
            "id": "121e0ee3-0257-44bf-b460-b66518000a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93df8eed-8904-4bb3-9bff-83b2c8c1a4a2",
            "compositeImage": {
                "id": "002b2fa6-d20f-402d-93f6-fdc73ee20c9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "121e0ee3-0257-44bf-b460-b66518000a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dac0900e-e540-4332-84ff-bd4ef9f08d12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "121e0ee3-0257-44bf-b460-b66518000a05",
                    "LayerId": "92656be9-c3c9-4b2f-9e72-7cbcd1912eb7"
                }
            ]
        },
        {
            "id": "18cc4e8f-0252-4453-bc45-f0639e48e875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93df8eed-8904-4bb3-9bff-83b2c8c1a4a2",
            "compositeImage": {
                "id": "b9d91e26-c3e2-4b8a-8632-e11508c57876",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18cc4e8f-0252-4453-bc45-f0639e48e875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9703045c-bd8c-4ba7-9e46-35f50a7d5a27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18cc4e8f-0252-4453-bc45-f0639e48e875",
                    "LayerId": "92656be9-c3c9-4b2f-9e72-7cbcd1912eb7"
                }
            ]
        },
        {
            "id": "40fac372-6b80-41bb-a2bf-d550e2971c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93df8eed-8904-4bb3-9bff-83b2c8c1a4a2",
            "compositeImage": {
                "id": "ae31e2fb-b8c4-428b-990f-0356c9b442a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40fac372-6b80-41bb-a2bf-d550e2971c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e56a0814-51e2-4ad5-a2c0-5c6a86d2ab04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40fac372-6b80-41bb-a2bf-d550e2971c35",
                    "LayerId": "92656be9-c3c9-4b2f-9e72-7cbcd1912eb7"
                }
            ]
        },
        {
            "id": "88d954ee-f700-409d-a0d4-000447a0bb13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93df8eed-8904-4bb3-9bff-83b2c8c1a4a2",
            "compositeImage": {
                "id": "a372f1b9-f08a-482d-b0b8-3b272b639048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88d954ee-f700-409d-a0d4-000447a0bb13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3721668a-c2a7-42ce-8e57-1e67628c1d8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88d954ee-f700-409d-a0d4-000447a0bb13",
                    "LayerId": "92656be9-c3c9-4b2f-9e72-7cbcd1912eb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "92656be9-c3c9-4b2f-9e72-7cbcd1912eb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93df8eed-8904-4bb3-9bff-83b2c8c1a4a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}