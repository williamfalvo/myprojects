{
    "id": "005c4215-fc43-4efd-9019-4c8438670253",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rm_new_game",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 10,
    "bbox_right": 252,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14fb024a-2e1c-44c7-b441-2597b7f4bdf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "005c4215-fc43-4efd-9019-4c8438670253",
            "compositeImage": {
                "id": "1ec01144-3f42-4e71-934c-fa36ce18c2e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14fb024a-2e1c-44c7-b441-2597b7f4bdf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc6a869-4f05-4603-9df2-6c3afa953a52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14fb024a-2e1c-44c7-b441-2597b7f4bdf7",
                    "LayerId": "efae9607-9c7b-42ba-b0e2-9b5661111f75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "efae9607-9c7b-42ba-b0e2-9b5661111f75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "005c4215-fc43-4efd-9019-4c8438670253",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 268,
    "xorig": 134,
    "yorig": 31
}