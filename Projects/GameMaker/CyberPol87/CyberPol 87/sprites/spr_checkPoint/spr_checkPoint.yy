{
    "id": "3bb712e8-e0d8-4452-9597-0c710abe374f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_checkPoint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48aee8c3-227d-48a2-8c49-66689bd9607c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bb712e8-e0d8-4452-9597-0c710abe374f",
            "compositeImage": {
                "id": "d7deaa55-9c9f-460b-8961-bdd28aa69a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48aee8c3-227d-48a2-8c49-66689bd9607c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59341263-3790-40a3-9f97-86cc824c3f85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48aee8c3-227d-48a2-8c49-66689bd9607c",
                    "LayerId": "964fc887-6c6e-4021-9343-30cb8df31763"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "964fc887-6c6e-4021-9343-30cb8df31763",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bb712e8-e0d8-4452-9597-0c710abe374f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}