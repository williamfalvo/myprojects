{
    "id": "b27c4af8-b9e0-4ce9-af37-36bf9609b7dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fat_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 38,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd90dbcb-5dd8-405d-b819-834921c5bca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b27c4af8-b9e0-4ce9-af37-36bf9609b7dc",
            "compositeImage": {
                "id": "fae303b0-6f4c-4e72-b990-7fabf93dd8a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd90dbcb-5dd8-405d-b819-834921c5bca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "015bc875-616e-4c58-bab8-e3a52448534c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd90dbcb-5dd8-405d-b819-834921c5bca9",
                    "LayerId": "2de67125-3a18-4562-9795-1377026dcd2e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2de67125-3a18-4562-9795-1377026dcd2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b27c4af8-b9e0-4ce9-af37-36bf9609b7dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}