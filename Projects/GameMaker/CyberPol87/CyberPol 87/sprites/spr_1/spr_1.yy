{
    "id": "cc98d78d-76d7-4998-b62e-5cfda4a71c54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bed55dee-eedf-4c01-9928-7c56e14f3e1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc98d78d-76d7-4998-b62e-5cfda4a71c54",
            "compositeImage": {
                "id": "3c43b1e8-0787-4fba-8bc0-bd36a823fc6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bed55dee-eedf-4c01-9928-7c56e14f3e1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef7d49a8-0709-4795-a525-743f5ac53b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bed55dee-eedf-4c01-9928-7c56e14f3e1b",
                    "LayerId": "ef826247-a68a-451a-9b4b-f49a0af69aa1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "ef826247-a68a-451a-9b4b-f49a0af69aa1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc98d78d-76d7-4998-b62e-5cfda4a71c54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 9
}