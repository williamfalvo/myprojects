{
    "id": "7047a7e2-6ad7-45ce-b898-4ad2efc4667f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cassa_equip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 59,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "145649f9-6f29-4da4-b771-6b4698242756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7047a7e2-6ad7-45ce-b898-4ad2efc4667f",
            "compositeImage": {
                "id": "d740ef79-05a0-49d6-b777-659c6360cc43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "145649f9-6f29-4da4-b771-6b4698242756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5364f2c9-448c-4401-91ee-a1441f6fee21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "145649f9-6f29-4da4-b771-6b4698242756",
                    "LayerId": "c5a6d74b-cae1-4f3b-8d0b-1816bab904e4"
                }
            ]
        },
        {
            "id": "18fd9eee-ab2c-4c94-9aaa-8789cab845b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7047a7e2-6ad7-45ce-b898-4ad2efc4667f",
            "compositeImage": {
                "id": "0b0fa271-2a1c-4cd2-a349-449a0a2962ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18fd9eee-ab2c-4c94-9aaa-8789cab845b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b22832e4-b7c4-40b2-a615-39432c941aec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18fd9eee-ab2c-4c94-9aaa-8789cab845b6",
                    "LayerId": "c5a6d74b-cae1-4f3b-8d0b-1816bab904e4"
                }
            ]
        },
        {
            "id": "8cfc4b1b-a766-403d-9635-6e6a64e26a58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7047a7e2-6ad7-45ce-b898-4ad2efc4667f",
            "compositeImage": {
                "id": "65869eab-4952-4b50-95dd-f39183b3acbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cfc4b1b-a766-403d-9635-6e6a64e26a58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2f149b0-2907-4475-9707-5a0c4f76b597",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cfc4b1b-a766-403d-9635-6e6a64e26a58",
                    "LayerId": "c5a6d74b-cae1-4f3b-8d0b-1816bab904e4"
                }
            ]
        },
        {
            "id": "9dafc938-2b29-4168-a074-cfe9d8fdbcd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7047a7e2-6ad7-45ce-b898-4ad2efc4667f",
            "compositeImage": {
                "id": "4d957248-1765-4510-aad1-8fea0d624345",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dafc938-2b29-4168-a074-cfe9d8fdbcd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d957017-d183-467c-a9eb-01ec2b241c5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dafc938-2b29-4168-a074-cfe9d8fdbcd2",
                    "LayerId": "c5a6d74b-cae1-4f3b-8d0b-1816bab904e4"
                }
            ]
        },
        {
            "id": "c45c5217-295a-43b6-8cb9-1d7419cba5ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7047a7e2-6ad7-45ce-b898-4ad2efc4667f",
            "compositeImage": {
                "id": "8eab6a85-724e-48b8-ad53-0eb08ffe64ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c45c5217-295a-43b6-8cb9-1d7419cba5ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46b164b5-e337-49d7-9491-c1f7288b9536",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c45c5217-295a-43b6-8cb9-1d7419cba5ca",
                    "LayerId": "c5a6d74b-cae1-4f3b-8d0b-1816bab904e4"
                }
            ]
        },
        {
            "id": "7b2fae32-7afc-4ff2-9ce5-eda825d40c70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7047a7e2-6ad7-45ce-b898-4ad2efc4667f",
            "compositeImage": {
                "id": "6169823b-625e-41ee-aa2a-3ff34b70c518",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b2fae32-7afc-4ff2-9ce5-eda825d40c70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31413b4e-96fe-4d0a-bbd5-3a944759f054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b2fae32-7afc-4ff2-9ce5-eda825d40c70",
                    "LayerId": "c5a6d74b-cae1-4f3b-8d0b-1816bab904e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c5a6d74b-cae1-4f3b-8d0b-1816bab904e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7047a7e2-6ad7-45ce-b898-4ad2efc4667f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}