{
    "id": "aca5e03d-4b7e-4050-b258-98a7e66e201d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 45,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b753a04-5e59-4145-8475-7d7ee4ac9715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca5e03d-4b7e-4050-b258-98a7e66e201d",
            "compositeImage": {
                "id": "db92c980-870f-4d17-9dc3-7be9fb06e729",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b753a04-5e59-4145-8475-7d7ee4ac9715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb087a62-532e-430b-91f8-213fb3837ebc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b753a04-5e59-4145-8475-7d7ee4ac9715",
                    "LayerId": "ff4057a1-a278-449a-867f-7f28642e558b"
                }
            ]
        },
        {
            "id": "7b8d4670-d006-4c1b-a2ec-5466a9e25345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca5e03d-4b7e-4050-b258-98a7e66e201d",
            "compositeImage": {
                "id": "c083c829-0eaa-4177-907e-3aac131ebe7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b8d4670-d006-4c1b-a2ec-5466a9e25345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "474c5af1-543b-45d1-93e1-4d2d45fdd34c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b8d4670-d006-4c1b-a2ec-5466a9e25345",
                    "LayerId": "ff4057a1-a278-449a-867f-7f28642e558b"
                }
            ]
        },
        {
            "id": "4f43b2fd-2c1a-4414-8dab-b8e34cccbcd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca5e03d-4b7e-4050-b258-98a7e66e201d",
            "compositeImage": {
                "id": "bb41eaeb-70a1-4747-95e5-d90648022ca5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f43b2fd-2c1a-4414-8dab-b8e34cccbcd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6af2c7bd-4140-407c-bae8-6e90c21f148b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f43b2fd-2c1a-4414-8dab-b8e34cccbcd7",
                    "LayerId": "ff4057a1-a278-449a-867f-7f28642e558b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ff4057a1-a278-449a-867f-7f28642e558b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aca5e03d-4b7e-4050-b258-98a7e66e201d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}