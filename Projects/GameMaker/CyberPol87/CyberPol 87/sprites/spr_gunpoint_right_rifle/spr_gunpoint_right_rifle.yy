{
    "id": "963cfd7d-f9ef-48b5-a6ad-26305a0522bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_right_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 16,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec7531fc-04b5-4742-bf09-a0abf5614e03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963cfd7d-f9ef-48b5-a6ad-26305a0522bb",
            "compositeImage": {
                "id": "3e813e18-9cf7-441f-808d-8d4cdbb55f5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec7531fc-04b5-4742-bf09-a0abf5614e03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f595f1f-e8c4-4275-b5f6-91234f177878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec7531fc-04b5-4742-bf09-a0abf5614e03",
                    "LayerId": "b9b38c47-94c7-424f-ab9e-fc1867a93f3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "b9b38c47-94c7-424f-ab9e-fc1867a93f3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "963cfd7d-f9ef-48b5-a6ad-26305a0522bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}