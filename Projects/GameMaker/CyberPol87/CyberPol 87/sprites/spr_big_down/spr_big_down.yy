{
    "id": "d1cab72e-a555-4050-bf6b-37b2d62b0163",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_big_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 38,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc224690-7ee5-4641-9d90-f13b177a5105",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1cab72e-a555-4050-bf6b-37b2d62b0163",
            "compositeImage": {
                "id": "fbb5490a-aacf-47d7-84da-a85d4623ac83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc224690-7ee5-4641-9d90-f13b177a5105",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71c1f594-82f9-4488-bbd4-48f5b1d0653b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc224690-7ee5-4641-9d90-f13b177a5105",
                    "LayerId": "ebddc1a1-9702-4ec2-9c7d-8f31ff6c6299"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ebddc1a1-9702-4ec2-9c7d-8f31ff6c6299",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1cab72e-a555-4050-bf6b-37b2d62b0163",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}