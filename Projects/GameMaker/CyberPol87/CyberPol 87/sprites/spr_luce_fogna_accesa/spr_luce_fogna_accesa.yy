{
    "id": "7a203946-3c09-4e49-b78b-01c09788d934",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_luce_fogna_accesa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28d0a535-8ffc-4170-b551-7ab5c41dd8ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a203946-3c09-4e49-b78b-01c09788d934",
            "compositeImage": {
                "id": "348b519f-e276-425c-b4cc-646a89da53a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28d0a535-8ffc-4170-b551-7ab5c41dd8ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b26526db-9155-43b4-a5e4-dce248c04c3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28d0a535-8ffc-4170-b551-7ab5c41dd8ec",
                    "LayerId": "c5ed08a8-697c-42e6-8e76-45398b6ae0c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c5ed08a8-697c-42e6-8e76-45398b6ae0c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a203946-3c09-4e49-b78b-01c09788d934",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}