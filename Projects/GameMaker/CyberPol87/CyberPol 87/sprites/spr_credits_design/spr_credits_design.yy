{
    "id": "479f6079-ab22-4a46-9884-1c8e4acb3c1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_credits_design",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 55,
    "bbox_right": 577,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9f56c60-ee52-4a78-9dd1-975e050f43fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "479f6079-ab22-4a46-9884-1c8e4acb3c1b",
            "compositeImage": {
                "id": "716da042-4fc1-4586-9024-50f3452a0a90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f56c60-ee52-4a78-9dd1-975e050f43fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd200c0e-ec3d-4239-aac6-9c9464de5279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f56c60-ee52-4a78-9dd1-975e050f43fb",
                    "LayerId": "aa62af03-150d-4573-8695-cbb39d9d99c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 310,
    "layers": [
        {
            "id": "aa62af03-150d-4573-8695-cbb39d9d99c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "479f6079-ab22-4a46-9884-1c8e4acb3c1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 610,
    "xorig": 305,
    "yorig": 155
}