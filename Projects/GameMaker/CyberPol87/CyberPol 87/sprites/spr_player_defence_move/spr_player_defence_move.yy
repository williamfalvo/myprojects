{
    "id": "64e73217-932b-487d-a854-f6bc6cf9e95a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_defence_move",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b4f47b6-6436-4bc4-93f0-58df4c5bb773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64e73217-932b-487d-a854-f6bc6cf9e95a",
            "compositeImage": {
                "id": "4d57aaec-f2b5-4326-955e-647a681ed9d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b4f47b6-6436-4bc4-93f0-58df4c5bb773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44d1fd1a-407f-4438-bdf3-67d4a0bc8a49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b4f47b6-6436-4bc4-93f0-58df4c5bb773",
                    "LayerId": "6067cf36-16bf-49da-9266-df2e31d249ce"
                }
            ]
        },
        {
            "id": "144bda7b-6963-4637-a935-fe45cdbb4379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64e73217-932b-487d-a854-f6bc6cf9e95a",
            "compositeImage": {
                "id": "026d04d7-c270-4aa3-8853-bc9f6ad2015c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "144bda7b-6963-4637-a935-fe45cdbb4379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a734d96-aec4-4395-967b-b997b897962e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "144bda7b-6963-4637-a935-fe45cdbb4379",
                    "LayerId": "6067cf36-16bf-49da-9266-df2e31d249ce"
                }
            ]
        },
        {
            "id": "eff94b12-c84f-4f12-a3d9-ddd7d7382993",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64e73217-932b-487d-a854-f6bc6cf9e95a",
            "compositeImage": {
                "id": "1a462510-52bc-49a5-a23d-bc1134d3a413",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eff94b12-c84f-4f12-a3d9-ddd7d7382993",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17efb4d7-92c6-4789-84b1-1302ff86092c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eff94b12-c84f-4f12-a3d9-ddd7d7382993",
                    "LayerId": "6067cf36-16bf-49da-9266-df2e31d249ce"
                }
            ]
        },
        {
            "id": "7530bb6c-0ecc-440a-9c76-f6d7666af1a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64e73217-932b-487d-a854-f6bc6cf9e95a",
            "compositeImage": {
                "id": "98055c20-6cfc-40e0-8d45-bb22adaec823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7530bb6c-0ecc-440a-9c76-f6d7666af1a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7fa84fc-1b2a-4225-a7f7-ffe4bfe93d5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7530bb6c-0ecc-440a-9c76-f6d7666af1a6",
                    "LayerId": "6067cf36-16bf-49da-9266-df2e31d249ce"
                }
            ]
        },
        {
            "id": "ddb42c88-0795-43e7-9d79-16c2c79a964b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64e73217-932b-487d-a854-f6bc6cf9e95a",
            "compositeImage": {
                "id": "be7cf421-4974-4421-9a8a-cd242283a829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddb42c88-0795-43e7-9d79-16c2c79a964b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "119c3153-6b9c-4aa5-88cb-f184c3acbdb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddb42c88-0795-43e7-9d79-16c2c79a964b",
                    "LayerId": "6067cf36-16bf-49da-9266-df2e31d249ce"
                }
            ]
        },
        {
            "id": "32fc8e1d-a6c0-4791-9ed9-16d4786ab73d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64e73217-932b-487d-a854-f6bc6cf9e95a",
            "compositeImage": {
                "id": "1a9b1a7f-8192-4b9c-927d-21652f50ad24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32fc8e1d-a6c0-4791-9ed9-16d4786ab73d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4d07b2a-a7b1-4cb6-9f96-99a0f420d9a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32fc8e1d-a6c0-4791-9ed9-16d4786ab73d",
                    "LayerId": "6067cf36-16bf-49da-9266-df2e31d249ce"
                }
            ]
        },
        {
            "id": "b128c513-341b-460e-90e5-c79b38d22b42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64e73217-932b-487d-a854-f6bc6cf9e95a",
            "compositeImage": {
                "id": "36fba4b4-c7b7-464c-bb08-e62bb2ed5838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b128c513-341b-460e-90e5-c79b38d22b42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3d1227d-23bf-47a4-8986-a77e2f63beb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b128c513-341b-460e-90e5-c79b38d22b42",
                    "LayerId": "6067cf36-16bf-49da-9266-df2e31d249ce"
                }
            ]
        },
        {
            "id": "cd8be296-dbbe-4cbc-b702-98633198c22c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64e73217-932b-487d-a854-f6bc6cf9e95a",
            "compositeImage": {
                "id": "b6860720-7faa-424c-a565-2014ac0b36f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd8be296-dbbe-4cbc-b702-98633198c22c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "990e73b2-e581-4531-b762-f9a6262b1777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd8be296-dbbe-4cbc-b702-98633198c22c",
                    "LayerId": "6067cf36-16bf-49da-9266-df2e31d249ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "6067cf36-16bf-49da-9266-df2e31d249ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64e73217-932b-487d-a854-f6bc6cf9e95a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}