{
    "id": "cc29a2b9-57e7-44cb-9315-9b420826cf90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 11,
    "bbox_right": 47,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6d032b2-3a7d-42c2-a712-79b3452ca1aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc29a2b9-57e7-44cb-9315-9b420826cf90",
            "compositeImage": {
                "id": "6b86b7fd-d575-46b4-b0d4-3eebdd0ea966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6d032b2-3a7d-42c2-a712-79b3452ca1aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75f45dd0-e199-486d-acf8-b3bfc7957e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d032b2-3a7d-42c2-a712-79b3452ca1aa",
                    "LayerId": "8f8821e4-887b-4393-83d1-ff452027a970"
                }
            ]
        },
        {
            "id": "558d3be2-19db-4603-b738-2698879ae451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc29a2b9-57e7-44cb-9315-9b420826cf90",
            "compositeImage": {
                "id": "6d94e6e4-026e-47d8-b682-89abf7cea014",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "558d3be2-19db-4603-b738-2698879ae451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02d7b05d-ad50-475f-826b-ed1921c803c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "558d3be2-19db-4603-b738-2698879ae451",
                    "LayerId": "8f8821e4-887b-4393-83d1-ff452027a970"
                }
            ]
        },
        {
            "id": "7e097160-0cbe-40a4-a12b-f3dd3667d22e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc29a2b9-57e7-44cb-9315-9b420826cf90",
            "compositeImage": {
                "id": "580a0122-7870-43ec-8d64-8291a46a0c42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e097160-0cbe-40a4-a12b-f3dd3667d22e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5cec0e1-5706-4bcc-8222-b4eebf342ac3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e097160-0cbe-40a4-a12b-f3dd3667d22e",
                    "LayerId": "8f8821e4-887b-4393-83d1-ff452027a970"
                }
            ]
        },
        {
            "id": "fbf25afb-85fe-49d7-8ffe-97d21e7ea5b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc29a2b9-57e7-44cb-9315-9b420826cf90",
            "compositeImage": {
                "id": "cee050f1-0817-432b-8efd-d9a7c908ffee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbf25afb-85fe-49d7-8ffe-97d21e7ea5b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4bdcb0c-3c0a-46ab-ab57-3d065764a4db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbf25afb-85fe-49d7-8ffe-97d21e7ea5b8",
                    "LayerId": "8f8821e4-887b-4393-83d1-ff452027a970"
                }
            ]
        },
        {
            "id": "16a2827e-08be-4ad8-9672-cd286a03ecac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc29a2b9-57e7-44cb-9315-9b420826cf90",
            "compositeImage": {
                "id": "7fc7ea53-0722-47df-becc-96fdc6efcea6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16a2827e-08be-4ad8-9672-cd286a03ecac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76625142-ea1a-46a9-ae4b-ee04e7d44229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16a2827e-08be-4ad8-9672-cd286a03ecac",
                    "LayerId": "8f8821e4-887b-4393-83d1-ff452027a970"
                }
            ]
        },
        {
            "id": "495fb328-f837-4760-b238-8590a3dc4434",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc29a2b9-57e7-44cb-9315-9b420826cf90",
            "compositeImage": {
                "id": "bd7ab0b3-8130-442d-b852-45e3c2a993c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "495fb328-f837-4760-b238-8590a3dc4434",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bd0883f-a7e5-474c-b632-e23705e17c90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "495fb328-f837-4760-b238-8590a3dc4434",
                    "LayerId": "8f8821e4-887b-4393-83d1-ff452027a970"
                }
            ]
        },
        {
            "id": "0e2b2119-b103-468b-aed6-f3764353602d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc29a2b9-57e7-44cb-9315-9b420826cf90",
            "compositeImage": {
                "id": "75a0b13d-1ed7-412c-9312-0299f3a0ee8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e2b2119-b103-468b-aed6-f3764353602d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "105025dd-395f-419d-9f44-04bb41079b94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e2b2119-b103-468b-aed6-f3764353602d",
                    "LayerId": "8f8821e4-887b-4393-83d1-ff452027a970"
                }
            ]
        },
        {
            "id": "02c5c02e-c6e1-454e-bd17-8f01839bf9fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc29a2b9-57e7-44cb-9315-9b420826cf90",
            "compositeImage": {
                "id": "8f3e511e-eb7a-4cc6-abe4-d489d644f99d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02c5c02e-c6e1-454e-bd17-8f01839bf9fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e1f97b8-4536-4393-9d99-ecb90d8f9ac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02c5c02e-c6e1-454e-bd17-8f01839bf9fa",
                    "LayerId": "8f8821e4-887b-4393-83d1-ff452027a970"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8f8821e4-887b-4393-83d1-ff452027a970",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc29a2b9-57e7-44cb-9315-9b420826cf90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}