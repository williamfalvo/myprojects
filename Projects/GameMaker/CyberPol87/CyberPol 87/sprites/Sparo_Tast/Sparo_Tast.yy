{
    "id": "639f512c-6fb2-4d06-a3a3-73aa78765c07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Sparo_Tast",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22e20c6c-51f7-4b49-86f1-9a1fb595dabf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "639f512c-6fb2-4d06-a3a3-73aa78765c07",
            "compositeImage": {
                "id": "9c8c5afc-f0f4-4e47-b9cb-5b07cc004518",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e20c6c-51f7-4b49-86f1-9a1fb595dabf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe0e7132-b871-4826-a72a-48ff245f5f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e20c6c-51f7-4b49-86f1-9a1fb595dabf",
                    "LayerId": "913dfec2-3120-456f-b133-7535174d3ce3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "913dfec2-3120-456f-b133-7535174d3ce3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "639f512c-6fb2-4d06-a3a3-73aa78765c07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}