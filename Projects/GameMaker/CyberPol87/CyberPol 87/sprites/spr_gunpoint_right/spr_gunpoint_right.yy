{
    "id": "42f5ab2f-1aba-43ae-a5bd-6cb271d527bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 17,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d333343c-2445-4ae3-b4a5-d49940a36b20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42f5ab2f-1aba-43ae-a5bd-6cb271d527bb",
            "compositeImage": {
                "id": "5a617d8d-8d90-4b93-b54f-2df31feaf515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d333343c-2445-4ae3-b4a5-d49940a36b20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c069e785-0884-489a-88a3-3ebd5879048f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d333343c-2445-4ae3-b4a5-d49940a36b20",
                    "LayerId": "b408db4b-3707-4c9c-ae0f-7cc182668763"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "b408db4b-3707-4c9c-ae0f-7cc182668763",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42f5ab2f-1aba-43ae-a5bd-6cb271d527bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}