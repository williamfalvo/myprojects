{
    "id": "474dc093-506d-412d-b4c4-573d26f6e929",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 2,
    "bbox_right": 57,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adebcaf1-f314-4a6b-aa41-e1c2e87cef00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "474dc093-506d-412d-b4c4-573d26f6e929",
            "compositeImage": {
                "id": "0a8c66c0-625d-41a7-92c9-d8c075663441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adebcaf1-f314-4a6b-aa41-e1c2e87cef00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33b5c0ba-5dca-41d5-9299-49031245cfe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adebcaf1-f314-4a6b-aa41-e1c2e87cef00",
                    "LayerId": "9dc964aa-50d2-4819-9b05-5a4448728aca"
                }
            ]
        },
        {
            "id": "fa87eb11-494f-436a-86dd-e64449208784",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "474dc093-506d-412d-b4c4-573d26f6e929",
            "compositeImage": {
                "id": "ff14c46e-ede3-4595-8090-14d6a8939ea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa87eb11-494f-436a-86dd-e64449208784",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f703790-d219-4013-836b-175ca6cb9481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa87eb11-494f-436a-86dd-e64449208784",
                    "LayerId": "9dc964aa-50d2-4819-9b05-5a4448728aca"
                }
            ]
        },
        {
            "id": "3ea41939-a261-4381-9487-91692acb3d13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "474dc093-506d-412d-b4c4-573d26f6e929",
            "compositeImage": {
                "id": "6ff18f5d-aef0-4f68-85ff-e980f3da3eab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ea41939-a261-4381-9487-91692acb3d13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25e53901-30e7-4abf-8b53-64576575d333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ea41939-a261-4381-9487-91692acb3d13",
                    "LayerId": "9dc964aa-50d2-4819-9b05-5a4448728aca"
                }
            ]
        },
        {
            "id": "041e5831-0703-4230-8444-ffc48db1247a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "474dc093-506d-412d-b4c4-573d26f6e929",
            "compositeImage": {
                "id": "969b2e0d-ed8a-41e0-840b-9984b1c50ba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "041e5831-0703-4230-8444-ffc48db1247a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79eb3d05-aa37-4ada-b6e7-42bc1f485fa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "041e5831-0703-4230-8444-ffc48db1247a",
                    "LayerId": "9dc964aa-50d2-4819-9b05-5a4448728aca"
                }
            ]
        },
        {
            "id": "cadcf88e-1485-42d4-ad5a-bae06eda2ea1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "474dc093-506d-412d-b4c4-573d26f6e929",
            "compositeImage": {
                "id": "24eda4df-febb-4109-8142-4626f966eb81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cadcf88e-1485-42d4-ad5a-bae06eda2ea1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd962584-e469-4f7a-aa6a-c260bad202d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cadcf88e-1485-42d4-ad5a-bae06eda2ea1",
                    "LayerId": "9dc964aa-50d2-4819-9b05-5a4448728aca"
                }
            ]
        },
        {
            "id": "8f9fff8b-8185-4e15-9fb0-5f84eed5f5d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "474dc093-506d-412d-b4c4-573d26f6e929",
            "compositeImage": {
                "id": "4fe3dfe5-abc0-4087-b477-f85c39aad13f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f9fff8b-8185-4e15-9fb0-5f84eed5f5d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7035c6ff-e745-47bc-ae45-d6a7c19ab18e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f9fff8b-8185-4e15-9fb0-5f84eed5f5d2",
                    "LayerId": "9dc964aa-50d2-4819-9b05-5a4448728aca"
                }
            ]
        },
        {
            "id": "a00b6aef-9e3a-4855-8737-86e0ad293867",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "474dc093-506d-412d-b4c4-573d26f6e929",
            "compositeImage": {
                "id": "c34838b1-174a-4d9a-ab0d-d4501b35aace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a00b6aef-9e3a-4855-8737-86e0ad293867",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef3eba2d-870c-46be-b2db-351d3c162a8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00b6aef-9e3a-4855-8737-86e0ad293867",
                    "LayerId": "9dc964aa-50d2-4819-9b05-5a4448728aca"
                }
            ]
        },
        {
            "id": "00649486-c4a3-43ca-b706-c488f4e13c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "474dc093-506d-412d-b4c4-573d26f6e929",
            "compositeImage": {
                "id": "56de95f9-20dd-4f9d-a297-ab74280afeea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00649486-c4a3-43ca-b706-c488f4e13c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9a6dcc1-a513-4f47-8f22-c138eaec5219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00649486-c4a3-43ca-b706-c488f4e13c05",
                    "LayerId": "9dc964aa-50d2-4819-9b05-5a4448728aca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "9dc964aa-50d2-4819-9b05-5a4448728aca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "474dc093-506d-412d-b4c4-573d26f6e929",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}