{
    "id": "998f3517-8664-4989-9bb8-40b9efb1c909",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_healtBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 2,
    "bbox_right": 62,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a401a9e5-d224-4eca-87cd-027b69b3cd7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "998f3517-8664-4989-9bb8-40b9efb1c909",
            "compositeImage": {
                "id": "bb2897ae-cf9e-4429-81ea-910e3b4ae1ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a401a9e5-d224-4eca-87cd-027b69b3cd7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd27285e-b943-4356-87bd-824c6c30e377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a401a9e5-d224-4eca-87cd-027b69b3cd7d",
                    "LayerId": "d297765c-f4e0-4509-ae92-11ba613bfbff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d297765c-f4e0-4509-ae92-11ba613bfbff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "998f3517-8664-4989-9bb8-40b9efb1c909",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 4,
    "yorig": 32
}