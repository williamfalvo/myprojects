{
    "id": "97c67f93-437b-4a4b-b3b5-867948a419a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PULSANTE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fcbe8a5-f9ad-442c-a2e6-4aa6024217c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97c67f93-437b-4a4b-b3b5-867948a419a6",
            "compositeImage": {
                "id": "93c31f43-a611-48a9-9a98-cb8a24d906d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fcbe8a5-f9ad-442c-a2e6-4aa6024217c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f46bd75b-4a67-47f2-ab03-c3ec3f20275f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fcbe8a5-f9ad-442c-a2e6-4aa6024217c9",
                    "LayerId": "83cdd82b-c186-4588-8b3f-704c33e5f96e"
                }
            ]
        },
        {
            "id": "44ffa7bc-6d99-4215-8051-d7907db9d77d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97c67f93-437b-4a4b-b3b5-867948a419a6",
            "compositeImage": {
                "id": "212386a1-e9c3-44ef-ac65-cf4612c57eea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44ffa7bc-6d99-4215-8051-d7907db9d77d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2776d47-8542-41c3-af12-45657cf0d930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44ffa7bc-6d99-4215-8051-d7907db9d77d",
                    "LayerId": "83cdd82b-c186-4588-8b3f-704c33e5f96e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "83cdd82b-c186-4588-8b3f-704c33e5f96e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97c67f93-437b-4a4b-b3b5-867948a419a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 60,
    "yorig": 75
}