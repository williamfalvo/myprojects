{
    "id": "13e561fc-47aa-4fd4-9591-8c9a4b32139f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Melee",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 243,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a4932f8-cb27-4ece-809c-bc2aa83b8424",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13e561fc-47aa-4fd4-9591-8c9a4b32139f",
            "compositeImage": {
                "id": "252a0917-e000-4edd-9158-5e2dc1842ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a4932f8-cb27-4ece-809c-bc2aa83b8424",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52f9e430-7afc-4b6d-b998-d6df3ec1e1d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a4932f8-cb27-4ece-809c-bc2aa83b8424",
                    "LayerId": "b1b9d089-1616-4514-bfd3-2880eff36f10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "b1b9d089-1616-4514-bfd3-2880eff36f10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13e561fc-47aa-4fd4-9591-8c9a4b32139f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}