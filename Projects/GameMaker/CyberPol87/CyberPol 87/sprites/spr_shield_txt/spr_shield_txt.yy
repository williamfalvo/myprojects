{
    "id": "950e6a50-c353-4c05-99e1-4ffef08b5423",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shield_txt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 126,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e10bdc4b-11b8-48fa-81b4-fd5fa5fa78ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "950e6a50-c353-4c05-99e1-4ffef08b5423",
            "compositeImage": {
                "id": "f6b128c4-a788-41e3-b65c-3bc26fa715cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e10bdc4b-11b8-48fa-81b4-fd5fa5fa78ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ed3a677-5786-4863-b157-c65f330d8334",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e10bdc4b-11b8-48fa-81b4-fd5fa5fa78ff",
                    "LayerId": "281247d7-e195-49a3-88f0-dc84f4d2debb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "281247d7-e195-49a3-88f0-dc84f4d2debb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "950e6a50-c353-4c05-99e1-4ffef08b5423",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 16
}