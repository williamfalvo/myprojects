{
    "id": "beec8625-ee82-4e9e-9a3d-f4f4c835e3d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 45,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38eb60e8-1bb6-4f0c-9b8d-f6f59dc84b6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beec8625-ee82-4e9e-9a3d-f4f4c835e3d0",
            "compositeImage": {
                "id": "a24cc81a-9f98-4564-a55d-cb9db8609005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38eb60e8-1bb6-4f0c-9b8d-f6f59dc84b6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ec50e65-beb4-4302-aba5-84eb6fe11fcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38eb60e8-1bb6-4f0c-9b8d-f6f59dc84b6b",
                    "LayerId": "f1b21d0e-9a00-4b86-93c5-e53f3ed3f0cb"
                }
            ]
        },
        {
            "id": "a899f2c5-f618-4956-bcda-5172c1632909",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beec8625-ee82-4e9e-9a3d-f4f4c835e3d0",
            "compositeImage": {
                "id": "8cb373c2-8e38-426b-b23f-d2664996efd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a899f2c5-f618-4956-bcda-5172c1632909",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20c2810f-cf64-4ecf-9b1f-30c1d6020540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a899f2c5-f618-4956-bcda-5172c1632909",
                    "LayerId": "f1b21d0e-9a00-4b86-93c5-e53f3ed3f0cb"
                }
            ]
        },
        {
            "id": "b0e17d9d-fc1c-4132-b1a2-b70ebf832120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beec8625-ee82-4e9e-9a3d-f4f4c835e3d0",
            "compositeImage": {
                "id": "a9d1abf3-3ccd-43cf-9869-b4c9dbca414f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0e17d9d-fc1c-4132-b1a2-b70ebf832120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caacef58-b90e-4d7d-b5ca-6871be8c5cc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0e17d9d-fc1c-4132-b1a2-b70ebf832120",
                    "LayerId": "f1b21d0e-9a00-4b86-93c5-e53f3ed3f0cb"
                }
            ]
        },
        {
            "id": "e2f2bc93-471f-4df8-9b88-8d8a2deb8d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beec8625-ee82-4e9e-9a3d-f4f4c835e3d0",
            "compositeImage": {
                "id": "b22d5627-866b-40e6-adea-fa76e8e63a49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2f2bc93-471f-4df8-9b88-8d8a2deb8d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12dbfbff-ea33-4aec-937e-eac0b39fb25d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2f2bc93-471f-4df8-9b88-8d8a2deb8d31",
                    "LayerId": "f1b21d0e-9a00-4b86-93c5-e53f3ed3f0cb"
                }
            ]
        },
        {
            "id": "ae1063a6-a414-4c5e-8f9a-f70d76cb71dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beec8625-ee82-4e9e-9a3d-f4f4c835e3d0",
            "compositeImage": {
                "id": "ed488b3a-2028-47d4-bae4-1113eae2709f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae1063a6-a414-4c5e-8f9a-f70d76cb71dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e443607-e0f8-4ca2-818c-3746e657d376",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae1063a6-a414-4c5e-8f9a-f70d76cb71dd",
                    "LayerId": "f1b21d0e-9a00-4b86-93c5-e53f3ed3f0cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f1b21d0e-9a00-4b86-93c5-e53f3ed3f0cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "beec8625-ee82-4e9e-9a3d-f4f4c835e3d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}