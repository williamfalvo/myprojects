{
    "id": "75f49b15-2a9b-47c8-b1dc-f253f4466097",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_WaterAnimation_Sewers",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3a14aa6-2d24-4c43-8f1e-6db731fb5b7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75f49b15-2a9b-47c8-b1dc-f253f4466097",
            "compositeImage": {
                "id": "538ce85f-6259-486e-8afa-803556d3dabe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3a14aa6-2d24-4c43-8f1e-6db731fb5b7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aadc3f2-0513-4ab5-bddf-d5a2c6ddf3a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3a14aa6-2d24-4c43-8f1e-6db731fb5b7e",
                    "LayerId": "50e436c3-edd7-47cd-92f9-4144ab565929"
                }
            ]
        },
        {
            "id": "f151248b-3740-4cdb-bd09-a9468656654d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75f49b15-2a9b-47c8-b1dc-f253f4466097",
            "compositeImage": {
                "id": "e3817ec0-f42a-4e46-a4e9-b6006dd119be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f151248b-3740-4cdb-bd09-a9468656654d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34196e98-cd46-4d1c-9e12-be0f94f47386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f151248b-3740-4cdb-bd09-a9468656654d",
                    "LayerId": "50e436c3-edd7-47cd-92f9-4144ab565929"
                }
            ]
        },
        {
            "id": "2424c14b-49d9-4587-9ab3-d1fe139848e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75f49b15-2a9b-47c8-b1dc-f253f4466097",
            "compositeImage": {
                "id": "f61c6fd3-b512-42a0-968a-d05f59c47313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2424c14b-49d9-4587-9ab3-d1fe139848e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1784a5e-5829-48da-8e9f-2f5d587f50fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2424c14b-49d9-4587-9ab3-d1fe139848e6",
                    "LayerId": "50e436c3-edd7-47cd-92f9-4144ab565929"
                }
            ]
        },
        {
            "id": "b4bc17c6-7c49-4cf0-9509-3df967509b84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75f49b15-2a9b-47c8-b1dc-f253f4466097",
            "compositeImage": {
                "id": "8420ed17-fd3a-453f-a6f8-ac16d9073dca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4bc17c6-7c49-4cf0-9509-3df967509b84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b9194d-fb26-4763-b9be-6ffea143db7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4bc17c6-7c49-4cf0-9509-3df967509b84",
                    "LayerId": "50e436c3-edd7-47cd-92f9-4144ab565929"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "50e436c3-edd7-47cd-92f9-4144ab565929",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75f49b15-2a9b-47c8-b1dc-f253f4466097",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": -11,
    "yorig": 16
}