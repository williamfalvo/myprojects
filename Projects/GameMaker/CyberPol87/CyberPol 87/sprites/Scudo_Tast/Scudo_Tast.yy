{
    "id": "56630ef5-3986-45a2-873b-236b977f842e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Scudo_Tast",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d15ee0f5-3932-4353-a1f1-e7553b343fae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56630ef5-3986-45a2-873b-236b977f842e",
            "compositeImage": {
                "id": "25df8ec2-c0b2-4fa4-812b-6975bccc97a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d15ee0f5-3932-4353-a1f1-e7553b343fae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff61bd84-9310-4380-b989-2c423d0acc70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d15ee0f5-3932-4353-a1f1-e7553b343fae",
                    "LayerId": "a5d12a1c-d4ce-47f8-bee8-df5d75e15833"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "a5d12a1c-d4ce-47f8-bee8-df5d75e15833",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56630ef5-3986-45a2-873b-236b977f842e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}