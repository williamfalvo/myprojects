{
    "id": "446ef59f-3fd5-42c7-8d2c-490e9e79152e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Background0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1893eaa9-5c48-4f15-8865-9a84c8cc9c6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "446ef59f-3fd5-42c7-8d2c-490e9e79152e",
            "compositeImage": {
                "id": "e686f5c1-d1e3-437a-a3bf-9f3b835d64d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1893eaa9-5c48-4f15-8865-9a84c8cc9c6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f07854-4b50-4b2f-8131-cf8556d9569a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1893eaa9-5c48-4f15-8865-9a84c8cc9c6d",
                    "LayerId": "2304791c-95e6-4601-bfa3-626f52643c9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "2304791c-95e6-4601-bfa3-626f52643c9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "446ef59f-3fd5-42c7-8d2c-490e9e79152e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}