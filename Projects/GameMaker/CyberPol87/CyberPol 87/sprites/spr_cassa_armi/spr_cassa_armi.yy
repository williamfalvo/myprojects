{
    "id": "67ec747d-e116-45c7-b4b7-9a4551df6b56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cassa_armi",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 59,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b79c402-1515-408d-a030-0a023247ff49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67ec747d-e116-45c7-b4b7-9a4551df6b56",
            "compositeImage": {
                "id": "5d833ef8-e49b-4b01-be48-73059c23804d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b79c402-1515-408d-a030-0a023247ff49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "657d5f48-4d07-428e-8103-038991caade8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b79c402-1515-408d-a030-0a023247ff49",
                    "LayerId": "8d91ee65-7391-40e5-9143-1ddeff870b71"
                }
            ]
        },
        {
            "id": "fc23b294-6bb7-4ea2-8c7d-d9ee3a36e285",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67ec747d-e116-45c7-b4b7-9a4551df6b56",
            "compositeImage": {
                "id": "7fc6b52f-701a-41b3-a620-429d4f9508da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc23b294-6bb7-4ea2-8c7d-d9ee3a36e285",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ed4aaf2-91ab-4bdd-9172-3623bb486482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc23b294-6bb7-4ea2-8c7d-d9ee3a36e285",
                    "LayerId": "8d91ee65-7391-40e5-9143-1ddeff870b71"
                }
            ]
        },
        {
            "id": "114c6277-b2a7-44dd-8ec8-f49209140853",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67ec747d-e116-45c7-b4b7-9a4551df6b56",
            "compositeImage": {
                "id": "dd7c79b0-beeb-456c-a72e-cbcef20dc709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "114c6277-b2a7-44dd-8ec8-f49209140853",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7535f3b0-46cb-4e3c-bcdd-9a7d5f2bab96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "114c6277-b2a7-44dd-8ec8-f49209140853",
                    "LayerId": "8d91ee65-7391-40e5-9143-1ddeff870b71"
                }
            ]
        },
        {
            "id": "5331d956-82b6-49b0-8cb9-a3ce6a90a2e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67ec747d-e116-45c7-b4b7-9a4551df6b56",
            "compositeImage": {
                "id": "201e5e18-f6ae-461d-bbba-823dcd8abf71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5331d956-82b6-49b0-8cb9-a3ce6a90a2e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9814736-dec5-4055-a362-09eee2182d41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5331d956-82b6-49b0-8cb9-a3ce6a90a2e6",
                    "LayerId": "8d91ee65-7391-40e5-9143-1ddeff870b71"
                }
            ]
        },
        {
            "id": "19b26ceb-2e43-4f92-8e05-d7cb5f48255b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67ec747d-e116-45c7-b4b7-9a4551df6b56",
            "compositeImage": {
                "id": "860d8f94-d560-4bb4-8199-1c133a2b022b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19b26ceb-2e43-4f92-8e05-d7cb5f48255b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c334a43c-2c1b-4fff-8ae2-2b875c1af778",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19b26ceb-2e43-4f92-8e05-d7cb5f48255b",
                    "LayerId": "8d91ee65-7391-40e5-9143-1ddeff870b71"
                }
            ]
        },
        {
            "id": "1eefe4a2-269a-44b4-88cc-609b631b98a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67ec747d-e116-45c7-b4b7-9a4551df6b56",
            "compositeImage": {
                "id": "df513f94-fccd-46df-be71-07641bca675c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eefe4a2-269a-44b4-88cc-609b631b98a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "192ee5e0-b956-4b35-b51e-ce1358c8ae87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eefe4a2-269a-44b4-88cc-609b631b98a8",
                    "LayerId": "8d91ee65-7391-40e5-9143-1ddeff870b71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8d91ee65-7391-40e5-9143-1ddeff870b71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67ec747d-e116-45c7-b4b7-9a4551df6b56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}