{
    "id": "c3982258-691d-4829-888e-359f1a934931",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1284bf0-4c29-423c-bae2-9b5d20cb823c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3982258-691d-4829-888e-359f1a934931",
            "compositeImage": {
                "id": "beef56c0-5fcb-44bd-a089-4f91c906d0ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1284bf0-4c29-423c-bae2-9b5d20cb823c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd61dd78-d854-4ec9-b9a1-f72c568a9a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1284bf0-4c29-423c-bae2-9b5d20cb823c",
                    "LayerId": "d43d731b-445e-45f2-ae21-e795a71d06f3"
                }
            ]
        },
        {
            "id": "84dc0769-36af-4a0e-b0ca-5b197f93046d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3982258-691d-4829-888e-359f1a934931",
            "compositeImage": {
                "id": "19d8e236-88a6-4730-8719-faf7e774d8e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84dc0769-36af-4a0e-b0ca-5b197f93046d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e66132b-d36d-4784-a1f1-29b86b866ab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84dc0769-36af-4a0e-b0ca-5b197f93046d",
                    "LayerId": "d43d731b-445e-45f2-ae21-e795a71d06f3"
                }
            ]
        },
        {
            "id": "37ab5b86-bc70-4d84-ba2e-c74527ad50a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3982258-691d-4829-888e-359f1a934931",
            "compositeImage": {
                "id": "03caec69-01e6-4187-997e-0bff7686b91e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37ab5b86-bc70-4d84-ba2e-c74527ad50a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "badf905e-570a-4379-a3b4-64c73c4edf95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37ab5b86-bc70-4d84-ba2e-c74527ad50a3",
                    "LayerId": "d43d731b-445e-45f2-ae21-e795a71d06f3"
                }
            ]
        },
        {
            "id": "929d570b-ac43-4d55-b1d2-744f97e6fa00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3982258-691d-4829-888e-359f1a934931",
            "compositeImage": {
                "id": "65e8044d-372a-41bc-b3c2-751ae10349a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "929d570b-ac43-4d55-b1d2-744f97e6fa00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a839165b-0f3a-40e9-a98e-47e2b2db8a67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "929d570b-ac43-4d55-b1d2-744f97e6fa00",
                    "LayerId": "d43d731b-445e-45f2-ae21-e795a71d06f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d43d731b-445e-45f2-ae21-e795a71d06f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3982258-691d-4829-888e-359f1a934931",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}