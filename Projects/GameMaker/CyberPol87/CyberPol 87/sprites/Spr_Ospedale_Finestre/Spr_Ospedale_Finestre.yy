{
    "id": "ffccbf19-b15c-4067-add2-c5d89fc8a8f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Spr_Ospedale_Finestre",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 671,
    "bbox_left": 15,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb7690fe-f53f-405a-91de-596b59c480ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffccbf19-b15c-4067-add2-c5d89fc8a8f7",
            "compositeImage": {
                "id": "6e6a6e6b-9a11-4f3e-81d2-963408012e5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb7690fe-f53f-405a-91de-596b59c480ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4374461c-04ad-4f32-b6f4-778d70eaaffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb7690fe-f53f-405a-91de-596b59c480ea",
                    "LayerId": "8e4847c7-d1c6-40fd-ad15-a7473a62d045"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 736,
    "layers": [
        {
            "id": "8e4847c7-d1c6-40fd-ad15-a7473a62d045",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffccbf19-b15c-4067-add2-c5d89fc8a8f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}