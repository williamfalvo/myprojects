{
    "id": "88c55fed-50a9-4fe6-982f-5208d960f102",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ENTER_A_per_continuare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 107,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d92caaa-5833-4117-8958-f2c8e3cac2b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88c55fed-50a9-4fe6-982f-5208d960f102",
            "compositeImage": {
                "id": "3a852968-57d9-4526-8e8d-9555077c17c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d92caaa-5833-4117-8958-f2c8e3cac2b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2047f618-7ff3-46f9-8e94-32b26fff2e1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d92caaa-5833-4117-8958-f2c8e3cac2b0",
                    "LayerId": "02a8f782-1c6a-4f54-86f6-8d44fce35f65"
                }
            ]
        },
        {
            "id": "8a3eab1e-d824-4ede-bb26-8461b8937b77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88c55fed-50a9-4fe6-982f-5208d960f102",
            "compositeImage": {
                "id": "5401358a-30d2-4a22-b3ec-3a86918e6038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a3eab1e-d824-4ede-bb26-8461b8937b77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e074d902-a13f-49ae-92aa-0f283a39ab97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a3eab1e-d824-4ede-bb26-8461b8937b77",
                    "LayerId": "02a8f782-1c6a-4f54-86f6-8d44fce35f65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "02a8f782-1c6a-4f54-86f6-8d44fce35f65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88c55fed-50a9-4fe6-982f-5208d960f102",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 54,
    "yorig": 20
}