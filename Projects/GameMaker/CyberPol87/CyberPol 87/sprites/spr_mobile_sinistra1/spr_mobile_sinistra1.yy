{
    "id": "79b0002e-a21e-4d0f-9c37-ebb2da8e311c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mobile_sinistra1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1133d5d-7cb6-40be-a233-5f9e8bd278ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79b0002e-a21e-4d0f-9c37-ebb2da8e311c",
            "compositeImage": {
                "id": "b83b8b27-953b-4ffc-9a26-2fc4dc7e6745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1133d5d-7cb6-40be-a233-5f9e8bd278ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15e8c121-875b-43aa-ac46-d8c26c8104c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1133d5d-7cb6-40be-a233-5f9e8bd278ec",
                    "LayerId": "7b507ae2-091a-4ea8-b835-dadc986dd6c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7b507ae2-091a-4ea8-b835-dadc986dd6c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79b0002e-a21e-4d0f-9c37-ebb2da8e311c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}