{
    "id": "75df0254-8467-4c69-bca9-2b0be7bb823d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_NO",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 8,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12427a0e-e0f9-4d49-83a4-b5760f16b6a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75df0254-8467-4c69-bca9-2b0be7bb823d",
            "compositeImage": {
                "id": "9b3e04c0-513d-4b0f-9cd8-abccd94f06a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12427a0e-e0f9-4d49-83a4-b5760f16b6a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6533a96a-256f-47e0-956e-683e58988090",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12427a0e-e0f9-4d49-83a4-b5760f16b6a5",
                    "LayerId": "fa10317c-4b04-4545-8d55-ebe145076561"
                }
            ]
        },
        {
            "id": "7b124c9b-be09-485f-8eae-271200c75940",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75df0254-8467-4c69-bca9-2b0be7bb823d",
            "compositeImage": {
                "id": "e83e77be-ff0c-4b5c-8eed-e8a73bb0eee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b124c9b-be09-485f-8eae-271200c75940",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf4e43f3-5f06-4847-85ea-01244a9a6174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b124c9b-be09-485f-8eae-271200c75940",
                    "LayerId": "fa10317c-4b04-4545-8d55-ebe145076561"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "fa10317c-4b04-4545-8d55-ebe145076561",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75df0254-8467-4c69-bca9-2b0be7bb823d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 73,
    "xorig": 36,
    "yorig": 23
}