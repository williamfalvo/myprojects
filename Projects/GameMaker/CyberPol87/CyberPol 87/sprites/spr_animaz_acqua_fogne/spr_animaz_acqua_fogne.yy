{
    "id": "d9f54456-cef1-4998-ab31-4b9038c2a118",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_animaz_acqua_fogne",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f590de6c-e95a-4207-9ab6-b70554af9b5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9f54456-cef1-4998-ab31-4b9038c2a118",
            "compositeImage": {
                "id": "e6e453e8-1994-46c1-bc81-e2a8a92d1b66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f590de6c-e95a-4207-9ab6-b70554af9b5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f611441b-e58c-47f9-8084-e42dbf91ae11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f590de6c-e95a-4207-9ab6-b70554af9b5e",
                    "LayerId": "e24acbe9-7247-4268-80cb-4b8b60bce266"
                }
            ]
        },
        {
            "id": "7fba705e-2c0e-4d0f-af3c-9e03dca92e98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9f54456-cef1-4998-ab31-4b9038c2a118",
            "compositeImage": {
                "id": "e501f2ba-edc1-4976-9008-b4f49a76061d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fba705e-2c0e-4d0f-af3c-9e03dca92e98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fba8ad5d-1cca-4920-961f-a2d385b2f0dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fba705e-2c0e-4d0f-af3c-9e03dca92e98",
                    "LayerId": "e24acbe9-7247-4268-80cb-4b8b60bce266"
                }
            ]
        },
        {
            "id": "5039a8c0-2ab9-41d2-ba2e-0ca507fb45f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9f54456-cef1-4998-ab31-4b9038c2a118",
            "compositeImage": {
                "id": "d01fe6fb-46ac-46ab-b43a-1658e453f9a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5039a8c0-2ab9-41d2-ba2e-0ca507fb45f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ce78eb7-ce0c-4e3a-be79-54691376f2bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5039a8c0-2ab9-41d2-ba2e-0ca507fb45f6",
                    "LayerId": "e24acbe9-7247-4268-80cb-4b8b60bce266"
                }
            ]
        },
        {
            "id": "faa7cc21-07ce-4e6b-ac50-54b2afa8da27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9f54456-cef1-4998-ab31-4b9038c2a118",
            "compositeImage": {
                "id": "89ddcbdf-91fe-4db9-8271-b149b0a56197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faa7cc21-07ce-4e6b-ac50-54b2afa8da27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61e86098-e5b0-4572-b250-2911e23ddeba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faa7cc21-07ce-4e6b-ac50-54b2afa8da27",
                    "LayerId": "e24acbe9-7247-4268-80cb-4b8b60bce266"
                }
            ]
        },
        {
            "id": "09efb549-8efd-437e-bd81-b9cf382ec63f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9f54456-cef1-4998-ab31-4b9038c2a118",
            "compositeImage": {
                "id": "608d2efd-31ef-4c35-a6dc-3a21b51a7467",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09efb549-8efd-437e-bd81-b9cf382ec63f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46abf7b4-67a8-42be-9952-de34a36a9218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09efb549-8efd-437e-bd81-b9cf382ec63f",
                    "LayerId": "e24acbe9-7247-4268-80cb-4b8b60bce266"
                }
            ]
        },
        {
            "id": "dfb3ed72-3f84-4724-b640-3306267373ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9f54456-cef1-4998-ab31-4b9038c2a118",
            "compositeImage": {
                "id": "df043a86-d699-4351-94fc-c6a3fc0d8b7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfb3ed72-3f84-4724-b640-3306267373ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a031fbb-2a9b-4a8b-a8e3-e5c2e6603790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfb3ed72-3f84-4724-b640-3306267373ef",
                    "LayerId": "e24acbe9-7247-4268-80cb-4b8b60bce266"
                }
            ]
        },
        {
            "id": "3a899121-d730-443f-a76b-88191da30e74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9f54456-cef1-4998-ab31-4b9038c2a118",
            "compositeImage": {
                "id": "18f63c28-6125-46c4-af27-b88f9c1e82c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a899121-d730-443f-a76b-88191da30e74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2ea2910-147d-4ca9-9ead-0df340142df1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a899121-d730-443f-a76b-88191da30e74",
                    "LayerId": "e24acbe9-7247-4268-80cb-4b8b60bce266"
                }
            ]
        },
        {
            "id": "8ef75dda-c324-4c95-9d46-6b7a44ad2f41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9f54456-cef1-4998-ab31-4b9038c2a118",
            "compositeImage": {
                "id": "a167002e-5e03-47f3-8120-7daae7805ef5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ef75dda-c324-4c95-9d46-6b7a44ad2f41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7b01f98-f695-425c-b84e-72987b5dc51c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ef75dda-c324-4c95-9d46-6b7a44ad2f41",
                    "LayerId": "e24acbe9-7247-4268-80cb-4b8b60bce266"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e24acbe9-7247-4268-80cb-4b8b60bce266",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9f54456-cef1-4998-ab31-4b9038c2a118",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}