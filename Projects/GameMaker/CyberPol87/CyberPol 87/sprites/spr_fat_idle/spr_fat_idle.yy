{
    "id": "6178b4ed-5f95-4fa9-93bb-929ad286808b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fat_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 49,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "336dafa3-2bd3-4b0b-8ea2-b8501c2a98b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6178b4ed-5f95-4fa9-93bb-929ad286808b",
            "compositeImage": {
                "id": "5fcda418-7d41-4fcd-bf2a-ad5fd188437c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "336dafa3-2bd3-4b0b-8ea2-b8501c2a98b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f62d762f-630c-49bf-919d-94ba84cd38d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "336dafa3-2bd3-4b0b-8ea2-b8501c2a98b5",
                    "LayerId": "58ab575e-837d-4bc0-9119-bb9324972876"
                }
            ]
        },
        {
            "id": "49b68ced-5548-44cc-8b1c-70a473ac8c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6178b4ed-5f95-4fa9-93bb-929ad286808b",
            "compositeImage": {
                "id": "846a332b-80e9-437b-b98f-d8ea374e683b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49b68ced-5548-44cc-8b1c-70a473ac8c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2063ea6d-ad70-4596-9312-3c3b1009afa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49b68ced-5548-44cc-8b1c-70a473ac8c87",
                    "LayerId": "58ab575e-837d-4bc0-9119-bb9324972876"
                }
            ]
        },
        {
            "id": "051d1ac5-12fb-4935-9335-fb15b0c9b9ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6178b4ed-5f95-4fa9-93bb-929ad286808b",
            "compositeImage": {
                "id": "291bb73f-f3b2-4976-a09f-fb88d7fc77c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "051d1ac5-12fb-4935-9335-fb15b0c9b9ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b114d453-3591-4e4d-93e6-daaf267259b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "051d1ac5-12fb-4935-9335-fb15b0c9b9ad",
                    "LayerId": "58ab575e-837d-4bc0-9119-bb9324972876"
                }
            ]
        },
        {
            "id": "df89b8f2-84e4-4a5f-bdc8-8ce3eea74bf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6178b4ed-5f95-4fa9-93bb-929ad286808b",
            "compositeImage": {
                "id": "b15fcb70-25f8-40bb-8c93-6c3ecbb8fe75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df89b8f2-84e4-4a5f-bdc8-8ce3eea74bf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f591cb6-b37b-4fe0-bcba-aa9edfae8fc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df89b8f2-84e4-4a5f-bdc8-8ce3eea74bf6",
                    "LayerId": "58ab575e-837d-4bc0-9119-bb9324972876"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "58ab575e-837d-4bc0-9119-bb9324972876",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6178b4ed-5f95-4fa9-93bb-929ad286808b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}