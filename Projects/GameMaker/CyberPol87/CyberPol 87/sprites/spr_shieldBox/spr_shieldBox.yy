{
    "id": "53765c61-bce7-4929-bf06-754390497d19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shieldBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 2,
    "bbox_right": 62,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fe2c8da-d4cf-42a0-91e8-dbe2422144ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53765c61-bce7-4929-bf06-754390497d19",
            "compositeImage": {
                "id": "22651128-c954-4bed-bac4-ad35745afcf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fe2c8da-d4cf-42a0-91e8-dbe2422144ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6a44df-1265-4dc3-9aa5-b1c6ffb4143c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fe2c8da-d4cf-42a0-91e8-dbe2422144ea",
                    "LayerId": "dc262bb8-190d-4465-8c47-9df25720915c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dc262bb8-190d-4465-8c47-9df25720915c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53765c61-bce7-4929-bf06-754390497d19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 4,
    "yorig": 32
}