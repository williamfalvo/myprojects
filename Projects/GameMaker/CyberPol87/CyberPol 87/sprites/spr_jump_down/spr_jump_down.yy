{
    "id": "8f034eaa-cf00-4370-9a37-ee2431411ea4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62c0cf1f-fd90-4f58-ab36-c52c65c72af9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f034eaa-cf00-4370-9a37-ee2431411ea4",
            "compositeImage": {
                "id": "af480315-5475-40c6-a7b7-64ae91fdf606",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62c0cf1f-fd90-4f58-ab36-c52c65c72af9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96cb2179-8c7b-465c-bfc5-21ba13b6a4c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62c0cf1f-fd90-4f58-ab36-c52c65c72af9",
                    "LayerId": "22760ab7-3490-4f6f-b32e-195fb19e769f"
                }
            ]
        },
        {
            "id": "f700b53d-6cbf-4cb5-8a86-7203d5ec5a42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f034eaa-cf00-4370-9a37-ee2431411ea4",
            "compositeImage": {
                "id": "06ba5f83-fde1-4c02-aca6-19cbc051bb78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f700b53d-6cbf-4cb5-8a86-7203d5ec5a42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f716f84a-7d23-4b99-9554-e320662e7529",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f700b53d-6cbf-4cb5-8a86-7203d5ec5a42",
                    "LayerId": "22760ab7-3490-4f6f-b32e-195fb19e769f"
                }
            ]
        },
        {
            "id": "e4bcfbf6-b769-4461-b539-6b6c4c9ed559",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f034eaa-cf00-4370-9a37-ee2431411ea4",
            "compositeImage": {
                "id": "54fed9a5-3bc9-4ac1-b2f6-ec38097ca8e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4bcfbf6-b769-4461-b539-6b6c4c9ed559",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "402dfe61-f82a-4fac-b975-119df4f6d972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4bcfbf6-b769-4461-b539-6b6c4c9ed559",
                    "LayerId": "22760ab7-3490-4f6f-b32e-195fb19e769f"
                }
            ]
        },
        {
            "id": "da8e2650-1860-4d3d-a013-2157b0781057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f034eaa-cf00-4370-9a37-ee2431411ea4",
            "compositeImage": {
                "id": "6f7ba5e8-2a3f-4c6a-b6c1-c9d06b5d547f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da8e2650-1860-4d3d-a013-2157b0781057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccc281dd-d853-4483-8ef0-b604ba066af5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da8e2650-1860-4d3d-a013-2157b0781057",
                    "LayerId": "22760ab7-3490-4f6f-b32e-195fb19e769f"
                }
            ]
        },
        {
            "id": "a40858e5-efd8-4ba6-b147-62543a58c066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f034eaa-cf00-4370-9a37-ee2431411ea4",
            "compositeImage": {
                "id": "36c403fa-0db3-4860-9002-14c6350567bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a40858e5-efd8-4ba6-b147-62543a58c066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "357c4936-9b4d-49cd-aa69-cd2bf7ef681c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40858e5-efd8-4ba6-b147-62543a58c066",
                    "LayerId": "22760ab7-3490-4f6f-b32e-195fb19e769f"
                }
            ]
        },
        {
            "id": "0126b914-6e43-42ed-8d48-fe7727589f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f034eaa-cf00-4370-9a37-ee2431411ea4",
            "compositeImage": {
                "id": "698359c4-47e2-4186-bb28-d3268b0aa553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0126b914-6e43-42ed-8d48-fe7727589f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4069630e-2328-4894-9679-8994555da78d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0126b914-6e43-42ed-8d48-fe7727589f76",
                    "LayerId": "22760ab7-3490-4f6f-b32e-195fb19e769f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "22760ab7-3490-4f6f-b32e-195fb19e769f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f034eaa-cf00-4370-9a37-ee2431411ea4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}