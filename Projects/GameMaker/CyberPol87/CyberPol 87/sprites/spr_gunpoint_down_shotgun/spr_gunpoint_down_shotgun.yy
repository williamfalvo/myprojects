{
    "id": "b5e70a72-e834-4d5f-802f-46e9c882d01f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_down_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 16,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf98998a-2663-496d-ab7d-d2d6abb49c47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5e70a72-e834-4d5f-802f-46e9c882d01f",
            "compositeImage": {
                "id": "ea9d6794-3658-4476-a43a-d4aeeb94184f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf98998a-2663-496d-ab7d-d2d6abb49c47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26e27867-5f05-4eea-8b27-5bb00676f338",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf98998a-2663-496d-ab7d-d2d6abb49c47",
                    "LayerId": "4fd54e4b-004f-46ec-9825-369264e1301f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "4fd54e4b-004f-46ec-9825-369264e1301f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5e70a72-e834-4d5f-802f-46e9c882d01f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}