{
    "id": "dbc0e382-7a90-4c71-940b-69ff346e6679",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c70e7d6c-a42d-4581-8b6b-84cf5c5ca6f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbc0e382-7a90-4c71-940b-69ff346e6679",
            "compositeImage": {
                "id": "aa226f97-1178-4a1a-8500-e3005458f26e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c70e7d6c-a42d-4581-8b6b-84cf5c5ca6f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e44695f-88e7-4506-9225-49e5f02f1c3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c70e7d6c-a42d-4581-8b6b-84cf5c5ca6f4",
                    "LayerId": "56418d7e-4b35-434b-9aaa-c84cde4d9351"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "56418d7e-4b35-434b-9aaa-c84cde4d9351",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbc0e382-7a90-4c71-940b-69ff346e6679",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 9
}