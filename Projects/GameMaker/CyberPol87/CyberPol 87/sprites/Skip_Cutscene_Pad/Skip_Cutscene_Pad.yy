{
    "id": "1f75eea7-8f8a-4e0d-97cf-410350372a9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Skip_Cutscene_Pad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 4,
    "bbox_right": 145,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20e42d80-06d4-47a4-a722-4a2663cd1e1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f75eea7-8f8a-4e0d-97cf-410350372a9e",
            "compositeImage": {
                "id": "855f6ab5-d03d-4bf4-9954-966cbe70f1af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20e42d80-06d4-47a4-a722-4a2663cd1e1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eed4d141-feaa-4021-b4e0-ea354b039d83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20e42d80-06d4-47a4-a722-4a2663cd1e1d",
                    "LayerId": "f1f937e7-7715-4996-bf6d-046d948374a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "f1f937e7-7715-4996-bf6d-046d948374a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f75eea7-8f8a-4e0d-97cf-410350372a9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 75,
    "yorig": 36
}