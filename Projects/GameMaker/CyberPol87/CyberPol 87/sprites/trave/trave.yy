{
    "id": "546d065d-5ded-4d3e-abca-09ee904b46cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "trave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a7e7216-6b9c-4c71-b42f-4fc7681d9af8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "546d065d-5ded-4d3e-abca-09ee904b46cd",
            "compositeImage": {
                "id": "1171e7d6-cde2-4238-addc-536fdad16022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a7e7216-6b9c-4c71-b42f-4fc7681d9af8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c840d09-a1aa-4400-9b07-59325f68c54a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a7e7216-6b9c-4c71-b42f-4fc7681d9af8",
                    "LayerId": "6ce4d5f5-1640-4b6d-89f6-2ed78654a106"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6ce4d5f5-1640-4b6d-89f6-2ed78654a106",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "546d065d-5ded-4d3e-abca-09ee904b46cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}