{
    "id": "f8935893-26b2-43ba-b9a5-8e9af6f129c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_barile_fogna2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 27,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d299ebe-218d-4eff-bd32-ba8d6a443196",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8935893-26b2-43ba-b9a5-8e9af6f129c1",
            "compositeImage": {
                "id": "0c50e0d8-9ae3-46da-a847-a9d37bd33e71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d299ebe-218d-4eff-bd32-ba8d6a443196",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3f810c3-301b-4563-b688-3044d44d1474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d299ebe-218d-4eff-bd32-ba8d6a443196",
                    "LayerId": "bf557d40-d6a9-4fa4-813f-8b9ce222ee78"
                }
            ]
        },
        {
            "id": "f5abe1a2-529e-43aa-9f4d-6885dfc61427",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8935893-26b2-43ba-b9a5-8e9af6f129c1",
            "compositeImage": {
                "id": "896945a7-50b7-42ac-ad5c-77781db9d47f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5abe1a2-529e-43aa-9f4d-6885dfc61427",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5b9097a-af0e-4393-811e-cbc399fb1062",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5abe1a2-529e-43aa-9f4d-6885dfc61427",
                    "LayerId": "bf557d40-d6a9-4fa4-813f-8b9ce222ee78"
                }
            ]
        },
        {
            "id": "3698c102-fe92-4c52-a85c-7720964c0632",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8935893-26b2-43ba-b9a5-8e9af6f129c1",
            "compositeImage": {
                "id": "9c214066-5bdb-4e5c-989b-d45989b3aefa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3698c102-fe92-4c52-a85c-7720964c0632",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05fd3150-ff06-401b-8beb-a44cdef39b7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3698c102-fe92-4c52-a85c-7720964c0632",
                    "LayerId": "bf557d40-d6a9-4fa4-813f-8b9ce222ee78"
                }
            ]
        },
        {
            "id": "4809da83-0c66-446d-a76b-d658c0c4c0f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8935893-26b2-43ba-b9a5-8e9af6f129c1",
            "compositeImage": {
                "id": "9fa5359f-dfca-4705-8c1a-9b1c90d5aadb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4809da83-0c66-446d-a76b-d658c0c4c0f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb534dc1-fdef-4633-bbd7-3b8969a2041f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4809da83-0c66-446d-a76b-d658c0c4c0f7",
                    "LayerId": "bf557d40-d6a9-4fa4-813f-8b9ce222ee78"
                }
            ]
        },
        {
            "id": "e57b25bb-1d0a-46c5-8539-c411b0141c15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8935893-26b2-43ba-b9a5-8e9af6f129c1",
            "compositeImage": {
                "id": "294c675d-5ad6-4008-acaa-35787c772834",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e57b25bb-1d0a-46c5-8539-c411b0141c15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b474987a-07ad-4853-be5d-88340a8b3db2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e57b25bb-1d0a-46c5-8539-c411b0141c15",
                    "LayerId": "bf557d40-d6a9-4fa4-813f-8b9ce222ee78"
                }
            ]
        },
        {
            "id": "e4552d77-cf36-443c-b54f-6e4ea1f812b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8935893-26b2-43ba-b9a5-8e9af6f129c1",
            "compositeImage": {
                "id": "efb54bae-6f7a-44c2-aa2d-ba25a95ef30c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4552d77-cf36-443c-b54f-6e4ea1f812b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6191ad9-767b-4593-a8f5-6d4ac335edae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4552d77-cf36-443c-b54f-6e4ea1f812b5",
                    "LayerId": "bf557d40-d6a9-4fa4-813f-8b9ce222ee78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bf557d40-d6a9-4fa4-813f-8b9ce222ee78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8935893-26b2-43ba-b9a5-8e9af6f129c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}