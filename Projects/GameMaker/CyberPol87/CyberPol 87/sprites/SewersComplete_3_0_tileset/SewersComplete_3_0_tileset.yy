{
    "id": "3019020e-70be-4d6f-a520-837102bc7e81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "SewersComplete_3_0_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbcb3d36-c0b5-4c32-9474-19dc763527a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3019020e-70be-4d6f-a520-837102bc7e81",
            "compositeImage": {
                "id": "7c60b900-4794-436d-99f6-0d3fef583717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbcb3d36-c0b5-4c32-9474-19dc763527a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "487ab29a-12ad-4b4e-8ecf-b451e33d893d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbcb3d36-c0b5-4c32-9474-19dc763527a6",
                    "LayerId": "cd07823c-9a6c-4311-ad04-175a6e2fa9cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "cd07823c-9a6c-4311-ad04-175a6e2fa9cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3019020e-70be-4d6f-a520-837102bc7e81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}