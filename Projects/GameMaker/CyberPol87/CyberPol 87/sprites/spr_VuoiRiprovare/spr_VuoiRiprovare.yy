{
    "id": "40f1c951-fa78-48cc-b7bb-840c566ff710",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_VuoiRiprovare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 138,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a726a542-1f71-4844-8805-56dfeec16b23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40f1c951-fa78-48cc-b7bb-840c566ff710",
            "compositeImage": {
                "id": "12ac8952-44dd-425d-b440-dfc749094dd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a726a542-1f71-4844-8805-56dfeec16b23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61e6a640-623b-428b-9003-8837b921de8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a726a542-1f71-4844-8805-56dfeec16b23",
                    "LayerId": "2030b01a-41e8-432e-b9be-c21060a2e8d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "2030b01a-41e8-432e-b9be-c21060a2e8d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40f1c951-fa78-48cc-b7bb-840c566ff710",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 72,
    "yorig": 15
}