{
    "id": "81be589c-653d-46b7-8a7f-20cba7bea768",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ESCi",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 71,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87cd1b7e-efd2-445f-9e9d-b37120276198",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81be589c-653d-46b7-8a7f-20cba7bea768",
            "compositeImage": {
                "id": "ec7e65de-403f-4ee4-af64-db98487ee7e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87cd1b7e-efd2-445f-9e9d-b37120276198",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e3d8820-072f-4bb6-a4bd-9d8814073323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87cd1b7e-efd2-445f-9e9d-b37120276198",
                    "LayerId": "98977303-9e4d-40cc-99e2-4c61548a8f7d"
                }
            ]
        },
        {
            "id": "e260b5f6-0ae3-4683-8c47-e1cdd568194b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81be589c-653d-46b7-8a7f-20cba7bea768",
            "compositeImage": {
                "id": "63c6e19c-0e58-450f-bb01-89c52f4900ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e260b5f6-0ae3-4683-8c47-e1cdd568194b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63d7b5ae-1d85-4b55-b68c-33ab4414b226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e260b5f6-0ae3-4683-8c47-e1cdd568194b",
                    "LayerId": "98977303-9e4d-40cc-99e2-4c61548a8f7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "98977303-9e4d-40cc-99e2-4c61548a8f7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81be589c-653d-46b7-8a7f-20cba7bea768",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 38,
    "yorig": 19
}