{
    "id": "1483fc32-a1f8-4c6d-987d-9c969c152251",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "PlatformSewers_2_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b41d74e-9aa8-477f-9c7b-12c205751689",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1483fc32-a1f8-4c6d-987d-9c969c152251",
            "compositeImage": {
                "id": "8173b3c9-f211-41f7-a364-545fae252e7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b41d74e-9aa8-477f-9c7b-12c205751689",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "924a7adf-5109-4cf3-afed-23f0bd71de48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b41d74e-9aa8-477f-9c7b-12c205751689",
                    "LayerId": "6b3a5984-5edd-4cbb-89a1-838a1af60383"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6b3a5984-5edd-4cbb-89a1-838a1af60383",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1483fc32-a1f8-4c6d-987d-9c969c152251",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}