{
    "id": "479cbae3-f068-4e30-98f1-7f77ba2bc7cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 38,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c2fe975-2faa-46bd-b645-dcd3cf811177",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "479cbae3-f068-4e30-98f1-7f77ba2bc7cf",
            "compositeImage": {
                "id": "94484a22-658f-4ebf-aebf-14fffb234a62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c2fe975-2faa-46bd-b645-dcd3cf811177",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce322bcf-ab47-4748-9878-2a3c3227126b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c2fe975-2faa-46bd-b645-dcd3cf811177",
                    "LayerId": "216427b6-f390-41bb-b95e-a9918a8ea36c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "216427b6-f390-41bb-b95e-a9918a8ea36c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "479cbae3-f068-4e30-98f1-7f77ba2bc7cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}