{
    "id": "92c8b584-7a1e-4c7a-b8b5-ba2857ca90f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_piattaforma_mobile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c91680f8-23b4-48a7-965d-21600f125014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92c8b584-7a1e-4c7a-b8b5-ba2857ca90f0",
            "compositeImage": {
                "id": "bf5fac3e-b013-4786-a4bf-428d776b5384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c91680f8-23b4-48a7-965d-21600f125014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d59e1cb8-d0c8-4c7f-93e4-a2d3d3336d7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c91680f8-23b4-48a7-965d-21600f125014",
                    "LayerId": "81820755-9479-4a93-b78f-5226a7a651bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "81820755-9479-4a93-b78f-5226a7a651bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92c8b584-7a1e-4c7a-b8b5-ba2857ca90f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}