{
    "id": "502bba73-089b-446b-8ee9-83bda9c65106",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_ground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e135a332-19b4-4da6-9468-07a19a2ca7ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "502bba73-089b-446b-8ee9-83bda9c65106",
            "compositeImage": {
                "id": "8ebec7f1-a179-4037-8a9e-360d7908611d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e135a332-19b4-4da6-9468-07a19a2ca7ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0368f66-3811-4fda-aa14-42d3bd213d4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e135a332-19b4-4da6-9468-07a19a2ca7ab",
                    "LayerId": "d1fd6812-b8e5-45ed-a4c5-eb0d163599d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d1fd6812-b8e5-45ed-a4c5-eb0d163599d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "502bba73-089b-446b-8ee9-83bda9c65106",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}