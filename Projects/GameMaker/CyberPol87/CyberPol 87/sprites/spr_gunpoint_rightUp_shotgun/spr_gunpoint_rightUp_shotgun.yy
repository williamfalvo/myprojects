{
    "id": "3f79136f-f712-412c-8615-ef65ecc008cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_rightUp_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 16,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3009db4-5b10-4b4e-a06f-3a9174c46bb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f79136f-f712-412c-8615-ef65ecc008cd",
            "compositeImage": {
                "id": "d63ea7a9-d65d-4446-ba9f-566f8a378002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3009db4-5b10-4b4e-a06f-3a9174c46bb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94987bd5-f407-41f3-b0be-a784c6feb4c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3009db4-5b10-4b4e-a06f-3a9174c46bb5",
                    "LayerId": "42da9394-1d98-488c-a565-bb1e122a818c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "42da9394-1d98-488c-a565-bb1e122a818c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f79136f-f712-412c-8615-ef65ecc008cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}