{
    "id": "6f208998-0f56-40ad-9d54-628464a46397",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lampada",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d53f82a6-db21-426a-99ce-90720854e04c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f208998-0f56-40ad-9d54-628464a46397",
            "compositeImage": {
                "id": "7feabdfe-cab8-4b18-84a2-e7cfd311faee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d53f82a6-db21-426a-99ce-90720854e04c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e6971ba-c0fa-40dc-97be-07e62ceaa5fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d53f82a6-db21-426a-99ce-90720854e04c",
                    "LayerId": "cdaa58e0-76a6-428b-adb8-15324768b9ad"
                }
            ]
        },
        {
            "id": "0a636b64-3322-4ef6-ad1f-e38d7d36557f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f208998-0f56-40ad-9d54-628464a46397",
            "compositeImage": {
                "id": "a83a9c8f-4763-4bc3-a8eb-dd123c853906",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a636b64-3322-4ef6-ad1f-e38d7d36557f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "933f9379-c475-4ef4-99f7-ac9d6ac3f513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a636b64-3322-4ef6-ad1f-e38d7d36557f",
                    "LayerId": "cdaa58e0-76a6-428b-adb8-15324768b9ad"
                }
            ]
        },
        {
            "id": "861df850-0cfa-422c-8d1a-36c1d1b43491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f208998-0f56-40ad-9d54-628464a46397",
            "compositeImage": {
                "id": "661f1e37-a45f-40a9-9c2b-fb3c3c06d005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "861df850-0cfa-422c-8d1a-36c1d1b43491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb9a87b1-9f14-498f-b7cc-b6e6ba7e756c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "861df850-0cfa-422c-8d1a-36c1d1b43491",
                    "LayerId": "cdaa58e0-76a6-428b-adb8-15324768b9ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cdaa58e0-76a6-428b-adb8-15324768b9ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f208998-0f56-40ad-9d54-628464a46397",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}