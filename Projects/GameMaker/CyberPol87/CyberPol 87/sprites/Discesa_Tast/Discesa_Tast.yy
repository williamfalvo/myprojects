{
    "id": "fe856ccb-6d2d-4202-b0ef-a1ac76fc36e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Discesa_Tast",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4b3bf73-1678-48e6-a3b6-43a2192549cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe856ccb-6d2d-4202-b0ef-a1ac76fc36e5",
            "compositeImage": {
                "id": "e7e2033c-185d-46a9-9683-f44f9f5c3249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4b3bf73-1678-48e6-a3b6-43a2192549cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf4719b1-b349-48fc-ae9b-5e998cb97e40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4b3bf73-1678-48e6-a3b6-43a2192549cc",
                    "LayerId": "a1d86927-6927-4aeb-b520-65cdbd2e0e04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "a1d86927-6927-4aeb-b520-65cdbd2e0e04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe856ccb-6d2d-4202-b0ef-a1ac76fc36e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}