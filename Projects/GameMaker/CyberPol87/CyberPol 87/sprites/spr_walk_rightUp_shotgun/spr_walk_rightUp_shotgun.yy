{
    "id": "06e32f42-a410-4dac-88ae-e73ef1cfca69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_rightUp_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbd1895e-00b9-4358-a884-353ef757c7ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06e32f42-a410-4dac-88ae-e73ef1cfca69",
            "compositeImage": {
                "id": "724411f6-c7df-45f6-94a7-76951fce29cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd1895e-00b9-4358-a884-353ef757c7ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58546abe-878c-4f0a-8712-4396c74978cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd1895e-00b9-4358-a884-353ef757c7ef",
                    "LayerId": "e71ed81a-a1c0-4cdb-9f31-f00512b2388c"
                }
            ]
        },
        {
            "id": "53469573-6d3c-4a96-8f89-2be6eea8ec3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06e32f42-a410-4dac-88ae-e73ef1cfca69",
            "compositeImage": {
                "id": "88fe13d7-7dc2-4706-8156-c3edd7c3ba80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53469573-6d3c-4a96-8f89-2be6eea8ec3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da83b6c9-4e48-4930-ad67-7994f104b3c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53469573-6d3c-4a96-8f89-2be6eea8ec3e",
                    "LayerId": "e71ed81a-a1c0-4cdb-9f31-f00512b2388c"
                }
            ]
        },
        {
            "id": "aeee768b-2be5-4379-92e4-0aff6c303f9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06e32f42-a410-4dac-88ae-e73ef1cfca69",
            "compositeImage": {
                "id": "ba2bfac1-d1e7-487c-8e67-2927431b9160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeee768b-2be5-4379-92e4-0aff6c303f9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ad7ece6-83ac-4f5c-bb62-ee616b050755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeee768b-2be5-4379-92e4-0aff6c303f9d",
                    "LayerId": "e71ed81a-a1c0-4cdb-9f31-f00512b2388c"
                }
            ]
        },
        {
            "id": "c3dfe563-f03e-4f93-8095-15da3d139d82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06e32f42-a410-4dac-88ae-e73ef1cfca69",
            "compositeImage": {
                "id": "2d658720-8977-433d-849a-f36a097c3bcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3dfe563-f03e-4f93-8095-15da3d139d82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6162b78f-1e12-431d-b3c7-929331cb3e23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3dfe563-f03e-4f93-8095-15da3d139d82",
                    "LayerId": "e71ed81a-a1c0-4cdb-9f31-f00512b2388c"
                }
            ]
        },
        {
            "id": "e19df788-1e62-4091-8c33-36e3004f3af8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06e32f42-a410-4dac-88ae-e73ef1cfca69",
            "compositeImage": {
                "id": "a0609e0f-d73a-420c-9232-e1b82c76b1d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e19df788-1e62-4091-8c33-36e3004f3af8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b79f512-4277-4632-8b1f-d5ad458d42ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e19df788-1e62-4091-8c33-36e3004f3af8",
                    "LayerId": "e71ed81a-a1c0-4cdb-9f31-f00512b2388c"
                }
            ]
        },
        {
            "id": "2685df73-22aa-4776-98e1-64d599807a65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06e32f42-a410-4dac-88ae-e73ef1cfca69",
            "compositeImage": {
                "id": "2540a25b-84d6-4623-9681-24719a5bf5d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2685df73-22aa-4776-98e1-64d599807a65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa1877b1-4663-4786-98e3-28fcfb7f0c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2685df73-22aa-4776-98e1-64d599807a65",
                    "LayerId": "e71ed81a-a1c0-4cdb-9f31-f00512b2388c"
                }
            ]
        },
        {
            "id": "44d8332c-ad40-435a-828e-a6c3df815380",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06e32f42-a410-4dac-88ae-e73ef1cfca69",
            "compositeImage": {
                "id": "8888ad69-00db-48e0-a637-39a626945e50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44d8332c-ad40-435a-828e-a6c3df815380",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77be422b-a38b-4ff0-85cd-f134f412059f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44d8332c-ad40-435a-828e-a6c3df815380",
                    "LayerId": "e71ed81a-a1c0-4cdb-9f31-f00512b2388c"
                }
            ]
        },
        {
            "id": "ae21794d-61b2-4700-a432-f9aeb0ebcb49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06e32f42-a410-4dac-88ae-e73ef1cfca69",
            "compositeImage": {
                "id": "c8267b2a-dc2a-4e47-954f-0842322d659a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae21794d-61b2-4700-a432-f9aeb0ebcb49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e327fdb7-dd0f-467f-b247-e32cdcf444b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae21794d-61b2-4700-a432-f9aeb0ebcb49",
                    "LayerId": "e71ed81a-a1c0-4cdb-9f31-f00512b2388c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "e71ed81a-a1c0-4cdb-9f31-f00512b2388c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06e32f42-a410-4dac-88ae-e73ef1cfca69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}