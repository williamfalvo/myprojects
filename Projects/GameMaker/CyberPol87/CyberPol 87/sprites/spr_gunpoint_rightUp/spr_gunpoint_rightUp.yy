{
    "id": "0d6e00e9-32c7-4029-81c2-96eb10bb2c3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_rightUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 17,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30e093db-d7c4-4e50-83c1-0f0472da73e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d6e00e9-32c7-4029-81c2-96eb10bb2c3b",
            "compositeImage": {
                "id": "350c784d-529e-48f5-9f93-696c812521e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30e093db-d7c4-4e50-83c1-0f0472da73e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c01cfb5-33f0-4304-ac85-eeb2286298c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30e093db-d7c4-4e50-83c1-0f0472da73e5",
                    "LayerId": "23454ae8-1eb2-45f6-b00a-dab2c9008e42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "23454ae8-1eb2-45f6-b00a-dab2c9008e42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d6e00e9-32c7-4029-81c2-96eb10bb2c3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}