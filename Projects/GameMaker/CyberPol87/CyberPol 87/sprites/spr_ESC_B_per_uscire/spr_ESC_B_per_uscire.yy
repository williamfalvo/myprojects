{
    "id": "313bf810-2ee3-44a9-a7ad-2410e824b96e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ESC_B_per_uscire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 104,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6072170d-f122-48c7-8feb-353b677c5a22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "313bf810-2ee3-44a9-a7ad-2410e824b96e",
            "compositeImage": {
                "id": "55cbff44-36db-4196-a57b-ad9df11691c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6072170d-f122-48c7-8feb-353b677c5a22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6168ba3d-d3ff-44cd-ae1d-b46c2738dacc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6072170d-f122-48c7-8feb-353b677c5a22",
                    "LayerId": "bebe002c-dbd9-4213-b4e2-77cdea17fb07"
                }
            ]
        },
        {
            "id": "362b2d7f-86b8-4afb-87aa-ac67b4a29ffa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "313bf810-2ee3-44a9-a7ad-2410e824b96e",
            "compositeImage": {
                "id": "d4498958-db32-44be-b11e-9fc04951d6d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "362b2d7f-86b8-4afb-87aa-ac67b4a29ffa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9bfccb2-b6f2-476a-a4e3-8d797dae3edb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "362b2d7f-86b8-4afb-87aa-ac67b4a29ffa",
                    "LayerId": "bebe002c-dbd9-4213-b4e2-77cdea17fb07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "bebe002c-dbd9-4213-b4e2-77cdea17fb07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "313bf810-2ee3-44a9-a7ad-2410e824b96e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 105,
    "xorig": 52,
    "yorig": 13
}