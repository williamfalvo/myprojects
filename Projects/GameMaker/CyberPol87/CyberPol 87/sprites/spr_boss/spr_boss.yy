{
    "id": "6a927b9c-72c6-46e3-b592-77de612c6648",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 8,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f03546c1-4c24-4b0c-903d-6f2316e3f177",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a927b9c-72c6-46e3-b592-77de612c6648",
            "compositeImage": {
                "id": "99b26dbd-0875-45ea-8ab9-fec8638b4493",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f03546c1-4c24-4b0c-903d-6f2316e3f177",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ce46494-bd39-46d8-90f4-5e3d50f71c0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f03546c1-4c24-4b0c-903d-6f2316e3f177",
                    "LayerId": "06f7b9ba-86f5-4645-ab84-39567f4dafba"
                }
            ]
        },
        {
            "id": "f792303a-f10f-4116-9ab0-d1fc6309131c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a927b9c-72c6-46e3-b592-77de612c6648",
            "compositeImage": {
                "id": "f155899b-66b1-4cc7-9497-1704037a7ad5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f792303a-f10f-4116-9ab0-d1fc6309131c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03959536-bc07-456f-a71d-bb40857d3d6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f792303a-f10f-4116-9ab0-d1fc6309131c",
                    "LayerId": "06f7b9ba-86f5-4645-ab84-39567f4dafba"
                }
            ]
        },
        {
            "id": "aa0b21eb-3c7d-4cf7-acaf-ce285a78bb6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a927b9c-72c6-46e3-b592-77de612c6648",
            "compositeImage": {
                "id": "43b6eab6-4615-4b1e-a0b9-e354de405d6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa0b21eb-3c7d-4cf7-acaf-ce285a78bb6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dd69b92-04b5-4159-b0cc-0cb55a2a2014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa0b21eb-3c7d-4cf7-acaf-ce285a78bb6b",
                    "LayerId": "06f7b9ba-86f5-4645-ab84-39567f4dafba"
                }
            ]
        },
        {
            "id": "115f4654-37cb-4a09-8b6e-e8d13b65b9be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a927b9c-72c6-46e3-b592-77de612c6648",
            "compositeImage": {
                "id": "29d1be0a-672a-425f-8a35-c9bf70d3af0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "115f4654-37cb-4a09-8b6e-e8d13b65b9be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6cccace-337b-487b-b8ee-785e8d47efcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "115f4654-37cb-4a09-8b6e-e8d13b65b9be",
                    "LayerId": "06f7b9ba-86f5-4645-ab84-39567f4dafba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "06f7b9ba-86f5-4645-ab84-39567f4dafba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a927b9c-72c6-46e3-b592-77de612c6648",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}