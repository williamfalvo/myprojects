{
    "id": "c11a5731-e8e9-4011-8b4f-a56404b7d17c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_down_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71c3b43f-5043-4bd7-943a-404f368201a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11a5731-e8e9-4011-8b4f-a56404b7d17c",
            "compositeImage": {
                "id": "c33fbaa5-8489-4c44-ba86-eefc8231016a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71c3b43f-5043-4bd7-943a-404f368201a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c047181-216e-40be-b8c0-be2620db0a9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71c3b43f-5043-4bd7-943a-404f368201a1",
                    "LayerId": "edbbd64e-e912-4eea-a8a9-beab1dda3445"
                }
            ]
        },
        {
            "id": "99c578e7-e3ca-4117-b81d-e5f0a353ae8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11a5731-e8e9-4011-8b4f-a56404b7d17c",
            "compositeImage": {
                "id": "08e2bed2-b5ac-42d9-a0a2-5caa1e3664a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99c578e7-e3ca-4117-b81d-e5f0a353ae8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d96ccd69-c914-4781-a66d-359aa4658fd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99c578e7-e3ca-4117-b81d-e5f0a353ae8f",
                    "LayerId": "edbbd64e-e912-4eea-a8a9-beab1dda3445"
                }
            ]
        },
        {
            "id": "a1f7e0d6-d3b9-477a-9c76-ac6b32644b03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11a5731-e8e9-4011-8b4f-a56404b7d17c",
            "compositeImage": {
                "id": "945dd579-715d-48f1-82da-97730cd3d536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f7e0d6-d3b9-477a-9c76-ac6b32644b03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f56c7b2-64ce-45fc-a7b4-c862dc8cbe3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f7e0d6-d3b9-477a-9c76-ac6b32644b03",
                    "LayerId": "edbbd64e-e912-4eea-a8a9-beab1dda3445"
                }
            ]
        },
        {
            "id": "dd3723d8-4920-4881-8597-37f86463a155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11a5731-e8e9-4011-8b4f-a56404b7d17c",
            "compositeImage": {
                "id": "d7009f0d-aa64-4850-bc06-05058ad263fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd3723d8-4920-4881-8597-37f86463a155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65014bc0-b76b-44e5-8783-3b39843ba533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd3723d8-4920-4881-8597-37f86463a155",
                    "LayerId": "edbbd64e-e912-4eea-a8a9-beab1dda3445"
                }
            ]
        },
        {
            "id": "9cd04be8-8e77-4922-80a0-46c8a7360ca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11a5731-e8e9-4011-8b4f-a56404b7d17c",
            "compositeImage": {
                "id": "c52dbd30-cd42-4025-8d31-36b9c305b761",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd04be8-8e77-4922-80a0-46c8a7360ca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd40117-1839-4ec6-ad22-eee7671e1082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd04be8-8e77-4922-80a0-46c8a7360ca1",
                    "LayerId": "edbbd64e-e912-4eea-a8a9-beab1dda3445"
                }
            ]
        },
        {
            "id": "8be11cc7-c169-4d5a-b47c-40a0f26fc168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11a5731-e8e9-4011-8b4f-a56404b7d17c",
            "compositeImage": {
                "id": "fcbb2565-93a5-41e1-90a3-31be77a249e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8be11cc7-c169-4d5a-b47c-40a0f26fc168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f32b1392-24ea-40a2-bb79-f9d9279448df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8be11cc7-c169-4d5a-b47c-40a0f26fc168",
                    "LayerId": "edbbd64e-e912-4eea-a8a9-beab1dda3445"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "edbbd64e-e912-4eea-a8a9-beab1dda3445",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c11a5731-e8e9-4011-8b4f-a56404b7d17c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}