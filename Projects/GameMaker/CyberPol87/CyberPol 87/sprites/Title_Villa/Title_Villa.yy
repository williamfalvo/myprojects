{
    "id": "bbbd336f-48e8-4856-ba05-c995d70d7e3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Title_Villa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 7,
    "bbox_right": 95,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8da2f93b-38ca-4357-8500-5008c41e66b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbbd336f-48e8-4856-ba05-c995d70d7e3c",
            "compositeImage": {
                "id": "b8a58c4f-f76b-4638-9718-20528ed0d095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da2f93b-38ca-4357-8500-5008c41e66b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4cd6a84-c7ed-48ef-9bde-1e470dacd28b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da2f93b-38ca-4357-8500-5008c41e66b7",
                    "LayerId": "d2775e4f-f3e3-45df-ada3-96fa1716676c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "d2775e4f-f3e3-45df-ada3-96fa1716676c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbbd336f-48e8-4856-ba05-c995d70d7e3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 12
}