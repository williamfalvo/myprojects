{
    "id": "c585489d-ae4c-42b9-b8c9-203ace437e36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_big_upr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 52,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ad2a5c4-7523-4fb9-8120-dd614786aa90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c585489d-ae4c-42b9-b8c9-203ace437e36",
            "compositeImage": {
                "id": "1d3e909f-50d3-4e45-8482-267e8db7fca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ad2a5c4-7523-4fb9-8120-dd614786aa90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc6d76d2-cab8-4e9b-b949-55581991d6e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ad2a5c4-7523-4fb9-8120-dd614786aa90",
                    "LayerId": "a9b3d85e-e394-44c8-a671-371a6f8949a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a9b3d85e-e394-44c8-a671-371a6f8949a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c585489d-ae4c-42b9-b8c9-203ace437e36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}