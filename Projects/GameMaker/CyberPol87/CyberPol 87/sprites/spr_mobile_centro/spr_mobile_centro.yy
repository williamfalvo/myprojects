{
    "id": "756e84ae-300a-4aff-ad65-b4e9cd8e8f44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mobile_centro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f86ddc7b-2099-4d6d-851d-b13d42b954e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "756e84ae-300a-4aff-ad65-b4e9cd8e8f44",
            "compositeImage": {
                "id": "3fa632e3-b95e-4eec-8120-c338346cf685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f86ddc7b-2099-4d6d-851d-b13d42b954e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e9a8bd4-07a2-4dcf-9494-ea73c378686e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f86ddc7b-2099-4d6d-851d-b13d42b954e8",
                    "LayerId": "ba7a426e-4f12-4b40-b04b-0f5bef679658"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ba7a426e-4f12-4b40-b04b-0f5bef679658",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "756e84ae-300a-4aff-ad65-b4e9cd8e8f44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}