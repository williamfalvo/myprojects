{
    "id": "f6110fb2-94a0-4d38-bea7-9ed53bf415e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Mira_Stabile_Tast",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48ee0ed0-3387-4f50-9457-692397b35027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6110fb2-94a0-4d38-bea7-9ed53bf415e3",
            "compositeImage": {
                "id": "e1201754-1aa3-4ab7-b4fd-a198e83bfad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48ee0ed0-3387-4f50-9457-692397b35027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7f98b9-fa74-4310-ad7b-fd66e6e623a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48ee0ed0-3387-4f50-9457-692397b35027",
                    "LayerId": "0e015f26-c1cc-44c9-b53d-0de7443b92a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "0e015f26-c1cc-44c9-b53d-0de7443b92a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6110fb2-94a0-4d38-bea7-9ed53bf415e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}