{
    "id": "f143a3cf-6ef4-439e-b38a-60d303a0c2e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_credits_programming",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 224,
    "bbox_left": 93,
    "bbox_right": 544,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "707c5992-61ba-46a9-a171-4197fda456a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f143a3cf-6ef4-439e-b38a-60d303a0c2e8",
            "compositeImage": {
                "id": "c12c49a4-f9b6-4e77-9257-cdcf3f739bff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "707c5992-61ba-46a9-a171-4197fda456a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39d9c7fa-8547-4ce1-9836-3805cc690e8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "707c5992-61ba-46a9-a171-4197fda456a5",
                    "LayerId": "a74effd3-8c0e-4f4c-88f2-f7e2107c0cef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 310,
    "layers": [
        {
            "id": "a74effd3-8c0e-4f4c-88f2-f7e2107c0cef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f143a3cf-6ef4-439e-b38a-60d303a0c2e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 610,
    "xorig": 305,
    "yorig": 155
}