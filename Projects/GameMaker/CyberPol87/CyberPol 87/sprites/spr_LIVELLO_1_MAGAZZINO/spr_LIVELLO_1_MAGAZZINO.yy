{
    "id": "fc4fcbd6-917d-4ca7-ba8f-f9e7721182ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_LIVELLO_1_MAGAZZINO",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 15,
    "bbox_right": 202,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67979968-fc3f-4f90-9cdc-7fa96ecb2081",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc4fcbd6-917d-4ca7-ba8f-f9e7721182ee",
            "compositeImage": {
                "id": "ca4e583a-2d32-4a54-b181-70792dc74dce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67979968-fc3f-4f90-9cdc-7fa96ecb2081",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d41eb3f2-7e76-498d-abf8-278dc42e388a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67979968-fc3f-4f90-9cdc-7fa96ecb2081",
                    "LayerId": "8b188699-0358-44a5-8bfc-eb70f7256ae5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 57,
    "layers": [
        {
            "id": "8b188699-0358-44a5-8bfc-eb70f7256ae5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc4fcbd6-917d-4ca7-ba8f-f9e7721182ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 215,
    "xorig": 107,
    "yorig": 28
}