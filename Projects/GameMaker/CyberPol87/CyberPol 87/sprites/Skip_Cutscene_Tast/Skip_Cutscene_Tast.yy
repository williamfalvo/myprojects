{
    "id": "25230000-3618-4a33-a901-3f01629c2c3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Skip_Cutscene_Tast",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 5,
    "bbox_right": 146,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc7c98c9-f110-45e4-8f1e-beb921575822",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25230000-3618-4a33-a901-3f01629c2c3b",
            "compositeImage": {
                "id": "76dabe24-707c-4895-b504-dca8f919445c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc7c98c9-f110-45e4-8f1e-beb921575822",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f86c2b97-af2e-4bd3-8e19-c0cb2353fc85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc7c98c9-f110-45e4-8f1e-beb921575822",
                    "LayerId": "1511b894-7cd1-49b1-88a9-ccf684fd6fda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "1511b894-7cd1-49b1-88a9-ccf684fd6fda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25230000-3618-4a33-a901-3f01629c2c3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 75,
    "yorig": 36
}