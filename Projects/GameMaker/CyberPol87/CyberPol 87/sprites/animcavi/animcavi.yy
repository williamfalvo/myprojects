{
    "id": "b089c4df-8ae7-4c86-8232-4537367142bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "animcavi",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fdce6411-4bb0-4439-9b73-2e5b9a6ad6b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b089c4df-8ae7-4c86-8232-4537367142bd",
            "compositeImage": {
                "id": "55442c1a-a24a-4882-b9b9-492438e8c966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdce6411-4bb0-4439-9b73-2e5b9a6ad6b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64c5e331-d6ae-4cd1-92c1-5446f6af7067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdce6411-4bb0-4439-9b73-2e5b9a6ad6b1",
                    "LayerId": "5b0cdc35-6714-4f3d-88d0-4fd923d30e51"
                }
            ]
        },
        {
            "id": "d8282929-e30a-4583-85de-3fc6fef59b46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b089c4df-8ae7-4c86-8232-4537367142bd",
            "compositeImage": {
                "id": "330ae882-1007-457e-9eac-d0ec1db72177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8282929-e30a-4583-85de-3fc6fef59b46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc2e53d1-74e6-445b-a25e-9fee1199b918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8282929-e30a-4583-85de-3fc6fef59b46",
                    "LayerId": "5b0cdc35-6714-4f3d-88d0-4fd923d30e51"
                }
            ]
        },
        {
            "id": "360eca42-257b-44bd-841b-f75d8e04c380",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b089c4df-8ae7-4c86-8232-4537367142bd",
            "compositeImage": {
                "id": "3640b4ea-26c4-45fe-a20d-77342e53c52b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "360eca42-257b-44bd-841b-f75d8e04c380",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0da22beb-10a4-4c13-84cb-f5634174883c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "360eca42-257b-44bd-841b-f75d8e04c380",
                    "LayerId": "5b0cdc35-6714-4f3d-88d0-4fd923d30e51"
                }
            ]
        },
        {
            "id": "91c949be-6a6e-4f05-a592-1f6caa6d10b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b089c4df-8ae7-4c86-8232-4537367142bd",
            "compositeImage": {
                "id": "4f71df6c-1068-46b6-945b-2f006dd535e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c949be-6a6e-4f05-a592-1f6caa6d10b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a44f40d3-0b3a-4cbc-9d28-06165fdc1ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c949be-6a6e-4f05-a592-1f6caa6d10b2",
                    "LayerId": "5b0cdc35-6714-4f3d-88d0-4fd923d30e51"
                }
            ]
        },
        {
            "id": "9d375f67-adf5-4b21-9727-38297d74fc9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b089c4df-8ae7-4c86-8232-4537367142bd",
            "compositeImage": {
                "id": "0e55a05b-d8b9-487c-a386-66bc20ab12ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d375f67-adf5-4b21-9727-38297d74fc9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb405c8a-1fed-448f-815e-8f94fc698d95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d375f67-adf5-4b21-9727-38297d74fc9f",
                    "LayerId": "5b0cdc35-6714-4f3d-88d0-4fd923d30e51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5b0cdc35-6714-4f3d-88d0-4fd923d30e51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b089c4df-8ae7-4c86-8232-4537367142bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}