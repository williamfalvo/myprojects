{
    "id": "2f72dc92-a222-4f1f-8aea-0c223abe6ce1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_LIVELLO_2_FOGNE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 9,
    "bbox_right": 157,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b5397e7-d0a0-4f8a-9385-84d418ae79ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f72dc92-a222-4f1f-8aea-0c223abe6ce1",
            "compositeImage": {
                "id": "84dc0e10-a280-4c9c-b4c1-49e5f7152f4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b5397e7-d0a0-4f8a-9385-84d418ae79ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e66b993d-174a-43c9-a895-0eedbcddc6ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b5397e7-d0a0-4f8a-9385-84d418ae79ea",
                    "LayerId": "38235623-9bf1-446f-b96b-157a26cc7b35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "38235623-9bf1-446f-b96b-157a26cc7b35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f72dc92-a222-4f1f-8aea-0c223abe6ce1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 167,
    "xorig": 83,
    "yorig": 23
}