{
    "id": "5db67731-6261-4c07-8f17-3a9fa6017ad5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cavi",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 32,
    "bbox_right": 383,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93e4c586-7e6e-4616-892f-fd0c5a7c3f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5db67731-6261-4c07-8f17-3a9fa6017ad5",
            "compositeImage": {
                "id": "e1572542-3736-43f0-adf9-1a60d7389445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e4c586-7e6e-4616-892f-fd0c5a7c3f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67984347-d04f-4646-acd2-8f8d42830eef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e4c586-7e6e-4616-892f-fd0c5a7c3f8b",
                    "LayerId": "be426bef-06b5-4a0a-a610-cbd44db42f18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "be426bef-06b5-4a0a-a610-cbd44db42f18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5db67731-6261-4c07-8f17-3a9fa6017ad5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}