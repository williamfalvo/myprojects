{
    "id": "fc738a8b-8412-4399-8cb4-40345e63c849",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite156",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2557a693-bb07-46fd-b056-8c496e00bc46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc738a8b-8412-4399-8cb4-40345e63c849",
            "compositeImage": {
                "id": "221f7112-e7ea-41ca-bcd4-46abdd2225c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2557a693-bb07-46fd-b056-8c496e00bc46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad163713-8878-43b9-984c-aeb15ad42472",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2557a693-bb07-46fd-b056-8c496e00bc46",
                    "LayerId": "bd9ec2e6-5c78-4bda-a0b2-60c59ecef5bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bd9ec2e6-5c78-4bda-a0b2-60c59ecef5bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc738a8b-8412-4399-8cb4-40345e63c849",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}