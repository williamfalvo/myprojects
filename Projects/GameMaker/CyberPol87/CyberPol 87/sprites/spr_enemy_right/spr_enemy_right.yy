{
    "id": "e7e62ced-d5d5-47e0-8102-323241ef8b6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 45,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b76a65c-e053-4b2f-af3a-a3ed60c7d139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7e62ced-d5d5-47e0-8102-323241ef8b6e",
            "compositeImage": {
                "id": "7d180204-cb48-4209-80b4-c8e6f9ea0be1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b76a65c-e053-4b2f-af3a-a3ed60c7d139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24e9b4cc-366a-4d89-82eb-9e4d0d72c60a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b76a65c-e053-4b2f-af3a-a3ed60c7d139",
                    "LayerId": "c49c3fa9-f8dc-427d-9f5f-4ec5d96ae6d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c49c3fa9-f8dc-427d-9f5f-4ec5d96ae6d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7e62ced-d5d5-47e0-8102-323241ef8b6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}