{
    "id": "42bcbdff-1df7-42c9-ba5a-583712149d59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 17,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c424d12-bdd2-4ce2-9bee-3d2dd06ada55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42bcbdff-1df7-42c9-ba5a-583712149d59",
            "compositeImage": {
                "id": "e45bc96a-a939-4388-a987-d32614e8a474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c424d12-bdd2-4ce2-9bee-3d2dd06ada55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b82311dc-fa90-4922-bb35-54b2bbe336e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c424d12-bdd2-4ce2-9bee-3d2dd06ada55",
                    "LayerId": "46354a5b-774e-488a-b871-12d19d897444"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "46354a5b-774e-488a-b871-12d19d897444",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42bcbdff-1df7-42c9-ba5a-583712149d59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}