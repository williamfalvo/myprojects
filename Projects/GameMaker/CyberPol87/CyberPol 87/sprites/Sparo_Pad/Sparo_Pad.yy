{
    "id": "ad778e5a-2b20-4d43-95c4-e0e30ccd6d9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Sparo_Pad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0990358c-cbad-4e65-8443-dd24608455eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad778e5a-2b20-4d43-95c4-e0e30ccd6d9b",
            "compositeImage": {
                "id": "d869be79-fc8c-41eb-a422-f1b934493b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0990358c-cbad-4e65-8443-dd24608455eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "792f2930-7afa-4a6f-855b-3a5290abf80e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0990358c-cbad-4e65-8443-dd24608455eb",
                    "LayerId": "06c629b6-11f9-490c-bd45-c8743b410191"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "06c629b6-11f9-490c-bd45-c8743b410191",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad778e5a-2b20-4d43-95c4-e0e30ccd6d9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}