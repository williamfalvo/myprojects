{
    "id": "8e8ea55f-ed83-418a-93a6-8610137a0960",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_x_txt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87c388f4-935e-439b-b0ef-99ceff4a871e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e8ea55f-ed83-418a-93a6-8610137a0960",
            "compositeImage": {
                "id": "c77e6e93-c86a-47bb-8dde-7441271f4fa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87c388f4-935e-439b-b0ef-99ceff4a871e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "489a1718-1b1c-4900-bed0-48a43d0b5922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87c388f4-935e-439b-b0ef-99ceff4a871e",
                    "LayerId": "7988d54b-8681-4c43-a01a-cc8f6df0d3f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7988d54b-8681-4c43-a01a-cc8f6df0d3f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e8ea55f-ed83-418a-93a6-8610137a0960",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}