{
    "id": "0058f228-2b93-48a8-acf1-15c670a1eda6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Testo_Villa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 8,
    "bbox_right": 598,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4ad9e12-9808-416c-8140-409a59a9ac27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0058f228-2b93-48a8-acf1-15c670a1eda6",
            "compositeImage": {
                "id": "f2d8468c-a6d4-4bab-86f8-bd22ad9e1f4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4ad9e12-9808-416c-8140-409a59a9ac27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a469acd3-7594-4fa9-83a9-fecfc2b22839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4ad9e12-9808-416c-8140-409a59a9ac27",
                    "LayerId": "c910a22b-7cc7-40d8-978a-c419ff261e31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "c910a22b-7cc7-40d8-978a-c419ff261e31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0058f228-2b93-48a8-acf1-15c670a1eda6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 610,
    "xorig": 305,
    "yorig": 55
}