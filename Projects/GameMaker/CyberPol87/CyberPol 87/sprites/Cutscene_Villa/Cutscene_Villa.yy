{
    "id": "de810795-724d-4c79-9811-2f59a80b941a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Cutscene_Villa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 228,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "388f1c17-1128-45c1-af70-cec5c122afab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de810795-724d-4c79-9811-2f59a80b941a",
            "compositeImage": {
                "id": "f61c770a-bafb-432b-b85d-c601f9e8b9da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "388f1c17-1128-45c1-af70-cec5c122afab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f4836b5-3b9e-45fc-8152-35bb65dbe40f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388f1c17-1128-45c1-af70-cec5c122afab",
                    "LayerId": "dfa55ef0-09af-41eb-9b54-3dacfd4144b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "dfa55ef0-09af-41eb-9b54-3dacfd4144b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de810795-724d-4c79-9811-2f59a80b941a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 180
}