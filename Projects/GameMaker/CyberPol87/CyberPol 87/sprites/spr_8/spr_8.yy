{
    "id": "1dd6a0c6-78de-46d6-aac9-21b4f0e08fb5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58d4e885-e55c-4076-b682-88a71c7012e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dd6a0c6-78de-46d6-aac9-21b4f0e08fb5",
            "compositeImage": {
                "id": "302beb69-956c-4166-9e8d-af5edf9420c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58d4e885-e55c-4076-b682-88a71c7012e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99d16ea9-6a7c-42d7-837e-932980bd18d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58d4e885-e55c-4076-b682-88a71c7012e3",
                    "LayerId": "aca6983f-e979-4537-918b-e5cacf220a84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "aca6983f-e979-4537-918b-e5cacf220a84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1dd6a0c6-78de-46d6-aac9-21b4f0e08fb5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 9
}