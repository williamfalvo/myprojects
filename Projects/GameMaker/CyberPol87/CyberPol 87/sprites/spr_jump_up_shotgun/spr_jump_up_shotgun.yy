{
    "id": "2d2e59ba-adb5-4919-8437-790287e62f9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_up_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72d52475-d89b-4d28-a3c2-adcb3090d362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d2e59ba-adb5-4919-8437-790287e62f9c",
            "compositeImage": {
                "id": "def192a8-0238-407e-b92c-0c91496585a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72d52475-d89b-4d28-a3c2-adcb3090d362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38ff2228-7484-4a0d-b08e-30a8691c1317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72d52475-d89b-4d28-a3c2-adcb3090d362",
                    "LayerId": "5692e9b2-d910-4d16-aca9-a7bfc9b5e85e"
                }
            ]
        },
        {
            "id": "3eaac5bd-c279-4c48-a5ab-f371c1760176",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d2e59ba-adb5-4919-8437-790287e62f9c",
            "compositeImage": {
                "id": "4dbb2b5e-048b-42dc-8912-9f39bdedfa79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eaac5bd-c279-4c48-a5ab-f371c1760176",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df21f28d-74b9-43d4-9649-9a0c54f1fe2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eaac5bd-c279-4c48-a5ab-f371c1760176",
                    "LayerId": "5692e9b2-d910-4d16-aca9-a7bfc9b5e85e"
                }
            ]
        },
        {
            "id": "70917332-3d9a-46a6-9134-1b4079549bd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d2e59ba-adb5-4919-8437-790287e62f9c",
            "compositeImage": {
                "id": "3e82e49e-6a9d-43af-abe4-00e816a72f34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70917332-3d9a-46a6-9134-1b4079549bd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59cd6d43-917a-40c2-ae8a-b5714dcb162a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70917332-3d9a-46a6-9134-1b4079549bd6",
                    "LayerId": "5692e9b2-d910-4d16-aca9-a7bfc9b5e85e"
                }
            ]
        },
        {
            "id": "77c26cb3-f3c0-40c8-bfd8-ff4b1c26f4a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d2e59ba-adb5-4919-8437-790287e62f9c",
            "compositeImage": {
                "id": "27e42974-c2d7-4870-a7ad-f09ac332c589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77c26cb3-f3c0-40c8-bfd8-ff4b1c26f4a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c72756e5-2a3a-4385-84cc-3ddcc689a9ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77c26cb3-f3c0-40c8-bfd8-ff4b1c26f4a9",
                    "LayerId": "5692e9b2-d910-4d16-aca9-a7bfc9b5e85e"
                }
            ]
        },
        {
            "id": "dd5dd996-555f-46ba-b9ef-c9cfe1245582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d2e59ba-adb5-4919-8437-790287e62f9c",
            "compositeImage": {
                "id": "269bb671-af47-4322-a5f0-719cd787e39c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd5dd996-555f-46ba-b9ef-c9cfe1245582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dc99cea-84e5-43b3-bf52-3a25cd42d21d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd5dd996-555f-46ba-b9ef-c9cfe1245582",
                    "LayerId": "5692e9b2-d910-4d16-aca9-a7bfc9b5e85e"
                }
            ]
        },
        {
            "id": "23776993-51f1-4c22-ab9e-04d99ad4be1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d2e59ba-adb5-4919-8437-790287e62f9c",
            "compositeImage": {
                "id": "4d7551ce-e6ec-4b76-b2c8-5527c84fff47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23776993-51f1-4c22-ab9e-04d99ad4be1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a8479ca-4fa6-4fb1-81eb-7911ef84488b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23776993-51f1-4c22-ab9e-04d99ad4be1b",
                    "LayerId": "5692e9b2-d910-4d16-aca9-a7bfc9b5e85e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "5692e9b2-d910-4d16-aca9-a7bfc9b5e85e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d2e59ba-adb5-4919-8437-790287e62f9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}