{
    "id": "46d8261b-5adf-48f8-9b53-1fc7f52a4ac9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lampadina",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 10,
    "bbox_right": 21,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3b627a0-7a1f-456a-a359-d729b9a79074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46d8261b-5adf-48f8-9b53-1fc7f52a4ac9",
            "compositeImage": {
                "id": "d5b3dff9-8439-468f-9825-40a7c4a8dff6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3b627a0-7a1f-456a-a359-d729b9a79074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e3aefbd-4d59-4cf2-b692-5f794d92a486",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3b627a0-7a1f-456a-a359-d729b9a79074",
                    "LayerId": "b6c3d4cf-b39c-417e-b93a-b3bc1a5afbbb"
                }
            ]
        },
        {
            "id": "ccc9b0ba-1f32-4518-b578-6a9a4e21255c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46d8261b-5adf-48f8-9b53-1fc7f52a4ac9",
            "compositeImage": {
                "id": "0015e41a-167e-42a3-bcbb-238f29b28d06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccc9b0ba-1f32-4518-b578-6a9a4e21255c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad134166-b15a-41ce-ad5b-0342ba7a55b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccc9b0ba-1f32-4518-b578-6a9a4e21255c",
                    "LayerId": "b6c3d4cf-b39c-417e-b93a-b3bc1a5afbbb"
                }
            ]
        },
        {
            "id": "1d17a6df-8b16-4531-9f49-c51738624e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46d8261b-5adf-48f8-9b53-1fc7f52a4ac9",
            "compositeImage": {
                "id": "6205dc62-4db5-460f-8600-5966151c5110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d17a6df-8b16-4531-9f49-c51738624e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "def4987b-f3bc-44d8-a709-6ba782b3b7d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d17a6df-8b16-4531-9f49-c51738624e0d",
                    "LayerId": "b6c3d4cf-b39c-417e-b93a-b3bc1a5afbbb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b6c3d4cf-b39c-417e-b93a-b3bc1a5afbbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46d8261b-5adf-48f8-9b53-1fc7f52a4ac9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}