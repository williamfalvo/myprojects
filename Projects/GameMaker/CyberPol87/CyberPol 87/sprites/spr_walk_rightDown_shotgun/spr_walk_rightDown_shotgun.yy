{
    "id": "94128c04-2762-4291-8ae2-7c6f674e6285",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_rightDown_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bd9f779-32ae-419c-8d7a-2a5c704e8a3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94128c04-2762-4291-8ae2-7c6f674e6285",
            "compositeImage": {
                "id": "5226174d-90c9-4f20-99cf-738c99e40af0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bd9f779-32ae-419c-8d7a-2a5c704e8a3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "865e417f-e680-424e-a6a8-eadb4216cc87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bd9f779-32ae-419c-8d7a-2a5c704e8a3f",
                    "LayerId": "4e137ea9-bdad-456e-a7a2-f91b6784853d"
                }
            ]
        },
        {
            "id": "b7c80bd7-784c-42f4-a7d3-5d35d2867d8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94128c04-2762-4291-8ae2-7c6f674e6285",
            "compositeImage": {
                "id": "307b24cd-70c9-465e-b7ec-7421ee6ec28f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7c80bd7-784c-42f4-a7d3-5d35d2867d8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64af4fe3-7844-4bab-95f9-17a9dc6ca9ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7c80bd7-784c-42f4-a7d3-5d35d2867d8a",
                    "LayerId": "4e137ea9-bdad-456e-a7a2-f91b6784853d"
                }
            ]
        },
        {
            "id": "9a758fca-3d19-4b3d-938c-20dc12deb32d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94128c04-2762-4291-8ae2-7c6f674e6285",
            "compositeImage": {
                "id": "6633f72b-a01b-4090-abc0-33742c510477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a758fca-3d19-4b3d-938c-20dc12deb32d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8a05d38-344d-4f4a-9083-f2ebcc27c8dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a758fca-3d19-4b3d-938c-20dc12deb32d",
                    "LayerId": "4e137ea9-bdad-456e-a7a2-f91b6784853d"
                }
            ]
        },
        {
            "id": "7471e054-9955-468c-88a0-782d9a32d5c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94128c04-2762-4291-8ae2-7c6f674e6285",
            "compositeImage": {
                "id": "bb4ec0d7-b957-46f5-a6d3-5a522b5f1ad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7471e054-9955-468c-88a0-782d9a32d5c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ee59ef1-7469-4de4-8c65-3f2918840fc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7471e054-9955-468c-88a0-782d9a32d5c1",
                    "LayerId": "4e137ea9-bdad-456e-a7a2-f91b6784853d"
                }
            ]
        },
        {
            "id": "eb98e76e-95a6-48bf-b132-05a8b3765d90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94128c04-2762-4291-8ae2-7c6f674e6285",
            "compositeImage": {
                "id": "1896cc04-c223-47e4-80e1-0bbf0de3053e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb98e76e-95a6-48bf-b132-05a8b3765d90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9a40a91-d3f5-40e4-8660-c29400c721ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb98e76e-95a6-48bf-b132-05a8b3765d90",
                    "LayerId": "4e137ea9-bdad-456e-a7a2-f91b6784853d"
                }
            ]
        },
        {
            "id": "269d30f7-34f4-4926-a8c4-0bea7ba116ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94128c04-2762-4291-8ae2-7c6f674e6285",
            "compositeImage": {
                "id": "610b5954-e306-4d32-9e19-801784f2d312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "269d30f7-34f4-4926-a8c4-0bea7ba116ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddcd630f-531e-4aea-ac07-f8785d650308",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "269d30f7-34f4-4926-a8c4-0bea7ba116ef",
                    "LayerId": "4e137ea9-bdad-456e-a7a2-f91b6784853d"
                }
            ]
        },
        {
            "id": "78114d7a-4b82-4a0e-9c5b-e4cc9721e631",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94128c04-2762-4291-8ae2-7c6f674e6285",
            "compositeImage": {
                "id": "ab8d1372-092b-41a8-8513-eb41e9971a7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78114d7a-4b82-4a0e-9c5b-e4cc9721e631",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c51284b8-4d85-4d71-84b6-7a2b420abc7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78114d7a-4b82-4a0e-9c5b-e4cc9721e631",
                    "LayerId": "4e137ea9-bdad-456e-a7a2-f91b6784853d"
                }
            ]
        },
        {
            "id": "0d34c7d9-e327-4c08-bc17-9cc2171ada8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94128c04-2762-4291-8ae2-7c6f674e6285",
            "compositeImage": {
                "id": "92e79efb-7320-4c22-a1d2-925cb8bb252a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d34c7d9-e327-4c08-bc17-9cc2171ada8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f7a9807-93f0-4c88-be98-f16ced03ae13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d34c7d9-e327-4c08-bc17-9cc2171ada8c",
                    "LayerId": "4e137ea9-bdad-456e-a7a2-f91b6784853d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "4e137ea9-bdad-456e-a7a2-f91b6784853d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94128c04-2762-4291-8ae2-7c6f674e6285",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}