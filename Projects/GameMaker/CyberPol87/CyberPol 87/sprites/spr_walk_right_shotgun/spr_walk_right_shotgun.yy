{
    "id": "9c701fc9-d98b-4e10-af6f-f197462ddd18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_right_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 15,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f2dc9ea-b7d8-4a20-9494-9b84f87ee518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c701fc9-d98b-4e10-af6f-f197462ddd18",
            "compositeImage": {
                "id": "cd28be2e-8314-4130-8594-b0ce18242c08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f2dc9ea-b7d8-4a20-9494-9b84f87ee518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "350b108d-c30f-40c8-aefd-8790d3ae7721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f2dc9ea-b7d8-4a20-9494-9b84f87ee518",
                    "LayerId": "add29f6f-d4af-4b28-83e5-681f156cf2ae"
                }
            ]
        },
        {
            "id": "088834d4-60cb-449d-bde1-b3a5f450f6f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c701fc9-d98b-4e10-af6f-f197462ddd18",
            "compositeImage": {
                "id": "2038be7d-942d-4688-9d97-15beaa7f85a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "088834d4-60cb-449d-bde1-b3a5f450f6f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcad5b64-e1b3-4e87-9e89-441712d719a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "088834d4-60cb-449d-bde1-b3a5f450f6f2",
                    "LayerId": "add29f6f-d4af-4b28-83e5-681f156cf2ae"
                }
            ]
        },
        {
            "id": "42bddb7c-5497-4c03-87a3-18c354b29f55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c701fc9-d98b-4e10-af6f-f197462ddd18",
            "compositeImage": {
                "id": "5bdc9b2b-24dd-47c0-85d1-375577856532",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42bddb7c-5497-4c03-87a3-18c354b29f55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7950d616-6927-4f66-a288-c3e6b1d729fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42bddb7c-5497-4c03-87a3-18c354b29f55",
                    "LayerId": "add29f6f-d4af-4b28-83e5-681f156cf2ae"
                }
            ]
        },
        {
            "id": "559303dc-9d70-4f8f-8860-ea23f0cb537c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c701fc9-d98b-4e10-af6f-f197462ddd18",
            "compositeImage": {
                "id": "431075a5-59d7-4232-8811-24706487e132",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "559303dc-9d70-4f8f-8860-ea23f0cb537c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80923101-9ad7-4ecc-bdd3-ecb4eef1b7c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "559303dc-9d70-4f8f-8860-ea23f0cb537c",
                    "LayerId": "add29f6f-d4af-4b28-83e5-681f156cf2ae"
                }
            ]
        },
        {
            "id": "66a90288-02fa-46dd-968b-25efb83d93b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c701fc9-d98b-4e10-af6f-f197462ddd18",
            "compositeImage": {
                "id": "e43cfd93-3e4a-431f-99a8-345e39afb1ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66a90288-02fa-46dd-968b-25efb83d93b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38ee7f03-7d90-4b67-93d6-0f215a60f932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66a90288-02fa-46dd-968b-25efb83d93b0",
                    "LayerId": "add29f6f-d4af-4b28-83e5-681f156cf2ae"
                }
            ]
        },
        {
            "id": "03f5663f-3886-4046-9960-c76523179066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c701fc9-d98b-4e10-af6f-f197462ddd18",
            "compositeImage": {
                "id": "0d525a62-d892-42f6-bc77-4a13338f6aff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03f5663f-3886-4046-9960-c76523179066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71fe09d3-ff7f-42d1-a4eb-ad559ceb1826",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f5663f-3886-4046-9960-c76523179066",
                    "LayerId": "add29f6f-d4af-4b28-83e5-681f156cf2ae"
                }
            ]
        },
        {
            "id": "7452d18a-7f5a-40f7-9c67-d05350357438",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c701fc9-d98b-4e10-af6f-f197462ddd18",
            "compositeImage": {
                "id": "52bcbe0e-a679-4014-bab5-80e98e842f91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7452d18a-7f5a-40f7-9c67-d05350357438",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "600cd790-310c-42e5-b87c-0fedad11d44a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7452d18a-7f5a-40f7-9c67-d05350357438",
                    "LayerId": "add29f6f-d4af-4b28-83e5-681f156cf2ae"
                }
            ]
        },
        {
            "id": "ee6af56b-acad-4875-b9b9-5685b910f16a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c701fc9-d98b-4e10-af6f-f197462ddd18",
            "compositeImage": {
                "id": "5c547456-c1ac-4749-9728-97ef3213b6a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee6af56b-acad-4875-b9b9-5685b910f16a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16eaaa7b-2e14-45b8-a9d6-14f800f44260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee6af56b-acad-4875-b9b9-5685b910f16a",
                    "LayerId": "add29f6f-d4af-4b28-83e5-681f156cf2ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "add29f6f-d4af-4b28-83e5-681f156cf2ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c701fc9-d98b-4e10-af6f-f197462ddd18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}