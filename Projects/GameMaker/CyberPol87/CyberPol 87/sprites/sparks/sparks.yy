{
    "id": "0ab1e5ac-6b96-4f5a-b827-594889904284",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sparks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9088f811-d6d9-41d0-bf23-2c52f6b298a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "7c3bcfb5-a57c-491d-ae0b-7698ef85c0a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9088f811-d6d9-41d0-bf23-2c52f6b298a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeaf6022-3e96-4e46-ae36-38f8555a667e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9088f811-d6d9-41d0-bf23-2c52f6b298a7",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "3d333806-8988-479c-95cc-a46045338d3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "32e74347-36ef-4069-8d8b-034f04c71482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d333806-8988-479c-95cc-a46045338d3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdd69821-e8d4-4311-95cf-f1c994994921",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d333806-8988-479c-95cc-a46045338d3c",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "678ecfde-65e0-4c02-bce9-8463b36b0c40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "bcaa532a-960e-4f2b-8e52-7969d700603f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "678ecfde-65e0-4c02-bce9-8463b36b0c40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99d3949c-1e19-42be-ace3-4d4d53c3eac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "678ecfde-65e0-4c02-bce9-8463b36b0c40",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "27bbdf3c-c44c-44c0-b950-c10f5fdefc34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "623b214d-8a5b-45df-ab09-b01c01b1e4c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27bbdf3c-c44c-44c0-b950-c10f5fdefc34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1d933f0-5e37-4a9f-824d-c1dc17d39c34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27bbdf3c-c44c-44c0-b950-c10f5fdefc34",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "0fba015a-51cc-4d86-801e-efd92c063d8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "af2c581c-4599-402d-8f4b-3c5a43d0c4e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fba015a-51cc-4d86-801e-efd92c063d8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98c963ab-148e-40ba-8631-dc843b356f48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fba015a-51cc-4d86-801e-efd92c063d8d",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "6f05e606-ee99-47ec-84d3-209b24c15f49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "d42cd8e5-abb4-4cdb-be47-b09a3184a092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f05e606-ee99-47ec-84d3-209b24c15f49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa7e7fdf-89c6-4f18-ae26-009f86a1b762",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f05e606-ee99-47ec-84d3-209b24c15f49",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "f8c221fd-a0c1-4beb-a6a3-580ab63f7b5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "93ad92db-c688-4d87-96c3-ca1fdb729996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8c221fd-a0c1-4beb-a6a3-580ab63f7b5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ef6fb44-d3ff-435c-b485-f81d66045f3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8c221fd-a0c1-4beb-a6a3-580ab63f7b5f",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "bad31930-4c39-4021-89be-837b955255ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "5ae3e15d-922e-41e0-a428-2d236adc905b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bad31930-4c39-4021-89be-837b955255ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7103a095-e200-46ff-a040-5814873c597f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bad31930-4c39-4021-89be-837b955255ec",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "2cdf7213-2540-405d-afae-7138286454ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "4823dc5b-d743-4a58-b3f4-9186337761ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cdf7213-2540-405d-afae-7138286454ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "073cbf90-ba98-4941-939c-049908fa7412",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cdf7213-2540-405d-afae-7138286454ed",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "068fef08-e0ce-4c19-8eab-8953fe4e72f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "4656790f-67af-4c61-acb7-7ba90d33ad3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "068fef08-e0ce-4c19-8eab-8953fe4e72f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f655d296-c2f0-4162-8e61-f255bd24b06a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "068fef08-e0ce-4c19-8eab-8953fe4e72f9",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "e5baa8c1-acac-4b66-8667-1e2375f8d351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "6d78845d-1b18-4721-93fd-2d647cd4614a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5baa8c1-acac-4b66-8667-1e2375f8d351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5842d6b0-c8b2-47f5-b970-a563a7248c40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5baa8c1-acac-4b66-8667-1e2375f8d351",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "43d0e575-262f-494a-af0d-d2f9c4b96741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "24325e1f-baed-4e44-a341-0f2a7c0e8b6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43d0e575-262f-494a-af0d-d2f9c4b96741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87666173-df49-491e-8dd9-b87d5dec46a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43d0e575-262f-494a-af0d-d2f9c4b96741",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "457ec7b2-8d2f-4094-8b03-38e32d58675b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "2c697e7e-a1ec-4742-9a7a-91f4d810272a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "457ec7b2-8d2f-4094-8b03-38e32d58675b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccdedb66-346e-4173-b229-053acbe420ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "457ec7b2-8d2f-4094-8b03-38e32d58675b",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "5b9e39d0-b45a-44ad-8fe1-dc9fd834466e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "fcb56bda-5f81-4700-92ff-380114246bcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b9e39d0-b45a-44ad-8fe1-dc9fd834466e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f07ae1-c426-4efc-8613-9e7a4f25502f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b9e39d0-b45a-44ad-8fe1-dc9fd834466e",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "4c0f1d54-a58f-42e4-b2c4-b493aa6917d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "845c5d40-8da5-4d51-9ef3-a551388c9edc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c0f1d54-a58f-42e4-b2c4-b493aa6917d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32904bbb-aa70-459b-9752-352b127205c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c0f1d54-a58f-42e4-b2c4-b493aa6917d9",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        },
        {
            "id": "796f09b5-836b-43ee-99a4-2b09991a95a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "compositeImage": {
                "id": "305e5ad7-d414-4b1a-8d82-09eccdd657a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "796f09b5-836b-43ee-99a4-2b09991a95a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d0e1e50-ecd5-46a6-912b-db413c24ff88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "796f09b5-836b-43ee-99a4-2b09991a95a4",
                    "LayerId": "c7d5ba55-4cb4-475f-857e-6963ac695540"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c7d5ba55-4cb4-475f-857e-6963ac695540",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ab1e5ac-6b96-4f5a-b827-594889904284",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}