{
    "id": "cdfc45da-432a-49f0-a342-39dc0778c209",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ESC_B_indietro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 153,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b49f4b7-3592-4244-b742-93a4ce6aac3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cdfc45da-432a-49f0-a342-39dc0778c209",
            "compositeImage": {
                "id": "f3fb25d3-d842-484d-8923-08f1dc830399",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b49f4b7-3592-4244-b742-93a4ce6aac3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc0fb96-2aca-4beb-b37e-87f4be532084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b49f4b7-3592-4244-b742-93a4ce6aac3a",
                    "LayerId": "68a14c93-605d-4dad-b54d-098bffdec327"
                }
            ]
        },
        {
            "id": "63bf6342-f5f4-41db-986c-f11089f21b15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cdfc45da-432a-49f0-a342-39dc0778c209",
            "compositeImage": {
                "id": "6414a415-3340-4461-9650-9f327612e7b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63bf6342-f5f4-41db-986c-f11089f21b15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14351ea4-68e0-46db-959d-48bfd8f6c2e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63bf6342-f5f4-41db-986c-f11089f21b15",
                    "LayerId": "68a14c93-605d-4dad-b54d-098bffdec327"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "68a14c93-605d-4dad-b54d-098bffdec327",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cdfc45da-432a-49f0-a342-39dc0778c209",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 154,
    "xorig": 77,
    "yorig": 13
}