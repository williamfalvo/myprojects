{
    "id": "dfc74252-81aa-4b7a-9d02-907ccfa4b625",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Movimento_Tast",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a0ec120-ab55-4a36-985b-2ec9a47ffe66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc74252-81aa-4b7a-9d02-907ccfa4b625",
            "compositeImage": {
                "id": "b5d5b52a-6a5c-4bc2-99d4-3249a3dc0a97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a0ec120-ab55-4a36-985b-2ec9a47ffe66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ebb6e7d-f6ea-437a-a8b6-77842cbcbbf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a0ec120-ab55-4a36-985b-2ec9a47ffe66",
                    "LayerId": "f3e29197-5c6f-4b07-81e9-5b741c2b8711"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "f3e29197-5c6f-4b07-81e9-5b741c2b8711",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfc74252-81aa-4b7a-9d02-907ccfa4b625",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}