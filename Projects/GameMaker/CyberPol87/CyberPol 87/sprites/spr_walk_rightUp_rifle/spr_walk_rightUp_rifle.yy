{
    "id": "8e06e12b-73c9-441b-8b34-23b8bf9761bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_rightUp_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 15,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0877209-e7bd-4d08-bc34-1a4ffb1e756f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e06e12b-73c9-441b-8b34-23b8bf9761bd",
            "compositeImage": {
                "id": "2e5dbedb-92df-45fe-bbdb-f34cc6db1720",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0877209-e7bd-4d08-bc34-1a4ffb1e756f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55ca25e5-3e63-4501-829c-0d2beb7ee1c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0877209-e7bd-4d08-bc34-1a4ffb1e756f",
                    "LayerId": "28a95af6-ec41-4620-9c00-6592dd3394b6"
                }
            ]
        },
        {
            "id": "cb2331f0-bf47-4208-b95c-bd58861962a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e06e12b-73c9-441b-8b34-23b8bf9761bd",
            "compositeImage": {
                "id": "206fb36b-facc-4aee-a8bd-a058fe0392cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb2331f0-bf47-4208-b95c-bd58861962a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81897d6b-13f8-4e1e-bc85-c7e6319aed66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb2331f0-bf47-4208-b95c-bd58861962a1",
                    "LayerId": "28a95af6-ec41-4620-9c00-6592dd3394b6"
                }
            ]
        },
        {
            "id": "d7090165-cc53-4385-b86e-da29e71c2823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e06e12b-73c9-441b-8b34-23b8bf9761bd",
            "compositeImage": {
                "id": "60f3f0b4-69fb-40a9-acc7-064f70450d8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7090165-cc53-4385-b86e-da29e71c2823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41558b3f-392a-496f-9e04-acfc2b237c27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7090165-cc53-4385-b86e-da29e71c2823",
                    "LayerId": "28a95af6-ec41-4620-9c00-6592dd3394b6"
                }
            ]
        },
        {
            "id": "ec449b83-0d91-43ae-8b38-362bbfed6da0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e06e12b-73c9-441b-8b34-23b8bf9761bd",
            "compositeImage": {
                "id": "997e5194-97aa-4ccf-9ca1-15dd624d1b18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec449b83-0d91-43ae-8b38-362bbfed6da0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bfa1ae4-84da-4027-851e-632f0714d477",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec449b83-0d91-43ae-8b38-362bbfed6da0",
                    "LayerId": "28a95af6-ec41-4620-9c00-6592dd3394b6"
                }
            ]
        },
        {
            "id": "7c9982e3-1b04-44e1-b78f-dc7dc77ab910",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e06e12b-73c9-441b-8b34-23b8bf9761bd",
            "compositeImage": {
                "id": "40a6c2c2-1bdd-4002-a8db-979dcb19c31a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c9982e3-1b04-44e1-b78f-dc7dc77ab910",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1521d375-f0e2-4901-a6aa-0d20ccf175c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c9982e3-1b04-44e1-b78f-dc7dc77ab910",
                    "LayerId": "28a95af6-ec41-4620-9c00-6592dd3394b6"
                }
            ]
        },
        {
            "id": "b7c52ec1-d5de-419c-94c0-85ebc0bcf0ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e06e12b-73c9-441b-8b34-23b8bf9761bd",
            "compositeImage": {
                "id": "15d828ff-3308-4fc4-b3bd-41ad193101ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7c52ec1-d5de-419c-94c0-85ebc0bcf0ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c39eb51-c7a8-43b6-a02b-afee4ce56b68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7c52ec1-d5de-419c-94c0-85ebc0bcf0ca",
                    "LayerId": "28a95af6-ec41-4620-9c00-6592dd3394b6"
                }
            ]
        },
        {
            "id": "7e923060-25b3-4c2d-be0f-495cd4c6e439",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e06e12b-73c9-441b-8b34-23b8bf9761bd",
            "compositeImage": {
                "id": "7777435f-9adc-41e9-8c5b-63eabe18a9f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e923060-25b3-4c2d-be0f-495cd4c6e439",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dafbddc5-a0d1-43fe-b821-c540006ad140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e923060-25b3-4c2d-be0f-495cd4c6e439",
                    "LayerId": "28a95af6-ec41-4620-9c00-6592dd3394b6"
                }
            ]
        },
        {
            "id": "7754ad2d-8b4b-468c-97f7-174aeb2cc18d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e06e12b-73c9-441b-8b34-23b8bf9761bd",
            "compositeImage": {
                "id": "c8192042-d31e-47ee-9842-4648cbac889f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7754ad2d-8b4b-468c-97f7-174aeb2cc18d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c8fa8a7-e7d7-4415-9cf0-e89ed95097dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7754ad2d-8b4b-468c-97f7-174aeb2cc18d",
                    "LayerId": "28a95af6-ec41-4620-9c00-6592dd3394b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "28a95af6-ec41-4620-9c00-6592dd3394b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e06e12b-73c9-441b-8b34-23b8bf9761bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}