{
    "id": "9f7885f1-5853-448c-a52a-5722a857b03b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 36,
    "bbox_right": 603,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf7d2ed5-6abf-441b-891e-36edfa2d17f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f7885f1-5853-448c-a52a-5722a857b03b",
            "compositeImage": {
                "id": "3e3b6460-2efe-41e5-ba42-815a5f0e1cbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf7d2ed5-6abf-441b-891e-36edfa2d17f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20e0351e-f45c-409d-bc50-9b2276fc2838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf7d2ed5-6abf-441b-891e-36edfa2d17f8",
                    "LayerId": "568d1fc0-1f88-41d2-bc5e-35749024bee2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 117,
    "layers": [
        {
            "id": "568d1fc0-1f88-41d2-bc5e-35749024bee2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f7885f1-5853-448c-a52a-5722a857b03b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 58
}