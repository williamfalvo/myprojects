{
    "id": "cbe04032-3cf4-4c2a-99bf-2bbc51afd54c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tv",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e44dd7f-a850-45fb-ab7c-2e59f947ef26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe04032-3cf4-4c2a-99bf-2bbc51afd54c",
            "compositeImage": {
                "id": "942ccd26-aaba-4752-99f9-bf292389be6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e44dd7f-a850-45fb-ab7c-2e59f947ef26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f081c95b-0aee-45f4-b2b4-7d1d06125cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e44dd7f-a850-45fb-ab7c-2e59f947ef26",
                    "LayerId": "2ad0b5e8-7f03-4045-8b89-13363886db77"
                }
            ]
        },
        {
            "id": "ed4904f9-0314-428a-8ed0-d0208247ac06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe04032-3cf4-4c2a-99bf-2bbc51afd54c",
            "compositeImage": {
                "id": "c7fa09ad-12e5-4f1e-bd64-a325386c0eca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed4904f9-0314-428a-8ed0-d0208247ac06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad489289-2818-40e5-b5ad-289ab331d8dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed4904f9-0314-428a-8ed0-d0208247ac06",
                    "LayerId": "2ad0b5e8-7f03-4045-8b89-13363886db77"
                }
            ]
        },
        {
            "id": "b21d2b3e-d749-4ece-a888-e577c9d338bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe04032-3cf4-4c2a-99bf-2bbc51afd54c",
            "compositeImage": {
                "id": "75425888-5b5f-4384-b3d8-8231b4191db4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b21d2b3e-d749-4ece-a888-e577c9d338bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bd3393f-a030-4e33-b44f-7c796541f13e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b21d2b3e-d749-4ece-a888-e577c9d338bb",
                    "LayerId": "2ad0b5e8-7f03-4045-8b89-13363886db77"
                }
            ]
        },
        {
            "id": "b7f06508-9566-44cc-a3f8-bee8cbd39349",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe04032-3cf4-4c2a-99bf-2bbc51afd54c",
            "compositeImage": {
                "id": "56205e9b-aa08-4b90-8643-c7505a1c1ccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7f06508-9566-44cc-a3f8-bee8cbd39349",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76c54516-74a1-49a4-80f5-64be178fb364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7f06508-9566-44cc-a3f8-bee8cbd39349",
                    "LayerId": "2ad0b5e8-7f03-4045-8b89-13363886db77"
                }
            ]
        },
        {
            "id": "711421a3-aa04-423f-aad7-29f6444a258d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe04032-3cf4-4c2a-99bf-2bbc51afd54c",
            "compositeImage": {
                "id": "6e911a2c-f594-45db-a399-684695fc3d23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "711421a3-aa04-423f-aad7-29f6444a258d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f931ab8-35da-4cb2-a63f-2e3c1dafe09a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "711421a3-aa04-423f-aad7-29f6444a258d",
                    "LayerId": "2ad0b5e8-7f03-4045-8b89-13363886db77"
                }
            ]
        },
        {
            "id": "a7fcce9d-2c69-428b-8fa1-6eb984e2ee89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe04032-3cf4-4c2a-99bf-2bbc51afd54c",
            "compositeImage": {
                "id": "2d12ed20-2791-43e8-8232-46492c312113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7fcce9d-2c69-428b-8fa1-6eb984e2ee89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f922fea9-d7e2-497a-bcc3-60208b4f2488",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7fcce9d-2c69-428b-8fa1-6eb984e2ee89",
                    "LayerId": "2ad0b5e8-7f03-4045-8b89-13363886db77"
                }
            ]
        },
        {
            "id": "9c4bd5cf-bf3d-4777-97e8-53350fd33e7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe04032-3cf4-4c2a-99bf-2bbc51afd54c",
            "compositeImage": {
                "id": "c47df478-b7bd-498f-9099-ad158528befb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c4bd5cf-bf3d-4777-97e8-53350fd33e7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "698c0d67-5604-4872-93ed-3c586fc057fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c4bd5cf-bf3d-4777-97e8-53350fd33e7c",
                    "LayerId": "2ad0b5e8-7f03-4045-8b89-13363886db77"
                }
            ]
        },
        {
            "id": "8e71e58e-3136-4b15-abfb-3b3b46e8cdce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbe04032-3cf4-4c2a-99bf-2bbc51afd54c",
            "compositeImage": {
                "id": "a89bb99f-615a-4cc6-b62c-0bac1606d498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e71e58e-3136-4b15-abfb-3b3b46e8cdce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44c0aec6-8147-4fa1-98f2-2caae736eea8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e71e58e-3136-4b15-abfb-3b3b46e8cdce",
                    "LayerId": "2ad0b5e8-7f03-4045-8b89-13363886db77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2ad0b5e8-7f03-4045-8b89-13363886db77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbe04032-3cf4-4c2a-99bf-2bbc51afd54c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}