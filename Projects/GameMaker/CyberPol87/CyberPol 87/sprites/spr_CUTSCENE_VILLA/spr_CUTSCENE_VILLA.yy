{
    "id": "e763ece8-8dd5-4a71-bfbd-dea0255e789e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CUTSCENE_VILLA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b36bb67-5ad7-4e24-8d96-ca400658d446",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e763ece8-8dd5-4a71-bfbd-dea0255e789e",
            "compositeImage": {
                "id": "6ee4d8fc-c683-46d6-808a-b5b10d862c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b36bb67-5ad7-4e24-8d96-ca400658d446",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a91dc59-48d8-4785-80d4-e10784e45d50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b36bb67-5ad7-4e24-8d96-ca400658d446",
                    "LayerId": "ebb16f0b-d15f-44f4-93b4-4081f3d39783"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "ebb16f0b-d15f-44f4-93b4-4081f3d39783",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e763ece8-8dd5-4a71-bfbd-dea0255e789e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}