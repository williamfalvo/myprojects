{
    "id": "5dfde7b7-19a7-4331-aaf3-d23a17730b23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_barile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 16,
    "bbox_right": 47,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7253c811-2295-40f4-988d-377aa934155b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5dfde7b7-19a7-4331-aaf3-d23a17730b23",
            "compositeImage": {
                "id": "b0147ae3-20a6-4daf-ae66-7d6c7b918a29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7253c811-2295-40f4-988d-377aa934155b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b09f96a-6e86-40a7-997a-67bd68654555",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7253c811-2295-40f4-988d-377aa934155b",
                    "LayerId": "3fb978d5-2f37-493e-b1d9-46384f1b8e51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3fb978d5-2f37-493e-b1d9-46384f1b8e51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5dfde7b7-19a7-4331-aaf3-d23a17730b23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}