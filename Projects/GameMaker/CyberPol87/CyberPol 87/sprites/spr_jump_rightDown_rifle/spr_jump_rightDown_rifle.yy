{
    "id": "e497a3ca-7935-403a-8f23-4345b1f37936",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_rightDown_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e82c419-58bc-4218-b613-030048c30286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e497a3ca-7935-403a-8f23-4345b1f37936",
            "compositeImage": {
                "id": "7a738c19-9a7d-4b0b-9fbf-a7e003e58657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e82c419-58bc-4218-b613-030048c30286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86c23697-2d75-4b40-8674-3493bf1fc096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e82c419-58bc-4218-b613-030048c30286",
                    "LayerId": "bf537718-2bbc-4687-863d-de060676ab26"
                }
            ]
        },
        {
            "id": "372e3cd3-a592-46d7-a75a-33c52f083c7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e497a3ca-7935-403a-8f23-4345b1f37936",
            "compositeImage": {
                "id": "59d8bed5-f3d1-4781-aeab-a746c26f38c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "372e3cd3-a592-46d7-a75a-33c52f083c7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb7c76b8-1de5-4cb6-a23c-a732a53b96bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "372e3cd3-a592-46d7-a75a-33c52f083c7a",
                    "LayerId": "bf537718-2bbc-4687-863d-de060676ab26"
                }
            ]
        },
        {
            "id": "fe66a690-a004-4bc9-8e0f-daffedbf93b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e497a3ca-7935-403a-8f23-4345b1f37936",
            "compositeImage": {
                "id": "47a80994-d15a-410e-a514-fe90ebc3fb4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe66a690-a004-4bc9-8e0f-daffedbf93b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d200d95d-309e-4cc2-aebd-2c179ae9173b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe66a690-a004-4bc9-8e0f-daffedbf93b9",
                    "LayerId": "bf537718-2bbc-4687-863d-de060676ab26"
                }
            ]
        },
        {
            "id": "2a2bdcb5-7b98-4da8-b2ad-4889aba31915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e497a3ca-7935-403a-8f23-4345b1f37936",
            "compositeImage": {
                "id": "abf65ac2-7952-4ff9-9749-2a8972223800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a2bdcb5-7b98-4da8-b2ad-4889aba31915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab1cad9-aedf-47b0-b532-c3b559596693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a2bdcb5-7b98-4da8-b2ad-4889aba31915",
                    "LayerId": "bf537718-2bbc-4687-863d-de060676ab26"
                }
            ]
        },
        {
            "id": "490f6e08-fe3c-4dc5-b1af-f011908f9ddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e497a3ca-7935-403a-8f23-4345b1f37936",
            "compositeImage": {
                "id": "2ae0dbdb-5ea2-4cef-ad01-5ee4066c5800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "490f6e08-fe3c-4dc5-b1af-f011908f9ddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07e31415-2dc4-4b94-ba32-18e756ea654f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "490f6e08-fe3c-4dc5-b1af-f011908f9ddd",
                    "LayerId": "bf537718-2bbc-4687-863d-de060676ab26"
                }
            ]
        },
        {
            "id": "03e86279-6c31-4353-a2d1-6ff9230edcd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e497a3ca-7935-403a-8f23-4345b1f37936",
            "compositeImage": {
                "id": "36c74196-de1e-4295-869d-b909b74d3a73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e86279-6c31-4353-a2d1-6ff9230edcd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36eb418a-bb82-40ac-af65-93a93a3a17c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e86279-6c31-4353-a2d1-6ff9230edcd3",
                    "LayerId": "bf537718-2bbc-4687-863d-de060676ab26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "bf537718-2bbc-4687-863d-de060676ab26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e497a3ca-7935-403a-8f23-4345b1f37936",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}