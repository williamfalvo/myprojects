{
    "id": "424372a7-6481-4dd4-860f-7455e0b45b73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controls_keyboard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7706181d-e2c2-449d-80e8-80aa176c03e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "424372a7-6481-4dd4-860f-7455e0b45b73",
            "compositeImage": {
                "id": "0872ecb6-4a57-40c4-908a-951442c08f1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7706181d-e2c2-449d-80e8-80aa176c03e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8843af95-9a5b-4afe-b5b4-2891f891e508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7706181d-e2c2-449d-80e8-80aa176c03e4",
                    "LayerId": "603b02da-d867-46fc-bbb7-bbf1273547e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "603b02da-d867-46fc-bbb7-bbf1273547e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "424372a7-6481-4dd4-860f-7455e0b45b73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 180
}