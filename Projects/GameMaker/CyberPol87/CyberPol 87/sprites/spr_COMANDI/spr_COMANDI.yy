{
    "id": "be1f706a-226e-4a99-9ae1-e14a0081a34c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_COMANDI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 107,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "594fd516-5b99-4a51-8e8e-49f503190063",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be1f706a-226e-4a99-9ae1-e14a0081a34c",
            "compositeImage": {
                "id": "e54471e3-62e9-4ff1-b955-6fcffd8124a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "594fd516-5b99-4a51-8e8e-49f503190063",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b60806de-6cae-418d-a928-97dcf862acab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "594fd516-5b99-4a51-8e8e-49f503190063",
                    "LayerId": "fb912871-cad7-4e0d-b1d2-aca01fbf919c"
                }
            ]
        },
        {
            "id": "8bcd440c-e3f6-4ad8-8922-a240d023b0f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be1f706a-226e-4a99-9ae1-e14a0081a34c",
            "compositeImage": {
                "id": "f78e6bfb-1359-4b23-b05d-1fa482b8945d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bcd440c-e3f6-4ad8-8922-a240d023b0f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5534880a-de67-4729-b4fb-cf40299491c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bcd440c-e3f6-4ad8-8922-a240d023b0f1",
                    "LayerId": "fb912871-cad7-4e0d-b1d2-aca01fbf919c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fb912871-cad7-4e0d-b1d2-aca01fbf919c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be1f706a-226e-4a99-9ae1-e14a0081a34c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 54,
    "yorig": 16
}