{
    "id": "c012885b-faa8-4a07-ae6a-da0781ba3010",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lampadina_villa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 3,
    "bbox_right": 26,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98d5e850-dbc2-464a-a4e5-ab6b4d56ea31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c012885b-faa8-4a07-ae6a-da0781ba3010",
            "compositeImage": {
                "id": "e812fd09-c0d1-4859-ae67-5f334fc03334",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98d5e850-dbc2-464a-a4e5-ab6b4d56ea31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "429a134c-5b1e-46e1-b487-fe8eb5c80c4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98d5e850-dbc2-464a-a4e5-ab6b4d56ea31",
                    "LayerId": "ec5ddd6a-a03e-47fa-9199-3bfe53e2cea0"
                }
            ]
        },
        {
            "id": "a63e3be0-ead4-4b22-a704-417821e5924a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c012885b-faa8-4a07-ae6a-da0781ba3010",
            "compositeImage": {
                "id": "49bb65e2-af12-4c4c-8c98-23737208565e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a63e3be0-ead4-4b22-a704-417821e5924a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a671c1c-e99d-486b-aac1-8455bbe27ae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a63e3be0-ead4-4b22-a704-417821e5924a",
                    "LayerId": "ec5ddd6a-a03e-47fa-9199-3bfe53e2cea0"
                }
            ]
        },
        {
            "id": "adcb9e36-20f1-4107-a79a-588de8e99f01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c012885b-faa8-4a07-ae6a-da0781ba3010",
            "compositeImage": {
                "id": "637d8fd0-ea5d-46fc-af92-c5d749e2a58b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adcb9e36-20f1-4107-a79a-588de8e99f01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "714cb305-ecf2-4420-9caa-937304c8175f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adcb9e36-20f1-4107-a79a-588de8e99f01",
                    "LayerId": "ec5ddd6a-a03e-47fa-9199-3bfe53e2cea0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ec5ddd6a-a03e-47fa-9199-3bfe53e2cea0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c012885b-faa8-4a07-ae6a-da0781ba3010",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}