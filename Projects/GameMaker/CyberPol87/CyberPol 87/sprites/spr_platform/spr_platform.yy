{
    "id": "a3485abd-1dfc-42e2-892b-2981d6990815",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_platform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c153aea8-f694-44fa-8b0d-a1199ba1cd9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3485abd-1dfc-42e2-892b-2981d6990815",
            "compositeImage": {
                "id": "78695bcc-2154-40f9-839f-27eaa37723a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c153aea8-f694-44fa-8b0d-a1199ba1cd9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1e5eb08-e718-40ee-ab57-e8fc5b6565c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c153aea8-f694-44fa-8b0d-a1199ba1cd9d",
                    "LayerId": "f3666a4d-daaf-4e39-b363-32041ad8a44b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f3666a4d-daaf-4e39-b363-32041ad8a44b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3485abd-1dfc-42e2-892b-2981d6990815",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}