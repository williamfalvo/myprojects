{
    "id": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cavi_backup1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0caa5ddd-b7eb-4b24-9af5-132cc92dbc1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "3473568c-f825-4cb2-b749-c10c8ee9aebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0caa5ddd-b7eb-4b24-9af5-132cc92dbc1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fb514ee-07f7-4b8b-959f-1366acb95e27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0caa5ddd-b7eb-4b24-9af5-132cc92dbc1d",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "76cfd9a2-9ea9-4299-a41e-eaff2be4a0c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "c51cf44c-ac4c-4d8e-bf97-d7337e10609b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76cfd9a2-9ea9-4299-a41e-eaff2be4a0c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "598b0e66-41c3-4101-a050-4f71d4dd776f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76cfd9a2-9ea9-4299-a41e-eaff2be4a0c5",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "32788866-f307-40f4-bdaf-55ce22c1f6fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "c2e061ec-ab1a-46b9-add1-0d432ab38312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32788866-f307-40f4-bdaf-55ce22c1f6fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0922582-bb43-47a8-95d8-da30015a7b1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32788866-f307-40f4-bdaf-55ce22c1f6fe",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "a912cceb-082a-4c96-96b0-88dc2ff3cf93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "92fd2e3c-e013-41e6-a0b2-0df7c8a5e8cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a912cceb-082a-4c96-96b0-88dc2ff3cf93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bf81cf2-f010-40aa-8bfe-b0e8e67506c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a912cceb-082a-4c96-96b0-88dc2ff3cf93",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "7905609e-b85a-47e3-9be7-743f0bb1c0ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "7321f51b-90f8-4aea-bd1a-409eff8f98ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7905609e-b85a-47e3-9be7-743f0bb1c0ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bd3b9d3-31e6-4201-ab9b-2697884221e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7905609e-b85a-47e3-9be7-743f0bb1c0ff",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "73d5f380-965c-4e44-8823-480b1a41a550",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "8c79190c-bd2d-438e-82a2-1583f49356e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73d5f380-965c-4e44-8823-480b1a41a550",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ba52a0a-1a01-49f2-8422-aff84e8fa9c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73d5f380-965c-4e44-8823-480b1a41a550",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "2ef69147-44b8-4e0d-8017-93b01ed2eca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "cf633217-683d-4d59-9dc4-1952a7b8d131",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ef69147-44b8-4e0d-8017-93b01ed2eca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae548ff2-4f99-415c-a0de-0428c2822965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ef69147-44b8-4e0d-8017-93b01ed2eca7",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "89c09792-928e-4266-99ec-3f338445fa49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "bde30eb2-820e-4dea-b0c0-8f5f3f682a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89c09792-928e-4266-99ec-3f338445fa49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19ade489-c133-4a8b-b161-ab7dde8c26d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89c09792-928e-4266-99ec-3f338445fa49",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "1cc03459-88d7-4e08-8401-379457cd5dfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "a559a3c4-fcaf-463a-9a12-e420e8dd9f3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cc03459-88d7-4e08-8401-379457cd5dfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4936716-729c-4900-96a9-f2bb997cf889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cc03459-88d7-4e08-8401-379457cd5dfd",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "201f56c4-3fac-4f0f-9c3c-0c68f9f4e0c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "b98b3d9d-084e-4f39-99be-a928870e47da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "201f56c4-3fac-4f0f-9c3c-0c68f9f4e0c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78ca427c-7dad-4d7b-914c-aa7afb1d1e5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "201f56c4-3fac-4f0f-9c3c-0c68f9f4e0c2",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "45d89169-3329-4faa-9a35-c2245f833121",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "77d3197b-d4f0-44e2-b0f4-bce2298b9e40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d89169-3329-4faa-9a35-c2245f833121",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a397fad4-b93e-416f-8ee0-800c4c92a19b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d89169-3329-4faa-9a35-c2245f833121",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "7e30b044-fa64-4fc8-a823-8e2b3f8d5b37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "9ee0cae2-b045-4d00-a447-d43edd1ea0e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e30b044-fa64-4fc8-a823-8e2b3f8d5b37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51cf5bd5-fe2c-4534-9e4c-cc6edc30a0dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e30b044-fa64-4fc8-a823-8e2b3f8d5b37",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "02ca507d-cbd0-4e32-aad4-987518436b4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "5515d17e-089a-4c64-a540-8b01b0e98a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02ca507d-cbd0-4e32-aad4-987518436b4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "788fdd7b-faf0-49f8-87f8-d349ea95deda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02ca507d-cbd0-4e32-aad4-987518436b4e",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "3497956c-fdaf-4739-9962-f58b4c6ae0d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "50715bdf-d4fc-458d-820a-9a0c2c3ad7d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3497956c-fdaf-4739-9962-f58b4c6ae0d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8c82ce0-3241-4f88-b8dd-da3969116667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3497956c-fdaf-4739-9962-f58b4c6ae0d8",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "53f916a1-7795-4e49-9c50-05cb078579d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "1fb1d7d5-22a5-4ac0-a945-d08d48b6158b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53f916a1-7795-4e49-9c50-05cb078579d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6152f955-08cb-4ab5-aef2-b1d8cb574c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53f916a1-7795-4e49-9c50-05cb078579d5",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "96ace5b3-952b-4eae-a767-6f3fec36dc1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "1ed41b96-9ba8-4df3-ad66-a5bda42ff6c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96ace5b3-952b-4eae-a767-6f3fec36dc1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c3652f6-c445-4120-afaa-51aec9a9bcae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96ace5b3-952b-4eae-a767-6f3fec36dc1e",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "3f9f4507-c777-4b70-9915-603582fa2cc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "a6d14de5-f3d8-4524-a3ef-5e8c44d7f686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f9f4507-c777-4b70-9915-603582fa2cc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2c5814f-f32e-4b3a-9675-5e061dd9e634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f9f4507-c777-4b70-9915-603582fa2cc1",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "b6bb5526-affa-4f4d-913c-0eaeae4040d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "fc32f55b-99d2-43cd-bae9-477ba99811c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6bb5526-affa-4f4d-913c-0eaeae4040d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43734f7d-fe94-427a-a072-d4387f5dfaf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6bb5526-affa-4f4d-913c-0eaeae4040d3",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "a1566bf9-8a37-4e13-acb2-e341d0ab14e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "f1012ebc-9ae0-4ec0-bba2-8e2d2d044c6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1566bf9-8a37-4e13-acb2-e341d0ab14e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1c3355c-e093-4912-ad67-72d9916daef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1566bf9-8a37-4e13-acb2-e341d0ab14e0",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "107a24ff-1d18-4571-9ef4-8e11e789e21d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "5a902ea7-9e8b-4e98-960c-4f01e4cd562a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "107a24ff-1d18-4571-9ef4-8e11e789e21d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c92ef740-9ece-46f2-9791-446db19d1986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "107a24ff-1d18-4571-9ef4-8e11e789e21d",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        },
        {
            "id": "3c59364d-0c59-43f4-b39e-91a6f3265f08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "compositeImage": {
                "id": "fdc4ae44-ac6a-4fb0-91cb-d764c82d96f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c59364d-0c59-43f4-b39e-91a6f3265f08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36cd2473-cc3c-440e-9a6d-ff3454c63ecf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c59364d-0c59-43f4-b39e-91a6f3265f08",
                    "LayerId": "0feb4fd2-8834-47aa-bc34-db5b297b4460"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0feb4fd2-8834-47aa-bc34-db5b297b4460",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "551c1b7a-7116-4c20-8843-58ee7a358c8b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}