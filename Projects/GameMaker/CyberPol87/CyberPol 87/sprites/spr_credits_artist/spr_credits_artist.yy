{
    "id": "393df9bb-53c9-4648-a81c-93059aeb2972",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_credits_artist",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 231,
    "bbox_left": 37,
    "bbox_right": 589,
    "bbox_top": 33,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb88b190-760b-4ddf-972f-df8955f1af34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "393df9bb-53c9-4648-a81c-93059aeb2972",
            "compositeImage": {
                "id": "09eff8b4-d850-405c-81ae-d1e96f196541",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb88b190-760b-4ddf-972f-df8955f1af34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54db9891-ca85-4360-9aa7-c0a15bda2be2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb88b190-760b-4ddf-972f-df8955f1af34",
                    "LayerId": "e9789602-5bd8-4c57-ae7a-83468cf7013b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 310,
    "layers": [
        {
            "id": "e9789602-5bd8-4c57-ae7a-83468cf7013b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "393df9bb-53c9-4648-a81c-93059aeb2972",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 610,
    "xorig": 305,
    "yorig": 155
}