{
    "id": "b8e6995e-e822-467f-b113-d57046987b4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "493e31fc-a191-4915-9f90-a199f16ad4d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8e6995e-e822-467f-b113-d57046987b4b",
            "compositeImage": {
                "id": "b70dee6f-b629-4a94-b97f-01fa4a5290e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "493e31fc-a191-4915-9f90-a199f16ad4d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19afea0b-0f18-43f9-8654-e3e8c401c508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "493e31fc-a191-4915-9f90-a199f16ad4d2",
                    "LayerId": "af2747ef-f9ab-4d65-bd31-269f0fba294e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "af2747ef-f9ab-4d65-bd31-269f0fba294e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8e6995e-e822-467f-b113-d57046987b4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}