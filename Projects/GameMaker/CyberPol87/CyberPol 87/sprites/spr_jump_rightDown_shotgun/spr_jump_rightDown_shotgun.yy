{
    "id": "cde5a693-0260-4e91-a71a-beef369d61f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_rightDown_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17fbe14e-c6da-4f27-b8c6-16e358b7294e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde5a693-0260-4e91-a71a-beef369d61f6",
            "compositeImage": {
                "id": "db3da244-99dc-4601-8c81-9cfded330c22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17fbe14e-c6da-4f27-b8c6-16e358b7294e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95e2c52d-b723-4b05-b0b6-06db699c97d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17fbe14e-c6da-4f27-b8c6-16e358b7294e",
                    "LayerId": "5d73045d-84b2-458e-8293-6fd7de3ce5b2"
                }
            ]
        },
        {
            "id": "11fcbbe7-5e74-48ba-8b37-408fd39f923b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde5a693-0260-4e91-a71a-beef369d61f6",
            "compositeImage": {
                "id": "789a4d82-ece2-45dd-bd01-37b4a1651d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11fcbbe7-5e74-48ba-8b37-408fd39f923b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9731f0a4-07f5-4266-9e62-8fd53cdf97b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11fcbbe7-5e74-48ba-8b37-408fd39f923b",
                    "LayerId": "5d73045d-84b2-458e-8293-6fd7de3ce5b2"
                }
            ]
        },
        {
            "id": "616ab85a-4cb3-4b75-835d-f1bef9840cbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde5a693-0260-4e91-a71a-beef369d61f6",
            "compositeImage": {
                "id": "a8246b83-b367-4826-a720-22f982cc0b4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "616ab85a-4cb3-4b75-835d-f1bef9840cbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eee1627-0697-4d79-99db-815c507c4e8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "616ab85a-4cb3-4b75-835d-f1bef9840cbc",
                    "LayerId": "5d73045d-84b2-458e-8293-6fd7de3ce5b2"
                }
            ]
        },
        {
            "id": "d2a9c74c-360d-4fa7-b184-96587226c2a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde5a693-0260-4e91-a71a-beef369d61f6",
            "compositeImage": {
                "id": "809e353a-3a80-4e8c-931d-517c5858d27e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2a9c74c-360d-4fa7-b184-96587226c2a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c301c1f6-b6e3-4203-ace1-766cae199ae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2a9c74c-360d-4fa7-b184-96587226c2a4",
                    "LayerId": "5d73045d-84b2-458e-8293-6fd7de3ce5b2"
                }
            ]
        },
        {
            "id": "0850d64a-b23c-4204-b58d-ccd184868a42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde5a693-0260-4e91-a71a-beef369d61f6",
            "compositeImage": {
                "id": "5253c63c-9447-4fde-a951-5ba9208930ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0850d64a-b23c-4204-b58d-ccd184868a42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d64ba7f6-7de5-4563-98df-ba5a9aaee189",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0850d64a-b23c-4204-b58d-ccd184868a42",
                    "LayerId": "5d73045d-84b2-458e-8293-6fd7de3ce5b2"
                }
            ]
        },
        {
            "id": "00b71d8e-d7c8-4955-92ab-f0b063223f3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde5a693-0260-4e91-a71a-beef369d61f6",
            "compositeImage": {
                "id": "298b0693-dbcc-4c35-970c-70863f569e4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00b71d8e-d7c8-4955-92ab-f0b063223f3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6ce8f0a-3929-49cf-87f2-4d7191930144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00b71d8e-d7c8-4955-92ab-f0b063223f3f",
                    "LayerId": "5d73045d-84b2-458e-8293-6fd7de3ce5b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "5d73045d-84b2-458e-8293-6fd7de3ce5b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cde5a693-0260-4e91-a71a-beef369d61f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}