{
    "id": "551c6978-b221-4f0a-ab02-1f8fc5be88b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_big_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 45,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80352382-ccfe-4424-ab7e-fbd1da76d7b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "551c6978-b221-4f0a-ab02-1f8fc5be88b6",
            "compositeImage": {
                "id": "58fb581e-42b5-4bed-bc3f-95e5c0dc3baa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80352382-ccfe-4424-ab7e-fbd1da76d7b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb0a47a1-04a0-42da-89ed-fb41f58eb5fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80352382-ccfe-4424-ab7e-fbd1da76d7b8",
                    "LayerId": "e5c812b9-dda7-4d19-af05-df94ff888dd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e5c812b9-dda7-4d19-af05-df94ff888dd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "551c6978-b221-4f0a-ab02-1f8fc5be88b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}