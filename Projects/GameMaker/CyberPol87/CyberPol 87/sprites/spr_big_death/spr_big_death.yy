{
    "id": "5d85a362-c31a-440a-ae91-5833c26a0543",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_big_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3139c1d5-0fa5-45a2-86a0-1626f4deff10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d85a362-c31a-440a-ae91-5833c26a0543",
            "compositeImage": {
                "id": "187599bc-82b6-4e95-b492-7d967adf518b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3139c1d5-0fa5-45a2-86a0-1626f4deff10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ec9c2c5-9491-4bef-a1ea-208214ebfc5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3139c1d5-0fa5-45a2-86a0-1626f4deff10",
                    "LayerId": "a4081d6b-49c6-4469-ab27-bc0652241d60"
                }
            ]
        },
        {
            "id": "a21c1900-84d7-4add-8803-b609502abd73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d85a362-c31a-440a-ae91-5833c26a0543",
            "compositeImage": {
                "id": "8685c547-70c0-45cf-ac46-829c791aa4cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a21c1900-84d7-4add-8803-b609502abd73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4cb02a1-e50e-4a07-9afe-ebfde13dbbd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a21c1900-84d7-4add-8803-b609502abd73",
                    "LayerId": "a4081d6b-49c6-4469-ab27-bc0652241d60"
                }
            ]
        },
        {
            "id": "ffa238c5-2872-4e8f-8f91-a2aabcbfb66a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d85a362-c31a-440a-ae91-5833c26a0543",
            "compositeImage": {
                "id": "82c84f8b-e1d5-4b8d-b2a9-7d24e00b06a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa238c5-2872-4e8f-8f91-a2aabcbfb66a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "015fcde6-3fb7-495b-b01b-efc695e0c8ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa238c5-2872-4e8f-8f91-a2aabcbfb66a",
                    "LayerId": "a4081d6b-49c6-4469-ab27-bc0652241d60"
                }
            ]
        },
        {
            "id": "975b12fb-819b-44f5-b486-d6305cfc8ffc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d85a362-c31a-440a-ae91-5833c26a0543",
            "compositeImage": {
                "id": "ae596aaa-d7ba-4a33-99a7-c3b86705c78b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "975b12fb-819b-44f5-b486-d6305cfc8ffc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee704884-4aa1-4bfe-bc02-d37e72f1c699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "975b12fb-819b-44f5-b486-d6305cfc8ffc",
                    "LayerId": "a4081d6b-49c6-4469-ab27-bc0652241d60"
                }
            ]
        },
        {
            "id": "75a39e6e-20ce-4564-9279-11a2e5d928dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d85a362-c31a-440a-ae91-5833c26a0543",
            "compositeImage": {
                "id": "8836972d-d0e6-4be7-a710-0b65f70379b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75a39e6e-20ce-4564-9279-11a2e5d928dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3340346b-1060-4e1e-9430-170422c92f80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75a39e6e-20ce-4564-9279-11a2e5d928dd",
                    "LayerId": "a4081d6b-49c6-4469-ab27-bc0652241d60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a4081d6b-49c6-4469-ab27-bc0652241d60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d85a362-c31a-440a-ae91-5833c26a0543",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}