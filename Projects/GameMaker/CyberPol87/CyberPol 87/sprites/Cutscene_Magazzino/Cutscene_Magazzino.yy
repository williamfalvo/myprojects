{
    "id": "e2572e46-89d6-4b97-bdec-b9927b9e54ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Cutscene_Magazzino",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 228,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f61965f1-5157-40be-a8c2-7b1de19525fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2572e46-89d6-4b97-bdec-b9927b9e54ea",
            "compositeImage": {
                "id": "f8d2fbc8-7326-44b4-bedb-19d064007692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f61965f1-5157-40be-a8c2-7b1de19525fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7462966-25e8-4a3a-84f2-31e21a6e3db5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f61965f1-5157-40be-a8c2-7b1de19525fe",
                    "LayerId": "337dc8a4-5721-4d96-b41d-94a8c2e1a734"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "337dc8a4-5721-4d96-b41d-94a8c2e1a734",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2572e46-89d6-4b97-bdec-b9927b9e54ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 180
}