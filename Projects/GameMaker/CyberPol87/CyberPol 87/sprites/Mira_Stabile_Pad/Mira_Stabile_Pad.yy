{
    "id": "7a7b2343-1ef3-40ea-ac00-06c837695607",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Mira_Stabile_Pad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ce4cd2b-d742-4c30-97ac-88ab337fad9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7b2343-1ef3-40ea-ac00-06c837695607",
            "compositeImage": {
                "id": "848d9a43-90aa-4bf5-9b45-f6028042f852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ce4cd2b-d742-4c30-97ac-88ab337fad9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e221f100-1c5a-4203-b8a8-ad6e42e44e50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ce4cd2b-d742-4c30-97ac-88ab337fad9f",
                    "LayerId": "23481099-309b-4e59-a3ea-9a8cf2902319"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "23481099-309b-4e59-a3ea-9a8cf2902319",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a7b2343-1ef3-40ea-ac00-06c837695607",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}