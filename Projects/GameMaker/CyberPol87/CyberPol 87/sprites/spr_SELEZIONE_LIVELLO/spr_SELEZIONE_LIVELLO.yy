{
    "id": "62054eca-a760-42a4-b36e-eb23434ef9d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SELEZIONE_LIVELLO",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 14,
    "bbox_right": 221,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93f157de-35cd-43af-a7fa-b1a05cb02c40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62054eca-a760-42a4-b36e-eb23434ef9d0",
            "compositeImage": {
                "id": "d975910d-c7f7-44e7-9ff1-e20043aebcc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93f157de-35cd-43af-a7fa-b1a05cb02c40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7c720be-7cda-4014-bfef-786e3c9033cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93f157de-35cd-43af-a7fa-b1a05cb02c40",
                    "LayerId": "3ac2a243-dccf-4deb-ac36-6d280fa5614c"
                }
            ]
        },
        {
            "id": "a128be81-4207-4b70-baa5-dbf3784ffd9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "62054eca-a760-42a4-b36e-eb23434ef9d0",
            "compositeImage": {
                "id": "f1cd64fa-b3ce-45f7-aeb3-d44e56f64290",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a128be81-4207-4b70-baa5-dbf3784ffd9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d0a7bef-d69b-451f-91ab-db573885f7c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a128be81-4207-4b70-baa5-dbf3784ffd9b",
                    "LayerId": "3ac2a243-dccf-4deb-ac36-6d280fa5614c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "3ac2a243-dccf-4deb-ac36-6d280fa5614c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "62054eca-a760-42a4-b36e-eb23434ef9d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 235,
    "xorig": 117,
    "yorig": 28
}