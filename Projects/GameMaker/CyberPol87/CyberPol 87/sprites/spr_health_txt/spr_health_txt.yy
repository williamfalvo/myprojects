{
    "id": "1fd9a6fb-8f06-4d89-84f7-486420b5e929",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_health_txt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2476f149-5ac1-4aa5-a17a-317d9966a1d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fd9a6fb-8f06-4d89-84f7-486420b5e929",
            "compositeImage": {
                "id": "e5672694-8a86-4aff-85a3-9b8d91e9ecfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2476f149-5ac1-4aa5-a17a-317d9966a1d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83ecd6bb-1b96-4dc6-a9db-bfc191e5ede1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2476f149-5ac1-4aa5-a17a-317d9966a1d7",
                    "LayerId": "98eddaa6-b104-4b13-a475-a95ef211308e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "98eddaa6-b104-4b13-a475-a95ef211308e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fd9a6fb-8f06-4d89-84f7-486420b5e929",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}