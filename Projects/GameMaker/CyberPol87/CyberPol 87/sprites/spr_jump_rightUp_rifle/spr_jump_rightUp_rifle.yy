{
    "id": "ed4feebe-44af-4caa-a1e2-1c461dec83e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_rightUp_rifle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96fac3b0-96d6-4040-8b57-9ab3a6a6477f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed4feebe-44af-4caa-a1e2-1c461dec83e4",
            "compositeImage": {
                "id": "95e487fd-655f-417e-91be-b25b63948f72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96fac3b0-96d6-4040-8b57-9ab3a6a6477f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cf95978-d34c-480b-98af-5bae29dae340",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96fac3b0-96d6-4040-8b57-9ab3a6a6477f",
                    "LayerId": "08576baa-06df-4daf-a33f-81140d4803c3"
                }
            ]
        },
        {
            "id": "19afdf2a-d346-4848-aa8c-4e003289304a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed4feebe-44af-4caa-a1e2-1c461dec83e4",
            "compositeImage": {
                "id": "d442744d-b355-4fbb-b58b-d83def965ae1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19afdf2a-d346-4848-aa8c-4e003289304a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c45d7968-3a11-49a6-bc6b-63985663f441",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19afdf2a-d346-4848-aa8c-4e003289304a",
                    "LayerId": "08576baa-06df-4daf-a33f-81140d4803c3"
                }
            ]
        },
        {
            "id": "0ffbc257-8753-4ef2-917e-c19dc9e48133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed4feebe-44af-4caa-a1e2-1c461dec83e4",
            "compositeImage": {
                "id": "dfafac9d-caad-4fe1-a3ce-3c8a458b2409",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ffbc257-8753-4ef2-917e-c19dc9e48133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "199e591a-9500-4986-af91-d1e8d9f29062",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ffbc257-8753-4ef2-917e-c19dc9e48133",
                    "LayerId": "08576baa-06df-4daf-a33f-81140d4803c3"
                }
            ]
        },
        {
            "id": "8dccc5cd-7907-4aa5-a249-74f6707f9662",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed4feebe-44af-4caa-a1e2-1c461dec83e4",
            "compositeImage": {
                "id": "57724c59-bc2a-4147-ad3c-dc95e62f4311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dccc5cd-7907-4aa5-a249-74f6707f9662",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69e523f6-bba2-444b-9926-2df258928c5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dccc5cd-7907-4aa5-a249-74f6707f9662",
                    "LayerId": "08576baa-06df-4daf-a33f-81140d4803c3"
                }
            ]
        },
        {
            "id": "d4c1a321-1a18-4954-9fbd-d750f8b22b19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed4feebe-44af-4caa-a1e2-1c461dec83e4",
            "compositeImage": {
                "id": "4f491145-b8b3-4522-be39-c5c15c3a09bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4c1a321-1a18-4954-9fbd-d750f8b22b19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7de4c0bf-54f2-4862-b69d-38fb5137fb48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4c1a321-1a18-4954-9fbd-d750f8b22b19",
                    "LayerId": "08576baa-06df-4daf-a33f-81140d4803c3"
                }
            ]
        },
        {
            "id": "da96093b-2f7c-4ac7-937c-36f45f232ab1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed4feebe-44af-4caa-a1e2-1c461dec83e4",
            "compositeImage": {
                "id": "464f81c0-a801-4f54-b1f3-44666002e140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da96093b-2f7c-4ac7-937c-36f45f232ab1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "299e6466-d19b-474f-8a66-738315540024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da96093b-2f7c-4ac7-937c-36f45f232ab1",
                    "LayerId": "08576baa-06df-4daf-a33f-81140d4803c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "08576baa-06df-4daf-a33f-81140d4803c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed4feebe-44af-4caa-a1e2-1c461dec83e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}