{
    "id": "0d82c301-5241-4096-b95f-6eec0c6e67c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Salto_Tast",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb05782a-fb30-41ba-af5b-8c4a4cbb2990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d82c301-5241-4096-b95f-6eec0c6e67c2",
            "compositeImage": {
                "id": "7bcdf490-9946-4ce5-941c-a4dc6236df1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb05782a-fb30-41ba-af5b-8c4a4cbb2990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc01ada7-1703-45ed-aa75-83016738bcc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb05782a-fb30-41ba-af5b-8c4a4cbb2990",
                    "LayerId": "1577e931-fe0b-4a9c-9fa2-56e14b6f9080"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "1577e931-fe0b-4a9c-9fa2-56e14b6f9080",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d82c301-5241-4096-b95f-6eec0c6e67c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}