{
    "id": "a5281a4f-455e-408a-b03d-01a1af2f2fd4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lampada_villa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 19,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f76e7572-dbf1-4da6-a5fe-e8a3ae1dd420",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5281a4f-455e-408a-b03d-01a1af2f2fd4",
            "compositeImage": {
                "id": "6a381954-0f27-4f47-8319-1619746d19dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76e7572-dbf1-4da6-a5fe-e8a3ae1dd420",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b63e1e2-773b-4048-8b1d-5f8dc1b28c96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76e7572-dbf1-4da6-a5fe-e8a3ae1dd420",
                    "LayerId": "c96fba5b-5d7d-4517-9cef-b477cb916845"
                }
            ]
        },
        {
            "id": "b0c4cd0a-f4a4-42de-94ff-f045bfc8711a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5281a4f-455e-408a-b03d-01a1af2f2fd4",
            "compositeImage": {
                "id": "21188901-35bc-4b25-81af-648b24f56df6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0c4cd0a-f4a4-42de-94ff-f045bfc8711a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db756274-8ecb-432c-8003-507b2345557c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0c4cd0a-f4a4-42de-94ff-f045bfc8711a",
                    "LayerId": "c96fba5b-5d7d-4517-9cef-b477cb916845"
                }
            ]
        },
        {
            "id": "108c979d-2620-45cf-b395-9eedc782c324",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5281a4f-455e-408a-b03d-01a1af2f2fd4",
            "compositeImage": {
                "id": "a0295aba-6b49-4b00-ab66-6e6288756804",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "108c979d-2620-45cf-b395-9eedc782c324",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c8426f3-db29-425a-8d6c-830fc55f10fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "108c979d-2620-45cf-b395-9eedc782c324",
                    "LayerId": "c96fba5b-5d7d-4517-9cef-b477cb916845"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c96fba5b-5d7d-4517-9cef-b477cb916845",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5281a4f-455e-408a-b03d-01a1af2f2fd4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}