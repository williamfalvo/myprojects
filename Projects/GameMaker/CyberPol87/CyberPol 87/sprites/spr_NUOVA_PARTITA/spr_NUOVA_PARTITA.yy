{
    "id": "1bbc6cfa-f3fb-4909-97aa-877ced20387f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_NUOVA_PARTITA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 9,
    "bbox_right": 184,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "baa2c628-a0d2-4419-b52e-d677e7a38fcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bbc6cfa-f3fb-4909-97aa-877ced20387f",
            "compositeImage": {
                "id": "df1457dd-cf9c-414f-aed1-30bc4f65836a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baa2c628-a0d2-4419-b52e-d677e7a38fcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "229f5654-7fbd-449e-903a-e1c49a15d21f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baa2c628-a0d2-4419-b52e-d677e7a38fcf",
                    "LayerId": "34942613-321a-4229-ac69-4b5d58a254dd"
                }
            ]
        },
        {
            "id": "9a5d4b10-ae82-47d9-8cb3-427c459f430a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bbc6cfa-f3fb-4909-97aa-877ced20387f",
            "compositeImage": {
                "id": "ead16c81-8b0e-4c0a-8561-4a2c6ed8f30f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a5d4b10-ae82-47d9-8cb3-427c459f430a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97c57960-49a4-47a9-ad09-9492544d7dbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a5d4b10-ae82-47d9-8cb3-427c459f430a",
                    "LayerId": "34942613-321a-4229-ac69-4b5d58a254dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 62,
    "layers": [
        {
            "id": "34942613-321a-4229-ac69-4b5d58a254dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bbc6cfa-f3fb-4909-97aa-877ced20387f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 194,
    "xorig": 97,
    "yorig": 31
}