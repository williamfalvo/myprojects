{
    "id": "2d41f47e-3fd9-4872-9b60-e4527a1ba844",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_rightUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e713ced4-7c15-47f5-9bcb-00b97e24ea8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d41f47e-3fd9-4872-9b60-e4527a1ba844",
            "compositeImage": {
                "id": "dd7f74ac-bbfc-4e36-8239-83f30edc6231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e713ced4-7c15-47f5-9bcb-00b97e24ea8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b37ab2d5-9a3d-4692-b9b2-15b303bcdf29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e713ced4-7c15-47f5-9bcb-00b97e24ea8d",
                    "LayerId": "48b6635c-386a-4d1b-918f-e52d5b83f18b"
                }
            ]
        },
        {
            "id": "b200315b-fadd-4bc1-85a8-6d236b49528f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d41f47e-3fd9-4872-9b60-e4527a1ba844",
            "compositeImage": {
                "id": "015e1d55-b95a-4edb-bed6-a5ebf49e2f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b200315b-fadd-4bc1-85a8-6d236b49528f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82c4e50f-e455-45e3-8239-481d9f715703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b200315b-fadd-4bc1-85a8-6d236b49528f",
                    "LayerId": "48b6635c-386a-4d1b-918f-e52d5b83f18b"
                }
            ]
        },
        {
            "id": "3ca0643b-62e4-4cbe-a011-223e0477c6d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d41f47e-3fd9-4872-9b60-e4527a1ba844",
            "compositeImage": {
                "id": "d9e72218-4f1c-4a49-96e1-723f197b387e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ca0643b-62e4-4cbe-a011-223e0477c6d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b5bcbb6-ebc3-4419-a50f-e23634a56ca2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ca0643b-62e4-4cbe-a011-223e0477c6d2",
                    "LayerId": "48b6635c-386a-4d1b-918f-e52d5b83f18b"
                }
            ]
        },
        {
            "id": "9d0a13f4-1ff8-4e3d-ad10-afb8630f9cd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d41f47e-3fd9-4872-9b60-e4527a1ba844",
            "compositeImage": {
                "id": "f759ff53-0ea9-4de8-8c97-227e9280e185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d0a13f4-1ff8-4e3d-ad10-afb8630f9cd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11480d98-f9eb-42f7-9a64-3fcf7897fa83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d0a13f4-1ff8-4e3d-ad10-afb8630f9cd0",
                    "LayerId": "48b6635c-386a-4d1b-918f-e52d5b83f18b"
                }
            ]
        },
        {
            "id": "05e7aa59-088d-4388-9854-87decb6bce41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d41f47e-3fd9-4872-9b60-e4527a1ba844",
            "compositeImage": {
                "id": "e1d72696-05fd-436b-9892-ffe426ecc2e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05e7aa59-088d-4388-9854-87decb6bce41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d717866-3007-4892-8b8e-9a653db4075d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05e7aa59-088d-4388-9854-87decb6bce41",
                    "LayerId": "48b6635c-386a-4d1b-918f-e52d5b83f18b"
                }
            ]
        },
        {
            "id": "80530fc8-062f-4760-adf4-7fd68ef50429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d41f47e-3fd9-4872-9b60-e4527a1ba844",
            "compositeImage": {
                "id": "6093cdf4-e332-490c-80b5-cd92723296ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80530fc8-062f-4760-adf4-7fd68ef50429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cad8aadf-844f-490b-b656-81ed01f061b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80530fc8-062f-4760-adf4-7fd68ef50429",
                    "LayerId": "48b6635c-386a-4d1b-918f-e52d5b83f18b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "48b6635c-386a-4d1b-918f-e52d5b83f18b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d41f47e-3fd9-4872-9b60-e4527a1ba844",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}