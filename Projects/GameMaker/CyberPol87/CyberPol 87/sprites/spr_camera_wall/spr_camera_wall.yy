{
    "id": "82691acb-8279-4020-a9f6-ea9e40f74d66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_camera_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3db178bc-f277-478b-9e3d-f326d18588df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82691acb-8279-4020-a9f6-ea9e40f74d66",
            "compositeImage": {
                "id": "53888997-365e-4e79-a0c4-c7faea26f54f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3db178bc-f277-478b-9e3d-f326d18588df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99f270ea-3348-493b-b808-acb69de6d2b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3db178bc-f277-478b-9e3d-f326d18588df",
                    "LayerId": "40318770-221a-481c-8678-b6cd8ea968fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "40318770-221a-481c-8678-b6cd8ea968fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82691acb-8279-4020-a9f6-ea9e40f74d66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 180
}