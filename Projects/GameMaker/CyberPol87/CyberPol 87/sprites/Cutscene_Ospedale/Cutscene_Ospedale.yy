{
    "id": "2133a7d0-68fc-4533-a71f-fd2df6e52a45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Cutscene_Ospedale",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "144625b2-1b3f-4abb-810e-2e98ab2bde2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2133a7d0-68fc-4533-a71f-fd2df6e52a45",
            "compositeImage": {
                "id": "ee084b98-7a80-4786-a1c8-1583f2baba1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "144625b2-1b3f-4abb-810e-2e98ab2bde2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0544fc8-f2a4-4875-9d7a-7cf787b47ab1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "144625b2-1b3f-4abb-810e-2e98ab2bde2e",
                    "LayerId": "8c0c1d6f-7b1b-470d-beb0-3f3ee375f6a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 352,
    "layers": [
        {
            "id": "8c0c1d6f-7b1b-470d-beb0-3f3ee375f6a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2133a7d0-68fc-4533-a71f-fd2df6e52a45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 176
}