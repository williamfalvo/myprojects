{
    "id": "8f7a765e-a0f8-44f6-9c51-6ae1c99dd8dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Ricarica_Ammo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 117,
    "bbox_left": 16,
    "bbox_right": 237,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7ccec24-ac6e-4900-b645-6cf0c6459e72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f7a765e-a0f8-44f6-9c51-6ae1c99dd8dd",
            "compositeImage": {
                "id": "4b4b5534-9851-45b8-98e8-786bf5ed619e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ccec24-ac6e-4900-b645-6cf0c6459e72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0ea9a60-9395-4adc-bb4c-7ff22eda539f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ccec24-ac6e-4900-b645-6cf0c6459e72",
                    "LayerId": "0d6cd758-37ed-4651-8571-304261d9420d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "0d6cd758-37ed-4651-8571-304261d9420d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f7a765e-a0f8-44f6-9c51-6ae1c99dd8dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 60
}