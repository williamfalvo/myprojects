{
    "id": "64530070-bfd4-444f-b705-03303f9fce85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_big_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 60,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6415da4e-7e39-4def-8783-96089e6f0f82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64530070-bfd4-444f-b705-03303f9fce85",
            "compositeImage": {
                "id": "a8c0515c-7a66-49da-be9e-039f9245ff4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6415da4e-7e39-4def-8783-96089e6f0f82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c3ab3f2-b185-44f4-9bc4-7a1b12df39c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6415da4e-7e39-4def-8783-96089e6f0f82",
                    "LayerId": "c9167e13-d21f-4492-8d0d-2b65f2f23c68"
                }
            ]
        },
        {
            "id": "d978389b-ff0d-4ef8-8171-d53544e30a4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64530070-bfd4-444f-b705-03303f9fce85",
            "compositeImage": {
                "id": "5e33c2bd-595a-4b04-acb5-0d37c1e58044",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d978389b-ff0d-4ef8-8171-d53544e30a4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ede6f3c5-b7ee-48ea-bf48-9526ff9351b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d978389b-ff0d-4ef8-8171-d53544e30a4c",
                    "LayerId": "c9167e13-d21f-4492-8d0d-2b65f2f23c68"
                }
            ]
        },
        {
            "id": "8fc72a61-bfd0-4998-9bfb-748355079f60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64530070-bfd4-444f-b705-03303f9fce85",
            "compositeImage": {
                "id": "80f34165-f6d3-467c-b94b-463939f46360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fc72a61-bfd0-4998-9bfb-748355079f60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6862bf3d-7fa2-4e74-a776-38d1537b46e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fc72a61-bfd0-4998-9bfb-748355079f60",
                    "LayerId": "c9167e13-d21f-4492-8d0d-2b65f2f23c68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c9167e13-d21f-4492-8d0d-2b65f2f23c68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64530070-bfd4-444f-b705-03303f9fce85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}