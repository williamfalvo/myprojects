{
    "id": "c86fa4fe-a724-4ce0-b661-5c849fd14ad8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CUTSCENE_MAGAZZINO",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1561b670-bef5-4af3-aa6d-c194c1ec6acf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c86fa4fe-a724-4ce0-b661-5c849fd14ad8",
            "compositeImage": {
                "id": "df34e2e9-91f9-43dd-9dc4-a731646f3e77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1561b670-bef5-4af3-aa6d-c194c1ec6acf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b000ad7-390c-4c39-9d90-a7f8530f20af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1561b670-bef5-4af3-aa6d-c194c1ec6acf",
                    "LayerId": "7842a6cb-d50d-4752-8cd2-58ad8e2bf131"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "7842a6cb-d50d-4752-8cd2-58ad8e2bf131",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c86fa4fe-a724-4ce0-b661-5c849fd14ad8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}