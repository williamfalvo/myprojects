{
    "id": "4f830257-47bd-445b-b7f2-81118f607625",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_down_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6888db75-f8a9-4eef-9eb4-045e519b7410",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f830257-47bd-445b-b7f2-81118f607625",
            "compositeImage": {
                "id": "7bf9498c-bdde-433c-a99f-dd60a3c9b9f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6888db75-f8a9-4eef-9eb4-045e519b7410",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e9c229f-738b-4c0d-b086-a32d5e274d1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6888db75-f8a9-4eef-9eb4-045e519b7410",
                    "LayerId": "fe95258c-d444-416c-bffc-bed0a6aa59f4"
                }
            ]
        },
        {
            "id": "daadc0fd-b6ee-4cc9-a633-a2508b01d837",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f830257-47bd-445b-b7f2-81118f607625",
            "compositeImage": {
                "id": "db2aa9e3-4d2f-4208-b117-eedcf4bae7dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daadc0fd-b6ee-4cc9-a633-a2508b01d837",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "712379b7-286a-4097-942d-2f1bd9706b14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daadc0fd-b6ee-4cc9-a633-a2508b01d837",
                    "LayerId": "fe95258c-d444-416c-bffc-bed0a6aa59f4"
                }
            ]
        },
        {
            "id": "7adb985d-2bce-4056-89ca-195fe53efb45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f830257-47bd-445b-b7f2-81118f607625",
            "compositeImage": {
                "id": "e0007b47-d630-4883-8708-e886ea30213e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7adb985d-2bce-4056-89ca-195fe53efb45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e4d1bed-4206-4500-a787-8ac1448cde4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7adb985d-2bce-4056-89ca-195fe53efb45",
                    "LayerId": "fe95258c-d444-416c-bffc-bed0a6aa59f4"
                }
            ]
        },
        {
            "id": "1c8a6680-fe18-475b-a13a-b77ddae97fc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f830257-47bd-445b-b7f2-81118f607625",
            "compositeImage": {
                "id": "b069c92d-7dd7-4a87-8557-74fe9cae8f4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c8a6680-fe18-475b-a13a-b77ddae97fc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6357c927-7fa3-44d7-aa20-401c1c656a98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c8a6680-fe18-475b-a13a-b77ddae97fc8",
                    "LayerId": "fe95258c-d444-416c-bffc-bed0a6aa59f4"
                }
            ]
        },
        {
            "id": "65de706b-188b-460a-8c54-81c5177b8d85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f830257-47bd-445b-b7f2-81118f607625",
            "compositeImage": {
                "id": "847c205b-b8b9-4a5b-8886-b0d69431fad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65de706b-188b-460a-8c54-81c5177b8d85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bebdf203-97d4-40fe-8662-127423605f24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65de706b-188b-460a-8c54-81c5177b8d85",
                    "LayerId": "fe95258c-d444-416c-bffc-bed0a6aa59f4"
                }
            ]
        },
        {
            "id": "b94a8a00-9691-4bbf-80c1-54117cc4d112",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f830257-47bd-445b-b7f2-81118f607625",
            "compositeImage": {
                "id": "8b60aade-54a0-4cd2-a95e-2a6bf5c11913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b94a8a00-9691-4bbf-80c1-54117cc4d112",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b36c046-6b7c-4d7e-a114-7e6824b1f110",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b94a8a00-9691-4bbf-80c1-54117cc4d112",
                    "LayerId": "fe95258c-d444-416c-bffc-bed0a6aa59f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "fe95258c-d444-416c-bffc-bed0a6aa59f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f830257-47bd-445b-b7f2-81118f607625",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}