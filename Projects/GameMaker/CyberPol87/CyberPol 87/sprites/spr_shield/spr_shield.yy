{
    "id": "0cc6bc4a-f446-490f-b220-015d8d9d28f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shield",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 34,
    "bbox_right": 45,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e14177fe-9469-4a9c-9c19-67fb71897788",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cc6bc4a-f446-490f-b220-015d8d9d28f3",
            "compositeImage": {
                "id": "36e69349-6994-4881-91eb-c5e36c76ba80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e14177fe-9469-4a9c-9c19-67fb71897788",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da4cce9f-9f9a-4768-bfc4-864a34c49bbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e14177fe-9469-4a9c-9c19-67fb71897788",
                    "LayerId": "ad8ed03e-6148-4122-991b-3f276372efc4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "ad8ed03e-6148-4122-991b-3f276372efc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cc6bc4a-f446-490f-b220-015d8d9d28f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}