{
    "id": "018de8e2-b591-4603-b00c-dc0a1b703bed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_testo_finale2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 137,
    "bbox_left": 24,
    "bbox_right": 563,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8107bf07-1fce-4c14-aa8c-958e8f56daa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "018de8e2-b591-4603-b00c-dc0a1b703bed",
            "compositeImage": {
                "id": "35775143-5641-4d0b-96b3-4c5558c4a5d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8107bf07-1fce-4c14-aa8c-958e8f56daa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b771aea-efee-4ead-99f6-207f9d7a2733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8107bf07-1fce-4c14-aa8c-958e8f56daa2",
                    "LayerId": "f8f455d1-d926-4577-9059-2f292233ea7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 310,
    "layers": [
        {
            "id": "f8f455d1-d926-4577-9059-2f292233ea7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "018de8e2-b591-4603-b00c-dc0a1b703bed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 610,
    "xorig": 305,
    "yorig": 155
}