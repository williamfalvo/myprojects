{
    "id": "7392cfbf-56d5-4bdd-9ea2-1c09927e0c13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gunpoint_rightDown_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 16,
    "bbox_right": 29,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85a9b576-9bc9-43c4-98dc-4af867e34e6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7392cfbf-56d5-4bdd-9ea2-1c09927e0c13",
            "compositeImage": {
                "id": "6abea736-c3a9-4d30-a4c5-c114ddc6ae48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85a9b576-9bc9-43c4-98dc-4af867e34e6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbd0fce5-f656-43fd-aab6-cbe77c4210df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85a9b576-9bc9-43c4-98dc-4af867e34e6c",
                    "LayerId": "2a805a33-113f-43d3-bc32-270a729a5c34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "2a805a33-113f-43d3-bc32-270a729a5c34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7392cfbf-56d5-4bdd-9ea2-1c09927e0c13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}