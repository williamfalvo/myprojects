{
    "id": "7bb73fa4-72ad-4d6f-bc48-2d4743ce91a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_healtBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 7,
    "bbox_right": 56,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b333885-d613-4429-b24c-6a7072b2bab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bb73fa4-72ad-4d6f-bc48-2d4743ce91a8",
            "compositeImage": {
                "id": "b390d323-658c-4181-a18e-b96ba8bd15c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b333885-d613-4429-b24c-6a7072b2bab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e18d470-8749-4150-925f-5af9044ad317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b333885-d613-4429-b24c-6a7072b2bab5",
                    "LayerId": "1dbb010d-2c87-4833-9269-c44a32d4031f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1dbb010d-2c87-4833-9269-c44a32d4031f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bb73fa4-72ad-4d6f-bc48-2d4743ce91a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 7,
    "yorig": 32
}