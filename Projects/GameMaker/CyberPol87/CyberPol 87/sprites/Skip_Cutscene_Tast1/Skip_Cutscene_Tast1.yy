{
    "id": "6a6c16bf-3009-48ba-8c88-6700c87626e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Skip_Cutscene_Tast1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 5,
    "bbox_right": 146,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45e64dd8-670e-4213-9726-e9bd00583383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6c16bf-3009-48ba-8c88-6700c87626e2",
            "compositeImage": {
                "id": "39692637-769d-436c-9afa-7179cea5c37f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45e64dd8-670e-4213-9726-e9bd00583383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "637d7ebb-34d3-4984-a3a2-21373f9eed15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45e64dd8-670e-4213-9726-e9bd00583383",
                    "LayerId": "1c8ddf28-77ce-46b3-b76b-9b82860555dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "1c8ddf28-77ce-46b3-b76b-9b82860555dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a6c16bf-3009-48ba-8c88-6700c87626e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 75,
    "yorig": 36
}