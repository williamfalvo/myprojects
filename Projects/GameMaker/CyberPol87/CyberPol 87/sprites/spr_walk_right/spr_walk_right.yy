{
    "id": "b285d13a-d3b4-4b74-9fe3-f3def6c97184",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 15,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e25d5f2-2ea7-4c02-a7a1-0e2b1379605c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b285d13a-d3b4-4b74-9fe3-f3def6c97184",
            "compositeImage": {
                "id": "9c17a267-348b-4087-b716-7e041dcc0a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e25d5f2-2ea7-4c02-a7a1-0e2b1379605c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c2661eb-c1f8-4a28-95d8-73816c4b0628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e25d5f2-2ea7-4c02-a7a1-0e2b1379605c",
                    "LayerId": "0e1d0f5d-84fa-437f-8b1c-2d7dd2937fa6"
                }
            ]
        },
        {
            "id": "6bc15488-fefa-47af-b2a9-1daa8a0f5c0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b285d13a-d3b4-4b74-9fe3-f3def6c97184",
            "compositeImage": {
                "id": "6276bdd0-cc03-4c40-a45a-e7a6646f205d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bc15488-fefa-47af-b2a9-1daa8a0f5c0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9d68cf1-ecda-4868-bd6e-d58e36674e56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bc15488-fefa-47af-b2a9-1daa8a0f5c0b",
                    "LayerId": "0e1d0f5d-84fa-437f-8b1c-2d7dd2937fa6"
                }
            ]
        },
        {
            "id": "6fa24a7c-9d64-40e0-9474-ce1c6bafda1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b285d13a-d3b4-4b74-9fe3-f3def6c97184",
            "compositeImage": {
                "id": "e0732511-e9eb-4d8b-a937-6cb0bad1fe30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fa24a7c-9d64-40e0-9474-ce1c6bafda1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b7cd81-43f1-475a-85c3-df2b68aebe36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fa24a7c-9d64-40e0-9474-ce1c6bafda1f",
                    "LayerId": "0e1d0f5d-84fa-437f-8b1c-2d7dd2937fa6"
                }
            ]
        },
        {
            "id": "35ea1fc0-21c3-4bb3-a238-a4519c1925e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b285d13a-d3b4-4b74-9fe3-f3def6c97184",
            "compositeImage": {
                "id": "c1788b70-de50-4559-aada-4516f0cdd530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35ea1fc0-21c3-4bb3-a238-a4519c1925e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bb89af3-77a0-4144-a186-4c992f92ac00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ea1fc0-21c3-4bb3-a238-a4519c1925e4",
                    "LayerId": "0e1d0f5d-84fa-437f-8b1c-2d7dd2937fa6"
                }
            ]
        },
        {
            "id": "4f0a687e-f2af-4c97-81ea-cdc60bce24be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b285d13a-d3b4-4b74-9fe3-f3def6c97184",
            "compositeImage": {
                "id": "1e1702f6-9c65-420c-bacf-bc3944635a5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f0a687e-f2af-4c97-81ea-cdc60bce24be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "084f4887-7916-4e00-9801-a876b06c194f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f0a687e-f2af-4c97-81ea-cdc60bce24be",
                    "LayerId": "0e1d0f5d-84fa-437f-8b1c-2d7dd2937fa6"
                }
            ]
        },
        {
            "id": "4d0b4137-c9f3-427e-82a6-cb4125d1b58d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b285d13a-d3b4-4b74-9fe3-f3def6c97184",
            "compositeImage": {
                "id": "6bceedae-37a6-4f32-ac03-27decfbfde1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d0b4137-c9f3-427e-82a6-cb4125d1b58d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6fe5310-7796-4ccc-9bf5-b7d419d39f9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d0b4137-c9f3-427e-82a6-cb4125d1b58d",
                    "LayerId": "0e1d0f5d-84fa-437f-8b1c-2d7dd2937fa6"
                }
            ]
        },
        {
            "id": "878c0f9b-e473-45bf-a309-88576bac46e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b285d13a-d3b4-4b74-9fe3-f3def6c97184",
            "compositeImage": {
                "id": "86546b4d-0627-42b7-9e95-b5f4fb5d242d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "878c0f9b-e473-45bf-a309-88576bac46e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "389fe1f9-d33e-4fd7-9e35-026143366336",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "878c0f9b-e473-45bf-a309-88576bac46e0",
                    "LayerId": "0e1d0f5d-84fa-437f-8b1c-2d7dd2937fa6"
                }
            ]
        },
        {
            "id": "418130a2-a478-456e-bd41-dc686429183e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b285d13a-d3b4-4b74-9fe3-f3def6c97184",
            "compositeImage": {
                "id": "9e8c7bb5-4ef2-4913-8022-e1aab09543b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "418130a2-a478-456e-bd41-dc686429183e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e290c0d-1761-44e3-838b-212587bc3156",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "418130a2-a478-456e-bd41-dc686429183e",
                    "LayerId": "0e1d0f5d-84fa-437f-8b1c-2d7dd2937fa6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "0e1d0f5d-84fa-437f-8b1c-2d7dd2937fa6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b285d13a-d3b4-4b74-9fe3-f3def6c97184",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}