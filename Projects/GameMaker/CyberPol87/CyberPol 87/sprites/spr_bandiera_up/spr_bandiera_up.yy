{
    "id": "49a2f92b-a23a-4156-986e-da531e441693",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bandiera_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afce6272-0510-4bd4-89ba-d8cde22b3f67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49a2f92b-a23a-4156-986e-da531e441693",
            "compositeImage": {
                "id": "b11138ca-b741-4f0b-9b6a-2b1c7d91c7ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afce6272-0510-4bd4-89ba-d8cde22b3f67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08b01408-ffd6-4e9f-9970-cb20515d0be8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afce6272-0510-4bd4-89ba-d8cde22b3f67",
                    "LayerId": "7fc4d2a9-0b41-467e-9dd0-bc8a1700636e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7fc4d2a9-0b41-467e-9dd0-bc8a1700636e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49a2f92b-a23a-4156-986e-da531e441693",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}