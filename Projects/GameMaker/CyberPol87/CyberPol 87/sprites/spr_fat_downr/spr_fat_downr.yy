{
    "id": "d51f6643-26c9-4eac-bcee-f8c82d03e519",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fat_downr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 9,
    "bbox_right": 38,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1f965fb-422f-422a-86bf-27dab139e8e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d51f6643-26c9-4eac-bcee-f8c82d03e519",
            "compositeImage": {
                "id": "79cc3e57-a2d6-4c81-8750-4a70dc6d374a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f965fb-422f-422a-86bf-27dab139e8e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80f8009f-8854-470c-a616-a16c7f3e2a98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f965fb-422f-422a-86bf-27dab139e8e5",
                    "LayerId": "516fe5d4-289d-437a-9977-961eed6dc5f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "516fe5d4-289d-437a-9977-961eed6dc5f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d51f6643-26c9-4eac-bcee-f8c82d03e519",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}