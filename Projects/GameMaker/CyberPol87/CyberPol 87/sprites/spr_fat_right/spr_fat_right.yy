{
    "id": "661eae07-fba8-472a-bce9-44987a7ba0fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fat_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 49,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a85f8a87-0593-4f2c-9b3e-1450ab57588f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "661eae07-fba8-472a-bce9-44987a7ba0fa",
            "compositeImage": {
                "id": "6678cef1-a802-4c16-812b-14648999449b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a85f8a87-0593-4f2c-9b3e-1450ab57588f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dc3c808-8bde-47b0-9b42-2bf3b3878b2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a85f8a87-0593-4f2c-9b3e-1450ab57588f",
                    "LayerId": "96c93c8a-7cb3-4c7f-87c6-881b3b4b8cc9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "96c93c8a-7cb3-4c7f-87c6-881b3b4b8cc9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "661eae07-fba8-472a-bce9-44987a7ba0fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}