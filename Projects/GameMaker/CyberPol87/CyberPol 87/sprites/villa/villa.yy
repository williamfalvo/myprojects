{
    "id": "ec3091d1-db10-461b-94fb-a3c221d6f154",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "villa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 818,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2f3e8da-9660-4463-8f3e-c940efcf1f67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec3091d1-db10-461b-94fb-a3c221d6f154",
            "compositeImage": {
                "id": "367dd07d-d216-473c-bc0b-c653bbb26b0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2f3e8da-9660-4463-8f3e-c940efcf1f67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a7260b-7436-402e-b3c8-5a6517a51159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2f3e8da-9660-4463-8f3e-c940efcf1f67",
                    "LayerId": "517ce1e2-1e00-4491-b2d0-22664dd5f342"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 832,
    "layers": [
        {
            "id": "517ce1e2-1e00-4491-b2d0-22664dd5f342",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec3091d1-db10-461b-94fb-a3c221d6f154",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}