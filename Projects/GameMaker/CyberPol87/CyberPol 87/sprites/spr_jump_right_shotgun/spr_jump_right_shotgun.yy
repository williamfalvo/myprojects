{
    "id": "73b223e4-2e02-4cef-be0f-e04da1778fca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_right_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "591a2a1c-e8d4-48df-adfc-be2f88339f75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73b223e4-2e02-4cef-be0f-e04da1778fca",
            "compositeImage": {
                "id": "29681131-cb90-4f80-92e8-d04184060e02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "591a2a1c-e8d4-48df-adfc-be2f88339f75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7650445-703f-4f91-bf68-b797e8b6026c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "591a2a1c-e8d4-48df-adfc-be2f88339f75",
                    "LayerId": "624975c6-a5d4-4920-a493-c699f9e203ae"
                }
            ]
        },
        {
            "id": "5d476847-2011-4a8a-814a-b597104e1f2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73b223e4-2e02-4cef-be0f-e04da1778fca",
            "compositeImage": {
                "id": "f9c2fe72-e63e-4d2b-b817-2523cf47ce9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d476847-2011-4a8a-814a-b597104e1f2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1e2126e-4dcd-4ac2-9f06-d1cc8ae0762c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d476847-2011-4a8a-814a-b597104e1f2b",
                    "LayerId": "624975c6-a5d4-4920-a493-c699f9e203ae"
                }
            ]
        },
        {
            "id": "c19d07ed-f496-4c0b-aa3c-967a0d92dbc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73b223e4-2e02-4cef-be0f-e04da1778fca",
            "compositeImage": {
                "id": "ebc9e730-ca44-41a0-8581-8a0c165988e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c19d07ed-f496-4c0b-aa3c-967a0d92dbc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d1abc52-3e63-45e8-bcb9-ecdf54e9af67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c19d07ed-f496-4c0b-aa3c-967a0d92dbc6",
                    "LayerId": "624975c6-a5d4-4920-a493-c699f9e203ae"
                }
            ]
        },
        {
            "id": "24ba213a-eaef-46ce-9775-947458aebb96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73b223e4-2e02-4cef-be0f-e04da1778fca",
            "compositeImage": {
                "id": "8773acc9-b042-4a32-b29a-772d9a2394f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24ba213a-eaef-46ce-9775-947458aebb96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba9b7273-b3e1-42e5-9ff2-37234f65d505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24ba213a-eaef-46ce-9775-947458aebb96",
                    "LayerId": "624975c6-a5d4-4920-a493-c699f9e203ae"
                }
            ]
        },
        {
            "id": "b9179ba3-2537-473f-985b-cad24b604dcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73b223e4-2e02-4cef-be0f-e04da1778fca",
            "compositeImage": {
                "id": "b1f59144-71f3-42b9-9a5c-5bfb778006d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9179ba3-2537-473f-985b-cad24b604dcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e3a640b-27e2-44a5-a884-6d72aedf004f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9179ba3-2537-473f-985b-cad24b604dcb",
                    "LayerId": "624975c6-a5d4-4920-a493-c699f9e203ae"
                }
            ]
        },
        {
            "id": "2571034b-9b94-458c-9242-f0497cd0bccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73b223e4-2e02-4cef-be0f-e04da1778fca",
            "compositeImage": {
                "id": "aa31bcb7-7da4-405c-8c88-fea03798cba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2571034b-9b94-458c-9242-f0497cd0bccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e000832-3f58-4011-972d-5d8c30a54c71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2571034b-9b94-458c-9242-f0497cd0bccc",
                    "LayerId": "624975c6-a5d4-4920-a493-c699f9e203ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "624975c6-a5d4-4920-a493-c699f9e203ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73b223e4-2e02-4cef-be0f-e04da1778fca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}