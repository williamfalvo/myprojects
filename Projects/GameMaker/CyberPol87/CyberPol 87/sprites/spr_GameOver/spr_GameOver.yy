{
    "id": "d4752fcc-e77b-4c96-946d-296f0731bcfe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GameOver",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 13,
    "bbox_right": 168,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c34fd26d-a1f9-4a48-ad92-2c29e1c82c7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4752fcc-e77b-4c96-946d-296f0731bcfe",
            "compositeImage": {
                "id": "f76acd06-2d6e-4724-8763-09ff4c758b21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c34fd26d-a1f9-4a48-ad92-2c29e1c82c7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "004b297a-b910-459d-b857-ebb71fe10e4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c34fd26d-a1f9-4a48-ad92-2c29e1c82c7b",
                    "LayerId": "2cfe5b11-ad6e-4aaa-956a-e19d45394cbb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "2cfe5b11-ad6e-4aaa-956a-e19d45394cbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4752fcc-e77b-4c96-946d-296f0731bcfe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 183,
    "xorig": 91,
    "yorig": 26
}