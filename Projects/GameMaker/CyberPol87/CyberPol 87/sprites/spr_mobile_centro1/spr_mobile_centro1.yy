{
    "id": "3a6af59a-5d31-421b-81af-114972e9ccd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mobile_centro1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ffd220a-9e31-419e-9a8e-70ed97e74134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a6af59a-5d31-421b-81af-114972e9ccd9",
            "compositeImage": {
                "id": "25d1df6d-49ff-48c9-bc73-e649b22b9e82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ffd220a-9e31-419e-9a8e-70ed97e74134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f5db554-3338-460c-9e60-ebbda7bf8611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ffd220a-9e31-419e-9a8e-70ed97e74134",
                    "LayerId": "00949f1a-9dca-43af-9a1b-2d42af7bd23a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "00949f1a-9dca-43af-9a1b-2d42af7bd23a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a6af59a-5d31-421b-81af-114972e9ccd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}