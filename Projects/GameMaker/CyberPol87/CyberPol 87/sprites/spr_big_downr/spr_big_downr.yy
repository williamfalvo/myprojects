{
    "id": "0494df48-f737-4f32-b2da-ecbbeeb84da3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_big_downr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 14,
    "bbox_right": 49,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ee58657-f7a0-4c89-aadf-7f805d32a4d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0494df48-f737-4f32-b2da-ecbbeeb84da3",
            "compositeImage": {
                "id": "7a5fb8a4-e1fc-427b-b272-4701a691d7c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ee58657-f7a0-4c89-aadf-7f805d32a4d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "477e019a-4ac1-4545-9a84-48bbe25ad611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ee58657-f7a0-4c89-aadf-7f805d32a4d1",
                    "LayerId": "06f82ce4-a942-4aa3-ab38-a29e7d1d36e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "06f82ce4-a942-4aa3-ab38-a29e7d1d36e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0494df48-f737-4f32-b2da-ecbbeeb84da3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}