{
    "id": "c95576a8-b1ec-47de-9649-8b9755650723",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump_rightDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 14,
    "bbox_right": 30,
    "bbox_top": 26,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39f2aee8-10d6-4c59-903c-04884261afdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c95576a8-b1ec-47de-9649-8b9755650723",
            "compositeImage": {
                "id": "00c79f61-724b-4837-8639-9642d049e2a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39f2aee8-10d6-4c59-903c-04884261afdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc6e0b69-9f14-4c1b-b48b-2cf49ecbfec0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39f2aee8-10d6-4c59-903c-04884261afdb",
                    "LayerId": "5bfc59db-3cfa-4819-8445-ef1be82bce84"
                }
            ]
        },
        {
            "id": "f758e81a-8516-4f63-acba-66865ef8e3b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c95576a8-b1ec-47de-9649-8b9755650723",
            "compositeImage": {
                "id": "45fd6687-7a2c-4fd9-902f-7105d48ee234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f758e81a-8516-4f63-acba-66865ef8e3b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d343ab-47b5-4fe1-ab4a-e2dd297babb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f758e81a-8516-4f63-acba-66865ef8e3b5",
                    "LayerId": "5bfc59db-3cfa-4819-8445-ef1be82bce84"
                }
            ]
        },
        {
            "id": "47c3dd4d-2ddd-43e5-bc5d-07d4c4cc1c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c95576a8-b1ec-47de-9649-8b9755650723",
            "compositeImage": {
                "id": "bbb9ca1d-2cb0-4865-a181-c7805107a13f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47c3dd4d-2ddd-43e5-bc5d-07d4c4cc1c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d17fbb32-672a-4d68-b289-d838a3304810",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47c3dd4d-2ddd-43e5-bc5d-07d4c4cc1c82",
                    "LayerId": "5bfc59db-3cfa-4819-8445-ef1be82bce84"
                }
            ]
        },
        {
            "id": "cc4b15b6-dc6b-4a08-84e1-1ee29960d033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c95576a8-b1ec-47de-9649-8b9755650723",
            "compositeImage": {
                "id": "477b7d7c-64a9-48ae-a602-93eff10a1188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc4b15b6-dc6b-4a08-84e1-1ee29960d033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "831a2caa-3cf0-401c-9ab4-252a4412653a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc4b15b6-dc6b-4a08-84e1-1ee29960d033",
                    "LayerId": "5bfc59db-3cfa-4819-8445-ef1be82bce84"
                }
            ]
        },
        {
            "id": "ecba3e52-8e4f-4fc1-88a6-6dcddd9dcff0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c95576a8-b1ec-47de-9649-8b9755650723",
            "compositeImage": {
                "id": "37ad5a90-3101-4b89-84b7-66d7d7202dab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecba3e52-8e4f-4fc1-88a6-6dcddd9dcff0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d283d54-2de4-4802-9f56-4c48ef39ed95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecba3e52-8e4f-4fc1-88a6-6dcddd9dcff0",
                    "LayerId": "5bfc59db-3cfa-4819-8445-ef1be82bce84"
                }
            ]
        },
        {
            "id": "23766536-4c2f-42e7-936e-378b41e7e764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c95576a8-b1ec-47de-9649-8b9755650723",
            "compositeImage": {
                "id": "6d5fdfcb-a5a7-4225-b143-70c2d1a3fc94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23766536-4c2f-42e7-936e-378b41e7e764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "827003fd-c393-491b-97d9-b842d0def6a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23766536-4c2f-42e7-936e-378b41e7e764",
                    "LayerId": "5bfc59db-3cfa-4819-8445-ef1be82bce84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "5bfc59db-3cfa-4819-8445-ef1be82bce84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c95576a8-b1ec-47de-9649-8b9755650723",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 41
}