{
    "id": "14e6205c-993e-4128-b5ba-2749ece9e7b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Testo_Magazzino",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 9,
    "bbox_right": 600,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87b6761b-5ddd-465c-8719-8244ac50f8bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14e6205c-993e-4128-b5ba-2749ece9e7b1",
            "compositeImage": {
                "id": "5a859833-e268-4fe0-a00f-d3207f2a8d58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87b6761b-5ddd-465c-8719-8244ac50f8bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21c31148-6550-4c61-822d-323adc3aa024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87b6761b-5ddd-465c-8719-8244ac50f8bd",
                    "LayerId": "96b08a89-8364-4349-8fe2-22993a7a42c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "96b08a89-8364-4349-8fe2-22993a7a42c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14e6205c-993e-4128-b5ba-2749ece9e7b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 610,
    "xorig": 305,
    "yorig": 55
}